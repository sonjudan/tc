<?php
$meta_title = "Adobe Photoshop 2019 Introduction | Hands-on beginner training";
$meta_description = "1-day Quick start Photoshop class in Chicago and LA. Hands-on learning from Photoshop experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../photoshop-training.php">Photoshop Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Quick Start</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Photoshop 2020</h1>
                    <h3 data-aos="fade-up" data-aos-delay="150">Quickstart Training Course</h3>

                    <p data-aos="fade-up" data-aos-delay="200">This 1-day beginner class is designed to get you up and running quickly in Photoshop. You will learn basic photo retouching, color correction, selections, replacing backgrounds, adding type, and preparing your images for print and web.</p>

                    <div class="mt-lg-5 mb-lg-5" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>1 Day</span>
                        <sup>$</sup>495<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg" data-target="#book-class-level-1" data-toggle="modal">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Photoshop Quick Start" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Photoshop Quick Start" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/photoshop/TC_Photoshop_2019_Quickstart.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-photoshop-quickstart.png" alt="Photoshop 2019 - Quickstart Training Course" class="" width="250">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>This course is ideal for designers, marketers and anyone who is pursuing a career in graphic design.	No prior experience of Photoshop is needed. Training available on Mac and PC.</p>
                    <p>This 6-hour class is meant to get you started fast. If you want to  learn Photoshop in greater depth, check out our 18-hour <a class="photoshop" href="/photoshop/fundamentals.php">Photoshop Fundamentals</a> class or view our full range of <a class="photoshop" href="/photoshop-training.php">Adobe Photoshop training courses</a>.</p>
                </div>

                <a href="#book-class-level-1" class="btn btn-secondary btn-lg" data-target="#book-class-level-1" data-toggle="modal" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/photoshop/sections/course-outline-introduction.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-photoshop-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-photoshop.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>