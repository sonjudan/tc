<?php
$meta_title = "Getting to know the Photoshop CC Interface | Training Connection";
$meta_description = "Learn the Photoshop Interface, Preferences and Workspaces. This course is part of our Fundamentals Photoshop Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Photoshop Overview</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Getting to know the Photoshop Interface</h1>
                        <h5>These  topics are covered  in Module 1 - Getting to know Photoshop, part of our <a href="/photoshop/fundamentals.php">Beginner Photoshop class</a>.</h5>
                    </div>
                </div>

                <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-1.jpg" width="750" height="452" alt="The Photoshop Interface"></p>

                    <p><img class="alignright" src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-2.jpg" width="288" height="675" alt="Photoshop Toolbar"></p>

                    <p>The <strong>Menu Bar</strong> across the top organizes commands under menus.</p>

                    <p>The <strong>Application Bar</strong> is no longer in Photoshop CC, since the interface has been streamlined.</p>

                    <p>The <strong>Toolbar</strong> contains tools for creating, selecting and editing images.</p>
                    <p>Related tools are grouped together  indicated by a <strong>small triangle</strong> in the bottom right corner.</p>
                    <p>The <strong>Options Bar </strong>displays options for the currently selected tool.</p>
                    <p><a href="/photoshop-training-chicago.php">Adobe Photoshop classes in the Chicago Loop</a>.</p>
                    <p>The <strong>Document Window</strong> displays the images you're working on.</p>
                    <p><strong>Panels</strong> help you monitor and modify your work. Certain panels are displayed by default, but you
                        can add any panel by selecting it from the <strong>Window</strong> menu. Panels have menus with panel-specific options.</p>
                    <p>The <strong>Panel Menu</strong> is accessed by clicking on the icon at the <strong>top right corner</strong> of the panel to expand it.</p>
                    <p>A <strong>Dock</strong> is a collection of panels or panel groups displayed together, generally in a vertical orientation. You dock and undock panels by moving them into and out of a dock.</p>
                    <p>To <strong>dock a panel</strong>, drag it by its tab into the dock, at the top, bottom, or in between other panels.</p>
                    <p>To <strong>dock a panel group</strong>, drag it by its title bar (the solid empty bar above the tabs) into the dock.</p>
                    <p>To <strong>remove a panel or panel group</strong>, drag it out of the dock by its tab or title bar. You can drag it into another dock or make it free-floating.</p>
                    <p>For instructor-led <a href="/photoshop-training.php">Adobe Photoshop courses in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-3.jpg" width="750" height="456" alt="Tapped Photoshop Interface"></p>

                    <p><img class="alignright" src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-4.jpg" width="204" height="402" alt="Numbered Tab Views in Photoshop"></p>

                    <p>The interface can be tabbed, showing one image or tiled showing multiple images at a time.</p>

                    <p>Choose <strong>Window &gt; Arrange</strong>, you can tile all open images in <strong>n-up views</strong> or float them in separate windows.</p>

                    <p><strong>Consolidate All to Tabs</strong> shows only one image at a time.</p>

                    <h4>Photoshop CC Preferences</h4>
                    <p>To access the application preferences, choose <strong>Photoshop &gt; Preferences</strong> (Mac)
                        or <strong>Edit &gt; Preferences</strong> (PC) (<strong>Cmd/Ctrl + K</strong>).</p>

                    <p>One of the new features is the ability to change the interface color theme to make viewing,color correcting and editing your images easier.
                        Photoshop <strong>auto-saves</strong> your work so that if the application crashes, it will restore your image based on the last time is was saved, whether you saved the file manually or not.</p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-5.jpg" width="750" height="217" alt="Photoshop CC 2014 Preferences"></p>

                    <h4 class="excel"><a name="photoshop-workspaces"></a>
                        Photoshop CC Workspaces</h4> <p><img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-6.jpg" width="171" height="290" alt="Photoshop Workspaces - Bubbles"></p>
                    <p>The Options Bar offers a shortcut to access your Workspaces via the <strong>switcher</strong>. <a href="/photoshop-training-los-angeles.php">Instructor-led Photoshop classes</a>.</p>
                    <p>By saving the current size and position of panels as a named workspace, you can restore that workspace even if you move or close a panel. The names of saved workspaces also appear in the <strong>Window &gt; Workspace</strong> menu.</p>

                    <p>Choose <strong>New Workspace </strong>to save a custom workspace.</p>

                    <p><img class="alignright" src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-7.jpg" width="363" height="178" alt="New Photoshop Workspace"></p>

                    <p>The saved workspace can include a specific <strong>keyboard shortcut set</strong> and menu set in addition to panel locations.</p>
                    <p>If changes are made to a Workspace, choose <strong>New Workspace</strong> and retype the exact name to save over it and update it. To put the panels back in the right location, choose <strong>Reset (name)</strong>.</p>
                    <p>If you have Workspaces saved form CS6, you can Migrate them.</p>
                    <p><img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-8.jpg" width="516" height="180" alt="Photoshop Preferences"></p>

                    <p>Custom Keyboard Shortcuts &amp; Preferences</p>
                    <p>Set up Multiple Un-do's in Photoshop by exchanging your keyboard shortcuts of Undo/Redo and Step Backwards. </p>

                    <p>&gt;Choose <strong>Photoshop &gt; Preferences</strong> (Mac) or <strong>Edit &gt; Preferences</strong> (PC).</p>

                    <p>In the Performance section you can increase the number of History States.</p>

                    <p>The number of History States determines how many "undo's" there are.</p>

                    <p>The default is only 20, so increase the number to 200 or more.</p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-9.jpg" width="750" height="223" alt="Photoshop Shortcuts and Preferences"></p>

                    <p>Another Preference worth changing is the compatibility question this comes up whenever a native Photoshop file (PSD) is saved and a dialog box requires a decision to maximize the compatibility of the file. To permanently eliminate this, change the Preference in the Interface section from "Ask" (the default) to "Always" in the File Handling section. </p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-10.jpg" width="630" height="416" alt="Photoshop File Save preferences"></p>

                    <p>The history of edits done to the file can be captured as text or metadata by selecting the History Log in Preferences.</p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-11.jpg" width="485" height="359" alt="History Log"></p>

                    <h4>Menu Customization</h4>

                    <p>Choose <strong>Edit &gt; Menus</strong> to customize menus by turning off <strong>visibility</strong> of never used items and <strong>color coding</strong> on for more important ones.</p>

                    <p>Click the <strong>eye icon</strong> to turn on/off  a particular menu item. Click the <strong>None</strong> option in the Color field to display a list of color labels to apply to that menu command.</p>

                    <p>When menus have been customized, they will show (modified) after the name of the menu set. To keep these changes, save your Workspace again with the same name and make sure the Menus option is checked.</p>

                    <p>Once the menus have been customized they will show their color coding on the commands specified.</p>

                    <p>When a menu has items hidden, the bottom will have an option to <strong>Show All Menu Items</strong> and temporarily expand it.</p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-12.jpg" width="653" height="705" alt="Shortcuts and Menus"></p>

                    <p class="aligncenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-13.jpg" width="750" height="466" alt="Menu Customization"></p>
                </div>

            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Photoshop Lessons</h4>

                    <ul>
                        <li><a href="/photoshop/lessons/image-formats.php">Image File Formats</a></li>
                        <li><a href="/photoshop/lessons/image-formats.php#photoshop-image-resolution">Photoshop Image Size and Resolution</a></li>
                        <li><a href="/photoshop/lessons/image-formats.php#photoshop-image-workflow">Image Production Workflow</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Photoshop. We have trained hundreds of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop student testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>