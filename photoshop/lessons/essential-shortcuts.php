<?php
$meta_title = "Useful Photoshop Shortcuts | Training Connection";
$meta_description = "This article covers all the essential Photoshop shortcuts to help you improve your productivity. We deliver Adobe Photoshop Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Shortcuts</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Essential Shortcuts in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Multitasking is king in Photoshop. There will be many times where you will need to perform multiple commands simultaneously. You might be Zooming In, while changing your Brush Size to Clone part of your image. Knowing how to use shortcuts allows you to do this.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/0-Shortcuts_Photoshop_Header.jpg" width="780" height="442" alt="Artcile on Photoshop Shortcuts"></p>

                <p>I am going to cover some of the go-to shortcuts with some useful tips. The commands are very similar across Adobe software and between PC and Mac computers. For Mac computers you will want to replace the Ctrl Key with the Command Key; and the Alt Key with the Option Key.</p>

                <p>Need to <a href="/photoshop-training.php">learn Adobe Photoshop?</a> Why not join one of our small classes  in Chicago or Los Angeles?</p>

                <h4>Creating a list of ShortCuts</h4>

                <p>You can create an online listing of Photoshop Shortcuts.</p>

                <p><strong>For Online Listing</strong></p>

                <ol>
                    <li>Select Keyboard Shortcuts at the bottom of the Edit Menu.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/1-KeyboardShortcuts_Photoshop.jpg" width="379" height="661" alt="Edit > Keyboard Shortcuts"></p>

                <ol start="2">
                    <li>Select the Summarize Button on the right-bottom of the dialog box.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/2-Add_Shortcuts_Photoshop.jpg" width="780" height="308" alt="Summarize button"> </p>

                <p>This will create an .html file that will open up a window in your default internet browser.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/4-Shortcuts_Online_CheatSheet.jpg" width="546" height="655" alt="HTML page of shortcuts you can print"></p>

                <h4>Customize Shortcuts</h4>

                <p>You can customize Shortcut Key combinations via Keyboard Shortcuts from the Edit Menu. <a href="/photoshop-training-chicago.php">Click here</a> for more on Photoshop training offered in Chicago.</p>

                <ol>
                    <li>Select the Shortcut Command you would like to Edit.</li>
                    <li> Click the text box for the shortcut key combination and type in a new combination of keys.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/3-Shortcuts_DialogBox.jpg" width="624" height="460" alt="Keyboard Shortcuts and Menus"></p>

                <ol start="3">
                    <li>You can also select the Add Shortcut button.</li>
                </ol>


                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/006_keyboard_shortcuts.jpg" width="539" height="212" alt="Add shotcut button"></p>

                <p>Here's a listing of common shortcuts by Menu.</p>

                <h4>These shortcuts are for commands found under the File Menu.</h4>

                <p>(Most of these shortcuts can be used in other software programs.)<br>
                    Ctrl + N Open a New Document<br>
                    Ctrl + O To Open an existing Document<br>
                    Ctrl + S To save a Document<br>
                    Ctrl + W To Close a document</p>

                <h4>Edit Menu</h4>
                <p>Ctrl + Z Undo last action<br>
                    Ctrl + C Copy selected<br>
                    Ctrl + X Cut selected<br>
                    Ctrl + V Paste<br>
                    Ctrl + T Transform. This is the command for transforming in Photoshop. It allows you to: Rotate, Scale, and Change Reference Position.</p>

                <h4>Layers Menu</h4>
                <p>Ctrl + J Duplicate Layers. Making a copy of the original image layer in Photoshop is a good practice to get into. This allows you to revert to your starting point if you change your mind about the edits.</p>
                <p>Ctrl + G Group Layers. This is good for organizing your Layers in Photoshop. You can select multiple Layers and use this shortcut to Group the selected Layers.</p>
                <p>Ctrl + F Merge. Merge/combine selected Layers</p>

                <h4>Select Menu</h4>
                <p>Ctrl + A Select All<br>
                    Ctrl + D Deselect</p>

                <h4>Filters Menu</h4>
                <p>Ctrl + F Last Filter</p>

                <h4>View Menu</h4>
                <p>Ctrl + + Zoom In<br>
                    Ctrl + - Zoom Out<br>
                    Ctrl + 0 Fit on Screen<br>
                    Ctrl + 1 100%</p>

                <h4>Brush Size</h4>
                <p>Many of the tool settings in Photoshop mimic a paintbrush. So essentially you paint on the tool. The settings can be customized by adjusting the brush size or hardness. To increase the size of a Tool cursor, use the (left bracket) [ key; and to decrease the size of the brush use the ] (right bracket) key. The cursor size is increased/decreased in 10 pixel increments. <a href="/photoshop-training-los-angeles.php">More info on Los Angeles Photoshop classes</a>.</p>

                <h4>Toolbar</h4>
                <p>Adobe has made selecting the Tools from the Toolbar super simple. You don't even have to remember a 2- or 3- key combination. All Toolbar shortcuts are one letter. If you select a Tool that is nested with other tools, you can cycle through the tools by repeatedly typing the 1-letter shortcut key. Here are the 1-letter shortcut Keys for the Toolbar:</p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-2">
                    <tbody>
                    <tr>
                        <td width="409"><span>Move tool</span></td>
                        <td width="129">V</td>
                    </tr>
                    <tr>
                        <td><span>Zoom tool</span></td>
                        <td>Z</td>
                    </tr>
                    <tr>
                        <td><span>Hand tool </span></td>
                        <td><span>H or Spacebar</span></td>
                    </tr>
                    <tr>
                        <td><span>Eyedropper tool</span></td>
                        <td><span>I</span></td>
                    </tr>
                    <tr>
                        <td><span>Rectangular Marquee tool / Elliptical Marquee tool </span></td>
                        <td><span> M</span></td>
                    </tr>
                    <tr>
                        <td><span>Lasso tool / Magnetic Lasso tool / Polygonal Lasso tool </span></td>
                        <td><span> L</span></td>
                    </tr>
                    <tr>
                        <td><span>Magic Wand tool / Selection Brush tool / Quick Selection tool </span></td>
                        <td><span>A</span></td>
                    </tr>
                    <tr>
                        <td><span>Horizontal Type tool / Vertical Type tool  </span></td>
                        <td><span>T</span></td>
                    </tr>
                    <tr>
                        <td><span>Crop tool</span></td>
                        <td><span>C</span></td>
                    </tr>
                    <tr>
                        <td><span>Straighten tool </span></td>
                        <td><span>P</span></td>
                    </tr>
                    <tr>
                        <td><span>Red Eye Removal tool</span></td>
                        <td><span>Y</span></td>
                    </tr>
                    <tr>
                        <td><span>Spot Healing Brush tool / Healing Brush tool</span></td>
                        <td><span> J</span></td>
                    </tr>
                    <tr>
                        <td><span>Clone Stamp tool / Pattern Stamp tool </span></td>
                        <td><span>S</span></td>
                    </tr>
                    <tr>
                        <td><span>Eraser tool / Background Eraser tool / Magic Eraser tool</span></td>
                        <td><span>E</span></td>
                    </tr>
                    <tr>
                        <td><span>Pencil tool </span></td>
                        <td><span>N</span></td>
                    </tr>
                    <tr>
                        <td><span>Brush tool </span></td>
                        <td><span>B</span></td>
                    </tr>
                    <tr>
                        <td><span>Paint Bucket tool </span></td>
                        <td><span>K</span></td>
                    </tr>
                    <tr>
                        <td><span>Gradient tool </span></td>
                        <td><span>G</span></td>
                    </tr>
                    <tr>
                        <td><span>Custom Shape tool / Rectangle tool / Rounded Rectangle tool / Ellipse tool / Polygon tool /Star tool / Line tool / Shape Selection tool </span></td>
                        <td><span>U</span></td>
                    </tr>
                    <tr>
                        <td><span>Blur tool / Sharpen tool / Smudge tool </span></td>
                        <td><span>R</span></td>
                    </tr>
                    <tr>
                        <td><p>Sponge tool / Dodge tool / Burn tool </p></td>
                        <td><span> O</span></td>
                    </tr>
                    <tr>
                        <td><span>Show/Hide all panels </span></td>
                        <td><span> TAB</span></td>
                    </tr>
                    <tr>
                        <td><span>Default foreground and background colors </span></td>
                        <td><span>D</span></td>
                    </tr>
                    <tr>
                        <td><span>Switch foreground and background colors </span></td>
                        <td><span>X</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="htt/testimonials.php?course_id=6">Photoshop class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>