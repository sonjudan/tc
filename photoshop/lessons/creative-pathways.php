<?php
$meta_title = "Creative Pathways in Photoshop | Training Connection";
$meta_description = "Learn to use Fill and Stroke Path in Photoshop. This course is part of our Fundamentals Photoshop Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creative Pathways</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creative Pathways in Photoshop</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>


                <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                    <p>Two extremely fun and useful features that many Photoshop users tend to overlook are <strong>Fill and Stroke Path</strong>. Hidden in the depths of the <em>Path Panel menu </em>or accessing the functions through the hollow and solid circle shaped buttons at the bottom of the panel, Fill and Stroke Path offers users both real utility and creative benefits. </p>

                    <p>To join an <a class="photoshop" href="/photoshop-training.php">accredited Photoshop class</a> in Chicago or Los Angeles call us on 888.815.0604</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/000_HEADER_CPFPS.jpg" width="839" height="536" alt="Fill and Stroke Pathways"></p>

                    <p><strong>Fill Path</strong> allows users to fill a path with any pattern, color, gradient, history state or even Content-Aware, making this feature a serious healing utility or a creative frame to shape content. <strong>Stroke Path</strong> gives users the power to stroke dozens of useful toolsets across a vector path including healing tools like the Spot Healing Brush, Healing Brush, Clone Stamp, Smudge, Blur, Dodge, Burn and so many other artistic and reparative tools and functions. Let’s look at where each of these features are located and how to use them. <a href="/photoshop-training-chicago.php">Best photoshop training available in Chicago</a>.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_FILLPATHSAMPLE.jpg" width="839" height="268" alt="Fill Path"></p>

                    <h4>Fill Path</h4>
                    <p>Before you can fill a path, you need to create one. A closed or open path will both work keeping in mind that an open path will only insert fill in its Bezier curves or path angles. Drawing freehand with the Pen Tool is probably the most popular option for the Fill Path feature, however any tool or function that creates a path will work. Using tools such as the Rectangle Tool, Rounded Rectangle Tool, Ellipse Tool, Polygon Tool and the Custom Shape Tool all produce closed paths and are perfect for filling, but you may find that the Fill Path and Stoke functions are grayed out. There is an easy fix for this. After you create paths using the closed paths tools, you will need to go to the top of the Path Panel Menu and <strong>Save Path </strong>to unlock Fill and Stroke Path.</p>

                    <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_FILLPATHMENU.jpg" width="839" height="268" alt="Fill Path Menu"></p>

                    <p>Once your path is finished you can activate Fill Path by going to <strong>Path Panel Menu &gt; Fill Path</strong> or by going to the bottom of the Path Panel and <em>Alt-Clicking (Windows) or Opt-Clicking (Mac)</em> on the filled circular button. Once the Fill Path dialogue window opens, you will be able to choose what you would like to fill your path with and even add Blending Modes and feathered edges. <a href="/photoshop-training-los-angeles.php">Best Photoshop training in Los Angeles</a>.</p>

                    <p>*<em>Note: Clicking on the filled circular button at the bottom of the Path Panel without holding down Alt or Opt will fill path with the default foreground color or will fill based on your previous settings.</em></p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_STROKEPATHMENU.jpg" width="839" height="268" alt="Stroke Path Menu"></p>


                    <h4>Stroke Path</h4>
                    <p>Like Fill Path, Fill Stroke begins with you creating an open or closed path. Once you have finished drawing that path, you can access Stroke Path in the same location as Fill Path. Go to <strong>Path Menu &gt; Stroke Path</strong>. You can also <em>Alt-Click (Windows) or Opt-Click (Mac)</em> the open circle button at the bottom of the Path Panel.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_STROKEPATHSELECTIONS.png" width="368" height="463" alt="Stoke Path Tool Options"></p>

                    <p>Once you open the Stroke Path dialogue window, under Tool, you will have a large amount of choices (20 as of Adobe Photoshop CC 2017) to apply across your path. VIP: To set the properties of the Healing Brush, Burn, Color Replacement or any tool on this list, you need to go to that tool before going to Stroke Path and set the radius, opacity or any other property that requires modification.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_CLONINGSTROKEPATH.jpg" width="750" height="239" alt=""></p>

                    <p>Now that you’ve learned how to access Fill Path and Fill Stroke let me give you some ideas for their use. Since vector paths tend to be used primarily for precision-based artwork or selections, you can use Fill Stroke to color sketches and line-based art. Many graphic artists will also trace elements from photographs and convert them to clip art or other types of graphic art. Fill Stroke could be used to add shading and textures to add depth and bring them to life. Want to remove something from a photograph or piece of art? Try creating a closed path around it and choose Content-Aware in the Fill Path dialogue window. </p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/006_STROKEPATHCREATIVE.jpg" width="839" height="268" alt="Finished artwork"></p>

                    <p>And what about Stroke Path? One might argue there are even more uses for this amazing feature. Imagine cloning across a path to re-create a fence or add a line of missing bricks from the side of a building. Or perhaps you wish to remove a row of bricks. Try using the Eraser Tool on a path to eliminate pixel art with surgical precision. Interested in creating a new logo? Draw a path in any shape or form and use an infinite amount of creative organic and technical brushes to draw across every angle or Bezier curve. Click on the <em>Simulate Pressure</em> toggle in the Stroke Fill dialogue window to taper the beginning and end of your stroke. Create decorative frames for certificates, birthday cards and flyers by creating a rectangular path around the border and using Stroke Fill to add art or even organic elements like wood or metal. </p>

                    <p>The uses for Fill and Stroke Path are endless in Adobe Photoshop. Hopefully you will find these features as indispensable in your workflows as I have. Give them a try and see where your pathways take you.</p>

                    <p><em>Fill Path &amp; Stroke Path: </em><a class="photoshop" href="https://adobe.ly/2kyBrKe">https://adobe.ly/2kyBrKe</a></p>
                </div>

            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Photoshop Lessons</h4>

                    <ul>
                        <li><a href="/photoshop/lessons/image-formats.php">Image File Formats</a></li>
                        <li><a href="/photoshop/lessons/image-formats.php#photoshop-image-resolution">Photoshop Image Size and Resolution</a></li>
                        <li><a href="/photoshop/lessons/image-formats.php#photoshop-image-workflow">Image Production Workflow</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Photoshop. We have trained hundreds of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop student testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>