<?php
$meta_title = "Photoshop File Types and Resolutions | Training Connection";
$meta_description = "Learn the different Photoshop Files formats, Images sizes and Resolution and Workflow Techniques. This course is part of our Fundamentals Photoshop Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Image File Formats</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>File Formats in Photoshop</h1>
                        <p>These  topics are covered  in Module 1 - Getting to know Photoshop, part of our <a href="/photoshop/fundamentals.php">Fundamentals Photoshop class</a>.</p>
                    </div>
                </div>


                <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                    <h4>Photoshop format (PSD)</h4>

                    <p>Photoshop format (PSD) is the default file format and the only format, besides the Large Document Format (PSB), that supports most Photoshop features. Because of the tight integration between Adobe products, other Adobe applications such as Adobe Illustrator, Adobe InDesign, Adobe Premiere, Adobe After Effects, and Adobe GoLive can directly import PSD files and preserve many Photoshop features. When saving a PSD, the preference to maximize file compatibility is the default. This saves a composite version of a layered image in the file so it can be read by other applications, including previous versions of Photoshop. It also maintains the appearance of blended layers in the future. In Photoshop, 16-bits-per-channel and high dynamic range 32-bits-per-channel images can be saved as PSD files.</p>

                    <p>For instructor-led <a href="/photoshop-training.php">Photoshop training classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                    <h4>JPEG format</h4>

                    <p>Joint Photographic Experts Group (JPEG) format is commonly used to display photographs and other continuous-tone images on the Web and other online services. JPEG format supports CMYK, RGB, and Grayscale color modes. It does not support transparency, alpha channels or layers. Unlike GIF format, JPEG retains all color information in an RGB image but compresses file size by selectively discarding data. A JPEG image is automatically decompressed when opened. A higher level of compression results in lower image quality, and a lower level of compression results in better image quality. In most cases, the Maximum quality option produces a result indistinguishable from the original.</p>

                    <p><strong>It is a good idea to avoid saving JPEG images over themselves because they will lose quality over time. Save your JPEGs as a PSD to preserve their quality.</strong>  </p>

                    <h4>TIFF format</h4>

                    <p>Tagged-Image File Format (TIFF, TIF) is used to exchange files between applications and computer platforms.  TIFF is a flexible bitmap image format supported by virtually all paint, image-editing, and page-layout applications. Also, virtually all desktop scanners can produce TIFF images. TIFF documents have a maximum file size of 4 GB. Photoshop supports large documents saved in TIFF format. However, most other applications and older versions of Photoshop do not support documents with file sizes greater than 2 GB. TIFF format supports CMYK, RGB, Lab, Indexed Color, and Grayscale images with alpha channels and Bitmap mode images without alpha channels. <a href="/photoshop-training-chicago.php">Adobe certified classes in Chicago</a>.</p>

                    <p><strong>Photoshop can save transparency and layers in a TIFF file</strong>, however, if you open the file in another application, only the flattened image is visible. In Photoshop, TIFF image files have a bit depth of 8, 16, or 32 bits per channel. You can save high dynamic range images as 32-bits-per-channel TIFF files.</p>


                    <h4>
                        About file compression</h4>
                    <p>Many file formats use compression to reduce the file size of bitmap images. <strong>Lossless techniques</strong> compress the file without removing image detail or color information; lossy techniques remove detail. The following are commonly used compression techniques:</p>
                    <p>RLE (Run Length Encoding) <strong>Lossless</strong> compression; supported by some common Windows file formats. <a href="/photoshop-training-los-angeles.php">Small Photoshop classes in Los Angeles</a>.</p>
                    <p>LZW (Lemple-Zif-Welch) <strong>Lossless</strong> compression; supported by TIFF, PDF, GIF, and PostScript language file formats. <strong>Most useful for images with large areas of single color</strong>.</p>
                    <p>JPEG (Joint Photographic Experts Group) <strong>Lossy</strong> compression; supported by JPEG, TIFF, PDF, and PostScript language file formats. Recommended for continuous-tone images, such as photographs. JPEG uses lossy compression. To specify image quality, choose an option from the Quality menu, drag the Quality pop‑up slider, or enter a value between 0 and 12 (Photoshop). For the best printed results, choose maximum-quality compression.</p>
                    <p>CCITT A family of <strong>lossless</strong> compression techniques for <strong>black-and-white images</strong>; supported by the PDF and PostScript
                        language file formats.</p>
                    <p>ZIP <strong>Lossless</strong> compression; supported by PDF and TIFF file formats. Like LZW, ZIP compression is most effective for images that contain <strong>large areas of single color</strong>.</p>
                    <p>You can also have images saved as a Photoshop PSD or TIF file as <strong>uncompressed</strong> to avoid any reduction of quality.</p>

                    <h4>Image Size &amp; Resolution</h4>

                    <p>The pixel dimensions (image size or height and width) of a bitmap image is a measure of the number of pixels along an image's width and height. Resolution is the fineness of detail in a bitmap image and is measured in pixels per inch (ppi). The more pixels per inch, the greater the resolution. Generally, an image with a higher resolution produces a better printed image quality.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-14.jpg" width="750" height="475" alt="Image size and resolution"></p>

                    <p>Photoshop now does a good job interpreting how the image should best be resized and defaults to <strong>Automatic</strong> interpolation when resizing an image.</p>

                    <p>To check if the quality of an image is maintained, always look at the image at <strong>100% magnification</strong>, to anticipate how the image will look printed as well as on-line. Choose <strong>View &gt; 100%</strong> (Cmd/Ctrl + 1).</p>

                    <p><strong>Basic PPI Resolution for various output:</strong></p>

                    <ul>
                        <li>72 PPI - Web graphics</li>
                        <li>150-200 PPI - Photo Inkjet and Laser Printers</li>
                        <li> 300 PPI - Photo Lab prints, High Quality Art Prints &amp; Prepress (Offset) Professional Printing</li>
                    </ul>

                    <p> These resolutions are of a file at 100% of the final output size.</p>

                    <p>When scanning printed photos or taking pictures, always capture your images at a maxium quality and high resolution, sometimes more than you need.</p>

                    <p>Use the RAW format with digital cameras not JPEG, so that maximum information is retained within the file</p>

                    <h4>Image Production Workflow</h4>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/photoshop-15.jpg" width="750" height="638" alt="Photoshop CC 2014 Preferences"></p>

                    <p>If you shoot RAW, you will always have your original file preserved, since RAW is comparable to having a negative, from which you can make as many different prints as you want. If you need to, the RAW file can always have any changes made to it removed, so it returns to the "as shot" state. For permanent archiving, Digital Negative (DNG) is a file format that contains the raw image data from a digital camera and metadata that defines what the data means. DNG, Adobe's publicly available, archival format for camera raw files, is designed to provide compatibility and decrease the current proliferation of camera raw file formats. The Camera Raw plug-in can save camera raw image data in the DNG format.</p>
                    <p>If you shoot JPEG, then save your file as a PSD file, to avoid file compression. PSD files also preserve Photoshop's layers or transparency in your file.</p> <p>
                        Generally speaking in addition to your original image (RAW or JPEG), you should have a PSD (Photoshop) file
                        as your working "master". Once it is finished in terms of being ready for output, you should keep this original and choose <strong>File &gt; Save As</strong> to create a flattened (smaller) TIF or a JPEG of the final file for print.</p>
                    <p>If you are saving images for on-line presentation or the internet, use <strong>File &gt; Save For Web</strong> to preview the quality of the image as you resize the image and compare JPEG, GIF &amp; PNG file formats at different compression settings.</p>

                </div>

            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Photoshop Lessons</h4>

                    <ul>
                        <li><a href="/photoshop/lessons/photoshop-interface.php">Photoshop Interface</a></li>
                        <li><a href="/photoshop/lessons/photoshop-interface.php#photoshop-preferences">Photoshop CC Preferences</a></li>
                        <li><a href="/photoshop/lessons/photoshop-interface.php#photoshop-workspaces">Photoshop CC Workspaces</a></li>
                        <li><a href="/photoshop/lessons/photoshop-interface.php#photoshop-shortcuts">Custom Keyboard Shortcuts &amp; Preferences</a></li>
                        <li><a href="/photoshop/lessons/photoshop-interface.php#photoshop-menus">Photoshop Menu Customization</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop reviews</h4>
                    <p>Learn Adobe Photoshop from the Pros. We have  hundreds of satisfied students. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Adobe Photoshop student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>