<?php
$meta_title = "More Photoshop Content Aware Tools | Training Connection";
$meta_description = "Learn how to use Content Aware Move and Patch tool in Adobe Photoshop. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Content Aware Tools</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Content Aware Tools in Photoshop Part 2</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article follows on from <a href="/photoshop/lessons/content-aware-tools-part-a.php">Content Aware Tools in Photoshop Part 1</a>.</p>

                <h4>Content Aware Move</h4>

                <p>The Content Aware Move Tool allows you move part of an image and not have to worry about filling in the space behind the moved object. Once again Photoshop approximates the pixels that should fill in the new vacant space in order to keep the photo intact.</p>

                <p>Why not join one of our <a href="/photoshop-training.php">Photoshop classes</a> in Chicago or Los Angeles? We are going to move the barrel can in this photo to the right side. But what will replace the area behind the barrel can once we move it? Let's watch Photoshop do its magic.</p>

                <ol>
                    <li>Select the Content Aware Move Tool from the Toolbar.</li>
                    <li> Outline and select the barrel and its drop shadow. It doesn't have to be a perfect selection.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_content_aware_move_tool.jpg" width="780" height="60" alt="Content Aware Move Tool"></p>

                <ol start="3">
                    <li>Now you need to adjust the Structure and Color options in the Options Bar. The Structure settings determine the integrity of the image you selected. Higher numbers ensure the accuracy of the object you move. The Color scale determines how the image you move will blend with the new background you move it to. For this exercise my Structure was 4 and Color was 9.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_structure_and_Color_options.jpg" width="780" height="91" alt="Structure and Color options in the Options Bar"></p>

                <ol start="4">
                    <li>Make sure 'Transform on Drop' checkbox is not selected.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_transform_on_drop.jpg" width="575" height="84" alt="Transform on Drop checkbox"></p>

                <ol start="5">
                    <li>Hold down the Content Aware Move cursor and drag the barrel to the right side of the image.</li>
                </ol>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_content_aware_move_cursor.jpg" width="792" height="283" alt="Content Aware Move cursor "></h4>

                <p>Photoshop will automatically replace the background behind the sign by approximating and duplicating the landscape and sand from the photo.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_finished_project.jpg" width="792" height="282" alt="Completed project"></p>

                <ol start="5">
                    <li>Deselect the barrel can.</li>
                </ol>
                <p><a href="/photoshop-training-chicago.php">Looking for a great Photoshop class in Chicago?</a></p>

                <h4>Patch Tool</h4>

                <p>The Patch Tool operates very similarly to the Clone Stamp Tool, by allowing you to copy one part of the photo to another part of the image.</p>

                <ol>
                    <li>You start by selecting the Patch Tool from the Toolbar.
                    </li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/006_patch_tool.jpg" width="792" height="116" alt="Patch tool"></p>

                <ol start="2">
                    <li>Draw a selection with the Patch Tool around the part of the image you want to replace.</li>
                </ol>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/007_make_selection.jpg" width="792" height="132" alt="Select items to remove"></h4>

                <ol start="3">
                    <li>Hold down the cursor on the selection and drag it to the area of the image you want to copy. We want to replace the birds we selected with concrete from the sidewalk. Too many birds.</li>
                </ol>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/008_remove_selection.jpg" width="720" height="370" alt="Delete selections"></h4>

                <p>The original selection of the birds will be replaced by the pixels from the sidewalk, shadows and sunlight from the area you copied. You need to adjust the Structure and Color settings to ensure your copy blends well.</p>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/009_finished_project.jpg" width="792" height="403" alt="Structure and Color settings"></h4>

                <p>One of the keys to using the Content Aware Tools is using images or parts of images that are simple and easy to duplicate. These magic tools don't work well on every photo. A photo with a simple background will give you better results. <a href="/photoshop-training-los-angeles.php">Certified Adobe training</a> in Los Angeles.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop student reviews</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>