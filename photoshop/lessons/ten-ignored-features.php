<?php
//$meta_title = "10 Useful overlooked Photoshop features | Training Connection";
//$meta_description = "Learn these 10 really useful but often ignored features in Photoshop. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Overlooked Features</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>10 Typically Ignored But Useful Features In Photoshop</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>

            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>Many of us working with Adobe applications tend to focus on the features and functionalities that we need for our occupation or personal projects. Unfortunately, there are so many great features that get overlooked or just simply ignored. The following is a list of useful features that so many Photoshop users typically ignore. Let us begin!</p><p>To join one of our <a href="/photoshop-training.php">Adobe Certified  Photoshop classes</a> in Chicago or Los Angeles call us on 888.815.0604</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/000_Header.jpg" width="839" height="536" alt="10 Ignored but useful Photoshop features"></p>

                <h4>Ignored Feature #01 Duplicating Smart Objects</h4>

                <p>Duplicating Smart Objects seems so easy and straight-forward. Hit the classic shortcut, Command or Control + J to duplicate any layer and you’re done. The problem with this method is that the original files inside  the Smart Object are now shared among the duplicate layers. This means that if you step into your Smart Object to change anything, it changes it for <u>ALL</u> duplicated layers. To truly duplicate a Smart Object and create an independent layer not linked to its origin, <strong>Right-Click any Smart Object Layer</strong> and choose <strong>New Smart Object via Copy</strong>. <a href="/photoshop-training-chicago.php">Photoshop graphic design classes in Chicago</a>.</p>

                <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_CreateLayer.jpg" width="839" height="268" alt="Create Layers in Photoshop"></p>

                <h4>Ignored Feature #02 Create Layers (Layer Styles to Pixels)</h4>
                <p>If you have ever used Layer Styles in Photoshop you know that they can add non-destructive effects like Drop Shadows, Bevels, Glows and a lot more to any of your layers. The bad news is that editing these effects are extremely limited in their non-destructive form. One way of making these effects more editable is to <u>rasterize the effects</u> on their own layers so they can be manipulated with tools like the Eraser Tool or Layer Masks, which allow variable control over layer visibility. To perform this, you need to <strong>Right-Click on the FX symbol on any layer</strong> (or go to <strong>Layer &gt; Layer Styles</strong>) and select <strong>Create Layers</strong>. Create Layers will “break” your Layer Styles up into their own rasterized layers, separating top effects like Bevels and Inner Glows to layers above your origin layer while separating bottom effects Drop Shadows to layers below your origin layer.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_MatchFont.jpg" width="839" height="268" alt="Match Font feature"></p>

                <h4>Ignored Feature #03 Match Font</h4>
                <p>Match Font was released in Photoshop version 2015.5 and was created to aid users in identifying fonts from layers. When using this feature, Photoshop will scan a JPEG or other file and then search across Type Kit and other sources to identify your font. If the font is in Type Kit, it will allow you to install and sync those fonts for immediate use. To use Match Font, select any layer and then go to <strong>TYPE &gt; MATCH FONT</strong>. Then adjust the “crop” handles to define the area you want Photoshop to focus on. <a href="/photoshop-training-los-angeles.php">Photoshop training in Downtown Los Angeles</a>.</p>

                <h4>Ignored Feature #04 Find &amp; Replace Text</h4>
                <p>Have you ever wanted to replace a word with another word? Find &amp; Replace Text in Photoshop will allow you to do just that. If you go to <strong>Edit &gt; Find and Replace Text </strong>you will simply be able to search and                replace text at the same time.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_StraighteningMethods.jpg" width="839" height="268" alt="Straighten Documents Photoshop feature"></p>

                <h4>Ignored Feature #05 Straightening Documents</h4>

                <p>If you are unlucky enough to have a photograph or graphic that is crooked, you should know that the manual way to fix this is to use <strong>Command or Control + T</strong> to activate Free Transform and then hover outside of any of the transform points to get the rotate symbol. &nbsp;Then you Left-Click and drag to rotate your image into place. Another way to straighten your document is to click on your Crop Tool and make sure your <strong>Straighten By Drawing Line</strong> button in the Options Panel is on. Then, draw a line across your document to tell Photoshop the angle that needs to be straightened. Voila! You’re done. An alternate to this is to draw any line with the Ruler Tool to tell Photoshop the angle that needs to be straightened and then go to <strong>Image &gt; Image Rotation &gt; Arbitrary. </strong>The number in the input box is the number Photoshop has determined will correct your crooked document. Just hit the OK button and you are home free!</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_LayerOptionsPanel.jpg" width="839" height="300" alt="Layer Panel Options"></p>

                <h4>Ignored Feature #06 Layers Panel Options</h4>
                <p>A lot of overlooked, useful functionality exists in the panel menus throughout Photoshop. The Panel Options in the Layers panel, for example, allow you to change the size of your layer thumbnails, adjust the way your layers fit around your layer thumbnails and even allows you to adjust specific naming functions for copied layers. FYI, you can also right click on any layer’s thumbnail to choose the thumbnail size.</p>

                <h4>Ignored Feature #07 Isolate Layers</h4>

                <p>If you have ever worked on a Photoshop document with a lot of layers, you know that it can be really challenging scrolling up and down continuously to find the specific layers you want. In comes Isolate Layers. Isolate Layers allows you to select any number of layers and isolate them by hiding all the non-selected layers. If you understand the concept of “soloing” a layer in music, its works the exact same way. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_GenerateImageAssets.jpg" width="839" height="268" alt="Generate Image Assets"></p>

                <h4>Ignored Feature #08 Generate Image Assets</h4>

                <p>Generate Image Assets is one of those features in Photoshop that most curious users will experiment with to find out that it does “nothing.” The truth is that it does a lot - but it requires you to do a little prep beforehand. Generate Image Assets primary task is to output your layers as individual image formats like JPEG, PNG or GIF. What makes this method unique is that you need to label your layers in a specific way so that Photoshop knows what format, size and or quality to output.</p>

                <h6>For example, this is how you would potentially label a layer to output as a JPEG:</h6>

                <p><strong>Photograph01.jpg50%</strong> <em>(can define quality from 1% to 100%)</em>
                    <strong>Photograph01.jpg5</strong> <em>(can define quality from 1 to 10)</em>
                    <strong>300px x 300px Photograph01.jpg75%</strong> <em>(size can be defined in any type of units: in, cm, etc)</em>
                    <strong>200% Photograph01.jpg75%</strong> <em>(a percentage as a prefix will define size, suffix)</em></p>

                <h6>PNG &amp; GIF Outputs can be labeled as:</h6>

                <p><strong>Photograph01.png24</strong> <em>(24 for a PNG 24)</em>
                    <strong>175% Photograph01.gif</strong> <em>(this will output a GIF at 175% size)</em>
                    <strong>Photograph01.png8</strong> <em>(8 for a PNG 8)</em>
                    <strong>50% Photograph01.png24</strong> <em>(this will output a PNG-24 at 50% size)</em></p>
                <p>Once you’ve labeled your layers, then you go to <strong>File &gt; Generate &gt; Image Assets</strong>. This will automatically output your images. If you have saved your Photoshop project file (PSD), files will be save alongside of your project. If you haven’t saved your project, files will be saved in a new folder on your desktop.</p>

                <h4>Ignored Feature #09 Copy Merged</h4>
                <p>One great feature in Photoshop is the ability to perform a Copy Merged command. If you select multiple layers and then go to <strong>Edit &gt; Copy Merged</strong> you can then paste the merged layers into any document as a single merged layer. Note: This feature can be a bit temperamental. Make sure that you are in RGB mode and you have layers selected. Beyond this, some individuals and companies have reported that this function can be grayed out regardless.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/006_TempZoom.jpg" width="839" height="300" alt="Photoshop Temp Zoom feature"></p>

                <h4>Ignored Feature #10 Temp Zoom</h4>
                <p>Temp Zoom allows you to quickly and temporarily zoom out to see your entire document with a guide that shows you what location you are in. To use this function, zoom in to any part of your document and then hold down the ‘H’ key. If you want, you can <strong>Left-Click and drag</strong> the white box around to zoom in to a different location.</p>
                <p>Well, that’s my top ten ignored list!&nbsp; Explore Photoshop for yourself. You will likely find things you’ve overlooked or never seen before.</p>
            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on face-to-face training is the most effective way to learn Adobe Photoshop. We have trained hundreds of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop past student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>