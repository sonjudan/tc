<?php
$meta_title = "Photoshop Image Formats | Training Connection";
$meta_description = "Learn the differences between all the file formats in Photoshop. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Image formats</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Saving & Image Formats in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>You might think "I know how to Save documents." True. You are probably an expert at saving in Microsoft Office and other popular office software. But saving in Photoshop requires knowledge of image formats, the final destination of your image, a little math and more.</p>

                <p>To join one of our <a href="/photoshop-training.php">Photoshop training classes</a> in Chicago or Los Angeles call us on 888.815.0604</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_Photoshop_Saving.jpg" width="792" height="235" alt="File formats in Photoshop"></p>

                <p>So the Save command in Photoshop works the same as any other software program. When you first Save an Image, you will be taken to the Save As dialog box where you give your document a name and specify where to save it. The tricky part is knowing which image format to save it to in Photoshop. There are many formats to choose from. Lets do a quick review of the most common image file formats and which ones are good for how the image will be viewed whether web or print.</p>
                <p><a href="/photoshop-training-los-angeles.php">Adobe certified Photoshop classes in LA</a>.</p>

                <h4>JPG</h4>
                <p>This format is best used for images that will be viewed on screen or the internet. This is a lossy, compression based format which means when Photoshop saves it as a JPG you will lose some of the pixel information and image quality in the compression process. JPGs normally don't have the quality for print media. There are exceptions. You can read about them at Resizing Images. JPG formats also flattens the images, so you can no longer edit individual layers, effects or adjustments. Therefore this file size will be small which keeps its load time on a website to seconds. This file format can be imported easily into Microsoft Office documents. JPGS are usually 72 dpi.</p>

                <h4>TIFF</h4>
                <p>TIFF files can be compressed but are ideal for print projects that require high-resolution (300 dpi) images with pixel integrity. TIFF files contain editable layers and effects. It is not used as often.</p>

                <h4>PSD</h4>
                This is a native Photoshop image file. It will contain all of the layers and editable aspects of Photoshop. This file size is normally huge. So it is not ideal for the internet where you need your image to load quickly. PSD files don't work well with programs outside of the Adobe family. So if you want to use the image in PowerPoint you would need to save it as a JPG or PDF. PSD files are good for editing images and maintaining the highest pixel integrity at 300 dpi. You will very seldom want to share your PSD file with someone outside of your organization unless it is another designer or the printer.

                <h4>PDF</h4>
                <p>This is Portable Document Format (PDF) which allows you to share the file and for others to view and open it without having Photoshop software. PDFs can be easily opened and viewed online or by downloading the free Adobe Reader app. This is also a compression format that flattens the image into one layer. You can't edit layers and effects in PDFs. This image format can be saved as a small, low-resolution file at 72 dpi or a high-resolution file that is printer quality at 300 dpi.</p>

                <h4>PNG</h4>

                <p>This is a Portable Network Graphic (PNG) file. This is a web file format. PNG files allow you to save transparency. So if you don't want the white background that you normally get with a JPG, save it as a PNG. This is a compression format that is ideal for small sizes and quick loading on websites. You can import PNG in most Microsoft Office documents.</p>

                <h4>GIF</h4>

                <p>A GIF is another web-based file format so it is ideal for small file sizes. GIFs are for animated files. You know all of those dancing mortgage people we saw in the sidebar of our webpages in the mid-2000s? Those were animated GIFs. A gif is like a very short video.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_image_jpg_white_background.jpg" width="576" height="189" alt="JPG with a White Background"></p>
                <p class="wp-caption-text text-left">JPG with a White Background. <a href="/photoshop-training-chicago.php">Small Photoshop classes in Chicago</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_image_jpg_transparent.jpg" width="576" height="216" alt="JPG with a Transparent Background."></p>
                <p class="wp-caption-text text-left">JPG with a Transparent Background.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_image_psd_format.jpg" width="576" height="177" alt="PSD format with special editable layers">
                <p class="wp-caption-text text-left">PSD format with special editable layers and Layer Styles: Outer Glow , Color Overlay and Stroke.</p>
            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousandss of students how to use Photoshop like pros. Click on the following link to view a sample of our <a class="photoshop" /testimonials.php?course_id=6">Photoshop training testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>