<?php
$meta_title = "Photoshop Content Aware Tools | Training Connection";
$meta_description = "Learn how to use Content Aware Scale, Fill and Crop commands in Adobe Photoshop. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Content Aware Tools</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Content Aware Tools in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>The Content Aware Tools in Photoshop have been likened to pixel magic. What took hours to correct in the early versions of Photoshop now only take seconds with the Content Aware Tools. These tools allow the designer/compositor to replace areas of an image with content from other parts of the image. Don't stress about how it's done. Sit back and relax and watch Photoshop do the work for you.</p>

                <p>Why not join one of our <a href="/photoshop-training.php">hands-on  Photoshop classes</a> in Chicago or Los Angeles?</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_ContentAwareTools.jpg" width="780" height="232" alt="Content-aware Tools"></p>

                <p>Let's start with Content Aware Scale.</p>

                <p>This is a first cousin to the process of resizing an image. So you have an image that is 7x5 and you need it to be 11 x 8.5 and bleed to the edges. The Content Aware Scale allows you to enlarge your image without compromising pixel quality. It focuses on a  part of the image and scales only that focal point while leaving the rest of the image the same. You ask how do you maintain pixel integrity? Content Aware Scale creates new pixels. Just like that - Magic! Looking for the <a href="/photoshop-training-chicago.php">best Photoshop training in Chicago</a>?</p>

                <h4>Content Aware Scale</h4>

                <ol>
                    <li>We start by selecting Canvas Size under the Image Menu. Increase the Canvas Size 135% for width and height. The new blank canvas space will appear on the top right corner.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_Content_Aware_Scale.jpg" width="792" height="199" alt="Image Menu > Canvas Size"></p>

                <ol start="2">
                    <li>Next select the Content Aware Scale command from the Edit Menu.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_content-aware_scale_command.jpg" width="792" height="501" alt=""></p>

                <ol start="3">
                    <li>Hold the cursor on the corner handles while holding down the Shift Key and scale up your image</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_cursor+shift.jpg" width="428" height="288" alt=""> </p>

                <ol start="4">
                    <li>You can see from our example that the Content Aware Scale freezes the landscape portion of the image while scaling the clouds. (The original image is on top of the Scaled version.)</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_cloud_scaling.jpg" width="792" height="488" alt=""></p>

                <h4>Content Aware Fill</h4>

                <p>Remember the good ole' days of Cloning to fill in a gap in an image or increase its size. The Clone Stamp is still a great stand-by and touch-up tool but the Content Aware Fill command can shave hours off of a composition project. Once again, the magic of Content Aware reads the area surrounding the area you want to repair or fill and intuitively (well really mathematically) fills the area with the correct pixels from your image.</p>
                <p>Let's see it at work. <a href="/photoshop-training-los-angeles.php">Click here</a> for details about the best Photoshop training in Los Angeles.</p>
                <p>In this image we have a sign on a brick wall. Since the sign takes up most of the image, it's hard to find enough space for the copy we want to use. So, we are going to increase the area of the brick wall.</p>

                <ol>
                    <li>First let's increase the Canvas Size 140% on the right side.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_increase_canvass.jpg" width="792" height="387" alt=""></p>

                <ol start="2">
                    <li>Now, select the new blank area with the Rectangular Marquee Tool. Be sure to include some overlap on the brick wall.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_Rectangular_Marquee_Tool.jpg" width="792" height="210" alt=""></p>

                <ol start="3">
                    <li>Select Fill from the Edit Menu.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/006_Edit_menu_fill.jpg" width="792" height="172" alt=""></p>

                <ol start="4">
                    <li>Choose Content Aware from the drop down instead of a solid color. Click the Color Adaptation box.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/007_Color_Adaptation_box.jpg" width="792" height="262" alt=""></p>

                <ol start="5">
                    <li>Click Ok.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/008_content_aware_fill_command_magic_wand.jpg" width="792" height="206" alt=""></p>

                <p>Just a quick wave of the Content Aware Fill command Magic wand and that area is filled with brand new pixels and the brick wall has been extended. </p>

                <h4>Content Aware Crop</h4>

                <p>Often times when you crop an image by a specific dimension you will want have extra blank space on the sides or top/bottom of your image. This is normally a by-product when you straighten a photo. There is an option for you to use Content Aware when you Crop. When Content Aware is selected, Photoshop will fill in the new empty blank areas of the image with pixels from the surrounding area.</p>
                <p>Let's take it for a test drive.</p>

                <ol>
                    <li> Select the Crop Tool</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/009_crop_tool.jpg" width="792" height="224" alt=""></p>

                <ol start="2">
                    <li>Set the ratio to 1:1 square for your Crop parameter. Drag the top left crop-handle up 2 inches.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/010_crop_parameter.jpg" width="634" height="445" alt=""></p>

                <p>You will now see blank canvas on the left-hand and top of the image.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/011_blank_canvas.jpg" width="792" height="450" alt=""></p>

                <ol start="3">
                    <li>Make sure you have Content Aware checked in the Options Bar. When you have the Crop settings the way you want them, click the checkmark at the top of the Options Bar.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/012_options_bar.jpg" width="792" height="49" alt=""></p>

                <ol start="4">
                    <li>Now Photoshop will not only Crop your image but it will fill in the blank canvas area by duplicating pixels from the left side of the image. Your cropped image is a perfect square.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/013_completed_bar.jpg" width="554" height="455" alt=""></p>

                <p><a href="/photoshop/lessons/content-aware-tools-part-b.php">Next - Content Aware Move and Patch Tools</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousandss of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop student class reviews</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>