<?php
$meta_title = "Saving Files in Photoshop | Training Connection";
$meta_description = "Learn the best practices for savings files in Adobe Photoshop CC. Photoshop training available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Saving Files</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Best Practices for Saving Files in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>So you have a JPG image that you selected from a stock image site or a client has sent you via email. You need to make edits to the image for the project you are working on but you don't want to lose pixel quality. The first thing you should do is Open the JPG in Photoshop and Save As a PSD file. It is always best to work on an image in PSD format.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/Photoshop_Saving.jpg" width="792" height="235" alt="Image formats in Photoshop"></p>

                <p>Need to learn Photoshop - join one of our <a href="/photoshop-training.php">Adobe CInstructor-led Photoshop classes</a> in Chicago or Los Angeles?</p>

                <p>Have you ever noticed that the JPG image you are working from seems to look more and more pixelated the more you edit it. This happens because every time you resave in the JPG format, Photoshop compresses the image by randomly deleting pixels. So every time you resave your JPG as a JPG it diminishes the pixel quality.</p>

                <p>So once you Open and Save your image in PSD format and have made your edits you may need to save the image as a JPG again. Why would you work this way you might ask? It is always simple to save to a lower resolution or compressed image from a high-resolution image. But you can never make a low-resolution image high resolution. So in summary here are the steps for working with and saving JPG's in Photoshop.</p>

                <p>Step 1 &gt; Save As a PSD file. </p>

                <p>Step 2 &gt; Make image Edits. </p>

                <p>Step 3 &gt; Save your PSD file. </p>

                <p>Step 4 &gt; Save As a JPG if you also need to use the image on-screen or online. </p>
                <p>If you need to make further edits, REPEAT THE STEPS ABOVE. <a href="/photoshop-training-chicago.php">Photoshop training sessions in Chicago</a>.</p>

                <h4>Saving Images for the Web</h4>
                <p>You have probably have noticed that when you Save an image as a JPG from the Save As menu, it doesn't necessarily compress the image file size. To save the image as a lower-resolution you need to Export it. This command is under the File Menu.</p>

                <p>Let's Save a JPG for the Web at 72 dpi that can load in under 30 seconds.</p>

                <ol>
                    <li> Select Export from the File Menu.</li>
                    <li> Choose Save for Web Legacy from the drop-down menu.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/1-SaveforWeb-Photoshop.jpg" width="546" height="382" alt="Saving images for Web"></p>

                <p>The Save for Web dialog box displays. <a href="/photoshop-training-los-angeles.php">Hands-on Photoshop classes in LA</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/2-SaveforWeb-Interface-Photoshop.jpg" width="702" height="412" alt="Saving for Web Dialog box"> </p>

                <p>You can Preview your image on the left-side of the window. The Preview shows you how the image will look with the settings that you choose on the right-hand side. You can see from the dialog box that you have many setting options to define your image format. You can even convert your Color Mode to RGB which is the required setting to view images onscreen.</p>
                <p>On the right-hand side at the top, you have a drop-down list with twelve image format options to choose from:</p>
                <p><strong>JPG: </strong>High, Medium or Low-Quality</p>
                <p><strong> GIF</strong>: 7 options/dithered</p>
                <p><strong> PNG</strong>: 24- and 8-bit color</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/3-FileFormatOptions-JPGS-GIFS.jpg" width="780" height="446" alt="Twelve preset image formats"></p>

                <ol start="3">
                    <li>We are going to focus on only a few of the options for this exercise. Select these settings.</li>
                </ol>

                <p>JPG-High</p>
                <p> Convert to sRGB</p>
                <p>Your Preview should be 100%.</p>
                <p>Underneath the Preview are the details of your Image Size and website load speed at the proposed internet speed. The image is 42.7K with 9 seconds to load at 56.6 mps.</p>

                <ol start="4">
                    <li> Click the Save Button.</li>
                </ol>

                <p>The Save Optimized As Dialog Box Opens. (Here you can enter a name and choose a location to save your file.) Go with the Default settings for: Format, Settings and Slices.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/4-SaveforWeb-DialogBox-Photoshop.jpg" width="546" height="601" alt="Save Optimized as Dialog Box"></h4>

                <p>Let's compare this image to the JPG created through the Save As command. The Save As JPG dimensions were 4.15MB at 300 dpi. This is 10x the size of the JPG that we Export through the Save for Web process.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/5-ImageSize-Photoshop.jpg" width="677" height="331" alt="Set Image size"></p>

                <p>So saving an image as a JPG will compress the file but it will not necessarily have the proper dimensions for the web. To ensure that your image can be viewed on the Web, be sure to use the Save for Web feature.</p>

                <h4>Photoshop Student Testimonials</h4>

                <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop class reviews</a>. </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop class reviews</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>