<?php
$meta_title = "Anaglyph 3D in Photoshop | Training Connection";
$meta_description = "Creating Anaglyph 3D in Adobe Photoshop. We deliver certified Photoshop Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Anaglyph 3D</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating Anaglyph 3D in Adobe Photoshop</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>One of the oldest and simplest methods of creating 3D imagery is a technique known as Anaglyph 3D. Monster and alien movies from the 50's, 60's and even 70's used this type of 3D as a gimmick to bring in audiences and make them feel more like they were a part of the film. As the years have passed and technology progressed and transformed, anaglyph 3D remained a novelty and has maintained  popularity with movies, series and social media powerhouses like Youtube.</p>

                <p>Join one of our <a href="/photoshop-training.php">Adobe Photoshop classes</a> offered in Chicago or Los Angeles - call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/000_Header_3D.jpg" width="839" height="536" alt="Anaglyph 3D in Photoshop"></p>

                <h4>What is Anaglyph 3D and How Does It Work?</h4>

                <p>Anaglyph 3D is a stereoscopic technique that achieves the 3D look by allowing you to see left and right views of the same image. This image, when delivered to each eye, gets perceived as depth. So how does Anaglyph 3D work?<img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_3DGuy.jpg" width="248" height="254" alt="3D Man"></p>
                <p>The system uses a two-color system which helps separate the image delivery to each eye, aided by a pair of glasses using those same colors to filter the images. Typically, the colors used are red and cyan, but other colors have been utilized to create this effect. <a href="/photoshop-training-chicago.php">Beginner Photoshop training in Chicago</a>.</p>
                <p> According to the Tristimulus Theory, the human eye is very sensitive to the primary colors red, green and blue. In the case of Anaglyph 3D, this sensitivity allows the red filter to block out the cyan in one eye and then allows the cyan filter to block out the red, effectively giving you a self-contained stereoscopic image. Confused? No problem! Either way you look at it, here is an easy way to convert your Photoshop images and art into 3D wow projects, assuming you have a pair of 3D glasses!</p>

                <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_STEP01_DUPLICATE.jpg" width="839" height="536" alt="Red and Cyan in layers panel"></p>

                <h4>Creating Your First Anaglyph 3D Art In Photoshop</h4>

                <p>In this exercise, I've decided to use this disturbing stock picture in the spirit of Halloween. If you would like to use this image it is available on the Adobe Stock website (ID#63304760). Otherwise, choose any photograph or image and the results should be the same. Once you have the photo - open it in Photoshop by going to FILE &gt; OPEN. To get the red and cyan from your image to create 3D, do the following:</p>

                <p><img class="imageright" src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_CONVERTING_CHANNELS.jpg" width="307" height="286" alt="Blending options"></p>

                <ol>
                    <li>DUPLICATE your image in the Layers Panel. This should give you two (2) layers total.</li>
                    <li> Rename each layer from the bottom up, Red and Cyan as presented in the photo above.</li>
                </ol>

                <p>Now we need to convert our red and cyan layers into usable color information that will produce our anaglyph 3D effect. To do this, we need to manipulate the Color Channels of each individual layer. If your initial thought was to run over to your Channels Panel this would be incorrect. </p>
                <p><a href="/photoshop-training-los-angeles.php">Advanced Photoshop classes in Los Angele</a>s.</p>
                <p>The Channels in your Channels Panel control the global color channels of the entire project. To manipulate the channels of each individual image layer, do the following:</p>

                <ol start="3">
                    <li>Select the RED Layer and then go to the bottom of your Layers Panel and click on FX. This will present you with a menu. At the top of this menu, select BLENDING OPTIONS. This will activate the general menu panel for Layer Styles. You will see that in the center of the panel,
                        there are three Channel checkboxes for Red, Green and Blue.</li>
                    <li> Since we are on the RED Layer, deselect the Green and Blue checkboxes. Then press OK. You have now told the RED Layer that we only wanted to see its Red Channel.</li>
                    <li> Now, select the CYAN Layer and repeat Step 3 (click on FX at the bottom of your Layers Panel and choose BLENDING OPTIONS).</li>
                    <li> Since we are on the CYAN Layer and CYAN is a combination of Green and Blue, deselect the Red Channel and leave the Green and Blue checkboxes checked. Then press OK. Now that we have our color channels properly separated,  the fun begins.</li>
                    <li> Select the CYAN Layer and then use your RIGHT ARROW on your keyboard to nudge your CYAN Layer to the Right about 25 - 40px. That's it!</li>
                </ol>

                <h4>Tweaking your Anaglyph 3D Media</h4>

                <p>Put your 3D Glass on and look at your image. Your  image should have more depth and look strangely  3D. To get more depth, move the CYAN Layer  further from the RED Layer. Now, because we  applied this technique to the whole image, you will not be seeing a lot of separation between all  the different objects of your image. For even more  mind-blowing 3D effects, select and separate  each individual object you would like to adjust  independently and perform the same 3D technique  for each.

                    <img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_CreatingColorOffsets.jpg" width="469" height="399" alt="Adjust the 3D image by moving the layers"></p>

                <p>For example, I could select just the arm in  this picture. If I separated the arm from the  background I could give the arm in the image its  own 3D depth. Just remember the rule of law with 3D  Anaglyph. The further the color channels are
                    apart the closer the object will seem. The closer the color channels are together  the further away the  object will seem. Now you can make your own social media 3D posts or create some unique 3D posters!</p>

                <h4>Achieving Anaglyph 3D in Video with Adobe Software</h4>
                <p>Harnessing the Anaglyph 3D in Photoshop is truly amazing but what about video? For video, Adobe After Effects would be the weapon of choice. Even if you are working in Premiere, After Effects would still be your best bet to convert live action video and animated graphics to Anaglyph 3D. To achieve this, there is an effect called 3D Glasses.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_AFTEREFFECTS3D.jpg" width="839" height="345" alt="3D Video in Adobe"></p>
                <p>It comes with After Effects and it works identically to the Photoshop technique we just explored, except that it's even simpler! Do the following:</p>

                <ol>
                    <li> Add the 3D Glasses Effect to any video layer.</li>
                    <li> Select the layer you want to deliver to each eye. To keep it simple, direct the Left and Right View to the layer you want to convert, even if it is the same layer you added the 3D Glasses effect on.</li>
                    <li> Switch 3D View to BALANCED COLORED RED BLUE. Warning: Don’t get this confused with some of the other selections in the menu which may have a similar name, otherwise the effect won't work correctly with the standard Red and Cyan 3D glasses.</li>
                    <li> Now we need to separate the color channels. To do this, use the property Scene Convergence and move the value into the negative numbers. The bigger the number the more dramatic the 3D depth. </li>
                </ol>
                <p>Once again, like Photoshop, separate each individual object and adjust the depth independently for a  more intense 3D experience. Good luck!</p>

                <p>All images created and composited by Kristian Gabriel using Adobe Stock, Techsmith Snag-It and Adobe Photoshop.</p>
            </div>


    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Face-to-face training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Check out our reviews: <a href="/testimonials.php?course_id=6">Photoshop class testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>