<?php
$meta_title = "Photoshop Clipping masks | Training Connection";
$meta_description = "Learn how to use Clipping Masks in Adobe Photoshop CC. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Clipping Masks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating a Clipping Mask in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>There are a few go-to tips that can give your design project immediate punch and pizzazz. They add a lot of polish and a stamp of professionalism with minimal difficulty. One of my favorites is Creating a Clipping Mask.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/1-ClippingMask_Header.jpg" width="702" height="209" alt="Creating a clipping mask"></p>
                <p>I'm sure you have seen those movie posters and advertisements where there's a photo coming through the shape of some text. It creates the 'wow' factor. You probably think it's an advanced feature of Photoshop but it's really pretty simple. It can be done with a shape or text. Here are the steps:			</p>

                <ol>
                    <li> Once you have your new document, grab your Type Tool and type the words for your project. It's best if you choose a font that has a heavy weight. If it's a narrow or script font, the letters are not wide enough for your image to show through. It's best to go with an Arial Black type font or a slab font like Rockwell.			</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/2-ClippingMask Text.jpg" width="780" height="233" alt="Add your text"></p>

                <p>Why not join one of our <a href="/photoshop-training.php">Adobe Photoshop classes</a> in Chicago or Los Angeles?</p>

                <ol start="2">
                    <li>Now you want to Place the image you want to feature through your text. It's very important that the Image is on top of the Type or the Shape.
                    </li>
                </ol>

                <img src="https://www.trainingconnection.com/images/Lessons/Photoshop/3-ClippingMaskImage.jpg" width="700" height="312" alt="Place your image on top">

                <ol start="3">
                    <li>With the image layer selected you can either right-click that layer or select the drop-down menu on the Layers Panel.</li>
                    <li> Select "Create Clipping Mask" from the drop down menu.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/4-ClippingMaskCommand.jpg" width="640" height="355" alt="Create Clipping Mask"></p>

                <p>Voila! You now see your photo image through your type. <a href="/photoshop-training-chicago.php">Chicago Photoshop training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/5-ClippingMaskPhoto_Text.jpg" width="624" height="334" alt="Finished job"></p>

                <p>Repeat this process using a Shape instead of Type and you will show your image through your Shape.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/6-ClippingMask_Shape.jpg" width="702" height="317" alt="Using a shape in a clipping mask"></p>


                <p>Instant Magic!</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/7-ClippingMaskImageShape.jpg" width="624" height="286" alt="Finished result - picture in a shape"></p>

                <h4>Layer Styles</h4>

                <p>Layer Styles is another quick way to add some quick, impressive visuals to your design project. Layer Styles are special effects (also known as Blending Options) that you can apply to your text, shapes or images.</p>

                <ul>
                    <li><strong>Bevel &amp; Emboss</strong> - gives your layer a lite 3-D effect.</li>
                    <li><strong>Outer Glow, Inner Glow, Drop Shadow &amp; Inner Shadow</strong> - good for making your object/text stand out from the backgroundt</li>
                    <li><strong>Stroke</strong> - good for adding thickness to an object or text, for example cursive text which is normally thin.</li>
                    <li><strong>Color &amp; Overlay</strong> - allows you to try out and preview different colors before making a selection or making a permanent edit.</li>
                    <li><strong>Pattern Overlay</strong> - adds texture</li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/8-LayerStyleMenu_Bevel.jpg" width="567" height="497" alt="Layer Styles"></p>


                <p>Once you select a Layer Style, The Layer Style dialog box opens up. All of the Style options are listed on the left side of the window. Make sure the Style you selected is highlighted. Otherwise you might not actually be making the edits on the right Style. The last settings for that Layer Style will immediately be applied to your Layer. Also check the Preview checkbox so that you can see your edits choices.</p>

                <ol>
                    <li>Select Layer &gt; Layer Style</li>
                </ol>

                <p>There are many options under Bevel &amp; Emboss. We are going to focus on a few key options for this exercise. First you want to choose the type of Bevel you want to apply. You can choose from:</p>

                <ul>
                    <li>Inner Bevel</li>
                    <li>Outer Bevel</li>
                    <li>Pillow Emboss</li>
                    <li>Emboss</li>
                    <li>Stroke Emboss (Only works if a Stroke Style is applied)</li>
                </ul>

                <p>I normally move my dialog box to the side so that I can Preview my edits as I work. Start with selecting each one of the Bevel styles to see which one works best for your project.</p>

                <ol start="2&quot;">
                    <li> Select Inner Bevel.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/9-LayerStyles_BevelOptions.jpg" width="780" height="289" alt="Select Inner Bevel"></p>

                <p>The next option we are going to set is Bevel Technique. This determines the degree of emphasis of your bevel choice. Try out the three options:</p>

                <ul>
                    <li>Chisel Soft</li>
                    <li>Chisel Hard</li>
                    <li>Smooth</li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/10-LayerStyles_BevelChisel.jpg" width="780" height="287" alt="Add Smooth Effect"></p>

                <ol start="3">
                    <li>Choose Smooth.</li>
                </ol>

                <p>Play around with the Size, Depth and Direction options.</p>

                <ol start="4">
                    <li>Here are the settings for this project. <br>
                        Depth: 170 <br>
                        Direction: Up <br>
                        Size: 30
                    </li>
                </ol>

                <p>The curves options provide the most dramatic impact of all the settings. There are 12 curve options. Take some time and see the effect that each curve has on the text. Curves 3 (Cone-Inverted), 9 (Ring), 10 (Ring Double) and 12 (Sawtooth) tend to give a metallic look to your bevel.</p>

                <ol start="5">
                    <li> Select curve nine for this exercise, second curve on second row.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/11.5-LayerStyles_Bevel_Curves.jpg" width="780" height="343" alt="Adjust Curve Options"></p>

                <p>Leave all of the other default settings. Enjoy your bevelled, gold-plated text. <a href="/photoshop-training-los-angeles.php">LA-based Adobe Photoshop training</a>.</p>

                <h4>Benefits of using Layer Styles</h4>

                <ol>
                    <li>Layer Styles are editable even after you close your image. They are attached to the layer versus being applied directly to the pixels.</li>
                    <li>Layer Styles can be turned on or off from the Layer Panel after they have been applied.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/12-LayerStyleGoldenText.jpg" width="780" height="259" alt="Benefits of Layer styles"></p>

                <ol start="3">
                    <li>Layer Styles can be copied to other layers. This allows for consistency plus you don't have to remember all of your settings.</li>
                    <li>You can save Layer Styles like you can Color Swatches. This way you can use the same Layer Styles in related projects.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/13-LayerStyles_LayersPanel.jpg" width="780" height="320" alt="Reusing your layer styles"></p>

                <p>These are just a few of the benefits of Layer Styles. It's a great way to add editable special effects to your projects.</p>
                <p><a href="https://helpx.adobe.com/photoshop-elements/using/clipping-masks.html">More about clipping masks</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop class reviews</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>