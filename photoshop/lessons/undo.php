<?php
$meta_title = "The Photosshop Undo Command | Training Connection";
$meta_description = "Learn how to undo commands in Adobe Photoshop using both the undo command and history panel. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Undo</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>The Undo Command in Photoshop</h1>
                        <h5>By Rhonda Jackson, Adobe Expert/ACI</h5>
                    </div>
                </div>


                <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                    <p>Most of us are familiar with the Undo command that is available in many of software programs we use. The Ctrl + Z shortcut is pretty universal and works in most programs. In Photoshop the Undo Command functions a little different.</p>

                    <p>To join one of our <a href="/photoshop-training.php">Certified  Photoshop training classes</a> in Chicago or Los Angeles call us on 888.815.0604</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/Photoshop_Undo.jpg" width="780" height="232" alt="Undo Command"></p>

                    <p>The Undo Command is located under the customary Edit Menu, like it is in most software programs. But Photoshop allows users to undo the last step completed. If you want to undo more steps, you need to select the Step Back Command from the Undo Menu. (You can set the number of Undo Commands you can perform in Preferences.)</p>

                    <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/000_undo_blending.jpg" width="780" height="190" alt="Edit > Undo"></p>

                    <h4>Using the History Panel to Undo Commands</h4>
                    <p>The better way to undo editing steps in Photoshop is to use the History Panel. <a href="/photoshop-training-los-angeles.php">Los Angeles Photoshop training</a>.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_history_panel.jpg" width="293" height="147" alt="History Panel"></p>

                    <p>The History Panel records every editing step you have completed on an image in Photoshop. If you want to Undo the last Command, you only need to select the Step above the last edit completed in Photoshop.</p>



                    <h4><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_history_panel.jpg" width="780" height="229" alt="Undo Blending Change"></h4>


                    <p>You can see in my example that I have applied a Blending Mode and a Filter to my image. I want to Undo the Filter I applied. In order to Undo Commands you always have to Undo the steps in reverse chronological order. You can't skip around and undo steps in a random order.</p>

                    <img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_history_panel.jpg" width="780" height="236" alt="Undo Filter">

                    <p>If you want to Redo those two steps all you have to do is select the last step again. <a href="/photoshop-training-chicago.php">Adobe Photoshop workshop in Chicago</a>.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/005_history_panel.jpg" width="780" height="218" alt="redo an undo"></p>

                    <p>If you want to return to your original image, you can select the Thumbnail at the top of the History Panel. This is good for seeing the before image and the after image. Many designers use the History Panel to see the progressive changes from their editing steps. The History Panel of commands will remember your steps until you perform a new step.</p>
                    <p>The key to using the Undo Command is to remember that once you Undo a command, if you perform another edit on your image you will not be able to Redo or select the step that you reversed. Once you close Photoshop the History Panel is erased and you can no longer Undo Commands.</p>
                </div>

            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on face-to-face training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>