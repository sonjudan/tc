<?php
$meta_title = "10 Useful overlooked Photoshop features | Training Connection";
$meta_description = "Learn these 10 really useful but often ignored features in Photoshop. We deliver certified Adobe Training in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/photoshop.php">Photoshop</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tool Presets</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Adobe Photoshop">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Workflow Efficiency with Tool Presets in Adobe Photoshop</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>


                <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                    <p>Working on Photoshop projects can be tedious and many times very challenging. No matter what level of Photoshop user you are, useful shortcuts and time-saving features are a must if you are to keep your sanity. One such useful feature is called Tool Presets. Tool Presets can be created using any tool from your Tool Panel and can greatly speed up your whole workflow by giving you access to custom tools setup by you. Custom brushes, healing tools, erasers and selection tools like the Lasso tools, Quick Selection or the Magic Wand tools are just a few of the many tools you can use.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/000_Header 2.jpg" width="839" height="536" alt="Adobe Photoshop Tool presets"></p>

                    <p>Looking to learning Adobe Photoshop - why not join one of our  <a href="/photoshop-training.php">Adobe Certified  Photoshop training classes</a> in Chicago or Los Angeles call us on 888.815.0604</p>
                    <p>Tool Presets are part of a bigger collection of presets that can be managed, organized and saved on a flash drive, hard drive, emailed or uploaded to the cloud. One great benefit is that most of the presets you can output in Photoshop have almost no significant size requirements. Aside from Brush and Pattern Presets, all presets are merely a set of instructions for Photoshop to re-create. These presets are known as Generated Presets. They are not attached to any physical media and use the features and functionality already existing in any standard Photoshop project to re-generate your custom presets when loaded. Brushes and Patterns have embedded images within them, so depending on the size of the pattern or brush, the size requirements can potentially be huge. Tool Presets are generated presets and the following article is a focus on how to create them and improve your workflows. <a href="/photoshop-training-chicago.php">Adobe training in Chicago</a>.</p>

                    <h4>The Tool Preset Panel(s)</h4>
                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/001_TOOL PRESETS IN OPTIONS BAR.jpg" width="392" height="298" alt="Photoshop Tool Preset Panel"></p>

                    <p>There are two locations in Photoshop that allow access to your Tool Presets. One is conveniently located in the upper left corner of your OPTIONS BAR. Depending on what tool you currently have selected, your Tool Presets will give you custom presets for that tool. If you click on a tool and notice that it has no presets, this is perfectly normal. By default, some tools have presets and some do not. It's then up to you to create presets you think would be useful to you. Note: Be careful when choosing the Brush Tool. The Brush Tool panel and the Tool Preset can look very similar. Just remember that the Tool Preset panel is the one the furthest to the left, the first feature on the Options Bar.</p>

                    <p>The second location for your Tool Presets is the official Tool Preset Panel at WINDOW &gt; TOOL PRESETS. Both panels are almost identical and offer you the ability to create Tool Presets and then manage the lists of tools you have created. You will notice that there is a check box labeled Current Tool Only at the bottom of the panel. If this is Deselected, it will show you All Tool Presets. If this is Selected it will only show you presets of the current tool you have selected. <a href="/photoshop-training-los-angeles.php">Certified Photoshop classes in LA</a>.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/002_CREATING TOOL PRESETS.jpg" width="839" height="268" alt="Creating a new Tool Preset"></p>

                    <h4>Creating Preset Tools</h4>
                    <p>Luckily, Tool Presets are extremely easy to create!</p>

                    <ul>
                        <li><strong>Step 1</strong> - Select any tool and adjust it any way you wish. Example: You could select the brush tool, make it a round brush and give it a size of 50 px, and 0% Hardness (feathering the edge of the brush).</li>
                        <li><strong>Step 2</strong> - Go to either of the Tool Preset locations and click on the Create New Tool Preset button (the folded Post-It Note like button on the right side of the dialogue panel) Note: You will notice that in the Create New Tool Preset dialogue panel, there is an option to 'Include Color'. This will record the current foreground color you have selected for your brush to use.</li>
                        <li><strong>Step 3</strong> - Now that you've created a Tool Preset, all you need to do is come back to your Tool Preset and click on the custom tool of choice. That's it!
                        </li>
                    </ul>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/003_PRESET_MANAGER.jpg" width="617" height="427" alt="Preset Manager"></p>

                    <h4>Managing Preset Tools</h4>

                    <p>Once you have created lots of tools, it's time to get organized. </p>

                    <p>What if you wanted to save your tools to a hard drive or send your tools via email to a client, a fellow artist or yourself? </p>

                    <p>The Preset Manager is a way of organizing your Tool Presets into sets or volumes. </p>


                    <p>To get to your Preset Manager, go to the right side of your Tool Preset dialogue panel and click on the little 'Rotor' icon or the Panel Menu icon (Hamburger Menu). This is the official setting for your presets panel and a way to access the Preset Manager. Once you click on the icon, select PRESET MANAGER. From the Preset Manager, you now have some choices on the right side of the panel. If you select DONE  it will close the dialogue panel. LOAD will allow you to       load a previously saved Tool Preset file. SAVE SET allows you save individual or groups of tools in a set as a Preset file (extension .tpl). To output groups of tools, just use Shift or Control/Command keys to select a range or various Tools to output. Then select SAVE SET and save to a location on your computer. Click any of your presets and select RENAME to rename that preset. The last button is just a DELETE button that allows you to delete presets. </p>

                    <p>To re-arrange your tools into a different display order, select one or many Tool Presets and drag them into any order you desire. Note: By default, Photoshop will save your Presets files into a default Preset folder on your computer. This location differs between Mac and Windows PC systems. For easier access, save to an easily accessed location like a hard drive or a convenient place on your computer.</p>

                    <p><img src="https://www.trainingconnection.com/images/Lessons/Photoshop/004_DIFFERENT PRESET FILES.jpg" width="839" height="436" alt="Other presets"></p>

                    <h4>Other Time-Saving Presets</h4>

                    <p>While Tool Presets are fantastically useful, other features in the Presets Manager can be useful as well! </p>

                    <p>The ability to save Brush Presets is powerfully convenient. Remember that, in Photoshop, brushes are not just standard artistic painting brushes, they can also be like stamps, allowing you to "stamp" complex art, both abstract and photo-realistic elements, in one click. </p>

                    <p>Shape Presets can be used to create a library of abstract elements that are frequently used like logos, parts of logos, vector shapes, pictures and other common abstract elements. Swatch Presets are used to organize and store frequently used colors and color sets. Contours Presets are used to change the distribution of colors in an effect. This can be used to create complex photo-realistic textures like chrome and other organic and non-organic materials. Pattern Presets are used to organize repetitive patterns for backgrounds, texturing and beyond.</p>

                    <p>See how useful Presets are? And that's not even all the presets in Photoshop. Features like Curves, Levels and others can also save their own presets as well. Now you have no excuse in getting your creative work done fast.</p>

                    <p>All images created and composited by Kristian Gabriel using Adobe Stock, Techsmith Snag-It and Adobe Photoshop</p>

                    <h4>Photoshop Student Testimonials</h4>


                </div>

            </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Photoshop Student Testimonials</h4>
                    <p>Hands-on training is the most effective way to learn Adobe Photoshop. We have trained thousands of students how to use Photoshop like a pro. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=6">Photoshop training testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Photoshop courses</h4>
                    <ul>
                        <li><a href="/photoshop/fundamentals.php">Photoshop Fundamentals</a></li>
                        <li><a href="/photoshop/advanced.php">Photoshop Advanced</a></li>
                        <li><a href="/photoshop/bootcamp.php">Photoshop Bootcamp</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>