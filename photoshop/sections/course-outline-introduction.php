<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting Around Photoshop <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Kick off your training with a bang and learn the basics of viewing/navigating the Photoshop interface, opening and manipulating images and learn about Photoshop tools.</strong></p>
                                    <ul>
                                        <li>Computer Requirements</li>
                                        <li>What’s new in the current version?</li>
                                        <li>Raster vs Vector</li>
                                        <li>Creating A New Document</li>
                                        <li>Workspaces Overview</li>
                                        <li>Opening an Image</li>
                                        <li>Place Embed vs Place Linked</li>
                                        <li>Interface Layout and Navigation</li>
                                        <li>Understanding Workspaces</li>
                                        <li>2020 Properties Panel Update</li>
                                        <li>Working with images</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Basic Image Fixes <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Perform common basic retouching tasks like eliminating red eye, erasing facial blemishes and more.</strong></p>
                                    <ul>
                                        <li>Fixing Blemishes using the Spot</li>
                                        <li>Healing Tool</li>
                                        <li>Fixing Blemishes using the Healing Brush Tool</li>
                                        <li>Removing and modifying element using the Clone Stamp Tool</li>
                                        <li>Using the Patch Tool group fixes</li>
                                        <li>Removing Red Eye with the Red Eye Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Creative Selections <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Selecting parts of an image can often be tricky with conventional selection tools. Quick Mask Mode and other tools allows you to make or refine selections with greater precision.</strong></p>
                                    <ul>
                                        <li>Using Marquee Tools</li>
                                        <li>Using the Magic Wand</li>
                                        <li>Using the Quick Select Tool</li>
                                        <li>The Object Selection Tool</li>
                                        <li>Redefining the Selection in QuickMask Mode</li>
                                        <li>Erasing/Deleting vs Layer Masks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Creating Simple Text &amp; Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn to the basics of using text and commonly used effects like drop shadows, glows and more.</strong></p>
                                    <ul>
                                        <li>Creating Text</li>
                                        <li>Adding Drop Shadows Manually</li>
                                        <li>Working with Layer Styles</li>
                                        <li>Adding a Drop Shadow</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">

                        <div class="card">
                            <div class="card-header" id="heading5">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Replacing Backgrounds <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Combine two or more graphic elements together to recreate the environment of your artwork.</strong></p>
                                    <ul>
                                        <li>More Work with the Magic Wand Tool</li>
                                        <li>Simple Image Compositing</li>
                                        <li>Working with Brightness &amp; Contrast</li>
                                        <li>Working with Color Balance</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                      <div class="card">
                          <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Retro Movie Poster <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use what you know combined with new tools and techniques to create this retro B-Movie poster.</strong></p>
                                    <ul>
                                        <li>Make a Selection using the Polygonal Lasso tool</li>
                                        <li>Desaturate the Background to make the Foreground &quot;Pop&quot;</li>
                                        <li>Managing in the Layers Panel</li>
                                        <li>Adding basic type</li>
                                        <li>Warping Text</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Saving Projects and Exporting for the Web and More <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>In this lesson, you will prepare images for the Web, converting them to RGB, reducing the resolution to 72 ppi, and reducing the file size to ensure fast download times.</strong></p>
                                    <ul>
                                        <li>Resizing an Image for the Web</li>
                                        <li>Saving Images as JPEGs</li>
                                        <li>Reducing the Image File Size</li>
                                        <li>Resampling</li>
                                        <li>Adding Metadata</li>
                                        <li>Comparing GIF and PNG</li>
                                        <li>Saving as GIF and PNG</li>
                                        <li>Web Transparency</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>