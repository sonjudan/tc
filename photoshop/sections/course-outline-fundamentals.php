<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting Around Photoshop <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Kick off your training with a bang and learn the basics of viewing/navigating the Photoshop interface, opening and manipulating images and learn about Photoshop tools.</strong></p>
                                    <ul>
                                        <li>Computer Requirements</li>
                                        <li>What’s new in the current version?</li>
                                        <li>Raster vs Vector</li>
                                        <li>Creating A New Document</li>
                                        <li>Workspaces Overview</li>
                                        <li>Opening an Image</li>
                                        <li>Place Embed vs Place Linked</li>
                                        <li>Interface Layout and Navigation</li>
                                        <li>Understanding Workspaces</li>
                                        <li>2020 Properties Panel Update</li>
                                        <li>Working with images</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Basic Image Fixes <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Perform common basic retouching tasks like eliminating red eye, erasing facial blemishes and more.</strong></p>
                                    <ul>
                                        <li>Fixing Blemishes using the Spot Healing Tool</li>
                                        <li>Fixing Blemishes using the Healing Brush Tool</li>
                                        <li>Removing and modifying elements using the Clone Stamp Tool</li>
                                        <li>Using the Patch Tool group fixes</li>
                                        <li>Removing Red Eye with the Red Eye Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Creative Selections <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Selecting parts of an image can often be tricky with conventional selection tools. Quick Mask Mode and other tools allows you to make or refine selections with greater precision.</strong></p>
                                    <ul>
                                        <li>Using Marquee Tools</li>
                                        <li>Using the Magic Wand</li>
                                        <li>Using the Quick Select Tool</li>
                                        <li>The Object Selection Tool</li>
                                        <li>Redefining the Selection in Quick Mask Mode</li>
                                        <li>Erasing/Deleting vs Layer Masks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Creating Simple Text &amp; Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn to the basics of using text and commonly used effects like drop shadows, glows and more.</strong></p>
                                    <ul>
                                        <li>Creating Text</li>
                                        <li>Adding Drop Shadows Manually</li>
                                        <li>Working with Layer Styles</li>
                                        <li>Adding a Drop Shadow</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Replacing Backgrounds <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Combine two or more graphic elements together to recreate the environment of your artwork.</strong></p>
                                    <ul>
                                        <li>More Work with the Magic Wand Tool</li>
                                        <li>Simple Image Compositing</li>
                                        <li>Working with Brightness &amp; Contrast</li>
                                        <li>Working with Color Balance</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Retro Movie Poster <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use what you know combined with new tools and techniques to create this retro B-Movie poster.</strong></p>
                                    <ul>
                                        <li>Make a Selection using the Polygonal Lasso tool</li>
                                        <li>Desaturate the Background to make the Foreground &quot;Pop&quot;</li>
                                        <li>Managing in the Layers Panel</li>
                                        <li>Adding basic type</li>
                                        <li>Warping Text</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Saving Projects and Exportingfor the Web and More <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>In this lesson, you will prepare images for the Web, converting them to RGB, reducing the resolution to 72 ppi, and reducing the file size to ensure fast download times.</strong></p>
                                    <ul>
                                        <li>Resizing an Image for the Web</li>
                                        <li>Saving Images as JPEGs</li>
                                        <li>Reducing the Image File Size</li>
                                        <li>Resampling</li>
                                        <li>Adding Metadata</li>
                                        <li>Comparing GIF and PNG</li>
                                        <li>Saving as GIF and PNG</li>
                                        <li>Web Transparency</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Adjustment Layers, Masks and More <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the power of Adjustments and Masks to non-destructively alter color and manipulate images.</strong></p>
                                    <ul>
                                        <li>Color Correction with Adjustment Layers</li>
                                        <li>Using Layer Masks to Mask out Unwanted Adjustments</li>
                                        <li>Using the Curves Tool</li>
                                        <li>Using Hue/Saturation Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    More Selection Techniques &amp; Smart Objects <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Combine selections and Smart Objects in various ways to harness destructive and non-destructive features in Photoshop.</strong></p>
                                    <ul>
                                        <li>Understanding Smart Objects</li>
                                        <li>Smart Objects to Layers</li>
                                        <li>Smart Objects &amp; Resolution</li>
                                        <li>Smart Objects &amp; Filters</li>
                                        <li>Working with Layer Masks</li>
                                        <li>Working with Textures and Layer Masks</li>
                                        <li>Features that don’t work with Smart Objects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Type Mastery 01: Down The Rabbit Hole <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>In this exercise, you will go beyond fundamentals and harness the effects of Photoshop to create specialize titles and creative paragraphs.</strong></p>
                                    <ul>
                                        <li>Formatting Special Titles</li>
                                        <li>In-depth Character and Paragraph Panels breakdown</li>
                                        <li>Lorem Ipsum, Ligatures and Glyphs</li>
                                        <li>Formatting a Paragraph</li>
                                        <li>Getting creative with fonts</li>
                                        <li>To rasterize text or not</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    The Pen Tool: Working Creatively with Paths <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Unlock the creative power of the Pen Tool to create amazing effects and path art.</strong></p>
                                    <ul>
                                        <li>Working with the Pen Tool</li>
                                        <li>Drawing basic shapes</li>
                                        <li>Unique shapes</li>
                                        <li>Accent Lines &amp; Florals</li>
                                        <li>Creating a logo</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    Type Mastery 02: Text &amp; Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Continue to explore the more advanced text features like text on a path, warping in Photoshop 2020 and more.</strong></p>
                                    <ul>
                                        <li>Text in a Path</li>
                                        <li>Text on a Path</li>
                                        <li>Closed Paths vs Open o Controlling the Position of your Text</li>
                                        <li>Concentric Rings of Text</li>
                                        <li>Flow text across objects</li>
                                        <li>Text Warping 2020 &amp; Other Tweaks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    Working with Presets <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to create, save and manage presets.</strong></p>
                                    <ul>
                                        <li>What are Presets?</li>
                                        <li>Preset Management</li>
                                        <li>Create Custom Shapes</li>
                                        <li>Create Gradient</li>
                                        <li>Creating Swatches</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    Awesome 80’s Ebook Cover <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use all your skills (and learn more) to create and composite a book cover for a 1980’s genre Kindle ebook.</strong></p>
                                    <ul>
                                        <li>Recommended Ebook Cover Sizes</li>
                                        <li>Manipulating graphic elements</li>
                                        <li>Creating retro graphic elements from scratch</li>
                                        <li>Load, Save and Create 80’s presets</li>
                                        <li>Creating 80’s style title art</li>
                                        <li>Working with vector media</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    Graphics for Video <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>In this exercise, you will create and format graphics for video &amp; web projects.</strong></p>
                                    <ul>
                                        <li>Size Matters: Designing for Various Screen Sizes (SD, FHD, UHD, DCI)</li>
                                        <li>Title/Action Safe-Center-cut</li>
                                        <li>Photoshop does Video?</li>
                                        <li>Designing &amp; Creating Lower 3rds</li>
                                        <li>Designing &amp; Creating</li>
                                        <li>Texturizing your title</li>
                                        <li>Testing your title with against video</li>
                                        <li>Exporting your title to Editorial</li>
                                        <li>Bugs/Watermarks/IDs</li>
                                        <li>Export Graphics for Broadcast</li>
                                        <li>Export for Social Media</li>
                                        <li>Export for Another Application</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                    Advanced Print Workflow <i></i>
                                </a>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use the deeper Photoshop color and formatting features to create and deliver print art.</strong></p>
                                    <ul>
                                        <li>Setting up Photoshop for print art</li>
                                        <li>RGB vs CMYK vs Other</li>
                                        <li>Resolution and Pixel Density</li>
                                        <li>Important Preferences affecting print</li>
                                        <li>Setting up bleed</li>
                                        <li>Formatting guides</li>
                                        <li>Process vs Spot Colors</li>
                                        <li>Delivery essentials</li>
                                        <li>Working with TIFF, PDF, and more</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
                                    Working with Illustrator <i></i>
                                </a>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Create a feature film title for the big screen using Adobe Illustrator dynamically with Photoshop.</strong></p>
                                    <ul>
                                        <li>Sending media and paths to Illustrator</li>
                                        <li>How much should a Photoshop user need to know about Illustrator?</li>
                                        <li>Creating simple text elements in Illustrator</li>
                                        <li>Cleaning and Saving Elements in a Vector Format</li>
                                        <li>Transferring Vector to Photoshop (The Best Method)</li>
                                        <li>Made for TV or the Big Screen: Photoshop has it all</li>
                                        <li>Setting up your project in Photoshop</li>
                                        <li>Setting up a quick project in Illustrator</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse16a" aria-expanded="true" aria-controls="collapse16a">
                                    Useful Effects 01: Filter Highlights <i></i>
                                </a>
                            </div>
                            <div id="collapse16a" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use some of Photoshop’s most popular filter effects affect images and artwork.</strong></p>
                                    <ul>
                                        <li>What are Smart Filters?</li>
                                        <li>Blurring the Background</li>
                                        <li>Motion &amp; Radial Blurs</li>
                                        <li>Adding Spinning Blur to the Wheels</li>
                                        <li>Layers</li>
                                        <li>Working with Distortion Effects</li>
                                        <li>Lens flares and lighting Effects</li>
                                        <li>Sketch and Cartoon Conversions</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse17">
                                    Useful Effects 02: Special &amp; Legacy Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use some of Photoshop’s lesser known filter effects to create surreal artwork.</strong></p>
                                    <ul>
                                        <li>Working with Paths to Define Effects</li>
                                        <li>Using Fibers for Texture</li>
                                        <li>Generated Effects: Flame,Trees and Picture Frame</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse18">
                                    Social Media Design &amp; Delivery <i></i>
                                </a>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Design and learn how to publish highly creative content to your socials right out of Photoshop 2020!</strong></p>
                                    <ul>
                                        <li>Designing Dimensions (IG,TW,FB,etc)</li>
                                        <li>Text Overlays</li>
                                        <li>Video &amp; Animated GIFs</li>
                                        <li>3rd Party Plugin Mentions</li>
                                        <li>Create Artwork for Instagram &amp; Twitter</li>
                                        <li>Create An Animated GIF</li>
                                        <li>Photoshop’s New Sharing Functions</li>
                                        <li>Tips on designing Interactively for Youtube End Cards, etc.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">
                                    The Haunted Mansion: Beyond the Pixels <i></i>
                                </a>
                            </div>
                            <div id="collapse19" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Turn a lovely happy mansion into a dark and creepy nightmare using techniques learned in the class.</strong></p>
                                    <ul>
                                        <li>Building Selections</li>
                                        <li>Organic Selections</li>
                                        <li>Replacing backgrounds</li>
                                        <li>Re-lighting elements</li>
                                        <li>Working with damage</li>
                                        <li>Color grading artwork</li>
                                        <li>Re-formatting for web and social media</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>