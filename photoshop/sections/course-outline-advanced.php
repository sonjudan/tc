<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Color Grading using Levels <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Levels offer greater control than Brightness/Contrast and Color Balance. In this exercise you will fix several photos color and contrast using Levels.</strong></p>
                                    <ul>
                                        <li>Color Correction with Adjustments Layers vs Adjustment Filters</li>
                                        <li>What is Tone?</li>
                                        <li>What is a Histogram?</li>
                                        <li>Levels Adjustment Explained</li>
                                        <li>Practical Work with Levels</li>
                                        <li>Repairing Photos</li>
                                        <li>Channel Manipulations with Levels</li>
                                        <li>Preset Levels</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Color Grading Using Curves <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the most power color and tonal correction tool in Photoshop to repair, correct and stylize images and graphics.</strong></p>
                                    <ul>
                                        <li>The Curves Tool Explained</li>
                                        <li>Setting White, Black, and Gray Points</li>
                                        <li>Setting Midpoints</li>
                                        <li>Saving and Loading Curves Presets</li>
                                        <li>Correcting Tone & Color</li>
                                        <li>Setting White, Black, and Gray Points</li>
                                        <li>Channel Manipulations with Curves</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse1">
                                    Retouching Techniques Part I <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Retouch any image by removing blemishes such as acne, scarring and more. Go into the settings of popular healing tools and tweak the features to optimize your retouching workflow.</strong></p>
                                    <ul>
                                        <li>Understanding Healing & Awareness</li>
                                        <li>Using the Clone Stamp Tool</li>
                                        <li>Using the Spot Healing Tool</li>
                                        <li>Using the Healing Brush Tool</li>
                                        <li>Using the Patch Tool</li>
                                        <li>Removing Blemishes & Stray Hairs</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse1">
                                    Retouching Techniques Part 2 <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Take retouching to the next level by using tools and professional techniques to remove blemishes, smooth out skin tone and replace skin texture.</strong></p>
                                    <ul>
                                        <li>The Power of Frequency Separation In Theory and Practice</li>
                                        <li>Creating High and Low Frequency Layers</li>
                                        <li>Isolating Skin Texture</li>
                                        <li>Isolating Skin Tone</li>
                                        <li>Re-combining Layers</li>
                                        <li>Using Masks & Brushes for Airbrush Effect</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse1">
                                    Retouching: Liquify <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Radically manipulate pixels using one of Photoshops most powerful distortion tool. Become a digital plastic surgeon!</strong></p>
                                    <ul>
                                        <li>An Overview of the Liquify Panel</li>
                                        <li>Brush Tool Options</li>
                                        <li>Using the Liquify Filter: Bloat, Pucker, Forward Warp, Restoration Tools</li>
                                        <li>Artistic Retouching of Facial Features</li>
                                        <li>Having fun with Face-Aware</li>
                                        <li>Freezing and Unfreezing</li>
                                        <li>Creating Your Own Alien</li>
                                        <li>Losing weight without a diet</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse1">
                                    Displacement Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the Displacement Filter to map one image over another.</strong></p>
                                    <ul>
                                        <li>What is Displacement?</li>
                                        <li>How Displacement is used Professionally</li>
                                        <li>Displacement Workflow</li>
                                        <li>Creating a Displacement Map</li>
                                        <li>Working with Displacement Filter</li>
                                        <li>Improving the Distortions</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse1">
                                    Blending Modes & Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Blending Modes are one of the most powerful secret weapons in graphic design. They allow you to blend one layer with another in various ways to creatively composite your images together.</strong></p>
                                    <ul>
                                        <li>About Blending Modes</li>
                                        <li>Experimenting with different Blend Modes (Screen, Multiply, Overlay)</li>
                                        <li>Using the Color Blending Mode to Tint an Image</li>
                                        <li>Mask Blending</li>
                                        <li>Adding Scars and Blood</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">

                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse07" aria-expanded="true" aria-controls="collapse1">
                                    Working In Perspective <i></i>
                                </a>
                            </div>
                            <div id="collapse07" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use Photoshop to map images accurately in perspective.</strong></p>
                                    <ul>
                                        <li>Manually putting in perspective</li>
                                        <li>Manually Editing Perspective</li>
                                        <li>Using Perspective Filters</li>
                                        <li>Troubleshooting perspective issues</li>
                                        <li>Selecting In Perspective</li>
                                        <li>Healing In Perspective</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse08" aria-expanded="true" aria-controls="collapse1">
                                    Compositing: Matching Color <i></i>
                                </a>
                            </div>
                            <div id="collapse08" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn methods to match or compliment one image’s color to another - making them look as if they belong in the same scene.</strong></p>
                                    <ul>
                                        <li>A Over B</li>
                                        <li>It’s All About Selections</li>
                                        <li>Lighting & Shadow</li>
                                        <li>Backlight Technique</li>
                                        <li>Matching Color</li>
                                        <li>Curves Color Channels</li>
                                        <li>Contrast Matching</li>
                                        <li>Clipping Masks</li>
                                        <li>Matching Noise/Grain</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse09" aria-expanded="true" aria-controls="collapse1">
                                    Working with Lighting Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse09" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>From Jedi sabers to Bar Signs - these Photoshop Effects will have you creating some amazing effects art.</strong></p>
                                    <ul>
                                        <li>Compositing Lighting Effects</li>
                                        <li>Creating & Working with Flares</li>
                                        <li>Creating A Neon Sign</li>
                                        <li>Techniques for Sabers</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse1">
                                    Organic Text <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn to make text look more organic with these mapping techniques and filters.</strong></p>
                                    <ul>
                                        <li>Endless Possibilities of Organic Text</li>
                                        <li>Text Out of Grass</li>
                                        <li>Working with Smoke</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse1">
                                    Advanced Selection Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use one of the most advanced techniques to make highly complex selections.</strong></p>
                                    <ul>
                                        <li>Select & Mask Review</li>
                                        <li>Going off-road with Procedurals</li>
                                        <li>It’s All About Channels</li>
                                        <li>Working with Levels to create contrast maps</li>
                                        <li>Configuring your Selection Brushes to isolate edges</li>
                                        <li>Selecting Hair - No Easy Thing</li>
                                        <li>Employing the Burn and Dodge Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse1">
                                    Working with Brushes <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn the power of Photoshop Brushes and Patterns through the newly updated and enhanced preset panels.</strong></p>
                                    <ul>
                                        <li>How to use and access new and legacy brushes</li>
                                        <li>Create and customize brushes and brush settings</li>
                                        <li>Brush across paths</li>
                                        <li>Working with organic brushes</li>
                                        <li>How patterns and brushes work together</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse1">
                                    Working with Camera Raw <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>High-end digital cameras can save images in the RAW format, which contains extra data obtained for the camera's sensor. Adobe Camera Raw helps you process these higher quality RAW images.</strong></p>
                                    <ul>
                                        <li>Opening & Editing Raw Files</li>
                                        <li>Camera Raw Panels</li>
                                        <li>Editing Color and Tone</li>
                                        <li>Fixing Exposure Issues</li>
                                        <li>Selective Corrections</li>
                                        <li>Applying Grades to Multiple Photos</li>
                                        <li>Removing Noise</li>
                                        <li>DeHaze Images</li>
                                        <li>Saving Settings and Versions</li>
                                        <li>Opening Images in Photoshop</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>