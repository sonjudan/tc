<?php
$meta_title = "Adobe Photoshop 2019 Fundamentals | Hands-on beginner training";
$meta_description = "Beginner Photoshop classes in Chicago and LA. Hands-on learning from Photoshop experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../photoshop-training.php">Photoshop Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Fundamentals</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Photoshop 2020</h1>
                    <h3 data-aos="fade-up">Fundamentals Training Course</h3>

                    <div data-aos="fade-up">
                        <p>This beginners course will teach you, step-by-step, how to create cutting-edge graphics and special effects with Adobe Photoshop. Trainees will complete a series of real world projects, where they will learn photo retouching, color adjustment, working with masks and layers, compositing images and much more.</p>
                    </div>

                    <div class="mt-lg-5 mb-lg-5" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>3 Days</span>
                        <sup>$</sup>1195<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg" data-target="#book-class-level-3" data-toggle="modal">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Photoshop Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Photoshop Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/photoshop/TC_Photoshop_2019_Fundamentals.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-photoshop-fundamentals.png" alt="Photoshop 2019 - Fundamentals Training Course" class="" width="262">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up" data-aos-delay="300">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>This course is ideal for designers, marketers and anyone who is pursuing a career in graphic design. No prior experience of Photoshop is needed. Training available on both Mac and PC.</p>
                    <p>View our full range of <a href="/photoshop-training.php">Adobe Photoshop training courses</a>, or see below for the detailed outline for Photoshop Fundamentals.</p>
                </div>

                <a href="#book-class-level-3" class="btn btn-secondary btn-lg" data-target="#book-class-level-3" data-toggle="modal" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/photoshop/sections/course-outline-fundamentals.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-photoshop-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-photoshop.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>