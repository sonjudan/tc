<?php
$meta_title = "Adobe After Affects 2019 Fundamentals | Hands-on certified training";
$meta_description = "Small face-to-face instructor-led After Affects classes taught by After Affects pros.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../after-effects-training.php">After Affects Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Fundamentals</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">After Affects 2019</h1>
                    <h3 data-aos="fade-up">Fundamentals Training Course</h3>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <p>Adobe After Effects is a powerful program used for creating motion graphics and special effects for video and animation projects. In this beginner's workshop you will learn tools and skills to effectively use this powerful program.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-4" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>3 Days</span>
                        <sup>$</sup>1395<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up" data-aos-delay="350">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="After Affects 2019 Fundamentals" data-price="$1395">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="After Affects Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="After Affects Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/after-effects/TC_After Affects_2019_Fundamentals.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-after_effects-fundamentals.png" alt="After Affects 2019 - Fundamentals Training Course" class="" width="230">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up" data-aos-delay="300">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>Classes taught by pros!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>This class is aimed at graphic and video professionals who need an essential understanding of motion graphics, title animation, plus powerful effects compositing. Experienced users will also benefit from many hands-on projects and tricks learned in the class. Training available on both Mac and PC.</p>

                    <p>View our full range of <a href="/after-effects-training.php">Adobe After Effects  training courses</a>, or see below for the detailed outline for After Effects Fundamentals.</p>

                    <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="After Affects 2019 Fundamentals" data-price="$1395">
                        <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                        Book Course
                    </a>
                </div>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/after-effects/sections/course-outline-fundamentals.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>