<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">BTITLE</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Adobe After Effects">
                    </div>
                    <h1 class="" data-aos="fade-up" >Title</h1>

                    <div data-aos="fade-up">

                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe After Effects Lessons</h4>
                    <ul>
                        <li><a href="/after-effects/lessons/animated-colors.php">How to create backgrounds and text with animated colors</a></li>
                        <li><a href="/after-effects/lessons/spinning-effect.php"> How to make a Spinning Object</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>