<?php
$meta_title = "Creating spinning objects in After Effects | Training Connection";
$meta_description = "Learn how to create a spinning planet in Adobe After Effects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Ordering Effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="How to creating spinning objects in After Effects">
                    </div>
                    <h1 class="" data-aos="fade-up">How to create spinning objects in After Effects</h1>

                    <div data-aos="fade-up">
                        <p>Need training? Our <a href="/after-effects-training.php">instructor-led After Effects certified classes</a> are the best and fastest way to learn this program. Public classes are offered in Chicago and Los Angeles, and onsite classes are offered country-wide.  Obtain a quote for <a /onsite-training.php">onsite Adobe training</a>.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>In After Effects go to Composition/New Composition.</p>
                <img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-16.jpg" alt="Creating a new composition" width="474" height="374" title="Applying a Drop Shadow Effect"></p>
                <p>Choose the preset of your choice. We are going with HDTV1080 29;97, background color black and then Click OK.</p>
                <p>We are ready to bring in our planet. We are using a flat planet map of the earth. Nasa.gov has planet maps and anything from NASA does not have a copyright.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-17.jpg" alt="Map of Earth" width="750" height="375" title="Rotation setting"></p>

                <p>File import your flat map into your project panel and then drag it in to the timeline. <a href="/after-effects-training-chicago.php">Chicago-based After Effects training</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-18.jpg" width="750" height="409" alt="Importing flat art into After Effects"></p>
                <p>Click on the picture name in the timeline and then type CC Sphere in the Effects &amp; Presets panel. Double click on the CC Sphere effect. What’s very cool about this effect is that if your map is drawn correctly you will not have a seam where the left and right edges meet.</p>Your map is starting to look like a planet. You can go to the Effect Controls panel and twirl open light and change the light height to your liking.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-19.jpg" alt="Adjust the light height setting" width="750" height="335"></p>

                <p>To make your planet spin twirl open rotation in the Effect Controls panel.</p>
                <p>Looking of <a href="/after-effects-training-los-angeles.php">After Effects training in Los Angeles</a>?</p>

                <p>Then click the stop watch on Rotation Y.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-20.jpg" alt="Adjusting Stopwatch on Y rotation" width="273" height="58" title="Rotation Effect"></p>

                <p>Then move your timeline indicator to the end of the timeline.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-21.jpg" width="477" height="170" alt="Move timeline indicator"></p>

                <p>Go back to the effect control and replace the 0x on Rotation Y with the number of your choice. We are using 3x.</p>You should now have a rotating planet. NASA also has public domain pictures of star fields which you can download and place beneath your planet in the timeline.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-22.jpg" width="750" height="405" alt="Finish project - rotating planet"></p>
            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe After Effects Lessons</h4>
                    <ul>
                        <li><a href="/after-effects/lessons/effects-order.php">Correct ordering of effects</a></li>
                        <li><a href="/after-effects/lessons/animated-colors.php">How to create backgrounds and text with animated colors</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>