<?php
$meta_title = "Motion Effects in After Effects | Training Connection";
$meta_description = "Learn how to create superhero Motion Effects in Adobe After Effects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Motion Effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Superhero Motion Effects In Adobe After Effects">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Superhero Motion Effects In Adobe After Effects</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>

                </div>
            </div>

            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">

                <p>The other day I was watching the series The Flash on Netflix and remembered how, when I was younger, I wanted to run "superhero" fast! Who wouldn't want to do that? For a short while, my 7-year-old self was a believer that this was actually possible. Unfortunately, the inevitable reality set in. Combine this with my body type and the fact that I hated physical education. I think you get the idea! Anyway, motion blur and motion trail effects have been a staple of the superhero and fantasy film genres for a very long time. The idea that someone can run faster than a speeding bullet still captures imaginations today. The digital effects of motion blurring and motion trailing go way beyond human speed though. They can add a sense of organic movement to animated titles and logos and even give basic shapes and particles like like behaviors. In this article, I am going to give you a simple pathway to achieving a superhero-like motion effect that you can modify or reverse engineer into creating all kinds of motion effects.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/000_Header_super_hero.jpg" width="839" height="536" alt="Motion Graphics in After Effects"></p>

                <p>Need to learn Adobe  After Effects? We run the best  <a href="/after-effects-training.php">After Effects workshops</a> in Chicago and Los Angeles, and  our trainers can deliver onsite training right across the country. Obtain a quote for <a href="/onsite-training.php">onsite Adobe training</a>.</p>

                <h4>Breaking Down The FX</h4>

                <p>Fundamentally, motion blur effects for fast-moving objects can be broken down in two categories: High Speed without Definition and High Speed with Definition. <strong>High Speed without definition</strong> allows you to              see an object moving very, very quickly - but you see very little detail as to what or who is moving that fast. This type of motion effect can be seen on titles and other types of graphics that animate in very quickly and then slow down or stop to reveal what the object or words are. Within the motion effect itself - it's not always necessary to see detail or even know what is moving that fast if the end result is going to be sitting right in front of your eyes. <strong>High Speed with Definition</strong> allows you to see an object moving very, very quickly and you can also visibly tell what or who the fast-moving object or person is.</p>
                <p><a href="/after-effects-training-los-angeles.php">Adobe certified After Effects classes in LA</a>.</p>
                <p>Once you've determined which style of motion effect you are going for - you can begin to pull your assets together in After Effects. Whether you have chosen High Speed with or without Definition - the process              of motion blurring your object will work much better if you have isolated your object on its own layer. For example, if you wanted to shoot yourself running fast  it would be best to shoot yourself against a green screen and then shoot your background separate so you can put the two together later and have control over each separately. In the following exercise, I will be using simple free footage from YouTube. If you would like to follow along, you can download the YouTube file here: <a href="https://youtu.be/9rH0GM3FzTA">https://youtu.be/9rH0GM3FzTA</a></p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/001_Mask Object.jpg" width="810" height="497" alt="Running motion Effects"></p>

                <h4>Setting Up The File</h4>
                <p>To continue working on this file - you will need to do the following:</p>

                <ul>
                    <li><strong>Import</strong> the Running Skeleton Green Screen footage or whatever footage you have into After Effects (File &gt; Import &gt; File)</li>
                    <li><strong> Drag your footage from your Project panel to the New Composition button</strong> at the bottom of the panel (this will create a brand-new Composition with your footage in it)</li>
                    <li><strong>Use Option+[ and Option+] to trim your footage down (playhead editing)</strong>. If you are using the same file I am,  there is an opening title  as well as several versions of the skeleton running we don't want to use. I'm using the first run of the skeleton and have cut the footage down to just this piece (it’s roughly about 1 second and 14 frames). Note: You can cut your footage down any way you wish.</li>
                    <li><strong>Make sure your footage begins at the beginning of your composition</strong> (if it's not, select your layer, hit the Home Key on your keyboard and then Left Bracket '['. This will slide and snap the head of your footage to the playhead.</li>
                    <li> <strong>Key out the green screen</strong>. To keep things simple, select your layer and then go to <strong>EFFECT &gt; KEYING    &gt; KEYLIGHT</strong>. Under Keylight's Effects Controls go to the property Screen Colour and select the sample tool. Now click any of the green in your Composition window to isolate your object. Note: If you are using your own images, you may have additional clean-up to do. Every image and video is different!</li>
                    <li> The final thing before we begin is to get rid of the watermark logo in the upper right of the footage (if you are using your own footage - ignore this). To do this, use the Pen Tool to select only the  portion of the running skeleton as illustrated above. Make sure to close the path and the logo will  disappear.</li>
                </ul>
                <p><a href="/after-effects-training-chicago.php">Chicago certified After Effects classes</a>.</p>
                <h4>Adding Your Superhero-like Motion Effect</h4>
                <p>Now that we have a video to play with, move your playhead until you can see your skeleton clearly and let's start to build out our effects. At this point, it's time to go over what we are about to do. We are going to  create a High Speed motion effect with Definition. To do that, we need to create a duplicate our skeleton  layer. Why? One layer will be used for the speed effects and the other layer will be used to control how much definition we want within our motion effect. Do the following:</p>

                <img class="pull-right ml-5 mb-4" src="https://www.trainingconnection.com/images/Lessons/After-Effects/002_KeylightMenu.jpg" width="339" height="589" alt="Keylights - Motion Effects">

                <ul>
                    <li>Select your layer and use Cmd+D (MAC) or Cntl+D (WIN) to <strong>Duplicate your layer</strong></li>
                    <li><strong>Name your layers: Detail Control</strong> (bottom), <strong>Motion Effect</strong> (top)</li>
                    <li>Your Motion Effect layer (top layer) will be the one that will actually create your trail. Select your Motion Effect layer and go to <strong>EFFECT &gt; TIME &gt; ECHO</strong>. The Echo effect is a popular After Effects effect that can create simple echoes and amazing streaming effects.</li>
                    <li>Now use these values to dial in your Echo in your Effect Controls: <strong>Echo Time = -0.060</strong> (controls the amount of time between echoes), Number Echoes = 8, Starting Intensity = 1.00 (the opacity of the first image in the echo sequence), <strong>Decay = .71</strong> (the echo trailing opacity) Note: <em>Leave everything else at default values.</em></li>
                    <li> The Echo effect is amazing - but we need to boost the motion blurring. Go to <strong>EFFECT &gt; BLUR &amp; SHARPEN &gt; DIRECTIONAL BLUR</strong> and in your Control Panel - make the <strong>Direction = 90°</strong> (this should make your motion blur whatever direction your object is moving in) and make <strong>Blur Length = 251.1</strong></li>
                    <li>You should have a pretty cool looking blur by now - and if you are using the video I am using - there is not much color in the blur. If you want to modify the color or add color - go to <strong>EFFECT&gt;COLOR CORRECTION &gt; COLOR BALANCE</strong>. Using Color Balance, we can alter and add color in the highlights, midtones or shadows. Here are the settings I have used: <strong>Shadow Red Balance = 82.0,  Midtone Red Balance = 100.0, Highlight Red Balance = 81.0</strong></li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/004_EndResultStatic.jpg" width="1003" height="512" alt="Finish project - Motion Effects"></p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget widget-col-2">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>