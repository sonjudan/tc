<?php
$meta_title = "Using Animated colors in After Effects | Training Connection";
$meta_description = "Learn how to create backgrounds and text with animated colors in After Effects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Animated Colors</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Adobe After Effects">
                    </div>
                    <h1 class="" data-aos="fade-up" >How to create backgrounds and text with animated colors in After Effects</h1>
                    <div data-aos="fade-up" >
                        <p>Need training? Our <a href="/after-effects-training.php">After Effects certified classes</a> are the best and fastest way to learn this software program. Our public classes are available in Chicago and Los Angeles, plus our trainers can deliver onsite training  right across the country. Obtain a quotation for an <a href="/onsite-training.php">After Effects onsite class</a>.<br></p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>Go to Composition/New Composition. </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-7.jpg" alt="New Composition Menu" width="750" height="435" title="Applying a Drop Shadow Effect"></p>

                <p>Create a composition with the preset of your choice.</p>
                <p>You can select layer/new layer or use the keyboard shortcuts Cmnd/Y or Control Y, then click ok to create a solid.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-8.jpg" alt="Composition Solid Settings panel" width="429" height="430" title="Rotation setting"></p>
                <p>The color of the solid does not matter. For <a href="/after-effects-training-chicago.php">more details on Chicago After Effects classes</a>.</p>
                <p>In order to see multiple colors on a layer you need to first apply a texture. We will  go to the effects control panel and apply the Turbulent Noise effect to our layer.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-9.jpg" width="750" height="373" alt="Applying the Turbulent Noise effect"></p>

                <p>Then we will apply the Colorama Effect.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-10.jpg" alt="Screen shot of Colorama Effect" width="750" height="382" title="Transform effect"></p>
                <p>Next we will go in to the Effect Controls panel and twirl open output cycle and change to the Fire preset. <a href="/after-effects-training-los-angeles.php">Beginner and advanced After Effects classes in Los Angeles</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-11.jpg" alt="Using the Fire preset in After Effects" width="750" height="185" title="Rotation Effect"></p>

                <p>The Fire preset looks much better than the blue green colors when we first applied Colorama.</p>
                <p>Under Effect Controls you can set keyframes for evolution to get a nice fluid animation.</p>
                <p>In Effect Controls you can also go to the noise type drop down under turbulent noise and change it to block.</p>
                <p>If you have set evolution keyframes under turbulent noise the blocks will move.</p>
                <p>Next we will double click on the type tool <img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-12.jpg" width="35" height="31" alt="Type Tool" class="d-inline"> and create text.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-13.jpg" width="750" height="488" alt="Add the words Training Connection"></p>
                <p>Then we will go down in to the timeline and change the Track Matte option on my solid to Luma Matte.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-14.jpg" width="723" height="222" alt="Select Luma Matte option"></p>

                <p>If you don't see the Track Matte option you may need to press the toggle switches and modes button on the bottom of the timeline.</p>

                <p>After you apply the Track Matte the solid layer with the effects should fill the text. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-15.jpg" width="730" height="485" alt="Finished After Effects Project"></p>

                <p>The area around the text is transparent so another video or picture can be added to layer 3. I also recommend experimenting with the CC Kaleidoscope effect for different patterns.</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe After Effects Lessons</h4>
                    <ul>
                        <li><a href="/after-effects/lessons/animated-colors.php">How to create backgrounds and text with animated colors</a></li>
                        <li><a href="/after-effects/lessons/spinning-effect.php"> How to make a Spinning Object</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>