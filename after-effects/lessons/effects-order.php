<?php
$meta_title = "Ordering Effects in Adobe After Effects | Training Connection";
$meta_description = "Learn how to order special effects in Adobe After Effects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Ordering Effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Ordering Effects in After Effects">
                    </div>
                    <h1 class="" data-aos="fade-up" >Ordering Effects in After Effects</h1>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>The order of your effects in the Effects Control panel will determine how things look in After Effects.
                    Here I've applied a Drop Shadow effect to my spaceman and brought the distance away in the effects control panel.</p>

                <p>Need training? Our <a href="/after-effects-training.php">After Effects classes</a> are the best and fastest way to learn this program. Public classes are available in Chicago and Los Angeles, plus our trainers can deliver onsite training  right across the country. Obtain a quote for <a href="/onsite-training.php">onsite After Effects training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-1.jpg" alt=""></p>

                <p>My intention for this animation is for the spaceman to spin and for the shadow to stay in place and spin also. I'll go in to timeline and twirl open to rotation and rotate my spaceman.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-2.jpg" alt=""></p>

                <p>I rotated my spaceman 133 degrees and ended up with the following results.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-3.jpg" alt=""></p>

                <p>Instead of the shadow staying in place and rotating it stayed on my spaceship's right wing. I'll reset rotation back to zero and find the Transform effect in Effects and Presets and apply it to my spaceman. Right now the Transform effect is beneath my drop shadow effect in the Effect Controls panel. <a href="/after-effects-training-chicago.php">Click here</a> for more information on Chicago After Effects classes.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-4.jpg" alt=""></p>
                
                <p>If I adjusted rotation under the Transform Effect we would have the same result of the shadow following the spaceship. However if I click and drag the Transform effect so that it is over the Drop Shadow effect in Effect Controls,</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-5.jpg" alt=""></p>
                
                <p>Then adjusted rotation underneath the Transform Effect in the Effect Controls panel, we would have the shadow spinning in place. <a href="/after-effects-training-los-angeles.php">LA-based Adobe After Effects classes</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/after-effects-6.jpg" alt=""></p>

                <p>In summation, if effects aren't behaving the way that you'd expect, try re-ordering them in the Effect Controls panel.</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe After Effects Lessons</h4>
                    <ul>
                        <li><a href="/after-effects/lessons/animated-colors.php">How to create backgrounds and text with animated colors</a></li>
                        <li><a href="/after-effects/lessons/spinning-effect.php"> How to make a Spinning Object</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>