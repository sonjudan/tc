<?php
$meta_title = "Illustrator to After Effects Worksflow | Training Connection";
$meta_description = "Learn how to import Illustrator graphics into After Effects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/after-effects.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Ordering Effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Illustrator to After Effects: 3D Logo Workflow">
                    </div>
                    <h1 class="" data-aos="fade-up">Illustrator to After Effects: 3D Logo Workflow</h1>
                    <h5 data-aos="fade-up">By Kristian Gabriel, Adobe Expert/ACI</h5>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">

                <p>The ability to take a custom vector logo designed in Illustrator and bring it to After Effects for animation is invaluable. Animated network Idents, logos, lower thirds - the applications are endless. In this quick, down and dirty guide, let us look at the best ways to get your logos from Illustrator to After Effects and prep them to be animated in 3D.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/000_Article-Header-Banner.jpg" alt="3D logo designed in Adobe Illustrator" width="839" height="536" title="Applying a Drop Shadow Effect"></p>

                <p>Looking to learn Adobe Illutrator or After Effects?. We run some of the best <a href="/illustrator-training.php">Illustrator workshops</a> and <a href="/after-effects-training.php">After Effects workshops</a> in  Chicago and Los Angeles, plus our trainers can deliver onsite training  right across the country. Obtain a quote for <a href="/onsite-training.php">onsite Adobe training</a>.</p>

                <h4>Step 01 - Prep &amp; Save Your Logo In Adobe Illustrator</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/001_ExportYourLogo.jpg" width="839" height="268" alt="Prep your logo in Illustartor"></p>

                <p>Before you can import your logo into After Effects, it needs to be prepped and saved in a proper vector format. This guide assumes that you have either designed your vector logo in Illustrator, or you have imported a vector logo into Illustrator. Note: <em>Raster formats like JPEGs, TIFFs, PNGs, GIFs and others cannot be used with this method</em>.</p>

                <p>Once you’ve determined that your logo is finished, make it a contrasting color based on your After Effects composition background color. If your background is black, you can make your logo white and if your background is white, make your logo black. This is mainly so you can see your logo when imported into the timeline. You can always change color later. Now, all you need to do is simply save your logo as a native ‘.ai’ file.</p>
                <p><a href="/after-effects-training-los-angeles.php">Small After Effects classes</a> in Los Angeles.</p>

                <p>This is the official Adobe Illustrator project file format and is the best file for bringing into programs like After Effects. As for the writing of this article, you can alternatively use EPS files (Encapsulated Post Script) and vector PDF files. Now, to save your logo, go to <strong>FILE &gt; SAVE AS...</strong> and make sure <strong>Adobe Illustrator (*.ai), Adobe EPS (*.eps) or Adobe PDF (*.pdf)</strong> is selected. Now save your project file.</p>


                <h4>Step 02 - Importing &amp; Prepping Your Logo for 3D</h4>
                <p>After saving your logo, open After Effects and use the standard <strong>FILE &gt; IMPORT &gt; FILE...</strong> to bring in your logo as an Adobe Illustrator file. At this point, you need an After Effects composition to put it in. If you already have one created, go ahead and drag your logo from the project window to the timeline. If you do not have a composition created yet, create one now at the size and framerate of your delivery project and then drag your logo from the project window to your timeline.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/002_ENABLE3D.jpg" width="839" height="268" alt="Enable 3D"></p>

                <p>Now, to prepare your logo layer and make it ready for 3d you need to do two things:</p>
                <ul>
                    <li>Enable your logo layer for 3D in your timeline by clicking on the Enable 3D checkbox. And...</li>
                    <li>Right-click your logo layer in the timeline and select <strong>CREATE SHAPES FROM VECTOR LAYER</strong>.</li>
                </ul>


                <p><strong>Create Shapes From Vector Layer</strong> properly converts your vector layer from a standard vector to a vector generated shape that After Effects can better manipulate, giving it 3D properties when you are in the right mode. You will now notice you have two (2) logos in your timeline. One shape layer version and one Illustrator .ai version that is disabled. At this point, you can delete the .ai layer to reduce clutter.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/003_ImportConvertYourLogo.jpg" width="839" height="268" alt="Create shapes from Vector Layer"></p>

                <h4>Step 03 - 3D Conversion</h4>

                <p>If you are in your composition or timeline window, use <strong>Command or Control + K</strong> or go to <strong>COMPOSITION &gt; COMPOSITION SETTINGS... </strong>and click on the <strong>3D Renderer tab </strong>as pictured above. By default, you will see that After Effects is in a mode called <strong>Classic 3D</strong>. <strong>Classic 3D</strong> is the traditional After Effects editing mode and primarily focuses on editing 2D Graphics in 2D or 2D Graphics in 3D space. Unfortunately, this will not allow you to work with “real” 3D objects without a 3rd party plugin like <strong>Element 3D</strong>. To remedy this, we have two additional rendering modes we can work with <strong>Ray-Traced 3D</strong> and <strong>Cinema 4D</strong>. Each of these rendering modes uses different math to enable the extrusions of 3D objects in 3D space. Both enable a lot of 3D functionality but each will disable a certain number of After Effects functions and features. For example:</p>

                <ul>
                    <li><strong>CINEMA 4D</strong> Mode <strong><u>WILL DISABLE</u></strong>: blending modes, track mattes, layer styles, masks and effects on continuously rasterized layers, text and shape layers and collapsed 3D precompositions. It will also disable underlying transparencies, light transmission, adjustment lights, transparency and index refraction, the ‘Accepts Shadows Only’ function, motion blur and camera depth of field.</li>
                    <li><strong>RAY-TRACED</strong> mode <strong><u>WILL DISABLE</u></strong>: blending modes, track mattes, layer styles, masks and effects on continuously rasterized layers, tet and shape layers and collapsed 3D precompositions. Ray-Traced mode will also disable underlying transparencies.</li>
                </ul>

                <p>Cinema 4D is the newer renderer and offers a generally faster and easier solution to you rendering process in some situations.&nbsp;Choose <strong>Cinema 4D</strong> as the renderer and then click ‘OK’.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/004_3DModes.jpg" width="528" height="458" alt="3D Renderer"></p>

                <h4>Step 04 - 3D Logo Details &amp; Additional Tweaks</h4>

                <p>Before you start making your logo 3D, add a simple <strong>Point or Spot Light</strong> so that you can see the extrusion properly. That’s it! Now, to extrude your logo in 3D space, reveal the <strong>Geometry Options</strong> under your 3D logo layer and increase <strong>Extrusion Depth</strong>. Other things you can play with is the type of <strong>Bevel Styles and Bevel Depth</strong> to make the edges of your text a little more interesting. If you want change color of the individual letters or pieces of your logo - go into your shape layer’s Content Properties and you will be able to tweak color and a whole lot of other things! <a href="/after-effects-training-chicago.php">Classroom After Effects classes in Chicago</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/005_AddingLights.jpg" width="839" height="536" alt="Final touches to logo before importing into After Effects"></p>

                <h4>Alternatives to bringing in logos from Photoshop and other Apps</h4>

                <p>The following are alternative methods you can use if your logo was created in Photoshop, CorelDraw or any other graphic application.</p>
                <ul>
                    <li><strong>PHOTOSHOP METHOD: </strong>If you have your logo in Photoshop, convert your logo to Paths (you can use the Make Work Paths feature) and then you can go to <strong>EXPORT &gt; PATHS TO ILLUSTRATOR... </strong>This will export your vector paths into an Illustrator Project file. Then you can fill paths and generally tweak your vector logo until its ready for saving out to After Effects.</li>
                    <li><strong>OTHER APPS: </strong>Using other apps like Corel Draw - you can export into an .EPS file for immediate import into After Effects or even a <strong>.CDR (Corel Draw file) or .SVG (Scalable Vector file)</strong> - but these formats will have to be loaded into Illustrator first and resaved as an <strong>.ai, .eps or .eps file</strong>.</li>
                </ul>

                <p>Hopefully this wasn’t too crazy to absorb! Remember that if you want to delve deeper into these subjects and more, check out Training Connection’s After Effects, Illustrator and Photoshop classes and boot camps and good luck with your own projects! <strong></strong></p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe After Effects Lessons</h4>
                    <ul>
                        <li><a href="/after-effects/lessons/animated-colors.php">How to create backgrounds and text with animated colors</a></li>
                        <li><a href="/after-effects/lessons/spinning-effect.php"> How to make a Spinning Object</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>