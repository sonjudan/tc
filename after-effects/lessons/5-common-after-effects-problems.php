<?php
$meta_title = "5 Common problem in After Effects | Training Connection";
$meta_description = "5 Common problems experienced in Adobe After Effects and how to fix them";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">After Effects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Motion Effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="After Effects: 5 Common Problems & Fixes for First-Time Users">
                    </div>

                    <div data-aos="fade-up">
                        <h1>After Effects: 5 Common Problems & Fixes for First-Time Users</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>Whenever you are learning a new application  there are always quirks, oddly programmed features and tricks you must embrace to take full advantage of that application. After Effects, like any professional software, has a number of these "quirks" or problems you need to figure out to truly master this amazing software. The following are common issues, with solutions, that many first-time After Effects users face when learning After Effects.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/000_Header.jpg" alt="Solutions to common problems in After Effects" width="839" height="536" title="Applying a Drop Shadow Effect"></p>
                <p>Looking to master Adobe After Effects. Why not join one of our legendary <a href="/after-effects-training.php">After Effects training classes</a> in  Chicago and Los Angeles, or we offer Onsite After Effects classes countrywide. Obtain a quote for <a href="/onsite-training.php">onsite Adobe After Effects training</a>.</p>
                <h4>Problem#1: Effects Look Cut-Off or Masked</h4>
                <p>One common issue people have with After Effects happens when they apply certain effects to specific layers. Depending on the effect used (frequently some type of lighting effect like CC Light Rays or Trapcode Shine) the effect appears to be "cut off" similar to the picture above. When you are adding an effect to an adjustment or an individual object layer, some effects in After Effects will exclude the Alpha or the transparency of a layer. This means that the effect will only show on the visible portion of the layer and not drift outside of it’s bounding box. The short solution to this is to Right-Click the layer before adding the effect(s) and choose PRECOMPOSE and then Move All Attributes into New Composition. You will see the transforms or bounding box of the layer expand to the full size of the composition window. Now, if you add the same effect(s) - they should extend beyond the visible part of the layer. There are other solutions but this is the most common. <a href="/after-effects-training-chicago.php">Chicago Adobe certified classes</a>.</p>
                <h4>Problem#2: My After Effects Is Slow!</h4>
                <p>A slow After Effects can be caused by so many different things. Out of all the applications Adobe creates, After Effects takes the most resources and can really push your computer to the edge which is why you want a good up-to-date computer. The following is the short list of common issues that can cause your After Effects to slow down.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/002_VIDEO_RESOULATION.jpg" alt="Enable 3D" width="839" height="268" title="After Effects is running slow"></p>
                <ul>
                    <li><strong>PREVIEWING IN AFTER EFFECTS</strong>: Remember that After Effects performs a lot of calculations which can slow down your computer. If you are <em>Previewing</em> your animation and wish to speed up the process-you can try to reduce the <em>Video Resolution</em>. One of the video resolution controls is located at the <strong>bottom of your Composition Window</strong> and the other is in your<strong> Preview Panel</strong>. <u>Each is different</u> though many people believe them to be the same thing. The Video Resolution drop down under your Composition Window controls <em>Pause Resolution,</em>which is the resolution when you are not previewing your animation. The resolution control in your Preview Panel is <em>Play Resolution</em> and can effectively lower the video resolution any time you play or preview your animation. This will make it much easier for After Effects to playback your animation because it doesn’t have to calculate or render at a high-quality setting. Both Pause and Play Resolution default at <strong>AUTO</strong> or <strong>FULL</strong> and sometimes  require you to reduce them to half or lower to get the benefit of real-time playback.</li>
                    <li><strong>AN OLDER COMPUTER</strong>: An older computer seems like an obvious issue, however, many older professional computers still run most of your software well. I would consider this option as a last resort if After Effects is consistently slow. Adobe CC takes advantage of the newer systems on the market while trying to maintain some backwards compatibility. If you wish to have After Effects run well on an older computer and operating system, <strong>install an older version</strong> of After Effects.</li>
                    <li><strong>SLOW HARD DRIVES</strong>: SSD or Solid-State Drives are the most recommended drives for running resource intensive programs like After Effects. Traditional hard drives known as HDDs have rotating elements within the drive that determine the read/write speed of the drive. Slower rotating drives like 5400rpm drives have been used by manufacturers for laptops because they produce less heat. 7200rpm drives rotate faster so the read and write capability of the hard drive is much faster than 5400rpm drives. It is not recommended to use After Effects with a 5400rpm drive. Use only 7200 or higher. Aside from this, SSD drives are still superior for both System and External Working drives.</li>
                    <li><strong>CONTROLLING YOUR CACHE</strong>: This is one that most first-time users either ignore or don’t know about. Cache can be a way of storing your previously rendered previews for faster playback. The problem is if your cache is stored on a slow hard drive-all your previews will be slow. To remedy this, most After Effects pros will re-direct their cache to a dedicated SSD drive (to understand differences between hard drives, see previous listing <em>SLOW HARD DRIVES</em>). To do this in your After Effects, go to <strong>PREFERENCES &gt; MEDIA &amp; DISK CACHE…</strong> and under <strong>DISK CACHE</strong> re-direct to an external SSD drive connected via Thunderbolt or USB 3.0/3.1.</li>
                    <li><strong>NOT ENOUGH RAM</strong>: A lot computers still pack only 8gig of RAM. Remember that your RAM or Random Access Memory is utilized by active programs and can, with your CPU, speed up functionality quite a bit. The faster the RAM, the faster applications can perform. The more RAM you have - the more multitasking your computer can accomplish. For After Effects, the recommendation is a minimum of 16gigs of RAM with a preference towards 32gigs or more.</li>
                    <li><strong>FASTER VIDEO CARD</strong>: Your video card can ease the pressure of resource intensive programs like After Effects by performing some or all of the most difficult rendering and calculations. The more powerful your video card, the more real-time effects and rendering you can potentially have. For a list of supported After Effects video cards, please check out the official list at Adobe: <a href="https://helpx.adobe.com/after-effects/system-requirements.html">https://helpx.adobe.com/after-effects/system-requirements.html</a></li>
                </ul>
                <p>Note: <em>Refer to Problem#4 for other issues that can slow down your computer.</em></p>
                <h4>Problem#3: When I Export, I Don’t See H.264</h4>
                <p>There are two primary ways to export from After Effects.</p>
                <ul>
                    <li><strong>ADD TO RENDER QUEUE </strong>which will export directly out of After Effects</li>
                    <li><strong>ADD TO ADOBE MEDIA ENCODER QUEUE</strong> which will export using Media Encoder</li>
                </ul>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/003_H264_WHERE IS IT.jpg" width="839" height="268" alt="H.264 missing"></p>
                <p><em>Add To Render Queue</em> only gives you an abbreviated list of file formats - usually the formats you would use to output to other applications or archive your animation in a high-quality format. If you did want to use H.264-you would need to select QuickTime and then make the codec H.264. Otherwise, to output to a real H.264 (MP4) for social media or other purposes-choose <em>Add To Adobe Media Encoder Queue</em>. </p>

                <h4>Problem#4: Where Is My Layer Located In My Project Panel?</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/004_LOCATINGLAYERSOURCE.jpg" width="839" height="268" alt="Missing a layer in After Effects"></p>
                <p>Losing one’s layers and where they originate from is common if you create larger projects with lots of layers. The following is a short breakdown of the most popular ways to locate the source of your media.</p>
                <ul>
                    <li><strong>LOCATE A LAYER IN PROJECT PANEL</strong>: In the Timeline, right-click any layer and select <strong>REVEAL LAYER SOURCE IN PROJECT</strong></li>
                    <li><strong>LOCATE A COMPOSITION IN PROJECT PANEL</strong>: In the Timeline, right-click any layer and select <strong>REVEAL COMPOSITION IN PROJECT</strong></li>
                    <li><strong>LOCATE A LAYER ON YOUR COMPUTER</strong>: In the Project Panel, right-click any layer and select <strong>REVEAL IN EXPLORER</strong> (Win) or <strong>REVEAL IN FINDER</strong> (Mac)</li>
                </ul>
                <p>As you can see from the picture above, there are other 'Reveal' selections - but these are the most common. Los Angeles <a href="/after-effects-training-los-angeles.php">certified Adobe After Effects training</a>.</p>

                <h4>Problem#5: My After Effects Keeps Crashing</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/After-Effects/005_AFTEREFFECTSCRASHES.jpg" width="839" height="268" alt="Why After Effectcs Keeps crashing"></p>
                <p>While it may seem unprofessional for any software application to crash-it is a fact that the more professional the application-the more potential for it to crash (with a focus on video and animation applications). &nbsp;The following is a quick breakdown of the most common causes of crashing in After Effects.</p>

                <ul>
                    <li><strong>GPU ACCELERATION/VIDEO CARD ISSUES</strong>: One of the most common causes for After Effects crashing is OpenGL and GPU Acceleration using your video card and Adobe’s Mercury Playback Engine. Now for most people, this combination is smooth and crash-free, however some may experience issues do to the many different types of computers and hardware combinations out there. If you experience After Effects crashing - you can temporarily disable Open GL and GPU acceleration to see if this remedies your problem. To do this, do the following:</li>
                    <li>Go to <strong>PREFERENCES &gt; DISPLAY</strong> and <strong>deselect</strong> <em>Hardware Accelerate Composition, Layer….</em></li>
                    <li>Go to<strong> PREFERENCES &gt; PREVIEWS</strong> and click into <em>GPU Information</em> and switch GPU to CPU</li>
                    <li><strong>CORRUPT CODECS</strong>: A common reason for After Effects crashing can be improperly installed, corrupt or malfunctioning codecs. Codecs are an essential part of coding and decoding your video files. Without them you would never be able to output a file. 3rd Party codecs for After Effects exists, however, most issues with codecs will arise through QuickTime. To remedy crashing due to codecs, you can <strong>re-install codecs or just delete them</strong>. If the codecs are natively installed through After Effects, you can always <strong>re-install After Effects</strong>. You can always look up the codec manufacturer to see if there are newer versions or updates for those codecs as well.&nbsp;</li>
                    <li><strong>VIDEO CARD DRIVERS</strong>: Old or newly updated video card drivers are common causes for crashing. To remedy this, you may have to "roll back" your newly updated drivers to an older version. If you are using older drivers, you may need to update your drivers to the latest version. Please consult your video cards manufacturer website for specific details on performing this.</li>
                    <li><strong>HARD DRIVE ISSUES</strong>: Yet another reason for application crashes can be a corrupt hard drive. Bad sectors and other issues can damage stable installs, projects and other files which can cause crashing. If this is the case, you will need to replace the hard drive (preferably with an SSD or Solid-State Drive).</li>
                    <li><strong>PLUGIN CORRUPTION OR INCOMPATIBILITIES</strong>: Old or new plugins can both cause crashing in After Effects. Typically, an incompatibility with the version of the plugin or After Effects is the cause. You must make sure that the 3rd party plugins you are using are 100% compatible with your version of After Effects. For more information, consult the plugin maker’s website. In addition, problems can also occur with a corrupted install as well. If this is the case, the you might want to try and re-install the plugin. There can also be problems between different plugins with each other. If you believe this is the case, try deactivating all but one plugin and then slowly try each plugin individually and in combinations to determine the crashing culprit.</li>
                </ul>
                <p>Unfortunately, these problems and solutions are just a few of the many issues one could potentially have. If you are lucky, you won’t run into any of these. Many problems can be eliminated merely by keeping up-to-date with the latest computer hardware, newest operating systems and all newly update software and drivers. The older your equipment and software the more likely  issues will occur. Even if your hardware is only a year or two old, it increases your chances of having problems. Anyways, good luck!</p>
            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-ae" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">After Effects training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe After Effects. We have trained hundreds of students how to use After Effects like pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=60">After Effects student testimonials</a>. </p>
                </div>
                <div class="widget widget-col-2">
                    <h4 class="widget-title">After Effects courses</h4>
                    <ul>
                        <li><a href="/after-effects/fundamentals.php">After Effects Fundamentals</a></li>
                        <li><a href="/after-effects/advanced.php">After Effects Advanced</a></li>
                        <li><a href="/after-effects/bootcamp.php">After Effects Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>