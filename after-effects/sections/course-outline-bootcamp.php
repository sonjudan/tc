<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting to Know After Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>After Effects: What it is (and is not)<u></u><u></u></li>
                                        <li>The After Effects enterprise workflow<u></u><u></u></li>
                                        <li>The power of Creative Cloud</li>
                                        <li>Leveraging your Creative Cloud subscription<u></u><u></u></li>
                                        <li>Integrating After Effects with other file formats<u></u><u></u></li>
                                        <li>Linked media: Organizing your Projects<u></u><u></u></li>
                                        <li>Tips and best practices for After Effects Projects </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 1: After Effects Basics <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Launching After Effects, and creating new projects</li>
                                        <li>Understanding and navigating the After Effects interface</li>
                                        <li>Customizing settings, Preferences, and Workspaces<u></u><u></u></li>
                                        <li>Importing and organizing footage</li>
                                        <li>Creating Compositions</li>
                                        <li>Working with Layers: The art of compositing</li>
                                        <li>Understanding Layer types</li>
                                        <li>Animating Layers with the Transform properties<u></u><u></u></li>
                                        <li>Adding, editing, and animating effects</li>
                                        <li>Understanding the Timeline and SMPTE timecode<u></u><u></u></li>
                                        <li>Optimizing performance in After Effects</li>
                                        <li>Previewing, rendering, and exporting a Composition </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 2: Working with Masks <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Alpha channels,&nbsp;Masks, mattes, and keys—oh my!</li>
                                        <li>Creating Masks with the vector Shape and Pen Tools</li>
                                        <li>Creating Bezier Masks</li>
                                        <li>Editing corner and smooth points<u></u><u></u></li>
                                        <li>Feathering the edges of a Mask</li>
                                        <li>Adding Solids to create light reflections<u></u><u></u></li>
                                        <li>Applying Mask Modes and Blend Modes<u></u><u></u></li>
                                        <li>Creating vignettes<u></u><u></u></li>
                                        <li>Tips for creating masks </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    UNIT 3: Animating Simple 2D Text <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Title design: Understanding Text layers</li>
                                        <li>Using Typekit</li>
                                        <li>Creating and formatting point and area Text</li>
                                        <li>Using Text Animation Presets</li>
                                        <li>Property animation and keyframe interpolation in the Graph Editor<u></u><u></u></li>
                                        <li>Animating by Parenting<u></u><u></u></li>
                                        <li>Converting and animating Photoshop text</li>
                                        <li>Animating with Text Animator Groups</li>
                                        <li>Animating Text on a path<u></u><u></u></li>
                                        <li>Auto-orienting Layers on a motion path<u></u><u></u></li>
                                        <li>Working with Timeline Switches and Modes<u></u><u></u></li>
                                        <li>Maximizing Motion Blur options </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    UNIT 4: Working with Shape Layers <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Creating Shape layers</li>
                                        <li>Customizing Shapes</li>
                                        <li>Shape properties and animators<u></u><u></u></li>
                                        <li>Duplicating Shapes with Repeaters</li>
                                        <li>Creating logo and text effects with Shapes</li>
                                        <li>Advanced Shape Animation&nbsp;techniques </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    UNIT 5: After Effects Dynamic Workflows  <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Packaging, prepping, and importing Illustrator files</li>
                                        <li>Working with Illustrator layers</li>
                                        <li>Packaging, prepping, and importing Photoshop files</li>
                                        <li>Managing Photoshop layers</li>
                                        <li>Dynamic Link: Integrating After Effects and Premiere Pro Projects</li>
                                        <li>MOGRT: Building Motion Graphics Templates in After Effects </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    UNIT 6:  Real-World Motion Graphics<i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Fundamentals of animation physics</li>
                                        <li>Bouncing, squashing/stretching, and more<u></u><u></u></li>
                                        <li>Leveraging velocity and speed in the Graph Editor<u></u><u></u></li>
                                        <li>Kinetic typography<u></u><u></u></li>
                                        <li>Simple Expressions: looping, conditionals, randoms<u></u><u></u></li>
                                        <li>Audio-powered animation with Expressions </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    UNIT 7: Motion Graphics for Broadcast and Social Media <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Title/Action Safe Guides<u></u><u></u></li>
                                        <li>Creating luma and alpha Track Mattes</li>
                                        <li>Creating traveling mattes<u></u><u></u></li>
                                        <li>Adding Lower Thirds<u></u><u></u></li>
                                        <li>Animating opening titles and end credits</li>
                                        <li>Batch rendering social media exports in the Adobe Media Encoder: YouTube, Instagram, and more </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    UNIT 8: 3D Motion Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>2D Text and Layers in 3D Space</li>
                                        <li>Classic 3D, Ray-Traced 3D, and CINEMA 4D renderers<u></u><u></u></li>
                                        <li>Extruding, shading, and animating 3D text</li>
                                        <li>Applying Lights to 3D Layers</li>
                                        <li>Leveraging multiple 3D views</li>
                                        <li>Animating Light and shadow properties</li>
                                        <li>After Effects and Cinema 4D Lite</li>
                                        <li>Third-party After Effects plugin roulette<u></u><u></u></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    BONUS UNIT 9: Advanced After Effects Exports <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Wrappers vs Codecs</li>
                                        <li>Lossless vs Lossy Codecs</li>
                                        <li>Mezzanine Codecs</li>
                                        <li>File formats for Visual Effects</li>
                                        <li>File conversions for Raw Files</li>
                                        <li>Working with Raw Video + Re-Indexing color</li>
                                        <li>Working with and applying LUT files</li>
                                        <li>Metadata tagging for Virtual Reality</li>
                                        <li>Batching HFR Footage</li>
                                        <li>Exporting Offline and Online&nbsp;Editorial </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    UNIT 10: Camera Workflows <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>After Effects Cameras: Not your Daddy’s DSLR<u></u><u></u></li>
                                        <li>Creating Cameras and setting Camera properties<u></u><u></u></li>
                                        <li>Camera cuts, rack focus, and other classic Camera moves</li>
                                        <li>Animating Cameras on a path</li>
                                        <li>Creating Flythroughs<u></u><u></u></li>
                                        <li>Managing Cameras, Lights, and shadow<u></u><u></u></li>
                                        <li>The power of Camera rigging<u></u><u></u></li>
                                        <li>Camera power linking </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    UNIT 11: Advanced Compositing Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding A Over B</li>
                                        <li>Blend Modes and Mask Modes</li>
                                        <li>Pre-Rendering and other techniques</li>
                                        <li>Render Order and Priorities</li>
                                        <li>Optimizing Projects: Global vs Persistent Cache</li>
                                        <li>Edges on Camera and the Real World </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    UNIT 12: Keying Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding blue and green screen chroma keys<u></u><u></u></li>
                                        <li>Tips for shooting green screen footage</li>
                                        <li>Using the Keylight effect</li>
                                        <li>Procedural mattes for the lazy</li>
                                        <li>Hi-con mattes</li>
                                        <li>Key Cleaner for poorly-lit footage</li>
                                        <li>Linear Keying effects<u></u><u></u></li>
                                        <li>Third Party Keying Suites </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    UNIT 13: Stabilization and Tracking <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About stabilization and tracking<u></u><u></u></li>
                                        <li>Using the Warp Stabilizer VFX feature</li>
                                        <li>Single-point and multi-point motion tracking</li>
                                        <li>Motion tracking with Mocha planar data</li>
                                        <li>Other tracking techniques</li>
                                        <li>3D Camera tracking<u></u><u></u></li>
                                        <li>Shadow catchers</li>
                                        <li>Null objects </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    UNIT 14: The Art of Rotoscoping <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About rotoscoping</li>
                                        <li>Classic rotoscoping workflows</li>
                                        <li>Using the Roto Brush Tool</li>
                                        <li>Creating and refining Roto Brush segmentation boundaries</li>
                                        <li>Using the Refine Edge Tool</li>
                                        <li>Freezing and unfreezing Roto Brush data<u></u><u></u></li>
                                        <li>Articulated Mattes<u></u><u></u></li>
                                        <li>Refining Mattes: Feather, Track<u></u><u></u></li>
                                        <li>The legacy Paint Tools: painting, cloning, and erasing<u></u><u></u></li>
                                        <li>Roto like a Jedi:&nbsp;Hot core energy effects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                    UNIT 15: Particle Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About particle simulation<u></u><u></u></li>
                                        <li>After Effects' particle generators</li>
                                        <li>The Particle Playground effect<u></u><u></u></li>
                                        <li>The CC Particle Systems II effect<u></u><u></u></li>
                                        <li>Customizing particle properties</li>
                                        <li>Additional particle effects and techniques</li>
                                        <li>Popular third-party particle effects </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
                                    UNIT 16: Color Correction and Color Grading <i></i>
                                </a>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Enterprise-based color workflows</li>
                                        <li>Histograms and Channels: Using the Lumetri scopes<u></u><u></u></li>
                                        <li>Using the Lumetri Color Effect and the Lumetri Color panel<u></u><u></u></li>
                                        <li>Adjusting levels and exposure</li>
                                        <li>Adjusting color balance and saturation</li>
                                        <li>Legalizing Luma/Color for Broadcast</li>
                                        <li>Additional color correction options</li>
                                        <li>Cinematic color grading </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse17">
                                    BONUS TRACKS UNIT 17: Puppet Tools Distortions <i></i>
                                </a>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About the Puppet Tools</li>
                                        <li>Adding Deform pins</li>
                                        <li>Animating pin positions</li>
                                        <li>Stiffening with the Starch Tool<u></u><u></u></li>
                                        <li>Recording deformations</li>
                                        <li>Acting it out with Adobe Character Animator </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse18">
                                    UNIT 18: Advanced Color Options <i></i>
                                </a>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding HDR (High Dynamic Range)</li>
                                        <li>Linear HDR Compositing: Lifelike</li>
                                        <li>Linear LDR Compositing</li>
                                        <li>Color Management and LUTs</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>