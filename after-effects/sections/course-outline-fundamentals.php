<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting to Know After Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>After Effects: What it is (and is not)<u></u><u></u></li>
                                        <li>The After Effects enterprise workflow<u></u><u></u></li>
                                        <li>The power of Creative Cloud</li>
                                        <li>Leveraging your Creative Cloud subscription<u></u><u></u></li>
                                        <li>Integrating After Effects with other file formats<u></u><u></u></li>
                                        <li>Linked media: Organizing your Projects<u></u><u></u></li>
                                        <li>Tips and best practices for After Effects Projects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 1: After Effects Basics <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Launching After Effects, and creating new projects</li>
                                        <li>Understanding and navigating the After Effects interface</li>
                                        <li>Customizing settings, Preferences, and Workspaces<u></u><u></u></li>
                                        <li>Importing and organizing footage</li>
                                        <li>Creating Compositions</li>
                                        <li>Working with Layers: The art of compositing</li>
                                        <li>Understanding Layer types</li>
                                        <li>Animating Layers with the Transform properties<u></u><u></u></li>
                                        <li>Adding, editing, and animating effects</li>
                                        <li>Understanding the Timeline and SMPTE timecode<u></u><u></u></li>
                                        <li>Optimizing performance in After Effects</li>
                                        <li>Previewing, rendering, and exporting a Composition</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    UNIT 2: Working with Masks  <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Alpha channels, Masks, mattes, and keys - oh my!</li>
                                        <li>Creating Masks with the vector Shape and Pen Tools</li>
                                        <li>Creating Bezier Masks</li>
                                        <li>Editing corner and smooth points<u></u><u></u></li>
                                        <li>Feathering the edges of a Mask</li>
                                        <li>Adding Solids to create light reflections<u></u><u></u></li>
                                        <li>Applying Mask Modes and Blend Modes<u></u><u></u></li>
                                        <li>Creating vignettes<u></u><u></u></li>
                                        <li>Tips for creating masks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    UNIT 3: Animating Simple 2D Text  <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Title design: Understanding Text layers</li>
                                        <li>Using Typekit</li>
                                        <li>Creating and formatting point and area Text</li>
                                        <li>Using Text Animation Presets</li>
                                        <li>Property animation and keyframe interpolation in the Graph Editor<u></u><u></u></li>
                                        <li>Animating by Parenting<u></u><u></u></li>
                                        <li>Converting and animating Photoshop text</li>
                                        <li>Animating with Text Animator Groups</li>
                                        <li>Animating Text on a path<u></u><u></u></li>
                                        <li>Auto-orienting Layers on a motion path<u></u><u></u></li>
                                        <li>Working with Timeline Switches and Modes<u></u><u></u></li>
                                        <li>Maximizing Motion Blur options</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    UNIT 4: Working with Shape Layers <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Creating Shape layers</li>
                                        <li>Customizing Shapes</li>
                                        <li>Shape properties and animators<u></u><u></u></li>
                                        <li>Duplicating Shapes with Repeaters</li>
                                        <li>Creating logo and text effects with Shapes</li>
                                        <li>Advanced Shape Animation techniques</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    UNIT 5:  After Effects Dynamic Workflows <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Packaging, prepping, and importing Illustrator files</li>
                                        <li>Working with Illustrator layers</li>
                                        <li>Packaging, prepping, and importing Photoshop files</li>
                                        <li>Managing Photoshop layers</li>
                                        <li>Dynamic Link: Integrating After Effects and Premiere Pro Projects</li>
                                        <li>MOGRT: Building Motion Graphics Templates in After Effects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    UNIT 6:  Real-World Motion Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Fundamentals of animation physics</li>
                                        <li>Bouncing, squashing/stretching, and more</li>
                                        <li>Leveraging velocity and speed in the Graph Editor</li>
                                        <li>Kinetic typography</li>
                                        <li>Simple Expressions: looping, conditionals, randoms</li>
                                        <li>Audio-powered animation with Expressions</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    UNIT 7: Motion Graphics for Broadcast and Social Media <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Title/Action Safe Guides</li>
                                        <li>Creating luma and alpha Track Mattes</li>
                                        <li>Creating traveling mattes</li>
                                        <li>Adding Lower Thirds</li>
                                        <li>Animating opening titles and end credits</li>
                                        <li>Batch rendering social media exports in the Adobe Media Encoder: YouTube, Instagram, and more</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    UNIT 8: 3D Motion Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>2D Text and Layers in 3D Space</li>
                                        <li>Classic 3D, Ray-Traced 3D, and CINEMA 4D renderers</li>
                                        <li>Extruding, shading, and animating 3D text</li>
                                        <li>Applying Lights to 3D Layers</li>
                                        <li>Leveraging multiple 3D views</li>
                                        <li>Animating Light and shadow properties</li>
                                        <li>After Effects and Cinema 4D Lite</li>
                                        <li>Third-party After Effects plugin roulette</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                    BONUS UNIT 9: Advanced After Effects Exports <i></i>
                                </a>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Wrappers vs Codecs</li>
                                        <li>Lossless vs Lossy Codecs</li>
                                        <li>Mezzanine Codecs</li>
                                        <li>File formats for Visual Effects</li>
                                        <li>File conversions for Raw Files</li>
                                        <li>Working with Raw Video + Re-Indexing color</li>
                                        <li>Working with and applying LUT files</li>
                                        <li>Metadata tagging for Virtual Reality</li>
                                        <li>Batching HFR Footage</li>
                                        <li>Exporting Offline and Online Editorial  </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>