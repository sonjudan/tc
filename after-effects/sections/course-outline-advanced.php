<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 1: Camera Workflows <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>After Effects Cameras: Not your Daddy’s DSLR</li>
                                        <li>Creating Cameras and setting Camera properties</li>
                                        <li>Camera cuts, rack focus, and other classic Camera moves</li>
                                        <li>Animating Cameras on a path</li>
                                        <li>Creating Flythroughs</li>
                                        <li>Managing Cameras, Lights, and shadow</li>
                                        <li>The power of Camera rigging</li>
                                        <li>Camera power linking</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 2: Advanced Compositing Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding A Over B</li>
                                        <li>Blend Modes and Mask Modes</li>
                                        <li>Pre-Rendering and other techniques</li>
                                        <li>Render Order and Priorities</li>
                                        <li>Optimizing Projects: Global vs Persistent Cache</li>
                                        <li>Edges on Camera and the Real World</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 3: Keying Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding blue and green screen chroma keys</li>
                                        <li>Tips for shooting green screen footage</li>
                                        <li>Using the Keylight effect</li>
                                        <li>Procedural mattes for the lazy</li>
                                        <li>Hi-con mattes</li>
                                        <li>Key Cleaner for poorly-lit footage</li>
                                        <li>Linear Keying effects</li>
                                        <li>Third Party Keying Suites</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                     UNIT 4: Stabilization and Tracking <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About stabilization and tracking</li>
                                        <li>Using the Warp Stabilizer VFX feature</li>
                                        <li>Single-point and multi-point motion tracking</li>
                                        <li>Motion tracking with Mocha planar data</li>
                                        <li>Other tracking techniques</li>
                                        <li>3D Camera tracking</li>
                                        <li>Shadow catchers</li>
                                        <li>Null objects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    UNIT 5: The Art of Rotoscoping <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About rotoscoping</li>
                                        <li>Classic rotoscoping workflows</li>
                                        <li>Using the Roto Brush Tool</li>
                                        <li>Creating and refining Roto Brush segmentation boundaries</li>
                                        <li>Using the Refine Edge Tool</li>
                                        <li>Freezing and unfreezing Roto Brush data</li>
                                        <li>Articulated Mattes</li>
                                        <li>Refining Mattes: Feather, Track</li>
                                        <li>The legacy Paint Tools: painting, cloning, and erasing</li>
                                        <li>Roto like a Jedi: Hot core energy effects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    UNIT 6: Particle Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About particle simulation</li>
                                        <li>After Effects' particle generators</li>
                                        <li>The Particle Playground effect</li>
                                        <li>The CC Particle Systems II effect</li>
                                        <li>Customizing particle properties</li>
                                        <li>Additional particle effects and techniques</li>
                                        <li>Popular third-party particle effects</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    UNIT 7: Color Correction and Color Grading <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Enterprise-based color workflows</li>
                                        <li>Histograms and Channels: Using the Lumetri scopes</li>
                                        <li>Using the Lumetri Color Effect and the Lumetri Color panel</li>
                                        <li>Adjusting levels and exposure</li>
                                        <li>Adjusting color balance and saturation</li>
                                        <li>Legalizing Luma/Color for Broadcast</li>
                                        <li>Additional color correction options</li>
                                        <li>Cinematic color grading</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse08" aria-expanded="true" aria-controls="collapse1">
                                    BONUS TRACKS UNIT 8: Puppet Tools Distortions <i></i>
                                </a>
                            </div>
                            <div id="collapse08" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>About the Puppet Tools</li>
                                        <li>Adding Deform pins</li>
                                        <li>Animating pin positions</li>
                                        <li>Stiffening with the Starch Tool</li>
                                        <li>Recording deformations</li>
                                        <li>Acting it out with Adobe Character Animator</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse09" aria-expanded="true" aria-controls="collapse1">
                                    UNIT 9: Advanced Color Options <i></i>
                                </a>
                            </div>
                            <div id="collapse09" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding HDR (High Dynamic Range)</li>
                                        <li>Linear HDR Compositing: Lifelike</li>
                                        <li>Linear LDR Compositing</li>
                                        <li>Color Management and LUTs</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>