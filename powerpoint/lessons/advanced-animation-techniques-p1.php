<?php
$meta_title = "Advanced Animation Techniques in Powerpoint Part One | Training Connection";
$meta_description = "Learn how to use advanced animation techniques in Microsoft PowerPoint. This lesson forms part of our overall PowerPoint Training Course, available in both Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced Animation Techniques Part One</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Advanced Animation Techniques, Part One</h1>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will introduce you to advanced animation techniques and how to use them in Microsoft PowerPoint.</p>
                <p>For classroom based <a href="/ms-powerpoint-training.php">Mircosoft PowerPoint classroom training</a> available in both Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <p>In this Article, we will learn how to:</p>
                <ul>
                    <li>Choose a basic animation effect</li>
                    <li>Set animation effect options</li>
                    <li>Customize the trigger for an animation</li>
                </ul>

                <h4>Choosing a Basic Effect</h4>
                <p>To apply an effect to a specific object in your presentation, you first need to select the object in question. For this example, click on the image on the second slide of the sample presentation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-object.jpg" alt="select object to apply effect"></p>

                <p>Next, click the Animations tab. Examine the various animations that are available inside the Animation gallery. Click the More arrow () to expand the entire gallery:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animations-tab.jpg" alt="click animation tab"></p>

                <p>As you can see, there are plenty of animations to choose from. At the bottom of the gallery, you will find commands to open various dialogs with a complete list of each type of animation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-gallery.jpg" alt="animation gallery dialog box"></p>

                <p>For this example, click outside of the gallery to close it. Then, click Fade:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-fade.jpg" alt="click fade"></p>

                <p>The effect will be applied to the selected object and immediately previewed. Save the changes that you have made to the presentation and close Microsoft PowerPoint 2013. <a href="/ms-powerpoint-training-los-angeles.php">PowerPoint 2013/2016/2019 training in Los Angeles</a>.</p>

                <h4>Setting Effect Options</h4>
                <p>Some animation effects include options that allow you to customize how they operate. To start, select the object in question. For this example, select the image on the second slide in the sample presentation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-object-to-customize.jpg" alt="Customizable options for object"></p>
                <p>Next, click Animations → Effect Options:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-effect-options.jpg" alt="click effect options"></p>
                <p>The options in this menu will be different depending upon the type of effect that has been applied. In this case, the Fly In effect has been applied to the selected picture, so the options listed here control the direction of this effect. Click From Left:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/options-menu.jpg" alt="options available in menu"></p>
                <p>The new effect option will be applied and previewed. Save the changes that you have made to this presentation and close Microsoft PowerPoint 2013.</p>

                <h4>Customizing the Trigger</h4>
                <p>In PowerPoint, you have the ability to set a special start condition for effects so that they run after you click an object. To view and apply these options, you first need to select an object that has an effect applied to it.</p>
                <p>For this example, select the picture on the second slide in the sample presentation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-sample-presentation.jpg" alt="second slide selected"></p>
                <p>Next, click Animations → Trigger:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-trigger.jpg" alt="click animation trigger"></p>
                <p>The first option in the drop-down menu is “On Click Of.” Click this option to see objects that you can choose from:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/on-click-of.jpg" alt="on click of to see object"></p>
                <p>In this case, choosing either object will mean that you will have to click on the selected object in order for the animation to start. You can also have the animation play when you reach a bookmark.</p>

                <p>See <a href="/powerpoint/lessons/advanced-animation-techniques-p2.php">Advanced Animation Techniques, Part Two</a></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>We have thousands of satisfied student who train with us every year. Take a look at our <a href="/testimonials.php?course_id=21">PowerPoint past student review page</a> and find out what our students have to say.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>