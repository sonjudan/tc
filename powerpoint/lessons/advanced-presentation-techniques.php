<?php
$meta_title = "Advanced Presentation Techniques in PowerPoint | Training Connection";
$meta_description = "Learn how to use advanced presentation techniques in PowerPoint. This lesson forms an element of our overall PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced Presentation Techniques</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Advanced Presentation Techniques</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will demostrate how to use Advanced Presentation Techniques with a simple, easy to follow set-by-set format.</p>

                <p>For instructor-led <a href="/ms-powerpoint-training.php">MS PowerPoint training</a> held in a classroom based environment, call us on 888.815.0604. Courses are available in Chicago and Los Angeles as well as <a href="/onsite-training.php">Onsite PowerPoint training</a> available countrywide.</p>

                <h4>Recording and Narrating a Show in PowerPoint</h4>
                <p>In PowerPoint 2013, you have the ability to record timings, narrations, and laser pointer gestures in a single package. To do this, click Slide Show → Record Slide Show (not the drop-down arrow):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/record-slide-show.jpg" alt="slide show options"></p>
                <p>When you click this command, you will be prompted to choose what to record. Leave the default settings unchanged and click Start Recording:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/choose-recording.jpg" alt="default recording settings"></p>
                <p>(If you do not have a microphone, the “Narrations and laser pointer” checkbox will be disabled. If so, that’s OK.) <a href="/ms-powerpoint-training-los-angeles.php">The best PowerPoint training in Los Angeles</a>.</p>

                <p>Your presentation will now begin, with the Recording toolbar in the upper left-hand corner of your screen. Using the controls on this toolbar, you can pause the recording, advance to the next slide, and see the current duration of the presentation. You may now run through the presentation as you would normally. If you have a microphone, you can also record a narration:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/recording-toolbar.jpg" alt="use controls on recording toolbar"></p>
                <p>To use your mouse as a laser pointer during a presentation, hold down the Ctrl key, press the left mouse button, and move the mouse:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/use-cursor-laser.jpg" alt="use mouse as laser pointer"></p>
                <p>(This action will only be recorded if you checked the “Narrations and laser pointer” checkbox in the Record Slide Show dialog.) To finish recording your show, complete the presentation by advancing past the last slide or pressing Esc on your keyboard. Click Yes when you are prompted to save the slide timings:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/narration-laser-pointer.jpg" alt="check narrations and laser pointer"></p>
                <p>Review the recording you created by clicking Slide Show → From Beginning:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/review-recording" alt="review recording from beginning"></p>
                <p>Your recording will now play.</p>

                <h4>Setting Narration Options</h4>
                <p>To begin, open Module 10-2 using Microsoft PowerPoint 2013.</p>
                <p>To view and change narration options, click Slide Show → Set Up Slide Show:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/change-narration-options.jpg" alt="view narration option"></p>
                <p>This action will open the Set Up Show dialog:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/set-up-show.jpg" alt="set-up-show-dialog"></p>
                <p>In this dialog, you will see numerous options that allow you to customize how your show is presented. Take a moment to review all of the options listed. Click Cancel to return to the primary PowerPoint 2013 window.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>We have thousands of happy students who undertake the PowerPoint Training every year. Find our what students think of our MS PowerPoint training course on the <a href="/testimonials.php?course_id=21">PowerPoint testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>