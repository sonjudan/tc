<?php
$meta_title = "Using Slide Masters in Powerpoint Part Four | Training Connection";
$meta_description = "Learn how to use slide masters in Microsoft PowerPoint with part four of our tutorial. This lesson is part of our MS PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Use Slide Masters Part Four</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Slide Masters, Part Four</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article is the forth part of our tutorial detailing how to use Slide Masters in Microsoft PowerPoint.</p>
                <p>For dedicated <a href="/ms-powerpoint-training.php">PowerPoint training classes</a> delivered by instructors in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <p>In this Article, we will learn how to:</p>
                <ul>
                    <li>Use multiple slide masters in a presentation</li>
                    <li>Remove masters from a presentation</li>
                </ul>
                <h4>Using Multiple Slide Masters in a Presentation</h4>
                <p>If a presentation contains multiple sets of slide masters, the Layout menu will look a little bit different. However, the process of changing the layout is the same. First, select the slide(s) to which you want to apply the master. For this example, ensure that slide one is selected in the Slides pane:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-slides-to-apply-master.jpg" alt="Click Slides To Apply Layout"></p>
                <p>Next, click Home → Layout:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/click-layout-p4.jpg" alt="From Home Menu Select Layout"></p>
                <p><a href="/ms-powerpoint-training-chicago.php">Instructor-led PowerPoint classes</a> in Chicago.</p>
                <p>You can see that this presentation contains two different sets of slide masters that are organized by category. For this example, click Title Slide from the Ion category:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/click-title-slide-p4.jpg" alt="Select Title Slide"></p>
                <p>The selected slide master will now be applied to the selected slide:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/master-slide-applied-p4.jpg" alt="Master Slide Applied to Slide Layout"></p>
                <p>Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>
                <h4>Removing Masters from a Presentation</h4>
                <p>Open Slide Master view by clicking View → Slide Master:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/open-slide-master-to-remove-masters.jpg" alt="Remove Master By Opening Slide Master"></p>

                <p>If you do not need a particular slide master or layout, you can remove it entirely by deleting it. Note that you cannot delete a slide master or layout that is currently in use. <a href="/ms-powerpoint-training-los-angeles.php">Click here</a> for more on Los Angeles PowerPoint training.</p>
                <p>Let’s delete the Ion slide master from the current presentation. Select its top-level slide in the Slides pane:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/delete-ion-slide.jpg" alt="Ion Slide Master Delete"></p>
                <p>Next, click Slide Master → Delete:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide-master-delete-p4.jpg" alt="Click Delete Under Slide Master"></p>
                <p>The entire master set will now have been deleted from the current presentation. Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>
            </div>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Take a look at our trusted student testimonials at our <a href="/testimonials.php?course_id=21"> PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>