<?php
$meta_title = "First PowerPoint Presentation | Training Connection";
$meta_description = "Learning how to create your every first PowerPoint presentation. This lesson is part of our Level 1 PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">First PowerPoint Presentation</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Your First PowerPoint Presentation</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article,  you will be shown how to create your first presentation. Presentations consist of slides. Each slide can contain text, graphics, animations, and more. You’ll learn how to add slides, use the content placeholders, and add text to your presentation. This module also covers using the Slides tab, which allows you to change the order of your slides. </p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">Microsoft PowerPoint classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Adding Slides</h4>

                <p>To add a new default (or last added) type of slide, use the following procedure.</p>

                <ol>
                    <li>Click the <strong>New Slide</strong> button on the <strong>Home</strong> tab of the Ribbon.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-slide-button.jpg" width="750" height="140" alt="Adding Slides Button"></p>

                <p>To add a new slide with a different layout, use the following procedure.</p>

                <ol>
                    <li>Select the arrow next to the <strong>New Slide</strong> tool.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-slide-tool.jpg" width="496" height="630" alt="New Slide Tool - Microsoft PowerPoint"></p>

                <ol start="2">
                    <li>Select the type of slide you want to add.</li>
                </ol>

                <h4>Using the Content Place Holder</h4>
                <p>Review the dialog box for the content placeholder for a table. <a href="/ms-powerpoint-training-chicago.php">Microsoft PowerPoint 365 Training in Chicago</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-table.jpg" width="162" height="98" alt="Insert a Table"></p>

                <p>Review the dialog box for the content placeholder for a chart.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-chart.jpg" width="525" height="485" alt="Insert Chart"> </p>

                <p>Review the dialog box for the content placeholder for a SmartArt Graphic.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-smartgraphic.jpg" width="593" height="317" alt="Insert Smart Objects"></p>

                <p>Review the dialog box for the content placeholder for a picture.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-image.jpg" width="546" height="384" alt="Insert pictures"></p>

                <p>Review the dialog box for the content placeholder for Online Pictures. <a href="/ms-powerpoint-training-los-angeles.php">LA-based Powerpoint 2019 classes</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-online-image.jpg" width="557" height="352" alt="Insert online pictures">  </p>

                <p>Review the dialog box for the content placeholder for a Video Clip.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/content-placeholder-video.jpg" width="502" height="317" alt="Insert Video Clip"></p>

                <h4>Adding Text</h4>
                <p>The placeholders in a blank presentation.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/text-placeholders.jpg" width="750" height="385" alt="Blank Presentation"></p>

                <p>To enter text in a placeholder, use the following procedure.</p>
                <ol>
                    <li>Click on a placeholder to make it active.</li>
                    <li>Type your text.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide1.jpg" width="750" height="414" alt="Enter text placeholder"> </p>

                <h4>Using the Slides Tab</h4>
                <p>To view a different slide, use the following procedure.</p>

                <ol>
                    <li>Click on the slide thumbnail in the Slides tab to view it in the Presentation window.</li>
                </ol>

                <p>To rearrange slides using the Slides tab, use the following procedure.</p>

                <ol>
                    <li>Select the thumbnail of the slide you want to move.</li>
                    <li>Hold the left mouse pointer down.</li>
                    <li>Move the cursor to the location where you want to move the slide. The cursor has an arrow and a small box to indicate that you are moving text. </li>
                    <li>Let the mouse button go when the cursor is in the desired location.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/highlighted-slide.jpg" width="750" height="423" alt="Highlighted Slide"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related PowerPoint Lessons</h4>
                    <ul>
                        <li><a href="/powerpoint/lessons/interface.php">Getting to know the PowerPoint Interface</a></li>
                        <li><a href="/powerpoint/lessons/working-with-text.php">Working with PowerPoint Text</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students...you can read some reviews at the following page -&gt;<a  href="/testimonials.php?course_id=21">PowerPoint testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>