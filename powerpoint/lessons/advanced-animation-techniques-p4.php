<?php
$meta_title = "Advanced Animation Techniques in Powerpoint Part Four | Training Connection";
$meta_description = "Further your skills with advanced animation techniques in Microsoft PowerPoint part four . This lesson forms part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced Animation Techniques Part Four</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Advanced Animation Techniques, Part Four</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Continuing on with advanced animation techniques, this article concludes editing options available for animation within Microsoft PowerPoint.</p>

                <p>For <a href="/ms-powerpoint-training.php">Microsoft PowerPoint training classes</a> led by a knowledgeable instructors in a classroom based setting, call us on 888.815.0604. Courses available in both Chicago and Los Angeles as well as <a href="/onsite-training.php">Onsite PowerPoint training</a> available countrywide.</p>
                <p>In this article, we will learn how to:</p>

                <ul>
                    <li>Use the Animation Painter</li>
                    <li>Use the Animation pane</li>
                </ul>

                <h2>Using the Animation Painter</h2>
                <p>If you’ve used the Format Painter before, the Animation Painter is similar: it lets you copy animations from one object to another. This can be really useful if you have created a custom animation and want to apply it to other objects.</p>
                <p><a href="/ms-powerpoint-training-chicago.php">MS PowerPoint workshops in Chicago</a>.</p>
                <p>To use the Animation Painter, first click on the object that has the animation(s) that you want to copy. For this example, click on the title of the first slide to select it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-painter.jpg" alt="click to use animation painter"></p>
                <p>Next, click Animations → Animation Painter:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-animation-painter.jpg" alt="animation painter selected"></p>
                <p>
                    With the Animation Painter now activated, your cursor will change to include a paintbrush
                    <img class="d-inline" src="https://www.trainingconnection.com/images/Lessons/powerpoint/paintbrush-icon.jpg" alt="paintbrush icon displayed">.
                </p>

                <p>Use this cursor to click on the object that you would like to animate. To continue this example, click on the quote on the third slide:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/cursor-paintbrush.jpg" alt="cursor changes to paintbrush"></p>
                <p>Immediately, the animation you copied will be applied to the selected object. A brief preview of this animation will also be shown. The Animation Painter will then automatically be disabled. <a href="/ms-powerpoint-training-los-angeles.php">Los Angeles PowerPoint course</a>.</p>

                <h2>Using the Animation Pane</h2>
                <p>To open the Animation pane, click Animations → Animation Pane:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-pane-p4.jpg" alt="animation pane displayed"></p>
                <p>The pane will open on the right hand-side of the PowerPoint 2013 window and list all of the various animations on the current slide. With the first slide selected in the sample presentation, you can see one animation listed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/pane-animation-options.jpg" alt="animation options"></p>
                <p>The colored bars beside each animation represent the length of time that the animation will last. Clicking and dragging these bars will adjust these values:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/coloured-bars.jpg" alt="adjust time values"></p>
                <p>At the top of the pane, the Play From command will play all of the animations on the current slide from the current one. The upward and downward facing arrows allow you to change the order in which animations appear:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/play-from-command.jpg" alt="change order animations"></p>
                <p>At the bottom of the pane, you can zoom in or out of the timeline with the Seconds menu, which will change the appearance of the time bars. You can also use the arrows to scroll through the timeline:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/zoom-function.jpg" alt="zoom options"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Find our what students think of our PowerPoint training course on the <a href="/testimonials.php?course_id=21">Microsoft PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>