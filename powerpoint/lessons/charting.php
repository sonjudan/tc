<?php
$meta_title = "PowerPoint 2013 Charts | Training Connection";
$meta_description = "Learning how to insert, edit and format charts in Microsoft PowerPoint. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Charts</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Charting in PowerPoint</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will show you step-by-step how to add charts in Microsoft PowerPoint 2013, how to edit them. change the chart type, and change the chart attributes.</p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">PowerPoint classes</a> in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> available countrywide.</p>

                <h4>How to insert a Chart</h4>

                <ol>
                    <li>Create a new blank slide.</li>
                    <li>
                        <p>Click on the Chart icon From the Title and Content layout or choose Insert Chart from the Illustrations Group of the Insert Tab.</p>
                        <img src="https://www.trainingconnection.com/images/Lessons/powerpoint/add-a-chart.jpg" width="384" height="352" alt="Add Chart Icon">
                    </li>
                    <li>Choose the Chart Layout you require, click OK.</li>
                    <li>A  Microsoft Excel will Chart will open on the Slide.&nbsp;Excel contains the data source used by the chart. Enter the data you require into this chart and then close the Excel Chart.                        <br>
                      <br>
                      <img src="https://www.trainingconnection.com/images/Lessons/powerpoint/bar-chart.jpg" width="780" height="422" alt="Add a bar chart">
                      <br>
                    </li>
                    <li>When a chart is selected in PowerPoint the Chart Tools Tab will appear in the Ribbon. These 2 tabs contain all controls needed for the design and formatting of charts
                        <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/format-tab.jpg" width="780" height="78" alt="Chart formating chart"></p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/design-tab.jpg" width="780" height="91" alt="Chart Design tab"></p>
                    </li>
                </ol>

                <p><a href="https://www.gcflearnfree.org/powerpoint2016/charts/1/">Read more about PowerPoint 2016 Charts</a></p>

                <h4>Editing the Chart</h4>

                <p>In order to change the chart you must have the Chart Tools Ribbons active.&nbsp; You can change the look of the chart at any time by using the icons on the Chart Tools Ribbons. <a href="/ms-powerpoint-training-chicago.php">Best PowerPoint training in Chicago</a>.&nbsp; </p>

                <h4>Changing the Chart Type</h4>

                <ol>
                    <li>Activate the Chart by clicking inside the border.</li>
                    <li>On the Chart Tools, Design Tab change the Chart Layout and colour scheme if applicable. </li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/chart-title.jpg" width="352" height="460" alt="Changing the chart type"></p>

                <h4>How to Change Chart Attributes</h4>
                <ol>
                    <li>Each element of the chart can be selected and changed, for example the chart Title area or the chart colours can be accessed under the Design menu.</li>
                    <li>
                        <p>Select the dropdown arrow next to Add Chart Element. Here you can change where the Chart Title is located.&nbsp;</p>
                        <img src="https://www.trainingconnection.com/images/Lessons/powerpoint/change-colors.jpg" width="260" height="480" alt="Changing the color attribute">
                    </li>
                </ol>




            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=21">PowerPoint testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>