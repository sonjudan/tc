<?php
$meta_title = "Using Slide Masters in Powerpoint Part Three | Training Connection";
$meta_description = "Learn how to use slide masters in Microsoft PowerPoint with part three of our tutorial. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Use Slide Masters Part Three</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Slide Masters, Part Three</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article is the third part of our tutorial detailing how to use Slide Masters in Microsoft PowerPoint.</p>
                <p>For <a href="/ms-powerpoint-training-chicago.php">PowerPoint training classes</a> delivered by instructors in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>
                <p>In this Article, we will learn how to:</p>

                <ul>
                    <li>Preserve slide masters</li>
                    <li>Update master slides</li>
                </ul>

                <h4>Preserving Slide Masters</h4>
                <p>Open Slide Master view by clicking View → Slide Master:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/open-slide-master-p3.jpg" alt="Slide Master View Part 3"></p>
                <p>There are some circumstances in which PowerPoint will automatically delete a slide master. This can happen if the slides in the master are deleted or if another layout is applied to those slides. To prevent this from happening, you can preserve slide masters so that they are always saved even if they are not in use. <a href="/ms-powerpoint-training-los-angeles.php">MS PowerPoint 365 training in Los Angeles</a>.</p>

                <p>For this example, select the top-level slide master from the Slides pane:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/top-level-slide-master-p3.jpg" alt="Top Level Slide Master Part Three"></p>
                <p>Next, click Slide Master → Preserve:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide-master-preserve-p3.jpg" alt="Slide Master Preserve Part Three"></p>
                <p>The slide master will now be preserved. You can verify this by looking for the small pin icon </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/pin-icon.jpg" alt="small pin icon"></p> that is displayed to the left of the slide in the Slides pane:<p></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/pin-icon-display.jpg" alt="Pin Icon Displayed"></p>
                <p>To toggle this command off, click Slide Master → Preserve again. Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>
                <h4>Updating Master Slides</h4>
                <p>Open Slide Master view by clicking View → Slide Master:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide-master-open-p3.jpg" alt="Open Slide Master View"></p>
                <p>Slide masters are especially useful for making updates quickly and easily. For example, let’s say that you need to change all of the title text in the current presentation. Rather than having to change it all manually directly in the presentation, you can make those changes in the slide master. These changes will then propagate to the presentation automatically. <a href="/ms-powerpoint-training-chicago.php">Small PowerPoint training classes in Chicago</a>.</p>

                <p>Click the top-level slide master for this presentation. Select the header text:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-header-text-p3.jpg" alt="Header Text Selected"></p>
                <p>Change this font type by clicking Home → Font → Andalus:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/change-font-type-p3.jpg" alt="Change Font Type"></p>
                <p>Notice that the rest of the slide layouts update with this change. Now, close Slide Master view by clicking Slide Master → Close Master View:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/close-slide-master-view-p3.jpg" alt="Click Slide Master To Close"></p>
                <p>Back in the normal view of the presentation, you will see that the title text throughout your presentation has been changed to Andalus:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/view-font-changes-p3.jpg" alt="See Font Changes In Presentation"></p>
                <p>Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>
                <p>See <a href="/powerpoint/lessons/using-slide-masters-part-four.php">Using Slide Masters Part 4</a></p>
            </div>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>View our trusted student testimonials at our <a href="/testimonials.php?course_id=21">MS PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>