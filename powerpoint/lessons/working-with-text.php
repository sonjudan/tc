<?php
$meta_title = "Working with Text in PowerPoint | Training Connection";
$meta_description = "Select Add, edit,copy and paste text, pictures, video into a PowerPoint. This lesson is part of our Level 1 PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">PowerPoint Text</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Text in PowerPoint</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>The PowerPoint 2013 editing tools make editing your presentation a breeze. This article covers how to work with text, including selecting, editing, deleting, cutting, copying, and pasting. It also explains how to use the Office Clipboard. You’ll learn how to use undo and redo and how to find and replace text, such as when you want to change a PowerPoint or phrase throughout your presentation.</p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">PowerPoint training classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Selecting, Editing, and Deleting Text</h4>

                <p>To use the keyboard to select text, use the following procedure.</p>

                <ol>
                    <li>Using the arrow keys, place the cursor either at the beginning of the text you want to select, or at the end of the text you want to select.</li>
                    <li>Hold down the shift key while pressing the arrow key to select text in that direction.</li>
                </ol>

                <p>The selected text is highlighted.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/highlighted-text.jpg" width="641" height="330" alt="highlight text to copy"></p>

                <p>To use the mouse to select text, use the following procedure.</p>

                <ol>
                    <li>Point the mouse to either the beginning or the end of the text you want to select.</li>
                    <li>Hold the left mouse button down.</li>
                    <li>Move the mouse to select the text. You can move left, right, up and/or down. </li>
                    <li>Let the mouse button up when you have finished selecting the text.</li>
                </ol>

                <p>Review the mouse shortcuts for selecting text. </p>

                <ul>
                    <li>You can double click on a word to select it.</li>
                    <li>You can click three times on a paragraph to select the whole paragraph.</li>
                    <li>You can press Shift while clicking to add to your selection. The selections must be next to each other.</li>
                    <li>You can press Control while clicking to add non-congruent text to your selection.</li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/selecting-multiple-text.jpg" width="557" height="209" alt="Multiple selections of text"></p>

                <p>To cut and paste text, use the following procedure.</p>

                <ol>
                    <li>Highlight the text you want to cut.</li>
                    <li>Right click the mouse to display the context menu and select cut.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/cut-text.jpg" width="528" height="392" alt="Cutting text"></p>

                <ol start="3">
                    <li>Move the cursor to the new location. It can be on the same slide or a different slide.</li>
                    <li>Right click the mouse to display the context menu and select the Keep Text Only paste option, as illustrated below.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/paste-text.jpg" width="550" height="400" alt="Pasting text"></p>

                <p>To copy and paste text using the keyboard shortcuts, use the following procedure.</p>

                <ol>
                    <li>Highlight the text you want to cut and press the Control key and the C key at the same time.</li>
                    <li>Move the cursor to the new location.</li>
                    <li>Press the Control key and the V key at the same time.</li>
                </ol>

                <p>To copy and paste a placeholder, use the following procedure.</p>
                <ol>
                    <li>Click on the borders of the placeholder you want to copy to select it. Notice the cursor changes to a cross with arrows in all directions and the placeholder points are visible.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/cursor-cross.jpg" width="623" height="316" alt="Cursor changes to a cross"></p>

                <ol start="2">
                    <li>Select copy from the Ribbon, the context menu, or by using the keyboard shortcut.</li>
                    <li>Click in the new location for the placeholder. You can select an existing placeholder to replace it.</li>
                    <li>Select page from the Ribbon, the context menu, or by using the keyboard shortcut.</li>
                </ol>
                <p><a href="/ms-powerpoint-training-chicago.php">PowerPoint classes in Chicago</a>.</p>

                <h4>Using the Office Clipboard</h4>
                <p>To open the Clipboard Task pane, use the following procedure.</p>

                <ol>
                    <li>On the Home tab of the Ribbon, select the icon next to Clipboard. This is the Dialog Box launcher.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/office-clipboard.jpg" width="361" height="186" alt="Home Tab - Clipboard"></p>

                <p>The Clipboard task opens, displaying any items you have cut or copied in this PowerPoint 2013 session (or the 24 most recent). A sample is illustrated below. This example includes placeholders (or objects) that have been cut or copied as well as text.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/clipboard-task.jpg" width="153" height="453" alt="Clipboard - paste">     </p>

                <p>To paste using the Office Clipboard Task pane, use the following procedure.</p>

                <ol>
                    <li>Place the cursor where you want to paste text from the clipboard. If you are pasting a placeholder, click in the new location for the placeholder. You can select an existing placeholder to replace it.</li>
                    <li>Click on the item in the Clipboard task pane that you want to paste. The following illustrations show a placeholder being replaced, both before and after the paste.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/office-clipboard-task-pane.jpg" width="750" height="423" alt="Office Clipboard Task pane"></p>

                <h4>Using Undo and Redo</h4>
                <p>To undo their most recent command, use the following procedure.</p>

                <ol>
                    <li>Select the Undo command from the Quick Access Toolbar. If there is more than one item listed, select the item you want to undo.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/undo-command.jpg" width="175" height="217" alt="Undo Command"></p>

                <p>To redo the last command or repeat it, use the following procedure.</p>

                <ol>
                    <li>Select the Redo command from the Quick Access Toolbar.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/redo-command.jpg" width="254" height="71" alt="Quick Access Tool Bar - Redo"></p>

                <p><a href="/ms-powerpoint-training-los-angeles.php">MS Powerpoint classes in Los Angeles.</a></p>
                <h4>Finding and Replacing Text</h4>

                <p>To find text, use the following procedure.</p>

                <ol>
                    <li>Select <strong>Find</strong> from the Editing group on the Home tab of the Ribbon to open the Find dialog box.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/find.jpg" width="122" height="122" alt="Find Menu"></p>


                <ol start="2&quot;">
                    <li>Enter the exact text you want to find in the <strong>Find what</strong> field.</li>
                    <li>If desired, check the <strong>Match Case</strong> box to find only instances with the same capitalization.</li>
                    <li>If desired, check the <strong>Find Whole Words Only</strong> box to find the whole PowerPoint. Leaving this box unchecked will find any instance of that group of letters. For example if you search for the PowerPoint box, but have the <strong>Find Whole Words Only</strong> box unchecked, PowerPoint will find instances of box, as well as instances of “boxes” and “boxed.”</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/find-dialogue-box.jpg" width="301" height="115" alt="Find dialogue box"> </p>

                <p>To replace text, use the following procedure.</p>

                <ol>
                    <li>Select <strong>Replace</strong> from the Editing group on the Home tab of the Ribbon to open the Replace dialog box. You can also select the <strong>Replace</strong> button on the Find dialog box.</li>
                    <li>Enter the exact text you want to find in the <strong>Find what</strong> field.</li>
                    <li>Enter the replacement text in the <strong>Replace with</strong> field.</li>
                    <li>If desired, check the <strong>Match case</strong>, <strong>Find Whole Words only, </strong>and<strong> Match half/full width forms</strong> boxes.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/replace-dialogue-box.jpg" width="302" height="145" alt="Replacement dialogue box"></p>

                <ol start="5">
                    <li>Select <strong>Find</strong> <strong>next</strong> to find the next instance of the item.</li>
                    <li>When PowerPoint highlights the item, select <strong>Replace</strong> to delete the “find” item and paste the “replace” item.</li>
                    <li>Select <strong>Close</strong> when you have finished. Or select <strong>Cancel</strong> to close the dialog box without making any replacements.</li>
                </ol>

                <p>To replace all instances of an item, use the following procedure.</p>

                <ol>
                    <li>Open the <em>Find and Replace</em> dialog box by selecting <strong>Replace</strong> from the Ribbon.</li>
                    <li>Enter the exact text you want to find in the <strong>Find what</strong> field.</li>
                    <li>Enter the replacement text in the <strong>Replace with</strong> field.</li>
                    <li>Select <strong>Replace All</strong>.</li>
                    <li>Select <strong>Close</strong> when you have finished. Or select <strong>Cancel</strong> to close the dialog box without making any replacements.</li>
                </ol>

                <p>PowerPoint replaces all instances of the item and displays a message indicating how many replacements were made. To replace a font, use the following procedure.</p>

                <ol>
                    <li>Select <strong>Replace</strong> from the Ribbon. Select the <strong>Replace Font</strong> option to open the <em>Replace Font</em> dialog box.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/replace-fonts.jpg" width="249" height="152" alt="Replace Font Menu"></p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/replace-font-dialogue-box.jpg" width="250" height="99" alt="Replacement Font - Dialogue Box"> </p>

                <ol start="2">
                    <li>Select the Font you want to replace from the <strong>Replace</strong> drop down list.</li>
                    <li>Select the Font you want to use from the <strong>With</strong> drop down list.</li>
                    <li>Select <strong>Replace</strong>.</li>
                    <li>PowerPoint replaces all matching fonts. Select <strong>Close</strong> when you have finished.</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related PowerPoint Lessons</h4>
                    <ul>
                        <li><a href="/powerpoint/lessons/interface.php">The PowerPoint Interface</a></li>
                        <li><a href="/powerpoint/lessons/first-presentation.php">Your first PowerPoint presentation</li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=21">PowerPoint training testimonials page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>