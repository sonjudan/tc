<?php
$meta_title = "Working with Templates in Powerpoint | Training Connection";
$meta_description = "Learn how to use templates in Microsoft PowerPoint with our dedicated tutorial. This lesson is part of our MS PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working with Templates</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Templates</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>This article details how to work with templates in Microsoft PowerPoint.</p>
                <p>For comprehensive <a href="/ms-powerpoint-training.php">Microsoft PowerPoint training classes</a> delivered by instructors in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <p>In this article you will learn how to create and save a template</p>
                <h4>Creating a Template</h4>
                <p>In order to successfully complete this module, you should complete all topics in order, in one session.</p>

                <p>To begin creating a template, it is usually best to start with a blank slate. Open PowerPoint 2013 and create a blank presentation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/open-powerpoint.jpg" alt="Open Powerpoint To Begin"></p>
                <p>Next, switch to Slide Master view by clicking View → Slide Master:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/switch-to-slide-master-view.jpg" alt="View In Slide Masters"></p>
                <p>Click to select the top-level master for the current presentation (at the top of the Slides pane):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/click-top-slide-pane.jpg" alt="Select Top Level Master"></p>
                <p>The current template is quite plain, so let’s dress it up a bit by applying a theme. Click Slide Master → Themes → Circuit:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/apply-theme.jpg" alt="Apply Theme To Slide Master"></p>
                <p>The new theme will be applied to the slide master. This has the effect of changing the presentation’s background, colors, and fonts:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-theme-applied.jpg" alt="New Theme Applied To Slide Master"></p>
                <p>From here, you can adjust the headers and footers and add images and placeholders, just as you would when designing a slide layout. Keep in mind that these elements will appear on any slide using this template. <a href="/ms-powerpoint-training-chicago.php">Microsoft PowerPoint training offered in Chicago</a>.</p>

                <p>Leave Microsoft PowerPoint 2013 open and continue to the next topic. <a href="/ms-powerpoint-training-los-angeles.php">LA PowerPoint classes</a>.</p>

                <h4>Saving a Template</h4>
                <p>When you have finished designing your template, save it by clicking File → Save As → Computer → Browse:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/save-template.jpg" alt="Save New Templete"></p>

                <p>When the Save As dialog appears, type “Module 3” into the “File name” text box. Then, choose PowerPoint Template from the “Save as type” drop-down menu:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/save-name-and-type.jpg" alt="Amend Save As Dialog Box"></p>

                <p>When PowerPoint Template is selected as the save type, the directory will automatically change to C:/Users/[username]/My Documents/Custom Office Templates if you are using Windows 8. (Previous versions of Windows will use the C:/Documents and Settings/[username]/Application Data/Microsoft/Templates directory.) Click Save:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/auto-change-to-directory.jpg" alt="Automatic Directory Change"></p>


                <p>Close Microsoft PowerPoint.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>View unbiased student testimonials at our <a href="/testimonials.php?course_id=21"> MS PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>