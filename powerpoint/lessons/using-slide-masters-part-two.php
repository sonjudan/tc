<?php
$meta_title = "Using Slide Masters in Powerpoint Part Two | Training Connection";
$meta_description = "Learn how to use slide masters in Microsoft PowerPoint with our comprehensive tutorial. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Use Slide Masters Part Two</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Slide Masters, Part Two</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article is the second part of our tutorial on how to use Slide Masters in Microsoft PowerPoint.</p>
                <p>For instructor driven <a href="/ms-powerpoint-training.php">PowerPoint training classes</a> in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <p>In this article, we will learn how to:</p>

                <ul>
                    <li>Work with placeholders</li>
                    <li>Change a slide’s layout</li>
                </ul>

                <h2>Working with Placeholders</h2>

                <p>Open Slide Master view by clicking View → Slide Master:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/open-slide-master-view-p2.jpg" alt="Slide Master View Part 2"></p>

                <p>Scroll to the bottom of the Slides pane and click to select the last slide layout:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-last-slide-layout-p2.jpg" alt="Last Slide Layout Part Two"></p>

                <p>This slide layout contains a title and footer. Our next step is to add placeholders, which will allow the user to insert objects (such as text and pictures) into specific areas of the slide.</p>
                <p><a href="/ms-powerpoint-training-chicago.php">Microsoft PowerPoint classes in the Chicago Loop</a>.</p>

                <p>To insert a placeholder, click Slide Master → Insert Placeholder (drop-down arrow):</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/insert-placecard.jpg" alt="Insert Placecard From Slide Master"></p>

                <p>From the drop-down menu, click on the type of placeholder you would like to insert. For this example, click Picture:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/select-placecard-type.jpg" alt="Select Placecard Type from Drop-down"></p>
                <p><a href="/ms-powerpoint-training-los-angeles.php">Los Angeles PowerPoint 2019 classes</a>.</p>

                <p>Then, your cursor will change into a crosshair (+) so that you can draw the placeholder. For this example, click and drag to draw a rectangle below the slide’s title:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/draw-placecard.jpg" alt="Cursor Change On Placecard"></p>

                <p>Release your cursor when the placeholder has been drawn. It will be placed on the current slide layout using the dimensions that you specified:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/placecard-on-current-slide.jpg" alt="Placecard Appears On Current Slide"></p>

                <p>Close Slide Master view by clicking Slide Master → Close Master View:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/close-master-slide.jpg" alt="Close Slide Master View"></p>

                <p>Let’s create a new slide using the layout you just modified to see how the placeholder works. Click Home → New Slide (drop-down arrow):</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/create-new-slide-p2.jpg" alt="Create New Slide From Modified Placecard"></p>

                <p>This drop-down menu will list all of the slide layouts that exist in the current template, including the new slide layout that you just customized (Custom Layout). Click this slide layout:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/customize-slide-layout.jpg" alt="Select Customize Slide Layout From Menu"></p>

                <p>A new slide using the layout that you selected will be inserted into the current presentation. Test the placeholder you just created by clicking the picture icon in the middle of it:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-slide-layout-in-presentation.jpg" alt="New Slide Appears In Presentation"></p>

                <p>In this case, as it is a picture placeholder, the Insert Picture dialog will be displayed. Using the controls in this dialog, you can search for and select a picture on your computer that you would like to insert into this placeholder. Close the Insert Picture dialog by clicking Cancel:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/insert-picture-dialog.jpg" alt="Insert Picture Dialog Box"></p>

                <p>Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>

                <h2>Changing the Slide Layout</h2>

                <p>To begin, open Module 1-4 using Microsoft PowerPoint 2013.</p>

                <p>In addition to using layouts for new slides, you can change the layout of existing slides. For example, with the first slide in the current presentation selected, click Home → Layout:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/change-existing-slide-layout.jpg" alt="Change New Or Existing Slide Layout"></p>

                <p>The Layout drop-down menu will then list all of the possible slide masters to choose from. For this example, click Title and Content:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/all-master-slide-layouts.jpg" alt="Full Master Slide Layout Menu"></p>

                <p>The new slide master that you selected will now be applied to the current slide:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-master-slide-applied.jpg" alt="New Master Slide Applied To Current Slide"></p>
                <p>Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>

                <p>See <a href="/powerpoint/lessons/using-slide-masters-part-three.php">Using Slide Masters Part 3</a></p>
            </div>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=21">MS PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>