<?php
$meta_title = "Advanced Animation Techniques in Powerpoint Part Two | Training Connection";
$meta_description = "Discover how to use advanced animation techniques in Microsoft PowerPoint. This lesson forms part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced Animation Techniques Part Two</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Advanced Animation Techniques, Part Two</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will continue to explore advanced animation techniques and how to use them in Microsoft PowerPoint.</p>
                <p>For classroom based <a href="/ms-powerpoint-training.php">PowerPoint training</a> delivered by friendly instructors and available in Chicago and Los Angeles, call us on 888.815.0604. We also provide <a href="/onsite-training.php">Onsite PowerPoint training</a> available countrywide.</p>

                <p>In this Article, we will learn how to:</p>
                <ul>
                    <li>Edit the motion path of an animation</li>
                    <li>Set animation start options</li>
                    <li>Modifying Duration and Delay</li>
                </ul>

                <h4>Editing the Motion Path</h4>
                <p>PowerPoint allows you to control the path of an animation as well. To start, select the target object. (In this case, click the picture on the second slide in the sample presentation.) Click the Animations tab, click the More arrow inside the Animation gallery, and click the Lines motion path:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/edit-motion-path.jpg" alt="line motion path"></p>
                <p>Once a motion path has been applied, it will immediately be previewed. In this example, you can see that the default setting for the motion path takes the image too far down the slide, clipping the bottom of the image:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/motion-path-applied.jpg" alt="motion path previewed"></p>
                <p>To edit a motion path, you first need to select it. It may be hard to see, but it looks like a dashed line with a green and red handle at either end. Click on it to select it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/change-motion-path-p2.jpg" alt="locate motion path"></p>
                <p>The green handle identifies the start position of the animation, while the red handle marks the end position. For this example, you need to change the end position, so click and drag the red handle upwards:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/handle-position-indentifiers.jpg" alt="indentify handle position"></p>
                <p>As you drag this handle, you will see a transparent preview of where the image will appear at the end of the animation. Move the end position of this animation so that it appears within the boundaries of the current slide, but so there is room for the underlying text:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/transparent-preview.jpg" alt="preview of image appearance"></p>
                <p>Release your cursor when the image is properly positioned to finish editing the motion path. Click Animations → Preview:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/release-cursor-to-position.jpg" alt="release cursor to finish edit"></p>
                <p>If necessary, continue editing the motion path until the animation is in the correct place. <a href="/ms-powerpoint-training-chicago.php">Microsoft certified PowerPoint classes</a>.</p>

                <p>The animation will now move lower in the slide without cutting the image off. In addition, there is enough room for the underlying text to be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-move.jpg" alt="animation movement"></p>

                <h4>Setting Start Options</h4>
                <p>You can also control when animations play. To start, you first need to select the object that has an animation applied to it.</p>
                <p>For this example, select the image on the second slide of the sample presentation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/control-animation-play.jpg" alt="select animation play"></p>
                <p>Next, click Animations → Start:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/click-animation-start-p2.jpg" alt="select animation start"></p>
                <p>The options in this drop-down menu include On Click, With Previous, and After Previous:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/menu-options-p2" alt="drop down menu options"></p>
                <p>On Click, which is the default setting, will start the animation after a mouse click. With Previous will trigger this animation at the same time as the previous one. After Previous, on the other hand, will trigger this animation after the previous one is complete.</p>

                <h4>Modifying Duration and Delay</h4>
                <p>You can control the duration and delay of an animation by first selecting the animated object. For this example, select the image on slide two:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/duration-control.jpg" alt="change animation duration"></p>
                <p>The options to control the duration and delay of an animation can be found within the Timing group of the Animations tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/timing-group.jpg" alt="timing group on animation tab"></p>
                <p>The Duration increment box controls the length of time the animation will run, while Delay controls the delay between the start action and the animation starting. Increase the duration to five seconds:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/duration-increment-box.jpg" alt="control animation time with duration box"></p>
                <p>See how this new change will affect the animation by clicking Animations → Preview:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-changes-p2.jpg" alt="new changes applied"></p>
                <p>As you can see, the animation is much slower now.</p>

                <p>See <a href="/powerpoint/lessons/advanced-animation-techniques-p3.php">Advanced Animation Techniques, Part Three</a></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Find out what our happy students have to say about the Microsoft PowerPoint training course on our <a href="/testimonials.php?course_id=21">PowerPoint trainee testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>