<?php
$meta_title = "Using Slide Masters in Powerpoint Part One | Training Connection";
$meta_description = "Learn how to use slide masters in Microsoft PowerPoint. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Use Slide Masters</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Slide Masters, Part One</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will introduce you to slide masters and how to use them in Microsoft PowerPoint.</p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">PowerPoint training classes</a> in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <p>In this article, we will learn how to:</p>

                <ul>
                    <li>Open Slide Master view</li>
                    <li>Create slide layouts</li>
                </ul>

                <h4>Opening Slide Master View</h4>

                <p>Every PowerPoint presentation contains one or more sets of slide masters that control the layout and appearance of each slide. Using Slide Master view, you can make changes to these slide masters and their related layouts. To open slide master view, click View → Slide Master:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide-master-view.jpg" alt="Slide master icon"></p>

                <p>When Slide Master view has been applied, you will see the top-level slide master at the top of the Slides pane, with the related layouts listed below:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/top-level-slide-master.jpg" alt="Slide Master Layout List"></p>

                <p>In addition to the Slides pane, you will also see the Slide Master tab on the ribbon. This tab contains a number of commands that you can use to modify the master:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/slide-master-tab.jpg" alt="Slide Master Tab On Ribbon"></p>

                <p>Click Slide Master → Close Master View to return to the default PowerPoint view:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/default-powerpoint-view.jpg" alt="Return To Default Powerpoint"></p>

                <p>Close Microsoft PowerPoint 2013 without saving the changes that you have made. <a href="/ms-powerpoint-training-chicago.php">Chicago based PowerPoint training</a>.</p>

                <h4>Creating Slide Layouts</h4>

                <p>Open Slide Master view by clicking View → Slide Master:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/view-slide-master.jpg" alt="Open Slide Master"></p>

                <p>To create a new slide layout using the Slide Master view, first click to select the top-level master that you want the new layout to appear in. For this example, ensure that the top-level slide master is selected:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/create-new-slide-layout.jpg" alt="New Slide Layout"></p>

                <p>Next, click Slide Master → Insert Layout:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/insert-layout.jpg" alt="Insert Layout in Slide Master"></p>

                <p>The new slide layout will now appear at the bottom of the Slides pane. This new layout will also be automatically selected and open in the workspace:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-slide-layout-slide-panel.jpg" alt="New Slide Layout Slide Panel"></p>

                <p>As you can see, slide layouts will automatically incorporate the theme elements that were defined in the top-level master. All you need to do is insert placeholders for text, videos, pictures, charts, or other media that you would like this new slide layout to include.</p>
                <p><a href="/ms-powerpoint-training-los-angeles.php">Instructor-led Powerpoint classes in LA</a>.</p>
                <p>Save the changes that you have made to the presentation and close Microsoft PowerPoint.</p>
                <p>See <a href="/powerpoint/lessons/using-slide-masters-part-two.php">Using Slide Masters Part 2</a></p>
            </div>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=21">PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>