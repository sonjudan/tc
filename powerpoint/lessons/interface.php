<?php
$meta_title = "The PowerPoint Interface | Training Connection";
$meta_description = "Learning how to navigate around the Microsoft PowerPoint interface. This lesson is part of our Level 1 PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">PowerPoint Interface</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with the PowerPoint Interface</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we’ll introduce you to the PowerPoint 2013 interface, which uses the Ribbon from the previous two versions of PowerPoint. You’ll get a closer look at the Ribbon, as well as the Navigation pane and the Status bar. You’ll also learn how to manage your Microsoft account right from a new item above the Ribbon. This module introduces you to the Backstage view, where all of the functions related to your files live. You’ll learn how to save files. Finally, we’ll look at closing files and closing the application.</p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">PowerPoint classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Understanding the Interface</h4>
                <p>The PowerPoint interface, including the Ribbon, the Slides tab, the presentation window, the Notes pane, the Comments pane, the Quick Access toolbar, and the Status bar.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/powerpoint-interface.jpg" width="750" height="422" alt="The PowerPoint Interface"></p>

                <ul>
                    <li>The Slides tab shows a thumbnail of each slide in the presentation. </li>
                    <li>The presentation window is where you can view and edit the entire slide. </li>
                    <li>The Status Bar shows the current slide number, as well as the total slides, as well as the language setting for proofing. It also has additional tools for making changes to the view or zoom. If the Notes and Comments pane are not showing, just click on those icons on the Status bar to show them.</li>
                    <li>The Notes pane allows you to add speaker notes to the presentation. You can print speaker notes to use when delivering a presentation. </li>
                    <li>The Comments pane allows you to add comments to a presentation, especially helpful when working with a team to develop the presentation.</li>
                </ul>

                <p>Each Tab in the Ribbon contains many tools for working with your presentation. To display a different set of commands, click the Tab name. Buttons are organized into groups according to their function. <a href="/ms-powerpoint-training-chicago.php">Hands-on PowerPoint classes in Chicago</a>.</p>

                <p>The Quick Access toolbar appears at the top of the PowerPoint window. It provides you with one-click shortcuts to commonly used functions, like save, undo, and redo.</p>

                <p>To zoom in or out, use the following procedure. </p>

                <ul>
                    <li>Click the minus sign in the Status bar to zoom out. Click the plus sign in the Status bar to zoom in. You can also drag the slider to adjust the zoom.</li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/status-bar.jpg" width="452" height="43" alt="The Status Bar"></p>

                <h4>An Introduction to Backstage View</h4>

                <p>Review the Backstage View, use the following procedure.</p>

                <ol>
                    <li>Select the <strong>File</strong> tab on the Ribbon.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/file-tab.jpg" width="374" height="306" alt="The File Tab"> </p>

                <p>PowerPoint displays the Backstage View, open to the Info tab by default. A sample is illustrated below. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/backstage-view.jpg" width="750" height="422" alt="Backstage View"> </p>

                <h4>Saving Files</h4>

                <p>To save a presentation that has not been previously saved, use the following procedure.  </p>

                <ol>
                    <li>Select the <strong>File</strong> tab on the Ribbon.</li>
                    <li>Select the <strong>Save</strong> command in the Backstage View.</li>
                    <li>Select the <strong>Place</strong> where you want to save the presentation.</li>
                    <li>If you choose your SkyDrive, you can select the <strong>Presentations</strong> folder. If you choose your Computer, select your <strong>Current</strong> <strong>Folder</strong> or one of your <strong>Recent</strong> <strong>Folders</strong>. Or in either place, you can choose <strong>Browse</strong> to select a new location.
                      <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/save-as.jpg" width="696" height="328" alt="Saving a Microsoft PowerPoint file"></p></li>

                    <li>The <em>Save As</em> dialog opens. Enter a <strong>File Name</strong>, and if desired, navigate to a new location to store the file. Select <strong>Save</strong>. <a href="/ms-powerpoint-training-los-angeles.php">Small MS PowerPoint class in LA</a>.</li>
                </ol>

                <h4>Closing Files vs. Closing PowerPoint</h4>

                <p>To close a file, use the following procedure.</p>

                <ol>
                    <li>Select the <strong>File</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Close</strong> from The Backstage View.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/backstage-view-close.jpg" width="78" height="320" alt="Close file using Backstage view"></p>

                <p>If you haven’t saved your file, you will see the following message.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/pp-warning-message.jpg" width="280" height="93" alt="Warning message when closing PowerPoint without saving"></p>

                <p>To close the application (if only one presentation is open), use the following procedure.</p>

                <ol>
                    <li>Click the X at the top right corner of the window.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/closing-powerpoint.jpg" width="246" height="64" alt="How to close PowerPoint"></p>

                <br>
                <h4>Other resources</h4>
                <p><a href="https://books.google.com/books?id=7PmlCgAAQBAJ&amp;pg=PA13&amp;dq=powerpoint+interface&amp;hl=en&amp;sa=X&amp;redir_esc=y#v=onepage&amp;q=powerpoint%20interface&amp;f=false" target="_blank">PowerPoint 2016 Ebook - The Interface</a></p>

                <p><a href="https://support.office.com/en-us/article/Training-PowerPoint-for-Mac-2011-basics-f5ae61ff-4a8d-4de8-8e41-3b6f5a5bbae6" target="_blank">raining videos on PowerPoint 2011 for Mac</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related PowerPoint Lessons</h4>
                    <ul>
                        <li><a href="/powerpoint/lessons/first-presentation.php">Starting your first PowerPoint Presentation</a></li>
                        <li><a href="/powerpoint/lessons/working-with-text.php">Working with Text in PowerPoint</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students...you can read some reviews at the following page -&gt;<a  href="/testimonials.php?course_id=21">PowerPoint testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>