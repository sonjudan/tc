<?php
$meta_title = "Advanced Animation Techniques in Powerpoint Part Three | Training Connection";
$meta_description = "Further your skills with advanced animation techniques in Microsoft PowerPoint part three . This lesson forms part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced Animation Techniques Part Three</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Advanced Animation Techniques, Part Three</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Continuing on with advanced animation techniques, this article delves deeper into editing options available for animation within Microsoft PowerPoint.</p>
                <p>For <a href="/ms-powerpoint-training.php">MS PowerPoint training</a> delivered by friendly instructors in a relaxed classroom based environment, call us on 888.815.0604. Courses available in both Chicago and Los Angeles as well as <a href="/onsite-training.php">Onsite PowerPoint training</a> available countrywide.</p>

                <p>In this article, we will learn how to:</p>
                <ul>
                    <li>Set animation options</li>
                    <li>Change the order of animations</li>
                </ul>

                <h4>Setting Animation Options</h4>
                <p>PowerPoint contains some additional options to further customize animations in a presentation. To start, select an object that has an animation applied to it. For this example, click the title text box on the first slide:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/additional-customize-options.jpg" alt="additional animation options"></p>
                <p>Next, click the Animations tab and click the option button <img class="d-inline" src="https://www.trainingconnection.com/images/Lessons/powerpoint/option-button.jpg" alt="option icon"> in the Animation group:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/option-button-animation-group.jpg" alt="option button on animations tab"></p>
                <p>For this example, this action will open the Fade dialog because a Fade effect has been applied to the selected object:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/fade-dialog-p3.jpg" alt="fade effect"></p>
                <p>Note that the controls in this dialog depend upon the type of effect that has been applied to the selected object. Here, you can change path settings, sounds, and timing options. For this example, click the Sound menu and choose any sound:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/controls-in-dialog-p3.jpg" alt="change sound in control dialog"></p>
                <p>Once a sound has been selected, click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/sound-selected.jpg" alt="click ok to select sound"></p>
                <p>The Fade dialog will close and preview the new setting. In this case, the sound you selected will play when the selected animation starts. <a href="/ms-powerpoint-training-los-angeles.php">MS PowerPoint 2019 training</a>. </p>

                <h4>Changing the Order of Animations</h4>
                <p>You may have noticed that objects with animations display small numbers in their upper left-hand corners. These numbers (which are only displayed when the Animations tab is open) indicate the order in which animations will appear.</p>
                <p>In the sample presentation, you can see that the fade for the title on slide one will appear first, while the fade on the subtitle will appear second:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/fade-sequence.jpg" alt="fade order changed"></p>
                <p>To adjust this order, first select the object that the animation has been applied to. For this example, ensure that the title on slide one is selected. Next, click Animations → Move Later:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/adjust-order.jpg" alt="amend fade order"></p>
                <p>The order of the two animations on the current slide will change, with the title now appearing second and the subtitle appearing first:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/animation-order-changed-p3" alt="animation changes"></p>
                <p>Click Animations → Preview to see how the new settings will look:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/new-settings-applied-p3.jpg" alt="animation settings applied"></p>
                <p>See <a href="/powerpoint/lessons/advanced-animation-techniques-p4.php">Advanced Animation Techniques, Part Four</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year thousands of students increase their PowerPoint skills with us. Read in-depth student reviews on our <a href="/testimonials.php?course_id=21">PowerPoint review page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>