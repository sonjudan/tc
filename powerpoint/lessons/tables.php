<?php
$meta_title = "PowerPoint 2013 Tables | Training Connection";
$meta_description = "Learn how to add, format, edit, and sort a table in Microsoft PowerPoint. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tables</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Tables in PowerPoint</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will show you step-by-step how to add and edit tables in Microsoft PowerPoint 2013. You will add a border and learn to sort a table.</p>

                <p>For instructor-led <a href="/ms-powerpoint-training.php">PowerPoint training classes</a> in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Onsite PowerPoint training</a> is available countrywide.</p>

                <h4>How to insert a Table</h4>

                <ol>
                    <li>Insert a new Slide with the Title and Content layout.</li>
                    <li>Click on the Table icon.</li>
                    <li>Select the required number of rows and columns from the Insert Table Dialog.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/insert-table.jpg" width="392" height="236" alt="Insert Table Dialog"></p>

                <ol start="4">
                    <li>Click <em>OK</em>.</li>
                    <li>Click outside the border area to embed the table into PowerPoint.</li>
                    <li>On selecting a table the Ribbon will display two new Tabs. All Table layout and design commands are contained on these tabs.
                        <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/design-table-tools.jpg" width="780" height="97" alt="Design Tab"> </p>
                    </li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/layout-table-tools.jpg" width="780" height="87" alt="Layout Tab"></p>

                <p><a href="http://www.uis.edu/informationtechnologyservices/wp-content/uploads/sites/106/2013/04/PP2007Shortcuts.pdf" target="_blank">See PowerPoint 2013 shortcuts</a></p>

                <h4>How to Insert Rows and Columns</h4>

                <ol>
                    <li>Position your cursor in the table.</li>
                    <li>On the Table Tools, Layout ribbon select the desired icon.</li>
                    <li>Choose Insert Rows Above or Below/or choose Insert Columns to the Left or Right</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/rows-and-columns.jpg" width="368" height="212" alt="PowerPoint 2013 Insert Rows &amp; Columns Group"></p>
                <p><a href="/ms-powerpoint-training-chicago.php">Chicago Onsite training in PowerPoint</a>.</p>

                <h4>How to Delete Rows and Columns</h4>
                <ol>
                    <li>Click in the Column or Row to be deleted.</li>
                    <li>On the Table Tools, Layout Tab Click the Delete button.</li>
                    <li>Select to delete a Row or Column or the entire table.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/delete.jpg" width="120" height="180" alt="PowerPoint 2013 Delete Rows/Columns"></p>

                <h4>How to Merge Cells</h4>
                <ol>
                    <li>Select two or more adjacent cells.</li>
                    <li>On the Table Tools, Layout tab.</li>
                    <li>
                        <p>Click the <em>Merge Cells</em> icon. </p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/merge-cells.jpg" width="428" height="336" alt="Merging Cells icon"></p>
                    </li>
                </ol>

                <h4>How to add Borders</h4>

                <ol>
                    <li>Select the table or cells to apply the border to.</li>
                    <li>On the Table Tools, Design Tab, Table Styles group.</li>
                    <li>Click the Borders icon.</li>
                    <li>
                        <p>Select the type of border required. The selected table cells will have the appropriate border applied. </p>
                        <img src="https://www.trainingconnection.com/images/Lessons/powerpoint/table-borders-tool.jpg" width="444" height="872" alt="">
                    </li>
                </ol>
                <p><a href="/ms-powerpoint-training-los-angeles.php">Los Angeles Public classes on PowerPoint</a>.</p>

                <h4>How to Colour a Table</h4>
                <ol>
                    <li>Select the table or cells to apply the colour to.</li>
                    <li>On the Table Tools, Design Tab, choose Shading.</li>
                    <li>Select the colour you require. </li>
                    <li>Click okay, the colour you chose will appear on the selected cells or table.</li>
                </ol>

                <h4>Sorting a table in PowerPoint</h4>

                <ul>
                    <li>You can sort text, numbers, or dates in ascending order (A to Z, zero to 9, or earliest to latest date). Or, you can sort in descending order (Z to A, 9 to zero, or latest to earliest date). </li>
                    <li><strong>If you sort by text</strong> Word first sorts items that begin with punctuation marks or symbols (such as !, #, $, %, or &amp;); items that begin with numbers are sorted next; and items that begin with letters are sorted last. Keep in mind that Word treats dates and numbers as text. For example, dates are treated as three-digit numbers, and "Item 12" is listed before "Item 2."</li>
                    <li><strong>If you sort by numbers</strong> Word ignores all characters except numbers. The numbers can be in any location in a paragraph.</li>
                    <li><strong>If you sort by date</strong> Word recognizes the following as valid date separators: hyphens, forward slashes (/), commas, and periods. Word also recognizes colons (:) as valid time separators. If Word doesn't recognize a date or time, it places the item at the beginning or end of the list (depending on whether you're sorting in ascending or descending order). </li>
                    <li><strong>If you sort by a specific language</strong> Word sorts according to the sorting rules of the language. </li>
                    <li><strong>If two or more items begin with the same character</strong> Word evaluates subsequent characters in each item to determine which item comes first.</li></ul>
                    <li><strong>If the data consists of field results</strong> Word sorts the field results according to the sort options you've set. If an entire field (such as a last name) is the same for two items, Word next evaluates subsequent fields (such as a first name). </li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=21">PowerPoint student testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>