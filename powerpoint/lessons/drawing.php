<?php
$meta_title = "PowerPoint 2013 Drawing | Training Connection";
$meta_description = "Learn how to draw shapes, change the fill, add a fill effect, a border and text in a shape in Microsoft PowerPoint. This lesson is part of our PowerPoint Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/powerpoint.php">Powerpoint</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Drawing</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint">
                    </div>

                    <div data-aos="fade-up">
                        <h1>PowerPoint Drawing Tools</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will show you step-by-step how to draw shapes, change the fill, the fill effect and the border in Microsoft PowerPoint 2013.</p>
                <p>For instructor-led <a href="/ms-powerpoint-training.php">Microsoft PowerPoint training</a> in Chicago and Los Angeles call us on 888.815.0604. <a href="/onsite-training.php">Group Onsite PowerPoint training</a> is available countrywide.</p>

                <h4>Drawing Options</h4>
                <ol>
                    <li>
                        <p>Things you draw fall into two categories.</p>
                        <ul>
                            <li>One category includes lines, arcs, and freeforms.</li>
                            <li>The other category includes rectangles, ellipses, and other shapes that we call AutoShapes. Each category has its own tools and attributes.</li>
                        </ul>
                    </li>
                    <li>
                        <p>Drawn objects have attributes, just like other objects.</p>
                        <ul>
                            <li>For instance, they can have a border, a shadow, a colour, and so on. The Drawing Tools, Format Ribbon has icons you can use as shortcuts for commands such as Line Style, Fill, and Shadow.</li>
                            <li>PowerPoint has a whole series of features for resizing, altering, and adjusting objects after you've drawn them. You can change any attribute at any time. For instance, you can change an object's colour, pattern, shadow, the colour of its text, or even the shape of the object itself after you've drawn it. Because there are lots of ways you can position objects on slides, you don't have to draw objects in exactly the right position. You can move, align, and reposition them at any time.</li>
                            <li>You can treat drawn objects like all other PowerPoint objects you work with.</li>
                            <li>You can cut, copy, and paste drawn objects. You can also duplicate, delete, resize, move, and colour them.</li>
                            <li>You can attach text to shapes.</li>
                        </ul>
                    </li>
                </ol>

                <p><a href="https://support.office.com/en-us/article/PowerPoint-training-40e8c930-cb0b-40d8-82c4-bd53d3398787" target="_blank">See PowerPoint video training resources</a></p>

                <h4>Drawing Shapes</h4>
                <p>With PowerPoint 2013 you can insert shapes, draw lines, insert pictures and create WordArt by using the Illustrations group on the Insert Ribbon. <a href="/ms-powerpoint-training-los-angeles.php">Read more</a> about PowerPoint training in LA.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/illustrations-group.jpg" width="400" height="" alt="PowerPoint Insert Tab – Illustrations Group"></p>

                <h4>How to Draw Circles, Squares, and Auto Shapes</h4>

                <ol>
                    <li>Click the Insert Tab and select the Shapes Icon from the Illustrations Group.</li>
                    <li>Select the shape you want.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/shapes.jpg" width="300" height="" alt="Available shapes to choose from"></p>

                <ol start="3">
                    <li>The pointer turns into a cross hair when you move it over the slide.</li>
                    <li>Click where you want the shape to begin, and then drag (<em>Holding the Shift Key whilst dragging will assist in creating a straight line or a perfect square or circle</em>).</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/inserted-shape.jpg" width="300" height="" alt="Drag the shape"></p>

                <h4>How to Change the Fill Colour of a Shape</h4>

                <ol>
                    <li>Select the Shape.</li>
                    <li>
                        <p>From the Drawing Tools, Format Tab choose style or colour from the Shape Fill icon.</p>
                        <img src="https://www.trainingconnection.com/images/Lessons/powerpoint/shape-fill.jpg" width="300" height="" alt="Shape fill icon">
                    </li>
                </ol>

                <h4>How to Change the Fill Effect of a Shape</h4>

                <ol>
                    <li>Select the Shape.</li>
                    <li>From the Drawing tools, Format Ribbon chose Gradient or Texture.</li>
                    <li>Choose the appropriate tab, Gradient or Texture.</li>
                    <li>For more options choose the More Gradients or More Texture options.</li>
                </ol>

                <h4>How to Change the Line Border and Border Colour of a Shape</h4>

                <ol>
                    <li>Select the Shape.</li>
                    <li>Click the Shape Outline icon on the Format Tab of the Shape Tools Ribbon.</li>
                    <li>Change the line style and colour if required.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/shape-outline.jpg" width="300" height="" alt="Shape Outline Icon"> </p>
                <p><a href="/ms-powerpoint-training-chicago.php">PowerPoint classes in the Chicago Loop</a>.</p>

                <h4>How to Resize a Shape</h4>

                <ol>
                    <li>Select the shape.</li>
                    <li>Point to any of the resize symbols around the shape.</li>
                    <li>The pointer changes to a double-ended arrow.</li>
                    <li>Drag the double-ended arrow until the outline of the object is the right shape and size.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/powerpoint/resizing-shape.jpg" width="300" height="" alt=""></p>

                <h4>How to Type Text in a Shape</h4>
                <ol>
                    <li>Select the shape.</li>
                    <li>Begin typing.</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-powerpoint" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">PowerPoint student reviews</h4>
                    <p>We train thousands of satisfied students.... read a sample of our testimonials at our <a href="/testimonials.php?course_id=21">PowerPoint class testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Microsoft PowerPoint courses</h4>
                    <ul>
                        <li><a href="/powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></li>
                        <li><a href="/powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>