<?php
$meta_title = "About Us | Training Connection";
$meta_description = "Training Connection is computer and business skills training school. We believe hands-on training taught face-to-face is the best way to learn.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About us</li>
                </ol>
            </nav>

            <div class="page-intro">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >About Us</h1>
                    <h5 class="" data-aos="fade-up" data-aos-delay="100">Our philosophy</h5>
                </div>
            </div>

            <div class="section-content-l2 copy-sm pb-5 pt-0" data-aos="fade-up">

                <h5>For 13 years we have set the benchmark for providing quality training on Adobe, Apple, Microsoft Office, Web Development and Business Skills training.</h5>

                <p>Our approach is simple - we are a traditional training company. We believe that learning from a <strong>live instructor present in the classroom</strong> is still the most effective way to learn a new software program or a new business skill.</p>

                <p>Our certified training classes are small and hands-on giving students an unrivaled learning experience, plus we allow students to repeat their training free of charge if they need to brush up on their skills or simply just need a refresher.</p>

                <p>We work with only a <strong>small group of really talented trainers</strong>, most of whom have been with us since 2006. The trainers are without a doubt the most important element in delivering an <strong>exceptional learning experience</strong>. Our trainers bring a <strong>wealth of real world experience</strong> into each class and are <strong>passionate about teaching</strong> others.</p>

                <p>These days there are a lot of different training choices available, online, webinar etc. but if you are looking for the <strong>best and highest quality learning experience</strong> then nothing compares to one of our face-to-face instructor-led classes.</p>

         

                <div class="card-blocks-2col">
                    <div class="card-blocks">
                        <i class="fas fa-chalkboard-teacher"></i>
                        <p>Live trainer (actually present in the classroom)</p>
                    </div>
                    <div class="card-blocks">
                        <i class="fas fa-globe-americas"></i>
                        <p>Emphasis on providing a real world learning experience</p>
                    </div>
                    <br>
                    <div class="card-blocks">
                        <i class="far fa-eye"></i>
                        <p>Free class repeat valid for 6 months</p>
                    </div>
                    <div class="card-blocks">
                        <i class="fas fa-redo-alt"></i>
                        <p>Money back guarantee (some conditions)</p>
                    </div>
                </div>

                <div class="page-footer mb-5">
                    <h4>Register for one of our public classes (Chicago or Los Angeles) today!</h4>
                    <p>Need group training? No problem, we can provide you with a competitive group pricing for onsite training countrywide.
                        <a href="/onsite-training.php" class="link-secondary">Click here</a> for a quotation.</p>
                </div>

            </div>

        </div>

    </main>




<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>