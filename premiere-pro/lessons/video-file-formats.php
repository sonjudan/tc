<?php
$meta_title = "Video File Formats Explained | Training Connection";
$meta_description = "Learn about different video file formats. This topic is covered in our Adobe Premiere Pro classes";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Spot Color Effect</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Demystifying Video File Formats</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>In the sea of confusing file formats out there, a lot of users find themselves lost  many times choosing to output their videos as files they don’t completely understand. If you find yourself in this situation, fear not. This quick, down and dirty guide is designed to give you a fundamental idea of how video files break down and what formats you should be using in various stages of your workflow. For a deeper understanding and more thorough exploration of file formats, check out the <a href=/premiere-pro-training.php">Adobe Premiere Classes</a> or <a href=/after-effects-training.php">Adobe After Effects classes</a> at <a href="/">Training Connection</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/000_Article Header Banner.jpg" width="839" height="536" alt="Video File Formats Explained"></p>

                <h4>Starting at the Beginning</h4>

                <p>Across the world of video editing, there can be many categories of files out there but these, arguably, are the most important:</p>

                <ul>
                    <li>Camera Original / Raw Media Formats</li>
                    <li>Proxy Formats</li>
                    <li>Mezzanine Formats</li>
                    <li>Delivery Formats</li>
                </ul>
                <p><a href="/premiere-pro-training-chicago.php">Adobe certified training on Premiere Pro</a>.</p>


                <h4>Camera Original or Raw File Formats</h4>
                <p>Camera Original or Raw Formats are the untouched media that your video camera produces. Some budget-friendly cameras shoot highly compressed formats while other, high end cameras shoot less compressed, higher quality footage or raw media. The following are a handful of popular camera file formats: </p>

                <ul>
                    <li>Quicktime (H.264/MOV) - Highly compressed, a format typical of DSLR cameras</li>
                    <li>Quicktime (Prores/MOV) - Typical of cameras like Blackmagic</li>
                    <li>MXF (HDVCPRO HD) - Typical cameras like Panasonic Varicam Series, P2</li>
                    <li>MXF (DNxHD) - Arri Alexa</li>
                    <li>AVCHD (MTS,MP4) - Panasonic GH4, Panasonic GH5, Canon HF M500, Sony HDR Series</li>
                    <li>Red Raw (.R3D) - Red One, Red Dragon</li>
                    <li>Arri Raw (.ARI) - Arri Alexia, Amira, </li>
                    <li>Sony Raw (.MXF) - F-Series/F55</li>
                    <li>Adobe Raw (.DNG) - Blackmagic, Arri</li>
                    <li>Phantom Cine (.CINE/DPX) - Phantom</li>
                    <li>MP4 (H.264)* - iPhone, Android, iPad, etc</li>
                </ul>

                <p>*Some camera formats use Proxy and Delivery Formats as their file of choice to keep file sizes and bitrates low to make your workflows easier or to use less expensive, lower bandwidth camera parts.</p>

                <h4>Proxy File Formats (Temporary File Formats)</h4>

                <p>Proxy Formats are smaller, lower resolution, replaceable files that are used to make it easier for editing and temporary hard drive or server storage. Since the files don’t require as much resources from your computer, hard drive or server, it can be much easier to edit, eliminating problems with playback. It can even make difficult 4K+ workflows easier (laptop, mobile device editing). The ultimate point of a proxy file is that it is a “placeholder” or temporary file that will eventually get replaced by a much higher quality file or an original camera file after the initial edit is finished. The following formats are either legitimate proxy formats or file formats that can be used as proxies:</p>

                <ul>
                    <li>Quicktime (R3D) - Proxies used in Red Cameras; a Quicktime (MOV) file with an .R3D codec</li>
                    <li>Quicktime (.JPG) - Can be used as a proxy file</li>
                    <li>Quicktime (Prores Proxy/LT) - Native to Final Cut Pro but can be used in Premiere and Avid</li>
                    <li>Quicktime (DVCPRO HD) - Based of the popular codec used in Varicam and P2 cameras</li>
                    <li>AVCHD (MP4) - lower resolution AVCHD formats can be used as proxies</li>
                    <li>MP4 (H.264/AVCHD)* - MP4s, in general, are highly compressed files that can be used as proxies</li>
                    <li>MXF (DNxHD 36) - Popular proxy format for feature films and other workflows</li>
                    <li>MXF (DVCPRO HD) - Can be used as proxy using this camera format</li>
                </ul>

                <p>*Note: Most files have a lower res version of themselves that can be used as a proxy. <a href="/premiere-pro-training-los-angeles.php">Small hands-on Premiere classes</a>.</p>

                <h4>Mezzanine File Formats (Intermediate File Formats)</h4>

                <p>Mezzanine files formats (also known as Intermediate File Formats) are higher quality files that can behave like original camera formats or what’s considered to be 1:1 or uncompressed files (also can be referred to as “Visually Lossless”). Of course, these files are compressed, but they won’t generally look compressed. These formats can be used for many reasons and here are the most popular:</p>

                <ul>
                    <li>To unify multiple types of camera footage under one high quality file format</li>
                    <li>To work with a high quality file that you can edit, use for VFX and color grade without using proxies</li>
                    <li>To make video files work better in your editing application</li>
                    <li>To archive or backup your project/sequence for storage</li>
                    <li>To send or transfer your project footage to another system for editing or VFX</li>
                </ul>

                <p>There are fewer mezzanine file formats out there and these formats are very special. Here are some of the MOST popular formats:</p>

                <ul>
                    <li>MXF (DNxHD/DNxHR) - Popular use with Avid and Premiere</li>
                    <li>MOV (DNxHD/DNxHR) - Popular use with Avid and Premiere</li>
                    <li>MOV (Prores 422/422 HQ/4444) - Popular use with Final Cut X, Premiere and Avid</li>
                    <li>MOV (GoPro Cineform) - Amazing format primarily used in Adobe Premiere</li>
                    <li>MOV/MXF (JPEG 2000) - Used with Avid, Premiere and Final Cut X</li>
                    <li>MOV (Animation) - Used with Avid and Premiere</li>
                </ul>

                <p><u>Most</u> of the mezzanine files above are available in Adobe Premiere, Avid and Final Cut X.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/001_Studio Banner.jpg" width="839" height="268" alt="Film and TV studio banner">  </p>


                <h4>Delivery File Formats (Final Stage and Format for Broadcast, Social Media, Theatrical)</h4>

                <p>You shot, edited, and color graded your film and now it is ready to be seen by millions. It’s time to output your video in a delivery format. First off, there are two basic levels of delivery format categories:</p>

                <ul>
                    <li>Digital Masters: These files are usually super high quality outputs of your final video many times exceeding the quality of mezzanine codecs. Instead of opening up your editing software and timeline to output every time you need a new copy or format, you can just make copies from the single, high quality file of the Digital Master. </li>
                    <li>Compressed Deliverables: These are files that you would need to output for distribution to social media, streaming services, mobile, etc. Usually a compressed deliverable is created from the Digital Master.</li>
                </ul>

                <h4>Bringing it Together</h4>

                <p>So, you are looking at all these file formats and wondering, “What makes a file a mezzanine file vs a proxy when using the same format?” The answer is simple. Bitrate. Bits are 1s and 0s that make up ALL digital content whether it be video, audio, digital photos, graphics, documents, etc. All digital content has bitrate associated with it that defines two basic things: The Quality and Size of your files. For example, a proxy file tends to have a bitrate from about 36 megabits/s to 100 megabits/s. A mezzanine codec tends to have a bitrate from about 100 megabits/s to 220 megabits/s. A digital master tends to have a bitrate of 175 megabits/s to 1000+ megabits/s. Using the numbers as a gauge for quality and size, it’s easy to see the comparisons between the different types of files and which ones are higher quality vs lower.</p>


                <h4>Video Formats Comparison (Short List)</h4>

                <table class="table table-bordered table-dark table-hover table-l2 mb-1">
                    <thead>
                        <tr>
                            <td><strong>PROXY (LQ)</strong></td>
                            <td><strong>Mbit/s</strong></td>
                            <td><strong>MEZZANINE (HQ)</strong></td>
                            <td><strong>Mbit/s</strong></td>
                            <td><strong>DELIVERY</strong></td>
                        </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Temporary files used for editing. Eventually these will be replaced with HQ files. Used to make editing easier—like editing 4K on a laptop.</td>
                        <td>&nbsp;</td>
                        <td>Also known as Intermediate Files, HQ Files used for editing, color grading, going from one pro software to another maintaining quality or archiving.</td>
                        <td>&nbsp;</td>
                        <td>Final Files output for Youtube/Vimeo, Mobile, Online, Broadcast. <em>Note: Mezzanine files can be delivery as well when it comes to digital masters.</em></td>
                    </tr>
                    <tr>
                        <td>Prores Proxy </td>
                        <td align="center">36</td>
                        <td>DNxHD 115 - 220+</td>
                        <td align="center">Var</td>
                        <td>MP4/H.264*Social Media</td>
                    </tr>
                    <tr>
                        <td>Prores LT</td>
                        <td align="center">102</td>
                        <td>DNxHR 4K HQX</td>
                        <td align="center">222</td>
                        <td>MP4/H.265</td>
                    </tr>
                    <tr>
                        <td>DNxHD 36</td>
                        <td align="center">36</td>
                        <td>Prores 422 (MOV)</td>
                        <td align="center">175</td>
                        <td>FLV</td>
                    </tr>
                    <tr>
                        <td>DNxHD 36</td>
                        <td align="center">60</td>
                        <td>Prores 422 HQ (MOV)</td>
                        <td align="center">220</td>
                        <td>MPEG-2</td>
                    </tr>
                    <tr>
                        <td>DNxHD 90</td>
                        <td align="center">90</td>
                        <td>Prores 4444 (MOV)</td>
                        <td align="center">330</td>
                        <td>MPEG-4</td>
                    </tr>
                    <tr>
                        <td>DVCPRO HD</td>
                        <td align="center">100</td>
                        <td>GoPro Cineform YUV</td>
                        <td align="center">235</td>
                        <td>VC-1</td>
                    </tr>
                    <tr>
                        <td>JPG (MOV)</td>
                        <td align="center">-</td>
                        <td>GoPro Cinform 12-Bit</td>
                        <td align="center">235+</td>
                        <td>WMV</td>
                    </tr>
                    <tr>
                        <td>R3D (MOV)</td>
                        <td align="center">Var</td>
                        <td>OpenEXR</td>
                        <td align="center">High/Var</td>
                        <td>TAPE</td>
                    </tr>
                    <tr>
                        <td>MP4 (H.264)</td>
                        <td align="center">Var</td>
                        <td>Adobe Cinema DNG</td>
                        <td align="center">High/Var</td>
                        <td>Prores (Digital Master)</td>
                    </tr>
                    <tr>
                        <td>DVCPRO 50</td>
                        <td align="center">50</td>
                        <td>CINE/DPX</td>
                        <td align="center">High/Var</td>
                        <td>DNxHD/HR (Digital Master)</td>
                    </tr>
                    <tr>
                        <td>PNG (MOV)</td>
                        <td align="center">-</td>
                        <td>JPEG2K (MXF/MOV)</td>
                        <td align="center">High/Var</td>
                        <td>Cineform (Digital Master)</td>
                    </tr>
                    <tr>
                        <td>MP2</td>
                        <td align="center">Var</td>
                        <td>Animation (MOV)</td>
                        <td align="center">High/Var</td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

                <p>Bitrates can change based on other file properties such as frame size so some of the rates listed above may fluctuate.</p>

                <p>In closing, here are some parting words of advice for you on specific scenarios if the numbers mentioned above make you dizzy:</p>

                <ul>
                    <li>Use camera files throughout your editing (natively) without transcoding to save time</li>
                    <li>Use camera files to save space. Transcoding often will increase the size of your project a lot</li>
                    <li>Use camera files if they are raw, to maximize color grading success when edit is complete</li>
                    <li>Transcode to mezzanine formats to make our project run smoother if camera footage is causing issues or playback is impossible</li>
                    <li>Transcode to mezzanine formats if you have a lot of different camera formats to eliminate problems such as frame rate, frame size and other issues.</li>
                    <li>Transcode to mezzanine formats if your footage is highly compressed (h.264) to improve potential quality of things like multiple rendering/outputs, color grading and VFX.</li>
                </ul>

                <p>Side Note: <em>As long as your video looks amazing and it grades well - that’s what counts</em></p>
                <p>Hopefully this wasn’t too crazy to absorb! Remember that if you want to delve deeper into this subject and more, check out Training Connection’s video and effects based bootcamp programs and good luck with your own projects! </p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Premiere Pro. We have trained hundreds of students how to use Premiere like pros. Click on the following link to view a sample of our <a class="premiere" href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>