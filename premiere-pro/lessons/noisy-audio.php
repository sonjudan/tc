<?php
$meta_title = "Cleaning noisy audio using Adobe Audition | Training Connection";
$meta_description = "Sending noisy audio from Premiere Pro to Audition for clean-up";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Noisy Audio</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Dealing with Noisy Audio in Premiere Pro</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>If you have noisy audio in Adobe Premiere Pro you can use Audition to clean it up.</p>

                <p>Need to learn Premiere Pro? Our  <a href="/premiere-pro-training.php">certified Premiere Pro classes</a> are the best and fastest way to learn. We run public classes in Chicago and Los Angeles, and onsite classes across the country. Obtain a quote for <a href="/onsite-training.php">onsite Premiere Pro training</a>.</p>

                <p>Right click on the clip in Premiere's Timeline and choose Edit in Adobe Audition.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-7.jpg" width="653" height="443" alt="Edit Clip with Adobe Audition"></p>

                <p>In Audition use the Time Selection tool to highlight the room noise. <a href="/premiere-pro-training-chicago.php">Chicago Adobe Premiere Pro training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-8.jpg" width="260" height="527" alt="Audition Time Selection Tool"></p>

                <p> On the top line of Audition's menu go to Effects/Noise Reduction/Restoration/Noise Reduction Process.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-9.jpg" width="750" height="585" alt="Noise Reduction Process"></p>

                <p>In the effect panel click on Capture Noise Print, then click on select Entire File and then click Apply. <a href="/premiere-pro-training-los-angeles.php">LA Adobe Premiere training classes</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-10.jpg" width="657" height="577" alt="Effect Noise reduction"></p>

                <p>Save in Audition, when you save in Audition it updates the file in Premiere.</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe Premiere Pro Lessons</h4>
                    <ul>
                        <li><a class="premiere" href="/premiere-pro/lessons/text-template.php">How to make a Text Template</a></li>
                        <li><a class="premiere" href="/premiere-pro/lessons/noisy-audio.php"> Cleaning Noisy Audio using Audition</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Premiere Pro. We have trained hundreds of students how to use Premiere like pros. Click on the following link to view a sample of our <a class="premiere" href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>