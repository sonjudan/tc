<?php
$meta_title = "Creating Text Templates in Adobe Premiere Pro | Training Connection";
$meta_description = "Learn how to make a Text Template in Adobe Premiere Pro";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Text Templates</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>How to make a Text Template in Premiere Pro</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>If you're going to make a multi episode  production, you don't have to export the title or re-create the same title each time. Premiere allows you to save your titles as templates which will always be available to you.</p>

                <p><a href="/premiere-pro-training.php">Need training in Premiere Pro</a>? Our <a ]href="/premiere-pro-training.php">Premiere Pro certified, instructor-led training classes</a> are the best and fastest way to learn this program. Public classes are available in Chicago and Los Angeles, plus our trainers can deliver onsite training  right across the country. Obtain a quote for <a href="/onsite-training.php">Premiere Pro onsite training</a>.</p>

                <p>The steps are as follows:</p>
                <p>In Premiere Pro click on Title &gt; New Title &gt; Default Still.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-11.jpg" width="724" height="208" alt="Title Menu "></p>


                <p>Name the Title and then create a new title in title editor.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-12.jpg" width="558" height="597" alt="Name and create a new title"></p>

                <p>Still in title editor click on the templates icon. <img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-13.jpg" width="39" height="28" alt="Templates icon"></p>
                <p>In the template screen click on the upper right wing menu icon. <a href="/premiere-pro-training-los-angeles.php">LA Adobe Premiere training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-14.jpg" width="613" height="354" alt="Premiere Pro template screen"></p>

                <p>Choose Import Current Title as Template.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-15.jpg" width="332" height="252" alt="Import Current Title as Template"></p>


                <p>In save as box name your title then click OK. <a href="/premiere-pro-training-chicago.php">Instructor-led Chicago Adobe training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-16.jpg" width="350" height="151" alt="Name and save title"></p>


                <p>Your Title will then be stored in the User Templates Menu. Then click OK.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-17.jpg" width="620" height="386" alt="User Templates Menu">    </p>

                <p>When you need the title for the next episode in your series you can click on the templates button <img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-13.jpg" width="39" height="28" alt="Template icon"> then click on the title of your template under users presets.</p>

                <p>Double click on your title name under User Templates and the title will load in the title editor. Once in the title editor your text can be changed and updated. </p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe Premiere Pro Lessons</h4>
                    <ul>
                        <li><a href="/premiere-pro/lessons/spot-color-effect.php">Spot Color Effect</a></li>
                        <li><a href="/premiere-pro/lessons/noisy-audio.php"> Cleaning Noisy Audio using Audition</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Premiere Pro. We have trained hundreds of students how to use Premiere like pros. Click on the following link to view a sample of our <a class="premiere" href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>