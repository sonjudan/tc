<?php
$meta_title = "Migrating and Archiving Video Files | Training Connection";
$meta_description = "Learn how to migrate and archive all your Premiere Pro and After Effects projects";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Migrating and Archiving</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Migrating & Archiving your Premiere Pro and After Effects Projects</h1>
                        <h5>By Kristian Gabriel, Adobe ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>For video professionals and enthusiasts, Adobe Premiere and After Effects are two of the most popular weapons of choice when it comes to video editing, animating and/or compositing media. When you understand how each application handles its media, you come to realize that there are some interesting challenges when you wish to move your project to another system or archive it for future use. The following are some fool-proof ways you can safely move or store your projects without broken links and other related issues.</p>
                <p>Need to learn Premiere Pro and After Effects? Our Adobe certified <a href="/premiere-pro-training.php">Premiere Pro classes</a> and <a href="/after-effects-training.php">After Effects classes</a> are the best and fastest way to learn. We run public classes in Chicago and Los Angeles, and onsite classes across the country. Obtain a quote for <a href="/onsite-training.php">onsite Adobe training.</a></p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/000_HEADER_AEPR.jpg" width="839" height="536" alt="Archiving Adobe video projects"></p>

                <h4>Fool-proof method#1: Manual project + Media organization</h4>

                <p>This simple solution requires the user to begin every project with an organizational game plan and maintain that organization throughout the active life of the project. For example, if you were going to start to edit a new film or web series and were going to edit off of a hard drive, you should setup your projects and media in a way that would allow you to keep everything in sync, linked and essentially together. Since both Premiere and After Effects do not “embed” their media within their project files like <em>Photoshop</em> and <em>Illustrator,</em> it’s very easy to break links between files and even lose camera and other graphic media. So, what is the manual solution? </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/001_FOLDERSTRUCTURE_AEPR.jpg" width="839" height="536" alt="Audition Time Selection Tool"></p>

                <h4>Create a Hierarchy of Folders</h4>

                <p>You could create one master folder and then within that folder create a series of folders, separating and containing your Project Files, Raw Video and Sound Media, Title Design Media, Graphic and Animation Design Media, Output Files, etc. Organizing in this way would keep the connection between all your media and project files. <a href="/premiere-pro-training-los-angeles.php">LA Premiere Pro classes</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/002_LINKINGFILES_AEPR.jpg" width="839" height="268" alt="Noise Reduction Process"></p>

                <p>Even if you have your media and projects across multiple drives, this method can be very successful. Just make sure all drives are mounted before opening any projects. If you do accidentally break links with your media, use the very powerful Link Media function in Premiere at FILE &gt; LINK MEDIA, and Replace Media in After Effects at FILE &gt; REPLACE FOOTAGE &gt; FILE. Right-clicking and/or double-clicking on your missing media can also bring up menus or dialogues that will allow you to relink your media as well.</p>

                <h4>Fool-Proof Method#2: Consolidating or Collecting Projects + Media</h4>

                <p>So, you’ve finished your project and now you are ready to store it or archive for future use. Unfortunately, your enthusiasm for editing in Premiere or creating animated titles in After Effects made you forget about organizing and you did not pay any attention to where your media was when you started these projects.&nbsp;And, of course, you imported hundreds of graphics and animation media and can’t remember where they were on your system or hard drives. Does this sound familiar? If so, you could use the powerful collecting and consolidating power within Premiere and After Effects to make your life much, much easier. <a href="/premiere-pro-training-chicago.php">Chicago Premiere Pro training</a>.</p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/004_PREMIERE_AEPR.jpg" width="839" height="268" alt="Effect Noise reduction"></p>


                <h4>Premiere Pro Method: Project Manager</h4>

                <p>The Project Manager in Premiere is the best way to automatically collect and/or consolidate your files. It is located under the File Menu at FILE &gt; PROJECT MANAGER. This menu option allows you to collect all files related to your project and copy them to a single location. One of the biggest benefits of using this function is that it can copy your files from wherever they are, eliminating the need for you to track them all down. Another powerful feature of using the Project Manager is to consolidate your project. Consolidation can greatly reduce the size of your entire project by creating new media of only the used clips in your sequences.  </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/003_AFTEREFFECTS_AEPR.jpg" width="839" height="268" alt=""></p>

                <h4 class="premiere">After Effects Method: Collect Files</h4>

                <p>The After Effects solution to collecting your files is under <em>Dependencies</em>. This can be found at FILE &gt; DEPENDENCIES &gt; COLLECT FILES. Like the Project Manager in Premiere, Collect Files in After Effects will copy your files from wherever they are across multiple locations and re-organize them in a footage folder. In addition to this, it will also make a copy of your After Effects Project and generate a detailed log file of all your files, where they came from, fonts that were used in the project and the plugin effects that were used in your compositions.  </p>

                <p>Well, that’s it! Hopefully you find some use for this information in your own projects and good luck. </p>

                <p>*<em>Both Photoshop and Illustrator have the ability to Embed Media or Link To Media. Embedding Media means that the media is self-contained with the project file, making it very convenient to move the project file around because the media is inside of it. Linking to media means that media your project file uses is external or just linked to media files outside of the project file. This can be excellent if you have multiple projects linking to the same media and wish to make changes to the media which would automatically affect all projects connected to it. The tricky side of linking to media is that your media must move with the project file. If it doesn’t, you could have projects with missing files.</em></p>

                <p>Photoshop Linked &amp; Embedded Smart Objects: <a href="https://adobe.ly/2iRMekN">https://adobe.ly/2iRMekN</a></p>
                <p>Illustrator Linked &amp; Embedded Artwork: <a href="https://adobe.ly/1HzTwky">https://adobe.ly/1HzTwky</a></p>



            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Premiere Pro. We have trained hundreds of students how to use Premiere like pros. Click on the following link to view a sample of our <a class="premiere" href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>