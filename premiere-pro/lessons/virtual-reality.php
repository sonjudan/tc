<?php
$meta_title = "Virtual Reality in Premiere Pro | Training Connection";
$meta_description = "This article discusses Virtual Reality in Adobe Premiere Pro";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Virtual Reality</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Understanding Virtual Reality In Adobe Premiere & Beyond</h1>
                        <h5>By Kristian Gabriel, Adobe ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>The world loves watching powerhouse films like Star Trek, Star Wars, Guardians of the Galaxy, Avengers and so many more because, aside from the action, the hi-tech gadgetry and holographic effects are just so mind-blowingly cool.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/000_Article-Header-Banner.jpg" width="839" height="536" alt="Understainding virtual reality and Premiere Pro"></p>

                <p>Most of these things are meant to be future-tech, however, future-tech is <em>present-tech</em> and a reality when it comes to the revolutionary phenom known as <em>virtual reality</em>. Imagine being able to see and experience almost anything from a first-person perspective as if you are there. Afraid of heights? Let’s say we walk along the edge of a skyscraper, hike up Mount Everest or even skydive. Whether you want to party in Rio De Janeiro or run for your life in a haunted castle, the possibilities are endless. Virtual Reality artificially and entirely “replaces” the world around you. </p>

                <p>Another similar technology known as <em>Augmented </em>or <em>Mixed Reality</em> allows you to see the real-world around you  but then augments it selectively with media like graphics and animations tracked in real-time space. Both technologies are currently blowing up in the biggest way and offer industries like entertainment, consumer services, health technology, space tech and so many more new opportunities,  advancing civilization to a whole new level. </p>

                <p>Looking for a career in film? Why not joining our <a href="/premiere-pro-training.php">Premiere Pro Bootcamp class</a>. 5 days of intensive, hands-on training to master this industry-leading video editing tool.</p>

                <p>How do you fit in? We are all consumers and are consuming this technology or will be soon. Beyond that, those working in some of these industries are also creators. So many hardware packages, cameras and software applications already exist and Adobe is just one of the forward-thinking companies already fusing this technology into their applications like Premiere and Media Encoder. This article focuses on running you up to speed on the different devices, types of VR and how Adobe currently works with this tech. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/001_VR_DEVICESLogo.jpg" width="839" height="268" alt="Virtual Reality devices"></p>

                <h4>The Virtual Reality Devices Out In Market in 2017</h4>

                <p>Currently, at the time of  writing  this article, the big Virtual Reality devices out in the market are:</p>

                <ul>
                    <li>Oculus Rift (Facebook)</li>
                    <li>Google Cardboard, Google Daydream (Google)</li>
                    <li>HTC Vive (HTC &amp; Valve)</li>
                    <li>Razer (FOV: 110°, RES: 2160 x 1200)</li>
                    <li>Gear VR (Samsung)</li>
                    <li>PlayStation VR</li>
                    <li>Microsoft HoloLens (Augmented/Mixed Reality)</li>
                </ul>

                <p>There are other devices available today and other, more powerful devices coming soon, however, these seem to saturate the headlines. All have their strengths and weakness and vary with regards to quality, field of view and other interactive features.</p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <thead>
                    <tr>
                        <td width="106"></td>
                        <td width="71"><strong>GEAR VR</strong></td>
                        <td width="143"><strong>PLAYSTATION VR</strong></td>
                        <td width="75"><strong>HTC VIVE</strong></td>
                        <td width="120"><strong>OCULUS RIFT</strong></td>
                        <td width="134"><strong>DAYDREAM</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td width="106">RESOLUTION</td>
                        <td width="71">2560 x 1440</td>
                        <td width="143">1920 x 1080</td>
                        <td width="75">2160 x 1200</td>
                        <td width="120">2160 x 1200</td>
                        <td width="134">2560 x 14P-XL)
                            1920 x 1080 (Pixel)</p></td>
                    </tr>
                    <tr>
                        <td width="106">PER EYE</td>
                        <td width="71">1280 x 1440</td>
                        <td width="143">960 x 1080</td>
                        <td width="75">1080 x 1200</td>
                        <td width="120">1080 x 1200</td>
                        <td width="134">1280 x 14P-XL)
                            960 x 1080 (Pixel)</p></td>
                    </tr>
                    <tr>
                        <td width="106">FIELD OF VIEW</td>
                        <td width="71">100°</td>
                        <td width="143">100°</td>
                        <td width="75">110°</td>
                        <td width="120">110°</td>
                        <td width="134">90°</td>
                    </tr>
                    </tbody>
                </table>
                <p>The FOV or Field of View in VR determines the part of the world that is visible through the VR device. The wider the Field of View, the more realistic the experience, and if you go wide enough it starts affecting your peripheral vision. This is the future of VR, however, many devices do not have a very wide field of view because there is increased chance for motion sickness in many individuals.&nbsp;So, as you can see in the table above, the highest field of view is 110°. There are devices that are wider but are not as popular. Another reason for the restricted field of view is simply about reducing the cost of the VR device. <a href="/premiere-pro-training-chicago.php">Small instructor-led Premiere Pro classes</a> in Chicago.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/002_MONOSCOPIC_FOOTAGE.jpg" width="839" height="268" alt="Monoscopic footage"></p>

                <h4>The Different Types of Virtual Reality</h4>

                <p>While others do exist, the two most popular types of VR footage are: </p>

                <ul>
                    <li>MONOSCOPIC: Monoscopic footage is also known as 360-degree footage. Through the use of specialty cameras or multiple cameras, Monoscopic is acquired by recording multiple overlapping angles and then “stitching” the different pieces of footage together. The resulting footage is then stretched spherically  like stretching skin around a globe. Putting your VR glasses on is the equivalent of you sticking your head inside that globe and looking around! Note: Being a Stitching Artist is one occupation that has been created since VR began.</li>
                    <li>STEREOSCOPIC: Stereoscopic is captured using a two lens system that kind of simulates the way humans see. The two versions of Stereoscopic VR are Stereoscopic Side-By-Side and Stereoscopic Top/Bottom. The difference between stereoscopic and monoscopic is that stereoscopic can yield more depth, allowing the VR experience to be even more <em><u>real </u></em>by enhancing the fact that certain objects are closer to you and other objects are further away.</li>
                </ul>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/003_STEREOSCOPIC_FOOTAGE.jpg" width="839" height="268" alt="stereoscopic footage"></p>

                <p>Not everything with Stereoscopic is perfection though. One of the biggest issues with this type of footage is that it is difficult to stitch. Even subtle mistakes can seriously handicap this type of footage. As a result, monoscopic tends to take up the bulk of VR experiences out there.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/004_VRSequence_Settings.jpg" width="839" height="268" alt="VR Sequence settings"></p>

                <h4>Working with Adobe Premiere and Virtual Reality</h4>

                <p>Working with Premiere and VR is quite easy. Most of the time, Premiere should recognize your VR footage automatically on import. To use the VR controls, you need to make sure that your Program Monitor is enabled for VR. To do this, click on the “little wrench”  in your monitor settings for your Program Monitor and then go to VR VIDEO &gt; ENABLE. Once you have enabled this, you now need to configure the VR settings to match whatever VR device you are editing for. To do this, go back to your Program Monitor settings and go to VR VIDEO &gt; SETTINGS.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/005_VR_Video_Settings.jpg" width="288" height="222" alt="VR Video Settings"></p>

                <p>If you are using Monoscopic VR footage,  the Stereoscopic View will be grayed out as above. If you are using Stereoscopic VR footage you will be given the choice to choose Side-By-Side or Top/Bottom variations. The remaining values control the FOV or Field of View for the device you are editing for. You want to find the Horizontal and Vertical FOV for your device so you as an editor can see exactly what your audience will see when they put on their VR goggles. Once this is finished, you would edit as normal, keeping in mind that the viewer will be able to look around in full 360° 3D space. To see what your audience sees, just left-click in your Program Monitor and drag the view around to see in 3D space. <a href="/premiere-pro-training-los-angeles.php">Premiere Pro training in Downtown Los Angeles</a>.</p>

                <h4>Troubleshooting VR Guide in Premiere</h4>

                <p>If for some reason, your footage is not recognized and everything seems to be grayed out, here is your plan of attack:</p>

                <ul>
                    <li>STEP 01 - Find out if your footage is either Monoscopic or Stereoscopic (if Stereoscopic, which kind?)</li>
                    <li>STEP 02 - Make sure your footage is “interpreted” as VR footage by selecting one or more pieces of VR footage in your Project Panel, then go to CLIP &gt; MODIFY &gt; INTERPRET VIDEO. Go to the bottom of the Interpret Dialogue menu to VR Properties and click on CONFORM TO.&nbsp; So often, Premiere will properly select either Monoscopic or Stereoscopic, but then is unable to define anything else. If your VR footage is Monoscopic, the Projection type is generally EQUIRECTANGULAR. Under Layout, choose the proper VR footage you have and your video clips are ready to go!</li>
                    <li>STEP 03 - If you are still unable to Enable your Program Monitor for VR viewing, your Sequence may not be ready for VR. To modify this, go to SEQUENCE &gt; SEQUENCE SETTINGS and go to the bottom of the dialogue pop-up and change your VR Properties to match your VR clip settings.
                </ul>

                <h4>Other Adobe Applications &amp; Virtual Reality</h4>

                <p>Adobe Media Encoder and After Effects can also work with virtual reality. In order for your videos to be read as VR Video when you output from Premiere or re-encode VR footage,  they must be “tagged” properly. To do this in Media Encoder, go to your Video Settings Tab and make sure to click on VIDEO IS VR. This turns on the VR tagging feature. You then just select the type of VR footage you are transcoding and all proper tags will be encoded. These tags will allow other software and social media networks like YouTube to recognize the outputs as VR video. </p>

                <p>There are many things we can do to prep footage for VR in After Effects - though popular 3rd party plugins such as <em>Mettle/Skybox or Dashwood Cinema Solutions 360 VR Toolbox</em> are more popular and give you advanced control to convert video footage and your animations to VR. </p>

                <p>The world is getting exciting with all this technology and Adobe is moving very quickly to embrace it. VR is still a new feature across Adobe applications so expect even more features in the future.</p>

                <p>All images created and composited by Kristian Gabriel using Adobe Stock, Techsmith Snag-It and Adobe Photoshop.</p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>We have trained hundreds of students to become top video editing pros. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>