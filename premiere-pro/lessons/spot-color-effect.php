<?php
$meta_title = "Creating Spot Color in Adobe Premiere Pro | Training Connection";
$meta_description = "Learn how to create a Spot Color Effect in Adobe Premiere Pro";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/premier-pro.php">Premiere Pro</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Spot Color Effect</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Adobe Premiere Pro">
                    </div>

                    <div data-aos="fade-up">
                        <h1>How to Create a Spot Color effect in Premiere Pro</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>The spot color effect is when everything on the screen is in black and white except for a certain color.
                    This effect can be done with both stills and video.</p>

                <p>Need training? Our <a href="/premiere-pro-training.php">Premiere Pro classes</a> are the best and fastest way to learn this program. Public classes are available in Chicago and Los Angeles, plus our trainers can deliver onsite training  right across the country. Obtain a quote for <a class="premiere" href="/onsite-training.php">onsite Premiere Pro training</a>.</p>

                <p>1. Have your still or video selected in the timeline.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-1.jpg" width="720" height="640" alt="Premiere Pro Timeline"></p>

                <p>Go to the Effects Panel, type in "Leave Color" and double click on the leave color effect. <a href="/premiere-pro-training-los-angeles.php">Small Premiere Pro classes in Los Angeles</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-2.jpg" width="720" height="308" alt="Premiere Pro Effects panel"></p>

                <p>Go to the Effects Control panel, click and release the eyedropper by color to leave and click on the color that you would like to stay on the screen.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-3.jpg" width="720" height="228" alt="Premiere Pro Effects Control panel"></p>

                <p>Change Match colors drop down from Using RGB to Using Hue. <a href="/premiere-pro-training-chicago.php">Certified Premiere Pro training</a> in Chicago.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-4.jpg" width="446" height="49" alt="Use Hue setting "></p>
                <p>Change Amount to Decolor from 0.0% to 100%.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-5.jpg" width="720" height="224" alt="Decolor setting"></p>


                <p>Everything should be in black and white except for the color which you chose. If there are spots of your designated color which turned black and white you can go to the tolerance setting and scrub the blue numbers to clean add color to those spots.</p>

                <p><img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/Premiere/premiere-pro-6.jpg" width="387" height="28" alt="Set Tolerance setting"> </p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Pr" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Adobe Premiere Pro Lessons</h4>
                    <ul>
                        <li><a class="premiere" href="/premiere-pro/lessons/text-template.php">How to make a Text Template</a></li>
                        <li><a class="premiere" href="/premiere-pro/lessons/noisy-audio.php"> Cleaning Noisy Audio using Audition</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Premiere Pro. We have trained hundreds of students how to use Premiere like pros. Click on the following link to view a sample of our <a class="premiere" href="/testimonials.php?course_id=61">Premiere Pro student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Premiere Pro courses</h4>
                    <ul>
                        <li><a href="/premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></li>
                        <li><a href="/premiere-pro/advanced.php">Premiere Pro Advanced</a></li>
                        <li><a href="/premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>