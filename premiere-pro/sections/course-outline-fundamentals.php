<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    INTRO: A Real-World Overview of Premiere Pro <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>What is Premiere Pro?</li>
                                        <li>New Premiere Pro features</li>
                                        <li>What media types can be rendered or exported with Premiere Pro?</li>
                                        <li>What kind of media can be ingested or captured using Premiere Pro?</li>
                                        <li>What kind of media can be imported into Premiere Pro?</li>
                                        <li>Setting up a Premiere Pro hardware/software digital video work environment</li>
                                        <li>Premiere Pro technical specifications</li>
                                        <li>Premiere Pro workflow: Terminology and best practices</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    BEFORE YOU BEGIN: Premiere Pro Project Management <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>What restrictions exist for a Project’s frame resolution?</li>
                                        <li>What restrictions exist for a Project’s pixel aspect ratio?</li>
                                        <li>What restrictions exist for a Project’s time base?</li>
                                        <li>What restrictions exist for a Project’s frame rate?</li>
                                        <li>What requirements exist for video and/or audio compression?</li>
                                        <li>What are the variables to consider for graphical screen-based assets?</li>
                                        <li>What are the variables to consider for audio assets?</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    UNIT 1: Touring Adobe Premiere Pro <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Exploring nonlinear editing basics in Premiere Pro</li>
                                        <li>Exploring panels and panel options</li>
                                        <li>Customizing the Premiere Pro workspace</li>
                                        <li>Premiere Pro VR Support</li>
                                        <li>Exploring monophonic and stereophonic options</li>
                                        <li>Enabling VR Video Display options</li>
                                        <li>Exploring The Interpret Footage panel</li>
                                        <li>Animating perspective with the Offset effect</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    UNIT 2: Project Settings and Preferences <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Specifying Project settings</li>
                                        <li>Customizing Project settings</li>
                                        <li>Specifying Sequence settings</li>
                                        <li>Setting Premiere Pro Preferences</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    UNIT 3: Ingesting Tapeless Media <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Ingesting media versus capturing footage</li>
                                        <li>Previewing, analyzing, and importing clips with Bridge</li>
                                        <li>Hover scrubbing, previewing, and importing assets with the Media Browser</li>
                                        <li>Subclipping media at import</li>
                                        <li>Additional Import options</li>
                                        <li>Managing media in bins</li>
                                        <li>Mixing different media formats within Sequences</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    UNIT 4: Rough Cuts and Three-Point Edits <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Previsualizing story content in the Project panel</li>
                                        <li>Setting clip poster frames</li>
                                        <li>Using the Selection and Rate Stretch Tools</li>
                                        <li>3-point editing and other editing strategies</li>
                                        <li>Source panel control options</li>
                                        <li>Editing clips in the Source panel</li>
                                        <li>Using the storyboard method to build a rough cut</li>
                                        <li>Timeline editing with the Rolling Edit and Ripple Edit Tools</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    UNIT 5: Creating Basic Titles <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Adding Premiere-generated Synthetic Media</li>
                                        <li>Working with the Type Tool</li>
                                        <li>Creating Text layers</li>
                                        <li>Setting and editing text parameters</li>
                                        <li>Applying text effects</li>
                                        <li>Saving custom styles</li>
                                        <li>Using the Essential Graphics panel</li>
                                        <li>Using the Pen, Rectangle and Ellipse Shape Tools</li>
                                        <li>Creating Shape layers</li>
                                        <li>Working within safe margins</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    UNIT 6: Specialized Editing Tools and Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Linking, unlinking, grouping, and nesting clips</li>
                                        <li>Using the superimpose tracks</li>
                                        <li>Using blend modes</li>
                                        <li>The Razor Tool</li>
                                        <li>Creating freeze-frame effects</li>
                                        <li>Overwrite edits and Insert edits</li>
                                        <li>Ripple deletes</li>
                                        <li>The Zoom, Hand, and Track Select Tools</li>
                                        <li>Using Markers</li>
                                        <li>Working in Audio Time Units vs. SMPTE frames</li>
                                        <li>Ganging the Source and Program CTIs</li>
                                        <li>The Slip Tool</li>
                                        <li>Split edits (J cuts, L cuts) and cutaways (U cuts)</li>
                                        <li>The Slide Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    UNIT 7: Adding Transitions <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Practical application of Transitions</li>
                                        <li>Applying Video Transitions</li>
                                        <li>Editing Transitions with the Effect Controls panel</li>
                                        <li>Using A/B mode to fine-tune a Transition</li>
                                        <li>Applying Transitions to multiple clips at once</li>
                                        <li>Applying Audio Transitions</li>
                                        <li>Using keyframes to adjust audio levels</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    UNIT 8: Animating Clips with Fixed Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Understanding keyframes and tweening</li>
                                        <li>Applying fixed effect property animation to clips</li>
                                        <li>Setting keyframe interpolation</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    UNIT 9: Adding Video Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Compositing Video Effects on clips and Sequences</li>
                                        <li>Keyframing time-based Video Effect properties</li>
                                        <li>Adjusting keyframe interpolation to manipulate speed</li>
                                        <li>Applying Track Matte feathering effects</li>
                                        <li>Applying Video Effects to a Master Clip</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    UNIT 10: Rendering and Exporting <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Testing for final export</li>
                                        <li>Using the Export Settings dialog</li>
                                        <li>Exporting single video frames</li>
                                        <li>Working with the Adobe Media Encoder</li>
                                        <li>Exporting a high-resolution master for recompositing</li>
                                        <li>Exporting H.264-compressed video for YouTube and mobile devices</li>
                                        <li>Adding Adobe Encore Chapter Markers to a Timeline</li>
                                        <li>Exporting video and audio files for DVD mastering</li>
                                        <li>Exporting to Apple Final Cut Pro XML</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    BONUS TRACK UNIT 11: Color	Correction <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Color grading versus color correction</li>
                                        <li>An overview of traditional color correction</li>
                                        <li>Using Lumetri color correction</li>
                                        <li>Using the Fast Color and Three-Way Color Correctors</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>