<?php
$meta_title = "Premiere Pro Affects 2019 Bootcamp | Hands-on certified training";
$meta_description = "Small face-to-face instructor-led Premiere Pro classes taught by Premiere Pro pros.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../premiere-pro-training.php">Premiere Pro Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Bootcamp</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Premiere Pro 2019</h1>
                    <h3 data-aos="fade-up">Bootcamp Training Course</h3>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <p>Our most popular course taking you from Premiere Pro beginner to master in 5 days. The course covers everything from capturing, editing, special effects, graphics, titles to final production. You will also work with audio, as well as integrating Photoshop and Illustrator.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-4" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>5 Days</span>
                        <sup>$</sup>1845<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up" data-aos-delay="350">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Premiere Pro 2019 Bootcamp" data-price="$1845">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Premiere Pro Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Premiere Pro Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/premiere-pro/TC_Premiere Pro_2019_Bootcamp.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-premierepro-bootcamp.png" alt="Premiere Pro 2019 - Bootcamp Training Course" class="" width="230">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up" data-aos-delay="300">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>Classes taught by pros!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>No prior experience of Premiere Pro is needed. Training available on Mac and PC.</p>
                    <p>View our full range of <a href="/premiere-pro-training.php">Adobe Premiere Pro training courses</a>, or see below for the detailed outline for Premiere Pro Bootcamp.</p>

                    <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Premiere Pro 2019 Bootcamp" data-price="$1845">
                        <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                        Book Course
                    </a>
                </div>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/premiere-pro/sections/course-outline-bootcamp.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>