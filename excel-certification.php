<?php
$meta_title = "Microsoft Excel Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Steps to passing a Microsoft Excel Certification Exam.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Excel Certification Exams</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ec" style="background-image: url('/dist/images/banner-excel.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">
                        Microsoft Excel 2019 <br>
                        & 365 Certification <br>
                        Exams
                    </h1>

<!--                    <div data-aos="fade-up">-->
<!--                        <h4 class="mt-1 mb-md-5">in Los Angeles</h4>-->
<!--                    </div>-->
                </div>

                <div class="img-specialist"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/excel-specialist.png" alt="Microsoft Office Excel Specialist" width="340">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-exams">
        <div class="container">
            <div class="section-heading" data-aos="fade-up">
                <h2>2 Microsoft Excel Exams</h2>
            </div>

            <div class="card-deck card-row-sm justify-content-center" data-aos="fade-up">
                <div class="card card-exam-detail">
                    <span class="card-img-top" style="background-image: url(/dist/images/courses/ms-office/excel-exam-1.jpg)"></span>
                    <div class="card-body">
                        <h3>Microsoft Office Specialist:</h3>
                        <h4>Excel Associate (MOS) – Exam MO-200</h4>
                        <h5>Recognition of core skills in Office applications.</h5>
                        <p>What skills are needed to pass exam – <a href="https://docs.microsoft.com/en-us/learn/certifications/mos-excel-2019?wt.mc_id=learningredirect_certs-web-wwl">click here</a>.</p>
                        <p>You can achieve all the skills by completing our Excel Level 1, 2 & 3 classes. We have a package deal</p>
                    </div>
                    <div class="card-footer" data-aos="fade-up">
                        <a href="/excel-training.php" class="card-price">
                            <h3>$900</h3>
                            <span>all 3 classes</span>
                        </a>
                    </div>
                </div>
                <div class="card card-exam-detail">
                    <span class="card-img-top" style="background-image: url(/dist/images/courses/ms-office/excel-exam-2.jpg)"></span>
                    <div class="card-body">
                        <h3>Microsoft Office Specialist:</h3>
                        <h4>Excel Expert (MOS) – Exam MO-201</h4>
                        <h5>Recognition of advanced skills in Excel.</h5>
                        <p>What skills are needed to pass exam – <a href="https://docs.microsoft.com/en-us/learn/certifications/mos-excel-expert-2019">click here</a>.</p>
                        <p>You can achieve all the skills by completing our <br><a href="#">Excel Level 1, 2 & 3 classes</a>.</p>
                    </div>
                    <div class="card-footer" data-aos="fade-up">
                        <a href="/excel-training.php" class="card-price">
                            <h3>$900</h3>
                            <span>all 3 classes</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="section section-step-passexam">
        <div class="container">
            <div class="section-heading"  data-aos="fade-up">
                <h2>Steps to Passing Exam</h2>
            </div>

            <div class="accordion step-passexam-holder" id="accordionPassExam">

                <div class="step-passexam"  data-aos="fade-up">
                    <div class="step-passexam-h collapsed" data-toggle="collapse" data-target="#collapseOne">
                        <div class="step-passexam-l">
                            <span class="step-passexam-ctr">01</span>
                            <div class="step-passexam-block">
                                <h5>
                                    <i class="fas fa-tags"></i>
                                    Purchase exam<br>voucher
                                </h5>

                            </div>
                        </div>
                        <div class="step-passexam-info">
                            <p>Purchase the exam voucher for $120. It is recommended that you purchase an exam voucher with a retake for $140. This way you can repeat the exam should you not pass the first time, and avoid paying the full fee again.</p>
                            <p>The vouchers can be acquired directly from <a class="link-secondary" href="www.certiport.com" target="_blank">www.certiport.com</a> or from a local testing center:</p>
                        </div>
                    </div>
                    <div id="collapseOne" class="step-passexam-body collapse" data-parent="#accordionPassExam">

                    </div>
                </div>

                <div class="step-passexam"  data-aos="fade-up">
                    <div class="step-passexam-h collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                        <div class="step-passexam-l">
                            <span class="step-passexam-ctr">02</span>
                            <div class="step-passexam-block">
                                <h5>
                                    <i class="far fa-calendar-alt"></i>
                                    Book the exam
                                </h5>
                            </div>
                        </div>
                        <div class="step-passexam-info">
                            <p>Use the Certiport testing center locator to find a testing center near to your location.
                                <a class="link-secondary" href="https://portal.certiport.com/locator" target="_blank">https://portal.certiport.com/locator</a></p>
                        </div>
                    </div>
                    <div id="collapseTwo" class="step-passexam-body collapse" data-parent="#accordionPassExam">

                    </div>
                </div>

                <div class="step-passexam"  data-aos="fade-up">
                    <div class="step-passexam-h collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                        <div class="step-passexam-l">
                            <span class="step-passexam-ctr">03</span>
                            <div class="step-passexam-block">
                                <h5>
                                    <i class="fas fa-certificate"></i>
                                    Certification
                                </h5>
                            </div>
                        </div>
                        <div class="step-passexam-info">
                            <p>If you pass you will receive your certification within 2-3 weeks. An electronic copy is generally available after a few days.</p>
                            <p>If you fail you will need to wait a minimum of 24 hours before you can attempt the exam again. If you fail a second time you will need to wait 48 hours to write the exam again.</p>
                        </div>
                    </div>
                    <div id="collapseThree" class="step-passexam-body collapse" data-parent="#accordionPassExam">
<!--                        <div class="cblock">-->
<!--                            <h5>ATG Testing Center</h5>-->
<!--                            <p>Address 1</p>-->
<!--                            <ul class="list-inline">-->
<!--                                <li>City</li>-->
<!--                                <li>State</li>-->
<!--                                <li>Zip</li>-->
<!--                            </ul>-->
<!--                            <h5>Tel: 818.909.7912</h5>-->
<!--                        </div>-->
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="section section-faq mb-md-4">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>FAQ</h2>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5>What is the exam format?</h5>
                    <p>MOS Office 365 and Office 2019 exams as well as MOS 2016 exams include multiple smaller projects to assess student skills within Microsoft Office. You will complete each project and then move to the next project. These small projects test your skills as they would in the real world and validate your understanding of Microsoft Office functionality</p>
                </li>
                <li>
                    <h5>How long is the Excel Certification Exam? </h5>
                    <p>Each exam is 50 minutes in duration.</p>
                </li>
                <li>
                    <h5>How do you prepare for an Excel Certification exam?</h5>
                    <p>You need to learn Microsoft Excel first. The best and faster way to learn Excel is to attend an instructor-led class. You will need to cover beginner, intermediate and advanced Excel topics, that typically this done over 3 days.</p>
                    <p>You can also purchase an exam study guide. Here are the links to each book. </p>
                </li>
            </ul>
            <div class="inline-actions" data-aos="fade-up">
                <a href="https://www.microsoftpressstore.com/store/mos-2016-study-guide-for-microsoft-excel-expert-9780735699427" class="btn btn-secondary btn-lg"><i class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></i> Purchase the Excel Expert exam guide</a>
                <a href="https://www.microsoftpressstore.com/store/mos-2016-study-guide-for-microsoft-excel-9780735699434" class="btn btn-secondary btn-lg"><i class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></i> Purchase the Excel Associate exam guide</a>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-excel-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-excel.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>