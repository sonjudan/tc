$(document).ready(function() {
    $('.js-card-select').select2({
        placeholder: "Card Type",
        allowClear: true
    });

    $('input[name=paymentOption]').change(function(){
        if($(' #js-payment-option-cc').is(":checked")) {
            $('.form-checkout').addClass('is-selected-cc');
        }else {
            $('.form-checkout').removeClass('is-selected-cc');
        }
        if($(' #js-payment-option-paypal').is(":checked")) {
            $('.form-checkout').addClass('is-selected-paypal');
            $('#checkout-submit').html('<i class="iconify mr-1" data-icon="simple-line-icons:basket" data-inline="false"></i> <span>Checkout with Paypal</span>');
        }else {
            $('.form-checkout').removeClass('is-selected-paypal');
            $('#checkout-submit').html('<i class="iconify mr-1" data-icon="simple-line-icons:calendar" data-inline="false"></i> <span>Place Booking</span>');
        }

        if($(' #js-payment-option-paypal').is(":checked")) {
            $('.form-checkout').addClass('is-selected-paypal');
        }else {
            $('.form-checkout').removeClass('is-selected-paypal');
        }
    });
});

