/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
        init: function() {
            // JavaScript to be fired on all pages

            AOS.init({
                disable: function() {
                    var maxWidth = 800;
                    return window.innerWidth < maxWidth;
                }
            });

            $(window).on("scroll", function() {
                var scroll = $(window).scrollTop();

                if(scroll > 50) {
                    $("body").addClass("scrolled");
                    } else {
                    //remove the background property so it comes transparent again (defined in your css)
                    $("body").removeClass("scrolled");
                }

                if (scroll < $(window).height() ) {
                    var offset = scroll / 50;
                    $(".paralax-bg").css({
                    transform: 'translate3d(0%,' + offset + '%, 0) scale(1)'
                    });
                }
            });

            $('.owl-reviews').owlCarousel({
                loop:false,
                nav:false,
                items:1,
                autoplay:true,
                autoplayTimeout:10000,
                autoplayHoverPause:false
            });

            $('.owl-timetable').owlCarousel({
                loop:false,
                nav:true,
                margin:8,
                navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
                responsive:{
                    0:    { items:1 },
                    500:  { items:2 },
                    700:  { items:3 },
                    1100: { items:4 }
                }
            });

            $('.owl-faq').owlCarousel({
                loop:false,
                nav:true,
                margin:8,
                navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
                responsive:{
                    0:   { items:1 },
                    500: { items:2 },
                    700: { items:3 }
                }
            });

            $('.owl-instructors').owlCarousel({
                loop:false,
                nav:true,
                navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
                margin:10,
                stagePadding: 50,
                responsive:{
                    0:    {
                        items:1
                    },
                    768:  {
                        items:2
                    },
                    900:  {
                        items:3
                    },
                    1100: { items:4 },
                    1200: { items:5 }
                }
            });

            $('.owl-courses').owlCarousel({
                loop:false,
                nav:false,
                margin:5,
                autoplay:true,
                autoplayTimeout:4000,
                autoplayHoverPause:false,
                responsive:{
                  0:   { items:1 },
                  500: { items:2 }
                }
            });

            $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
                if (!$(this).next().hasClass('show')) {
                  $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');


                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                  $('.dropdown-submenu .show').removeClass("show");
                });
                return false;
            });

            $('.navbar-nav .dropdown-menu').each(function(index) {
                $(this).prepend('<li class="nav-link-back"><a href="#"><i class="fa fa-arrow-left mr-3"></i> Back</a></li>');
            });

            $('.navbar-nav .dropdown-menu .nav-link-back').on('click', function(e) {
                e.preventDefault();
                $(this).parent('ul').removeClass('show');
            });

            // ANIMATE HAMBURGER MENU
            $('.hamburger-menu').on('click', function() {
                $('.hamburger-menu .bar').toggleClass('animate');
                if($('body').hasClass('open-menu')){
                    $('body').removeClass('open-menu');
                }else{
                    $('body').toggleClass('open-menu');
                }
            });


            $('.nav-item.dropdown').on('hide.bs.dropdown', function (e) {
                if (e.clickEvent) {
                    e.preventDefault();
                }
            });


            $(function(){
                var hash = window.location.hash;

                /// TAB
                hash && $('ul.nav a[href="' + hash + '"]').tab('show');
                // $('html,body').scrollTop();

                $('.nav-tabs a').click(function (event) {
                    event.preventDefault();
                    $(this).tab('show');
                    var scrollmem = $('body').scrollTop();
                    //window.location.hash = this.hash;
                    $('html,body').scrollTop();
                });

                /// Acourdion
                $(hash).collapse('show');

                $('.accordion-collapse').on('shown.bs.collapse', function(e) {
                    var $panel = $(this).closest('.accordion-item');
                    $('html,body').animate({
                        scrollTop: $panel.offset().top - 78
                    }, 500);
                });
            });

            ////// HOMEPAGE

            $(function(){
                $('.hiddenMail').on('click',function(event){
                  event.preventDefault();
                  $(this).off("click");
                  var email = $(this).attr("data-email").replace(/AT/,'@').replace(/DOT/,'.');
                  $(this).removeClass("hiddenMail");
                  $(this).html(email);
                  $(this).attr("href","mailto:"+email);
                });
            });

            /// Smooth scrolling when clicking an anchor link
            /// https://stackoverflow.com/questions/7717527/smooth-scrolling-when-clicking-an-anchor-link

            $(function(){
                var $root = $('html, body');

                $('.js-anchor-scroll').click(function () {
                  $root.animate({
                      scrollTop: $( $.attr(this, 'href') ).offset().top - 55
                  }, 500);

                  return false;
                });
            });

            ///// Modal Link

            $(document).ready(function () {
                $('.modal').each(function () {
                    const modalId = '#' + $(this).attr("id");

                    if (window.location.href.indexOf(modalId) !== -1) {
                        $(modalId).modal('show');
                    }
                });
            });

            ///// Training Landing pages

            $(document).ready(function() {
                $('.js-location-select').select2({
                    placeholder: "Select Location*",
                    allowClear: true
                });
                $('.js-class-select').select2({
                    placeholder: "Select Class*",
                    allowClear: true
                });
                $('.js-course-select').select2({
                    placeholder: "Select Course*",
                    allowClear: true
                });
                $('.js-course-student').select2({
                    placeholder: "Choose Name",
                    allowClear: true
                });
            });

            ///// CART

            $(function(){
                $('.js-remove').each(function () {
                    $(this).on('click',function(event){
                        event.preventDefault();
                        $(this).parent('.js-remove-parent').remove();
                        $(this).parent().parent('.js-remove-parent').remove();
                    });
                });
            });

            ///// Articles Load more

            $(function(){

                $( ".js-load-list" ).each(function(index) {

                    function loadMore(){
                        $(".post-default.d-none").slice(0,10).removeClass("d-none");

                        if ( !$('.post-default.d-none').length > 0) {
                            $(".js-load-btn").parent().addClass('d-none');
                        }
                    }

                    loadMore();

                    $(".js-load-btn").on("click",loadMore);
                });

            });


            //// STATIC to remove on dynamic development

            // data-* attributes to scan when populating modal values
            var ATTRIBUTES = ['course', 'classes', 'timetable', 'bb', 'address', 'price', 'course-list'];

            $('.js-modal-data').on('click', function (e) {


                // convert target (e.g. the button) to jquery object
                var $target = $(e.target);

                // modal targeted by the button
                var modalSelector = $target.data('target');

                // iterate over each possible data-* attribute
                ATTRIBUTES.forEach(function (attributeName) {
                    // retrieve the dom element corresponding to current attribute
                    var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
                    var dataValue = $target.data(attributeName);

                    // if the attribute value is empty, $target.data() will return undefined.
                    // In JS boolean expressions return operands and are not coerced into
                    // booleans. That way is dataValue is undefined, the left part of the following
                    // Boolean expression evaluate to false and the empty string will be returned
                    $modalAttribute.text(dataValue || '');
                });
            });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'blog': {
      init: function() {

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
