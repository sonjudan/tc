<?php
$meta_title = "Microsoft Excel Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Passing Microsoft Excel Certification Exams. Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted">
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="" itemprop="url">
                    <span itemprop="title">Class Evaluation and Certificate</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page masterhead-md" style="background-image: url('/dist/images/banner-excel.jpg');">
    <div class="container">
        <div class="book-training-holder align-items-md-center">
            <div class="masterhead-copy">
                <h1 data-aos="fade-up">
                    Certificate of Course Completion <br>& Class evaluation
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row row-sm">
            <div class="col-6">
                <div class="card-cc card-cc-step"  data-aos="fade-up">
                    <span>
                        <h4>STEP 01</h4>
                        <span class="card-cc-img"><img src="/dist/images/courses/ms-office/icon-certificate-evaluation.png" alt=""></span>
                        <h4>Certificate of <br>course completion</h4>
                    </span>
                </div>
            </div>
            <div class="col-6">
                <div class="card-cc card-cc-step"  data-aos="fade-up">
                    <span>
                        <h4>STEP 02</h4>
                        <span class="card-cc-img"><img src="/dist/images/courses/ms-office/icon-complete-evaluation.png" alt=""></span>
                        <h4>Complete a class <br>evaluation</h4>
                    </span>
                </div>
            </div>
        </div>

        <div class="card-cc card-cc-request-form"  data-aos="fade-up">
            <div class="card-cc-heading">
                <h2 class="title-l5">Request a Certificate of <br>Course Completion</h2>
            </div>

            <div class="tab-content"  data-aos="fade-up">
                <div class="tab-pane fade show active" id="eval-form_ClassID" role="tabpanel" aria-labelledby="eval-form_ClassID-tab">
                    <div class="row justify-content-md-center">
                        <div class="col col-md-7 col-xl-5">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Class ID">
                            </div>
                            <div class="form-group-actions">
                                <button class="btn btn-secondary btn-block btn-md"
                                        data-toggle="tab" data-target="#eval-form_public" role="tab" aria-controls="eval-form_public" aria-selected="false">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="eval-form_public" role="tabpanel" aria-labelledby="eval-form_public-tab">
                    <div class="row justify-content-md-center">
                        <div class="col col-md-7 col-xl-5">

                            <?php if( isset($_GET['type'] ) == 'onsite' ) : ?>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter Name">
                                </div>
                            <?php else : ?>
                                <div class="form-group">
                                    <select class="form-control js-course-student" name="course" placeholder="Choose Name">
                                        <option value=""></option>
                                        <option value="1">Ben Smith</option><option value="2">Student Name2</option><option value="3">Student Name3</option><option value="4">Student Name4</option><option value="5">Student Name5</option>
                                    </select>
                                </div>
                            <?php endif; ?>


                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Email Address">
                            </div>
                            <div class="form-group-actions">
                                <button class="btn btn-secondary btn-block btn-md"
                                        data-toggle="tab" data-target="#eval-tc" role="tab" aria-controls="eval-tc" aria-selected="false">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="tab-content">

            <div class="tab-pane fade" id="eval-tc" role="tabpanel" aria-labelledby="eval-tc-tab">
                <div class="card-cc card-cc-thankyou">
                    <div class="row-icon-ty">
                        <i class="icon-ty mr-sm-3 mt-sm-2"></i>
                        <div class="row-icon-ty-body">
                            <h5>Thank you for choosing Training Connection!</h5>
                            <p>You will receive your certificate of course <br>completion via email shortly. </p>

                            <button class="btn btn-secondary btn-md pl-md-5 pr-md-5"
                               data-toggle="tab" data-target="#eval-form_eval" role="tab" aria-controls="eval-form_eval" aria-selected="false">

                                Complete a Class Evaluation
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="eval-form_eval" role="tabpanel" aria-labelledby="eval-form_eval-tab">
                <div class="card-cc card-cc-evaluate">
                    <div class="card-cc-heading">
                        <h2 class="title-l5">Class Evaluation</h2>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col col-xl-10">
                            <div class="content-default">

                                <div class="table-responsive">
                                    <?php if( isset($_GET['type'] ) == 'onsite' ) : ?>
                                        <h4 class="mb-md-4">Ben Smith</h4>
                                        <div class="form-group-range">
                                            <label>1. Your overall satisfaction with this course.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>2. The course length and pace was appropriate.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>3. The instructor was knowledgeable and engaging.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>4. The course provided an appropriate balance of hands-on, lecture and discussion.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>5. Instructor provided good exercises to re-enforce concepts.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>6. I would recommend this course to others</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>

                                    <?php else : ?>

                                        <h4 class="mb-md-4">Peter Barnes</h4>
                                        <div class="form-group-range">
                                            <label>1. The training center staff were efficient and friendly.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>2. The training center was clean.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>3. The training computers were of high standard.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>4. Overall, I was satified with the course.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>5. The course length and pace was appropriate.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>6. The instructor was knowledgeable and engaging.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>7. The course provided an appropriate balance of hands-on, lecture and discussion.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>8. Instructor provided good exercises to re-enforce concepts.</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                        <div class="form-group-range">
                                            <label>9. I would recommend this course to others</label>
                                            <div class="range-offset-l">
                                                <input type="range" min="1" max="5" value="1" labels="Strongly agree, Agree, Neutral, Disagree, Strongly Disagree">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group mt-sm-3 mb-3">
                                    <textarea name="" id="" cols="30" rows="3" class="form-control form-control-lg" placeholder="Add Comment"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="custom-checkbox custom-checkbox-3"><input type="checkbox" name="business_skills"><i></i> Make my comment anonymous.</label>
                                    <label class="custom-checkbox custom-checkbox-3"><input type="checkbox" name="business_skills" checked><i></i> My comment can be used for marketing purposes.</label>
                                </div>

                                <button id="submit" class="btn btn-secondary btn-md pl-md-5 pr-md-5"
                                        data-toggle="tab" data-target="#eval-tc2" role="tab" aria-controls="eval-tc2" aria-selected="false">
                                    Submit your class evaluation
                                </button>

                                <script>
                                    $('#submit').click(function(){
                                        $('body, html').animate({scrollTop:$('.card-cc-request-form').offset().top-90}, 'slow');
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="eval-tc2" role="tabpanel" aria-labelledby="eval-tc-tab">
                <div class="card-cc card-cc-thankyou">
                    <div class="row-icon-ty align-items-center">
                        <i class="icon-ty mr-sm-2 mt-sm-2"></i>
                        <div class="row-icon-ty-body">
                            <h5 class="mb-0">Thank you for taking the time to submit your feedback.</h5>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="mt-4"></div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-excel-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-excel.php'; ?>

<script src="https://rawgit.com/andreruffert/rangeslider.js/develop/dist/rangeslider.min.js"></script>
<script>
    $(function() {
        var cssClasses = [
            'rangeslider--value-1',
            'rangeslider--value-2',
            'rangeslider--value-3',
            'rangeslider--value-4',
            'rangeslider--value-5'
        ];

        $('input[type="range"]').rangeslider({
            // Feature detection the default is `true`.
            // Set this to `false` if you want to use
            // the polyfill also in Browsers which support
            // the native <input type="range"> element.
            polyfill: false,

            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',

            // Callback function
            onInit: function() {
                $rangeEl = this.$range;
                // add value label to handle
                var $handle = $rangeEl.find('.rangeslider__handle');
                var handleValue = '<div class="rangeslider__handle__value">' + this.value + '</div>';
                $handle.append(handleValue);

                // get range index labels
                var rangeLabels = this.$element.attr('labels');
                rangeLabels = rangeLabels.split(', ');

                // add labels
                $rangeEl.append('<div class="rangeslider__labels"></div>');
                $(rangeLabels).each(function(index, value) {
                    $rangeEl.find('.rangeslider__labels').append('<span class="rangeslider__labels__label">' + value + '</span>');
                })
            },

            // Callback function
            onSlide: function(position, value) {
                var $handle = this.$range.find('.rangeslider__handle__value');
                $handle.text(this.value);

                this.$range.removeClass(cssClasses);
                this.$range.addClass('rangeslider--value-' + this.value);
            },

            // Callback function
            onSlideEnd: function(position, value) {
            }
        }).on('input', function() {

        });
    });

</script>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>