<?php
$meta_title = "Apple Keynote Training Classes in Los Angeles | Training Connection";
$meta_description = "Apple Keynote for Mac classes in Los Angeles. For great deals call 888.815.0604. Apple certified.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted" >
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/final-cut-pro-training.php" itemprop="url">
                    <span itemprop="title">Apple Keynote Training</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-keynote.jpg');">
    <div class="container">
        <div class="masterhead-copy">
            <h1 data-aos="fade-up" >Apple Keynote Training<br>
                Los Angeles
            </h1>

            <div data-aos="fade-up" data-aos-delay="100">
                <p>Private  Group Keynote training classes</p>

                <a href="#section-course-form" class="btn btn-secondary btn-lg js-anchor-scroll" data-aos="fade-up" data-aos-delay="200">
                    <i class="fas fa-users mr-2" aria-hidden="true"></i>
                    Obtain a Quote for Group Training
                </a>
            </div>
        </div>
    </div>
</div>

<div class="section section-instructors">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="mb-4" data-aos="fade-up">Apple Keynote Classes</h2>
            <p data-aos="fade-up">Keynote for Mac makes it simple to create and deliver stunning presentations. Easy-to-use visual tools, interactive charts, reflections and picture frames and cinema quality animations will bring your ideas to life.</p>
            <p data-aos="fade-up" data-aos-delay="100"><strong><a class="text-link-blue js-anchor-scroll" href="#section-course-form">Obtain a quotation for group keynote training.</a></strong></p>
        </div>

    </div>
</div>

<div id="section-book-course" class="section section-training-options pb-0">
    <div class="container">
        <div class="section-heading align-center mb-0" data-aos="fade-up">
            <h2>Detailed Course Description</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">

            <div class="accordion-classes g-text-Apple Keynote" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default pb-md-5">
                        <div class="post-img"><img src="/dist/images/icons/icon-apple-keynote.png" alt="Apple Keynote Logo"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Apple Keynote Fundamentals
                                </a>
                            </h3>
                            <p>On this beginners class, students will learn to create beautiful presentations. You will drop in photos, audio and video, display                              data in interactive charts, and punch up your presentation with engaging animations. You will then publish your video for multiple
                            different uses.</p>

                            <div class="group-action-inline">
                                <a href=".section-course-form" class="btn btn-md btn-secondary js-anchor-scroll" target="_blank">
                                    <i class="fas fa-users mr-2" aria-hidden="true"></i>
                                    Obtain a quotation for group training
                                </a>
                                <a href="/downloads/keynote/Keynote.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup></sup>
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse" aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Outlining a Presentation<br></h5>
                                        <ul>
                                            <li>Creating a Presentation</li>
                                            <li>Customizing Your Workspace</li>
                                            <li>Adding Slides</li>
                                            <li>Opening a PowerPoint Presentation in Keynote</li>
                                            <li>Reordering and Deleting Slides</li>
                                            <li>Working in Outline View</li>
                                            <li>Adding Slide Transitions</li>
                                            <li>Using Builds</li>
                                            <li>Applying Themes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Charts</h5>
                                        <ul>
                                            <li>Animating a title slide in Keynote</li>
                                            <li>Creating and enhancing charts in your Keynote presentation</li>
                                            <li>Animating a table in Keynote</li>
                                            <li>Adding content from a Ms Word document into your Keynote presentation</li>
                                            <li>Saving a PowerPoint or PDF File from Keynote</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Making Media-Rich Presentations</h5>
                                        <ul>
                                            <li>Making a Slide Background</li>
                                            <li>Updating a Paragraph Style</li>
                                            <li>Creating New Slides by Dragging Files</li>
                                            <li>Reordering Slides Using the Light Table</li>
                                            <li>Using Transitions to Create Moods</li>
                                            <li>Adding an Audio Soundtrack</li>
                                            <li>Controlling a Presentation with Builds</li>
                                            <li>Layering Objects with Master Slides</li>
                                            <li>Building Animations Using Actions</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Developing a Custom Theme</h5>
                                        <ul>
                                            <li>Selecting Master Slides</li>
                                            <li>Updating Paragraph Styles</li>
                                            <li>Modifying Image Placeholders</li>
                                            <li>Placing Objects on Master Slides</li>
                                            <li>Saving Themes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Rehearsing and delivering your presentation</h5>
                                        <ul>
                                            <li>Playing a Slideshow</li>
                                            <li>Rehearsing a Slideshow</li>
                                            <li>Using an iOS Device as a Remote Control</li>
                                            <li>Recording a Narrated, Self-Playing Presentation</li>
                                            <li>Creating a Self-Playing Slideshow</li>
                                            <li>Designing a Presentation with Links</li>
                                            <li>Making Keynote Handouts</li>
                                            <li>Performing Presentation Preflight Checks</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="inner-section-nobg">
    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>
</div>
<div id="section-course-form" class="section section-course-form">
    <div class="container">
        <div class="section-heading w-auto">
            <h2 class="mb-3" data-aos="fade-up">Group Keynote  Training Quote</h2>
            <p data-aos="fade-up" data-aos-delay="100">
                We offer group training in Apple Keynote for Mac. This can be delivered onsite at your premises, at  our training center in Los Angeles or via webinar.

                <br>
                <strong>Fill out the form below to receive pricing.</strong>
            </p>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="200">
            <form action="" class="form-group-training">

                <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                        <h3><input type="radio" name="training">
                            <i></i> Online Webinar</h3>

                    </label>
                </div>

                <div class="row">
                    <div class="col-md-6 pr-1 mb-3">
                        <h4>Step 2: Choose your Course/s</h4>

                        <div class="option-group-training inline-block">
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>Apple Keynote Fundamentals <span>1 Day</span></h3>
                            </label>
                          
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Step 3: Enter Details</h4>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="No. of Trainees*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Company Name*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone no*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="City*">
                        </div>

                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Zip*">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>
