<?php
$meta_title = "Class rescheduling | Training Connection";
$meta_description = "Training Connection is computer and business skills training school. We believe hands-on training taught face-to-face is the best way to learn.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Class rescheduling</li>
                </ol>
            </nav>
        </div>

        <div class="section-content">

            <div class="container">

                <div class="tab-content">
                    <div class="tab-pane active" id="class-reschedule-1" role="tabpanel">

                        <div class="page-intro">
                            <div class=" intro-copy">
                                <h1 id="date"></h1>
                                <h1 data-aos="fade-up" >Class Rescheduling</h1>
                                <h5 class="" data-aos="fade-up">You have elected to reschedule the following class.</h5>
                            </div>
                        </div>

                        <div class="copy-sm pb-5 pt-0" data-aos="fade-up">

                            <div class="card-reschedule g-text-excel">
                                <div class="card-reschedule-class">
                                <span class="card-icon">
                                    <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Changes in MS Excel">
                                </span>
                                    <div class="card-details">
                                        <header class="">
                                            <h6 class="title-l4">Jeanne Whitfield</h6>
                                            <h5>Excel Level 1 - Introduction</h5>
                                            <p>Chicago | <?php echo date('F j, Y', strtotime('2019-11-18')); ?></p>
                                            <input id="classDate" type="hidden" value="2019-11-18">
                                        </header>
                                        <aside>

                                            <select id="day_selected" class="form-control">
                                                <option value="" disabled selected>New class date</option>
                                                <option value="2019-11-22"><?php echo date('F j, Y', strtotime('2019-11-22')); ?></option>
                                                <option value="2019-11-22"><?php echo date('F j, Y', strtotime('2019-12-02')); ?></option>
                                                <option value="2019-11-22"><?php echo date('F j, Y', strtotime('2019-12-24')); ?></option>
                                            </select>
                                        </aside>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="card-l">
                                        <h5 class="title-l4 js-lbl-fee d-none">Rescheduling fee</h5>

                                        <a href="/checkout.php?p=1" id="btnCheckout" class="btn btn-secondary btn-md pl-md-5 pr-md-5" disabled>
                                            Confirm new class date
                                        </a>
                                    </div>
                                    <div class="card-r">
                                        <h3 class="h-price">Free</h3>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </main>


    <div class="modal fade modal-checkout" id="modalCheckout" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="card-reschedule">
                        <div class="card-reschedule-class">
                                <span class="card-icon">
                                    <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Changes in MS Excel">
                                </span>
                            <div class="card-details">
                                <header>
                                    <h5>Excel Level 1 - Introduction</h5>
                                    <p>Chicago | <?php echo date('F j, Y', strtotime('2019-11-18')); ?></p>

                                </header>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="card-l">
                                <h5 class="title-l4 js-lbl-fee d-none">Rescheduling fee</h5>

                                <a href="/checkout.php" class="btn btn-secondary btn-md pl-md-5 pr-md-5">
                                    <i class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></i>
                                    Checkout
                                </a>
                            </div>
                            <div class="card-r">
                                <h3 class="h-price"></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-5"></div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>

