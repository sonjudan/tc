<?php
$meta_title = "Repeat Policy | Training Connection";
$meta_description = "Steps to book your free repeat class";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">How to Book a Repeat</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Repeat Policy</h1>
                    <h4 data-aos="fade-up" data-aos-delay="100">Please read the following steps carefully to book your FREE repeat</h4>

                </div>
            </div>
        </div>

        <div class="section-content-l2">
            <div class="container">
                <div class="card-default copy-sm">
                    <ul>
                        <li>1. Check our website for a suitable date.</li>
                        <li>2. Contact us via email the day before the class is due to commence. (or on the Friday before if the class commences on a Monday)</li>
                        <li>3. A sales representative will be able to confirm via return email whether there is a seat available.</li>
                        <li>4. Please don't forget to bring your original training manual with you.</li>
                    </ul>
                </div>
                <div class="card-default copy-sm">
                    <h4>All Repeats are subject to the following conditions:</h4>
                    <ul>
                        <li>1. Repeats are subject to classes running with other paying students and us having availability on the chosen class.</li>
                        <li>2. Repeats cannot be taken more than 6 months after the date of the original course.</li>
                        <li>3. Repeats cannot be transferred and are only valid for the exact same course.</li>
                        <li>4. If Training Connection changes courseware, repeat students will be required to purchase the new courseware at cost price.</li>
                        <li>5. Repeats are only available for public class bookings and not onsite or private training bookings.</li>
                    </ul>
                </div>
            </div>
        </div>



    </main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>