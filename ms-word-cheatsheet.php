<?php
$meta_title = "Microsoft Word Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Passing Microsoft Word Certification Exams. Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Word Cheat Sheets & shortcuts</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ec" style="background-image: url('/dist/images/banner-word.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">
                        Word 2019 <br>
                        Cheat Sheets <br>
                        & shortcuts
                    </h1>
                </div>

                <div class="img-specialist"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/word-specialist.png" alt="Microsoft Word Specialist" width="340">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-exams">
        <div class="container">
            <div class="section-heading align-center" data-aos="fade-up">
                <h2>Word 2019 Cheat Sheets & Shortcuts</h2>
            </div>

            <div class="card-deck card-row-sm card-deck-fblock">
                <div class="card card-fblock card-fblock-word" data-aos="fade-up">
                    <h3 class="card-title">The Complete Microsoft <br>
                    Word Cheat Sheet</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>The complete Microsoft <br class="d-sm-none">Word CheatSheet</p>
                        <a href="/downloads/word/Word-CheatSheet.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>