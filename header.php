<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/functions.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php
            if(isset($meta_title) ) {
                echo $meta_title;
            }else {
                echo "Chicago & Los Angeles Computer and Business Skills Training Classes | Training Connection";
            }
        ?></title>
    <meta name="description" content="<?php
        if(isset($meta_description)) {
            echo $meta_description;
        }else {
            echo "Face to face instructor-led computer and business skills classes. Don't settle for an average class!";
        }
    ?>" />

    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">
    <link rel='stylesheet'  href='/dist/styles/main.css?v=1.6.8' type='text/css' media='all' />

    <script src="https://kit.fontawesome.com/7ff48b9e03.js"></script>
    <script type='text/javascript' src='/dist/scripts/jquery.js'></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-main">
    <div class="container">

        <div class="navbar-brand" itemscope itemtype="http://schema.org/Organization">
            <a itemprop="url" href="/">
                <img itemprop="logo" src="/dist/images/logo.svg" alt="Training Connection">
                <i class="sr-only" itemprop="name">Training Connection</i>
            </a>
        </div>

        <div class="mobile-nav-right">
            <a href="tel:8888150604" class="nav-phone"><span>CALL</span> <i class="iconify  mr-2" data-icon="simple-line-icons:call-out" data-inline="false"></i> <span>(888) 815-0604</span></a>

            <div class="hamburger-menu">
                <div class="bar"></div>
            </div>
        </div>




        <div class="navbar-js-holder">
            <div class="ml-auto">
                <div class="navbar-top">
                    <a href="tel:8888150604" class="nav-phone">CALL <span class="iconify mr-2" data-icon="simple-line-icons:call-out" data-inline="false"></span>(888) 815-0604</a>

                    <a href="/" class="side-nav-logo">
                        <img itemprop="logo" src="/dist/images/logo-tc-icon.svg" alt="Training Connection">
                    </a>

                    <a class="nav-cart" href="#modal-cart" data-target=".modal-cart" data-toggle="modal">
                        <span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span>
                        <span class="badge badge-primary">3</span>
                    </a>
                </div>
                <ul class="navbar-nav ">
                    <li class="nav-item nav-item-home active">
                        <a class="nav-link" href="/"><span class="iconify" data-icon="simple-line-icons:home" data-inline="false"></span> </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Courses
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="dropdown">
                                <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    All Courses</a>

                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class=" dropdown">
                                        <a href="#" class="dropdown-item dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Adobe</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/acrobat-training.php">Acrobat</a></li>
                                            <li><a class="dropdown-item" href="/after-effects-training.php">After Effects</a></li>
                                            <li><a class="dropdown-item" href="/captivate-training.php">Captivate</a></li>
                                            <li><a class="dropdown-item" href="/dreamweaver-training.php">Dreamweaver</a></li>
                                            <li><a class="dropdown-item" href="/illustrator-training.php">Illustrator</a></li>
                                            <li><a class="dropdown-item" href="/indesign-training.php">InDesign</a></li>
                                            <li><a class="dropdown-item" href="/photoshop-training.php">Photoshop</a></li>
                                            <li><a class="dropdown-item" href="/premiere-pro-training.php">Premiere Pro</a></li>
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Apple</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                             <li><a class="dropdown-item" href="/final-cut-pro-training.php">Final Cut Pro</a></li>
                                             <li><a class="dropdown-item" href="/keynote-training.php">Keynote</a></li>
                                             <li><a class="dropdown-item" href="/logic-pro-training.php">Logic Pro</a></li>
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Business Skills</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/business-communication-training.php">Business Communication</a></li>
                                            <li><a class="dropdown-item" href="/business-etiquette-training.php">Business Etiquette</a></li>
                                            <li><a class="dropdown-item" href="/business-leadership-training.php">Business Leadership</a></li>
                                            <li><a class="dropdown-item" href="/business-writing-training.php">Business Writing</a></li>
                                            <li><a class="dropdown-item" href="/customer-service-training.php">Customer Service</a></li>
                                            <li><a class="dropdown-item" href="/grammar-training.php">Grammar</a></li>
                                            <li><a class="dropdown-item" href="/presentations-training.php">Presentations</a></li>
                                            <li><a class="dropdown-item" href="/project-management-training.php">Project Management</a></li>
                                            <li><a class="dropdown-item" href="/time-management-training.php">Time Management</a></li>
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Microsoft Office</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/access-training.php">Access</a></li>
                                            <li><a class="dropdown-item" href="/excel-training.php">Excel</a></li>
                                            <li><a class="dropdown-item" href="/outlook-training.php">Outlook</a></li>
                                            <li><a class="dropdown-item" href="/ms-powerpoint-training.php">PowerPoint</a></li>
                                            <li><a class="dropdown-item" href="/ms-project-training.php">Project</a></li>
                                            <li><a class="dropdown-item" href="/visio-training.php">Visio</a></li>
                                            <li><a class="dropdown-item" href="/ms-word-training.php">Word</a></li>
                                        </ul>
                                    </li>
                                    <li class=" dropdown">
                                        <a href="#" class="dropdown-item dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Web Development</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/html-training.php">HTML</a></li>
                                            <li><a class="dropdown-item" href="/javascript-training.php">JavaScript</a></li>
                                            <li><a class="dropdown-item" href="/php-mysql-training.php">PHP</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Chicago Courses
                                </a>

                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Adobe</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/acrobat-training-chicago.php">Acrobat</a></li>
                                            <li><a class="dropdown-item" href="/after-effects-training-chicago.php">After Effects</a></li>
                                            <li><a class="dropdown-item" href="/captivate-training-chicago.php">Captivate</a></li>
                                            <li><a class="dropdown-item" href="/dreamweaver-training-chicago.php">Dreamweaver</a></li>
                                            <li><a class="dropdown-item" href="/illustrator-training-chicago.php">Illustrator</a></li>
                                            <li><a class="dropdown-item" href="/indesign-training-chicago.php">InDesign</a></li>
                                            <li><a class="dropdown-item" href="/photoshop-training-chicago.php">Photoshop</a></li>
                                            <li><a class="dropdown-item" href="/premiere-pro-training-chicago.php">Premiere Pro</a></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Business Skills</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/business-communication-training-chicago.php">Business Communication</a></li>
                                            <li><a class="dropdown-item" href="/business-etiquette-training-chicago.php">Business Etiquette</a></li>
                                            <li><a class="dropdown-item" href="/business-leadership-training-chicago.php">Business Leadership</a></li>
                                            <li><a class="dropdown-item" href="/business-writing-training-chicago.php">Business Writing</a></li>
                                            <li><a class="dropdown-item" href="/customer-service-training-chicago.php">Customer Service</a></li>
                                            <li><a class="dropdown-item" href="/grammar-training-chicago.php">Grammar</a></li>
                                            <li><a class="dropdown-item" href="/presentations-training-chicago.php">Presentations</a></li>
                                            <li><a class="dropdown-item" href="/project-management-training-chicago.php">Project Management</a></li>
                                            <li><a class="dropdown-item" href="/time-management-training-chicago.php">Time Management</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Microsoft Office</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/access-training-chicago.php">Access</a></li>
                                            <li><a class="dropdown-item" href="/excel-training-chicago.php">Excel</a></li>
                                            <li><a class="dropdown-item" href="/outlook-training-chicago.php">Outlook</a></li>
                                            <li><a class="dropdown-item" href="/ms-powerpoint-training-chicago.php">PowerPoint</a></li>
                                            <li><a class="dropdown-item" href="/ms-project-training-chicago.php">Project</a></li>
                                            <li><a class="dropdown-item" href="/visio-training-chicago.php">Visio</a></li>
                                            <li><a class="dropdown-item" href="/ms-word-training-chicago.php">Word</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Web Development</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/html-training-chicago.php">HTML</a></li>
                                            <li><a class="dropdown-item" href="/javascript-training-chicago.php">JavaScript</a></li>
                                            <li><a class="dropdown-item" href="/php-mysql-training-chicago.php">PHP</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    LA Courses
                                </a>

                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Adobe</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/acrobat-training-los-angeles.php">Acrobat</a></li>
                                            <li><a class="dropdown-item" href="/after-effects-training-los-angeles.php">After Effects</a></li>
                                            <li><a class="dropdown-item" href="/captivate-training-los-angeles.php">Captivate</a></li>
                                            <li><a class="dropdown-item" href="/dreamweaver-training-los-angeles.php">Dreamweaver</a></li>
                                            <li><a class="dropdown-item" href="/illustrator-training-los-angeles.php">Illustrator</a></li>
                                            <li><a class="dropdown-item" href="/indesign-training-los-angeles.php">InDesign</a></li>
                                            <li><a class="dropdown-item" href="/photoshop-training-los-angeles.php">Photoshop</a></li>
                                            <li><a class="dropdown-item" href="/premiere-pro-training-los-angeles.php">Premiere Pro</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apple</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/final-cut-pro-training.php">Final Cut Pro</a></li>
                                            <li><a class="dropdown-item" href="/keynote-training.php">Keynote</a></li>
                                            <li><a class="dropdown-item" href="/logic-pro-training.php">Logic Pro</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Business Skills</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/business-communication-training-los-angeles.php">Business Communication</a></li>
                                            <li><a class="dropdown-item" href="/business-etiquette-training-los-angeles.php">Business Etiquette</a></li>
                                            <li><a class="dropdown-item" href="/business-leadership-training-los-angeles.php">Business Leadership</a></li>
                                            <li><a class="dropdown-item" href="/business-writing-training-los-angeles.php">Business Writing</a></li>
                                            <li><a class="dropdown-item" href="/customer-service-training-los-angeles.php">Customer Service</a></li>
                                            <li><a class="dropdown-item" href="/grammar-training-los-angeles.php">Grammar</a></li>
                                            <li><a class="dropdown-item" href="/presentations-training-los-angeles.php">Presentations</a></li>
                                            <li><a class="dropdown-item" href="/project-management-training-los-angeles.php">Project Management</a></li>
                                            <li><a class="dropdown-item" href="/time-management-training-los-angeles.php">Time Management</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Microsoft Office</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/access-training-los-angeles.php">Access</a></li>
                                            <li><a class="dropdown-item" href="/excel-training-los-angeles.php">Excel</a></li>
                                            <li><a class="dropdown-item" href="/outlook-training-los-angeles.php">Outlook</a></li>
                                            <li><a class="dropdown-item" href="/ms-powerpoint-training-los-angeles.php">PowerPoint</a></li>
                                            <li><a class="dropdown-item" href="/ms-project-training-los-angeles.php">Project</a></li>
                                            <li><a class="dropdown-item" href="/visio-training-los-angeles.php">Visio</a></li>
                                            <li><a class="dropdown-item" href="/ms-word-training-los-angeles.php">Word</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a class="dropdown-item dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Web Development</a>

                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="/html-training-los-angeles.php">HTML</a></li>
                                            <li><a class="dropdown-item" href="/javascript-training-los-angeles.php">JavaScript</a></li>
                                            <li><a class="dropdown-item" href="/php-mysql-training-los-angeles.php">PHP</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="timetable.php" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Timetable
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a href="/timetable_chicago.php" class="dropdown-item">Chicago</a></li>
                            <li><a href="/timetable_la.php" class="dropdown-item">Los Angeles</a></li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="/onsite-training.php" class="nav-link">Onsite Training</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/room-hire.php">Room Rentals</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about-us.php">About Us</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="/timetable.php" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Contact Us
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a href="/contact-us-chicago.php" class="dropdown-item" href="#">Chicago</a></li>
                            <li><a href="/contact-us-los-angeles.php" class="dropdown-item" href="#">Los Angeles</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>


