<?php
$meta_title = "Project Management Course | Los Angeles & Chicago";
$meta_description = "Project Management classes in Chicago & Los Angeles. Don't settle for an average class! For great deals call 888.815.0604.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted" >
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/captivate-training.php" itemprop="url">
                    <span itemprop="title">Project Management Training</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-project-management.jpg');">
    <div class="container">
        <div class="masterhead-copy">
            <h1 data-aos="fade-up">Project Management Training</h1>

            <div data-aos="fade-up" data-aos-delay="100">
                <h4>Instructor-led Project Management classes in Chicago and Los Angeles</h4>
                <p>
                    Live face-to-face Project Management training. <br>
                    This is NOT a webinar!
                </p>
            </div>

            <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll" data-aos="fade-up" data-aos-delay="200">
                <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                Book Course
            </a>
        </div>
    </div>
</div>

<div class="section section-instructors">
    <div class="container">
        <div class="section-heading mb-0">
            <h2 class="mb-4" data-aos="fade-up" >Project Management Training Classes</h2>

            <p>Looking for Project Management training in Chicago or Los Angeles?</p>
          <p>Our 2-day instructor-led Project Management classes are ideal for  anyone who is new to Project Management who needs to learn how to  complete projects on time and within budget.</p>
            <p>Once considered a specialist skill, Project Management is today an important and necessary skill for all team leaders  and managers.</p>
        </div>

    </div>
</div>

<div id="section-book-course" class="section section-training-options pb-0">
    <div class="container">
        <div class="section-heading align-center mb-0" data-aos="fade-up">
            <h2>Detailed Course Outline</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">

            <div class="accordion-classes g-text-Project Management" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default pb-md-5">
                        <div class="post-img"><img src="/dist/images/icons/icon-project-management.png" alt="Project Management"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Project Management
                                </a>
                            </h3>
                            <p>This course teaches participants the fundamentals of project management. You will learn how to manage time and costs, implement quality measures, handle project risks, acquire and motivate team members and communicate effectively.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Project Management" data-price="$895">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-md btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="far fa-calendar-alt mr-2"></i>
                                        Timetable
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Project Management">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable Chicago
                                        </a>
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Project Management">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable LA
                                        </a>
                                    </div>
                                </div>
                                <a href="/downloads/business-skills/Project%20Management.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>2 Days</span>
                                <sup>$</sup>895
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Key Concepts</h5>
                                        <ul>
                                            <li>What is a Project?</li>
                                            <li> What is Project Management?</li>
                                            <li> What is a Project Manager?</li>
                                            <li> About the Project Management Institute (PMI)</li>
                                            <li> About the Project Management Body Of Knowledge (PMBOK)</li>
                                            <li> The Five Process Groups</li>
                                            <li> The Nine Knowledge Areas</li>
                                            <li> The Triple Constraint</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Project Initiation</h5>
                                        <ul>
                                            <li>Identifying Your Stakeholders</li>
                                            <li> Assessing Needs and Wants</li>
                                            <li> Setting a SMART Project Goal</li>
                                            <li> Creating Requirements and Deliverables</li>
                                            <li>Creating a Statement of Work</li>
                                            <li> Completing the Project Planning Worksheet</li>
                                            <li> Completing the Project Charter</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Planning</h5>
                                        <ul>
                                            <li>Managing Expectations</li>
                                            <li> Creating a Task List</li>
                                            <li> Estimating Time</li>
                                            <li> Estimating Resources</li>
                                            <li> Estimating Costs</li>
                                            <li>Building the Work Breakdown Structure</li>
                                            <li> Creating the Schedule</li>
                                            <li> Creating a Risk Management Plan</li>
                                            <li> Creating a Communication Plan</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Planning Tools</h5>
                                        <ul>
                                            <li>The Gantt Chart</li>
                                            <li> The Network Diagram</li>
                                            <li> Using a RACI Chart</li>
                                            <li>Introduction to Microsoft Project</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Executing the Project</h5>
                                        <ul>
                                            <li>Establishing Baselines</li>
                                            <li> Monitoring Project Progress</li>
                                            <li> Triple Constraint Reduction Methods</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Maintaining and Controlling the Project</h5>
                                        <ul>
                                            <li>Making the Most of Status Updates</li>
                                            <li> Managing Change</li>
                                            <li> Monitoring Risks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Closing Out</h5>
                                        <ul>
                                            <li>Preparing for Closeout</li>
                                            <li> Celebrating Successes</li>
                                            <li> Learning from Project Challenges</li>
                                            <li> Scope Verification</li>
                                            <li> A Final To-Do List</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="card-copy mt-2" data-aos="fade-up">
                                <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="inner-section-nobg">
    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>
</div>

<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Project Management training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_8">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                </div>

                <h3><span>Project Management classes rating:</span> 4.8 stars from 129 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">
                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I cannot say enough good things about this course! The knowledge and tools that I gained from the course is priceless and will help me to further my career. Christian has been a joy to work with and has been very inspirational. I am definitively going to take more classes in order to enhance my new knowledge-base.&quot;</p>
                            <span><strong>Cheryl Van Nuys | Hospira Inc.</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Mr. Chris was right on point with his instructions for Project Management. he also knew what he was talking about. and I enjoyed the class feed back/class interaction. it was a pleasure.&quot;</p>
                            <span><strong>Onnie Brown | Department Of Coroner</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I found Carol and her teachings to be highly engaging and thoughtful. With little context prior to taking the class, I did come away from it feeling like I had a solid handle on a new area of professional learnings that I didn't have before.&quot;</p>
                            <span><strong>Andrew Stewart | Participant Media</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This course is excellent for new project managers. I have utilized most of the concepts, templates etc. While this course provided positive reinforcement to the practices of my organization there were new ideas and learnings for me.&quot;</p>
                            <span><strong>Kay O'Connell | United Airlines</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Christian is an excellent teacher with a passion for project management like not many other people in industry today. I highly recommend this class for anyone in the PM field.&quot;</p>
                            <span><strong>Jason Gies | Hendrickson</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=8" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>

<div class="section section-guarantee">
    <div class="container container-sm">
        <div class="section-heading">
            <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
            <p data-aos="fade-up" data-aos-delay="150">We recognize that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months.  <br>
            </p>
        </div>

        <div class="section-body">
            <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Our Project Manangement class Guarantee">
            <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
            <p data-aos="fade-up" data-aos-delay="250">Project management is not for everyone. If you decided that after a half a day  in class that the class is not for you we will give you a complete refund. </p>
        </div>
    </div>
</div>

    <div id="section-course-form" class="section section-group-training">
        <div class="container">
            <div class="section-heading mb-0">
                <h2 data-aos="fade-up">Group Training Quotation</h2>
            </div>

            <div class="section-body">
                <form action="" class="form-group-training">

                    <h4 class="text-center" data-aos="fade-up" data-aos-delay="100">Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>
                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>
                        </label>
                    </div>

                    <hr class="mt-4 mb-4" data-aos="fade-up" data-aos-delay="150">

                    <ul class="list-radio-checkbox row-3 mb-4" data-aos="fade-up" data-aos-delay="200">
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Communication</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Etiquette</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Leadership</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Writing</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Conflict Resolution</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Customer Service</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Grammar</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Presentations</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]" checked><i></i> Project Management</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Time Management</label></li>
                    </ul>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="No. of Trainees*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="First Name*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Last Name*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Phone no*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address 1*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address 2">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="City*">
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="State*">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Zip*">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-actions"  data-aos="fade-up" data-aos-delay="130">
                        <button class="btn btn-secondary btn-lg"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

    <div class="section section-faq">
        <div class="container">

            <div class="section-heading mb-4">
                <h2 class="section-title">FAQ</h2>
            </div>

            <div class="card-columns card-faq-list">
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>Where do your Project Management classes take  place? </h4>
                    <div class="card-body">
                      <p>We have two  training centers, one located in  Chicago (230 W Monroe, Suite                         610, Chicago IL 60606) and the other in Los  Angeles (915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017). </p>
                    </div>
                </div>
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>What are the Project Management class times?</h4>
                    <div class="card-body">
                      <p>Our Project Management classes start at 9.00am and finish at approximately 4.30pm. We take an hour lunch break around 12.15pm.</p>
                    </div>
                </div>
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>Can you deliver Project Management training onsite at our location?                    </h4>
                    <div class="card-body">
                      <p>Yes, we service the greater Chicago and Los Angeles metros. We also provide onsite Project Management training at client sites right across the country. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Project Management class.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>