<?php
$meta_title = "Powerpoint Training Classes | Los Angeles | Training Connection";
$meta_description = "Need to learn Microsoft Powerpoint? Our Los Angeles PowerPoint classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Powerpoint Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-powerpoint.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">Powerpoint Training<br>
                      Los Angeles
                    </h1>

                    <div data-aos="fade-up">
                        <h4>PowerPoint 2013, 2016, 2019 &amp; 365</h4>
                        <p>Looking for an instructor-led PowerPoint training class in Los Angeles?</p>
                        <p>All our PowerPoint classes are taught by live instructors present in the classroom. They will teach you step-by-step how to create professional presentations using Microsoft PowerPoint.</p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-powerpoint-2019.png" alt="Microsoft Powerpoint box">
                    <img src="/dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">
                <h3>What's Included</h3>
                <ul>
                    <li>A printed PowerPoint training manual</li>
                    <li>Certificate of course completion</li>
                    <li>Small class size with average of 4-5 students</li>
                    <li>6 month free class repeat</li>
                </ul>
                <p><strong>Book a PowerPoint course today.  All classes guaranteed to run!</strong><br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-package-deals" class="btn btn-primary btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-box mr-2"></i>
                        Package deals
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0" data-aos="fade-up">
                <h2>Powerpoint course outlines</h2>
            </div>

            <div class="accordion-classes g-text-powerpoint" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="PowerPoint Level 1 - Introduction"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    PowerPoint Level 1 - Introduction
                                </a>
                            </h3>
                            <p>This hands-on PowerPoint class is suitable for beginners. You will start by learning the program's interface, then  build your own  presentation, adding slides,  formating text, adding graphical  elements, displaying data in tables and charts, and finalizing your  presentation for  delivery.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="PowerPoint Level 1 - Introduction" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/powerpoint/Powerpoint%20Level%201.pdf" class="btn btn-md btn-dark">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="PowerPoint Level 1 - Introduction">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">PowerPoint Basics</h5>
                                        <ul>
                                            <li>Exploring the PowerPoint Environment</li>
                                            <li>Starting PowerPoint</li>
                                            <li>Opening an Existing Presentation</li>
                                            <li>The PowerPoint Interface</li>
                                            <li>Exploring Views</li>
                                            <li>Getting Help</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating a Presentation</h5>
                                        <ul>
                                            <li>Types of new Presentations</li>
                                            <li>Backstage View</li>
                                            <li>Creating a Blank Presentation</li>
                                            <li>Creating a Presentation from a Template</li>
                                            <li>Creating and Modifying Slide Content</li>
                                            <li>Slides and their Elements</li>
                                            <li>Inserting Text on a Slide</li>
                                            <li>Adding, Deleting and Hiding Slides in a Presentation</li>
                                            <li>Navigating a Presentation</li>
                                            <li>Inserting a Hyperlink on a Slide</li>
                                            <li>Inserting Shapes</li>
                                            <li>Inserting Images</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Formatting</h5>
                                        <ul>
                                            <li>Working with Slide Masters and Layouts</li>
                                            <li>About Slide Masters</li>
                                            <li>Applying a Theme to a Presentation</li>
                                            <li>Headers and Footers</li>
                                            <li>Applying a Layout to a Slide</li>
                                            <li>Formatting Slides and Text</li>
                                            <li>About Text Styles</li>
                                            <li>Changing Master Text Styles</li>
                                            <li>Changing Individual Slide Text Styles</li>
                                            <li>Creating Bulleted and Numbered Lists</li>
                                            <li>About Slide Backgrounds</li>
                                            <li>Using Gridlines</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Shapes and Images</h5>
                                        <ul>
                                            <li>Creating and Formatting Shapes</li>
                                            <li>About Shapes</li>
                                            <li>Drawing Shapes</li>
                                            <li>Curved Shapes</li>
                                            <li>Useful Tools for Working with Shapes</li>
                                            <li>Resizing Shapes</li>
                                            <li>Shape Styles</li>
                                            <li>Layering, Aligning and Grouping Shapes</li>
                                            <li>About Images</li>
                                            <li>Inserting Images from a Local File</li>
                                            <li>Inserting Online Images</li>
                                            <li>Sizing and Moving Images</li>
                                            <li>Cropping Images</li>
                                            <li>Applying Styles and Effects</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Charts and Tables</h5>
                                        <ul>
                                            <li>About Charts</li>
                                            <li>ChartTypes</li>
                                            <li>Adding a Chart to a Presentation</li>
                                            <li>Changing the Chart Type</li>
                                            <li>Chart Elements</li>
                                            <li>Changing the Chart Layout</li>
                                            <li>Applying Chart Styles</li>
                                            <li>Inserting an Excel Chart</li>
                                            <li>Editing Chart Data</li>
                                            <li>About Tables</li>
                                            <li>Creating Tables</li>
                                            <li>Inserting a Table</li>
                                            <li>Entering Data</li>
                                            <li>Applying Styles</li>
                                            <li>The Table Style Options</li>
                                            <li>Importing Tables</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Transitions, Importing Content and Printing</h5>
                                        <ul>
                                            <li>SlideTransitions</li>
                                            <li>ApplyingthesameTransitionto all Slides</li>
                                            <li>ApplyingTransitionsto Individual  Slides</li>
                                            <li>Additional Text Options</li>
                                            <li>Text from other Sources</li>
                                            <li>Importing a Word Document</li>
                                            <li>Importing a PDF File</li>
                                            <li>Creating WordArt from Text</li>
                                            <li>Printing in PowerPoint</li>
                                            <li>Previewing your Printout</li>
                                            <li>Setting Print Options</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Powerpoint Level 2 - Advanced"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Powerpoint Level 2 - Advanced
                                </a>
                            </h3>
                            <p>On this advanced PowerPoint course you will learn to create more dynamic and visually appealing  presentations.  	          The course covers  transitions and animation,  themes, slide  masters, advanced formatting, SmartArt, adding audio and video,  collaborating with others,	protecting your presentations and	more. </p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="Powerpoint Level 2 - Advanced" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/powerpoint/Powerpoint%20Level%202.pdf" class="btn btn-md btn-dark">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Powerpoint Level 2 - Advanced">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Formatting</h5>
                                        <ul>
                                            <li>Inserting and Formatting SmartArt</li>
                                            <li>About SmartArt</li>
                                            <li>Creating SmartArt</li>
                                            <li>Converting Lists to SmartArt</li>
                                            <li>SmartArt Tools</li>
                                            <li>Changing Layouts</li>
                                            <li>Promoting and Moving List Items</li>
                                            <li>Changing Styles and Colors</li>
                                            <li>Shapes and Shape Styles</li>
                                            <li>SmartArt Charts</li>
                                            <li>Creating an Organization Chart</li>
                                            <li>Adding Shapes to a Chart</li>
                                            <li>Working with Multiple Slide Masters</li>
                                            <li>The Edit Master Group</li>
                                            <li>Adding a Slide Master to a Presentation</li>
                                            <li>Duplicating a Slide Master</li>
                                            <li>Applying Masters to Slides</li>
                                            <li>Restoring slide-master Placeholders</li>
                                            <li>Creating Presentation Sections</li>
                                            <li> Page Setup Options </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Animation, Time Effects and Media</h5>
                                        <ul>
                                            <li>About Animation</li>
                                            <li>Animating Text</li>
                                            <li> Animating Shapes</li>
                                            <li>Modifying Animations</li>
                                            <li>Customizing Animations</li>
                                            <li>Copying Animations</li>
                                            <li>Timing of Animation and Transition Effects</li>
                                            <li>Inserting and Formatting Media</li>
                                            <li>About Media</li>
                                            <li>Adding Sounds Effects</li>
                                            <li>Inserting an Audio Object on a Slide</li>
                                            <li>Adding Video</li>
                                            <li>Online Videos</li>
                                            <li>Video Tools</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Reviewing Content, Tracking Changes and Saving</h5>
                                        <ul>
                                            <li>Proofing your Presentation</li>
                                            <li>Automatic Spell Checking</li>
                                            <li>Using the Spelling Pane</li>
                                            <li>Proofing Options</li>
                                            <li>AutoCorrect Options</li>
                                            <li>About Comments</li>
                                            <li>Adding Comments</li>
                                            <li>Managing Comments</li>
                                            <li>Comparing Presentations</li>
                                            <li>Reviewing Changes</li>
                                            <li>Saving a Presentation in other Formats</li>
                                            <li>Available Formats</li>
                                            <li>Creating PDFs and XPS Documents</li>
                                            <li>Creating Word Handouts</li>
                                            <li>About Video</li>
                                            <li>Creating a Video</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Notes and Custom Slide Shows</h5>
                                        <ul>
                                            <li>Working with Notes Pages</li>
                                            <li> About Notes</li>
                                            <li>Adding Notes to a Slide </li>
                                            <li>Notes Page View</li>
                                            <li>Notes Master View</li>
                                            <li>Adding an extra Notes Page</li>
                                            <li>Printing Notes Pages</li>
                                            <li>Configuring, Rehearsing and Presenting Slide Shows</li>
                                            <li>Presenter View</li>
                                            <li>Mouse and Keyboard Slide-Show Controls</li>
                                            <li>Rehearsing Slide Timings</li>
                                            <li>Creating a Custom Slide Show</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sharing, Collaborating, and Security</h5>
                                        <ul>
                                            <li>Protecting your Presentations</li>
                                            <li>The Info Window</li>
                                            <li>Making a Presentation "Read-only"</li>
                                            <li>Protecting a Presentation with a Password</li>
                                            <li>Sharing your Presentations</li>
                                            <li>Checking Compatibility</li>
                                            <li>Checking  Accessibility</li>
                                            <li>Compressing Media</li>
                                            <li>Saving to a Shared Location</li>
                                            <li>Saving a Presentation to OneDrive</li>
                                            <li>Inviting others to Share a Presentation</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section section-package-deals">
        <div class="container">
            <div class="section-heading" data-aos="fade-up">
                <h2>Package deals</h2>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="150">
                <div class="card-deck card-row-sm card-deck-training">

                    <div class="card card-package card-package-powerpoint">
                        <h3 class="card-title sm"><span>PowerPoint Package - 2 Levels</span></h3>
                        <div class="card-body">
                            <div class="card-text">
                                <h3 class="card-price">$600</h3>
                            </div>

                            <p>Book both levels of PowerPoint Training and save $100.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-secondary" data-target="#book-package-deal-1" data-toggle="modal">
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book Package
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>


    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Powerpoint training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>

                    <h3><span>Powerpoint classes rating:</span> 4.7 stars from 1,053 reviewers </h3>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;This course covered everything I needed to create great PowerPoint presentations at work. The pace was perfect. Chris, the instructor, was extremely knowledgeable and friendly. Chris took the time to ensure each topic was completely understood before moving on to the next. I would absolutely recommend this class and instructor (if you're able to choose) for anyone looking to brush and improve upon their basic PowerPoint knowledge.&quot;</p>
                                <span><strong>Alexandra Hill | Starz Enteratainment</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Carol made it fun and exciting. Enjoyed the class and can't wait to share what I learned with my co-workers, family and friends.&quot;</p>
                                <span><strong>Silvia Glick | City of Hope</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Sandy is an excellent instructor! I have taken other Microsoft Office courses from her and she consistently delivers an excellent presentation. She keeps you engaged and has a good sense of humor that keeps the presentation interesting. I really enjoyed this course and look forward to taking the advance level of PowerPoint. I only wish that she was teaching it too! I will recommend this course to others as I did the Excel course.&quot;</p>
                                <span><strong>Zonda Zschau | ACHE</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Level 2 was just as great as Level 1 yesterday. Small class allowed us to spend a great deal of time troubleshooting real examples from our work life. Excellent classes and would highly recommend to anyone looking to enhance their PowerPoint skills.&quot;</p>
                                <span><strong>Michael Fuko | DHL Global Forwarding</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Allyncia made this training very pleasant and effective. Her training style was just excellent! plus her positive reinforcement and approach and of course her beautiful smiles!&quot;</p>
                                <span><strong>Sung H. Yu</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=21" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>

    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that  trainees often benefit from repeating their class. Included in your course price is a FREE Repeat valid for 6 months.  Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Class guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Powerpoint is not for everyone. If you decide that after half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Powerpoint Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Powerpoint. This can be delivered onsite at your premises, at our training center in LA or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" checked><i></i> <h3>Powerpoint Level 1 - Introduction</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Powerpoint Level 2 - Advanced</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>

    <div class="section section-faq g-text-powerpoint">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Powerpoint Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best PowerPoint learning experience for our clients in Los Angeles. Please find answers to some of our most frequently asked PowerPoint training related questions below, or contact us should you require any further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
              <li>
                <h5>What version of PowerPoint do you teach on?</h5>
                <p>We are currently teaching our classes on PowerPoint 2019. PowerPoint 2019 is similar to PowerPoint 2016 and PowerPoint 2013, so the training is suitable for all 3 versions of PowerPoint.</p>
              </li>
              <li>
                <h5>What times are your PowerPoint classes?</h5>
                <p>All our classes run from 9.00am to 4.30pm.</p>
              </li>
              <li>
                <h5>Where are your PowerPoint classes held?</h5>
                <p>Our Los Angeles PowerPoint classes are held in our LA Training center located at 915 Wilshire Blvd, Suite 1800, Los Angeles, CA 90017. For more information about directions, parking, trains please <a href="/contact-us-los-angeles.php">click here</a>.</p>
              </li>
              <li>
                <h5>Is this face-to-face training or a webinar?</h5>
                <p>Our PowerPoint classes are all face-to-face, in other words, our instructors are present in the training lab. They are passionate about teaching and will ensure you have a great learning experience.</p>
              </li>
              <li>
                <h5>What are the class times?</h5>
                <p>Our MS PowerPoint classes start at 9.00am and finish at 4.30pm. We normally take a lunch hour around 12.15pm.</p>
              </li>
              <li>
                <h5>Do I need to bring my own computer?</h5>
                <p>No, we provide the computers at our training center in downtown Los Angeles.</p>
              </li>
              <li>
                <h5>Can you deliver PowerPoint training onsite at our location?</h5>
                <p>Yes, we service the greater Los Angeles metro including Anaheim, Burbank, Covina, Downtown, Fullerton, Irvine, Long Beach, Northridge, Pasadena, San Bernardino, Santa Monica, Van Nuys, Ventura and surrounding areas.</p>
                <p>Our trainers can also travel anywhere in the country to deliver PowerPoint training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite PowerPoint training.</p>
              </li>
            
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-powerpoint.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>