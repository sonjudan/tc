<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Unit 1: Exploring Captivate <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Explore a Finished Captivate Project</li>
                                        <li>Zoom and Magnify</li>
                                        <li>Navigate a Project</li>
                                        <li>Explore and Reset the Workspace</li>
                                        <li>Preview the Entire Project</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Unit 2: New Projects &amp; Soft Skills eLearning  <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Create a Blank Project</li>
                                        <li>Add Images to Placeholders</li>
                                        <li>Import Images</li>
                                        <li>Use a Smart Shape as a Button</li>
                                        <li>Disable Click Sounds</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Unit 3: Screen Recordings  <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Rehearse a Script</li>
                                        <li>Review Recording Settings</li>
                                        <li>Review Recording Modes</li>
                                        <li>Record Using Multiple Modes</li>
                                        <li>Record a Custom Simulation</li>
                                        <li>Record a Demonstration that Pans</li>
                                        <li>Manually Record the Screen</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Unit 4: Video Demos <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Record a Video Demo</li>
                                        <li>Add a Video Zoom</li>
                                        <li>Add a Video Pan</li>
                                        <li>Smooth a Mouse Path and Show Visual Clicks</li>
                                        <li>Split a Video</li>
                                        <li>Trim a Video</li>
                                        <li>Insert a Video Project into a Standard Project</li>
                                        <li>Publish a Video Demo</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Unit 5: Captions, Styles, Timing, and Round Tripping <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Insert and Edit Text Captions</li>
                                        <li>Edit the Default Caption Style</li>
                                        <li>Change a Callout Type Used by a Text Caption</li>
                                        <li>Control Slide Timing</li>
                                        <li>Control Slide Object Timing</li>
                                        <li>Check Spelling</li>
                                        <li>Align Slide Objects</li>
                                        <li>Export Captions to Word</li>
                                        <li>Import Captions from Word into Captivate</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Unit 6: Pointers, Paths, Paths, Boxes, and Buttons <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Control Mouse Effects</li>
                                        <li>Edit a Mouse Path</li>
                                        <li>Clone an Object Style</li>
                                        <li>Insert a Highlight Box</li>
                                        <li>Insert an Image Button</li>
                                        <li>Control Appear After Timing</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Unit 7: Images and Videos <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Insert, Resize, and Restore an Image</li>
                                        <li>Import Images into the Library</li>
                                        <li>Resize, Transform, and Align Images</li>
                                        <li>Manage Unused Library Assets</li>
                                        <li>Create an Image Slideshow</li>
                                        <li>Insert a Video</li>
                                        <li>Set Video Properties</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    Unit  8: Audio <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Work with Rollover Captions</li>
                                        <li>Import Audio onto a Slide Object</li>
                                        <li>Import Background Audio</li>
                                        <li>Add a Slide Note</li>
                                        <li>Calibrate a Microphone</li>
                                        <li>Record Slide Audio</li>
                                        <li>Import Audio onto a Slide</li>
                                        <li>Edit an Audio File</li>
                                        <li>Insert Silence</li>
                                        <li>Convert Text-to-Speech</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    Unit 9: States, Animations, and Object Effects <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Change State Views for a Button</li>
                                        <li>Add an Animation to a slide</li>
                                        <li>Insert a Text Animation</li>
                                        <li>Apply an Effect to a Slide Object</li>
                                        <li>Apply a Free Fall Effect to an Object</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    Unit 10: Software Simulations  <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Hide the Mouse</li>
                                        <li>Replace Phrases</li>
                                        <li>Insert a Click Box</li>
                                        <li>Insert a Text Entry Box</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    Unit 11: Working with PowerPoint <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Create a Project from a Presentation</li>
                                        <li>Edit the Source Presentation</li>
                                        <li>Synchronize with Source</li>
                                        <li>Rescale a Project</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                    Unit 12: Quizzing  <i></i>
                                </a>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Edit Quizzing Object Styles</li>
                                        <li>Set the Quiz Preferences</li>
                                        <li>Question Slides</li>
                                        <li>Insert Question Slides</li>
                                        <li>Edit a Question Slide</li>
                                        <li>Compare Submit All to Submit Buttons</li>
                                        <li>Insert a Knowledge Check</li>
                                        <li>Review a GIFT File</li>
                                        <li>Import a GIFT File into a Project</li>
                                        <li>Create Question Pools</li>
                                        <li>Move Questions to Pools</li>
                                        <li>Insert Random Question Slides</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
                                    Unit 13: Publishing <i></i>
                                </a>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Apply a Skin</li>
                                        <li>Edit, Save, and Delete a Skin</li>
                                        <li>Name Slides</li>
                                        <li>Check Publish Settings and Add a Loading Screen</li>
                                        <li>Publish as SWF and PDF</li>
                                        <li>Run the HTML5 Tracker</li>
                                        <li>Publish as HTML5</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>