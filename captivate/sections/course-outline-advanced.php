<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Unit 1: Interactive Software Simulations and Caption Pre-Editing <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Rehearse a Script</li>
                                        <li>Set Recording Preferences</li>
                                        <li>Record a Simulation</li>
                                        <li>Edit a Text Capture Template</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Unit 2: Object Styles, Project Sharing, and Branching <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Create a New Style</li>
                                        <li>Apply an Object Style Globally</li>
                                        <li>Export and Import an Object Style</li>
                                        <li>Name a Slide</li>
                                        <li>Copy/Paste Project Assets</li>
                                        <li>Use Buttons to Create a Branch</li>
                                        <li>Explore the Branching View</li>
                                        <li>Create a Branch Group</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse1">
                                    Unit 3: Variables and Widgets <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Add Project Information</li>
                                        <li>Insert a System Variable</li>
                                        <li>Edit a System Variable</li>
                                        <li>Create a User Variable</li>
                                        <li>Use a Variable to Gather Learner Data</li>
                                        <li>Insert and Format a Widget</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Unit 4: Interactive Videos and Virtual Reality <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Insert an Interactive Video</li>
                                        <li>Add Bookmarks</li>
                                        <li>Add Slide Overlays</li>
                                        <li>Create a Virtual Reality Project</li>
                                        <li>Add a Text Hotspot</li>
                                        <li>Add an Audio Hotspot</li>
                                        <li>Add a Quiz to a Virtual Reality Project</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Unit 5: Interactions <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Insert a Process Circle</li>
                                        <li>Create a Basic Drag and Drop Interaction</li>
                                        <li>Manage Drag and Drop Buttons and Write Captions</li>
                                        <li>Create a “Trick” Retry Slide</li>
                                        <li>Explore an Advanced Drag and Drop Project</li>
                                        <li>Create an Advanced Drag and Drop Interaction</li>
                                        <li>Change the States of a Smart Shape</li>
                                        <li>Use States to Swap Images</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Unit 6: Accessible eLearning <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Set Document Information</li>
                                        <li>Enable Accessibility</li>
                                        <li>Add Accessibility Text to Slides</li>
                                        <li>Import Slide Audio</li>
                                        <li>Add Shortcut Keys</li>
                                        <li>Add Closed Captions</li>
                                        <li>Set a Tab Order</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Unit 7: Advanced Actions <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Use a Completed Action</li>
                                        <li>Name Objects</li>
                                        <li>Create a Mask</li>
                                        <li>Control Object Visibility</li>
                                        <li>Create a Standard Advanced Action</li>
                                        <li>Attach an Action to a Button</li>
                                        <li>Group Timeline Objects</li>
                                        <li>Create a Variable</li>
                                        <li>Create a Conditional Action</li>
                                        <li>Create Decision Blocks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse08" aria-expanded="true" aria-controls="collapse1">
                                    Unit 8: Project Templates and Master Slides <i></i>
                                </a>
                            </div>
                            <div id="collapse08" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Work with the Main Master Slide</li>
                                        <li>Work with Content Masters</li>
                                        <li>Apply a Master to Filmstrip Slides</li>
                                        <li>Edit a Master</li>
                                        <li>Apply a Theme</li>
                                        <li>Create a Custom Theme</li>
                                        <li>Review a Template</li>
                                        <li>Create a Project Based on a Template</li>
                                        <li>Create a Project Template</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse09" aria-expanded="true" aria-controls="collapse1">
                                    Unit 9: Responsive Projects <i></i>
                                </a>
                            </div>
                            <div id="collapse09" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Customize Breakpoints</li>
                                        <li>Save a Standard Project As Responsive</li>
                                        <li>Insert and Name Fluid Boxes</li>
                                        <li>Resize Fluid Boxes</li>
                                        <li>Add Content to Fluid Boxes</li>
                                        <li>Switch Modes</li>
                                        <li>Use the Position Inspector</li>
                                        <li>Modify a Single Breakpoint</li>
                                        <li>Exclude from View</li>
                                        <li>Add a New Breakpoint</li>
                                        <li>Position and Link Objects</li>
                                        <li>Edit Breakpoint Object Styles</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse1">
                                    Unit 10: Reporting Results <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <ul>
                                        <li>Set Quiz Reporting Options</li>
                                        <li>Create a Manifest File</li>
                                        <li>Report a Button Interaction</li>
                                        <li>Adjust Slide Object Interaction</li>
                                        <li>Preview in SCORM Cloud</li>
                                        <li>Publish a Content Package</li>
                                        <li>Create an Inquisiq LMS Account</li>
                                        <li>Create an LMS Course</li>
                                        <li>Attach a Lesson to a Course</li>
                                        <li>Test an eLearning Course</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>