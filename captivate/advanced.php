<?php
$meta_title = "Adobe Captivate 2019 Advanced | Hands-on certified training";
$meta_description = "Small face-to-face instructor-led Captivate classes taught by Captivate pros.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../captivate-training.php">Captivate Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Captivate 2019</h1>
                    <h3 data-aos="fade-up">Advanced Training Course</h3>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <p>This Adobe certified class covers the the more advanced functionality in Captivate.</p>
                        <p>The instructor will show you step-by-step, how to create a highly interactive software simulations, responsive lessons (using Fluid Boxes and Breakpoints) that automatically reflow to fit just about any kind of display, Virtual Reality with 360-degree images and videos, interactive videos, how to use object styles, master slides, themes, and advanced actions.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-4" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>2 Days</span>
                        <sup>$</sup>995<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up" data-aos-delay="350">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Captivate 2019 Advanced" data-price="$995">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Captivate Advanced" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Captivate Advanced" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/captivate/TC_Captivate_2019_Advanced.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-captive-advanced.png" alt="Captivate 2019 - Advanced Training Course" class="" width="288">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up" data-aos-delay="300">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>You will engage your learners by adding variables, widgets, and learner interactions (including drag and drop), apply object styles globally, create branching scenarios that allow learners to plot their own path through your course, and templates. You will ensure your content is 508-compliant and therefore accessable to people with disabilities, and enable Captivate’s reporting features and learn about Learning Management Systems, SCORM, SCOs, Manifests, and content packages.</p>
                    <p>View our full range of <a href="/captivate-training.php">Captivate training courses</a>, or see below for the detailed outline for Captivate Advanced.</p>

                    <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Captivate 2019 Advanced" data-price="$995">
                        <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                        Book Course
                    </a>
                </div>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/captivate/sections/course-outline-advanced.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>