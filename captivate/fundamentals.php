<?php
$meta_title = "Adobe Captivate 2019 Fundamentals | Hands-on certified training";
$meta_description = "Small face-to-face instructor-led Captivate classes taught by Captivate pros.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../captivate-training.php">Captivate Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Fundamentals</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Captivate 2019</h1>
                    <h3 data-aos="fade-up">Fundamentals Training Course</h3>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <p>On this beginner Adobe certified class you will learn the skills needed to create interactive eLearning content and software demonstrations.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-4" data-aos="fade-up">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>2 Days</span>
                        <sup>$</sup>995<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up" data-aos-delay="350">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Captivate 2019 Fundamentals" data-price="$995">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Captivate Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Captivate Fundamentals" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/captivate/TC_Captivate_2019_Fundamentals.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-captivate-fundamentals.png" alt="Captivate 2019 - Fundamentals Training Course" class="" width="288">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up" data-aos-delay="300">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>The instructor will show you step-by-step how create a soft-skills lessons, software demonstrations, interactive training simulations, and video demos.</p>
                    <p>You will learn to add such standard objects to a slide as text captions, images, characters, videos, PowerPoint content, audio, Smart Shapes, and more. You will add to the user experience by adding interactivity via click boxes, buttons, text entry boxes, and quizzes.</p>
                    <p>You will finish by learning how to publish your content on practically any device including desktop computers, laptops, smart phones, and tablets.</p>

                    <p>View our full range of <a href="/captivate-training.php">Adobe Captivate training courses</a>, or see below for the detailed outline for Captivate Fundamentals.</p>

                    <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Captivate 2019 Fundamentals" data-price="$995">
                        <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                        Book Course
                    </a>
                </div>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/captivate/sections/course-outline-fundamentals.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>