<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting Started <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn how to navigate the InDesign Interface, manage and create workspaces and learn how to layout your first text. </strong></p>
                                        <ul>
                                            <li>What’s new in 2020</li>
                                            <li>Create New Document</li>
                                            <li>Understanding Units</li>
                                            <li>Creating a document</li>
                                            <li>Layout and Navigation</li>
                                            <li>Understanding Workspaces</li>
                                            <li>Accessing Panels</li>
                                            <li>Creating a Text Frame</li>
                                            <li>Frame vs Content</li>
                                            <li>Anatomy of a Frame</li>
                                            <li>Adding placeholder text</li>
                                            <li>Where to turn on Grids and Guides</li>
                                            <li>The Properties Panel</li>
                                            <li>The Character Panel (basic overview)</li>
                                            <li>The Paragraph Panel (basic overview)</li>
                                            <li>Basic Text Spacing & Alignment</li>
                                            <li>All about Adobe Fonts</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Top Secret! Letter Creation <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn more advanced text and graphics layout features as you re-create a de-classified UFO letter.</strong></p>
                                        <ul>
                                            <li>Creating a Print Document</li>
                                            <li>Understanding the Parts of a Letter</li>
                                            <li>Working with and Re-sizing Text Frames</li>
                                            <li>Importing and Managing Graphics</li>
                                            <li>Working with Bullets</li>
                                            <li>Saving an InDesign Project</li>
                                            <li>Multi-Frame Document</li>
                                            <li>More Text Attributes</li>
                                            <li>Position Text with Baseline Shift</li>
                                            <li>Paragraph Spacing</li>
                                            <li>Control Leading in Character Options</li>
                                            <li>Using Tabs</li>
                                            <li>Hanging Bullets and Check Marks</li>
                                            <li>Adding Hyphenation/Discretionary</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Simple Retro Ad <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Create an entire ad, pulling in a photo, graphic logo and laying out text.</strong></p>

                                        <ul>
                                            <li>Creating Document for Print</li>
                                            <li>Setting the Bleed</li>
                                            <li>The Rectangle Frame Tool</li>
                                            <li>Working with Headlines</li>
                                            <li>Placing Images</li>
                                            <li>Combining Text and Images</li>
                                            <li>Layout Adjustments</li>
                                            <li>Pages vs Master Pages</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Pyramid Pets Article <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn the basics of page layouts along with more text manipulation. You will master paragraph spacing, leading, and use tabs to complete the layout.</strong></p>

                                        <ul>
                                            <li>Creating a 1-Page article for web</li>
                                            <li>Simple Layout Sketching</li>
                                            <li>Creating a Document based on Sketch</li>
                                            <li>Working with a Multi-Column Document</li>
                                            <li>Rectangle Frame Tool Again!</li>
                                            <li>Adding Graphics Elements</li>
                                            <li>Placing and formatting images</li>
                                            <li>Text Wrapping</li>
                                            <li>Working with Loaded Cursors</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Pure Elegance: 2-Page Simple Magazine Ad <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn how to create and layout a 2-Page Magazine Ad for Print.</strong></p>

                                        <ul>
                                            <li>Setting up the Document as a 2-Page Spread</li>
                                            <li>Working with Facing Pages</li>
                                            <li>Understanding & Working with Bleed</li>
                                            <li>Working with and formatting Bulleted Lists</li>
                                            <li>Working with FX to solve object issues</li>
                                            <li>Simple Paragraph Formatting</li>
                                            <li>Adding Page Numbers</li>
                                            <li>Understanding the Layers Panel</li>
                                            <li>Working with Decorative Fonts</li>
                                            <li>Printing as a Spread</li>
                                            <li>Simple Web Export</li>
                                            <li>Working with the Pages Panel</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Creating Vector Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn how to create useful graphics and effects within InDesign to use in your projects.</strong></p>

                                        <ul>
                                            <li>More About Vector vs Raster</li>
                                            <li>Understanding the Pen Tool</li>
                                            <li>Pen Tool Exercises</li>
                                            <li>Segments, Anchor Points, Curves</li>
                                            <li>Creating Custom Shapes & Page Elements</li>
                                            <li>Use Shape Tools more efficiently</li>
                                            <li>Working with Gradients for lighting</li>
                                            <li>Working with Effects to enhance graphics</li>
                                            <li>Simple Perspective graphic creation</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    The Ultimate Lemon Cake <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Get creative and design a one-page recipe masterpiece for a delicious cake!</strong></p>

                                        <ul>
                                            <li>Setup one-page document for web distribution</li>
                                            <li>Layout a one-page recipe with images</li>
                                            <li>Working with Image Content within Frames</li>
                                            <li>Create custom graphics elements</li>
                                            <li>Combine images with frames with FX</li>
                                            <li>Learn how to use the Direct Selection Tool</li>
                                            <li>Understanding Text Import Options</li>
                                            <li>Working with Swatches</li>
                                            <li>Saving Custom Swatches</li>
                                            <li>Streamline for Online Web Distribution with an option to print</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Haunted Tours Map <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>In this lesson, you will use what you have learned so far to create a map guide for a Haunted Tour service in Hollywood, CA.</strong></p>
                                        <ul>
                                            <li>High Quality vector map creation</li>
                                            <li>Manipulating vector maps</li>
                                            <li>Using the Ellipse Frame Tool</li>
                                            <li>Grouping Elements</li>
                                            <li>Working with Multiple FX on single object</li>
                                            <li>Output your map as a downloadable and Printable PDF</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    Postcard Promotional <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <div class="card-body copy">
                                        <p><strong>Learn how to setup and create a two-sided promotional postcard for print.</strong></p>
                                        <ul>
                                            <li>Setting up a 2-Sided Project for Print</li>
                                            <li>Working with custom-sized documents</li>
                                            <li>Bar Codes/QR Codes and Beyond</li>
                                            <li>Generating a QR Code</li>
                                            <li>Understanding Basic Blending Modes</li>
                                            <li>Using FX to blend edges</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Backend Tweaks & Secrets  <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn about the various ways to expand the power and features of InDesign by using Extensions and Scripts.</strong></p>
                                    <ul>
                                        <li>Extensions to know about</li>
                                        <li>Scripts to know about</li>
                                        <li>How to install extensions and scripts</li>
                                        <li>Managing Lists</li>
                                        <li>Style Modifications and tweaks</li>
                                        <li>Splitting and combining frames</li>
                                        <li>Advanced Sorting functions</li>
                                        <li>Batch image treatments</li>
                                        <li>Style tweaks and more!</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    The Newspaper - Design & Layout <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Paragraph Styles to design and layout a Newspaper for print.</strong></p>
                                    <ul>
                                        <li>Using Adobe Bridge and other programs to “tag” media</li>
                                        <li>Adding Metadata to media</li>
                                        <li>Adding Multiple images to multiple frames</li>
                                        <li>Working with image Captions</li>
                                        <li>Live vs Static Captions</li>
                                        <li>Manipulating Master Pages</li>
                                        <li>Creating Page Numbers</li>
                                        <li>Creating Repetitive Page Graphics</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    The Newspaper - Graphics & Images <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Take the Newspaper to the next level by creating and customizing graphics and prep images with captions.</strong></p>
                                    <ul>
                                        <li>Using Adobe Bridge and other programs to “tag” media</li>
                                        <li>Adding Metadata to media</li>
                                        <li>Adding Multiple images to multiple frames</li>
                                        <li>Working with image Captions</li>
                                        <li>Live vs Static Captions</li>
                                        <li>Manipulating Master Pages</li>
                                        <li>Creating Page Numbers</li>
                                        <li>Creating Repetitive Page Graphics</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    Switzerland Multi-Panel Brochure <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>In this lesson, you will learn to use InDesign's spell check feature, and the find and change feature.</strong></p>
                                    <ul>
                                        <li>Understanding the various brochure folds</li>
                                        <li>Learn about 3rd party plugins for InDesign</li>
                                        <li>Creating a bi-fold, four panel brochures</li>
                                        <li>Start Page Numbers</li>
                                        <li>Managing your panels in Pages</li>
                                        <li>Spreads vs Pages</li>
                                        <li>Duplicating and Deleting Spreads</li>
                                        <li>Enhancing Logos</li>
                                        <li>Adding graphics to text elements for contrast</li>
                                        <li>Design and add text for Front and Back Panels</li>
                                        <li>Casually recording Paragraph Styles & Swatches</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    Switzerland Brochure Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Add some wow factor to the travel brochure panels 3-4 by creating some amazing graphics.</strong></p>

                                    <ul>
                                        <li>Use the Direct Selection Tool to modify Frames</li>
                                        <li>Use the Direct Selection Tool to modify shapes</li>
                                        <li>Converting points to Bezier Curves</li>
                                        <li>Re-create a Polaroid-style frame to display your travel images</li>
                                        <li>Sharing graphics and text between Spreads</li>
                                        <li>Working with FX to create shadows</li>
                                        <li>Layering Graphics for visual impact</li>
                                        <li>Working with Slugs</li>
                                        <li>Previewing your project</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    Fundamental Publishing & Export <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Go into more depth on exporting for Print, Web and so much more. This lesson gives you an overview of your options for delivery and archive.</strong></p>
                                    <ul>
                                        <li>Understanding PDF Options for Print InDepth</li>
                                        <li>PDF/X Version Breakdown</li>
                                        <li>Creating your own PDF Presets</li>
                                        <li>Understanding the various options for Web Export (JPG, PNG, other) and why you would use one over another</li>
                                        <li>What happens to your color on export</li>
                                        <li>What is the EPUB standard?</li>
                                        <li>Notes on Sending your work to an Inkjet or Laserjet</li>
                                        <li>Exporting Marks for the Printer</li>
                                        <li>Understanding Color Conversion</li>
                                        <li>Packaging your Project for Archive or Transfer</li>
                                    <ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>