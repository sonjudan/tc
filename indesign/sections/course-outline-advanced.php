<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Infographics Design <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Take your design skills in InDesign to the next level and create various types of infographics for a colorful power presentation.</strong></p>
                                    <ul>
                                        <li>What infographics can realistically be created in InDesign</li>
                                        <li>The tools and techniques to make things happen</li>
                                        <li>Compound Paths and so much more</li>
                                        <li>Working with basic table data from excel</li>
                                        <li>Reasons use Illustrator with InDesign</li>
                                        <li>Creating quick 3D infographics in Illustrator for InDesign</li>
                                        <li>SVG vs other formats</li>
                                        <li>Working with SVG formats in 2020 and what you need to know</li>
                                        <li>Troubleshooting AI to ID Graphics Transfers</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Wonka Bar Menu – Working with Tables In-Depth <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to create, manage and style Tables like the pros in InDesign.</strong></p>
                                    <ul>
                                        <li>Creating Tables from Scratch</li>
                                        <li>Stylizing Tables</li>
                                        <li>Stylizing Table Content</li>
                                        <li>Selecting & Modifying Cells</li>
                                        <li>Excel to InDesign Tables</li>
                                        <li>Tab Delimited File Imports</li>
                                        <li>Text to Table Conversions</li>
                                        <li>Colorizing Tables</li>
                                        <li>Creative Table Design</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Ghost Toys - Intro to GREP <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>An introduction to GREP. GREP is a special language that lets you tell InDesign to find, change and apply all kinds of formatting - including Conditions that hide and show text at your command.</strong></p>
                                    <ul>
                                        <li>What is GREP</li>
                                        <li>Tips on Learning GREP</li>
                                        <li>Two Ways of Adding GREP in your Projects</li>
                                        <li>Composing a GREP Phrase</li>
                                        <li>Creating conditions for conditional text</li>
                                        <li>Using GREP to apply conditions</li>
                                        <li>Creating sets to manage conditions</li>
                                        <li>Using conditional images</li>
                                        <li>GREP in Paragraph and Character Styles</li>
                                        <li>The best GREP phrases you will want to know for pure speed and efficiency</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Fiction Novel - Formatting a Book <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to setup a Fiction Novel from scratch and use Paragraph, Character Styles & GREP to manage the books content.</strong></p>
                                    <ul>
                                        <li>Documents & Book Project contrasts</li>
                                        <li>The Anatomy of a Book</li>
                                        <li>Classic Trade book sizes</li>
                                        <li>Setting up a standard paperback book</li>
                                        <li>Creating Book Styles for easy formatting</li>
                                        <li>Formatting Master Pages</li>
                                        <li>Defining Sections</li>
                                        <li>Numbering and Organizing Sections</li>
                                        <li>Importing and Stylizing book text</li>
                                        <li>Auto-flow text</li>
                                        <li>Placeholder text for pre-formatting</li>
                                        <li>GREP for eliminating “Runts”</li>
                                        <li>Fixing and Eliminating Forced Line Breaks</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Fiction Novel - Indexes, TOCs & Beyond <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>An introduction to GREP. GREP is a special language that lets you tell InDesign to find, change and apply all kinds of formatting - including Conditions that hide and show text at your command.</strong></p>
                                    <ul>
                                        <li>The Difference between TOCs and Indexes</li>
                                        <li>Preparing content for a TOC</li>
                                        <li>Creating a Table of Contents</li>
                                        <li>Formatting between Numbers and Chapters</li>
                                        <li>Stylizing a Table of Content</li>
                                        <li>Working with Next Style</li>
                                        <li>Prepping a document for an Index</li>
                                        <li>Creating an Index</li>
                                        <li>Working with Scripts to manage data</li>
                                        <li>What are XMLs? How are they used?</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Fiction Novel - Creating a Cover <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use InDesign to get creative and design and layout a cover for a Fiction Novel complete with Spine.</strong></p>
                                    <ul>
                                        <li>Setup for 6” x 9” Trade Book with 200 Pages</li>
                                        <li>Creating a document with a Spine</li>
                                        <li>Manipulating multiple pages as one document</li>
                                        <li>Working with Photoshop and InDesign</li>
                                        <li>Working with Importing layered content</li>
                                        <li>Working with InDesign’s Layers Panel</li>
                                        <li>Naming and Arranging Layers</li>
                                        <li>Creating a Title for a Book Cover</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Working with Book Projects <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn about the benefits and power of working with Book Projects in InDesign.</strong></p>
                                    <ul>
                                        <li>More details about Documents vs Book Projects</li>
                                        <li>When a Book Project might be useful</li>
                                        <li>Creating a Book Project from scratch</li>
                                        <li>INDB File Formats</li>
                                        <li>Anatomy of the Book Panel</li>
                                        <li>Synchronizing Projects</li>
                                        <li>Preflighting your Book</li>
                                        <li>Error Reporting Details</li>
                                        <li>Status Symbols Meaning</li>
                                        <li>Page Numbering and Conversion</li>
                                        <li>Exporting your book to a PDF</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Pitch Deck Design & Layout <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>In this lesson, you will learn about Pitch Decks and how to create them in InDesign.</strong></p>
                                    <ul>
                                        <li>What is a Pitch Deck?</li>
                                        <li>How your Pitch Deck is going to be used</li>
                                        <li>Creating your Pitch Deck document</li>
                                        <li>Pitch Deck Sections</li>
                                        <li>Setting up Pitch Deck Master Pages</li>
                                        <li>Creative Continuity with Master Pages</li>
                                        <li>Designing a Title Page for the Deck</li>
                                        <li>Creativity with Transform More Options</li>
                                        <li>Organizing Layers in the Layer Panel</li>
                                        <li>Creative Page Titles with FX</li>
                                        <li>Getting Creative with Image Frame</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    Pitch Deck Design - Style & Graphics <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Create and manage attractive graphics through your Pitch Deck Project.</strong></p>
                                    <ul>
                                        <li>Working with Object Styles</li>
                                        <li>Color Continuity</li>
                                        <li>Enhancing Titles using Blend Modes</li>
                                        <li>Formatting Text using Styles and Next Style</li>
                                        <li>Creating a Gallery-Style TOC</li>
                                        <li>Understanding Clippings Masks</li>
                                        <li>Creating a Clipping Mask</li>
                                        <li>Pitch Deck Tweaks</li>
                                        <li>Exporting Deck to Preview</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Pitch Deck Design - Interactive Documents <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Convert and Design your Pitch Deck to be an Interactive Document for presentations and more.</strong></p>
                                    <ul>
                                        <li>Switching to the Interactive Workspace</li>
                                        <li>So many interactive features, so little time</li>
                                        <li>Exploring Interactive PDFs</li>
                                        <li>Create and manage Hyperlinks</li>
                                        <li>Understanding Text Anchors</li>
                                        <li>Interactive In-Document Navigation</li>
                                        <li>How to embed Video and Audio</li>
                                        <li>Buttons and Forms menu</li>
                                        <li>Exporting an Interactive PDF</li>
                                        <li>Testing your Interactive PDF</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>