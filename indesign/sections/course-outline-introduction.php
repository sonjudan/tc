<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Getting Started <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to navigate the InDesign Interface, manage and create workspaces and learn how to layout your first text. </strong></p>
                                    <ul>
                                        <li>What’s new in 2020</li>
                                        <li>Create New Document</li>
                                        <li>Understanding Units</li>
                                        <li>Creating a document</li>
                                        <li>Layout and Navigation</li>
                                        <li>Understanding Workspaces</li>
                                        <li>Accessing Panels</li>
                                        <li>Creating a Text Frame</li>
                                        <li>Frame vs Content</li>
                                        <li>Anatomy of a Frame</li>
                                        <li>Adding placeholder text</li>
                                        <li>Where to turn on Grids and Guides</li>
                                        <li>The Properties Panel</li>
                                        <li>The Character Panel (basic overview)</li>
                                        <li>The Paragraph Panel (basic overview)</li>
                                        <li>Basic Text Spacing & Alignment</li>
                                        <li>All about Adobe Fonts</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Top Secret! Letter Creation <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn more advanced text and graphics layout features as you re-create a de-classified UFO letter.</strong></p>
                                    <ul>
                                        <li>Creating a Print Document</li>
                                        <li>Understanding the Parts of a Letter</li>
                                        <li>Working with and Re-sizing Text Frames</li>
                                        <li>Importing and Managing Graphics</li>
                                        <li>Working with Bullets</li>
                                        <li>Saving an InDesign Project</li>
                                        <li>Multi-Frame Document</li>
                                        <li>More Text Attributes</li>
                                        <li>Position Text with Baseline Shift</li>
                                        <li>Paragraph Spacing</li>
                                        <li>Control Leading in Character Options</li>
                                        <li>Using Tabs</li>
                                        <li>Hanging Bullets and Check Marks</li>
                                        <li>Adding Hyphenation/Discretionary</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Simple Retro Ad <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Create an entire ad, pulling in a photo, graphic logo and laying out text.</strong></p>
                                    <ul>
                                        <li>Creating Document for Print</li>
                                        <li>Setting the Bleed</li>
                                        <li>The Rectangle Frame Tool</li>
                                        <li>Working with Headlines</li>
                                        <li>Placing Images</li>
                                        <li>Combining Text and Images</li>
                                        <li>Layout Adjustments</li>
                                        <li>Pages vs Master Pages</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Pyramid Pets Article <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn the basics of page layouts along with more text manipulation. You will master paragraph spacing, leading, and use tabs to complete the layout.</strong></p>
                                    <ul>
                                        <li>Creating a 1-Page article for web</li>
                                        <li>Simple Layout Sketching</li>
                                        <li>Creating a Document based on Sketch</li>
                                        <li>Working with a Multi-Column Document</li>
                                        <li>Rectangle Frame Tool Again!</li>
                                        <li>Adding Graphics Elements</li>
                                        <li>Placing and formatting images</li>
                                        <li>Text Wrapping</li>
                                        <li>Working with Loaded Cursors</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Pure Elegance: 2-Page Simple Magazine Ad <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to create and layout a 2-Page Magazine Ad for Print.</strong></p>
                                    <ul>
                                        <li>Setting up the Document as a 2-Page Spread</li>
                                        <li>Working with Facing Pages</li>
                                        <li>Understanding & Working with Bleed</li>
                                        <li>Working with and formatting Bulleted Lists</li>
                                        <li>Working with FX to solve object issues</li>
                                        <li>Simple Paragraph Formatting</li>
                                        <li>Adding Page Numbers</li>
                                        <li>Understanding the Layers Panel</li>
                                        <li>Working with Decorative Fonts</li>
                                        <li>Printing as a Spread</li>
                                        <li>Simple Web Export</li>
                                        <li>Working with the Pages Panel</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>