<?php
$meta_title = "Adobe InDesign 2019 Advanced | Hands-on beginner training";
$meta_description = "1-day Quick start InDesign class in Chicago and LA. Hands-on learning from InDesign experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../indesign-training.php">InDesign Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Advanced</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">InDesign 2019</h1>
                    <h3 data-aos="fade-up" data-aos-delay="50">Advanced Training Course</h3>

                    <div data-aos="fade-up" data-aos-delay="100">
                        <p>This 2-day advanced class is aimed at existing InDesign users who need to create more complex projects like books or decks, as well as learn how to improve productivity by using some of the more advanced features in InDesign.</p>
                        <p>Our instructors will lead you step-by-step through 10 unique projects. These include a movie poster, a restuarant menu, how to format copy using GREP, a novel (including cover and table of contents), a pitch deck, interactive presentations and much more.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-2" data-aos="fade-up"  data-aos-delay="150">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>2 Days</span>
                        <sup>$</sup>895<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Advanced" data-price="$895">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="InDesign Advanced" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="InDesign Advanced" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/indesign/TC_InDesign_2019_Advanced.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-indesign-advanced.png" alt="InDesign 2019 - Advanced Training Course" class="" width="295">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>It is recommended that you complete the <a href="/indesign/fundamentals.php">InDesign Fundamentals</a> class prior to commencing this training. Training available on Mac and PC.</p>
                    <p>View our full range of <a href="/indesign-training.php">InDesign training courses</a>, or see below for the detailed outline for InDesign Advanced.</p>
                </div>

                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Advanced" data-price="$895" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/indesign/sections/course-outline-advanced.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>