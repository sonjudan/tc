<?php
$meta_title = "Adobe InDesign 2019 Introduction | Hands-on beginner training";
$meta_description = "1-day Quick start InDesign class in Chicago and LA. Hands-on learning from InDesign experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../indesign-training.php">InDesign Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Quick Start</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">InDesign 2020</h1>
                    <h3 data-aos="fade-up" data-aos-delay="50">Quickstart Training Course</h3>

                    <div data-aos="fade-up" data-aos-delay="100">
                        <p>This 1-day beginner class is designed to get you up and running quickly in InDesign. You will learn to layout pages with text and graphics. In no time at all you will be able to control typography, add photos, create/use colors, and more!</p>
                        <p>The training consists of 5 unique Real World projects.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-2" data-aos="fade-up"  data-aos-delay="150">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>1 Day</span>
                        <sup>$</sup>495<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Quickstart" data-price="$495">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="InDesign Quick Start" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="InDesign Quick Start" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/indesign/TC_InDesign_2019_Quickstart.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-indesign-quickstart.png" alt="InDesign 2019 - Quickstart Training Course" class="" width="260">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>No prior experience of InDesign is needed. Training available on Mac and PC.</p>
                    <p>This 6-hour class is meant to get you started fast. If you want to  learn InDesign in greater depth, check out our 18-hour <a href="/indesign/fundamentals.php">InDesign Fundamentals</a> class or view our full range of <a href="/indesign-training.php">Adobe InDesign training courses</a>.</p>
                </div>

                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Quickstart" data-price="$495" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/indesign/sections/course-outline-introduction.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>