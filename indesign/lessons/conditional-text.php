<?php
$meta_title = "Working with Conditional Text in InDesign | Training Connection";
$meta_description = "Learn how to create documents with conditional text InDesign. course topic is covered in our Advanced InDesign Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Id">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Conditional Text</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Id.png" alt="Working with Conditional Text in InDesign">
                    </div>
                    <h1 class="" data-aos="fade-up">Working with Conditional Text in InDesign</h1>
                    <h5  data-aos="fade-up">These  topics are covered  in Module 4 of our <a class="" href="/indesign/advanced.php">Advanced Indesign class</a>.</h5>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <img class="pull-right ml-4" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-3.png" width="284" height="303" alt="Conditional Text - Panel">

                <ol>
                    <li>Open <strong>Conditional Text.indd</strong>.</li>
                    <li>
                        <p>Choose <strong>Window &gt; Type and Tables &gt; Conditional Text</strong> to open the panel. The panel already has conditions created and applied to various text items in the layout. Note the two versions of the word "color" and "colour" for the US and UK spelling; also the dimensions on the artwork is in "in" and "cm"; there are also prices for US, UK and Europe.</p>
                        <p><strong>Note: The text which has a condition applied to it, will have a wavy line below it with the corresponding color of the condition that is applied.</strong></p>
                        <p>For instructor-led <a href="/indesign-training.php">Adobe indesign classes in Chicago and Los Angeles</a> call us on (888) 815-0604.</p>
                    </li>
                </ol>

                <img class="pull-left ml-2 mr-5 mb-4" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-4.png" width="304" height="237" alt="New Condition Dialog Box">

                <ol class="mt-5 pt-3" start="3">
                    <li> Click the <strong>New Condition</strong> button at the bottom of the Conditional Text panel.</li>
                    <li> Name the condition "<strong>Price - US</strong>" and click OK.</li>
                </ol>

                <p>To quickly apply the Price-US condition to all the $ prices, we will use GREP.</p>

                <div class="clearfix mt-1"></div>

                <img class="pull-right mr-2 ml-5" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-5.png" width="399" height="380" alt="Find and Change">

                <ol start="5">
                    <li> Choose <strong>Edit &gt; Find/Change</strong>.</li>
                    <li> Click on the GREP option.</li>
                </ol>

                <p>Since the $ sign is a GREP instruction, we cannot type in just a $ to have GREP locate all the $ prices in the layout.</p>


                    </p><ol start="7">
                        <li>Type in "<strong>\$</strong>" in the Find What field.</li>
                        <li> To look for numbers after the $ sign, click on the button with the <strong>@ symbol</strong> on it to the
                            right of the Find What field. Choose <strong>ww &gt; Any Digit</strong>.
                            This will add a '<strong>\d</strong>" to the search instruction.
                            <br>
                            <br>
                        </li>
                    </ol>
                    <p class="imgCenter">
                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-6.png" width="435" height="313" alt="Choosing Wildcards"></p><br>
                    <ol start="9">
                        <li>			      To look for more than one number following the $ sign, choose              <strong>Repeat &gt; One or More Times</strong>.<br>
                            <br>
                        </li>
                    </ol>
                    <p class="imgCenter">
                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-7.png" width="438" height="286" alt="Repeat One or More Times"><br></p>
                    <br>
                    <p>
                        This will add a "+" to the search instruction.			Click the diskette symbol to the right of the Query to save this. Name it: <strong>Find Price $$</strong>. <a href="/indesign-training-chicago.php">InDesign classes in Chicago</a>.</p>
                    <ol start="10">
                        <li><strong>Click the icon</strong> to the right of the Change Format box at the bottom.

                            <br>
                        </li>
                    </ol>
                    <p class="imgCenter">

                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-8.png" width="338" height="313" alt="Change Format Box"></p><br>  <p><img class="imageright" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-1.png" width="532" height="301" alt="Change Format Setting"></p><br>
                    <br>
                    <ol start="11">
                        <li>Once the Change Format Setting box opens,              click on the <strong>Conditions</strong> option.</li>
                        <li> In the list of Conditions, select the <strong>Price-US</strong>              condition. Then click OK.</li>
                    </ol><br>
                    <br>
                    <p>
                        <img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-2.png" width="440" height="414" alt="GREP settings"></p>
                    <p>
                        <br>
                        <br>
                    </p>
                    <p>
                        In the Find/Change box, the Search should target the               Document. Click the Change All button.               You will see a pop up box showing that 4 changes
                        were made. In the document, the $ prices will now               be underlined with the color associated with the               condition. Once the text that is variable has all the               conditions applied to it, the visibility of the different
                        text items can be managed through Sets. <a href="/indesign-training-los-angeles.php">Los Angeles Adobe InDesign classes</a></p>


                    <br>


                    <br>
                    <br>

                    <p><img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-9.png" width="275" height="423" alt="Conditional Text"></p><br><br>
                    <ol start="13">
                        <li> In the Conditional Text panel choose <strong>Show Options</strong> form the           panel menu.
                            This will open a section at the bottom of the panel where sets can be           created to manage multiple conditions.</li>
                        <li>Click on the eyeball next to the conditions that should not be           visible, to turn them off, since all the conditions are currently visible.           Turn off the following conditions: <strong>Price-US; Price-Europe;           Size Europe and Text US/Europe</strong>. The conditions left visible will           show just the UK version text.</li>
                        <li>Click on the pull-down menu to the right of the <strong>Se</strong>t: and           choose <strong>Create New Set</strong>....<br><br><br>

                            <br>
                        </li>
                    </ol>


                    <p><img class="imageright" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-10.png" width="344" height="438" alt="Conditional Text - UK Price Sheet"></p><br>
                    <ol start="16">
                        <li> Name the set UK Price Sheet.</li>
                        <li> Repeat the same process for the US and Europe versions on the             sheet.</li>
                        <li> Once you have created the sets, select each of the three sets from             the pull-down menu to switch between each version of the price             sheet showing just the text that relates to that version.</li>
                    </ol>
                    <p>&nbsp;</p>
                    <p>
                        <strong>Note: [Unconditional] text is the text that is visible at all times.             The text with different conditions applied does not need             any spaces between the words for each condition. </strong></p><br>
                    <p>
                        Each set of conditions will manage the visibility of             each version of the price sheet for the US, UK and European options.
                    </p><br>
                    <p></p><p class="imgCenter">
                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-11.png" width="374" height="502" alt="Condition Set Name"></p><br>
                    <br>
                    <br>
                    <p>Practice with the names and conditions to create your own conditions as well.                Best to use it when needed to create a document with              different languages and still have the same style.</p><p>
                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-12.png" width="720" height="559" alt="US and UK Price sheets"></p><br><p>
                        <img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-13.png" width="720" height="283" alt="European Price Sheet"></p><br><br>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Id" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Other InDesign topics</h4>
                    <ul>
                        <li><a href="/indesign/lessons/conditional-text.php">Using conditional text in InDesign</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">InDesign Reviews</h4>
                    <p>There is no better way to learn Adobe InDesign than in one of our face-to-face instructor-led classes. We have trained hundreds of students how to use InDesign like pro's. Click on the following link to view a sample of our <a class="indesign" href="/testimonials.php?course_id=17">InDesign student reviews</a> or  contact us for instructor-led <a href="/indesign-training.php">Adobe InDesign training in Chicago and Los Angeles</a> on (888) 815-0604.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">InDesign courses</h4>
                    <ul>
                        <li><a class="indesign" href="/indesign/fundamentals.php">InDesign Fundamentals</a></li>
                        <li><a class="indesign" href="/indesign/advanced.php">InDesign Advanced</a></li>
                        <li><a class="indesign" href="/indesign/bootcamp.php">InDesign Bootcamp</a></li>

                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>