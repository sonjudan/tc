<?php
$meta_title = "Working with Conditional Text and Images in InDesign | Training Connection";
$meta_description = "Learn how to use conditional text with images in InDesign. This topic is covered in our Advanced InDesign Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Id">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Conditional Text with Images</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Id.png" alt="Using Conditional Text with Images in InDesign">
                    </div>
                    <h1 class="" data-aos="fade-up">Using Conditional Text with Images in InDesign</h1>
                    <h5  data-aos="fade-up">These  topics are covered  in Module 4 of our <a class="indesign" href="/indesign/advanced.php">InDesign Advanced training course</a>.</h5>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Conditional Text in InDesign is similar to Layer Comps in Photoshop. But in InDesign it is limited to text only.</p>
                <p>You can use conditional text with images by changing them to Inline Graphics. <a href="/indesign-training-chicago.php">Chicago Adobe InDesign classes</a>.</p>
                <p>Place the Small Zebra.PSD and Small Elephant.PSD on the second page. Type: This Zebra has Stripes. Copy the Zebra file and paste it at the end of the word to create a inline Graphics. Create a new Condition named Zebra.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-16.jpg" width="720" height="373" alt="Conditional Images workaround in InDesign"></p>
                <p><img class="imageright" src="https://www.trainingconnection.com/images/Lessons/InDesign/indesign-17.jpg" width="363" height="241" alt="Conditional images and text"></p>
                <p>Do the same with the Elephant image. Create a new condition. You can see that the image is selected with the type tool and will create a conditional Text. <a href="/indesign-training-los-angeles.php">LA-based InDesign classes</a>.</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Id" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Other InDesign topics</h4>
                    <ul>
                        <li><a href="/indesign/lessons/conditional-text.php">Using conditional text in InDesign</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">InDesign Reviews</h4>
                    <p>There is no better way to learn Adobe InDesign than in one of our face-to-face instructor-led classes. We have trained hundreds of students how to use InDesign like pro's. Click on the following link to view a sample of our <a class="indesign" href="/testimonials.php?course_id=17">InDesign student reviews</a> or  contact us for instructor-led <a href="/indesign-training.php">Adobe InDesign training in Chicago and Los Angeles</a> on (888) 815-0604.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">InDesign courses</h4>
                    <ul>
                        <li><a class="indesign" href="/indesign/fundamentals.php">InDesign Fundamentals</a></li>
                        <li><a class="indesign" href="/indesign/advanced.php">InDesign Advanced</a></li>
                        <li><a class="indesign" href="/indesign/bootcamp.php">InDesign Bootcamp</a></li>

                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>