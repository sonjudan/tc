<?php
$meta_title = "Adobe InDesign 2019 Bootcamp | Hands-on beginner training";
$meta_description = "1-day Quick start InDesign class in Chicago and LA. Hands-on learning from InDesign experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../indesign-training.php">InDesign Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Bootcamp</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">InDesign 2020</h1>
                    <h3 data-aos="fade-up" data-aos-delay="50">Bootcamp Training Course</h3>

                    <div data-aos="fade-up" data-aos-delay="100">
                        <p>This bootcamp you will take you from an InDesign beginner to expert. You will complete a series of Real World publishing projects including an advertisement, a magazine, a corporate brochure, a novel, a movie poster, a receipe book, a restaurant menu, a travel guide, pitch deck, and much more.</p>
                        <p>Our instructors will lead you step-by-step through each project, teaching the correct workflow, how to master each tool and useful shortcuts to speed up your production. You will leave the course fully competent and able to use	InDesign in a professional design environment.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-2" data-aos="fade-up"  data-aos-delay="150">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>5 Days</span>
                        <sup>$</sup>1695<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Bootcamp" data-price="$1695">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="InDesign Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="InDesign Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/indesign/TC_InDesign_2019_Bootcamp.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-indesign-bootcamp.png" alt="InDesign 2019 - Bootcamp Training Course" class="" width="310">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>No prior experience of InDesign is needed. Training available on Mac and PC.</p>
                    <p>View our full range of <a href="/indesign-training.php">Adobe InDesign courses</a>.</p>
                </div>

                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Bootcamp" data-price="$1695" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/indesign/sections/course-outline-bootcamp.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>