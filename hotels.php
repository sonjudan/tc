<?php
$meta_title = "Recommended Hotels | Training Connection";
$meta_description = "Recommended hotels near to our training centers.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Where to stay</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Recommended Hotels</h1>
                </div>
            </div>
        </div>


        <div class="section-list-hotels">
            <div class="container">
                <div class="card-default">
                    <h4>Chicago</h4>

                    <div class="card-body">
                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/club-quarters-hotel.jpg" alt="Club Quarters (Central Loop) Hotel">
                            </div>
                            <div class="post-body">
                                <div class="post-heading">
                                    <h2>Club Quarters (Central Loop) Hotel</h2>
                                    <p>(4 blocks from our training center)</p>
                                </div>
                                <p>Room rates (excel taxes) as follows:</p>

                                <div class="row row-sm">
                                    <div class="col-xl-6">
                                        <div class="card-schedule">
                                            <div class="sched-date">
                                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                                Mar 18 - Nov 17
                                            </div>
                                            <ul>
                                                <li>Club room - $182</li>
                                                <li>Standard room - $197</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="card-schedule">
                                            <div class="sched-date">
                                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                                Nov 18 - Mar 17
                                            </div>
                                            <ul>
                                                <li>Club room - $134</li>
                                                <li>Standard room - $149</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <p>To book visit <a href="#">here</a> (enter password "TRAININGCONNECTION" )</p>
                            </div>

                        </article>

                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/la-quinta-inn.jpg" alt="La Quinta Inn & Suites Chicago Downtown">
                            </div>
                            <div class="post-body">
                                <div class="post-heading">
                                    <h2>La Quinta Inn & Suites Chicago Downtown</h2>
                                    <p>(only 1 block from our training center)</p>
                                </div>
                                <p>The newly renovated La Quinta Inn & Suites offers 10% off of our Best Available Rate which is offered 365 days a year. This rate includes our full, hot breakfast buffet each morning, complimentary high-speed wireless internet access, 24 hour Fitness & Business centers, and all rooms are equipped with mini-refrigerators, microwaves, and Keurig coffee makers.</p>

                                <p>To book your upcoming reservation please click <a href="#">here</a></p>
                            </div>

                        </article>

                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/w-chicago-city-center.jpg" alt="W Chicago City Center">
                            </div>
                            <div class="post-body">
                                <div class="post-heading b-0">
                                    <h2>W Chicago City Center</h2>
                                    <p>Both these hotels can also be booked on <a href="#">here</a></p>

                                </div>

                            </div>

                        </article>
                    </div>

                </div>

                <div class="card-default">
                    <h4>Los Angeles</h4>

                    <div class="card-body">
                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/mayfair-hotel.jpg" alt="The Mayfair Hotel Los Angeles">
                            </div>
                            <div class="post-body">
                                <h2>The Mayfair Hotel Los Angeles</h2>
                                <p>Walking distance and the cheapest option. Used to be quite run down but a recent renovation has seen big improvements. Best value hotel in the area. Walk time - 9 minutes.</p>

                                <ul>
                                    <li>
                                        <i class="mr-2 fas fa-phone"></i>
                                        Tel: (866) 734-6018
                                    </li>
                                    <li> 
                                        <i class="mr-2 fas fa-window-maximize"></i>
                                        Hotel <a href="https://www.mayfairla.com/">website</a> </li>
                                    <li>
                                        <i class="mr-2 fas fa-hotel"></i>
                                        View on <a href="hotels.com" target="_blank">Hotels.com</a>
                                    </li>
                                </ul>
                            </div>

                        </article>

                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/figueroa-hotel.jpg" alt="Hotel Figueroa Downtown Los Angeles">
                            </div>
                            <div class="post-body">
                                <h2>Hotel Figueroa Downtown Los Angeles</h2>
                                <p>Trendy hotel also walking distance, near Staples center/restaurants etc. So nicer area than Mayfair but more expensive. Walk time - 9 minutes</p>
                                <ul>
                                    <li>
                                        <i class="mr-2 fas fa-phone"></i>
                                        Tel: (866) 734-6018
                                    </li>
                                    <li> 
                                    <i class="mr-2 fas fa-window-maximize"></i>
                                        Hotel <a href="https://www.hotelfigueroa.com/">website</a> </li>
                                    <li>
                                        <i class="mr-2 fas fa-hotel"></i>
                                        View on <a href="hotels.com" target="_blank">Hotels.com</a>
                                    </li>
                                </ul>
                            </div>

                        </article>

                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/standard-hotel.jpg" alt="The Standard Downtown LA">
                            </div>
                            <div class="post-body">
                                <h2>The Standard Downtown LA</h2>
                                <p>Also a decent option - similar to Hotel Figueroa in price. Walk time - 5 mins</p>
                                <ul>
                                    <li>
                                        <i class="mr-2 fas fa-phone"></i>
                                        Tel: (213) 892-8080
                                    </li>
                                    <li> 
                                    <i class="mr-2 fas fa-window-maximize"></i>
                                        Hotel <a href="http://www.standardhotels.com/la/properties/downtown-la">website </a></li>
                                    <li>
                                        <i class="mr-2 fas fa-hotel"></i>
                                        View on <a href="hotels.com" target="_blank">Hotels.com</a>
                                    </li>
                                </ul>

                            </div>

                        </article>

                        <article class="post-inline-block">
                            <div class="post-img">
                                <img src="/dist/images/hotels/intercontinental-hotel.jpg" alt="Intercontinental Los Angeles Downtown">
                            </div>
                            <div class="post-body">
                                <h2>Intercontinental Los Angeles Downtown</h2>
                                <p>Brand new hotel - literally across the road from our training center. Walk time - 2 mins</p>
                                <ul>
                                    <li>
                                        <i class="mr-2 fas fa-phone"></i>
                                        Tel: (213) 688-7777
                                    </li>
                                    <li> 
                                    <i class="mr-2 fas fa-window-maximize"></i>
                                        Hotel <a href="https://dtla.intercontinental.com/">website</a> </li>
                                    <li>
                                        <i class="mr-2 fas fa-hotel"></i>
                                        View on <a href="hotels.com" target="_blank">Hotels.com</a>
                                    </li>
                                </ul>

                            </div>

                        </article>


                    </div>

                </div>
            </div>
        </div>



    </main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>