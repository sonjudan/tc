<?php
$meta_title = "Outlook Training Classes | Chicago & Los Angeles | Training Connection";
$meta_description = "Need to learn Microsoft Outlook? Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Outlook Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-outlook.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">Outlook Training</h1>

                    <div data-aos="fade-up">
                        <h4>Chicago, Los Angeles</h4>
                        <p>All our Outlook classes are taught by live instructors present in the classroom. </p>
                        <p>Suitable for Outlook 2013, 2016, 2019 &amp; 365 users.</p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-outlook-2019.png" alt="MS Outlook Software">
                    <img src="/dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">
                <h3>What's Included</h3>
                <ul>
                    <li>Outlook training manual</li>
                    <li>Certificate of course completion</li>
                    <li>Small class sizes</li>
                    <li>6-month FREE repeat (in case you need a refresher)</li>
                </ul>
                <p>
                    <strong>Book an Outlook training course today. All classes guaranteed to run! </strong><br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading"  data-aos="fade-up">
                <h2>MS Outlook course outlines</h2>
            </div>

            <div class="accordion-classes g-text-outlook" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Microsoft Outlook Level 1 - Introduction"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Outlook Level 1 - Introduction
                                </a>
                            </h3>
                            <p>This Outlook course will provide you with the skills you need to start sending and responding to email in Microsoft Office Outlook, as well as maintaining your Calendar, scheduling meetings, and working with tasks and notes.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="Outlook Level 1 - Introduction" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/outlook/Outlook%20Level%201.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-md btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="far fa-calendar-alt mr-2"></i>
                                        Timetable
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Outlook Level 1 - Introduction">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable Chicago
                                        </a>
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Outlook Level 1 - Introduction">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable LA
                                        </a>
                                    </div>
                                </div>

                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Outlook Essentials</h5>
                                        <ul>
                                            <li>Getting Started</li>
                                            <li>Exploring the Outlook Window</li>
                                            <li>Navigating in Outlook</li>
                                            <li>The Navigation Pane</li>
                                            <li>Getting Help</li>
                                            <li>Using Outlook Help</li>
                                            <li>Obtaining Context-Sensitive Help</li>
                                            <li>Printing Documents</li>
                                            <li>Setting Up the Page</li>
                                            <li>Selecting Additional Printing Options</li>
                                            <li> Previewing and Printing a Document</li>
                                            <li>Ending an Outlook Session</li>
                                            <li>Saving Your Data</li>
                                            <li>Minimizing and Restoring Outlook</li>
                                            <li>Exiting from Outlook and Logging Off</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Communicating with Mail</h5>
                                        <ul>
                                            <li>Using the Inbox</li>
                                            <li>Checking for New Messages</li>
                                            <li>Previewing and Opening Messages</li>
                                            <li>The Ribbon</li>
                                            <li>Selecting and Printing a Message</li>
                                            <li>Closing a Message</li>
                                            <li>Sorting Messages</li>
                                            <li>Filtering Messages</li>
                                            <li>Composing a New Message</li>
                                            <li>Addressing and Typing a Message</li>
                                            <li>Editing Text</li>
                                            <li>Formatting Text</li>
                                            <li>Using AutoComplete</li>
                                            <li>Including a Signature with a Message</li>
                                            <li>Correcting Spelling as You Type</li>
                                            <li>Sending a Message</li>
                                            <li>Forwarding and Replying to Messages</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <ul>
                                            <li>Replying to a Message</li>
                                            <li>Managing Messages Using Folders</li>

                                            <li>Moving a Message to a Folder</li>
                                            <li>Deleting and Restoring a Message</li>
                                            <li>Customizing your Signature</li>
                                            <li>Setting up an Out of Office Reply</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Organising Contacts </h5>
                                        <ul>
                                            <li>Creating a Contact List</li>
                                            <li>Adding Contacts Manually</li>
                                            <li>Selecting and Editing an Address Card</li>
                                            <li>Transmitting and Adding Contacts with E-mail</li>
                                            <li>Managing Contacts</li>
                                            <li>Changing the Current View</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Scheduling with Calendar</h5>
                                        <ul>
                                            <li>Navigating in Calendar</li>
                                            <li>Viewing a Day, a Week, or a Month</li>
                                            <li>Using the Date Navigator</li>
                                            <li>Viewing a Range of Dates, Several Weeks, and Discontiguous Days</li>
                                            <li>Showing Two Time Zones</li>
                                            <li>Making and Moving Appointments</li>
                                            <li>Selecting an Appointment and Modifying the Date in Day View</li>
                                            <li>Modifying the Date in Week View</li>
                                            <li>Changing the Time of an Appointment</li>
                                            <li>Managing Appointments</li>
                                            <li>Editing Recurring Appointments</li>
                                            <li>Creating Tentative Appointments</li>
                                            <li>Inserting All Day Events</li>
                                            <li>Deleting Appointments</li>
                                            <li>Restoring Deleted Appointments</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Tasks</h5>
                                        <ul>
                                            <li>Working with Tasks</li>
                                            <li>Creating a Task</li>
                                            <li>Selecting and Editing a Task</li>
                                            <li>Making a Task Recurring</li>
                                            <li>Deleting and Restoring a Task</li>
                                            <li>Managing Tasks</li>
                                            <li>Setting the Priority for a Task</li>
                                            <li>Tracking a Task's Status and Marking a Task Completed</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Keeping a Journal and Using Notes</h5>
                                        <ul>
                                            <li>Tracking Activities</li>
                                            <li>Working with Notes</li>
                                            <li>Writing a Note</li>
                                            <li>Opening and Editing a Note</li>
                                            <li>Organising Notes</li>
                                            <li>Changing Icon Size</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Microsoft Outlook Level 2 - Advanced"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Outlook Level 2 - Advanced
                                </a>
                            </h3>
                            <p>This Outlook course is designed for experienced Outlook users who need to customize their environment, Calendar, and email messages to meet their specific requirements and who wish to track, share, assign, and locate various Outlook items.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="Outlook Level 2 - Advanced" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/outlook/Outlook%20Level%202.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-md btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="far fa-calendar-alt mr-2"></i>
                                        Timetable
                                    </button>
                                    <div class="dropdown-menu">
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Outlook Level 2 - Advanced">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable Chicago
                                        </a>
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Outlook Level 2 - Advanced">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable LA
                                        </a>
                                    </div>
                                </div>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Using Advanced Management Features</h5>
                                        <ul>
                                            <li>Managing Items</li>
                                            <li>Setting Up AutoArchiving</li>
                                            <li>Archiving Manually</li>
                                            <li>Retrieving Archived Items</li>
                                            <li>Creating and Applying Categories</li>
                                            <li>Finding Items</li>
                                            <li>Grouping Items</li>
                                            <li>Creating and Clearing a Filter</li>
                                            <li>Customizing Outlook</li>
                                            <li>Setting Options</li>
                                            <li>Adding Shortcuts to the Outlook Bar</li>
                                            <li>Moving a Shortcut on the Outlook Bar</li>
                                            <li>Removing a Shortcut from the Outlook Bar</li>
                                            <li>Changing Your Password</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Contacts</h5>
                                        <ul>
                                            <li>Refining Your Contact List</li>
                                            <li>Adding a Contact from the Same Company</li>
                                            <li>Using the File as Feature</li>
                                            <li>Accessing Contacts Directly</li>
                                            <li>Sending a Message to a Contact</li>
                                            <li>Accessing a Contact's Web Page</li>
                                            <li>Managing Favorite Web Addresses</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Optimizing Mail Functions</h5>
                                        <ul>
                                            <li>Using Address Books</li>
                                            <li>Using an Address Book to Address a Message</li>
                                            <li>Adding a Name to Your Personal Address Book</li>
                                            <li>Creating an Alias in Your Personal Address Book</li>
                                            <li>Selecting a Default Address Book</li>
                                            <li>Working with Personal Distribution Lists</li>
                                            <li>Creating a Personal Distribution List</li>
                                            <li>Editing a Personal Distribution List</li>
                                            <li>Deleting a Personal Distribution List</li>
                                            <li>Using Message Features</li>
                                            <li>Saving and Closing a Message without Sending It</li>
                                            <li>Setting Spelling Options</li>
                                            <li>Setting Additional Spelling Features</li>
                                            <li>Setting and Using Message Options</li>
                                            <li>Using Voting Buttons</li>
                                            <li>Flagging a Message</li>
                                            <li>Recalling a Message</li>
                                            <li>Attaching a File</li>
                                            <li>Opening, Closing, and Saving an Attached File</li>
                                            <li>Accessing the Internet</li>
                                            <li>Inserting and Editing a Hyperlink in a Message</li>
                                            <li>Using a Hyperlink in a Message</li>
                                            <li>Using the Web Toolbar</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Other Users</h5>
                                        <ul>
                                            <li>Scheduling Meetings</li>
                                            <li>Inviting Colleagues and Setting a Meeting Time</li>
                                            <li>Inviting a Contact to a Meeting</li>
                                            <li>Responding to a Meeting Request</li>
                                            <li>Reviewing Meeting Information</li>
                                            <li>Rescheduling and Canceling a Meeting</li>
                                            <li>Scheduling an Event</li>
                                            <li>Completing Tasks</li>
                                            <li>Scheduling a Task</li>
                                            <li>Regenerating a Task</li>
                                            <li>Assigning a Task to Someone Else</li>
                                            <li>Responding to a Task Request</li>
                                            <li>Reclaiming Ownership of a Declined Task</li>
                                            <li>Creating a New Item from an Existing One</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Templates and Forms</h5>
                                        <ul>
                                            <li>Working with Templates</li>
                                            <li>Using a Template</li>
                                            <li>Creating a Template</li>
                                            <li>Utilizing Forms</li>
                                            <li>Installing Sample Forms</li>
                                            <li>Using a Form</li>
                                            <li>Creating a Form</li>
                                            <li>Modifying an Existing Form</li>
                                            <li>Publishing and Saving a New Form</li>
                                            <li>Creating a Form from Another Program</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <hr class="m-0">
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>




    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
              <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that  trainees often benefit from repeating their class. Included in your course price is a FREE Repeat valid for 6 months.  Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Our Outlook class guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Outlook is not for everyone. If you decide that after  half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Outlook Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Outlook. This can be delivered onsite at your premises, at our training center or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" checked><i></i> <h3>Outlook Level 1 - Introduction</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Outlook Level 2 - Advanced</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

    <div class="section section-faq ">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Outlook Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best Outlook learning experience for our clients in Chicago and Los Angeles. Please find answers to some of our most frequently asked Outlook training related questions below, or contact us should you require any further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
              <li>
                <h5>What version of Outlook do you teach on?</h5>
                <p>We are currently teaching our classes on Outlook 2019. Outlook 2019 is similar to Outlook 2016 and Outlook 2013, so the training is suitable for all 3 versions of Outlook.</p>
              </li>
              <li>
                <h5>Where are your Outlook classes held?</h5>
                <p>We have two training centers. Our Los Angeles Outlook classes are held at 915 Wilshire Blvd, Suite 1800, Los Angeles, CA 90017. For more information about directions, parking, trains please <a href="/contact-us-los-angeles.php">click here</a>.</p>
                <p>Our Chicago Outlook classes are held at 230 W Monroe Street, Suite 610, Chicago IL 60606. For more information about directions, parking, trains please <a href="/contact-us-chicago.php">click here</a>.</p>
              </li>
              <li>
                <h5>Is this face-to-face training or a webinar?</h5>
                <p>Our Outlook classes are taught by live trainers present in the classroom. Having a trainer on hand is a far superior learning experience to a webinar. </p>
              </li>
              <li>
                <h5>What are the Outlook class times?</h5>
                <p>Our MS Outlook classes start at 9.00am and finish at approximately 4.30pm. We take an hour lunch break around 12.15pm.</p>
              </li>
              <li>
                <h5>Do I need to bring my own computer?</h5>
                <p>No, we provide the computers.</p>
              </li>
              <li>
                <h5>Can you deliver Outlook training onsite at our location?</h5>
                <p>Yes, we service the greater Chicago and Los Angeles metros. We also provide onsite Outlook training at client sites right across the country. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Outlook training.</p>
              </li>
             
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-outlook.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>