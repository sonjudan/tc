<?php
$meta_title = "Access Training Classes | Chicago | Training Connection";
$meta_description = "Need to learn Microsoft Access? Our face-to-face instructor-led classes are still the best way to learn. Call 312.698.4475 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Access Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-access.jpg');">

        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">Access Training<br>
                      Chicago
                    </h1>

                    <div data-aos="fade-up">
						 <h4>Access 2013, 2016, 2019 &amp; 365</h4>
                        <p>We offer 2 levels of Access training  in Chicago. Our  hands-on Microsoft Access classes are taught by experienced Access database developers.</p>
                        <p>The instructors are live in the classroom. This is not a webinar! </p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-access-2019.png" alt="MS Access 2019 box shot">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">
                <h3>What's Included</h3>
                <ul>
                    <li>A printed Access training manual</li>
                    <li>Certificate of course completion</li>
                    <li>Small class size (average of 3 students)</li>
                    <li>Free  repeat valid for 6 months.</li>
                </ul>
                <p><strong>Book an Access training course today. All classes guaranteed to run! </strong><br>
                <a class="" href="/onsite-training.php">Onsite Access training available countrywide.</a><br>
                    <a class="" href="/onsite-training.php"></a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-package-deals" class="btn btn-excel btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-box mr-2"></i>
                        Package deals
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0" data-aos="fade-up">
                <h2>Microsoft Access Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-access" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Access Fundamentals"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Access Fundamentals
                                </a>
                            </h3>
                            <p>This class is for participants new to Microsoft Access. You will learn basic relational database concepts, and how to design and build an efficient  database, construct data fields and tables, run queries,  design forms and build reports.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Access Fundamentals" data-price="$895">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/access/Access%20Fundamentals.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Access Fundamentals">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>

                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>2 Days</span>
                                <sup>$</sup>895
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Database Fundamentals</h5>
                                        <ul>
                                            <li>Understanding general database terms</li>
                                            <li>Database management systems</li>
                                            <li>Table Overview</li>
                                            <li>Data types</li>
                                            <li>Table metadata</li>
                                            <li>Queries and SQL</li>
                                            <li>SQL Statements</li>
                                            <li>Relational databases</li>
                                            <li>Flat file databases</li>
                                            <li>NoSQL databases</li>
                                            <li>Advantages of relational databases</li>
                                            <li>Access interface and objects</li>
                                            <li>New databases</li>
                                            <li>Existing databases</li>
                                            <li>The navigation Pane</li>
                                            <li>Access objects: Tables, Queries, Forms and Reports</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Database Design</h5>
                                        <ul>
                                            <li>Fundamentals of efficient design</li>
                                            <li>Modeling</li>
                                            <li>The logical model</li>
                                            <li>The physical model</li>
                                            <li>Normal forms</li>
                                            <li>Creating and Modifying databases</li>
                                            <li>Choosing data types: Numeric, String, Other</li>
                                            <li>Constraints</li>
                                            <li>Changing table structure</li>
                                            <li>Changing data types</li>
                                            <li>Adding and deleting fields</li>
                                            <li>Relationships and Keys</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Tables</h5>
                                        <ul>
                                            <li>Creating a simple table</li>
                                            <li>Creating tables in Design View</li>
                                            <li>Creating fields</li>
                                            <li>Creating fields in Datasheet view</li>
                                            <li>Creating fields in Table Design view</li>
                                            <li>Setting a Primary Field</li>
                                            <li>Data Validation using Field Properties</li>
                                            <li>Setting validation rules</li>
                                            <li>Setting table-level validation</li>
                                            <li>Relationships and keys</li>
                                            <li>The importance of primary keys and indexes</li>
                                            <li>Deciding whether you need unique indexes</li>
                                            <li>Creating relationships</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Queries</h5>
                                        <ul>
                                            <li>Creating basic queries</li>
                                            <li>Select queries</li>
                                            <li>Action queries</li>
                                            <li>Adding data sources to a query</li>
                                            <li>Selecting query fields</li>
                                            <li>Query criteria</li>
                                            <li>Adding mutiple criteria conditions</li>
                                            <li>Parameter queries</li>
                                            <li>Total queries</li>
                                            <li>Cross tab queries</li>
                                            <li>Append queries</li>
                                            <li>Update queries</li>
                                            <li>Delete queries</li>
                                            <li>Modifying queries</li>
                                            <li>Sorting query results</li>
                                            <li>Query field formatting</li>
                                            <li>Using calculated fields</li>
                                            <li>Creating calculated Yes/No fields</li>
                                            <li>Calculated text fields</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Forms</h5>
                                        <ul>
                                            <li>Creating simple forms</li>
                                            <li>Basic form design options</li>
                                            <li>Bound and unbound form controls</li>
                                            <li>Using the Form Wizard</li>
                                            <li>Form Design</li>
                                            <li>Displaying form properties</li>
                                            <li>Form sections</li>
                                            <li>Form controls</li>
                                            <li>Adding bound controls to a form</li>
                                            <li>Adding controls form the field list</li>
                                            <li>Types of controls</li>
                                            <li>Test box and label controls</li>
                                            <li>List controls</li>
                                            <li>Adding a command button</li>
                                            <li>Changing the layout of controls</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Reports</h5>
                                        <ul>
                                            <li>Creating Reports</li>
                                            <li>Report basics</li>
                                            <li>Setting up data sources</li>
                                            <li>Filtering and ordering reports</li>
                                            <li>Automatic report generators</li>
                                            <li>Using the Report Wizard</li>
                                            <li>Report Controls</li>
                                            <li>Calculated fields</li>
                                            <li>Grouping and summarizing</li>
                                            <li>Formatting Reports</li>
                                            <li>Report Page Setup options</li>
                                            <li>Images and other objects</li>
                                            <li>Themes</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="MS Access Advanced"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Access Advanced
                                </a>
                            </h3>
                            <p>This course is aimed at Intermediate users of Access and covers topics such as building advanced forms, writing advanced queries, creating advanced reports and how to automate operations using Macros.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Access Advanced" data-price="$450">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/access/Access%20Advanced.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Access Advanced">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable Chicago
                                </a>

                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>450
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Queries</h5>
                                        <ul>
                                            <li>Grouping and summarizing</li>
                                            <li>About grouping</li>
                                            <li>Building totals queries</li>
                                            <li> Totals query design</li>
                                            <li> Where totals</li>
                                            <li> Expression totals</li>
                                            <li> Common aggregate functions</li>
                                            <li> Updating, deleting, and relationships</li>
                                            <li> Referential integrity</li>
                                            <li> Cascading practices</li>
                                            <li> About cascading deletes</li>
                                            <li> Allowing deletes in key fields</li>
                                            <li> Allowing updates in key fields</li>
                                            <li> Indexing for performance</li>
                                            <li> About indexes</li>
                                            <li> How an index works</li>
                                            <li> Index planning</li>
                                            <li> Creating indexes in tables</li>
                                            <li> Setting index properties</li>
                                            <li> Maximizing performance</li>
                                            <li> Specialized query types</li>
                                            <li> Pass-Through Queries</li>
                                            <li> Data Definition Query</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Forms</h5>
                                        <ul>
                                            <li>About subforms</li>
                                            <li>Creating the main form and subform using the Form Wizard</li>
                                            <li>Creating a subform using the SubForm Wizard</li>
                                            <li>Using an existing form as a subform</li>
                                            <li>Nested subforms</li>
                                            <li>Linking forms</li>
                                            <li>Main form–subform data relationships</li>
                                            <li>The hyperlink control</li>
                                            <li>Adding a hyperlink to a form</li>
                                            <li>The Web Browser control</li>
                                            <li>The attachment control</li>
                                            <li>Managing attachments in a form</li>
                                            <li>The chart control</li>
                                            <li>Specialized form types</li>
                                            <li>About navigation forms</li>
                                            <li>Creating a navigation form</li>
                                            <li>Modal dialog forms</li>
                                            <li>Creating a database start-up form</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Reports</h5>
                                        <ul>
                                            <li>About subreports</li>
                                            <li>Subreport record sources</li>
                                            <li>Main and subreport with the same record source</li>
                                            <li>Main and subreport with related record sources</li>
                                            <li>Using an unbound main report as a container</li>
                                            <li>Advanced grouping</li>
                                            <li>Date ranges</li>
                                            <li>Running sums</li>
                                            <li>Advanced formatting</li>
                                            <li>Banded reports</li>
                                            <li>Formatting and design options</li>
                                            <li>Showing Username name</li>
                                            <li>Using a logo</li>
                                            <li>Showing and hiding for printing</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Macros</h5>
                                        <ul>
                                            <li>Creating a macro</li>
                                            <li>Planning macros</li>
                                            <li>Using the Macro Builder</li>
                                            <li>Using Macro Builder features</li>
                                            <li>Embedded macros</li>
                                            <li>Assigning macros to events</li>
                                            <li>Data macros</li>
                                            <li>Using variables</li>
                                            <li>Variable scope</li>
                                            <li>Choosing variable sources</li>
                                            <li>When to use variables</li>
                                            <li>Creating macros with variables</li>
                                            <li>Troubleshooting macros</li>
                                            <li>Error handling basics</li>
                                            <li>Stepping through macros</li>
                                            <li>Displaying variable values</li>
                                            <li>Using the OnError action</li>
                                            <li>Using the MacroError Object</li>
                                            <li>Designing robust macros</li>
                                            <li>Logging Debug info</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Importing and Exporting Data</h5>
                                        <ul>
                                            <li>Data exchange basics</li>
                                            <li>Choosing which parts of Access to use</li>
                                            <li>Other database applications</li>
                                            <li>OCDB databases</li>
                                            <li>Saving import and export procedures</li>
                                            <li>Import options</li>
                                            <li>Export basics</li>
                                            <li>Exchanging data with other Access databases</li>
                                            <li>Linking to Access tables</li>
                                            <li>Importing data in Access</li>
                                            <li>Exporting Access objects</li>
                                            <li>Exchanging data with Excel</li>
                                            <li>Importing from Excel</li>
                                            <li>Linking to Excel data</li>
                                            <li>Appending Excel data</li>
                                            <li>Exporting data to Excel</li>
                                            <li>Using Text and XML data files</li>
                                            <li>Exporting to text files</li>
                                            <li>Exporting to XML</li>
                                            <li>Importing from text files</li>
                                            <li>Importing from XML files</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Database Management</h5>
                                        <ul>
                                            <li>Preparing and running Compact & Repair</li>
                                            <li>Running Compact & Repair automatically</li>
                                            <li>Restoring a database</li>
                                            <li>Other maintenance tasks</li>
                                            <li>Encrypting a database</li>
                                            <li>Creating an executable file</li>
                                            <li>Documenting the database structure</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/package-deals-access.php'; ?>

    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>

    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Access training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Microsoft Access classes - 5 rating"></span>
                    </div>

                    <h3><span>Access classes rating:</span> 4.7 stars from 1,895 reviewers </h3>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Jeff was great, going above and beyond, and helping us with specific questions that we had without taking away from the learning experience of others.&quot;</p>
                                <span><strong>Monyrotana Keo | Paramount Unified School District</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I enjoyed everything about this class. Your staff member here was so enjoyable. The Instructor "Chris" was very nice and was able to work well the diverse amount of knowledge and mixed abilities of the students&quot;</p>
                                <span><strong>Mike McCallister | Maricopa Comm College Dist</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Chris is excellent! He answered all of our questions and maintained a good pace for us to learn MS Access. I hope to see him again when I take the Intermediate level of MS Access.&quot;</p>
                                <span><strong>Laura Flenoury | Cal State Los Angeles</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Great class! Great Instructor! I'm from Georgia and I plan to come again to take courses with Training Connection. Best money I ever spent for a class!&quot;</p>
                                <span><strong>Kay Walters</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Great intro course! Took me from knowing nothing to feeling like I could go out and construct my own database! Looking forward to learning more at higher levels.&quot;</p>
                                <span><strong>Ted Hixson | BPI group</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=18" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>

    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that Microsoft Access is a complex program, and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months. Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Money back guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Access is not for everyone. If you decide that after half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Access Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Access. This can be delivered onsite at your premises, at our training center in the Chicago Loop or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training">
                            <i></i> Private Training <span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Access Fundamentals</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Access Advanced</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

    <div class="section section-faq g-text-access">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Access Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best  Access learning experience  in Chicago. Please find answers to some of our most frequently asked Access training related questions below, or contact us should you require any further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5>What version of Access do you teach on?</h5>
                    <p>We are currently teaching our classes on Access 2019. Access 2019 is similar to Access 2016 and Access 2013, so the training is suitable for all 3 versions of Access.</p>
                </li>
                <li>
                    <h5>What times are your Access classes?</h5>
                    <p>All our classes run from 9.00am to 4.30pm. A lunch hour is taken  at 12.15pm.</p>
                </li>
                <li>
                    <h5>Where are your Access classes held?</h5>
                    <p>Our Chicago Access classes are held in our Chicago Training center located at 230 W Monroe Street, Suite 610, Chicago IL 60606. For more information about directions, parking, trains please
                        <a href="/contact-us-chicago.php">click here</a>.</p>
                </li>
                <li>
                    <h5>Is this face-to-face training or a webinar?</h5>
                    <p>Our Access classes are taught by live trainers present in the classroom. This is not a webinar. All our trainers are Access database experts and are passionate about teaching the software.</p>
                </li>
                <li>
                  <h5>Do I need to bring my own computer?</h5>
                    <p>No, we provide the computers at our training center in the Chicago Loop.</p>
                </li>
                <li>
                    <h5>Can you deliver Access training onsite at our location?</h5>
                    <p>Yes, we service the greater Chicago metro including Arlington Heights, Aurora, Bollingbrook, Chicago Loop, Deerfield, Des Plaines, Evanston, Highland Park, Joliet, Naperville, Schaumburg, Skokie, South Chicago and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver Access training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Access training.</p>
                </li>
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-access.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>