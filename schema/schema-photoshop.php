<!--SCHEMA EducationEvent - PHOTOSHOP Quickstart - CHICAGO-->

<?php function schema_educationEvent($name, $description, $location, $url, $image, $price) {

    return <<<HTML
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-01-15",
            "endDate": "2020-01-15",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-01-05T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-02-20",
            "endDate": "2020-02-20",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-02-20T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-03-25",
            "endDate": "2020-03-25",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-03-25T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-04-02",
            "endDate": "2020-04-02",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-04-02T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-04-02",
            "endDate": "2020-04-02",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-04-02T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            "name": "Photoshop Training Classes - Photoshop Quickstart",
            "url":  "/photoshop/advanced.php",
            "startDate": "2020-04-21",
            "endDate": "2020-04-21",
            "description": "Photoshop Quickstart Class",
            "image": "photoshop-quickstart.jpg",
            "performer": {
                "@type": "Organization",
                "name": "training Connection"
            },
            "location": {
                "@type": "Place",
                "name": "Chicago",
                "alternateName": "Training Connection - Chicago",
                "address": {
                    "@type": "PostalAddress",
                    "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
                }
            },
            "offers": {
                "@type": "Offer",
                "url":  "/photoshop/introduction.php",
                "validFrom":  "2020-04-21T14:50:08",
                "priceCurrency":  "USD",
                "availability":  "http://schema.org/InStock",
                "price":  "495.00"
            }
        }
    </script>
    HTML;

} ?>



<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-01-15",
        "endDate": "2020-01-15",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-01-05T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-02-20",
        "endDate": "2020-02-20",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-02-20T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-03-25",
        "endDate": "2020-03-25",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-03-25T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-02",
        "endDate": "2020-04-02",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-02T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-02",
        "endDate": "2020-04-02",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-02T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-21",
        "endDate": "2020-04-21",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Chicago",
            "alternateName": "Training Connection - Chicago",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-21T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>

<!--SCHEMA EducationEvent - PHOTOSHOP Quickstart - LA-->
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-01-15",
        "endDate": "2020-01-15",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"
            }

        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-01-05T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-02-20",
        "endDate": "2020-02-20",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {"@type": "PostalAddress","streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"}
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-02-20T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-03-25",
        "endDate": "2020-03-25",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-03-25T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-02",
        "endDate": "2020-04-02",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-02T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-02",
        "endDate": "2020-04-02",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-02T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "EducationEvent",
        "name": "Photoshop Training Classes - Photoshop Quickstart",
        "url":  "/photoshop/advanced.php",
        "startDate": "2020-04-21",
        "endDate": "2020-04-21",
        "description": "Photoshop Quickstart Class",
        "image": "photoshop-quickstart.jpg",
        "performer": {
            "@type": "Organization",
            "name": "training Connection"
        },
        "location": {
            "@type": "Place",
            "name": "Los Angeles",
            "alternateName": "Training Connection - Los Angeles",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"
            }
        },
        "offers": {
            "@type": "Offer",
            "url":  "/photoshop/introduction.php",
            "validFrom":  "2020-04-21T14:50:08",
            "priceCurrency":  "USD",
            "availability":  "http://schema.org/InStock",
            "price":  "495.00"
        }
    }
</script>