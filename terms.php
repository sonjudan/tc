<?php
$meta_title = "Booking Terms and Conditions | Training Connection";
$meta_description = "Booking terms for rescheduling and cancellation";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Booking Terms</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Our Terms</h1>
                </div>
            </div>
        </div>

        <div class="section-content-l2">
            <div class="container">
                <div class="card-default copy-sm">
                    <h4>Cancellation Policy | Public classes</h4>
                    <p>All cancellations and rescheduling requests must be made in writing and emailed to <a href="mailto:info@trainingconnection.com" target="_blank">info@trainingconnection.com</a>.</p>
                    <p>Cancellations made 7 days or less before the commencement of the training, will not be eligible for a refund but will receive a training voucher (equal to the amount paid less $100 admin fee), which can be used to book other classes. Such vouchers are valid for a period of 12 months from the date of issue.</p>
                    <p>Cancellations made more than 7 days before the commencement of the training, qualify for a refund less a $100 administration fee.</p>
                </div>
                <div class="card-default copy-sm">
                    <h4>Rescheduling Policy | Public classes</h4>
                    <p>Rescheduling requests made 7 days before the commencement of the training are free. Rescheduling that takes place within 7 days of the commencement of the training will incur a $100 administration fee. We do not charge for student substitutions at any time.</p>
                    <p>While we make every effort to deliver classes as scheduled, we reserve the right to reschedule previously confirmed classes for a variety of reasons including but not exclusively, the illness of the Trainer. In such cases, we will endeavor to inform the Client as soon as possible of the new training dates. We are not liable for any additional expenses incurred as a result of rescheduling training.</p>
                    <p>We are not able to offer refunds or training vouchers for no-shows or uncompleted courses.</p>
                </div>
                <div class="card-default copy-sm">
                    <h4>Private Group Training, Onsites and Room hire</h4>

                    <p>Private training, onsite training and room hire bookings can be rescheduled up until 7 days before the commencement of the training. Additional costs may be payable to cover  changes to flights/hotels etc. To reschedule please email us on <a href="mailto:info@trainingconnection.com" target="_blank">info@trainingconnection.com</a>.</p>
                    <p>We are unable to offer refunds for cancellations.</p>
                </div>
            </div>
        </div>



    </main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>