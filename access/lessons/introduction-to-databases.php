<?php
$meta_title = "An Introduction to Microsoft Access Databases | Training Connection";
$meta_description = "This lesson will introduce you to some important concepts needed to learn Microsoft Access. This course is part of our Level 1 access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Introduction to Databases</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Introduction to Access Databases</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Access is a relational database. A relational database is a collection of data items organized as a set of tables. In this lesson, you’ll get a chance to familiarize yourself with the basic components of the database. First, we’ll look at some common database terms. You’ll also take a closer look at the Navigation pane. We’ll introduce tables, table relationships, queries, forms, and reports. Finally, you’ll learn how to close database objects in Access.</p>
                <p>For instructor-led <a href="/access-training.php">Microsoft Access classes in Chicago and LA</a> call us on 888.815.0604.</p>

                <h4>Common Database Terms</h4>
                <p>Before you start learning more about Access procedures, let’s make sure that you have a solid grounding in database terminology. The following terms are commonly used in Access:</p>

                <ul>
                    <li><strong>File</strong> - A file is a collection of associated records.</li>
                    <li><strong>Record</strong> - All information (all fields/columns) for every item in a file is called a record (or each individual line).</li>
                    <li>
                        <p><strong>Field</strong> - A record is divided into separate headings/sections and each is known as a field - this could refer to each column/heading. There are different types of fields, including:</p>

                        <ul>
                            <li>NUMBER fields, which can be sorted in ascending or descending numeric order</li>
                            <li>Currency and Date/Time fields</li>
                            <li>TEXT fields, which can contain numbers and text that do not need to be sorted, such as telephone numbers</li>
                        </ul>
                    </li>
                    <li><strong>Data</strong> - Data is a collection of pieces of information.</li>
                    <li><strong>Database</strong> - A database is the organized collection of your data. The Access database can be sorted, queried, or amended at any time.</li>
                    <li><strong>Database object</strong> - An object is a container for the work you want Access to perform. It includes tables, macros, queries, forms, reports, and/or pages.</li>
                    <li><strong>Table</strong> - A table is a collection of data organized by categories called fields, into unique sets of data called records.</li>
                    <li><strong>Datasheet</strong> - A datasheet is a different way of looking at a table, form query, or stored procedure. It is displayed in rows and columns.</li>
                    <li><strong>Query</strong> - A query is a request you make of your data to extract only the information you want.</li>
                    <li><strong>Form</strong> - A form is a user-friendly interface used for entering or displaying data.</li>
                    <li><strong>Report</strong> - A report is similar to a form, but it only shows the information you want. It is also the end result of a query. You can print a report. </li>
                </ul>

                <h4>Using the Navigation Pane</h4>
                <p>The Navigation Pane provides a way to open all of the database objects that make up your database.</p>
                <p><a href="/access-training-los-angeles.php">Beginner Access training in Los Angeles</a>.</p>
                <p>To expand or collapse the Navigation pane, and how to expand or collapse each of the items listed in the pane. </p>
                <p>To open the Navigation pane, select the bar on the left of the screen, titled “Navigation Pane.”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/access-navigation-pane.jpg" width="553" height="312" alt="Access Navigation Pane"></p>

                <p>The Navigation Pane displays all of the objects included with your database, including tables, forms, reports, and queries. The sample database is organized with custom categories.</p>
                <ol>
                    <li>Select a Category header to expand it (or collapse it).</li>
                    <li>Double-click on an object to open it.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/access-table.jpg" width="196" height="391" alt="Access Table List"></p>

                <p>You can customize the Navigation Pane. </p>

                <ol start="1" type="1">
                    <li>Select the arrow in the Navigation Pane title bar.</li>
                </ol>
                <p>Access displays a list.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/access-navigation-pane-title-bar.jpg" width="437" height="354" alt="Navigate to Category - Filter by Group"></p>

                <ol start="2" type="1">
                    <li>Under the <strong>Navigate To Category</strong>, select the category that matches the objects you wish to view. You can also filter the contents of the Navigation Pane. Select an option under <strong>Filter By Group</strong> to show only those items.</li>
                </ol>
                <p><a href="/access-training-chicago.php">Classroom Access training in the Chicago Loop</a>.</p>

                <h4>Understanding Tables and Table Relationships</h4>
                <p>Tables make up the backbone of your database. Tables have relationships to connect data without having to store it in multiple places.</p>

                <h4>Tables</h4>
                <p>Tables are made up of columns and rows. Each column is a field. A field is a single piece of information. For example, a field could be Last Name, or Product description. Each field includes some type of information relevant to the table you have created.</p>

                <p>Each row is a record. A record is a meaningful and consistent way to combine information about something. In other words, the record includes all of the fields and their information about one unique item in the table. The same fields of information are stored for each record in the table. Each record includes a unique ID known as the primary key.</p>

                <h4>Table Relationships</h4>

                <p>One of the goals of good database design is to remove data redundancy or duplicate data. In order to achieve this goal, you can divide your data into many subject-based tables so that each fact is only represented once. Relationships are a way to bring the divided information back together. For example, in one table you could store customer information. The fields might include the customer number, first name, last name, address and so on. In another table, you might store order information. One of the pieces of information you may want to store about orders is the customer who ordered it. You can simply use the customer number, and then create a relationship between the two tables to access the rest of the customer information.</p>

                <p>To open a table, use the following procedure.</p>
                <ol>
                    <li>In the Navigation pane, select the arrow next to Northwind Traders.</li>
                    <li>Select <strong>Object</strong> <strong>Type</strong> from the drop down list.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/object-type.jpg" width="182" height="330" alt="Object Type from Dropdown"></p>

                <ol start="2" type="1">
                    <li>Select the double arrows to the right of <strong>Tables</strong>. Double-click any table to open it.</li>
                </ol>

                <p><span class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/access/table-list.jpg" width="717" height="404" alt="Open a table list in MS Access"></span></p>

                <p>Notice the icon next to the tables.</p>

                <h4>Closing Database Objects</h4>
                <p>To close database objects, use the following procedure.</p>

                <ol>
                    <li>Right-click on the tab for the database object you want to close.</li>
                    <li>Select <strong>Close</strong> from the context menu. You can also select <strong>Close All</strong> to close all open objects.</li>
                </ol>

                <p class="imgCenter"><span><img src="https://www.trainingconnection.com/images/Lessons/access/table-icon.jpg" width="750" height="356" alt="Closing Database objects in Microsoft Access"></span></p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Access Lessons</h4>
                    <ul>
                        <li><a href="/access/lessons/basic-tasks.php">Basic Access Database Tasks</a></li>
                        <li><a href="/access/lessons/introduction-to-databases.php">Formatting Text in Access</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>