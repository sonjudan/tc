<?php
$meta_title = "Formatting Text in Microsoft access | Training Connection";
$meta_description = "Learn to format text in Microsoft Access. This course is part of our Level 1 Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Formatting Text</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Formatting Text in Microsoft Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>There are several options for formatting your text in the Datasheet. To change the font face and size using the Ribbon tools, use the following procedure.</p>
                <ol>
                    <li>Select the text you want to change.</li>
                    <li>Select the arrow next to the current font name to display the list of available fonts.</li>
                    <li>Use the scroll bar or the down arrow to scroll down the list of fonts.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/formatting-text.jpg" width="750" height="392" alt="Formatting Text"></p>

                <ol start="4" type="1">
                    <li>Select the desired font to change the font of text.</li>
                    <li>With the text still selected, select the arrow next to the current font size to see a list of common font sizes.</li>
                    <li>Use the scroll bar or the down arrow key to scroll to the size you want and select it. You can also highlight the current font size and type in a new number to indicate the font size you want. </li>
                </ol>

                <p>For instructor-led <a href="/access-training.php">Access classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>To select a color for their fonts from the gallery, use the following procedure.</p>

                <ol>
                    <li>Select the text you want to change.</li>
                    <li>Select the arrow next to the Font Color tool on the Ribbon to display the gallery. Or select the same tool from the context menu (appears when you select text or by right-clicking).</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/font-colors.jpg" width="750" height="326" alt="Choosing a font color from the color picker"></p>

                <ol start="3">
                    <li>Select the color to change the font color.</li>
                </ol>

                <p>To review the Colors dialog box, use the following procedure.</p>

                <ol>
                    <li>Select the text you want to change.</li>
                    <li>Select the arrow next to the Font Color tool on the Ribbon to display the gallery. Or select the same tool from the context menu (appears when you select text or by right-clicking).</li>
                    <li>Select <strong>More Colors</strong> to open the Colors dialog box.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/standard-colors.jpg" width="272" height="293" alt="standard colors dialogue box"> In the <em>Standard Colors</em> dialog box, simply click on the color and select <strong>OK</strong> to use that color.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/custom-colors.jpg" width="272" height="293" alt="custom colors dialog box"> In the <em>Custom Colors</em> dialog box, you can click on the color, or you can enter the red, green, and blue values to get a precise color. When you have the color you want, select <strong>OK</strong>.</p>

                <p>Below are the tools used to add font enhancements.</p>

                <img src="https://www.trainingconnection.com/images/Lessons/access/font-enhancements.jpg" width="92" height="37" alt="font enhancements">

                <h4>Finding and Replacing Text</h4>
                <p>The Find and Replace feature is another one that you may have used in other applications. It’s a nice way to make global changes to your Datasheet. To find and replace one instance of an item at a time, use the following procedure.</p>
                <ol>
                    <li>Select <strong>Replace</strong> from the Find group on the <strong>Home</strong> tab of the Ribbon.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/access-ribbon.jpg" width="750" height="81" alt="Access Ribbon"></p>
                <ol start="2">
                    <li>In the <em>Find and Replace</em> dialog box, enter the exact text you want to find in the <strong>Find what</strong> field.</li>
                    <li>Enter the replacement text in the <strong>Replace with</strong> field.</li>
                </ol>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/access/find-replace.jpg" width="428" height="183" alt="Find and Replace"></h4>

                <ol start="4" type="1">
                    <li>Select an option from the <strong>Look In</strong> drop down list to indicate whether Access should look in the current field or the current document.</li>
                    <li>Select an option from the <strong>Match</strong> drop down list to indicate whether Access should look for any part of the field, the whole field, or the start of the field.</li>
                    <li>Select an option from the <strong>Search</strong> drop down list to indicate whether Access should search up, down, or all (the whole datasheet).</li>
                    <li>Check the <strong>Match Case</strong> box if applicable.</li>
                    <li>Check the <strong>Search Fields As Formatted</strong> box if applicable.</li>
                    <li>Select <strong>Find</strong> <strong>next</strong> to find the next instance of the item.</li>
                </ol>

                <p>Access highlights the record where it finds the search term. It also highlights the item from the Find and Replace dialog box.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/replace.jpg" width="750" height="342" alt="Replacing Text in Microsoft Access"></p>
                <ol start="10">
                    <li>When Access highlights the item, select <strong>Replace</strong> to delete the “find” item and paste the “replace” item.</li>
                    <li>Select <strong>Cancel</strong> when you have finished.

                    </li>
                </ol>
                <p>To Replace all instances of an item, use the following procedure.</p>
                <ol>
                    <li>Open the Find and Replace dialog box by selecting <strong>Replace</strong> from the Ribbon.</li>
                    <li>Enter the exact text you want to find in the <strong>Find what</strong> field.</li>
                    <li>Enter the replacement text in the <strong>Replace with</strong> field.</li>
                    <li>Select <strong>Replace All</strong>.</li>
                </ol>

                <img src="https://www.trainingconnection.com/images/Lessons/access/replace-all.jpg" width="428" height="183" alt="Replace all">

                <p>Access replaces all instances of the item in the current datasheet. Access displays a warning message.</p>
                <h4>
                    <img src="https://www.trainingconnection.com/images/Lessons/access/warning.jpg" width="285" height="123" alt="Warning Message">
                </h4>
                <ol start="5" type="1">
                    <li>Select <strong>Cancel</strong> when you have finished.</li>
                </ol>
            </div>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Access Lessons</h4>
                    <ul>
                        <li><a href="/access/lessons/introduction-to-databases.php">Introduction to Databases</a></li>
                        <li><a href="/access/lessons/basic-tasks.php">Basic Tasks in Access</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">Microsoft Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>