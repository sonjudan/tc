<?php
$meta_title = "Working with Records in Microsoft Access | Training Connection";
$meta_description = "Learn to Work with Records in Microsoft Access. This course is part of our Microsoft Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working With Records</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working With Records in Microsoft Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>For instructor delivered <a href="/access-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this Article, we will learn how to add, edit, search, browse, delete, and print records in a database.</p>

                <h4>Adding and Editing Records</h4>

                <p>To add records to a table, you have the option of entering them manually, using a form, or using the import tools on the External Data tab. As the database we are using does not have a form for it yet and we do not have external data we can import into it, we will add records manually.</p>
                <p>To add records to a table in Access manually, ensure that the table in question (Expenses) is open and that Datasheet View is applied:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/add-records-to-table.jpg" alt="Add Records in Access"></p>

                <p>Click inside the Employee ID field and type “1” (replacing the “0”). Press the Tab key to advance to the Expense Date field. Click the small calendar icon that appears beside this field:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/amend-data-field.jpg" alt="Make Changes to Datafield"></p>

                <p>From the small calendar that appears, click Monday of next week:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/calendar-selection.jpg" alt="Select From calendar"></p>

                <p>Press Tab again to advance to the next field. Type “34.39” and press Tab. In the Description field type “Printing paper:”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/tab-to-advance.jpg" alt="Use Tab Function to Advance"></p>

                <p>Repeat the previous steps to add the following information:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/repeat-to-add-info.jpg" alt="Repeat and Add Information"></p>

                <p>You may edit information using the same method: simply place your cursor inside the field you wish to edit and type.</p>
                <p>Save your database.</p>
                <p><a href="/access-training-chicago.php">Microsoft Access beginner and advanced training</a>.</p>

                <h4>Searching and Browsing Records</h4>

                <p>When viewing a table, you can browse records easily using the controls in the small toolbar at the bottom of Datasheet View:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/toolbar-view-controls.jpg" alt="Use Controls to View"></p>


                <p>Click the Next button to select the next record. Click the Previous buttonto select the previous record. The First button will select the first record in the table and the Last button will select the last record in the table.</p>
                <p>Using this toolbar, you can also search the records in the current table. Click inside the Search field and type “Flight.” As you type, Access will search the table in real time. Any records that match the keywords you entered will be selected. In this example, the second record will be selected:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/search-records-in-current-sheet.jpg" alt="Search Current Table for Records"></p>

                <h4>Deleting Records</h4>

                <p>To delete a record using Datasheet View, right-click the record header for the record in question and click Delete Record. Try using this method to delete the third record in the Expenses table:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/delete-record.jpg" alt="Delete Record Via Database View"></p>

                <p>A dialog will be displayed that asks you to confirm your choice to delete the selected record. Click Yes to continue:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/delete-confirmation.jpg" alt="Confirmation Box for Deletion"></p>

                <p>The record will then be deleted from the table. Save your database.</p>
                <p><a href="/access-training-los-angeles.php">LA classroom training in MS Access</a>.</p>

                <h4>Printing Records</h4>

                <p>To print specific records, first select the records you wish to print. For this exercise, select only the second record by clicking on the record row header:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/select-record-to-print.jpg" alt="Make Record Selection To Print"></p>

                <p>With the record selected, click File → Print → Print:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/record-file-print.jpg" alt="Select Record, File &amp; Print"></p>

                <p>When the Print dialog appears, ensure that the Selected Record(s) radio button is selected:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/selected-record-radio-button.jpg" alt="Ensure Correct Radio Button Pressed"></p>

                <p>Using this dialog, you can choose what printer you would like to print with, as well as how many copies of the item you would like printed. Click OK to print the selected record.</p>
                <p>Close Microsoft Access.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>We teach thousands of students in MS Applications. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">MS Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>