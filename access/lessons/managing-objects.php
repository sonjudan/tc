<?php
$meta_title = "Managing Objects in Microsoft Access | Training Connection";
$meta_description = "Learn to Manage Objects in Microsoft Access. This course is part of our Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Managing Objects</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Managing Objects in Microsoft Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For instructor driven <a href="/access-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this Article, we will learn how to manage objects in Microsoft Access</p>

                <p>There are a number of ways you can manage your objects using the Navigation Pane in Access 2013. All of these ways may be accessed by right-clicking on the object you would like to work with.</p>
                <p>For this example, right-click the Customers table:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/right-click-table.jpg" alt="Object Management With Nav Panel"></p>


                <p>(For this example, it does not matter if the Customers table is still open.) <a href="/access-training-chicago.php">Click here</a> for details on Chicago Access training.</p>
                <p>In the context menu, you will see options to delete, rename, cut, and copy the selected object. For this example, click Copy:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/format-object-options.jpg" alt="Object Formatting Options"></p>

                <p>With the Customers table now copied to your clipboard, you may then paste it elsewhere in this database, or even inside another database you currently have open. With any item selected inside the Navigation Pane, click Home → Paste:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/paste-object.jpg" alt="Paste Object Anyway In Database"></p>

                <p>The action will cause the Paste Table As dialog to open. Here, you may choose a new name for this table to paste, as well as choose from various paste options. For this example, leave the default settings and click OK:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/paste-table-dialog-box.jpg" alt="Paste Options From Dialog Box"></p>

                <p>The pasted table will now be listed within the Navigation Pane as a copy:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/paste-table-in-navigation.jpg" alt="Paste Table Appearance In Navigation"></p>

                <p>Right-click the “Copy Of Customers table” you just pasted and click Delete from the context menu:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/copy-of-table.jpg" alt="Copy and Delete From Context Menu"></p>

                <p>A dialog will then be displayed asking you to confirm the operation. Click Yes to complete the action:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/dialog-confirmation-box.jpg" alt="Confirmation Box to Complete Action"></p>

                <p><a href="/access-training-los-angeles.php">Small Access instructor-led classes in downtown LA</a>.</p>
                <p>The table will now be removed from the current database. Close Microsoft Access 2013 to complete this module.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>We train thousands of satisfied students in Microsoft Office Applications every year. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">MS Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>