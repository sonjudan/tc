<?php
$meta_title = "Basic Tasks in Microsoft access | Training Connection";
$meta_description = "Performing Basic Table Tasks in Microsoft Access. This course is part of our Level 1 Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Basic Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Performing Basic Table Tasks in Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>This lesson focuses on tables. You’ll learn how to enter and edit data in the Datasheet view of a table. Next, we’ll look at using the clipboard to cut, copy, and paste data to and from different fields. </p>
                <p>For instructor-led <a href="/access-training.php">MS Access classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Entering and Editing Data </h4>
                <p>In this lesson, we’ll take a look at entering and editing data, which works much the same as it did in the app.</p>
                <p>To enter data using a variety of data types, use the following procedure.</p>
                <p>In the example table, the first field is a multi-answer lookup column. The options appear as checkboxes. </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/entering-data.jpg" width="433" height="484" alt="Entering data into Microsoft Access"></p>

                <ol>
                    <li>Check one or more boxes to enter the data for this field. Select <strong>OK</strong>.</li>
                    <li>Press <strong>Enter</strong> or <strong>Tab</strong> to move to the next field.</li>
                </ol>

                <p>In a new record, the Primary Key field (in this example, labeled ID) says New at first, until you start entering information. Then Access enters the next number.</p>
                <ol start="3" type="1">
                    <li>
                        <p>The following fields are TEXT fields. Enter text to complete the information.</p>
                        <ol start="1" type="a">
                            <li>Product Code</li>
                            <li>Product Name</li>
                            <li>Description</li>
                            <li>Quantity per Unit</li>
                            <li>Minimum Reorder Quantity</li>
                        </ol>
                    </li>
                    <li>
                        <p>The following fields are CURRENCY fields. Enter a number, with a decimal for cents, if applicable.</p>
                        <ol start="1" type="a">
                            <li>Standard Cost</li>
                            <li>List Price</li>
                        </ol>
                    </li>
                    <li>
                        <p>The following fields are NUMBER fields. Enter a number.</p>
                        <ol start="1" type="a">
                            <li>Reorder Level</li>
                            <li>Target Level</li>
                        </ol>
                    </li>
                    <li>The Discontinue field is a YES/NO field. Check the box, if applicable.</li>
                    <li>The Category field is a Lookup column (drop down list). Select an item from the list.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/lookup-column.jpg" width="643" height="335" alt="Lookup column"></p>

                <p>The final field allows attachments.</p>
                <p>To edit a record. Let’s imagine that one of the Northwind customers got a promotion, with a new title and contact information. This example uses the Customers table from the sample database.</p>

                <ol>
                    <li>Highlight the information you want to change.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/editing-records.jpg" width="625" height="252" alt="editing records in MS Access"></p>

                <ol start="2" type="1">
                    <li>Enter the new information. After you have entered the new information, close the table. There is no need to save - Microsoft Access saves the new information in the table automatically.</li>
                </ol>

                <h4>Using the Clipboard </h4>
                <p>Tables make up the backbone of your database. Tables have relationships to connect data without having to store it in multiple places.</p>
                <p>Looking for an <a href="/access-training-chicago.php">Access training class in Chicago</a>?</p>
                <p>
                    Just as with other applications, you can use the Clipboard to cut, copy, and paste information from one place to another. We’ll practice these tasks in Datasheet view.
                    To cut and paste text, use the following procedure.
                </p>
                <ol>
                    <li>Highlight the text you want to cut.</li>
                    <li>Select <strong>Cut</strong> from the <strong>Home</strong> tab on the Ribbon.

                    </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/table-tools.jpg" width="780" height="330" alt="Using the Access clipboard">
                </p>
                <ol start="3" type="1">
                    <li>Move the cursor to the new location.</li>
                    <li>Right click the mouse and select <strong>Paste</strong> from the context menu. </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/paste-to-clipboard.jpg" width="545" height="233" alt="Paste to clipboard"></p>

                <p>To copy and paste text using the keyboard shortcuts, use the following procedure.</p>

                <ol>
                    <li>Highlight the text you want to cut and press the Control key and the C key at the same time.</li>
                    <li>Move the cursor to the new location.</li>
                    <li>Press the Control key and the V key at the same time.</li>
                </ol>

                <p>To open the Clipboard Task pane, use the following procedure.</p>
                <ol>
                    <li>On the Home tab of the Ribbon, select the icon next to Clipboard.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/Clipboard Task pane.jpg" width="197" height="150" alt="Clipboard Task Pane"> </p>
                <p>The Clipboard pane opens, displaying any items you have cut or copied in this Word 2013 session (or the 24 most recent). A sample is illustrated below. </p>
                <p>For <a href="/access-training-los-angeles.php">more information about hands-on Access training in Los Angeles</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/clipboard.jpg" width="170" height="493" alt="Access Clipboard"> </p>

                <p>To paste using the Office Clipboard Task pane, use the following procedure.</p>

                <ol>
                    <li>Place the cursor where you want to paste text from the clipboard.</li>
                    <li>Click on the item in the Clipboard task pane that you want to paste. </li>
                </ol>

                <p>You can copy and paste data from another program like Excel or Word into an Access table. This works best if the data is separated into columns. If the data is in a word processing program, such as Word, either use tags to separate the columns or convert the columns into a table format before copying. </p>
                <ol>
                    <li>If the data needs editing, such as separating full names into first and last names, do that first in the source program.</li>
                    <li>Open the source and copy (Ctrl + C) the data.</li>
                    <li>Open the Access table where you want to add the data in Datasheet view and paste it (Ctrl + V).</li>
                    <li>Double-click each column heading and type a meaningful name.</li>
                    <li>Click <strong>File</strong> &gt; <strong>Save</strong> and give your new table a name.</li>
                </ol>

                <p>Note that Access sets the data type of each field based on the information you paste into the first row of each column, so make sure that the information in the following rows match the first row.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Access Lessons</h4>
                    <ul>
                        <li><a href="/access/lessons/introduction-to-databases.php">Introduction to Databases</a></li>
                        <li><a href="/access/lessons/formatting-text.php">Formatting Text</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">Access training reviews page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>