<?php
$meta_title = "Formatting Tables in Microsoft Access Part A | Training Connection";
$meta_description = "Learn to format tables in Microsoft Access. This course is part of our Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Formatting Tables Part A</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Formatting Tables in Microsoft Access Part A</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For instructor-led <a href=/access-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this Article, we will learn how to:</p>
                <ul>
                    <li>Select data</li>
                    <li>Change the height and width of rows and columns</li>
                </ul>

                <h4>Selecting Data</h4>

                <p>You may select data in a table in a number of different ways. To select an entire table of data, click the Table Selector button in the upper left-hand corner of Datasheet View:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/table-selector-button.jpg" alt="Locate Table Selector Button"></p>

                <p>To select an entire record, click the gray box to the left of the record you would like to select. Your cursor will change to a right-facing arrow when it is over this area:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/arrow-change.jpg" alt="Right Facing Arrow Change"></p>

                <p>To select entire columns of data, click the gray column header buttons. For this example, click to select the Employee ID header. Your cursor will change to a downward-facing arrow when it is over this area:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/cursor-change.jpg" alt="Cursor Change To Downward Arrow"></p>

                <p>To select an individual cell, place your cursor close to any border of the cell in question until you see your cursor change to a cross (). Then, click to select that cell. For this example, select Employee ID “2:”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/cursor-change-to-cross.jpg" alt="Cursor Change To Cross Icon"></p>

                <p>You also have the option of selecting a range of adjacent cells. For this example, click to select the Employee ID field where the value is “2.” With the mouse button still held down, drag your mouse down to the Description field with the value “Car rental:”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/range-of-adjacent-cells.jpg" alt="Select Range Of Adjacent Cells"></p>

                <p>Once you have selected any combination of rows, cells, or columns, you can then copy and cut them as needed.</p>
                <p><a href="/access-training-los-angeles.php">LA Microsoft Access training classes</a> offered in Downtown LA.</p>

                <h4>Changing Column and Row Width and Height</h4>

                <p>You may have noticed that when you add data to a field, it can appear cut off due to the dimensions of the row or column. For example, in the sample table, you can see that the Employee ID and Expense Date field titles are cut off:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/data-field-cutoff.jpg" alt="Data Field Dimensions"></p>

                <p>To adjust the field width, right-click the Expense Date field header and click Field Width from the context menu:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/adjust-field-width.jpg" alt="Data Field Adjustment"></p>

                <p>The Column Width dialog will then be displayed. Type a new value into the Column Width field. For this example, type “15.” Click OK:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/column-width-dialog.jpg" alt="Column Width Dialog Display"></p>

                <p>The Expense Date field width will have increased to accommodate the field title:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/expand-data-field.jpg" alt="Expand Data Field Width"></p>

                <p>To adjust the row height, you follow basically the same procedure. However, you cannot adjust row height individually: any new values will be applied to all rows in the table. Right-click the row header for any record in the table and click Row Height:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/adjust-row-height.jpg" alt="Click Row Height To Adjust"></p>

                <p>The Row Height dialog will then be displayed. Type a new value into the Row Height field. For this example, type “20.” Click OK to apply the new height:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/row-height-dialog.jpg" alt="Row Height Dialog Box"></p>

                <p>The row height will now have increased:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/row-height-increase.jpg" alt="Row Height Has Increased"></p>

                <p>Save your changes.</p>
                <p>For more on <a href="/access-training-chicago.php">face-to-face instructor-led Access classes</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students in a variety of Microsoft Office Applications. Read a sample of testimonials at our <a href=/testimonials.php?course_id=18">Microsoft Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>