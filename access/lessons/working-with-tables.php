<?php
$meta_title = "Working with Tables in Microsoft Access | Training Connection";
$meta_description = "Learn to Work with Tables in Microsoft Access. This course is part of our MS Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working With Tables</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working With Tables in Microsoft Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For instructor delivered <a href="/access-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this Article, we will learn how to:</p>
                <ul>
                    <li>Create a table</li>
                    <li>Use table views</li>
                </ul>

                <h4>Creating a Table</h4>
                <p>To create a table, click Create → Table:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/create-new-table.jpg" alt="Creating A New Table"></p>
                <p>A new table will then be created with the default ID field:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/new-table-default-id.jpg" alt="Default Table and ID Created"></p>
                <p>Press Ctrl + S to save this new table. When the Save As dialog appears, type “Expenses” as a table name and click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/save-as-dialog.jpg" alt="Save As Dialog Box"></p>
                <p>The new table will now be saved within the database using the table name you selected. <a href="/access-training-los-angeles.php">More info</a> on LA classes.</p>
                <h4>Understanding Table Views</h4>
                <p>Now that you have created a table, you need to add fields to it. Access 2013 includes Design View to help you add fields easily. To apply Design View, click Home → View → Design View:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/add-fields-with-design-view-2.jpg" alt="Add Fields Via Design View"></p>
                <p>In the first field, type “Expense ID:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/name-first-field.jpg" alt="Type Name In First Field"></p>
                <p>Press Tab. This will advance the cursor to the Data Type combo box. Leave AutoNumber as the default data type. Press Tab once more to advance to the Description Field. Type “Unique ID for each expense claimed to the company.”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/data-type-combo-box.jpg" alt="Use Tab Function to Edit Data"></p>

                <p>Use the methods detailed above to enter the following information into the table. Use Tab to move quickly between the fields:</p><p><img src="https://www.trainingconnection.com/images/Lessons/access/use-tab-for-quick-movement.jpg" alt="Press Tab For Fast Cell Movement"></p>
                <p>When you are done, switch back to Datasheet View by clicking Home → View → Datasheet View:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/back-to-data-view.jpg" alt="Switch Back To Data View"></p>
                <p>A dialog will appear asking if you want to save the table. Click Yes:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/save-table-dialog-box.jpg" alt="Click Yes On Save Dialog Box"></p>
                <p><a href="/access-training-chicago.php">Chicago Microsoft classes</a>.</p>
                <p>With Datasheet View applied, you can now see the new fields you entered using Design View:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/new-fields-in-design-view.jpg" alt="Use Design View to See New Fields"></p>
                <p>Save your database.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>We teach thousands of students in MS Applications every year. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">MS Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>