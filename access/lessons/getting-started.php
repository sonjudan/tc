<?php
$meta_title = "Getting Started in Microsoft access | Training Connection";
$meta_description = "This Lesson will cover how to get started in Access 2013. This lesson is part of our Level 1 Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Getting started</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Getting Started in Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>This lesson is for students new to Access 2013, and focuses on:</p>
                <ul>
                    <li>Get started with Access 2013</li>
                    <li>Sign into a Microsoft account</li>
                    <li>Create, save, and open a database</li>
                    <li>Define common database terms</li>
                </ul>
                <p>For	<a href="/access-training.php">Chicago and Los Angeles based Access training classes</a> call us on 888.815.0604.</p>
                <h4>Getting Started</h4>
                <p>To open Microsoft Access 2013, click the Access 2013 tile on your Start screen:</p>
                <p></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/start-screen.jpg" width="780" height="438" alt="Start screen in Access"></p>
                <p>(If you are using Windows 7 or Windows Vista, click Start -&gt; All Programs -&gt; Microsoft Office 2013 -&gt; Access 2013.) <a href="/access-training-los-angeles.php">Small Access classes in Los Angele</a>s.</p>
                <p>After a moment, Access 2013 will open on your desktop. Click the type of database you would like to create. For this example, scroll down and click "Desktop issue tracking."</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/new-database.jpg" width="780" height="408" alt="New database screen"></p>
                <p>Review the information inside the pop-up dialog that appears. Note the File Name field where you can give your new database a name. Click Create to complete the process:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/database-file-name.jpg" width="780" height="408" alt="Give your new database a file name"></p>
                <p>You can now work with your new database:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/access-new-database-screen.jpg" width="780" height="408" alt="Access new database screen"></p>
                <p>When you are ready, close Access by clicking the X button in the top right-hand corner of the Access window:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/closing-access.jpg" width="780" height="87" alt="Closing Access"></p>
                <h4>Signing In</h4>
                <p>To begin, open Access 2013 and create a new database using the "Blank desktop database" template by following the steps from the previous topic. Click Enable Content if a security warning is displayed.</p>
                <p>More information in <a href="/access-training-chicago.php">Chicago-based Access training</a>.</p>
                <p>If you are using Windows 8, and your user account is a Microsoft account, Microsoft Office will automatically sign you in when you open Access:</p>
                <p></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/switch-user.jpg" width="400" height="450" alt="Access Sign-in"></p>
                <p>If you are not signed in, you will see a “Sign in” link:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/click-sign-in.jpg" width="300" height="198" alt="click to log-in"></p>
                <p>With either action, you will be prompted to enter the e-mail address of the account you would like to use. Click Sign In when you are ready:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/sign-in-pop-up.jpg" width="580" height="711" alt="Enter email">Next, enter your password and click Sign In:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/password-enter.jpg" width="580" height="712" alt="Enter password"></p>
                <p>After a moment, you will be signed in using the e-mail account you entered.</p>
                <h4>Creating a Database</h4>
                <p>To begin, open Access 2013 and create a new database using the "Blank desktop database" template. (You can also use the database that you created in the last topic.) Click Enable Content if a security warning is displayed.</p>
                <p>To create a completely new database while in Access, click File -&gt; New:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/file-new.jpg" width="780" height="250" alt="Create a new database"></p>
                <p>Next, choose the template you would like to use to create your new database. For this example, click the "Blank desktop database" option:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/blank-new-database.jpg" width="780" height="408" alt="Blank desktop database"></p>
                <p>A new database, using the template you selected, will now be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/database-template.jpg" width="780" height="408" alt="New database screen"></p>

                <p>Close Microsoft Access 2013.</p>
            </div>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">Access testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>