<?php
$meta_title = "Opening Database Objects in Microsoft Access | Training Connection";
$meta_description = "Learn to Open Database Objects in Microsoft Access. This course is part of our Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Opening Database Objects</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Opening Database Objects in Microsoft Access</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For instructor driven <a href="/access-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this Article, we will learn how to open database objects in Microsoft Access</p>

                <p>To open any objects that are listed inside the Navigation Pane, double-click on their listing. For this example, find and double-click the Employees table:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/access/open-object-from-nav-panel.jpg" alt="Open Object from Navigation Pane"></p>

                <p>The table will open, allowing you to view and work with its data:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/open-table.jpg" alt="Table Open to View and Edit"></p>

                <p>Close this table by right-clicking on its tab (Employees) and choosing Close from the menu:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/right-click-to-close-table.jpg" alt="Close Table with Right Click"></p>

                <h4>Changing the Object View</h4>

                <p>Depending upon the type of object you are currently working with, there are different ways to view it and its data. To change the view of a particular object, open it, click Home → View, and click on the view you want to apply.</p>
                <p>For this example, open the Customers table and click Home → View → Design View:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/change-object-view.jpg" alt="Change to Design View"></p>

                <p>(If you are prompted to open the table as read-only, click Yes.) Design View will now be applied:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/design-view-applied.jpg" alt="Read Only Design View"></p>

                <p>Design View lists all of the table’s fields, along with their data types and descriptions. However, you will not see the actual data records using this view. As the name implies, this view is better suited to designing your table structure than Datasheet View.</p>
                <p><a href="/access-training-los-angeles.php">MS Access classes</a> in Downtown Los Angeles.</p>
                <p>Reapply Datasheet View to this table by clicking Home → View → Datasheet View:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/reapply-datasheet-view.jpg" alt="Datasheet View Reapplied"></p>

                <h4>Understanding the Types of Views Available</h4>
                <p>Views differ upon the type of object you currently have open. Below is a breakdown of the possible view options you have available by each object type.</p>
                <h4>Table</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/views-available.jpg" alt="List of Views Available"></p>


                <ul>
                    <li><strong><span>Datasheet</span></strong> <strong><span>View</span></strong> displays all data in a table in a columnar view.</li>
                    <li><strong><span>Design</span></strong> <strong><span>View</span></strong> lets you modify the properties of a table to make it contain and display the data you need.</li>
                </ul>

                <h4>Form</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/forms-available.jpg" alt="List of Froms Available"></p>

                <ul>
                    <li><strong><span>Datasheet</span></strong> <strong><span>View</span></strong> is a way of showing you the table that the form references.</li>
                    <li><strong><span>Design</span></strong> <strong><span>View</span></strong> lets you modify the look and feel of a form as well as add different controls to perform actions.</li>
                </ul>
                <p><a href="/access-training-chicago.php">Chicago-based MS Access training classes</a>.</p>

                <h4>Report</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/reports-available.jpg" alt="List of Reports Available"></p>

                <ul>
                    <li><strong><span>Report</span></strong> <strong><span>View</span></strong> displays the contents of the report in a manner suitable for printing or presenting.</li>
                    <li><strong><span>Print Preview</span></strong> will show you what the report will look like if it was printed on paper using the current paper settings.</li>
                    <li><strong><span>Layout</span></strong> <strong><span>View</span></strong> is an intermediate step between Report View and Design View. It lets you adjust the location of objects in a report while still being able to see the data it contains.</li>
                    <li><strong><span>Design</span></strong> <strong><span>View</span></strong> lets you modify the look and feel of a report as well as add different controls to display data or perform actions.</li>
                </ul>

                <h4>Query</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/queries-available.jpg" alt="List of Queries Available"></p>


                <ul>
                    <li><strong><span>Datasheet</span></strong> <strong><span>View</span></strong> displays the results of a query in a view similar to a table.</li>
                    <li><strong><span>SQL</span></strong> (Structured Query Language) <strong><span>View</span></strong> is a way of viewing and modifying the SQL code used to make a query. (Note: SQL editing is beyond the scope of this manual.)</li>
                    <li><strong><span>Design</span></strong> <strong><span>View</span></strong> lets you add and remove fields from the search as well as add criteria to retrieve more specific results.</li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>We train thousands of satisfied students in MS Office Applications every year. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">MS Access training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>