<?php
$meta_title = "Access Database Relationships | Training Connection";
$meta_description = "The Lesson will cover the different ways tables are related to each other in Microsoft Access. This lesson is part of our Level 1 Access Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/access.php">Access</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Relationships</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Access">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Access Database Relationships</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Relationships define a database. The power of a database lies in the way the tables are related to each other. Nearly every database that contains multiple tables also includes relationships between these different sets of information. There are three types of table relationships: one-to-many, many-to-many, and one-to-one. Let's learn about each type. </p>
                <p>For <a href="/access-training.php">Microsoft Access classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>One-to-Many</h4>
                <p>Consider the following database, which contains two tables:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/access/two-tables.jpg" width="780" height="306" alt="2 Tables"></p>

                <p>This database has a table of Employees and a table of Expenses. As time goes by, the Expenses table could grow to become quite large. </p>

                <p>Now imagine that you want to find out the phone number of every employee that submitted an expense. It is impractical to place the employee phone number in the same table as the expenses because it will create a lot of unnecessary and duplicate data. This is particularly true if there is another table containing employee data elsewhere. </p>
                <p>For more on  <a href="/access-training-los-angeles.php">MS Access classes in Los Angeles</a>.</p>

                <p>Therefore, a relationship between the two tables can be created between the two common fields: Employee ID. The Employees table and the Expenses table are then in a <strong>one-to-many</strong> relationship, meaning that one entry in the Employees table can relate to many entries in the Expenses table. In other words, one employee can log many expenses.</p>

                <h4>Many-to-Many</h4>
                <p>Consider a database that contains two separate tables: Orders and Products. While a single order can include more than one product, a single product can appear on many orders. Therefore, for each record in the Orders table, there can be many records in the Products table. In addition, for each record in the Products table, there can be many records in the Orders table. This type of relationship is called a many-to-many relationship because, for any product, there can be many orders and, for any order, there can be many products.</p>
                <p>
                    In order to represent a many-to-many relationship, a third table will need to be created. Such tables are often called <strong>junction tables</strong>, and they break down the many-to-many relationship into two separate one-to-many relationships. In our example, the Orders table and the Products table have a many-to-many relationship that is defined by creating two one-to-many relationships to the Order Details table. One order can have many products, and each product can appear on many orders.</p>

                <h4>One-to-One </h4>
                <p>A one-to-one relationship requires each record in one table to have only one matching record in a second. Due to these constraints, one-to-one relationships are relatively rare, as information that is related in this way is most often stored in the same table. Most often, such relationships are used to divide and isolate parts of a table for security purposes. </p>
                <p>Looking for <a href="/access-training-chicago.php">Access training in Chicago</a>?</p>

                <h4>Summary</h4>
                <p>When designing a database, relationships are frequently the most challenging step and often the place where most of the confusion with databases arises. However, as with most things, proper planning can make a world of difference.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-access" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Access student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=18">Access training testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Access courses</h4>
                    <ul>
                        <li><a href="/access/introduction.php">Access Level 1 - Introduction</a></li>
                        <li><a href="/access/intermediate.php">Access Level 2 - Intermediate</a></li>
                        <li><a href="/access/advanced.php">Access Level 3 - Advanced</a></li>
                        <li><a href="/access/vba.php">Access Level 4 - Macros and VBA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>