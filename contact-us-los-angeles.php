<?php
$meta_title = "Contact Us | Training Connection | Los Angeles";
$meta_description = "Contact details for our Los Angeles Training Center";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>


    <main class="page-single-content">

        <div class="room-hire-map">

            <div class="embed-responsive embed-responsive-21by9 sm">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13222.883976755706!2d-118.2600668!3d34.0510274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5831c6d11713747a!2sTraining+Connection!5e0!3m2!1sen!2snl!4v1560364559799!5m2!1sen!2snl" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>


        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                </ol>
            </nav>

            <div class="page-intro room-hire-heading mt-0">
                <div class="heading-l4">
                    <h2>Los Angeles</h2>
                    <h4>Training Center</h4>
                </div>
                <ul class="room-hire-cdetails">
                    <li><i class="fas fa-map-marker-alt"></i>
                        <a href="https://www.google.com/maps?ll=34.051027,-118.260067&z=14&t=m&hl=en&gl=NL&mapclient=embed&cid=6355079150534095994" target="_blank">
                            915 Wilshire Blvd, Suite 1800, Los Angeles, CA 90017
                        </a>
                    </li>
                    <li><i class="fas fa-phone"></i>
                        <a href="tel:8888150604"><span class="link-secondary">888) 815.0604</span></a>
                    </li>
                    <li><i class="fas fa-fax"></i> (866) 523-2138</li>
                    <li><i class="fas fa-envelope-square"></i>
                        <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                            Show Email
                        </a>
                    </li>
                </ul>
            </div>



            <div class="room-hire-body copy-sm">
                <div class="room-hire-col">

                    <div class="rh-block">
                        <h4><i class="fas fa-directions"></i> Directions</h4>
                        <p>The easiest access is from the 110 (Harbor) Freeway</p>

                        <div class="card-l2 lg">
                            <p>
                                If traveling North-bound on <span>110N Fwy</span><br>
                                Take the <span>9th St exit</span><br>
                                Turn <span>Left</span> at <span>Figueroa Street</span><br>
                                Turn <span>Left</span> into <span>Wilshire Blvd</span>
                            </p>
                            <p>
                                <span>915 Wilshire</span> is on the <span>Right</span> (look for the <span>Bank of the West</span> located in our lobby)
                            </p>
                        </div>


                        <div class="card-l2 lg">
                            <p>If traveling South-bound on <span>110N Fwy</span>
                                Take the <span>4th St exit</span> towards <span>WILSHIRE blvd / 6TH St / 3RD St</span><br>
                                Turn <span>left</span> at <span>Wilshire Blvd</span>
                            </p>

                            <p>
                                <span>915 Wilshire</span> is on the <span>Left</span> (look for the <span>Bank of the West</span> located in our lobby)
                            </p>

                            <p>The entrance to our parking garage is at the light of Franciso St.</p>
                        </div>

                    </div>
                </div>


                <div class="room-hire-col">
                    <div class="rh-block">
                        <h4><i class="fas fa-subway"></i> Trains</h4>

                        <div class="copy">
                            <p><img src="/dist/images/hire-train/train.jpg" alt="Trains"></p>
                            <p>We are easily accessable by train via the Metro Red, Purple, and Blue lines. The nearest train station is the 7th Street / Metro Center Station. Also available are the Metrolink commuter trains. Take Metrolink to Los Angeles Union Station and transfer for free to the Metro Red or Purple line.</p>
                            <p>Visit LA Metro and Metrolink websites for schedule and fare information. Please view the Google Map above for directions from the 7th Street / Metro Center Station to our facility.</p>
                            <p>The nearest train station is the 7th Street / Metro Center Station. Also available are the Metrolink commuter trains. Take Metrolink to Los Angeles Union Station and transfer for free to the Metro Red or Purple line.</p>
                        </div>

                    </div>
                </div>

            </div>

            <div class="rh-block copy copy-sm">
                <h4><i class="fas fa-parking"></i> Parking</h4>
                <p>Parking is available at our location for $35/day.</p>

                <h5 class="h5 mt-4">Alternative Parking</h5>

                <div class="row row-sm">
                    <div class="col-lg-6 col-xl-4">
                        <div class="card-l2 lg">
                            <p>
                                <strong>616 S Figueroa</strong> <br>
                                Big sign out front says 811 Wilshire Northeast corner of Figueroa and Wilshire <br>
                                Indoor lot <br>
                                Early Bird rate before 8:30am - $15, $30 after
                                2 minute walk
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-4">
                        <div class="card-l2 lg">
                            <p>
                                <strong>1055 W 7th St</strong> <br>
                                Entrance also at 690 Bixel St On the Northeast corner of 7th and Bixel <br>
                                Indoor Lot <br>
                                Early Bird rate before 9:00am - $10, $35 after
                                4 minute walk
                            </p>
                        </div>
                    </div>
                </div>

            </div>



            <div class="section-footer mt-5 copy-sm">
                <p><i class="fas fa-wheelchair"></i> This center is wheel  chair friendly. For more details please call us.</p>
            </div>
        </div>

    </main>

    <div class="mt-5 pt-2"></div>




<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>