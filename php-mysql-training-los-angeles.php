<?php
$meta_title = "PHP & MySQL Training Classes | Los Angeles| Training Connection";
$meta_description = "Need to learn PHP & MySQL? Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">PHP & MySQL Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-web" style="background-image: url('/dist/images/banner-php.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">PHP & MySQL Training</h1>

                    <div data-aos="fade-up">
                        <h4>Los Angeles, Los Angeles</h4>
                        <p>Looking for a PHP training class in Los Angeles?</p>
                        <p>We offer hands-on PHP classes taught by experienced Web developers, who are present in the classroom.</p>
                        <p>PHP is the Web's most popular server-side scripting language. It is fast, stable, feature-rich and free to use.</p>
                        <p>PHP can help bring your website to life, changing it from a static HTML experience to a two-way interactive user experience.</p>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">

                <div class="mb-4 mt-md-4 float-md-right"  data-aos="fade-up">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>

                <h3>What's Included</h3>
                <ul>
                    <li>PHP training manual</li>
                    <li>Certificate of Course Completion</li>
                    <li>Free repeat valid for 6 months (in case you need a refresher)</li>
                </ul>
                <p>
                    <strong>Book an PHP & MySQL training course today. All classes guaranteed to run!</strong> <br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes border-bottom pb-0" data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0"  data-aos="fade-up">
                <h2>PHP & MySQL Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-php" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-php.png" alt="Understanding the PHP & MySQL Interface"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    PHP & MySQL Web Development
                                </a>
                            </h3>
                            <p>This JavaScript class is suitable for beginners. You will learn how to create better user experiences by adding interactive elements for your Web pages. This class will introduce you to JavaScript syntax, before delving into jQuery the World's most popular JavaScript library.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="PHP & MySQL Web Development" data-price="$1495"
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book this course
                                </a>
                                <a href="/downloads/php/PHP%20and%20MySQL%20Web%20Development.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="PHP & MySQL Web Development">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1495
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Basic PHP Syntax</h5>
                                        <p>This exercise gets you started with the basics of PHP syntax. If you don't have previous programming experience, you will find that PHP is pretty simple to learn.</p>
                                        <ul>
                                            <li>Echo, Strings, and Variables</li>
                                            <li>Single Quotes vs. Double Quotes</li>
                                            <li>Escaping Characters</li>
                                            <li>Heredoc</li>
                                            <li>Concatenation</li>
                                            <li>Comments</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Numbers</h5>
                                        <p>In this exercise, we will show you how PHP works with numbers.</p>
                                        <ul>
                                            <li>Arithmetic Operators</li>
                                            <li>Assignment Operators</li>
                                            <li>Table of Arithmetic Operators</li>
                                            <li>Table of Assignment Operators</li>
                                            <li>Defining a Site and Testing Server in Dreamweaver</li>\
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Conditionals</h5>
                                        <p>Conditional operators will be one of the most-used elements of your programming life. Simply put, conditional operators are a way to choose when certain things happen. For example, if something is true, then do something. If it is not true, do something else.</p>
                                        <ul>
                                            <li>If/Else</li>
                                            <li>Elseif</li>
                                            <li>Switch</li>
                                            <li>Comparison Operators</li>
                                            <li>Logical Operators</li>
                                            <li>The Difference Between == and ===</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Arrays</h5>
                                        <p>One of the most common and powerful types of variables is called an array. There are many ways you can create, output, and manipulate arrays, but here we will just focus on some of the most commonly-used basics.</p>
                                        <ul>
                                            <li>Creating a simple array</li>
                                            <li>Using array()</li>
                                            <li>Multidimensional Arrays</li>
                                            <li>Printing an Entire Array Using print_r()</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Loops</h5>
                                        <p>Loops are an incredibly important and often-used element of your programming tool belt. Here we will explore the many kinds of loops PHP has to offer.</p>
                                        <ul>
                                            <li>While Loops</li>
                                            <li>Do...While</li>
                                            <li>For Loops</li>
                                            <li>ForEach</li>
                                            <li>Break Out of Loop</li>
                                            <li>Continue</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Strings</h5>
                                        <p>Let’s explore some basic string functions.</p>
                                        <ul>
                                            <li>Comparing Strings</li>
                                            <li>Converting to Upper and Lower Case</li>
                                            <li>Searching Through Strings</li>
                                            <li>Case-Sensitive and Case-Insensitive</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Functions &amp; Objects</h5>
                                        <p>In this exercise, you will learn the basics of functions and how to use arguments within them. We will go over how to create objects with properties and methods. We will explore the differences between public and private properties, and  how to extend classes' functionality.</p>
                                        <ul>
                                            <li>Functions</li>
                                            <li>Arguments</li>
                                            <li>Objects and Properties</li>
                                            <li>Objects and Methods</li>
                                            <li>Private Properties</li>
                                            <li>Creating Classes that Extend Classes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Form Basics and Security</h5>
                                        <p>In this exercise, we will cover the basics of PHP form submission. You will learn the difference between POST and GET, how to deal with radio buttons, checkboxes, and select fields, and how to secure your pages from attacks.</p>
                                        <ul>
                                            <li>Setting up a basic form</li>
                                            <li>Post vs. Get</li>
                                            <li>Radios, Checkboxes, and Select Fields</li>
                                            <li>Magic Quotes</li>
                                            <li>Securing the page</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sending Email</h5>
                                        <p>One of the great things about PHP is how easy it is to send an email. In this exercise we will show you how to set up MAMP Pro or XAMPP to send test emails. </p>
                                        <ul>
                                            <li> Setting Up MAMP Pro</li>
                                            <li>Setting Up XAMPP and Mercury Mail</li>
                                            <li>Sending a Test Email</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Simple Form Validation and Email</h5>
                                        <p>This exercise will focus on a very simple form validation script that checks for simple errors, sanitizes input, and sends an email. </p>
                                        <ul>
                                            <li>Sanitizing Input</li>
                                            <li>Error Checking</li>
                                            <li>Displaying Errors</li>
                                            <li>Sending Email</li>
                                            <li>Adding a Thank You Page</li>
                                            <li>Including Files</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Cookies</h5>
                                        <p>Every website should use analytics to track where visitors come from and find  information such as their landing page, how many times they have visited, where they came from, etc. and store it in a cookie.</p>
                                        <ul>
                                            <li>Adding Cookies</li>
                                            <li>Tracking the Number of Visits</li>
                                            <li>Sending an Email with the Cookie Info</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sessions</h5>
                                        <p>In this exercise we will create a simple login/logout application where we make a few pages password-protected.</p>
                                        <ul>
                                            <li>Starting a Session</li>
                                            <li>Using Session Variables</li>
                                            <li>Log In/Log Out</li>
                                            <li>Destroying Session Variables</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">File Uploads</h5>
                                        <p>In this exercise we will show you some very basic security measures as well as how to upload files. If you add this type of capability on your live site, it should be in a password-protected area or authenticated in some way.</p>
                                        <ul>
                                            <li>Making a File Upload Form</li>
                                            <li>The $_FILES Array</li>
                                            <li>Uploading Files</li>
                                            <li>Basic Security</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating a Database/MySQL/SELECT</h5>
                                        <p>In this exercise, you’ll learn how to create a database in the phpMyAdmin control panel, as well as how to connect to a database and display some data.</p>
                                        <ul>
                                            <li>Creating a New Database</li>
                                            <li>Connecting to the Database</li>
                                            <li>SQL Basics</li>
                                            <li>The SELECT Statement</li>
                                            <li>Display the Number of Rows Returned</li>
                                            <li>Making a Reusable Connection Script</li>
                                            <li>MySQL vs. MySQLi vs. PDO</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Making a Reusable Connection Script</h5>
                                        <p>Because we will be connecting to the same database from several webpages, it makes sense to save the connection script into another page. That way, we can just include it at the top of any page that needs to connect to the database.</p>
                                        <ul>
                                            <li>Error Checking</li>
                                            <li>Making an Include</li>
                                            <li>Sorting Results</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Prepared Statements</h5>
                                        <p>In this exercise we are going to show how to select a certain row of data in a database. We will  select all the records in the database that are equal to a certain email.</p>
                                        <ul>
                                            <li>Selecting and Filtering Results</li>
                                            <li>Preventing SQL Injection Attacks with Prepared Statements</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">SQL: Insert</h5>
                                        <p>Interacting with a database wouldn't be much use if we couldn't add data to it. Here you will learn how to insert records into a database using prepared statements.</p>
                                        <ul>
                                            <li>The INSERT Statement</li>
                                            <li>Using phpMyAdmin</li>
                                            <li>Inserting Information from a Form</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">SQL: Update</h5>
                                        <p>This exercise will show you the SQL syntax for updating a record in a database as well as how to use a form to update user information including checkboxes and hidden fields.</p>
                                        <ul>
                                            <li>The UPDATE Statement</li>
                                            <li>Update Form</li>
                                            <li>Display Data in the Update Form</li>
                                            <li>Display Checkboxes</li>
                                            <li>Hidden Fields</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">SQL: Delete</h5>
                                        <p>This exercise shows you how to delete records from a database.</p>
                                        <ul>
                                            <li>The DELETE Statement</li>
                                            <li>Deleting Rows from a Database</li>
                                            <li>Passing ID Variables in a URL</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">SQL: Search</h5>
                                        <p>There are an enormous number of ways to search for information in a database. We will show how to perform a basic wildcard search on a column.</p>
                                        <ul>
                                            <li>Wildcard Searches</li>
                                            <li>Searching with a Form</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>

    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>PHP & MySQL training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="PHP & MySQL - 4.7 rating"></span>
                    </div>

                    <h3><span>PHP & MySQL classes rating:</span> 4.7 stars from 201 reviewers </h3>
                </div>

                <div data-aos="fade-up">
                    <div class="owl-carousel owl-instructors owl-theme instructors-list-block " data-aos="fade-up">
                        <div class="list-block">
                            <a href=".modal-instrutor-profile-jeff" data-toggle="modal">
                                <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-jeff.jpg')"></i>
                                <span>Jeff</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;This class was excellent and exceeded my expectations. The instructor presented the material very clearly and tailored the examples to the needs of the students. This approach was effective in reinforcing the subject matter. I would gladly take another course with this instructor.&quot;</p>
                                <span><strong>Cindy Holly | USAgain</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;The instructor was able to freely use topics in real world examples instead of learning "by the book". This helped solidify learning the new material by understanding why certain things are done and how things connect to other things. Project-based learning was awesome!&quot;</p>
                                <span><strong>Kelly Alario | Lady Of The Sea General Hospital</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Another great PHP experience with Chris. He always takes the extra time to make sure I understand what is going on and why something is being done a certain way. Will definitely be back! Thanks.&quot;</p>
                                <span><strong>Mike White</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;The instructor we amazing, he learned what our needs were and tailored the class to our level. He has very high energy and extremely knowledgeable.&quot;</p>
                                <span><strong>Russell Query | Purdue University</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Chris is a very knowledgable, and very very clear in his instructional techniques. He's been able to answer questions that I've had for a very long time! Thank you Chris!&quot;</p>
                                <span><strong>Amanda Webb | Weigel Broadcasting Co</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=48" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>


    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that PHP is a complex programming language and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months. Often the repeat class can be with a different trainer too.<br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Our Walk Away, No Hard Feelings Guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">PHP is not for everyone. If you decided after Day 1 that the class is not for you, we will give you a full refund.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">PHP Group Training</h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in PHP & MySQL. This can be delivered onsite at your premises, at our training center in LOOP or via online webinar.
                    <br>
                    Fill out the form below to receive pricing.
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course[]" checked><i></i> <h3>PHP and MySQL Web Development</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>

    <div class="section section-faq g-text-php">
        <div class="container">
          <div class="section-heading align-left"  data-aos="fade-up">
                <h2>PHP & MYSQL Training FAQ</h2>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5><span class="card-title">Do I need to bring my own computer?</span></h5>
                    <p>No, we provide the computers.</p>
                </li>
                <li>
                    <h5><span class="card-title">Do your PHP classes have a live trainer in the classroom? </span></h5>
                    <p>Yes. Our PHP classes are  taught by  qualified and passionate trainers present in the classroom. Class times are 9.00am to 4.30pm.</p>
                </li>
                   <li>
                    <h5><span class="card-title">Where do your PHP training classes take place?</span></h5>
                    <p>Our training center is located at 915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017.
                     For more information about directions, parking, trains please <a href="/contact-us-los-angeles.php">click here</a>.</p>
                </li>
                <li>
                    <h5><span class="card-title">Can you deliver PHP onsite at our location?</span></h5>
                    <p>Yes, we service the greater Los Angeles metro including Anaheim, Burbank, Covina, Downtown, Fullerton, Irvine, Long Beach, Northridge, Pasadena, San Bernardino, Santa Monica, Van Nuys, Ventura and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver PHP training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite PHP training.</p>
                </li>
            </ul>
        </div>
    </div>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-jeff.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>