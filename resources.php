<?php
$meta_title = "Our Resources Library | Training Connection";
$meta_description = "Learning articles for Microsoft Office, Adobe, Web Development and Business Skills. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Resources</li>
            </ol>
        </nav>

        <div class="page-intro mb-3">
            <div class=" intro-copy">
                <h1 data-aos="fade-up" >Resources Page</h1>
                <p data-aos="fade-up" data-aos-delay="100">Below you will find useful informational articles , including tips and tricks, how-to articles etc. Content is updated on a weekly basis.</p>

            </div>
        </div>

        <div class="tab-section" data-aos="fade-up" data-aos-delay="250">

            <ul class="nav nav-timestable mb-3" id="timestable-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="resource-adobe-tab" data-toggle="pill" href="#resource-adobe" role="tab" aria-controls="resource-adobe" aria-selected="true">
                        <img src="/dist/images/icons/icon-adobe.svg" alt="Adobe Training - Timetable">
                        <h4>Adobe Learning</h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="resource-office-tab" data-toggle="pill" href="#resource-office" role="tab" aria-controls="resource-office" aria-selected="false">
                        <img src="/dist/images/icons/icon-office.svg" alt="Microsoft Office- Timetable">
                        <h4>Microsoft Office Learning</h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="resource-web-tab" data-toggle="pill" href="#resource-web" role="tab" aria-controls="resource-web" aria-selected="false">
                        <img src="/dist/images/icons/icon-coding.svg" alt="Web Development - Timetable">
                        <h4>Web Development Learning</h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="resource-business-tab" data-toggle="pill" href="#resource-business" role="tab" aria-controls="resource-business" aria-selected="false">
                        <img src="/dist/images/icons/icon-business.svg" alt="Business Skills - Timetable">
                        <h4>Business and Softskills</h4>
                    </a>
                </li>
            </ul>

            <div class="tab-content content-default" id="resource-tabContent">
                <div class="tab-pane fade show active" id="resource-adobe" role="tabpanel" aria-labelledby="resource-adobe-tab">
                    <h5 class="show-md">Adobe Learning</h5>

                    <div class="section-list">
                        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resource-posts-adobe.php'; ?>
                    </div>
                </div>

                <div class="tab-pane fade " id="resource-office" role="tabpanel" aria-labelledby="resource-office-tab">
                    <h5 class="show-md">Microsoft Office Learning</h5>
                    <div class="section-list">
                        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resource-posts-office.php'; ?>
                    </div>
                </div>

                <div class="tab-pane fade " id="resource-web" role="tabpanel" aria-labelledby="resource-web-tab">
                    <h5 class="show-md">Web Development Learning</h5>
                    <div class="section-list">
                        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resource-posts-web.php'; ?>
                    </div>
                </div>

                <div class="tab-pane fade" id="resource-business" role="tabpanel" aria-labelledby="resource-business-tab">
                    <h5 class="show-md">Business and Softskills</h5>
                    <div class="section-list">
                        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resource-posts-business.php'; ?>
                    </div>
                </div>

            </div>


        </div>

    </div>

</main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>