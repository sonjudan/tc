<?php
$meta_title = "Thanking you for your booking | Training Connection";
$meta_description = "";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content">
    <div class="section-thank-you copy">
        <div class="container">

            <?php if( isset($_GET['success']) == 1 ) : ?>
                <i class="fa fa-check"></i>
                <h1>Success</h1>
                <h5 class="mt-4">Thanking you for your booking.</h5>
                <p>Your booking details will be emailed to you shortly.</p>
                <a href="/" class="btn btn-primary btn-md mt-3"><i class="fa fa-arrow-left"></i> Back to Homepage</a>
            <?php else: ?>
                <i class="fa fa-check"></i>
                <h1>Success</h1>
                <h5 class="mt-4">Thank you for your inquiry.</h5>
                <p>Your quotation will be emailed to you shortly.</p>
                <a href="/" class="btn btn-primary btn-md mt-3"><i class="fa fa-arrow-left"></i> Back to Homepage</a>
            <?php endif; ?>
        </div>
    </div>

</main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>