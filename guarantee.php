<?php
$meta_title = "Training Guarantee | Training Connection";
$meta_description = "Money-back guarantee terms and conditions.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Class Guarantee</li>
                </ol>
            </nav>

            <div class="page-intro page-intro-row-ribbon mb-5 pb-5">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Money Back Guarantee</h1>
                    <h4 class="mt-4" data-aos="fade-up" data-aos-delay="100">We offer a money back guarantee on our public classes.</h4>
                    <p>If the training does not meet expectations, students can leave the class at the end of Day 1 (or by lunchtime in the case of single day courses) and request a refund. Students have to notify the training center manager before they leave and return all the training materials to qualify for a refund.</p>
                    <p>We are unable to offer a money back guarantee to students who leave a class later than the times noted above. Students can however still repeat the training (often with a different trainer)</p>
                </div>

                <div class="intro-img">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>

    </main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>