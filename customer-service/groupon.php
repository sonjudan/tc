<?php
$meta_title = "The Groupon Effect on Customer Service | Training Connection";
$meta_description = "How Groupon can hurt a business and 5 ways to revamp your customer service offering.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Groupon Effect</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>The Groupon Effect on Customer Service</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Groupon is a name that can make or break your business. You can be a small or large business who wants to expand your customer base but if you are not prepared to make way for new customers, your business will be well known but not for the better. Refreshing customer service skills and experience will be critical to your management of such instantaneous demand.</p>
                <p>For instructor-led <a href="h/customer-service-training.php">Customer Service workshops</a> in Chicago and Los Angeles call us on 888.815.0604.</p>



                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/customer-service-banner.jpg" width="750" height="374" alt="Customer Service Training"></p>

                <p>Since 2008, Groupon is known as one of the fastest growing companies in history connecting millions of subscribers to local merchants by offering activities, travel, goods and services in more than 28 countries. Essentially, today when you leave for lunch, use your groupon app in the city and you will be directed to local and immediate deals on things to do, places to eat and even items you might like to buy. <a href="/customer-service-training-chicago.php">Customer service training class in Chicago</a>.</p>
                <p>Not all businesses have become huge fans of Groupon. </p>
                <p>There is a popular story about Groupon’s effect on a company known as Back Alley Waffles. The complaint:</p>
                <p>"Due to the shocking business practices of an obscenity known as 'Groupon' -- contemptible even by the nearly non-existent standards of the modern corporation - I can no longer afford to sell waffles for $8.00 and still pay, for example, my employees something north of a subsistence wage," the former restaurant&nbsp;wrote on its website. Certainly no waffling on there.</p>

                <blockquote>"Therefore, starting now, waffles are $450.00 each and are available by appointment only." </blockquote>

                <p>Read more:&nbsp;<a href="http://www.nasdaq.com/article/does-groupon-help-businesses-thrive-or-bury-them-alive-cm243672#ixzz4N05dosbX">http://www.nasdaq.com/article/does-groupon-help-businesses-thrive-or-bury-them-alive-cm243672#ixzz4N05dosbX</a></p>

                <p><a href="http://www.businessinsider.com/the-man-who-claims-groupon-killed-his-waffle-shop-hates-groupon-users-too-2012-7">Back Alley Waffles</a> closed down officially because it never obtained a business license or certificate of occupancy. After researching this company’s story a bit more, we can see that Back Alley Waffles closed down because it was not able to meet the unforgiving expectations of unfamiliar customers Groupon brings. By having a customer service strategy ready for demands beyond just providing discounts, your business can greatly benefit from making new relationships with customers eager to see you succeed.</p>
                <p>Since I have delivered many customer service training programs and classes in the LA area, I noticed a trend in management complaints about how the reviews online brought them to terms to refresh their team’s customer service skills. One such company, a laser salon in Glendale, CA where the Yelp reviews were showing a decrease in ratings, are still thriving and getting high ranking reviews in customer service thanks to my visit. This salon had great people and services already yet it badly needed to overhaul the customer experience and ensure current customers felt as special as new customers. </p>
                <p>The training program I designed revamped customer experience in 5 ways:</p>
                <ul>
                    <li>Greeting</li>
                    <li>Body Language</li>
                    <li>Customer Education</li>
                    <li>Customer Attention</li>
                    <li>Customer Follow Up</li>
                </ul>

                <h4>Greeting</h4>
                <p>When you enter a 5 Star Hotel, you know it’s a 5 Star Hotel. If you have bags in your arms or you are pushing Samsonite luggage on wheels, someone will have already approached you before you have had a chance to get out of the car! When you are attempting to upgrade your customer service experience, consider the value of going beyond smiling at your desk and actually rising to say hello and welcome guests. Say “Welcome!” Introduce your name and point out the most critical next steps for those arriving. </p>

                <blockquote>“Hello! Welcome! I’m Tara. Here is where you sign in, bathrooms are just around that corner and you can place your things right here as I get you set up."</blockquote>
                <h4>Body Language</h4>
                <p> Show openness and hush yourself if the customer begins speaking over or before you. This seems simple enough but it’s not because customers may have requests up front that may challenge any expectation you ever intended to meet. The customer could show up late, be carrying a pet, still have a cigarette or even show up on the wrong day and have the wrong or an expired Groupon deal in hand. Remember that this guest experience needs to be one that will make them want to come back. Remain calm, welcoming and show the customer you will take care of him or her. <a href="/customer-service-training-los-angeles.php">Los Angeles workshop on great customer service</a>.</p>
                <p><strong>Body Language of Openess:</strong></p>
                <ul>
                    <li>Use full hand to direct and point out the environment, seating, any paperwork or people. Avoid using index fingers unless bringing attention to something written in small print on a piece of paper</li>
                    <li>Maintain a positive expression and a quiet disposition waiting your turn to speak and nod respectfully no matter how informal the customer behaves.</li>
                    <li>Stand to attention and relax arms to your sides</li>
                    <li>Show concern and never roll your eyes or look at others to agree with you on how this person is behaving</li>
                    <li>Be willing to peek away quickly to make eye contact and nod to any others waiting in line for your attention</li>
                    <li>Continue to show thankfulness for the person’s visit, comments and feedback</li>
                </ul>
                <h4>Customer Education</h4>
                <p>When companies ask me about improving their customer experience, we first must acknowledge that anyone who is not you is your customer and second, your customer is not always right- your customer is always important. The way you talk to others affects what they think of how you talk to them. Make your team and your customer aware how important they are with the deference you show and the expectations you set. Instead of telling a customer what they cannot do, first show appreciation and briefly educate. </p>

                <blockquote>"I’m glad you are here. We have a policy that prohibits any animals inside. Allow me to research what we can recommend for your animal friend."</blockquote>
                <blockquote>"We are excited for your visit today. After sitting here and completing your forms, your laser technician will come and ask for you by name and direct you to your room for the requested services."</blockquote>
                <blockquote>"My name is Michelle and I will be your information source and cashier during your visit. Guests sit in this room where we have some magazines, beverages and here is the wifi code. Your technician will find you here and direct to you to the laser room for services."</blockquote>
                <blockquote>"Good afternoon, I’m your laser technician today. First, I’ll have you answer some questions, we will prepare any extra details. The procedure takes approximately 20minutes and we can address follow up notes shortly after. Any questions?"</em></span></blockquote>

                <h4>Customer Attention</h4>
                <p>Check in on your guests. Assign someone the task of visiting new or regular guests to confirm they are having a positive experience. In today’s world, reviews are being sent into the social media universe while people wait for service. Facebook now has live broadcasting tools available for any and all posts- even comments! Show positive and energetic willingness to give guests evidence of your desire to impress them with how you care. Sometimes, you may have to double check what you already know yet it is better to show the gesture of effort than turning down a request flat.</p>

                <h6>
                    Customer: Can you change the radio station?  <br>
                    Is there a way you could turn the heat down? <br>
                    Thanks for your request. I will check on this for you!
                </h6>

                <h4>Follow Up</h4>
                <p>Ensure that every visiting customer takes a business card, menu or list of services brochure. When the customers are showing extra thankfulness for their experience, remind them to submit a review and hashtag your business name in their post. This could even lead to promotional ideas. Should a customer say nothing, thank the persons for their visit and remind them to feel free to call with any feedback or questions about their experience.</p>
                <p>The laser salon employees and management were very happy to get a final live onsite simulation experience reinventing their customer experience with these basic changes at work. More business should mean more money and not complaints. Should you decided to use promotions that will invite new customers, upgrade and refresh your customer service strategy first! </p>

                <p>Sign up for our <a href="/customer-service-training.php">Customer Service class at Training Connection</a> or hire us for onsite team coaching today!</p>


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Other related articles:</h4>
                    <ul>
                        <li><a href="http://boss.blogs.nytimes.com/2010/11/23/doing-the-math-on-a-groupon-deal/?_r=0">Doing the Math on a Groupon Deal</a></li>
                        <li><a href="http://www.businessinsider.com/inside-groupon-the-truth-about-the-worlds-most-controversial-company-2011-10">The Truth about Groupon the Worlds most controversial company</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>