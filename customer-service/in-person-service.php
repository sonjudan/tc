<?php
$meta_title = "In-Person Customer Service | Training Connection";
$meta_description = "Learn how to deliver great service when dealing face-to-face with your customers. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">In-Person Customer Service</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Face to Face Customer Service</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In-person interactions provide a great opportunity to build rapport with customers. When you talk to a customer on the phone or you exchange emails with a customer, it can be difficult sometimes to get a sense of what the other person is thinking and feeling. But when you talk to a customer in person, you get constant feedback, both verbal and nonverbal. It’s easy to tell if you are creating the right impression. Although in-person interactions can be difficult at times, they offer exceptional insight into what customers want and need.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/customer-service-rep.jpg" width="750" height="435" alt="Delivering great service face to face"></p>
                <h4>Dealing with At-Your-Desk Requests</h4>
                <p> Sometimes working in a job that requires customer service will put you on the spot. You can be working hard on something you have had in your planner for days, when suddenly a customer turns up at your desk with a problem. It is common in these situations to wonder if you should go ahead and provide the customer with the help they need - thus missing the work you were scheduled to do - or to try and find a way to get rid of the customer so you can get on with what you need to do. In any case like this, the matter of priority arises.</p>
                <p>For more details on <a href="/customer-service-training.php">our customer service classes</a> call us on 888.815.0604.</p>
                <p>There is nothing more unprofessional than turning a customer away and saying “Sorry, I’m busy. If you want to come back another time I’m sure I’ll be able to fit you in tomorrow/next week/ in ten days’ time”. The customer has come to see you in person, they consider their problem to be of some importance, and they are relying on you to help them. If the work in hand is really something that cannot wait you have two options. You can find someone else who will be able to do the work, and turn your attention to the customer, or you can find someone to help the customer so that you can get on with your work.</p>
                <p>It is vital to keep a sense of courtesy whatever you decide to do. The customer who has come all this way to see you will be somewhat unimpressed if you just ask them to take a seat and assign someone to ask what the problem is. However urgent the work you are doing is, you have enough time to speak to the customer, ask them the nature of their problem, and think about how best to bring about a solution. It may be that the difficulty is one that only you can solve. If, however, you can find someone else to help the customer, make sure that you have taken note of the problem and explain it thoroughly to whoever you delegate the matter to. Also ensure that the person you hand over to is someone who can genuinely solve the problem. Otherwise it will look like you have simply wriggled out of the situation. Courtesy and competence are essential, whoever deals with the problem. <a href="/customer-service-training-los-angeles.php">Customer Service group training in LA</a>.</p>

                <h4>The Advantages and Disadvantages of In-Person Customer Service</h4>
                <p>Sometimes working in a job that requires customer service will put you on the spot. You can be working hard on something you have had in your planner for days, when suddenly a customer turns up at your desk with a problem. It is common in these situations to wonder if you should go ahead and provide the customer with the help they need - thus missing the work you were scheduled to do - or to try and find a way to get rid of the customer so you can get on with what you need to do. In any case like this, the matter of priority arises.</p>
                <p>There is nothing more unprofessional than turning a customer away and saying “Sorry, I’m busy. If you want to come back another time I’m sure I’ll be able to fit you in tomorrow/next week/ in ten days’ time”. The customer has come to see you in person, they consider their problem to be of some importance, and they are relying on you to help them. If the work in hand is really something that cannot wait you have two options. You can find someone else who will be able to do the work, and turn your attention to the customer, or you can find someone to help the customer so that you can get on with your work.</p>
                <p>It is vital to keep a sense of courtesy whatever you decide to do. The customer who has come all this way to see you will be somewhat unimpressed if you just ask them to take a seat and assign someone to ask what the problem is. However urgent the work you are doing is, you have enough time to speak to the customer, ask them the nature of their problem, and think about how best to bring about a solution. It may be that the difficulty is one that only you can solve. If, however, you can find someone else to help the customer, make sure that you have taken note of the problem and explain it thoroughly to whoever you delegate the matter to. Also ensure that the person you hand over to is someone who can genuinely solve the problem. Otherwise it will look like you have simply wriggled out of the situation. Courtesy and competence are essential, whoever deals with the problem.</p>
                <h4>Using Body Language to Your Advantage</h4>
                <p>In any in-person interaction, body language has a major effect on how people interpret your message and respond to it.</p>
                <p>Types of body language:</p>
                <ul>
                    <li>Eye contact</li>
                    <li>Facial expression</li>
                    <li>Posture</li>
                    <li>Gestures</li>
                    <li>Nodding (or shaking your head “no”)</li>
                </ul>
                <p>Body language is a controversial topic. There are many people who will swear blind that, whatever anyone else says, the only kind of language that can be trusted is that which is spoken and is written down. However, there are many dedicated body language experts who have made clear and incontrovertible findings that make clear the truth about body language. It is said for a reason that it is possible to lie with your words, but never with your eyes. Whatever you can say to a person vocally, it is always better to be able to back it up by looking them in the eye and making a statement that they can trust.</p>
                <p>Body language is honest in many ways because it happens by accident. When you are speaking to someone, you are likely to be doing things with your body that you do not even realize you are doing. Ask any seasoned poker player and they will tell you that - in a game which uses few words - the way they know what another player is going to do, and what hand they have, is by looking at their hands, their face and their body. Some people touch their face or tug their collar when they are lying. Someone at ease will sit back in their seat and be more open in their posture. A person under pressure will look around themselves more. <a href="/customer-service-training-chicago.php">Interactive customers service classes</a> in Chicago.</p>
                <p>Whatever you say to a customer, it is important to use body language to your advantage. The way you conduct yourself in the presence of a customer may well have more impact on their confidence in you than anything you say to them. If you look around you when they are relating a problem, it will give them the impression that you do not care and only want them to get it off their chest and leave you alone. If you look at them and nod when they say something of importance, they will take from that that you are listening to them and are interested in seeing that their problem gets solved. Retaining a customer’s confidence is essential, and <a href="/business-communication/reading-body-language.php">your use of body language</a> will dictate how successful you are in doing that.</p>

            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Customer Service Lessons</h4>
                    <ul>
                        <li><a href="/customer-service/phone-service.php">Customer Service over the Phone</a></li>
                        <li><a href="/customer-service/online-service.php">Customer Service via Email</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Customer Service training</h4>
                    <p>Through our network of local trainers we deliver onsite Customer Service classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Customer Service class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=25">Customer Service student reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>