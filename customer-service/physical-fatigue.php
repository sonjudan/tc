<?php
$meta_title = "Reducing Fatigue in Customer Service | Training Connection";
$meta_description = "This article discusses ways for Customer Service representatives to reduce physical fatigue on the job.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reducing Tiredness</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Reduce Physical Fatigue In Customer Service</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>While it would be easy to blame the customers for how weary you feel after handling irate customers all day, there are other factors that can make one less friendly or efficient in Customer Service. In this article, we will cover how activity level can limit your ability to enjoy a strong and energetic body throughout your day and in your personal time. </p>
                <p>For <a class="cust-serv" href="/customer-service-training.php">interactive instructor-led Customer Service classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/tired-service-rep.jpg" width="780" height="446" alt="Customer Service Training"></p>
                <p><strong>First, let’s look at this scenario:</strong></p>
                <p>Bruce arrives late to the call center at 8:05 in the morning, so he rushes to clock in, straps on earphones and sits down in a rolling office chair to take the first call. He will take only a couple breaks for restroom, a chat with Greg on escalated calls and may go to the conference room for any weekly meetings before sitting the rest the day at his desk. He often stays an hour later to check personal emails. He comes home, watches his favorite shows on the couch and occasionally goes to the gym late but he hurt his shoulder the last time he bench pressed so he’s stayed away as of late. <a href="/customer-service-training-chicago.php">Customer Service training sessions in Chicago</a>.</p>
                <p>Bruce is like many who work 9 to 5 at a desk. He is too tired to leave work and hardly makes it to the gym. Best advice? </p>

                <h4>If you are seated all day, keep it moving!</h4>
                <p>Dr. James Levine, director of the Mayo Clinic-Arizona State University Obesity Solutions Initiative and inventor of the treadmill desk is responsible for coining the phrase “Sitting is the New Smoking.” Also the author of the book <em>Get Up! Why Your Chair Is Killing You and What You Can Do About It</em>, has investigated the <a class="cust-serv" href="http://articles.mercola.com/sites/articles/archive/2014/09/28/dangers-prolonged-sitting.aspx">health effects of sitting</a>. </p>

                <p>His findings concur with many other studies that the body was designed to be active and moving all day long. For example, when you are sitting for a long period of time and get up, within the first 90 seconds, a number of molecular changes can finally take place. The muscular and cellular systems process that much more blood sugar, triglycerides, and <a class="cust-serv" href="http://www.mercola.com/infographics/cholesterol-levels.htm">cholesterol</a> simply by moving one’s own body weight. Prolonged lack of movement on a regular basis can lead to organ damage, weak digestion, and postural imbalances which contribute to bodily pain, poor circulation and weak bones. Sedentary lifestyles due to many of these related health effects can also lead to a shorter life. One study found, for instance, that reducing the average time you spend sitting down to less than three hours a day could increase your life expectancy by two years.</p>
                <p>What can we recommend for desk bound customer service professionals like Bruce? </p>
                <ul>
                    <li><strong>Stand Up and Stretch between, while or after taking calls every 15 minutes </strong>When you are sitting at a chair all day, your hips are flexed, abs and glutes are not firing and your head leans forward further hunching your untrained back muscles. Your body’s range of motion becomes limited as it adapted to your repeated posture<strong></strong>.</li>
                    <li><strong>Use Self Myofacial Release to prevent muscle tightness and postural dysfunction </strong>Start with self myofacial release by applying pressure using a lacrosse or Trigger Point balls on tight muscles that have adapted and weakened while you have been at your desk for two hours. Common tight muscles from sitting at a computer are between should blades, lower back, chest and even hamstrings. </li>
                    <li><strong>Stretch using bands or dynamic movements </strong>Light stretch bands can be used to raise over your head and in front of you to open up your fullest range of motion. You can also place your hand on the wall and push your chest forward. For your lower body, you can step back into a lunge or step to the side to a side lunge remember to do both sides!</li>
                    <li><strong>Give yourself posture checks while you are working at your computer! </strong>Good posture while seated would include shoulders rolled back while sucking your abs in. For your glutes, be sure to walk or stand with abs locked in and pelvis forward. Make exercises dynamic by doing at least 3-5 couple repetitions at a steady pace.</li>
                    <li><strong>Get away from your workstation and walk as much as possible during breaks and lunch </strong>You may only have 15 minutes and like Bruce, you are so fatigued from sitting all day that you have no desire to move but it is critical you get your body moving! Instead of chatting the whole lunch break over the food you ate, go for a walk as soon as you finish your food! Take the stairs, walk to your car, or even the perimeter of your work building.</li>
                </ul>
                <p>Also read <a class="cust-serv" href="http://www.emaxhealth.com/8782/sitting-work-harmful-15-ways-fight-fatigue">Sitting at Work is Harmful: 15 Ways to Fight Fatigue</a>.</p>
                <p>It is highly recommended that Bruce start the day with exercise but if not, it would behoove him to do this immediately after work if possible. Before he starts his workout, he can also review the same release and stretches he did at the office. It is quite possible his shoulder is injured due to a limited range of motion and / or strain from lifting with poor posture. <a href="/customer-service-training-los-angeles.php">Los Angeles training</a> to improve a company's service levels.</p>
                <p>While Dr. Levine would say that one hour of exercise does not cancel out 8 hours of sitting, it will be up to Bruce and each of us to continue to be mindful to keep moving, stretching and checking our posture to become stronger and remain energized throughout and beyond the work day.</p>
                <p>Also read <a class="cust-serv" href=/customer-service/groupon.php">5 ways to revamp your customer service experience</a>.</p>


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other related articles:</h4>
                    <ul>
                        <li><a href="http://fitness.mercola.com/sites/fitness/archive/2015/05/08/sitting-too-long.aspx" target="_blank">Here's What Sitting Too Long Does to Your Body by Dr. Mercola</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Need Onsite Customer Service training?</h4>
                    <p>Have a group of customer service people who are low in morale? We can provide <a href="/customer-service-training.php">group onsite Customer Service training</a> at your offices. <a class="cust-serv" href="hhttps://www.trainingconnection.com/onsite-training.php">Obtain a quotation</a> today!</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>