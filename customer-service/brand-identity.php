<?php
$meta_title = "Customer Service Scripting | Training Connection";
$meta_description = "This article discusses how to build a Brand through Customer Service Scripting.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Scripting</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Brand Identity through Customer Service Scripting</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>A recent <a href="https://www.scribd.com/document/74623976/The-Ritz-Carlton-Hotel-Company">Ritz-Carlton Case Study</a> reveals how standardizing service language across multiple corporate branches and locations can set a business apart from other businesses by setting an expectation for higher quality communication from service personnel. &nbsp;Similar to updating a brand spokesperson or logo look and typography, updating customer service language of your business is also key to connecting with the times.</p>
                <p>For <a href="/customer-service-training.php">Customer Service classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/customer-service-brand.jpg" width="780" height="453" alt="Greeting Customer Effectively"></p>

                <p>Brand identity connects with customers by providing a consistent image, quality, and experience customers rely on. In this article, we consider how the Ritz Carlton branded their service communication across regions becoming a major service industry influencer while still seeing the need to update their communication brand to stay relevant. </p>
                <p>When you think of luxury hotels, The Ritz Carlton likely comes to mind. The Ritz-Carlton Hotel Company is recognized as one of the world’s leading luxury lifestyle brands and enjoys a global reputation for setting the gold standard with award-winning luxury hotels, residences, golf communities, elegant spas, innovative retail and acclaimed restaurants. The brand engages guests with the motto "We are Ladies and Gentlemen Serving Ladies and Gentlemen.” </p>

                <p>Micah Solomon wrote in his book, <a href="https://www.amazon.com/dp/0814415385/ref=cm_sw_su_dp">Exceptional Service, Exceptional Profit</a>.</p>
                <p><em>To help launch their Ritz-Carlton luxury hotel brand [the founding leadership] decided on a set of ideal phrases for use in conversation with customers, then trained employees to use those phrases. The frequent use of certain phrases helped unify their employees around a shared identity and contributed to a distinctive ‘Ritz style’ that the public could easily recognize: phrases like ‘my pleasure,’ ‘right away,’ ‘certainly,’ and personal favorite‘we’re fully committed tonight.’ (Translation: ‘We’re booked solid, bub!’) The list of words and phrases to be avoided included ‘folks,’ ‘hey,’ ‘you guys’ and ‘OK.’</em></p>

                <p>With 91 hotels worldwide, you can imagine what standardizing this experience must have cost the company. The Ritz Carlton today is the only service company in America that has won the <a href="https://www.nist.gov/baldrige">Malcolm Baldridge National Quality Award</a> twice, and&nbsp;<a href="https://trainingmag.com/"><em>Training Magazine</em>&nbsp;</a>has called it the best company in the nation for employee training. Even so, feedback in the mid-2000s began to lose it’s shine and phrases became misused and abused. </p>

                <p>For example, it makes sense to say “Thank you, Mr. Johnson. It’s been my pleasure to assist during your stay.” It almost sounds like a joke to say, “It will be my pleasure to remove your trash.”</p>
                <p>With over 20 years training workforces and delivering one-on-one coaching for executives in the US and abroad on corporate communication skills, customer service language training can tap into more than teaching phrases or scripts but also instructing professionals on the service purpose of their communication, allowing for agents in all industries to express sincere concern in a more natural way. <a href="/customer-service-training-chicago.php">Chicago customer service classes</a>.</p>
                <p>For example, when working with professionals on how to handle correcting an issue, I teach three steps: Care, Educate and Offer Options which I have coined "The CEO Method." © With a trucking firm, that sounds like: I get what you are saying. We have a turn-around time of 24 hours. How about we do (option A) or (option B)?”</p>
                <p>In fast growing mobile app firm full of millennials: “I super understand this is important to you. Our process takes 24 hours. We can offer you (option A) or we can (option B)? Which works best for you?</p>
                <p>The Ritz-Carlton Case Study shows that not only the overuse of a phrase can weather it’s meaning but the relatability of a phrase is essential to allowing people to express themselves outside of the script and create authentic connections with customers, especially for younger customers who felt extra distanced with 1860s language of elegance. Elegance would then be defined by genuine and authentic care, knowledge and courtesy.

                  <a href="/customer-service-training-los-angeles.php">LA-based customer service classes</a>.
              <p>While the current CEO, Herve Humler, who serves as Ritz-Carlton’s current president and COO and was a member of the founding Ritz-Carlton leadership team in 1983, confided in an interview the signature script has become “intentionally less formal overtime,” speaking with respect and courtesy remain. </p>

                <p>For example, instead of saying, “You owe us $50,” you can say “Our records show a balance of $50” which is an objective and non-accusatory way to communicate someone’s negative account. </p>
                <p>Learning a script takes rote, but learning a new strategy for handling people takes fluency. </p>

                <p>Read more about the <a href="/customer-service/greeting.php">5 How to Greet Customers</a>.</p>


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Need Onsite Customer Service training?</h4>
                    <p>Need to improve your customer's service brand? We  provide  <a href="/customer-service-training.php">onsite Customer Service classes</a> countrywide. <a href="/onsite-training.php">Obtain a  quotation</a> today! Review our <a href="/testimonials.php?course_id=25">past student feedback</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>