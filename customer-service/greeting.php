<?php
$meta_title = "How to Greet your Customers | Training Connection";
$meta_description = "This article discusses betters ways to greet your customers, including how to remember their names.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Greetings</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Greet Customers</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Unlike an unresponsive touch screen or automated message system, you have the power to humanize your company’s service and redirect customer relations in a winning direction when interacting with a live customer. In today’s article, cover how you can improve the impression you make when you greet your customer. </p>
                <p>For <a href="/customer-service-training.php">exceptional  Customer Service classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/greeting.jpg" width="750" height="506" alt="Greeting Customer Effectively"></p>

                <p>When someone walks through the door of a business or reaches a company representative on the phone, it is more likely that the customer has a complaint than a compliment to share. While the popular business thought has taught us to say, “The Customer is Always Right,” in my classes, I teach customer service representatives to remember “The customer is not always right, the customer is always important.” Instead of patronizing the customer, we need to do a better job of making them feel important. <a href="/customer-service-training-los-angeles.php">Customer Service workshops in Los Angeles</a>.</p>
                <p><em>“I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.” Maya Angelou</em></p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/customer-feels.jpg" width="518" height="289" alt="How a customer feels"></p>

                <h4>Greeting at 100%</h4>
                <p>When you are at home and the phone rings, you answer from varying levels of personal content. For example, if you are in an okay mood, you answer with a neutral tone- let’s say 50 or 60%. If you are considering some sad news, you may answer in a somber tone at about 20-30%. Depending who is on the other line, if you are so pressed, you may even answer with more emotion such as with a trembling voice about to cry at 2% or suddenly exclaim “Hello!” at 100% with full on energy because you are happy to hear from that someone. In this social scenario, you are at home to answer in any mood you want. </p>
                <p>When you are in customer service, you must create a consistent 100-150% energy front showing the customer he or she has reached someone with traits including authentic care, evident intelligence and an eager willingness to help. When the caller detects you lack these, their sense of content decreases and now, resolving their issue becomes a lot more work. </p>

                <h4><strong>Reconsider Your Greetings</strong></h4>

                <p>Now imagine yourself planted at your desk and the phone rings. What mood will you exhibit? If someone walks in the door? What act of authentic care will you show? Does the customer need to earn your energy? Have you always chalked it up to personality to explain why you are not showing any of these traits? Even if you are physically at home, since you are paid to represent the business front, demonstrating good customer service traits could make or break your relationship as well as shorten or lengthen the customer interaction. </p>

                <p>Greeting customers at a 100% means you do everything you can to make your customer feel important by showing authentic care, evident intelligence and an eager willingness to help. <a href="/customer-service-training-chicago.php">Customer service course offered in Chicago</a>.</p>
                <p>Here are three ways to improve how you greet your customers 100% plus!</p>

                <ol>
                    <li>
                        <h4>Demonstrate A Positive Disposition</h4>
                        <p>People know when you appreciate them just by the genuine approach exhibited by your facial expression, posture and willingness to assist.</p>
                        <p>Imagine your best friend who has been away a long time suddenly shows up at your work to say hello. Not only would you smile so big but you would find your eyebrows had raised up, your disposition and posture became brighter and out of mere habit, you would go and greet your friend. When a customer shows up to your business, these same expressions of concern and eagerness can help recover a lot of lost content before he or she even has a chance to speak. </p>
                        <p><strong>In person: </strong></p>
                        <p>Raise your eye brows, smile, sit up straight and rise from your seat to acknowledge the customer right away. Be sincere! Remember that you need to make this customer your ally to get things done.</p>
                        <p><strong>Over the phone: </strong></p>
                        <p>Follow the same nonverbal cues since being extra laid back and not using any facial expression often sounds like it even over the phone. Use your sound to communicate enthusiasm to help!</p>
                        <p>When you do not do this, your customer will work that much harder to express urgency, importance and a desire to speak with a more willing and able service provider. Start off in control of this perception! Even if your department is not responsible for what this person needs, they can say that you were on top of your game to redirect them to the appropriate channels.</p>
                    </li>
                    <li>
                        <h4>Voice</h4>
                        <p>When you learn to speak other languages, you soon discover that learning one word is not enough to truly communicate. If you have only learned English, delivering customer service  will be your first second language. What you learn from foreign languages is that in order to be understood, you will need to enunciate, observe a respectful tone and consider your speed. </p>

                        <p><strong>Enunciate:</strong></p>
                        <p>“Afortunutly, we haven receive ya check.” This is an example of speaking with all the words crammed together and can be heard anywhere in the US.</p>
                        <p>“Un-fortunate-Ly, we have not receiveD’ your check.” This is an example of making the extra effort to enunciate all parts of the words just so that is clear to the listener. This applies to those with a different accent, hard of hearing or when English is not their first language. You will need to pronounce words clearly with customers to improve how you are understood. </p>
                        <p><strong>Tone: </strong></p>
                        <p>Many find Chinese hard to learn since they must observe <a href="https://youtu.be/3wV8B4bx1lM">tone</a>. Even saying hello in Swedish might sound like you are asking a question in English. Tones are also significant in English. Finishing a statement with rising intonation shows openness and finishing with descending intonation shows finality. </p>
                        <p>
                            Agent: (Intonation descending) How can I help you? <br>
                            Agent: (Intonation ascending) How can I help you?
                        </p>
                        <p>On a casual basis, you can get away with being you but speaking in the capacity of customer service will require you to master intonation which communicates good intent, openness and willingness even when you are resolving customer challenges. </p>

                        <p><strong>Speed:</strong></p>
                        <p>Speaking faster when someone does not understand or speaking extra deliberately (staccato) can be an insult to the person who has asked for more clarification. Since we know that metronomes can ease a crying infant if set to the beat of a mother’s resting heart rate, how much more important is it that we speak with a well-paced speed. Your speed demonstrates urgency and yet control to manage the expectations on the other side of the phone. </p>
                        <p>Speed can affect pronunciation but it can also make you have to repeat yourself. Should a customer ask you to repeat, find another way to say what you said. When you speak this time, watch your pace, pronunciation and tone. Be careful not to speak so slow that the other person senses any condescension. Ensure that you remain in a positive disposition.</p>
                    </li>
                    <li>
                        <h4>Name</h4>
                        <p>When working with the Virtual Bank training team at LaSalle Bank in downtown Chicago, using the customer’s name was among the key metrics online teller calls were evaluated on. An agent would lose points for not learning and using an account holder’s name.   </p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/customer-name.jpg" width="780" height="506" alt="Ask for a customer's name"></p>

                        <p>Asking a customer’s name is one of the few closed questions you can ask while establishing rapport with a customer having challenges. Knowing the customer’s name can also assist you later in the call when you need to summarize and present action items. </p>
                        <p>When you have asked, “With whom am I speaking” or “May I have your name, please?” you have demonstrated utmost thoughtfulness to the customer. When you forget or do not know the customer’s name, you can lose all the respect you could have been earning by asking so late or forgetting so remember to&nbsp;write it down and insert the customer’s name intermittently during your interaction. </p>
                        <p><strong>In Person: </strong><br>

                            “May I have your name, please?” “How do you spell that?” and “Thank you, (use name here)” “How may I address you?”</p>

                        <p><strong>Over the phone: </strong></p>
                        <p>“With whom am I speaking?” “How do you spell that?” and “Thank you, (use name here)” “How may I address you?” </p>
                        <p>Asking “May I call you ____” may not be as polite as asking the customer how they would like to be referred to in this interaction but it is moving in the right direction. Sometimes, we cannot assume male or female over the phone or via chat messaging with a customer, so this gives the customer the power to confirm what name is most comfortable.</p>
                    </li>
                </ol>


                <p>Customer Service is an exercise of willpower. <a href="http://www.apa.org/helpcenter/willpower.aspx">Willpower</a> is a muscle and you only get stronger by using it so, keep improving on how you demonstrate a positive disposition, use customer’s names and manage your vocal cues in customer service. Though we cannot control what makes other’s happy, we can control those cues that make people feel less important. Even when handling a difficult issue or customer attitude, your control of yours it what makes you professionally invulnerable since you show authentic care, provide information with a respectful tone and make the effort to learn other’s names and understand their questions. </p>
                <p>For more work on gaining fluency in customer service skill and language, sign up for or one of our upcoming Customer Service classes.</p>


                <p>Read more about the <a href="/customer-service/words.php">5 words Customers don't want to hear</a>.</p>


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Need Onsite Customer Service training?</h4>
                    <p>Need to improve your customer's experiences? We can provide  <a href="/customer-service-training.php">onsite Customer Service classes</a> countrywide. <a href="/onsite-training.php">Obtain a  quotation</a> today! Review our <a href="/testimonials.php?course_id=25">past student testimonials</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>