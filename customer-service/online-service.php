<?php
$meta_title = "How To Provide Excellent Customer Service Online | Training Connection";
$meta_description = "Electonic and Email customer Service is becoming more important. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Email Customer Service</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Providing Excellent Service Online</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>A growing number of customer interactions are taking place online. Younger people in particular prefer to do too much of their business online rather than in person. But online interactions have limitations. To provide excellent customer service online, you need to understand what works and what doesn’t work, and how to make the most of the tools that are available to you.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/email-service.jpg" width="640" height="360" alt="Delivering great service via email"></p>
                <h4>The Advantages and Disadvantages of Electronic Communication</h4>
                <p>Electronic communication is something which has taken off in a big way in today’s society. Most people now are familiar with e-mail, text messaging, instant messaging, and social networking sites such as Facebook and Twitter. Each of these forms of communication has definite advantages, but it is worth remembering that, relatively speaking, these forms are in their infancy. Many of us may have been e-mailing for a decade or more now, but people have been using the telephone for much longer, writing letters for longer than that, and speaking directly (one way or another) since mankind began. There are many people who have become very used to doing things in the “old” ways, and who are not yet on the same page when it comes to electronic communication.</p>
                <p>For more details on <a href="/customer-service-training.php">our customer service workshops</a> call us on 888.815.0604.</p>
                <p>The advantages of e-mail are very obvious. Firstly, it is highly convenient. Unlike sending a letter, e-mail gets there instantly. If someone is looking for a detailed, same day response and cannot get to the telephone at the same time as you, e-mail is absolutely wonderful as a way of getting the information across. As well as this, a telephone call costs a certain amount per minute. However long you make your e-mail message, it will cost the same to send as a three-line update. Once it is sent, it stays in the recipient’s inbox until such time as they read it and then decide what action to take.</p>
                <p>However, as has been mentioned with telephone conversations, there is a body of opinion which holds that e-mail is a very impersonal and cold way of communicating. Certainly if someone wishes to pass on news that may be sensitive, e-mail is not the best way to go about it. A telephone conversation leaves us relying on the inflection in our voice to give the correct interpretation to the words (in the absence of body language) - and in e-mail, we don’t even have that inflection to rely on. Therefore, e-mail does have its benefits and cannot be dispensed with entirely as a way of providing customer service, but its limitations need to be understood. For <a href="/customer-service-training-chicago.php">more details</a> on customer service training in Chicago.</p>

                <h4>Understanding Netiquette</h4>
                <p>With the massive changes that the Internet has brought to many of our lives, it is entirely unsurprising that it has brought another substantial change - that which it has wrought on the English language - and most other languages too. New words have been invented - and new uses found for old words - in order to describe things which simply did not exist before the Internet came along and changed our world. A decade ago, pretty much no-one “blogged”, absolutely no sane human being Tweeted, and the word “netiquette” was unknown. Not only do we have to mind our Ps and Qs, we would be well advised to keep an eye on our @s as well. </p>
                <p>If we are required to contact a customer by e-mail, it is important to be aware that the usual standards pertaining to e-mail do not apply. Many, if not most, people, have a different way of expressing ourselves in e-mail than we would if we were writing a letter or speaking on the telephone. Perhaps emboldened by the text messaging revolution, many people have taken the “txt spk” approach to writing e-mails. Even though e-mail is not bound by the character limits that text messaging and Twitter impose upon us, people will still try to squeeze a message into a few short lines and cut words down. But when using e-mail in a business setting, it is essential to avoid this, as it is seen as being unprofessional.</p>

                <h4>Tips and Tricks</h4>
                <p>Because so much of what we do on the Internet has been molded from the social aspect, which makes the medium great fun for most of us, the process of electronic communication has become more influenced by that social aspect. When we are communicating with customers it is essential to remember that things are different. We all have different ways of expressing ourselves in person, on the phone, and the Internet. The issue of how to correctly express oneself in online communications will be somewhat different from the traditional ways.</p>

                <p>Electronic communication is disembodied, and specifically e-mail can come across as being extremely abrupt. Even phrases like “thank you” “have a great time” and even “I love you” can seem quite straight and lifeless when placed in a standard font on a computer screen. It is essential to avoid this abruptness in a customer service e-mail. Picking your words carefully is essential, avoiding jargon is fundamental, and it must be remembered that brevity in what you say should be limited to simply saying things in the simplest way. Abbreviations are not for this kind of e-mail.</p>

                <p>When we speak out loud, our words have an inflection, they are absorbed by the listener, and then we move on. In an e-mail, it stays there on the page and can be read into a number of different ways. It is essential to avoid saying things that are ambiguous, as this can lead to a complaint some way down the line if misinterpreted. Remember that in person if you say something, the listener can then respond instantly before you move on to your next point. This means that if something you said was unclear, they can seek clarification before replying. In e-mail, this is not possible. Getting things said clearly and unambiguously - and ideally just once - is hugely important.</p>
                <p><a href="/customer-service-training-los-angeles.php">Instructor-led customer service classes in Los Angeles</a>.</p>

                <p>One of the benefits of e-mail is its promptness. Sending an e-mail to a friend, a customer, or a co-worker can be done very quickly, and will usually be read within a short time of being sent. This system means that, wherever our conversation partner is in the world, we can converse in real time without the need for a huge telephone bill. Partially due to this, we have a habit of sending e-mails in a very cursory manner, which can lead to them being sent with information missing. This leads to a phenomenon known as “electronic ping-pong”, with each party sending ten e-mails to each other to organize or clarify something that could have been handled in the space of two or three messages.</p>




            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Customer Service Lessons</h4>
                    <ul>
                        <li><a href="/customer-service/in-person-service.php">In-Person Customer Service</a></li>
                        <li><a href="/customer-service/phone-service.php">Phone Customer Service</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Customer Service training</h4>
                    <p>Through our network of local trainers we deliver onsite Customer Service classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Customer Service class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=25">Customer Service student reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>