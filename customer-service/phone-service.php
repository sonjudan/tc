<?php
$meta_title = "How To Provide Exceptional Face-to-Face Service | Training Connection";
$meta_description = "Techniques to deliver exceptional service over the phone. This and other topics are covered in our Customer Service training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content g-text-Ps">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                <li class="breadcrumb-item active" aria-current="page">Telephone Customer Service</li>
            </ol>
        </nav>

        <div class="page-intro mb-3">
            <div class=" intro-copy">
                <h1 data-aos="fade-up" >Giving Good Service over <br>the Phone</h1>
                <p class="post-meta-author"  data-aos="fade-up" >Author: Billy Gee (Communication and Customer Service expert)</p>

            </div>
        </div>

        <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

            <p>When you are talking to someone in person, body language makes up a large part (some would say more than half) of your message. But as soon as you pick up the phone, body language becomes irrelevant. The success of your interactions depends almost entirely on your tone of voice and your choice of words. Getting these things right isn’t easy, but with a little practice anyone can learn how to provide excellent customer service over the phone.</p>

            <div class="media-full">
                <img src="/dist/images/placeholders/phone-service.jpg" alt="Giving Good Service over the Phone">
            </div>

            <h4>1. The Advantages and Disadvantages of Telephone Communication</h4>

            <p>As has previously been mentioned, many companies place their customer service issues in the hands of a dedicated department who can only be contacted in a non-personal manner. Some companies do this by using e-mail and others by way of the mail. In most cases, however, a customer service department will do the bulk of their work over the phone, and will have a dedicated call center for this purpose. While this removes the personal element from customer service to a large extent, it would be inaccurate to claim that there is not an advantage to doing things this way. Quite apart from anything else, it does permit some thinking time that you might not get in person.</p>

            <p>For more details on <a href="/customer-service-training.php">our customer service training workshops</a> call us on (888) 815-0604.</p>

            <p>The main disadvantage to this way of doing things is that there is no allowance made for the fact that people are almost always more reassured by speaking to someone in person. The body language we use when trying to transmit reassurance and confidence relies on customers being able to see us. If they relate a problem to you, and you are silent while they do so (for obvious reasons of manners and courtesy), they may understandably wonder if you are actually listening. The only way to avoid this becoming a problem is to be as reassuring in your tone of voice as you can. Make clear to the customer that you understand the severity of the situation, and that you will do all you can to solve it.</p>

            <p>Even with this level of understanding there are some customers who will feel that things will take longer to get solved over the phone, and the moment they hang up their problem will be forgotten about. This is why you must explain to them at each stage of the process what you are doing; why you are doing it and what will happen next. Solving the problem in the course of one call may be impossible, and you may have to promise them that they will be called back. Some companies make promises like this and fail to follow through on them. This can lead to a loss of confidence in all companies who make promises and a resultant pressure on those who are good at problem solving. Solving problems over the phone takes dedication and perseverance. Doing the job well and promptly will pay dividends.</p>

            <h4>2. Telephone Etiquette</h4>

            <p>Customers expect a courteous, helpful response when they call your business. Reviewing the basics of telephone etiquette can remind you about what it takes to provide the kind of response that customers expect.</p>

            <p>Telephone etiquette</p>

            <ul>
                <li>Answer promptly, on the third ring at the latest.</li>
                <li>Before you pick up the phone, end any conversation you are having.</li>
                <li>Greet the caller, identify yourself, and ask if you can help.</li>
                <li>Speak clearly in a pleasant tone of voice. Avoid speaking too quickly. (For discussion: what message do you send if you speak too quickly?)</li>
                <li>Give the caller time to explain the reason for the call. Don’t interrupt. Don’t sound like you’re in a hurry.</li>
                <li>When you need to put someone on hold, ask first: “Can I put you on hold for just a minute?” After you return to the line, thank the customer for holding.</li>
                <li>If you need to transfer a call, explain what you’re doing.</li>
                <li>When you end a call, let the customer hang up first. This will ensure that you don’t cut the customer off prematurely.</li>
            </ul>

            <p>Telephone etiquette is a major issue in any company that conducts a lot of its business and its customer service over the phone. The main factors in telephone etiquette are, as the words suggest, manners and efficiency. Because most of our conversations are carried out in person, a lot of people feel quite ill-at-ease when speaking on the phone. Things that they would normally be able to rely on for reassurance, like eye contact and body language, are not easily translated to a phone conversation. It means that dealing with urgent business on the phone can be something of a minefield. <a href="/customer-service-training-chicago.php">Chicago-based customer service course</a>.</p>

            <p>If you are required to provide a lot of your customer services over the phone, it is essential that you pay attention to your telephone manner. If you are receiving a call, this will mean responding promptly, and making your opening greeting courteous and warm. Rather than simply saying “Hello?” or saying the name of your company, you should state the name of the company, your name, and ask how you can help. No matter how many times you have done this, do not race through it and make it sound robotic - it may be commonplace to you, but to the customer this is an important matter.</p>

            <p>The fact that the customer cannot see you when you are on the phone does not mean that you can do whatever you like while you are speaking to them. If you are reading something or waving to a friend while on the telephone, it will be clear from your voice that your full attention is not on the call. If, for any reason, you have to speak to someone in the office, first ask the customer to bear with you, and then place them on hold. It is insulting to leave the customer hanging as though they are less important than what you are doing, and this is compounded by leaving them to hear what you are saying to someone else - the impression given is “it doesn’t matter, it’s only a customer”. Because the customer cannot see what you are doing, it is important to keep them posted on what you are doing. If you do not, they will become agitated.</p>
            <p><a href="/customer-service-training-los-angeles.php">LA customer service training</a>.</p>

            <h4>3. Tips and Tricks</h4>

            <p>For some people, dealing with issues over the phone will never be as beneficial as doing it in person. There are many advantages to using the telephone, and in many cases this has led to over-reliance on the system. This in turn has led to us almost developing a specific form of language when we are on the phone, all the more so in a business setting. Remember though that a customer may not be party to this language, and that they will have difficulty keeping up with the conversation if you are not careful to keep their needs in mind. Below are some hints to make your telephone etiquette in a business setting as good as it possibly can be.</p>

            <ul>
                <li>Answer the phone by saying “hello” or “good morning.” Often callers don’t hear the first thing you say. If the first thing you say is your name, some people might not catch it.</li>
                <li>Smile. Yes, of course, the customer can’t see you, but smiling gives your voice a more cheerful, enthusiastic tone. It also reminds you to be upbeat and positive.</li>
                <li>Sound enthusiastic. Try to maintain a positive attitude and let that come through in your voice.</li>
                <li>Say your name and your phone number clearly. Because people say these things often, they tend to slur them. But these are the things that you need to say as clearly as possible!</li>
                <li>Avoid company jargon (such as acronyms).</li>
                <li>Avoid technical terminology as much as possible. Some people may feel reluctant to ask you to explain a technical term because they don’t want to seem like dummies.</li>
                <li>Stay alert to how the customer is responding. Does the customer sound confused, skeptical, unsatisfied?</li>
                <li>Don’t use a speaker phone unless you’re having a conference call. When you use a speaker phone, callers get the impression that you’re too busy (or you consider yourself too important) to give them your full attention.</li>
                <li>Say good-bye. Don’t end a call abruptly. You will sound like you’re trying to get rid of the customer.</li>
                <li>If you need to make notes about the call, do that right away. Don’t rely on your memory to reproduce information accurately.</li>
            </ul>

        </div>


        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>

    </div>
</main>
<div class="mb-5">&nbsp;</div>

    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Customer Service Lessons</h4>
                    <ul>
                        <li><a href="/customer-service/in-person-service.php">Face-to-face Customer Service</a></li>
                        <li><a href="/customer-service/online-service.php">Electronic Customer Service</a></li>
                    </ul>
                </div>
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Customer Service training</h4>
                    <p>Through our network of local trainers we deliver onsite Customer Service classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Customer Service class</a>.</p>

                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=25">Customer Service student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>