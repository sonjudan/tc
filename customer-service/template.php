<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/header.php'; ?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Title</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Title</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Customer Service Lessons</h4>
                    <ul>
                        <li><a href="/customer-service/phone-service.php">Customer Service over the Phone</a></li>
                        <li><a href="/customer-service/online-service.php">Customer Service via Email</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Customer Service training</h4>
                    <p>Through our network of local trainers we deliver onsite Customer Service classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Customer Service class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=25">Customer Service student reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>