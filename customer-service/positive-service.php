<?php
$meta_title = "3 Ways To Start Positive In Customer Service | Training Connection";
$meta_description = "This article discusses 3 different ways to start off positive in Customer Service.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Positive Service</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>3 Ways To Start Positive In Customer Service</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>When a customer calls about their experience with your business, it is more likely that it is to fix a problem than to compliment you on your service. We are all used to hearing “The Customer is Always Right.” Still, the point of this saying should not be misunderstood. In my classes, I help service agents understand how even if the customer is not always right- they are always important. How important you make your customers feel can be the difference between a viral complaint and a loyal following. </p>

                <p>For <a href="/customer-service-training.php">educational instructor-led Customer Service classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/positive-service.jpg" width="780" height="464" alt="Customer Service Classes"></p>
                <p><a href="http://clientsfromhell.net/post/92044483828">Clients From Hell: A Collection of Anonymously Contributed Client Horror Stories From Designers</a></p>
                <p>Winston Churchill once said, “<strong>Tact</strong> is the ability to tell someone to go to hell in such a way that they look forward to the trip." </p>
                <p>When you  encounter a misinterpretation of your service, product or terms, how do you gently start to correct a misunderstanding? There is a process to good diplomacy and dispute resolution. In this article, we will cover 3 ways to begin resolving requests by starting positive! </p>
                <p>When the agent corrects the customer like this: </p>
                <p>“Ms. Peters, you made this order incorrectly so now, the only thing you can do is..”</p>
                <p>First off, Ms. Peters was given no credit for anything she did right. Second, she hears no reason or policy that confirms what right looks like so she may claim, “But I didn’t know!” and/or feel still justified. </p>
                <p>Third, when she is told there is no other option, if she believes she is  a good person, she will look for an exception or someone who might show her more grace. In Dale Carnegie’s book, <a href="https://books.google.com/books/about/How_To_Win_Friends_and_Influence_People.html?id=1rW-QpIAs8UC&amp;redir_esc=y">How to Win Friends and Influence People</a>, he describes that even a villain will see his or herself in a justified way when being corrected.</p>
                <p><a href="/customer-service-training-chicago.php">Customer Service onsite training in Chicago</a>.</p>

                <p>Instead of waiting for your turn to tell your customer what they did wrong or label your corrective news as good and bad, focus on building rapport with your customer first so that when you educate your customer about your policy, what your system reads or how typically a scenario happens, the customer is in a more cooperative state to resolve and look at suggested solutions. </p>

                <p><strong>Negative start:</strong></p>

                <p>"Sorry, you are mistaken. This does not work like that. You didn't..."</p>
                <p><strong>Positive start: </strong></p>
                <p>"Thanks for bringing this to my attention. I'm happy to help you with this. (Our system shows this and we see this from your side... here’s what we can do..")</p>
                <p>Which statement makes you feel validated and which statement puts you in a defensive mode?</p>

                <p><strong>Start Positive!</strong></p>
                <p>Here are 3 great ways to start positive before educating your customer on how you can best help. </p>

                <ol>
                    <li>Compliment</li>
                    <li>Show Understanding</li>
                    <li>Show Willingness to Help</li>
                </ol>

                <h4>Compliment</h4>
                <p>“Compliment? But the customer did nothing right! It is past the 30 days for return, the product is damaged and the item did not come from this store so we cannot honor this refund!”
                </p>
                <p><a href="/customer-service-training-los-angeles.php">Looking for customer service training in LA</a>?</p>
                <p>Compliments are the easiest to give but they must be sincere. You can compliment someone on their tenacity, intelligent questions or even for bringing this challenge to your attention. You can compliment someone for their name, their appearance, or their handwriting. Compliment the fact that this person is your customer! It’s not that hard! Just be sincere. </p>
                <p>Examples of compliments</p>
                <ul>
                    <li>Your question is very important.</li>
                    <li>Great to see you again!</li>
                    <li>Thanks for taking the time...</li>
                    <li>Lovely name!</li>
                    <li>We appreciate your business with us for several years</li>
                    <li>That’s a good observation.</li>
                    <li>You are very important to us!</li>
                    <li>I am glad you brought this to our attention.</li>
                    <li>Good point/Good question.</li>
                    <li>You make a valid point.</li>
                    <li>Brilliant suggestion!</li>
                    <li>We appreciate this feedback as we continue to improve!</li>
                    <li>First of all, you have great taste!</li>
                    <li>I so appreciate your patience.</li>
                    <li>You are right about (unrelated point)</li>
                </ul>

                <p>Just remember any compliment you cannot sincerely mean will affect its effectiveness so use what applies and avoid saying the same compliment to everybody or every time you are about to give correction because the compliment will lose its charm! For more tips on your ability to use compliments, I highly recommend, “How to Make Friends and Influence People” by Dale Carnegie.</p>
                <h4>Show Understanding</h4>
                <p>“But the client does not want to pay her bill! It is late and there are extra charges and service will stop since the payment is beyond the due date and the payment plan was not heeded. How can I show understanding when they have clearly ignored the terms of their part as customer?”
                </p>
                <p>While living in Japan, I learned why Japanese culture is considered one of the <a href="http://www.japantimes.co.jp/community/2011/12/24/our-lives/politeness-beyond-words/#.WBvQrNygNRk">most polite</a> cultures of the planet. To show thanks, they may even use the word <em>sumimasen</em> which is actually the same word to express ‘excuse me.’ When leaving work before one’s peers, you say “<a href="https://www.flickr.com/photos/anjin/355543231/?ytcheck=1"><em>osakini shitsureishimasu</em></a>" which means "it is rude of me to go ahead of you." There are so many examples of this politeness but nothing shows you the extra effort they will make to promote harmony more than how they avoid saying the word “No, you are wrong.” In Japanese, I might gently grunt “ie” or simply smile or at least pause to think about what someone said for a couple of extra seconds so I can say, “Wakatta” which just means the very neutral response, “I see.” So in essence, I have not said a direct no and I am not being disagreeable because I thought deeply about it and finally, I showed that I understood and only after that would I give the bad news. How extra considerate could one be! “I see” is a neutral acknowledgement that does not condemn or endorse a statement that is said. Being understood alone gives a positive impression on how you care about the customer’s request.</p>
                <p>Examples of showing understanding:</p>
                <ul>
                    <li>I see. </li>
                    <li>I understand.</li>
                    <li>I can see how this will affect your experience.</li>
                    <li>I respect your feedback on this.</li>
                    <li>I understand (time/cost/quality) is important to you.</li>
                    <li>I realize this is critical for your business.</li>
                    <li>I see this is urgent.</li>
                    <li>I know you are eager to get this in time.</li>
                    <li>I can certainly appreciate this amount is challenging.</li>
                    <li>I know this is important for you. </li>
                    <li>I realize this is a lot to handle at once.</li>
                    <li>I understand your concern.</li>
                </ul>
                <p>The less you say- for example, just saying “I see” can be very powerful in showing that balance you have about the matter. When you state this, use intonation that shows you care and that you are objective. Avoid making “mmmhmmmm” sounds or saying “okay” thinking you are showing understanding because these cues are casual and commonly used when a person is waiting for their turn to speak because they have heard all those same excuses before. You can show extra respect by saying, “Yes, I see” or saying “I understand.” </p>
                <h4>Show Willingness to Help!</h4>
                <p>“He does not get it! This customer keeps pressing me and does not let me look up the information I need to resolve the issue. Aren’t I already showing willingness to help because I said I would see what I could do?”</p>
                <p>Imagine you are playing volleyball and the game is getting close. You hear a weak voice behind you, “I got it…” then you hear a voice to your left that is strong, “I GOT IT!” Who are you going to pass the ball to? When someone has a challenge or is in crisis, he or she wants to find someone to intercept who is confident he or she can do the job and not place them in a state of suspense. A suspense state makes me wonder, “Can she do it? Will he help me?” When an agent says, “Let’s see..." “I’m not sure but I’ll check...” “I’ll try/I’m trying to do (this) for you,” it conveys a level of powerlessness and even some incompetence so a customer may press for someone with more authority to ensure they get the matter handled as best as possible.</p>
                <p>To show your willingness to help begins with confidently stating, “I can help you.” “I can help you” is not a guarantee of doing whatever is asked but it is stated upfront to assure that whatever the request, the customer has reached a confident, competent and willing party to intercede for them. </p>
                <p>Even if the customer has the most irrational request, for example, “Change the system/Let me speak to the president now/Give it to me free!” By starting with “I can definitely help you with your question/request” gives them a good sense you are working on their behalf even for something they do not qualify for. Of course, by getting them in a less defensive state, you can later qualify them for the criteria they would have to meet to get any unreasonable request met and before giving them options, you can educate the policy or terms that apply. You are still helping them see what their options are. You definitely can help!</p>
                <p>Examples of showing willingness to help appear like this:</p>
                <ul>
                    <li>I am happy to help you with this.</li>
                    <li>I want to make sure we thoroughly investigate this for you.</li>
                    <li>I want to make sure you have all the information you need to choose the next steps.</li>
                    <li>I am sure we can help you with this request. </li>
                    <li>I want to make sure we get this corrected in time.</li>
                    <li>I want to make sure this is done in full compliance with our policies so that you can get this done right.</li>
                    <li>I can definitely assist you with your question!</li>
                    <li>I am confident we can fix this for you. </li>
                    <li>I can take care of you from here on out.</li>
                    <li>I will make sure we get to the bottom of this so you can see your best options moving forward.</li>
                    <li>I want to go to bat for you on what we can do.</li>
                </ul>
                <p>These 3 ways to start positive are just the beginning of resolving your customer’s need to feel important and advocated for as a valued customer. These starter statements are buffers you can use anytime you hear something contrary. “I appreciate your thoughts on that.” “I respect your candor.” “I know you mean well.” When you add two from each category, you have tapped into Winston Churchill charm in diplomacy and you will notice that it creates a better relationship than telling someone how wrong they are. In the case that an issue is the company’s fault, they can see that you gave them positive credit at the outset of the investigation. Start positive and no matter the outcome, you can ensure a positive impression of how you care about your customers.</p>


            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Customer Service Lessons</h4>
                    <ul>
                        <li><a href="/customer-service/physical-fatigue.php">How to Reduce Physical Fatigue In Customer Service</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Need Onsite Customer Service training?</h4>
                    <p>Have a group of customer service people who are low in morale? We can provide an <a href="/customer-service-training.php">onsite Customer Service class</a> at your offices. <a href="/onsite-training.php">Obtain a quotation</a> today! Review our <a href="/testimonials.php?course_id=25">past student testimonials</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>