<?php
$meta_title = "5 Words Customer don't want to hear | Training Connection";
$meta_description = "This article discusses 5 words customer service agents should avoid when dealing with Customers.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/customer-service.php">Customer Service</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Words</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>5 Words Customers Do Not Want to Hear</h1>
                        <p class="post-meta-author" >Author: Billy Gee (Communication and Customer Service expert)</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>A customer can start out angry or can start calm and later become angry yet when you interface either in person or by phone, your role as a customer service agent is to provide solutions and ameliorate relations. Often, this is lost in translation when we use words that put a customer in a defensive state. </p>
                <p>For <a href="/customer-service-training.php">highly interactive and educational Customer Service classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><em>"Words are singularly the most powerful force available to humanity. We can choose to use this force constructively with words of encouragement, or destructively using words of despair. Words have energy and power with the ability to help, to heal, to hinder, to hurt, to harm, to humiliate and to humble."</em> - <strong>Yehuda Berg</strong></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/customer-service/words-to-avoid.jpg" width="780" height="407" alt="Words in avoid in good service"></p>
                <p>“What did I say?” you wonder. “She seemed completely happy and then everything turned upside-down.” What instigates this is not only the problem they need to resolve but words we use either make them feel important or less important. </p>
                <p><a href="https://www.psychologytoday.com/experts/andrew-newberg-md-and-mark-waldman">Andrew Newberg, M.D. and Mark Waldman</a>, authors of Words Can Change Your Brain, suggest if our brains were evaluated under an MRI scanner, flashing the word “<a href="https://www.psychologytoday.com/blog/words-can-change-your-brain/201208/the-most-dangerous-word-in-the-world">No</a>” for even less than a second can release dozens of stress producing hormones and transmitters. Even a list of negative words can make a highly anxious or depressed person feel worse. </p>
                <p>Giving bad news is necessary but  there is a formula to making bad news more palatable. Let’s take a look at common words that have an incendiary effect on customers and what we can do to say it better:</p>

                <h4>1. NO</h4>
                <p>When we are told no, cooperation is at a standstill. When something we want is declined, we can respond in one or all of three ways. </p>
                <ul>
                    <li>Become anxious</li>
                    <li>Double check why</li>
                    <li>Search for exceptions </li>
                </ul>
                <p>Your customer brings the issue to you because they believe you have the power to resolve it. Using the word ‘No’ will be necessary education yet here are two ways to start. <a href="/customer-service-training-chicago.php">Customer Service training offerings</a> in Chicago.</p>
                <p>First, educate your customer on your rule, standard, policy or how a service is typically delivered. </p>
                <p>
                    <em>Customer: “Do you have any BMWs?” </em> <br>
                    <em>Agent: We sell luxury cars Lexus and Infinity. BMW is a competitor.</em><br>
                    <em>Customer: I would like a refund.</em><br>
                    <em>Agent: I can help you with your request. We have a refund policy. Allow me to ask you some questions.</em>
                </p>

                <p>
                    <em>Customer: Do you serve breakfast at this hour?</em> <br>
                    <em>Agent: Breakfast is served 7-10am. I can double check if the kitchen has anything or I can recommend another item closer to your tastes for breakfast.</em>
                </p>
                <p>
                    <em>Customer: Can I park in the reserved parking spot?</em> <br>
                    <em>Agent: The reserved parking spots are designated spaces specifically forcompany employees. We recommend parking in...</em></p>
                <p>
                    <em>Customer: Can I smoke?</em> <br>
                    <em>Agent: Smoking is prohibited. We have designated space behind the facilities.</em>
                </p>
                <p>As you can see, there is a way to navigate any of these emotions by being kind, informative and offering options.</p>

                <h4>2. CANNOT</h4>

                <p>Any negative contraction such as “can’t,” “don’t” and “didn’t” is going to be confronted with defensiveness simply because you are denying ability, will and permission and there is something in most people that refuse to believe these limitations apply to them while they feel the blame is on another person. For example, “You can’t do that, sir.” Response, “But I didn’t know!”</p>
                <p>Instead, before you tell the person what they cannot do, explain the rule or what customers can do first!</p>

                <p>
                    <em>Agent: You didn’t send us notice in advance so we can’t refund the money.</em><br>
                    <em>CORRECTED:</em><br>
                    <em>Agent: We require notice in advance. It is now beyond the 30 day return policy date. We can give you a credit or you can make an exchange.</em></p>
                <p>
                    <em>Customer: I would like to add a new specification.</em><br>
                    <em>Agent: We can review that. The specifications are already confirmed so this may increase the price and/or the delivery. Let me know what you would like to do.</em>
                </p>

                <p>It is important to focus on showing the customer you care, you know how the business works and you can tell them what is possible. If they push back, always start with the rule and then you can say the negative.</p>

                <p>
                    <em>Customer: Can you get this to us by tomorrow morning?</em>
                </p>
                <p>
                    <em>Agent: The delivery is en route and we can give you tracking at this point. We can also order another with express delivery to arrive soonest by 6am. </em></p>
                <p>
                    <em>Customer: So you can’t get our current order to us by the early morning?</em>
                </p>
                <p>
                    <em>Agent: The delivery is en route so we cannot change the shipping speed. We can order another to arrive with express delivery to arrive soonest by 6am at an additional cost. You can also return the one that arrives late for a refund.</em>
                </p>
                <p>Los Angeles <a href="/customer-service-training-los-angeles.php">classes on how to improve customer service</a>.</p>

                <h4>3. ONLY</h4>
                <p>When we say “only,” it is merely a show of our experience yet it does not show helpfulness and gives the customer a feeling they have lost all special exception, control and value. Instead, we want to the only option in possibilities.</p>
                <p>
                    <em>Agent: The only thing you can do...</em><br>
                    <em>CORRECTED:</em><br>
                    <em>Agent: In this case, you can pay cash, check or credit for the amount.” </em><br>
                    <em>Agent: You can expect the item to arrive within 7-10 days via ground shipping. </em><br>
                    <em>Agent: I will make a note of this feedback for future improvements.</em><br>
                    <em>Agent: You can double check with us next time. </em><br>
                    <em>Agent: You can feel free to read our policy or call in to confirm any concerns in advance.</em>
                </p>
                <p>Read more about the <a href="/customer-service/benefits-of-customer-service-training.php">benefits of Customer Service training</a>.</p>

                <h4>4. FAULT</h4>
                <p>Should it slip out of our mouth the word “fault” when discussing the responsibility of some error going wrong. We may be shifting the conversation from solution to guilty charge. In the case that it is the fault of the business, apologize quickly and emphatically and discuss only what is being done to correct this. If it is the error of the customer, focus only on what the customer can do moving forward. We are all defensive when being corrected but business can be very much like a ball game. If you keep it moving and no one gets hurt, you can appreciate doing business again. </p>

                <p>
                    <em>Agent: It was your fault so you will have to...</em><br>
                    <em>CORRECT</em>ED:<br>
                    <em>Agent: This is what happened so here’s what we can do...</em><br>
                    <em>Agent: This is the policy and given wegot your return outside of the 30 day refund window, here’s what we can do...</em>
                </p>
                <h4>5. THEY</h4>
                <p>This word seems super innocent and almost unrelated to all the words we listed so far. The pronoun “They” is actually an attempt to blame parties not present to defend themselves. This is something an agent might say when giving an explanation of how business is done or what mistake happened to help the customer understand. Blaming one’s own team members may make you look good but it sinks the ship you are standing in. Don’t be that guy! </p>
                <p>Instead of blaming sales, management or an employee who did something, take full representation of your company’s desire to rectify the matter and take care of your customer.</p>
                <p><em>
                        Customer: The sales people told me that this would be done much sooner!</em>
                    <em>
                        Agent: I see. I want to make sure we take care of this for you as soon as possible. Our turn around is typically 48hrs to ensure we follow through with compliance. We can end you status updates or we can give you a call about any sooner estimations you can expect.</em></p>
                <p><em>Customer: Tim was unreasonable. He would not give us what we wanted!</em>
                    <em>
                        Agent: We appreciate your feedback on this. I will take care of you from here on out.</em></p>
                <p><em>
                        Customer: This always happens. Why hasn’t management made these changes yet? </em>
                    <em>
                        Agent: Your feedback is valuable to us and I can assure that we have many plans underway to address our best delivery on a daily basis. Thank you so much for sharing this and we will continue to update feedback as we improve.</em></p>
                <p>The bottom line to removing negative language is to stop, think, and then answer. Consider the rule or policy or possibility that applies to inform the customer. Customers do respond to:</p>
                <ul>
                    <li>Yes, I can help!</li>
                    <li>I see. Our policy is...so here’s what we can do...</li>
                    <li>Allow me to take care of this for you</li>
                    <li>Thank you for your patience</li>
                    <li>What we can do...</li>
                    <li> We appreciate your expertise on this</li>
                    <li>We understand this is urgent</li>
                    <li>We want to make sure we provide you the best options</li>
                    <li>I am glad to help you with this</li>
                </ul>

                <p>Read about dealing with <a href="/customer-service/physical-fatigue.php">physical fatigue in customer service</a>.</p>

            </div>

            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-Ps" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Need Onsite Customer Service training?</h4>
                    <p>Have a group of customer service people who are low in morale? We can provide an <a href="/customer-service-training.php">onsite Customer Service class</a> at your offices. <a href="/onsite-training.php">Obtain a training quotation</a> today! Review our <a href="/testimonials.php?course_id=25">past student testimonials</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>