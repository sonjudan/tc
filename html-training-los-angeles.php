<?php
$meta_title = "HTML Training Classes | Chicago & Los Angeles | Training Connection";
$meta_description = "Need to learn HTML? Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">HTML Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-web" style="background-image: url('/dist/images/banner-html.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">HTML Training</h1>

                    <div data-aos="fade-up">
                        <h4>Los Angeles</h4>
                        <p>Looking for an HTML training class in Los Angeles?</p>
                        <p>We have been teaching hands-on HTML classes for the last 12 years. <br>
                            Our class sizes are small averaging at around 2-4 trainees, so you will receive a lot of individual attention.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">

                <div class="mb-4 mt-md-4 float-md-right"  data-aos="fade-up">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>

                <h3>What's Included</h3>
                <ul>
                    <li>HTML training manual</li>
                    <li>Certificate of Course Completion</li>
                    <li>Free repeat valid for 6 months (in case you need a refresher)</li>
                </ul>
                <p>
                    <strong>Book an HTML training course today. All classes guaranteed to run!</strong> <br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0" data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0"  data-aos="fade-up">
                <h2>HTML Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-html" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Understanding the HTML Interface"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    HTML5 and CSS Fundamentals
                                </a>
                            </h3>
                            <p>HTML and CSS Fundamentals is aimed at students new to Web design. On this 3 day hands on course you will learn to construct web pages by hand coding your HTML and then applying Cascading Style Sheets (CSS) to control the layout and appearance.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="HTML5 and CSS Fundamentals" data-price="$1495"
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/html-css/HTML%20and%20CSS%20Fundamentals.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="HTML5 and CSS Fundamentals">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1495
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Coding Basics: Intro to HTML Syntax</h5>
                                        <p>In this exercise, you will learn the basic syntax and the most essential HTML tags that are needed to get up and running.</p>
                                        <ul>
                                            <li>The HTML, Head, Title, &amp; Body Tags</li>
                                            <li>Headings, Paragraphs, &amp; Lists</li>
                                            <li>Strong &amp; Em Tags</li>
                                            <li>The Doctype Declaration (DTD)</li>
                                            <li>The Lang Attribute</li>
                                            <li>The Meta Tag &amp; the Unicode Character Set</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Coding Links</h5>
                                        <p>In this exercise, you will add some links to both  external websites and other pages on your site.</p>
                                        <ul>
                                            <li>Anchor Tags &amp; Hrefs</li>
                                            <li>Linking to Other Websites</li>
                                            <li>Linking to Pages Within a Website</li>
                                            <li>Opening a Link in a New Browser Window</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Adding Images</h5>
                                        <p>In this exercise, you will add images to your webpage. You will also learn two other simple tags: the  line break tag and another for creating the appearance of content division.</p>
                                        <ul>
                                            <li>The Break Tag</li>
                                            <li>The Image Tag &amp; Source Attribute</li>
                                            <li>Using the Width, Height, &amp; Alt Attributes</li>
                                            <li>Using Horizontal Rules</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Intro to Cascading Style Sheets (CSS)</h5>
                                        <p>In this exercise, you will style the page using Cascading Style Sheets (CSS). This is the first of many exercises on writing CSS. The focus of this exercise is on styling text (such as font, color, etc).</span> </p>
                                        <ul>
                                            <li>The Style Tag</li>
                                            <li>Tag Selectors</li>
                                            <li>Class Selectors</li>
                                            <li>Font-Size, Font-Family, Color, &amp; Line-Height Properties</li>
                                            <li>Hexadecimal Shorthand</li>
                                            <li>The Span Tag</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Div Tag &amp; Basic Page Formatting</h5>
                                        <p>In order to have full control over real page layout, another HTML element which comes in  handy is the &lt;div&gt; tag. Wrapping content in &lt;div&gt; tags allows authors to create sections of content that are grouped together and styled via CSS rules.</p>
                                        <ul>
                                            <li>Creating Divisions with the Div Tag</li>
                                            <li>Setting a Div Width</li>
                                            <li>CSS Background-Color</li>
                                            <li>Adding Padding Inside a Div</li>
                                            <li>Centering Content</li>
                                            <li>CSS Borders</li>
                                            <li>CSS Shorthand &amp; the DRY Principle</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Using Browser Developer Tools</h5>
                                        <p>In this exercise, we will introduce you to browser developer tools. All major browsers have built-in tools that allow you to access the code with precision. These tools allow you to review and edit the HTML and CSS with great  ease.</p>
                                        <ul>
                                            <li>Opening the DevTools in Chrome</li>
                                            <li>Editing HTML in the DevTools Elements Tab</li>
                                            <li>Enabling, Disabling, &amp; Editing CSS in the DevTools</li>
                                            <li>Using DevTools to Fine Tune Your CSS</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">HTML5 Semantic Elements</h5>
                                        <p>In the latest version of HTML, HTML5, we can make use of semantic tags to natively describe the contextual function of the element. This has several benefits over the &lt;div&gt; tag.</p>
                                        <ul>
                                            <li>The Outline Algorithm</li>
                                            <li>The Header, Nav, Aside, &amp; Footer Elements</li>
                                            <li>Understanding Articles &amp; Sections</li>
                                            <li>The Main Element</li>
                                            <li>The Figure &amp; Figcaption Elements</li>
                                            <li>Validation</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Fluid Layout &amp; Max-Width</h5>
                                        <p>In this exercise, we will investigate how the &lt;div&gt; tag can and should still be used to group and style content non-semantically. We will also start to think about how to make our layouts fluid, so they adapt to different-sized browser windows.</p>
                                        <ul>
                                            <li>Making Images Fluid</li>
                                            <li>Divs for Presentational Style</li>
                                            <li>Assigning IDs to Divs</li>
                                            <li>Assigning Max-Width to Content</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Revolution Travel: Real World Layout</h5>
                                        <p>This is the first in a series of exercises in which you will lay out a complete website. You will  begin by laying out the basics of a single page. You will  build the structure of the page and place content inside each section.</p>
                                        <ul>
                                            <li>Organizing Content into Semantic Sections</li>
                                            <li>Adding Images</li>
                                            <li>Tagging Headings</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Box Model</h5>
                                        <p>In this exercise, we will explore this CSS Box Model to see how the width, padding, and margin properties can allow us to control the page layout.</p>
                                        <ul>
                                            <li>What is the Box Model?</li>
                                            <li>Setting Div Width</li>
                                            <li>Fixing a Display Issue in Internet Explorer</li>
                                            <li>Setting Page Defaults for Font Styles</li>
                                            <li>Padding &amp; Margin Spacing</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Floats &amp; Images</h5>
                                        <p>In this exercise, you will  add some images and also learn how to position images alongside text by using the float property. Additionally, you will learn  the usefulness of the class selector.</p>
                                        <ul>
                                            <li>Adding a Hero Image</li>
                                            <li>Fluid Images</li>
                                            <li>Floating Images</li>
                                            <li>Class Selectors</li>
                                            <li>Margins</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Coding Links: Images, Email, &amp; Named Anchors</h5>
                                        <p>This exercise is a refresher  on how to link to other pages within your site and how to code external links that open in a new browser tab or window.</p>
                                        <ul>
                                            <li>Anchor Tags &amp; Relative URLs</li>
                                            <li>Wrapping Links Around Images</li>
                                            <li>External Links (Using the Target Attribute)</li>
                                            <li>Spambot-Resistant Email Links</li>
                                            <li>Links Within a Page</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Styling Links</h5>
                                        <p>This exercise will show you how to create your own link styles and customize different link styles for different sections of the page.</p>
                                        <ul>
                                            <li>Styling the Anchor Tag</li>
                                            <li>The :link, :visited, :active, &amp; :hover Pseudo-Classes</li>
                                            <li>Love/Hate: Ordering Link Styles</li>
                                            <br>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Styling the Navigation</h5>
                                        <p>This exercise will teach you how to  style a  navigation bar.</p>
                                        <ul>
                                            <li>Semantically Correct Navigation</li>
                                            <li>Overriding Default List Styles</li>
                                            <li>CSS Navigation Styles</li>
                                            <li>Using Descendant Selectors</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Shared CSS &amp; Centering Content</h5>
                                        <p>In this lesson, you will learn to store the CSS in an external file, which  can be applied to multiple web pages. This will result in consistent styles across a website and facilitate easy site-wide changes.</p>
                                        <ul>
                                            <li>Moving Embedded Styles into an External CSS File</li>
                                            <li>Sharing Styles Across a Site</li>
                                            <li>The Text-Align Property</li>
                                            <li>Centering Divs</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Setting the Viewport Meta Tag</h5>
                                        <p>In this exercise, you will add viewpoint meta tag controls that control how a webpage will  display on a mobile device.</p>
                                        <ul>
                                            <li>The Viewport Meta Tag</li>
                                            <li>Device-Width</li>
                                            <li>Initial-Scale</li>
                                            <li>Maximum-Scale</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">HTML &amp; CSS Wireframe</h5>
                                        <p>This is the first in a series of exercises where you will  build a mini-site to announce a product launch. In this exercise you will setup a wireframe to visualize how the content will be presented.</p>
                                        <ul>
                                            <li>Stacking Sections</li>
                                            <li>Linking to an External Stylesheet</li>
                                            <li>Using DevTools to Unpack the Box Model</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Background Images</h5>
                                        <p>In this exercise, you will build the homepage of our product launch website. You will overlay a header on a background image using the CSS background-image property.</p>
                                        <ul>
                                            <li>HTML Images vs CSS Background Images</li>
                                            <li>Background-Position</li>
                                            <li>Background-Repeat</li>
                                            <li>Background-Size</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Fun with Fonts</h5>
                                        <p>In this exercise, you will enhance the look of your website by embedding custom fonts rather than using  the  bland  fonts that ship with most operating systems. You will use  Google Fonts, the industry leader in free web fonts.</p>
                                        <ul>
                                            <li>How to Use Google Fonts</li>
                                            <li>Safe Fallbacks in the Font Stack</li>
                                            <li>Improving Line-Height and Margin for Legibility</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Hipstirred Layout: Fine-Tuning with Box Model</h5>
                                        <p>In this exercise, you will refine the spacing and dimensions of the layout for the mini site and revisit the handy CSS max-width property. We will  investigate wrapping content in container divs and firm up our understanding of how an ID selector differs from a class selector.</p>
                                        <ul>
                                            <li>Removing Default Page Margin</li>
                                            <li>Setting a Max-Width</li>
                                            <li>Outer and Inner Wrappers</li>
                                            <li>The Difference Between ID and Class Selectors</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Buttons &amp; Floats</h5>
                                        <p>In this exercise, you will learn CSS border-radius for rounding corners, how to create a handy, reusable button class, and how to make a more complex layout by using floats to pull elements to the far left or far right of the layout.</p>
                                        <ul>
                                            <li>Floats for Layout</li>
                                            <li>Float Insert Position</li>
                                            <li>Simple CSS Buttons</li>
                                            <li>CSS Border-Radius</li>
                                            <li>Reusing Class Selectors</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Hipstirred: Hi-Res Images</h5>
                                        <p>In this exercise, you will add high resolution images which display better on smaller mobile devices.</p>
                                        <ul>
                                            <li>Retina or HiDPI Graphics (@2x Images)</li>
                                            <li>Setting HTML &amp; CSS Size to Half the Image's Native Size</li>
                                            <li>Hardware Pixels vs Reference Pixels</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Uploading to a Live Website via FTP</h5>
                                        <p>In this exercise, you will use FTP (File Transfer Protocol) to upload a website to a web host's remote server so you can see how to take the work you have done on your local computer and actually put it live on the web.</p>
                                        <ul>
                                            <li>What is FTP?</li>
                                            <li>Using an FTP Client</li>
                                            <li>Going Live</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Form Basics</h5>
                                        <p>Forms allow you to collect information about your visitors so you can better serve their needs. In this exercise, you will learn to code and style a basic form to collect a user's name and email.</p>
                                        <ul>
                                            <li>The Form Tag</li>
                                            <li>The Input &amp; Label Elements</li>
                                            <li>Name &amp; ID Attributes</li>
                                            <li>The Button Element</li>
                                            <li>Styling the Form</li></ul></ul>
                                        <ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Understanding the HTML Interface"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-outline-2">
                                    HTML5 and CSS Advanced
                                </a>
                            </h3>
                            <p>In this advanced course you will take your CSS knowledge to the next level. You learn to create complex page layouts as well as add new interactive features available in CSS3.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="HTML5 and CSS Advanced" data-price="$1495"
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/html-css/HTML%20and%20CSS%20Advanced.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="HTML5 and CSS Advanced">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-outline-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-outline-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1495
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-outline-2">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Box Model</h5>
                                        <p>This is the first in a series of three exercises in which you will style a simple blog using foundational CSS styling techniques.</p>
                                        <ul>
                                            <li>Adding the Viewport Meta Tag</li>
                                            <li>Using normalize.css</li>
                                            <li>Linking our CSS File and setting page defaults</li>
                                            <li>Scaling down hi-res images to fit the browser width</li>
                                            <li>Constraining the width of content</li>
                                            <li>The box model: adding padding, margins, &amp; borders</li>
                                            <li>Visualizing the box model in Chrome’s DevTools</li>
                                            <li>Fixing spacing issues around images</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Background Images and CSS Shorthand</h5>
                                        <p>In this exercise we will take a deep dive into CSS background images and learn how to streamline the CSS with shorthand.</p>
                                        <ul>
                                            <li>Modifying the H1 Text Style</li>
                                            <li>Adding a background image to the h1</li>
                                            <li>Using shorthand for the Background Property
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Redefining CSS Typography</h5>
                                        <p>In this exercise we will refine the look of our basic blog by fine-tuning the typography. We will examine font-style, font-weight, and improve legibility with line-height.</p>
                                        <ul>
                                            <li>Differentiating Text with Font-Style</li>
                                            <li>Unitless Line-Height</li>
                                            <li>Grouping Selectors Using a Comma Separator</li>
                                            <li>Numeric Font-Weight: Beyond Normal &amp; Bold</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating Columns with Float</h5>
                                        <p>CSS lets you "float" content next to other elements. The float property specifies whether or not an element should float. The clear property is used to control the behavior of floating elements.</p>
                                        <ul>
                                            <li>Creating a 2-column layout with float</li>
                                            <li>Solution #1: using the clear property</li>
                                            <li>Adding a border between the columns</li>
                                            <li>Solution #2: setting the overflow property to hidden</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Introduction to Media Queries</h5>
                                        <p>In this exercise, we will introduce you to media queries which are useful overrides for alternative layouts such as phones. We will use media query to switch our design from a 1-column layout to a 2-column layout. </p>
                                        <ul>
                                            <li>Finding an appropriate breakpoint</li>
                                            <li>Anatomy of a media query</li>
                                            <li>Using a media query to create an alternate layout</li>
                                            <li>Max-width media queries</li>
                                            <li>Sizing down the headings on smaller screens</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Display Property: A Deep Dive</h5>
                                        <p>In this exercise, we will style the header and footer links using the elements' display properties making our pages totally responsive. You will  use inline-block as a hassle-free alternative to floats.</p>
                                        <ul>
                                            <li>Display types: block, inline, &amp; inline-block</li>
                                            <li>Styling the header &amp; footer navigation the DRY way</li>
                                            <li>Changing the display property to increase tappable area</li>
                                            <li>Displaying the navigation side-by-side on wider screens</li>
                                            <li>Using inline-block as an alternative to float</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Position Property: The Key to Complex Layouts</h5>
                                        <p>In this exercise we will explore the layout options that the position property affords you. The position property is your best defense against a boring layout. Mastering this property and getting to know its relative, absolute, and fixed values opens up a world of "outside the box" possibilities.</p>
                                        <ul>
                                            <li>The static value &amp; the normal document flow</li>
                                            <li>A nostalgic wanderer: the relative value</li>
                                            <li>The absolute value</li>
                                            <li>The dynamic duo: relative parent, absolute child</li>
                                            <li>The fixed value</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating a Fixed Header</h5>
                                        <p>In this exercise, we will utilize the position property to make a website header more interesting. In the layout for wider screens, we will use fixed positioning to make a fixed header.</p>
                                        <ul>
                                            <li>Moving the Contact list item to the navbar’s far right</li>
                                            <li>Creating a fixed header on wider screens</li>
                                            <li>Positioning a background image next to the FAQ</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Image Replacement</h5>
                                        <p>In this exercise, you will place a background image behind a heading, hiding the text, and create a fluid image container that scales well with all devices.</p>
                                        <ul>
                                            <li>Replacing HTML text with CSS background images</li>
                                            <li>Removing text with negative text-indent value</li>
                                            <li>Using overflow: hidden;</li>
                                            <li>Removing text with a zero height</li>
                                            <li>Creating a fluid image container by using proportional top padding</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Sprites</h5>
                                        <p>In this exercise, you will learn how to create a navbar using CSS sprites. The sprite technique allows you to create a search engine-friendly and accessible text-based navbar with the added luxury of using well-designed images.</p>
                                        <ul>
                                            <li>What are Sprites?</li>
                                            <li>Creating Sprites</li>
                                            <li>Image Replacement</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Background Gradients &amp; Transparent Colors</h5>
                                        <p>In this exercise, we will add content to the Tahoe Adventure Club page, including a background image. The image is a black &amp; white photo that we will colorize using semi-transparent background gradients.</p>
                                        <ul>
                                            <li>Specifying colors are RGB</li>
                                            <li>Adding transparency (alpha) to colored backgrounds with RGBA</li>
                                            <li>CSS background gradients</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Multi-Column Layout</h5>
                                        <p>In this exercise, we will continue adding to the Tahoe Adventure Club website. We will add three articles and investigate the ease of creating columns using inline-block instead of floats.</p>
                                        <ul>
                                            <li>3-column layout using inline-block</li>
                                            <li>Nested CSS Selectors</li><br>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced CSS Selectors</h5>
                                        <p>In this exercise, we will finish styling the Tahoe Adventure Club page. Instead of adding a bunch of classes or IDs to our markup, we are going to rely on the relationships between elements in order to target them cleanly and efficiently with CSS selectors.</p>
                                        <ul>
                                            <li>first-of-type</li>
                                            <li>first-child</li>
                                            <li>last-child</li>
                                            <li>last-of-type</li>
                                            <li>nth-child</li>
                                            <li>child selector</li>
                                            <li>:before and :after</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Attribute Selectors</h5>
                                        <p>In this exercise, we will make the links at the bottom of each article functional. We will focus on using attribute selectors to add nifty little icons next to each link to let users know what kind of resources they are linking to.</p>
                                        <ul>
                                            <li>Attribute selector syntax</li>
                                            <li>Caret (^) Operator</li>
                                            <li>Dollar ($) Operator</li>
                                            <li>Asterisk (*) Operator</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Form Styling</h5>
                                        <p>In this exercise, we will use attribute selectors to style forms, which is a more common and useful real-world scenario.</p>
                                        <ul>
                                            <li>Using an attribute selector to target inputs</li>
                                            <li>Overriding default form element styling</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Clearing Floats</h5>
                                        <p>In this exercise, we layout content for narrow screens, which converts to a  two column layout on large screens. We will run up against a new issue with clearing floats that will require us to learn how to use the CSS "clearfix" method.</p>
                                        <ul>
                                            <li>The CSS Clearfix class</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sizing Typography</h5>
                                        <p>In this exercise, we will look at how we can customize font sizes for different screen sizes. We will go over the difference between fixed pixel font sizes, em units, and rem units.</p>
                                        <ul>
                                            <li>Working with REM</li>
                                            <li>REM versus EM</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS3 Shadows</h5>
                                        <p>A well-placed drop shadow can add depth and dimension to any design. In this exercise, you will use thebox-shadow and text-shadow properties to elements on our page. You will also learn how to adjust an element's stacking order on the page using the z-index property.</p>
                                        <ul>
                                            <li>Box shadow</li>
                                            <li>Text shadow</li>
                                            <li>z-index</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Hiding &amp; Showing Elements</h5>
                                        <p>In this exercise, we will investigate various ways to hide and show elements: setting the display property to none, the visibility to hidden, and setting opacity to 0. Many of these settings are designed to make content accessible to the visually impaired.</p>
                                        <ul>
                                            <li>display: none;</li>
                                            <li>Visibility</li>
                                            <li>Opacity</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Transitions</h5>
                                        <p> In this exercise, we will add CSS3 transitions that are triggered by users. We will add two transitions: a basic fade-in that animates the color and text-shadow in the links in the header.</p>
                                        <ul>
                                            <li>Transition-Property</li>
                                            <li>Transition-Duration</li>
                                            <li>Transition Shorthand &amp; the Transition Stack</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Transforms</h5>
                                        <p>In this exercise, we will add 2D scale and translate transforms onto our preexisting transitions to make our page even more engaging to our users.</p>
                                        <ul>
                                            <li>The Scale Transform</li>
                                            <li>The Rotation Transform</li>
                                            <li>Transitioning Transforms</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">You are Here Indicator</h5>
                                        <p>In this exercise, we will work on a local city guide website that features travel info about a few U.S. cities. Our goal is to add an essential user experience feature: an indicator in the navigation that shows users the current page.</p>
                                        <ul>
                                            <li>Navigation page marker</li>
                                            <li>CSS Shapes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Specificity</h5>
                                        <p>In this exercise, we will demystify CSS' specificity hierarchy so you can override styles with confidence.</p>
                                        <ul>
                                            <li>Advanced CSS Specificity</li>
                                            <li>The Specificity Hierarchy</li>
                                            <li>How Attribute Selectors &amp; Pseudo-Classes Are Weighted</li>
                                            <li>Inline Styles: The Highest Priority</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Understanding the HTML Interface"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-3"  data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                    Mobile & Responsive Web Design
                                </a>
                            </h3>
                            <p>Responsive web design is a relatively new approach to website design that ensures users have a great viewing experience on any device. A fully responsive site will display well on a desktop, a tablet or mobile phone.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Mobile & Responsive Web Design" data-price="$1495"
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/html-css/Mobile%20&%20Responsive%20Web%20Design.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Mobile & Responsive Web Design">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-outline-3">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-3" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-outline-3">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1495
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-3" class="accordion-collapse collapse " aria-labelledby="heading-3" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-3"  data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-outline-3">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Flix: Setting the Viewport Meta Tag</h5>
                                        <p>This is the first in a series of exercises where you will work on a mobile website for Flix, a movie review website. In this exercise, you will learn how to set the very important viewport meta tag.</p>
                                        <ul>
                                            <li>The viewport meta tag</li>
                                            <li>device-width</li>
                                            <li>initial-scale</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Flix: SVG (Scalable Vector Graphics) & .htaccess Files</h5>
                                        <p>Most of today's mobile devices feature high resolution screens that are typically 2-3 times the pixel density of older screens. SVG (scalable vector graphics) work really well for graphics such as icons and logos.</p>
                                        <ul>
                                            <li>Adding SVG as an image</li>
                                            <li>Setting SVG width & height</li>
                                            <li>Configuring the web server’s .htaccess file for SVG</li>
                                            <li>Additional configuration with the .htaccess file
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Flix: Mobile-Friendly Forms</h5>
                                        <p>HTML5 has new input types and attributes that you can use in forms. In this exercise you will see how HTML 5 input types improve mobile usability.</p>
                                        <ul>
                                            <li>Input types (search & email)</li>
                                            <li>Making CSS background gradients fill the page</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Flix: Creating a Scrollable Area</h5>
                                        <p>In this exercise, you will learn how to make one section of a page scrollable. You will  add the "native" scroll bounce people expect on iOS devices.</p>
                                        <ul>
                                            <li>Creating a horizontal scrollable area</li>
                                            <li>Optimizing the scrolling for iOS touch devices</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Flix: Media Queries for Retina/HiDPI Graphics</h5>
                                        <p>In this exercise, you will learn how to use a media query to load a high-resolution background image for hi-res devices (which Apple calls Retina displays). </p>
                                        <ul>
                                            <li>Using media queries to load hi-res images for Retina/HiDPI displays</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Mobile First vs. Desktop First</h5>
                                        <p>How you order media queries is important.  Using a mobile first approach, we put mobile rules first, and with a desktop first approach, we put desktop rules first. In this exercise you will explore and weight the pros and cons of both options.</p>
                                        <ul>
                                            <li>Mobile first thinking</li>
                                            <li>Fluid widths</li>
                                            <li>Min-width vs. max-width media queries</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Box Model: CSS3 Box-Sizing & Calc()</h5>
                                        <p>In this exercise, you will take a deeper look at the box model, and see how padding affects the size of an element. You will learn how to switch to an alternate box model that makes building CSS layouts easier and more intuitive.</p>
                                        <ul>
                                            <li>Reviewing the box model</li>
                                            <li>CSS3 box sizing</li>
                                            <li>CSS3 calc() function</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Creating a Basic Wireframe</h5>
                                        <p>This is the first in a series of exercises in which you will create a responsive website for The Jive Factory, using the mobile first approach.  We will start by  designing the page structure using  a wireframe.</p>
                                        <ul>
                                            <li>Wireframing the basic Jive Factory layout</li>
                                            <li>Setting up "mobile first" media queries</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Finishing the Wireframe</h5>
                                        <p>In this exercise, we will finish the wireframe by adding the structure for the page at different breakpoints.</p>
                                        <ul>
                                            <li>Structuring the page for various sizes/devices</li>
                                            <li>Min & max-width media queries</li>
                                            <li>Using CSS calc() to gain control over fluid layouts</li>
                                            <li>Hiding elements for specific sizes/devices</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Creating CSS Gradient Patterns</h5>
                                        <p>In this exercise, we will add a background pattern using pure CSS gradients. We will also choose a couple of Google-hosted fonts to enhance the typographic style of the site.</p>
                                        <ul>
                                            <li>Editing the visual indicator for each media query</li>
                                            <li>Fun with RGBA</li>
                                            <li>Anatomy of a CSS gradient</li>
                                            <li>Linear-gradients & repeating-linear-gradients</li>
                                            <li>Linking to Google’s free web fonts<br>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Starting the Header</h5>
                                        <p>In this exercise, we will add the HTML for the Logo & Nav and add the CSS for mobile. Then we will style the header for phones and tablets.</p>
                                        <ul>
                                            <li>Adding the logo & nav content</li>
                                            <li>Styling the logo & nav for various sizes/devices</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: SVG Sprites &amp; Styling the Header for Desktop</h5>
                                        <p>In this exercise, we will finish styling the header for tablets and desktops. You will also  learn how to use SVG for sprites.</p>
                                        <ul>
                                            <li>Adding SVG Sprites</li>
                                            <li>Styling the logo &amp; nav for various sizes/devices</li>
                                            <li>Fixing opacity inheritance</li><br>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Final Touches &amp; Limiting Flexible Content</h5>
                                        <p> In this exercise, you will  optimize the content for specific screen sizes. You will also give the page a centered look by setting some limits on how wide the content can get.</p>
                                        <ul>
                                            <li>Improving Upcoming Shows on mobile</li>
                                            <li>Constraining the design at certain breakpoints</li>
                                            <li>Centering the design at certain screen sizes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Jive Factory: Responsive Slideshow</h5>
                                        <p>In this exercise, you will learn how to create a responsive slideshow using a free jQuery plugin called FlexSlider2.</p>
                                        <ul>
                                            <li>Getting the slideshow working</li>
                                            <li>Styling the slideshow content &amp; controls</li>
                                            <li>Preventing hidden images from loading</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Off-Screen Side Nav Using Only CSS</h5>
                                        <p>In this exercise, you will learn how to create an off-screen navigation menu that is hidden from view until the user clicks a button. You will create this functionality purely with CSS. No JavaScript is required!</p>
                                        <ul>
                                            <li>Responsive off-screen navigation</li>
                                            <li>Toggling the navigation with a checkbox</li>
                                            <li>CSS transitions</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Full Screen Backgrounds &amp; Viewport Sizing Units vh &amp; vw</h5>
                                        <p>In this exercise, you will create a page header that fills the entire screen with an image and heading. This is often called a hero image.</p>
                                        <ul>
                                            <li>Creating a full screen background</li>
                                            <li>Using viewport sizing units vh &amp; vw</li>
                                            <li>Vertically aligning content</li>
                                            <li>Darkening the background image via CSS</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: Getting Started</h5>
                                        <p>Bootstrap 3 is a powerful framework that can make web development faster and easier.  In this exercise, you will start coding a page using Bootstrap's basic elements.</p>
                                        <ul>
                                            <li>Adding content &amp; laying out the page</li>
                                            <li>Using Bootstrap’s grid system</li>
                                            <li>Creating &amp; adjusting columns</li>
                                            <li>Adding a navbar &amp; other components</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: More Elements &amp; Nesting Grids</h5>
                                        <p>In this exercise, you will learn about some more of Bootstrap's components and use the grid system to nest columns within other columns.</p>
                                        <ul>
                                            <li>Adding an email signup form</li>
                                            <li>Nesting sections</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: Controlling Grids &amp; Layout</h5>
                                        <p>In this exercise, you will take a closer look at Bootstrap's grid. you will refine the layout so the content fits better at various breakpoints.</p>
                                        <ul>
                                            <li>Changing the grid at specific sizes</li>
                                            <li>Showing &amp; hiding elements at specific sizes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: Creating a Photo Grid</h5>
                                        <p>In this exercise you will learn about Bootstrap's fluid-width grid that fills the entire screen width.</p>
                                        <ul>
                                            <li>Bootstrap’s fluid container</li>
                                            <li>Making images fill the grid</li>
                                            <li>Nesting sections</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: Skinning/Customizing Appearance</h5>
                                        <p>In this exercise you will learn how to customize the appearance of layouts created with Bootstrap, which is a process known as skinning.</p>
                                        <ul>
                                            <li>Using Bootstrap's Optional Theme</li>
                                            <li>Creating a fixed navbar</li>
                                            <li>Adding icons using Bootstrap's Included Icon Font</li>
                                            <li>Skinning or customizing the design  </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Bootstrap: Adding a Slideshow (Carousel)</h5>
                                        <p>Bootstrap also comes with a handy JavaScript library. In this exercise, you will see how easy it is to add a slideshow to the page using Bootstrap's carousel.</p>
                                        <ul>
                                            <li>Adding Markup for the Slideshow</li>
                                            <li>Hiding the Slideshow on Mobile</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="inner-section-nobg">
        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>
    </div>

    <div class="section section-reviews">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>HTML training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Photoshop classes - 5 rating"></span>
                    </div>

                    <h3><span>HTML classes rating:</span> 4.8 stars from 308 reviewers</h3>
                </div>

                <div data-aos="fade-up">
                    <div class="owl-carousel owl-instructors owl-theme instructors-list-block " data-aos="fade-up">
                        <div class="list-block">
                            <a href=".modal-instrutor-profile-jeff" data-toggle="modal">
                                <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-jeff.jpg')"></i>
                                <span>Jeff</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Jeff is excellent, very friendly and we had lots in common so learning was very relaxed and pleasant. He wasn't feeling well but still put in 100% and kept us ahead of schedule. I would highly recommend this course to others, especially to have Jeff as instructor.&quot;</p>
                                <span><strong>Emmanuel Masongsong | UCLA</strong></span>
                            </div>
                        </div>
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;HTML/CSS class with Chris L. He was a fantastic teacher. I would absolutely without a doubt take another class with him. I'm going to be taking the Javascript/jQuery class in a month and look forward to learning more.&quot;</p>
                                <span><strong>Richard Loos | Froeter Design Company</strong></span>
                            </div>
                        </div>
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Jeff is the best! He has a deep understanding of the subject and shares invaluable real world experiences that cannot be understood just by reading the typical textbooks available elsewhere.&quot;</p>
                                <span><strong>Robert Marshall | KLM Labs</strong></span>
                            </div>
                        </div>
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I really enjoyed the class. It was very informative and the instructor (Chris) was very knowledgeable and patient. I would recommend this class to anyone that is wanting to learn more about html/css.&quot;</p>
                                <span><strong>Adam Moore | Craig Bachman Imports, Inc.</strong></span>
                            </div>
                        </div>
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I started course not truly understanding how html structure relates to website and email design. I now feel confident reviewing html and applying skills I've learned by Chris in a website and email environment. Thank you!&quot;</p>
                                <span><strong>Suzanne OReilly | American Academy Of Orthopaedic Surgeons</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=15" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>


    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that HTML is a complex programming language and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months.</p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Our Walk Away, No Hard Feelings Guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">HTML is not for everyone. If you decide that after Day 1 in class that the class is not for you, we will give you a complete refund.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group HTML Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in HTML. This can be delivered onsite at your premises, at our training center in LOOP or via online webinar.
                    <br>
                    Fill out the form below to receive pricing.
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training span</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course[]" checked><i></i> <h3>HTML5 and CSS Fundamentals</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course[]"><i></i> <h3>HTML5 and CSS Advanced</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course[]"><i></i> <h3>Mobile & Responsive Web Design</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>


    <div class="section section-faq ">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>HTML Training FAQ</h2>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5><span class="card-title">Do I need to bring my own computer?</span></h5>
                    <p>No, we provide the computers.</p>
                </li>
                <li>
                    <h5><span class="card-title">Do your HTML classes have a live trainer in the classroom? </span></h5>
                    <p>Yes. Our HTML classes are  taught by  qualified and passionate trainers present in the classroom.</p>
                </li>
                   <li>
                    <h5><span class="card-title">Where do your HTML training classes take place?</span></h5>
                    <p>Our training center is located at 915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017.
                     For more information about directions, parking, trains please <a href="/contact-us-los-angeles.php">click here</a>.</p>
                </li>
                <li>
                    <h5><span class="card-title">Can you deliver HTML onsite at our location?</span></h5>
                    <p>Yes, we service the greater Los Angeles metro including Anaheim, Burbank, Covina, Downtown, Fullerton, Irvine, Long Beach, Northridge, Pasadena, San Bernardino, Santa Monica, Van Nuys, Ventura and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver HTML training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite HTML training.</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="section section-resouces-hero l2" style="background-image:url('/dist/images/bg-hero-9.png');">
        <div class="container">
            <div class="section-body">
                <h2>Resources</h2>
                <p>For more resources and articles on <br>Web development trainings</p>

                <a href="/resources/web-development.php" class="btn btn-primary btn-lg"><i class="fas fa-angle-double-right mr-2" aria-hidden="true"></i> Read more</a>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-jeff.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>