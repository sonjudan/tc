<?php
$meta_title = "Understanding the Shapes Pane in Microsoft Visio | Training Connection";
$meta_description = "Learn how to navigate and understand the shapes pane in MS Visio. This course is part of our Microsoft Visio Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Understanding the Shapes Pane</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Understanding the Shapes Pane in Microsoft Visio</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For comprehensive instructor led <a href="/visio-training.php">Microsoft Visio training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>
                <p>To begin, open Visio and create a new drawing using the “Blank Drawing” template.</p>
                <p>The Shapes pane is the primary tool that you will use to add shapes to your diagram. Open by default, you can find this pane on the left-hand side of the Visio window:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shapes-pane.jpg" alt="Shapes Pane Displayed">
                <p>Shapes in Visio are organized into stencils. You can open these stencils and display their contents inside the Shapes pane. Most templates will have related stencils already open, but you can also open stencils manually. <a href="/visio-training-chicago.php">Visio classes in Chicago</a>.
              <p>To open a stencil, click More Shapes (with Stencils selected at the top of the pane). A menu will appear listing stencil categories to choose from. For this example, open the Brainstorming Shapes stencil by clicking Business → Brainstorming → Brainstorming Shapes:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/open-stencil.jpg" alt="Stencil Menu Displayed">
                <p>Click outside the menu to close it. The stencil you selected will now be displayed in the Shapes pane. In this case, you can see that the Brainstorming Shapes stencil includes five shapes:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/stencil-displayed-shape-pane.jpg" alt="Stencil Appears In Shapes Pane">
                <p>A somewhat faster method to find shapes inside the Shapes pane is to use the search function. First, click the Search tab near the top of the Shapes pane:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/search-tab.jpg" alt="Use Search Tab in Shapes Pane">
                <p>The Search field will now be displayed. Inside this field, type the search term(s) you would like to look for. For this example, type “Circle” and press Enter to execute the search. The results will then be displayed and organized alphabetically by the stencil in which they belong:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/search-field-displayed.jpg" alt="Type Search Term Inside Search Field">
                <p>To give yourself more room to work on your drawings, you can minimize the Shapes pane. Do this by clicking the “Minimize the Shapes window” button () in the top right-hand corner of the pane:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/minimize-shapes-pane.jpg" alt="Shrink Shapes Pane">
                <p>The Shapes pane will now appear minimized. Notice that you can still see the shapes in the current stencil, but in a minimized form:
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/minimize-shapes-pane-form.jpg" alt="Shape Pane Visable">
                <p>Expand the Shapes pane once again by clicking the “Expand the Shapes window” button (&gt;):</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>