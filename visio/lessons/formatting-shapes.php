<?php
$meta_title = "Formatting Shapes in Visio | Training Connection";
$meta_description = "Formatting Shapes in Visio. This course is part of our Microsoft Visio Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Formatting Shapes</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Formatting Shapes in Visio</h1>
                        <h5>For instructor led <a href="/visio-training.php">Visio training in Chicago and Los Angeles</a> call us on 888.815.0604.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h4>Applying a Shape Style</h4>
                <p>When creating drawings using templates, you have the ability to adjust the shape and style that the selected template uses. The sample drawing shown below was created using the Basic Flowchart template. This template automatically applies a specific blue shape style to each individual type of shape used. For example, the Start/End shape has a different shape style than the Process shape, but both are blue:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/blue-shape-style.jpg" alt="Different Shape Styles"></p>
                <p>You can change the style for each individual shape by first selecting the shape in question and examining the options found in the Shape Styles gallery on the Home tab. For this example, click to select the Sub-Process One shape and click the More arrow () in the Shape Styles gallery:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shape-style-gallery.jpg" alt="Shape Style Gallery Options"></p>
                <p>(If you do not see this command, click the Quick Styles button.) All of the available shape styles will now be listed. Move your cursor over these styles. You will see that the selected shape will change to show you a preview of the style your cursor is currently on. Click the “Subtle Effect – Blue, Variant Accent 2” shape style to apply it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/quick-style-button.jpg" alt="Apply Style Via Quick Style Button"></p>
                <p>The new style will now be applied to the selected shape:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/new-style-applied.jpg" alt="New Style Applied to Shape"></p>

                <h4>Changing the Fill Color</h4>
                <p>You also have the ability to adjust a shape’s fill color independent of its style. In the sample drawing, select the Start shape. Then, click Home → Fill:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/adjust-fill-colour.jpg" alt="Adjust Shapes Fill Colour"></p>
                <p><a href="/visio-training-los-angeles.php">Microsoft Visio training in Los Angeles</a>.</p>
                <p>The Fill drop-down menu will list a wide variety of colors to choose from. Move your cursor over these colors to see a preview of how they will look when applied to the currently selected shape. Click Black:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/fill-drop-down-menu.jpg" alt="Fill Menu Options"></p>
                <p>The new fill color will now be applied to the selected shape:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/fill-colour-applied.jpg" alt="Fill Colour Applied to Shape"></p>

                <h4>Changing the Line Color</h4>
                <p>You can also change a shape’s line color (its border). Click to select the Start shape and then click Home → Line:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/change-line-colour.jpg" alt="Adjust Line Colour"></p>
                <p>The Line drop-down menu lists many colors to choose from, as well as options to adjust the weight (thickness) and style of the line. Move your cursor over the various color options that are available to see a preview of how they will look once applied. For this exercise, click Red:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/change-weight-style-line.jpg" alt="Change Lines Weight and Style"></p>
                <p>The new line color will now be applied to the selected shape. Because the line is thin, it may be hard to see:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/line-colour-applied.jpg" alt="Line Colour Applied to Shape"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>