<?php
$meta_title = "Adding and Connecting Shapes in Visio | Training Connection";
$meta_description = "Adding and Connecting Shapes in a Visio Drawing. This course is part of our MS Visio Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding and Connecting Shapes</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding and Connecting Shapes in a Visio Drawing</h1>
                        <h5>For instructor delivered <a href="/visio-training.php"> training in Chicago and Los Angeles</a> call us on 888.815.0604.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>To begin, open Visio 2013 and create a new drawing using the “Blank Drawing” template.</p>
                <p>To add shapes to your drawing, you first need to open the stencil in which the shapes you would like to add are found. For this example, click More Shapes → General → Basic Shapes:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/add-shape-to-drawing.jpg" alt="Open stencil to Add Shape to Drawing"></p>
                <p>Click outside the menu to close it. <a href="/visio-training-los-angeles.php">Visio classes in Downtown LA</a>.</p>
                <p>With the Basic Shapes stencil now open in the Shapes pane, add the Rectangle shape by clicking and dragging it onto your drawing. Release your mouse button to place it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shapes-stencil-open-shapes-pane.jpg" alt="Add Shape to Drawing"></p>
                <p>The shape will now be added to your drawing:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shape-added-to-drawing.jpg" alt="Shape Displayed in Drawing"></p>

                <h4>Connecting Shapes</h4>
                <p>To begin, open Module 2-3 using Microsoft Visio 2013. <a href="/visio-training-chicago.php">Visio training in the Chicago Loop</a>.</p>
                <p>To connect shapes together, you first need to activate the Connector tool. Click Home → Connector:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/connect-shapes-together.jpg" alt="Activate Connector Tool"></p>
                <p>With the Connector tool now activated, move your cursor over one of the shapes that you would like to connect. For this example, focus on the left-most rectangle. When you do this, you will see hotspots appear on each side of the shape as well as in the shape’s center. In this example, move your cursor over the hotspot on the far right-hand side of the rectangle. You will see it outlined in green:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/select-shapes-to-connect.jpg" alt="Move Cursor to Select Shapes"></p>
                <p>Click and drag from this hotspot to the opposite one on the adjacent rectangle. Release your mouse button when the corresponding hotspot is outlined in green to complete the connection:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/click-drag-from-hotspot.jpg" alt="Drag and Drop From Hotspot"></p>
                <p>These two shapes will now be connected:</p><p><img src="https://www.trainingconnection.com/images/Lessons/visio/shapes-now-connected.jpg" alt="shapes are connected"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>