<?php
$meta_title = "Moving Objects in Microsoft Visio | Training Connection";
$meta_description = "Learn how to move objects in MS Visio. This course is part of our Microsoft Visio Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Moving Objects</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Moving Objects in Microsoft Visio</h1>
                        <h5>For instructor led <a href="/visio-training.php">Microsoft Visio training in Chicago and Los Angeles</a> call us on 888.815.0604.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Once objects have been placed on your drawing, you can move and rearrange them using the drag and drop method. In the sample drawing, you can see three rectangle shapes, two of which are connected to each other.</p>
                <p>Click and drag the left-most shape so that it appears near the top of the page:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/move-object-with-drag-drop.jpg" alt="Drag and Drop to Move Object"></p>
                <p>Once the shape is in place, release your mouse button to complete the move:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/release-mouse-move-shape.jpg" alt="Use Mouse to Complete Shape Move"></p>
                <p>Repeat the previous steps to drag and drop the right-most shape so that it appears on top of the existing connector near the bottom of the page. It is in the correct position when the connector end is outlined in green:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/connector-outline-green.jpg" alt="Connector Line Appears Green"></p>
                <p>The shape in question will now be connected to the bottom shape using the existing connector:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shape-now-connected.jpg" alt="Shaped Connect by Connector"></p>


                <h4>Using Cut, Copy, and Paste</h4>
                <p>Cut, copy, and paste work in Visio the same way as they do in other applications in Windows. <strong><span style="font-family: Calibri;">Cut</span></strong> is intended to move one item from one location to another, or when you want to remove something you may need later. <strong><span style="font-family: Calibri;">Copy</span></strong> is used to duplicate items. When using either option, items you copy or cut will be placed on your computer’s clipboard. They can then be pasted into a new location. The item will also remain on the clipboard until you cut or copy new items. Once a new item is placed on the clipboard, any previous items that resided there cannot be recovered. <a href="/visio-training-chicago.php">Microsoft Visio training course in Chicago</a>.</p>
                <p>In the sample drawing, you need to copy one of the existing shapes and paste the duplicate in a new location. Click to select any of the existing shapes on the drawing. Then, click Home → Copy:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/copy-shape.jpg" alt="Copy Existng Shape"></p>
                <p>With the selected object now copied to your computer’s clipboard, right-click on any blank area of your drawing. Click Paste:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/right-click-paste.jpg" alt="Paste From Clipboard"></p>
                <p>The copied shape will now be placed on your drawing:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/copied-shape-applied.jpg" alt="Copied Shape Appears"></p>
                <p>Right-click the rectangle shape between the two existing shapes. Click Cut:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/cut-shape.jpg" alt="Click Cut"></p>
                <p>The selected object will be removed from the drawing and placed on your computer’s clipboard. Right-click anywhere on the page and click Paste:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/selected-shape-cut.jpg" alt="Selected Shape Removed"></p>
                <p>The object will now be placed in this new location:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/objects-in-new-location.jpg" alt="Object Appears In New Location"></p>
                <p>Note that any existing connectors will not be copied along with the cut object. <a href="/visio-training-los-angeles.php">Los Angeles Visio group training</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>