<?php
$meta_title = "Understanding the Microsoft Visio Interface | Training Connection";
$meta_description = "Learn all the different components that make up the Microsoft Visio interface. This lesson is part of our Visio Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Visio Interface</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Microsoft Visio Interface</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Visio 2010 has a new interface that builds on interface from the previous version of Visio. Visio 2010 uses the ribbon interface that was introduced in Microsoft Office 2007 applications. Each tab in the ribbon contains many tools for working with your drawing. To display a different set of commands, click the tab name. Buttons are organized into groups according to their function.</p>
                <p>In addition to the tabs, Visio 2010 also makes use of the Quick Access Toolbar from the MS Office 2007 applications. The File tab is a new feature that opens the Backstage View. The new Backstage View will be discussed in the next topic.
                    Below is the Visio interface, including the Ribbon, the Slides tab, the Slides pane, the Quick Access toolbar, and the Status bar.</p>
                <p>Did you know, we are an industry leading software training company and we offer instructor-led Visio classes in Chicago and Los Angeles. For friendly, fun and informative training out why not give us a call on 888.815.0604.</p>
                <p>For an instructor-led <a href="/visio-training.php">Microsoft Visio class</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-interface.jpg" width="750" height="564" alt="MS Visio Screen Interface"></p>
                <ul>
                    <li>The Shapes panel shows a thumbnail of shapes and/or stencils. </li>
                    <li>The Drawing pane is where you can view and edit the entire drawing. </li>
                    <li>The Status Bar provides information about your drawing and has additional tools for making changes to the view.</li>
                </ul>

                <p>The Quick Access Toolbar appears at the top of the Visio window and provides you with one-click shortcuts to commonly used functions. By default, the Quick Access Toolbar contains buttons for Save, Undo and Redo.<a href="/visio-training-los-angeles.php"> LA-based Visio training</a>.</p>
                <p>Use the following procedure to customize the contents of the Quick Access toolbar.</p>
                <ol>
                    <li>
                        <p>Click the arrow icon immediately to the right of the Quick Access toolbar.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/customize-quick-access-toolbar.jpg" width="500" height="478" alt="Visio Quick Access Toolbar"> </p>
                    </li>

                    <li>
                        <p>Add an item to the Quick Access Toolbar by selecting it from the list. You can remove an item by reopening the list and selecting the item again. <a href="/visio-training-chicago.php">MS Visio classes in Chicago</a>.</p>
                        <p>If you select More Commands, Visio opens the Visio Options window.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-options.jpg" width="600" height="490" alt="Visio options panel"></p>
                    </li>
                </ol>

                <p>To add a command, select the item from the list on the left and select Add. Select OK when you have finished. Read more about <a href="https://spscc.edu/sites/default/files/imce/students/using_the_ribbon_in_microsoft_office_2013.pdf">customizing the Visio Ribbon</a>. </p>

                <h4>Using Backstage View</h4>
                <p>Select the File tab in the Ribbon to open the Backstage view. The Backstage view is where you will find the commands for creating, saving, opening, and closing files, as well as information about the file. The Backstage view includes new interfaces for printing and sharing your drawings. The Options command is also available to open a new screen for setting your Visio Options.</p>
                <p>Use the following procedure to open the Backstage View.</p>
                <ol>
                    <li>Select the File tab on the Ribbon.</li>
                </ol>
                <p>Visio displays the Backstage View, open to the Info tab by default. A sample is illustrated below.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-backstage-view.jpg" width="750" height="564" alt="Backstage View"></p>

                <h4>Creating a Blank Drawing</h4>
                <p>The New tab of the Backstage view provides several options for creating new drawings. The Blank Document option is at the bottom of the screen under “Other Ways to Get Started.”</p>
                <p>Use the following procedure to create a blank drawing.</p>
                <ol>
                    <li>Select the File tab on the Ribbon.</li>
                    <li>Select the New tab in the Backstage View.</li>
                    <li>Select Blank Drawing.</li>
                    <li>Select Create.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/blank-doc.jpg" width="750" height="563" alt="Creating a Blank drawing"></p>

                <p>See tutorial on how to <a href="/visio/lessons/screen.php">setup the screen in Visio</a>. Download a <a href="https://www.winthrop.edu/uploadedFiles/tlc/visio-2010-quick-reference.pdf">Microsoft Visio quick start guide</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=54">Visio training testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>