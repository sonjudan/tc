<?php
$meta_title = "Rotating, Resizing, Duplicating and Deleting Shapes in Microsoft Visio | Training Connection";
$meta_description = "Learn how to rotate, resize, duplicate and delete shapes in MS Visio. This course is part of our Microsoft Visio Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Rotating, Resizing, Duplicating and Deleting Shapes</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Rotating, Resizing, Duplicating and Deleting Shapes in Microsoft Visio</h1>
                        <h5>For instructor led <a href="/visio-training.php">Microsoft Visio training in Chicago and Los Angeles</a> call us on 888.815.0604.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h4>Rotating Shapes</h4>
                <p>When you select a shape, you will see the rotate icon appear above it. This is used to rotate the selected shape in any direction. Click to select the top-most rectangle shape. Then, click and drag the rotate icon in a clockwise direction, until it is on its side:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/rotate-icon.jpg" alt="Use Rotate Icon to Move Shape"></p>
                <p>Just as when moving a shape, you will see that the connector has automatically adjusted itself to accommodate for the connected shape’s new orientation. <a href="/visio-training-chicago.php">Chicago MS Visio classes</a>.</p>
                <p>You can also rotate objects in set amounts by clicking Home → Position → Rotate Shapes. This menu will list four options, including rotating the shape right or left by 90° as well as flipping it vertically or horizontally. For this example, click Rotate Right 90°:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/rotate-objects-set-amounts.jpg" alt="Rotate Multiple Shapes"></p>
                <p>The selected rectangle will now appear upside down in relation to its previous orientation:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/upside-down-shape-orientation.jpg" alt="Shape Appears Upside Down"></p>
                <h4>Resizing Shapes</h4>
                <p>To resize any shape, you first need to select the shape in question. For this example, click to select the top-most rectangle shape:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/resize-shape.jpg" alt="Select Shape to Resize"></p>
                <p>When a shape is selected, you will see a resize handle on each of its sides and corners. In the example above, you can see that there are eight of these handles in total. The resize handles on the sides of the shape are used to adjust the individual dimensions of the shape, while those on the corners adjust the overall size of the shape. To use these handles, click and drag them outwards to increase the size of the shape or inwards to decrease it (respectively). For this example, click and drag the resize handle in the bottom right-hand corner of the shape in an outward direction:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/resize-handle.jpg" alt="Resize Handle Appears"></p>
                <p>You will see that the size of the shape will increase in relation to how far you drag your cursor. Increase the size of the shape so that it is roughly double the original size. Release your mouse button to apply the new dimensions. <a href="/visio-training-los-angeles.php">Visio 2019 training in Los Angeles</a>.</p>

                <h4>Duplicating Shapes</h4>
                <p>When creating diagrams in Visio, you will often find that you are using the same shape multiple times. To help save you time, Visio includes the ability to duplicate any shape on your page. Duplicate the top-most shape in the sample drawing by clicking to select it. Then, click Home → Paste → Duplicate:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/duplicating-shapes.jpg" alt="Select Shape to Duplicate"></p>
                <p>A duplicate of the selected shape will now appear on the page, partially overlapping the original:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/overlapping-duplicated-shape.jpg" alt="Duplicated Shape Appears Overlapped"></p>

                <h4>Deleting Shapes</h4>
                <p>Delete the top-most shape in the provided sample drawing by clicking to select it and then pressing the Delete key on your keyboard. The shape and its connector will be removed. Your drawing should now look like this:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/deleted-shape.jpg" alt="Highlighted Shape Deleted"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>