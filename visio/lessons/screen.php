<?php
$meta_title = "Screen setup in Microsoft Visio | Training Connection";
$meta_description = "Learn how to setup your screen in Microsoft Visio. This lesson is part of our Visio Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Visio screen setup</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Setting up the screen in Microsoft Visio</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this module, you will learn how to set up your Visio screen. You have different elements to help you create your drawing, which you can show or hide as needed. This module will explain how to add, move, and delete a guide. It will also explain how to change the ruler settings and the grid settings. Also see the <a href="/visio/lessons/interface.php">Microsoft Visio Interface</a>.</p>
                <p>Did you know we offer face-to-face instructor-led <a href="/visio-training.php">MS Visio classes</a> in Chicago and Los Angeles. For more details give us a call on 888.815.0604.</p>
                <h4>Showing and Hiding Screen Elements</h4>

                <p>Visio’s ruler, grid, guides, and connection points can be toggled on or off with the View menu. The Task panes can also be shown or hidden depending on the current need.</p>
                <p>Use the following procedure to hide the grid lines.</p>
                <ol>
                    <li>Select the View tab from the Ribbon.</li>
                    <li>Clear the Grid box. </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/grid-box.jpg" width="350" height="" alt="Visio Grid Box"></p>

                <h4>Adding a Guide</h4>
                <p>Guides help you line up the shapes in your drawing. They do not appear on the final drawing when you print or save the drawing as a picture. You can add as many guides as you need, either horizontally or vertically, to help you with your drawing. <a href="/visio-training-chicago.php">MS Visio training in Chicago</a>.</p>
                <p>Use the following procedure to add a guide.  </p>
                <ol>
                    <li>Click and drag the ruler to create a guide.</li>
                    <li>Drag it to the desired location. The guide appears as a dotted line. Release the mouse button to place the guide. The guide is still selected in the second illustration.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-guides.jpg" width="600" height="478" alt="Adding a guide in Visio"></p>
                <p>Download a <a href="https://products.office.com/en-us/visio/visio-professional-free-trial-flowchart-software" target="_blank">free trial version of Microsoft Visio flowcharting software</a>.</p>

                <h4>Moving or Deleting a Guide</h4>
                <p>Use the following procedure to move a guide. </p>
                <ol>
                    <li>Click on the guide you want to move. The small circle containing a plus sign, plus the larger blue highlighting of the guide indicates that it is selected.</li>
                    <li>Drag it to the desired location. The guide appears as a dotted line. Release the mouse button to place the guide in the new position.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/moving-guide.jpg" width="600" height="253" alt="Moving a Visio Guide">   </p>

                <p>Use the following procedure to delete a guide.
                </p>
                <ol>
                    <li>Click on the guide you want to move.</li>
                    <li>Press Delete or Backspace on the keyboard to delete it.</li>
                </ol>
                <h4>Changing Ruler and Grid Settings</h4>
                <p>The Rulers and Grid dialog box allows you to customize how you use the ruler and grid in the Visio canvas. You can control the ruler subdivisions or grid spacing, as well as set the origins to help with measuring objects in different positions in the drawing workspace. <a href="/visio-training-los-angeles.php">Los Angeles Visio training classes</a>.</p>
                <p>Use the following procedure to open the Ruler and Grid dialog box.</p>
                <ol>
                    <li>Select the square at the bottom right corner of the <strong>Show</strong> group on the <strong>View</strong> tab of the Ribbon.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/ruler-grid.jpg" width="500" height="417" alt="Ruler &amp; Grid Panel"></p>

                <p>Get started on <a href="/visio/lessons/first-doc.php">your first Visio drawing</a>. Download a <a href="https://www.winthrop.edu/uploadedFiles/tlc/visio-2010-quick-reference.pdf" target="_blank">Microsoft Visio quick start guide</a>. </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student testimonials at our <a href="/testimonials.php?course_id=54">Visio  testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>