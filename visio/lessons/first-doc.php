<?php
$meta_title = "Your First Visio Drawing | Training Connection";
$meta_description = "Learn how to create your first drawing in Microsoft Visio. This lesson is part of our Visio Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/visio.php">Visio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Your First Drawing</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Visio">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Your First Visio Drawing</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this module, you will create your first drawing. Drawings consist of shapes. This module will cover how to find the right shape and place it on your drawing. You’ll learn how to add text to shapes. You’ll learn how to work with shapes, including resizing, moving, and deleting shapes. This module will also cover using the Tools group, which helps with refining your shapes. Also see <a href="/visio/lessons/screen.php">setting up the Visio screen</a>. </p>
                <p>Why not join our Training Connection family of satisfied students by signing up to one of our friendly, fun and informative  <a href="/visio-training.php">Microsoft Visio classes</a>? Call us on 888.815.0604 and we'll talk you through a few options that best suits your training requirements
                </p>
                <h4>Finding the Required Shape</h4>
                <p>The Shapes pane allows you to search through different categories to find the shape that you need. You can expand or collapse categories as you use them. You can use the Search Shapes tool to search for shapes within Visio, and you can even look for more shapes online. </p>
                <p>Us the following procedure to open a stencil. </p>
                <ol>
                    <li>Select <strong>More Shapes</strong> from the Shapes pane.</li>
                    <li>Select a Category.</li>
                    <li>Select the Shape stencil you want.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-shapes.jpg" width="750" height="476" alt="Shapes Pane"></p>
                <p>Visio displays the shapes in that stencil in the Shapes pane. <a href="/visio-training-chicago.php">Hands-on Visio training in Chicago</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/arrow-shapes.jpg" width="750" height="400" alt="Visio shapes"></p>
                <p>Use the following procedure to search for shapes.</p>
                <ol>
                    <li>Select <strong>More Shapes</strong> from the Shapes pane.</li>
                    <li>Select <strong>Search for Shapes.</strong>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/more-shapes.jpg" width="350" height="" alt="Arow Shapes"></p>
                        <p>Visio displays a Search field. <a href="/visio-training-los-angeles.php">Los Angeles Visio training classes</a>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/shapes-panel.jpg" width="200" height="" alt="More Shapes"></p>
                    </li>
                    <li>Enter text to describe the shape you want to find.</li>
                    <li>
                        <p>Press the Enter key or select the magnifying glass to begin the search.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/searching-for-shapes.jpg" width="200" height="" alt="Search for more shapes"></p>
                        <p>If there are many matches, Visio displays the following warning.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-warning.jpg" width="450" height="" alt="View results"></p>
                    </li>
                    <li>
                        <p>Select <strong>Yes</strong> to continue.</p>
                        <p>Visio displays the matches in the Shapes pane. Use the scroll bar to scroll down to see all of the matching shapes. Also see how to create an <a href="https://support.microsoft.com/en-us/kb/318330">Organization Chart using Visio</a>. </p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/visio/matching-shapes.jpg" width="200" height="" alt="Matching shapes"></p>
                    </li>
                </ol>

                <h4>Placing the Shape in the Drawing</h4>
                <p>It is easy to drag shapes to your drawing. For greater control over positioning, use your guides or grid to help place the shapes. Use the following procedure to add a shape to the drawing. This example shows the Glue to Guide feature.</p>
                <ol>
                    <li>Click the shape you want to use.</li>
                    <li>Drag it to the drawing canvas.</li>
                    <li>If you want to glue the shape to a guide, position it so that the red box (es) are visible.</li>
                    <li>Release the mouse button to position the shape.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/glue-to-guide.jpg" width="300" height="" alt="Glue to Guide"></p>
                <p>Download a <a href="https://products.office.com/en-us/visio/visio-professional-free-trial-flowchart-software">free trial version of Microsoft Visio flowcharting software</a>.</p>

                <h4>Adding Text to a Shape</h4>
                <p>Text can bring additional meaning to your diagram. To add text to a shape, simply double-click on the shape. Use the following procedure to enter text in a shape. </p>
                <ol>
                    <li>Double-click on the shape.</li>
                    <li>Type your text.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/adding-text.jpg" width="320" height="" alt="Adding Text to a Shape"> </p>

                <p>Use the following procedure to delete a guide. </p>
                <ol>
                    <li>Click on the guide you want to move.</li>
                    <li>Press Delete or Backspace on the keyboard to delete it.</li>
                </ol>
                <h4>Resizing, Moving, and Deleting Shapes</h4>
                <p>Shapes can be adjusted by size and position on the drawing. You can also simply delete a shape that is not working for the drawing. Use the following procedure to resize a shape. </p>
                <ol>
                    <li>Click on the shape to activate it. Visio displays handles around the shape to show that it is active.</li>
                    <li>When you move your cursor to one of the corner handles, the cursor changes to a double-arrow. Click and drag the handle to resize the shape proportionally. Release the mouse when the shape is the size you want.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/resizing-text.jpg" width="320" height="" alt="Moving Shapes"></p>

                <p>Use the following procedure to move a shape. </p>
                <ol>
                    <li>Click on the shape to activate it. Visio displays handles around the shape to show that it is active.</li>
                    <li>Drag the shape. The cursor appears as a cross of double arrows. </li>
                    <li>Release the mouse when the shape is in the new position.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/moving-shapes.jpg" width="320" height="" alt="Visio handles"> </p>
                <p>Use the following procedure to delete a shape.</p>
                <ol>
                    <li>Click on the shape to activate it. Visio displays handles around the shape to show that it is active.</li>
                    <li>Press the Delete key.</li>
                </ol>

                <h4>Using the Tools Group</h4>
                <p>The Tools group includes a number of tools to help you complete your drawing.</p>
                <ul>
                    <li>The Pointer tool is used to select shapes and other objects.</li>
                    <li>The Connector tool allows you to connect shapes.</li>
                    <li>The Text tool allows you to select text in a shape or type free-form text on your drawing.</li>
                    <li>The Rectangle, Ellipse, and Line tools allow you to draw basic shapes.</li>
                    <li>The Connection Point tool allows you to add, move, or delete connection points on shapes.</li>
                </ul>
                <p>The Text Block tool allows you to move, resize, or rotate the text block on a shape. You will find the Tools group on the Home tab of the Ribbon.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-home-tab.jpg" width="750" height="103" alt="Visio menu"></p>

                <p>Use the following procedure to connect two shapes.</p>
                <ol>
                    <li>Select the Connector tool from the Home tab of the Ribbon.</li>
                    <li>Click on the shape you want to connect. The cursor changes to a red square to show that you are using a connection point.</li>
                    <li>Drag the connector to the shape you want to connect. Release the mouse to connect the shapes.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/visio/visio-connector.jpg" width="600" height="" alt="Visio Connectors"></p>

                <p>Download a <a href="https://www.winthrop.edu/uploadedFiles/tlc/visio-2010-quick-reference.pdf">Microsoft Visio quick start guide.</a></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-visio" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Visio student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of student reviews at our <a href="/testimonials.php?course_id=54">Visio  testimonial </a>page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>