<?php
$meta_title = "Adding Resources in MS Project | Training Connection";
$meta_description = "Learn how to adding Resources in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding Resources</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Resources in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article  introduces you to adding resources in MS Project. Resources are the people, equipment, and materials needed to complete your project. This module will give an overview of how resources are used in Project 2010. You’ll learn how to add resources and view resource information. You’ll also learn how to assign resources to tasks. Finally, this module presents an introduction to leveling resources, which is a way to reschedule tasks so that your resources are not overscheduled.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Understanding Resources</h4>
                <p>There are three types of resources related to your project:</p>
                <ul>
                    <li>Work</li>
                    <li>Material</li>
                    <li>Cost</li>
                </ul>
                <p>The resources must be added to the project before they are available to assign to tasks. </p>
                <p>There are three different types of resources in Project 2010:
                    Work resources are the people included in your project plan. These are the people who will do the work of completing tasks related to the project. </p>
                <p>Material resources are the items that are required to complete your project that are measured in units rather than work hours, such as cases of roofing shingles or gallons of paint.</p>
                <p>Cost resources are fixed costs associated with your project, such as airfare or equipment rental. <a href="/ms-project-training-chicago.php">Chicago-based Project 2019 training</a>.</p>

                <h4>Adding Resources</h4>
                <p>You’ll need to create your data base of resources before you can do anything else with your resource information. The Resource Sheet allows you to enter your resource information. </p><p>Use the following procedure to enter resources.</p>
                <ol>
                    <li>Select the <strong>View</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Resource Sheet</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-sheet.jpg" width="750" height="101" alt="Resources Sheet"></p>

                <ol start="3">
                    <li>Select the <strong>Resource</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Add Resources</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-resources.jpg" width="750" height="684" alt="Add Resources"></p>

                <ol start="5">
                    <li>You can add resources from your <strong>Active Directory</strong> or your <strong>Outlook</strong> <strong>Address</strong> <strong>Book</strong>. Or select <strong>Work Resource</strong>, <strong>Material</strong> <strong>Resource</strong>, or <strong>Cost</strong> <strong>Resource</strong> to add a resource of that type. You can also add a resource by entering information directly into the Resource sheet. Enter the <strong>Resource Name</strong>, the resource <strong>Type</strong>, and the <strong>Standard</strong> <strong>Rate</strong> for the resource in the Resource sheet. The standard rate can be indicated in different ways, including an hourly or annual rate for work resources and a per unit price for material resources.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-sheet-tools.jpg" width="750" height="215" alt="Resource Sheet Tools"></p>

                <ol start="6">
                    <li>Make sure to save your work.</li>
                </ol>

                <h4>Viewing Resource Information</h4>
                <p>The <em>Resource Information</em> dialog box includes information about the resource, including availability and costs. You can also store notes about the resource and set up custom information. </p>
                <p>Use the following procedure to view the <em>Resource Information</em> dialog box.</p>
                <ol>
                    <li>Highlight the resource you want to view in the <strong>Resource Sheet.</strong></li>
                    <li>Select <strong>Information</strong> from the <strong>Resource</strong> tab on the Ribbon.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-tab-information.jpg" width="750" height="178" alt="Resource Tab - Information"></p>
                <p>The <strong>General</strong> tab of the <em>Resource Information</em> dialog box includes basic information about the resource, including the name, email address, and resource type. Some more advanced options including the calendar and availability are also available on this tab.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-information-general.jpg" width="750" height="564" alt="General Tab"></p>
                <p>The <strong>Costs</strong> tab of the <em>Resource Information</em> dialog box includes information about the resource pay rates or costs.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-information-costs.jpg" width="750" height="564" alt="Costs Tab"></p>
                <p>The <strong>Notes</strong> tab of the <em>Resource Information</em> dialog box allows you to include notes about the resource.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-information-notes.jpg" width="750" height="571" alt="Notes Tab">  </p>

                <h4>Adding Resources to Tasks</h4>
                <p>Once you have added resources to your project, you can assign resources to your tasks. You return to the task view to assign resources to individual tasks. You can assign more than one resource to each task.</p> <p>Use the following procedure to assign a resource to a task.</p>
                <ol>
                    <li>Select <strong>Gantt Chart</strong> to return to the Gantt Chart view.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gantt-chart-view.jpg" width="750" height="210" alt="Gantt Chart View"></p>

                <ol start="2">
                    <li>Select the <strong>Resource</strong> tab from the Ribbon.</li>
                    <li>Select a task from the task list.</li>
                    <li>Select <strong>Assign Resources</strong>. </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-tab-ribbon.jpg" width="750" height="221" alt="Assign Resources"></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assign-resources-dialog-box.jpg" width="500" height="339" alt="Assign Resources Dialog Box"></p>
                <ol start="4">
                    <li>Highlight one or more resources from the list. To select multiple resource, press the Shift or Ctrl key while selecting the resource name.</li>
                    <li>Select <strong>Assign</strong>. You can also assign multiple resource individually by highlighting each resource and then clicking <strong>Assign</strong>.</li>
                    <li>When you have finished assigning resources, select <strong>Close</strong>.</li>
                </ol>
                <p>Notice the resource information has been added to the Gantt chart next to the duration bars.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/updated-gantt-chart.jpg" width="750" height="208" alt="Updated Gantt Chart">  </p>

                <h4>Leveling Resources</h4>
                <p>The <strong>Level Resources</strong> tool in Project 2010 looks at task assignments for your resources and makes sure that a resource is not overscheduled. If task assignments for a given day cause a resource to be overscheduled, the affected tasks are rescheduled (sometimes by splitting or delaying tasks) so that the resource is not overallocated.</p>
                <p>Use the following procedure to level resources. </p>

                <ol>
                    <li>Select <strong>Level Resource</strong> from the <strong>Resource</strong> tab on the Ribbon.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/level-resources.jpg" width="750" height="101" alt="Resource Tab > Level Resource"></p>
                <ol start="2">
                    <li>Select the <strong>Resource</strong> you want to level.</li>
                    <li>Select <strong>Level Now</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/level-resources-dialog-box.jpg" width="500" height="685" alt="Level Resources Dialog Box"></p>
                <p>Notice how Task 2 has been split to accommodate Joe’s time at the Weekly meeting.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-2.jpg" width="750" height="224" alt="Task 2 on Gantt Chart"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Other resources</h4>
                    <ul>
                        <li><a href="http://www2.ita.vt.edu/software/department/products/microsoft/project_2007/index.html" target="_blank">System Requirements for Project 2016</a></li>
                        <li><a href="https://blogs.office.com/2012/08/06/introducing-projects-new-reports/" target="_blank">Project 2016 new reports</a></li>
                        <li><a href="https://books.google.com/books?id=P5ttR3SgsRQC&amp;pg=PA201&amp;source=gbs_toc_r&amp;cad=4#v=onepage&amp;q&amp;f=false" target="_blank">Connecting Resources to Tasks</a></li>
                        <li><a href="/project/lessons/multiple-projects.php" >Working with mutliple projects </a></li>

                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Related Project Lessons</h4>
                    <ul>
                        <li><a href="/project/lessons/first-project.php">Your First Project</a></li>
                        <li><a href="/project/lessons/adding-tasks.php">Adding Tasks in MS Project</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project student review page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>