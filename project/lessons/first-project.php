<?php
$meta_title = "Starting a Project in MS Project | Training Connection";
$meta_description = "Starting your first Project using Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Your First Project</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Starting a Project in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, you’ll get started using Microsoft Project by creating a basic project and setting up the project schedule. You’ll add tasks to your project and set constraints on those tasks to further customize your project.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Project classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>


                <h4>Creating a Basic Project</h4>
                <p>The Project Information dialog box allows you to determine the start (or finish) date for your project to determine accurate scheduling. Use the following procedure to schedule a project start date.</p>
                <ol>
                    <li>Select the <strong>Project</strong> tab.</li>
                    <li>Select <strong>Project</strong> <strong>Information</strong>.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-information.jpg" width="750" height="101" alt="Select Project Tab and then Project Information Icon"></p>

                <p>The system opens the <em>Project Information</em> dialog box. <a href="/ms-project-training-los-angeles.php">Los Angeles instructor-led Project classes</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-information-dialogue-box.jpg" width="750" height="542" alt="Project Information Dialogue Box"></p>


                <ol start="3">
                    <li>Select the arrow next to <strong>Start date</strong> to choose a date from the calendar.</li>
                </ol>
                <p>While you have the <em>Project Information</em> dialog box open, discuss some of the other options. You can select Project Finish Date for <strong>Schedule from</strong> and choose the date you would like the project finished instead. The <strong>Current Date</strong> and <strong>Status</strong> <strong>Date</strong> allow you to change the viewpoint when reviewing or reporting on details about your project. The <strong>Calendar</strong> sets the default calendar for determining the schedule. </p>
                <ol start="4">
                    <li>Select <strong>OK</strong> to set your schedule start date.</li>
                </ol>

                <h4>Adding Tasks to your Project</h4>
                <p>The left side of the Gantt chart in the default view works like a spreadsheet. You enter each task on a new row. You can add as much or as little information about the task as you like to get started. You can enter the tasks manually or paste the information from another program (such as Word or an email message).</p>
                <p>Use the following procedure to add tasks to a project.</p>
                <ol>
                    <li>In the <strong>Task Name</strong> column on the left side of the Gantt chart, enter the name of the task and press Enter. </li>
                    <li>Continue entering task names for as many tasks as you want to enter.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-name-column.jpg" width="750" height="271" alt="Adding Task to your Project"></p>
                <p>Use the following procedure to add tasks to a project by pasting.</p>
                <ol>
                    <li>Copy the information from another program. In this example, text is taken from a Notebook file (.txt).</li>
                    <li>Place your cursor in an empty row in the <strong>Task Name</strong> column. </li>
                    <li>Select the <strong>Task</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Paste</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gant-chart-tools.jpg" width="750" height="344" alt="Paste icon in Task Tab"></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gant-chart-tools-2.jpg" width="750" height="538" alt="Manually scheduled icon"></p>

                <h4>Setting Constraints on Tasks</h4>
                <p>The Task Details form is another way to enter task information. The form includes information on constraints. Constraints are restrictions set on the start or finish date of a task.</p>
                <p>There are several types of constraints: </p>
                <ul>
                    <li>As soon as possible</li>
                    <li>As late as possible</li>
                    <li>Finish no earlier than</li>
                    <li>Finish no later than</li>
                    <li>Must Finish On</li>
                    <li>Must Start On</li>
                    <li>Start No Earlier Than</li>
                    <li>Start No Later Than</li>
                </ul>

                <p>Use the following procedure to open the Task Details form and set a constraint type and date.</p>

                <ol>
                    <li>Select the <strong>Display Task Details</strong> icon from the <strong>Task</strong> tab on the Ribbon. It is located in the <strong>Properties</strong> area.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/display-task-details-icon.jpg" width="750" height="103" alt="Display Task Details Icon"></p>

                <p>The system displays the <em>Task Details Form</em> in the bottom area of the screen.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-details-form.jpg" width="750" height="563" alt="Task Details Form"></p>


                <ol start="2">
                    <li>You can use the <strong>Next</strong> or <strong>Previous</strong> buttons to select the task for which you want to set a constraint. </li>
                    <li>Uncheck the <strong>Manually Scheduled</strong> box. Constraints are ignored for manually scheduled tasks.</li>
                    <li>Select the <strong>Constraint type</strong> from the drop down list.</li>
                    <li>Select the arrow next to the <strong>Constraint date</strong> and select the constraint date from the calendar.</li>
                    <li>Select OK.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/manually-scheduled-box.jpg" width="750" height="563" alt="Task Form Tools"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Other resources</h4>
                    <ul>
                        <li><a href="http://wheatoncollege.edu/technology/files/2013/04/project-2013-quick-reference.pdf" target="_blank">Project 2013 Shortcuts</a></li>
                        <li><a href="https://books.google.com/books/about/Microsoft_Project_for_Dummies.html?id=lmfHG3jxI2oC&amp;redir_esc=y" target="_blank">MS Project for Dummies</a></li>
                        <li><a href="https://www.microsoft.com/en-us/evalcenter/evaluate-project-professional-2016" target="_blank">Download a Trial Version of MS Project 2016</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Related Project Lessons</h4>
                    <ul>
                        <li><a href="/project/lessons/adding-tasks.php">Adding Tasks in Project</a></li>
                        <li><a href="https://books.google.com/books/about/Microsoft_Project_for_Dummies.html?id=lmfHG3jxI2oC&amp;redir_esc=y" target="_blank">MS Project for Dummies</a></li>
                        <li><a href="/project/lessons/adding-resources.php">Adding Resources in Project</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">MS Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project review page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>