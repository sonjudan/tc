<?php
$meta_title = "Task duration in Auto Schedule mode | Training Connection";
$meta_description = "Learn How Project recalculates the task duration in Auto Schedule mode";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Task Duration in Auto Schedule mode</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>How Project recalculates the task duration in Auto Schedule mode</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/auto-scheduling.jpg" width="780" height="327" alt="Task durations in Auto Schedule mode"></p>
                <p>Assigning a Resource to a Task <strong>defines the hours of Work</strong> for the Task: </p>

                <p><em><strong>WORK = DURATION hours </strong></em><strong>x<em> RESOURCE UNITS</em></strong></p>
                <p>When you <strong>first</strong> allocate Resource(s) to a Task, the <strong>initial</strong> hours of Work for <strong><u>each</u></strong> Resource on the Task are calculated as the Task Duration expressed in hours.</p>
                <p>When <strong>subsequent changes</strong> are made to the <strong>number of resources</strong> assigned to the task <u>or</u> to any of the allocated Resource <strong>Units </strong><u>or</u><strong> </strong>to<strong> </strong>the<strong> Resource Work hours</strong>, the Task Duration is <strong><em>automatically recalculated</em></strong> <strong><em>for each Resource </em></strong>in the course of recalculating the Task Duration:</p>
                <p>For each allocated resource, the following is automatic:</p>

                <h4>Resource Duration Hours = Resource Work / Resource Units</h4>
                <ol>
                    <li>Project identifies the Resource which spends the longest number of hours in real time on the Task as calculated by the above expression -  this Resource is termed the ‘Driving Resource’.</li>
                    <li>The task duration is automatically recalculated to the hours between the first resource starting work on the task and the last resource finishing work on the task, in accordance with their Resource Calendars.</li>
                    <li>By default, if you modify the split of the Resources’ Work, MS Project will recalculate the Duration as above.</li>
                    <li>If you modify the Resources’ Units on the Task, MS Project will recalculate the Duration as above.</li>
                    <li>If you modify the Duration, MS Project will recalculate the Work for the Driving Resource(s). <a href="/ms-project-training-chicago.php">Chicago Project classes</a>.</li>
                    <li>By default, MS Project will not change the Resources’ Units on the Task.</li>
                    <li>On a Task allocated to multiple Resources which do not all share the same Calendar From and To daily working hours settings, the start and finish of the Task will be determined by the above Driving Resource calculation in combination with the individual Resource Calendars’ From and To times.</li>
                </ol>

                <p><strong>NOTE: Resources do not have to be assigned to Summary Tasks</strong></p>

                <p>Need to learn MS Project? Why not join one of our small <a href="/ms-project-training.php">Microsoft Project classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

            </div>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Microsoft Project class testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>