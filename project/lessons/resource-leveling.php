<?php
$meta_title = "Assigning resources to tasks in MS Project | Training Connection";
$meta_description = "Learn about adding resources in Microsoft Project's Auto Schedule mode. Step by step instructions";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Leveling Resources</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Resource Leveling in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article we take you through 3 examples of how to level rescources in  MS Project. Start by looking at the project in the <strong>Resource Usage</strong> view. In image below you will  see that three Resources are over allocated. This means that:</p>

                <ul>
                    <li>the project schedule is incorrectly planned; and/or </li>
                    <li>your resources really are literally over allocated; and/or</li>
                    <li>your resources have been incorrectly set up for this project. </li>
                </ul>
                <p>In fact, in this project all these factors exist and are causing the project to be incorrectly represented and scheduled.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources.jpg" width="800" height="407" alt="Resource Leveling"></p>

                <p>Need to learn MS Project? Why not join one of our small  <a href="/ms-project-training.php">Microsoft Project training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <ol>
                    <li>Check the first over allocation for <strong>Bryan Hayden</strong>.</li>
                </ol>

                <p>In the resource usage view you can see quite clearly that there are a number of days where Bryan is working more than 8 hours a day.Either the allocation is illogical, or he is simply working too many hours, which could lead to high extra costs or work not being completed. </p>

                <p>Analysis of the Resource Usage view will show that from 9 September Bryan is spending 8 hours per day viewing available plots. This means that if he is required to undertake another task during the day he will appear over-allocated on the system, which is clearly illogical.</p>

                <h4><strong>To resolve this over-allocation</strong></h4>

                <p>The link between tasks 3 and 4 is <strong>Start to Start</strong>. Whilst it is true that task 4 (make an offer on a plot) can come at any time within the one month period that has been allocated to viewing plots, it cannot take place, logically, until viewing has stopped. <a href="/ms-project-training-los-angeles.php">Microsoft 2019 training in Los Angeles</a>.</p>

                <p>One solution here is to make the link between the tasks <strong>Finish to Start</strong>. That is to say that, although we do not know when viewing will finish (i.e. when a suitable plot is found), it must be finished before the next task (making an offer) can start. In your own planning you might consider various other ways of restructuring the project. For example, the omission altogether of the task called ‘Make an offer’ and consider it as part and parcel of the Viewing task. In which case, the next task would be Pay Deposit. </p>

                <p>This would be a partial, and logical solution. However, you would still end up with the same problem regarding a finish to start link, unless you physically moved the next task or allowed an over allocation.</p>

                <p>For the sake of this exercise, let’s assume that Bryan will stop viewing when he makes an offer. So on the Gantt Chart, double click the link line between tasks 3 and 4 and change the relationship to Finish-to-Start:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-dependency.jpg" width="500" height="" alt="Task Dependency"></p>

                <p>You will note that the resource, Bryan Hayden, is no longer over allocated.</p>

                <p>Check the over allocation for the <strong>Amenity Contractor</strong>.</p>

                <p>This resource is committed to a total of three tasks simultaneously and at 100 % availability.&nbsp; However, this resource is a contractor and, as per the Resource notes <strong>‘Has own workforce for projects’</strong>. </p>

                <p>Since this resource CAN work on three tasks simultaneously due to having his own resources and team, we can set his percentage availability to 300% - on the Resource Sheet increase the percentage and then note that this clears the over allocation of this resource.</p>
                Need <a href="/onsite-training.php">onsite Microsoft Project training</a>?<br clear="all">

                <p>The third over allocation is the <strong>Architect</strong>.</p>

                <p>Checking the logic of the tasks on the Gantt Chart - it makes sense that:</p>

                <ul>
                    <li>Build Double Garage is dependent on the finished tasks of: Design Double Garage and the Installation of Amenities</li>
                    <li>Build House is dependent on Design House.</li>
                    <li>Build Swimming Pool is dependent on Swimming Pool.</li>
                </ul>
                <p>The problem is the lack of resources - since we only have one architect, he/she cannot work 100% on three different tasks simultaneously and therefore we need to change the sequence of the tasks. </p>
                <ul>
                    <li>In the Gantt Chart, change the Design Garage Task to Manually Scheduled and type a start date of 13 Oct</li>
                    <li>Change the Design House Task to Manually Scheduled and drag the Gantt bar to the right to fit when the architect is available (this is the same as typing in a date)</li>
                </ul>

                <p>This should solve the over allocation problem and your project should look similar to the following will all resources 'levelled':</p>

                <p><a href="https://software.grok.lsu.edu/article.aspx?articleid=7465"></a></p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/leveled-resources.jpg" width="800" height="112" alt="Levelled Taks"></p>

                <p>Also see <a href="https://www.izenbridge.com/blog/underlining-the-differences-between-resource-leveling-and-resource-smoothing/">difference between levelling and smoothing resources</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project class reviews</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>