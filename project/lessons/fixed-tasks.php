<?php
$meta_title = "Fixed Tasks in Microsoft Project | Training Connection";
$meta_description = "Learn how to create fixed tasks in Microsoft Project. This lesson is part of our comprehensive MS Project Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Fixed Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Fixed Tasks in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For classroom based <a href="/ms-project-training.php">Microsoft Project training</a> in Chicago and Los Angeles delivered by trained instructors, call us on 888.815.0604.</p>
                <p>In this article, we will learn about:</p>
                <ul>
                    <li>Fixed duration tasks</li>
                    <li>How to set and modify fixed tasks</li>
                </ul>

                <h2>About Fixed Duration Tasks</h2>
                <p>There are three types of tasks in Project: Fixed Duration, Fixed Units, and Fixed Work. Fixed Duration tasks have a fixed duration that will not change, Fixed Units tasks will have constant fixed units, and Fixed Work tasks will have the work remain constant.</p>
                <p>By default, all tasks are of the Fixed Units type unless otherwise specified by you. If you adjust the amount of resources assigned to this type of task after the first resource assignment, the task duration will need to be adjusted to accommodate for this change.</p>
                <p>If Fixed Duration were the task type instead, either the work for the task or the units need to change when additional resources are added; the duration of that task will remain constant. The assignment of additional resources to a task requires either the amount of work for the task to be increased, or the assigned units for each resource be decreased. <a href="/ms-project-training-chicago.php">MS Project classes in the Chicago Loop</a>?</p>
                <p>For example, let’s say that we have a task with a fixed duration of 40 (hours), work equal to 80 (hours), and two resource units:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/fixed-duration-task.jpg" alt="task with fixed duration"></p>
                <p>Now let’s say that another resource is added to this task. Project has three choices: reduce the duration (but keep the same amount of work), increase the work required (while keeping the duration), or decrease the units from each resource. Typically when you add a resource, you will see an action button allowing you to choose which aspect of the equation changes:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-added-to-task.jpg" alt="resource variable added"></p>
                <p>Always keep in mind the type of task you are working with when you are making changes to your project.</p>

                <h2>Setting and Modifying Fixed Tasks</h2>
                <p>To modify a task’s task type, first double-click on the task in question from inside the Task Entry Sheet. For this example, double-click “Task 1:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/modify-task-type.jpg" alt="task entry sheet"></p>
                <p>The Task Information dialog will now be on your screen. Click the Advanced tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/advance-tab-task-info.jpg" alt="click advance tab"></p>
                <p>For this example, leave the default setting (Fixed Units) selected.</p>
                <p>Once the task type has been selected, you can then modify its various settings. While modifying a fixed task is a fairly straightforward process, you need to be cautious as any changes you make may adversely affect the task in question.</p>
                <p>For example, suppose that you have a Fixed Work task that involves a single resource. If you decrease the number of resource units assigned, the resource will have to work longer, and therefore Project will increase the duration to accomplish the fixed amount of work. On the other hand, if you decrease the duration, the resource units must increase to achieve the fixed amount of work required in the reduced time. The parts of the equation that are not fixed will adjust to achieve balance depending on what is changed.</p>
                <p>Fixed Units tasks require a fixed amount of units to complete the work based on its first resource assignment. Therefore, if you assign more resources, the work will be done faster and the faster this task will be completed.</p>
                <p>Remember that Fixed Duration tasks are a bit different. Because the duration cannot change, some other term in the equation must adjust to compensate if the work or resources is increased. Summary tasks, for example, are considered fixed duration, because their length depends on how long their subtasks take.</p>
                <p>Click OK to close this dialog and apply any new settings.</p>
            </div>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Find out just how satisfied our students are with a training course centered around success on our <a href="/testimonials.php?course_id=22">Student Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>