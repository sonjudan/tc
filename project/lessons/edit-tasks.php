<?php
$meta_title = "Editing Tasks in Microsoft Project | Training Connection";
$meta_description = "Learn how to edit tasks on a Gantt Chart in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editing Tasks on a Gantt Chart</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Editing Tasks on a Gantt Chart</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>One method of editing tasks is to change them on the Gantt Chart using the mouse and dragging:</p>
                <ol>
                    <li>Positioning the pointer at the beginning of a bar will change the pointer to a % sign and dragging with the mouse to the left will update the percentage complete of the task.</li>
                    <li>If the pointer is placed in the centre of the bar it will change to a four-way arrow pointer. It is then possible to drag the bar to the left or right. A label will appear informing you of what you are doing: </li>
                </ol>
                <p>To learn more about our <a href="/ms-project-training.php">hands-on MS Project classes</a> offered in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-task.jpg" width="592" height="388" alt="Microsoft Project Task"></p>
                <p>If you change a date that then causes a conflict, the Planning Wizard dialog box will alert you of the problem:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/planning-wizard.jpg" width="750" height="563" alt=""></p>
                <p><a href="http://www.mpug.com/articles/microsoft-project-keyboard-shortcuts/" target="_blank">Also see useful Project shotcut keys</a></p>
                <ol start="3">
                    <li>The duration of the task can be changed by changing the length of the bar. If the pointer is positioned at the right end of the bar it will change into a right pointing arrow. You can then drag to change the length of the bar. <a href="/ms-project-training-los-angeles.php">Hands-on Project classes</a> in Los Angeles.</li>
                </ol>
                <p>Also see <a href="/project/lessons/gantt-charts.php">Customizing the Gantt Chart</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite Microsoft Project training</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">MS Project student testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>