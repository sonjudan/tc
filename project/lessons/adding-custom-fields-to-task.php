<?php
$meta_title = "Adding Custom Fields to a Task in MS Project | Training Connection";
$meta_description = "Learn how to add Custom Fields to a Task in Microsoft Project. This lesson forms part of our comprehensive MS Project Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding Custom Fields to a Task</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Custom Fields to a Task in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will take a look at how to add Custom Fields to a Task within MS Projects. This article features easy to follow steps which will guide you through adding Custom Fields to an MS Projects Task.</p>
                <p>For classes dedicated to <a href="/ms-project-training.php">Microsoft Project in Chicago and Los Angeles</a> and delivered by trained instructors, call us on 888.815.0604.</p>

                <h4>Adding Custom Fields to a Task</h4>
                <p>A very useful way in which you can customize Project 2013 is through the addition of custom fields. To add a custom field, first right-click any column header within the tasks list. (The new custom field will be added to the left of the header you right-click.) Next, click Custom Fields:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-custom-fields.jpg" alt="right click to add custom fields"></p>
                <p>The Custom Fields dialog will now be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/custom-fields-dialog.jpg" width="450" alt="custom fields dialog displayed"></p>
                <p>Using the radio buttons at the top of this dialog, you can choose the field category; Task will be selected by default. In the same area, the Type drop-down menu allows you to choose what type of custom field you would like to create. By default this is set to Text, but you have a wide variety of other options to choose from. <a href="/ms-project-training-chicago.php">Small hands-on Chicago Project classes</a>.</p>
                <p>Leave these settings unchanged:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/settings-unchanged.jpg" width="450" alt="displayed as unchanged settings"></p>
                <p>Double-click on the field that you would like to add. For this example, double-click “Text1:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/select-field-to-add.jpg" width="450" alt="double click selected field"></p>
                <p>You can now rename this new field. For this example, type “Module 3-4:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/rename-new-field.jpg" width="450" alt="name new field"></p>
                <p>Press Enter on your keyboard to apply this new change.</p>
                <p>Further down in this dialog, you have the option of adding custom attributes as well as calculations and values to display:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-custom-attributes.jpg" width="450" alt="apply custom attributes"></p>
                <p>For this example, leave these settings unchanged and click OK to add the new custom field:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/ok-to-add-new-field.jpg" width="450" alt="Click OK to finish new field"></p>
                <p>Double-click any task in the sample project to open the Task Information dialog. Click to open the Custom Fields tab. You will see the new custom field that you just created listed here:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-info-dialog.jpg" width="450" alt="task information dialog box"></p>
                <p>Click OK to close the Task Information dialog and then save the changes that you have made to the current project.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p><a href="/testimonials.php?course_id=22">Read real MS Project testimonies</a> from students who have undertaken our Microsoft Project training.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>