<?php
$meta_title = "Delaying Tasks and Assigning variable units to a Task in MS Project | Training Connection";
$meta_description = "Learn how to delay Tasks and assign variable units to a Task in MS Project. This lesson features elements from our MS Project Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Delaying Tasks and Assigning variable units to a Task</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Delaying Tasks and Assigning variable units to a Task in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will delve a little deeper into how to delay tasks as well as assign variable units to a task in MS Projects. This multi purpose guide takes a step-by-step approach to task management within Microsoft Project.</p>
                <p>For <a href="/ms-project-training.php">MS Project training in Chicago and Los Angeles</a> delivered by knowledgeable instructors in a friendly classroom based environment, call us on 888.815.0604.</p>

                <h4>Delaying Tasks</h4>
                <p>In this article, we will learn how to:</p>
                <ul>
                    <li>Delay tasks</li>
                    <li>Assign variable units to a task</li>
                </ul>
                <p>As projects can inevitably incur delays, you can quickly have individual tasks reflect these delays when required. The most straightforward method to delay a task is to drag the start of a task ahead on the Gantt chart. For the example, suppose that “Task 4” on the sample project has been delayed by two days as John Smith (the resource assigned to it) has gotten sick. Click and drag the Gantt bar for this task in a forward direction until you see “Fri 1/23/15” displayed as a Task Start date on the ScreenTip:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-on-gantt-chart.jpg" alt="gantt chart placement"></p>
                <p>Release your mouse button. As you’ve moved “Task 4” away from the task that it is linked to (“Task 3”), the Planning Wizard dialog will now be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/planning-wizard-dialog.jpg" alt="planning wizzard displayed"></p>
                <p>In this scenario you have the option to move the task and remove the link between these two tasks, or move the task and keep it. You can also cancel this action entirely. Leave the default radio button selected and click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/default-radio-button.jpg" alt="leave default settings"></p>
                <p>The delayed start date will now be applied to the selected task and its link to the previous task will have been removed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/delayed-start-date.jpg" alt="delayed date applied"></p>

                <p>Occasionally a resource may be required for an unforeseen, but high priority task. If this is the case, you may split the original task to accommodate the resource’s availability. The resource attached to this task will be available during the split for the higher priority task, without creating a conflict. <a href="/ms-project-training-los-angeles.php">Certified Project classes in Downtown Los Angeles</a>.</p>
                <h4>Assigning Variable Units to a Task</h4>
                <p>Project allows you to adjust how much a resource is available over the duration of your project. Generally this is used when you have a resource who is spread across a variety of projects or duties.</p>
                <p>To assign variable units to a task, first switch to the Resource Sheet view by clicking Task → Gantt Chart → Resource Sheet:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assign-variable-units.jpg" alt="variables assigned to task"></p>
                <p>In this case, you can see that there are two resources dedicated to this project. Double-click the John Smith resource:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-dedicated-to-project.jpg" alt="dedicated resources displayed"></p>
                <p>The Resource Information dialog will now be on your screen. With the General tab open, you will see the Resource Availability table in the lower left-hand corner:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-info-dialog.jpg" alt="resource information box displayed"></p>
                <p>In the Resource Availability table, using the Available From column, you can enter the date that the given resource is first available. For this example, use the date picker to choose January 5th, 2015:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-availability-table.jpg" alt="resources availability"></p>
                <p>Next, enter the date that the resource will stop being available using the Available To date picker. For this example choose February 6th, 2015:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/date-of-resource.jpg" alt="apply date to resource"></p>
                <p>Now you can enter the level of units that are available for the current resource (and availability range) using the Units column. Type 75% into the Units box on the first row:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/level-of-units.jpg" alt="enter level of units"></p>
                <p>Click OK to apply these new changes:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/apply-new-changes-project.jpg" alt="changes applied"></p>
                <p>Returning to the Resource Sheet view, you will see that this resource is now over-allocated with the new availability restrictions.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Other resources</h4>
                    <ul>
                        <li><a href="https://books.google.com/books?id=P5ttR3SgsRQC&amp;pg=PA39&amp;source=gbs_toc_r&amp;cad=4#v=onepage&amp;q&amp;f=false">Steps to plan a Project</a></p><p><a href="https://products.office.com/en-za/Project/project-professional-desktop-software?legRedir=true&amp;CorrelationId=cf0f378d-2fca-4e48-856a-8460613fbdec" target="_blank" of using Microsoft Project</a></li>
                        <li><a href="https://support.office.com/en-us/article/Office-2013-Quick-Start-Guides-4a8aa04a-f7f3-4a4d-823c-3dbc4b8672a1?ui=en-US&amp;rs=en-US&amp;ad=US" target="_blank">Microsoft Office Quick Start Guides</a><p></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>We have thousands of satisfied students who have completed our MS Project course. Read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>