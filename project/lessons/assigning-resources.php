<?php
$meta_title = "Assigning resources to tasks in MS Project | Training Connection";
$meta_description = "Learn about adding resources in Microsoft Project's Auto Schedule mode. Step by step instructions.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding Resources to Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Assigning resources to tasks - Auto Schedule Mode</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article we will show you step-by-step how to add resources to tasks in MS Project's Auto Schedule Mode.</p>
                <p>Need to learn MS Project? Why not join one of our small  <a href="/ms-project-training.php">Microsoft Project classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Steps</h4>
                <ol>
                    <li>In the Gantt Chart view, select the Task</li>
                    <li>On the Resource tab of the ribbon click the Assign Resources command</li>
                    <li>In the Assign Resources dialog box, from the Resource list select the Resource and click the Assign button:</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assigning-resources- dialogue-box.jpg" width="500" height="" alt="Assign Resources Dialogue Box"></p>

                <p>Note: the Resource’s Units on this Task are by default set to the Resource’s Maximum Units for this project set in the Resource Sheet, but you can edit the Resource’s Units on this Task to a lower % if required. The costs for a work type resource are displayed but cannot be amended here.</p>

                <h6>Tips:</h6>
                <ul>
                    <li>you can leave the dialog box open for assigning to other Tasks</li>
                    <li>to assign the same Resource(s) to more than one Task at a time, select the Tasks before using the Resource Assignment dialog box </li>
                    <li>to assign more than one Resource to Tasks, select the Task(s) and select the Resources in the dialog box before clicking the Assign button</li>
                    <li>use SHIFT or CTRL to select contiguous or non-contiguous Tasks</li>
                    <li>the Task Duration does <strong><u>not</u></strong> change on the first assignment of Resources to a Task, even if you assign more than one Resource - the Work hours for <strong>each</strong> Resource are by default equivalent to the full Duration. However, if you <strong>later</strong> assign <strong>additional</strong> Resources to the Task, the workload is shared - see below on how to have <strong>complete</strong> <strong>control over the split of the work between multiple resources on a task</strong></li>
                    <li>by default the resource name is displayed on the Gantt chart - you can choose to display the initials (or any other data) against each Task bar - (this is covered later in the course). </li>
                </ul>
                <p><a href="https://software.grok.lsu.edu/article.aspx?articleid=7465">GanttProject: Adding a Resource</a></p>

                <h4>Assistance in finding resources with available working time</h4>
                <h6>In the Assign Resources dialog box:</h6>
                <ol>
                    <li>Click the Resource list options to expand the dialog box </li>
                    <li>Tick the Filter by check box and make sue All Resources is displayed (alternatively you can filter for a specific resource group, etc) </li>
                    <li>Tick the ‘Available to work’ check box and enter the number of hours availability for which you are searching</li>
                    <li>The list of resources will be automatically filtered:</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assign-resources.jpg" width="500" height="" alt="Assigning resources"></p>
                <p><strong>Note</strong>: In the Assign Resources dialog box, there is a Graph button. This feature has changed from the previous versions of project. When you click the Graph button you will see a split screen view with the Gantt chart in the top half of the screen and the Resource Graph view in the bottom half. You can right click the graph to display further details:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gantt-resources.jpg" width="780" height="407" alt="assigning resouces on a Gannt chart"></p>

                <h4>Assigning additional resources to reduce task duration</h4>
                <p>If you later assign additional resources to a task, by default the work on the task is initially <strong>shared equally between the Resources</strong> and the <strong>Duration is reduced</strong>.&nbsp;However, you can modify the work allocated to each resource using the Task Entry View:</p>
                <ol start="1" type="1">
                    <li>In the Gantt Chart, click the Details command on the Task tab to split the screen and place the Task Form in the bottom half.</li>
                </ol>

                <ol start="2" type="1">
                    <li>Right click the form and choose <strong>Work:</strong></li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assigning-additional-resources.jpg" width="780" height="729" alt="Adding additional resources"></p>

                <ol start="3" type="1">
                    <li>Assign another resource using the drop-down list of resources as shown above. <a href="/ms-project-training-chicago.php">Hands-on MS Project training</a>.</li>
                    <li>In the form, you can edit the Work against each Resource assigned to the Task to give your required split of Work - the Task Duration is automatically recalculated (you are <strong><u>not</u></strong> bound to stay with the original total hrs work).</li>
                    <li><strong><u>Click the Form’s OK button</u></strong> (pressing Enter is not the same here as OK).</li>
                    <li>In the form, you can also change the Units of each resource on the task and this will also recalculate the duration of the task.</li>
                    <li>Edit the Task Duration in either half of the screen and notice that the work for the work for the <strong>Driving</strong> <strong>Resource(s)</strong> is automatically recalculated</li>
                </ol>
                <p>Also see <a href="/project/lessons/task-duration.php">How Project recalculates the task duration in Auto Schedule mode</a></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project training testimonials page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>