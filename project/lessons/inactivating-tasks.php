<?php
$meta_title = "Inactivating Tasks in MS Project | Training Connection";
$meta_description = "Learn about inactivating tasks and updating task completion in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Inactivating Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Inactivating Tasks in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article,  will delve a little deeper into inactivating tasks and updating task completion within Projects.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <ul>
                    <li>Inactivate tasks</li>
                    <li>Update task completion</li>
                </ul>
                <h4>Inactivating Tasks</h4>

                <p>Tasks in Project may be marked as inactive to prevent them from having an effect on the schedule or resource availability without having to delete them entirely. For example, you could mark a task an inactive if you cannot complete it now, but you may be able to later in the project.</p>

                <p>To mark a task as inactive, first select the task in question from the tasks list and then click Task → Inactivate. For this example, inactivate the “Revise Proposal if Needed” task:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/inactive-task-from-list.jpg" alt="select inactive task from list"></p>
                <p>Once a task has been inactivated, it will appear in a light gray font with strikethrough formatting. Its instance on the Gantt chart will also appear grayed out:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gray-inactivated-task.jpg" alt="gray inactivated task with strike through"></p>
                <p>You may reactivate a task by following the same steps outlined above: click to select the task in question and then click Task → Inactivate.</p>
                <p>Close Microsoft Project 2013 and save your changes. <a href="/ms-project-training-chicago.php">Click here</a> for information on Chicago MS Project classes.</p>

                <h4>Updating Task Completion</h4>
                <p>To update the completion percentage of a task, you first need to open its Task Information dialog. To do this, you can double-click the task or select it and click Task → Information. For this example, open the Task Information dialog for the “Brainstorm Ideas” task (#16):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/opening-task-infomation-dialog.jpg" alt="opening task infomation dialog box"></p>
                <p>In the Task Information dialog, you will see the “Percent complete” increment box:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/percentage-complete-box.jpg" alt="percentage complete in dialog box"></p>
                <p>Here, you may type a new value or use the increment arrows to select a value. Use either method to enter a 50% completion value. When you’re done, click OK:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/select-percentage-value.jpg" alt="selecting percentage value in dialog box"></p>
                <p>The Gantt chart will now reflect the new completion level for the selected task:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/completion-level-in-gant-chart.jpg" alt="completion level as seen on gant chart"></p>
                <p>Alternatively, you may apply set completion levels by clicking the 0%, 25%, 50%, 75%, or 100% commands on the Task tab:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/completion-level-on-task-tab.jpg" alt="selecting completion level on task tab"></p>
                <p>These commands can also be found on the mini toolbar when right-clicking a task in the Gantt chart:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/command-on-mini-toolbar.jpg" alt="select command on mini toolbar"></p>
                <p>Any tasks that are marked as 100% complete will be considered finished and a completed task indicator will appear beside them. Here, you can see that the “Brainstorm Ideas” task is complete:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/complete-task-indicator.jpg" alt="complete task indicator"></p>

                <p>Close Microsoft Project 2013 and save your changes.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">MS Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>