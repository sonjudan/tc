<?php
$meta_title = "Creating a Timeline in MS Project | Training Connection";
$meta_description = "Learn how to create a timeline in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating a Timeline</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating a Timeline in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In this article,  will delve a little deeper into how to create a timeline within Projects.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>In this article, we will learn how to:</p>
                <ul>
                    <li>Show and hide the timeline</li>
                    <li>Customize timeline tasks</li>
                    <li>Change the font for individual timeline items</li>
                </ul>

                <h4>Showing and Hiding the Timeline</h4>
                <p>By default, no matter what view you have applied, the timeline will always be displayed just below the ribbon. If you don’t need it, you can hide it to give yourself more room to work inside the Project window. To hide the timeline, click the View tab and deselect the Timeline checkbox:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/deselect-timeline-checkbox.jpg" alt="deselect timeline checkbox"></p>
                <p>The timeline will now be hidden:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/hidden-timeline.jpg" alt="hidden timeline"></p>
                <p>Show the timeline again by ensuring that the Timeline checkbox on the View tab is selected. Close Microsoft Project 2013 without saving your changes. <a href="/ms-project-training-chicago.php">Instructor-led Microsoft Project classes in Chicago</a>.</p>

                <h4>Customizing Timeline Tasks</h4>
                <p>To add an existing task to the timeline, first select the task in question. For this example, select the “Choose New Product” task. Then, click Task → Add to Timeline:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-to-timeline.jpg" alt="add to timeline"></p>

                <p>The selected task will now appear on the timeline:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-appear-on-timeline.jpg" alt="task appears on timeline"></p>
                <p>Alternatively, you can add existing tasks to the timeline by clicking anywhere on the timeline to select it and then clicking Timeline Tools – Format → Existing Tasks:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/format-existing-tasks.jpg" alt="format existing tasks"></p>
                <p>Once this command has been selected, the Add Tasks to Timeline dialog will open. This dialog lists all of the tasks that are found in your project. You may add or remove any of these tasks by clicking their corresponding checkboxes. For this example, ensure that the checkboxes for both of the summary tasks (“Choose New Product” and “Design Prototype”) are selected. Click OK:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-tasks-to-timeline-checkbox.jpg" alt="add tasks to timeline checkbox"></p>
                <p>Both summary tasks will now appear on the timeline:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/summary-tasks-on-timeline.jpg" alt="summary tasks on timeline"></p>
                <p>Remove the “Choose New Product” summary task from the timeline by right-clicking on its timeline entry and clicking “Remove from Timeline:”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/remove-from-timeline.jpg" alt="remove from timeline"></p>
                <p>Close Microsoft Project 2013 and save your changes.</p>

                <h4>Changing the Font for Individual Timeline Items</h4>
                <p>To adjust the font used for each task on the timeline, click to select the task in question on the timeline. For this example, select “Choose New Product.” Then, click in the timeline to select it. Examine the controls found inside the Font group of the Timeline Tools – Format tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/adjust-font-on-timeline.jpg" alt="adjust font on timeline"></p>
                <p>Here, you can see controls to apply various text effects; change the font type, color, size; and more. For this example, increase the font size to 12. Also, apply the underline effect:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/text-effects-box.jpg" alt="text effects box"></p>

                <p>The changes will now be applied to the selected task:</p><p><img src="https://www.trainingconnection.com/images/Lessons/project/font-changes-applied.jpg" alt="font changes applied"></p>
                <p>Close Microsoft Project 2013 and save your changes.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>