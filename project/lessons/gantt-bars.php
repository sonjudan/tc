<?php
$meta_title = "Adding Gantt Bars in Microsoft Project | Training Connection";
$meta_description = "Learn how to add customized Gantt Bars in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gantt Bars</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Gantt Bars in MS Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>If you want to spotlight a particular task <strong>category</strong> that is not represented by its own Gantt bar, you can create a new Gantt bar. For example, you can create a Gantt bar to show available slack or to call attention to delayed tasks.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project Professional training</a> in Chicago and Los Angeles call us on 888.815.0604. </p>
                <ol>
                    <li>On the <strong>Format</strong> tab, click Format and select <strong>Bar Styles</strong>.</li>
                    <li>In the Gantt bar list, click the <strong>Insert Row </strong>button.</li>
                    <li>In the <strong>Name</strong> column of the new row, type a name for the new bar style. Example below - ‘<strong>Yet to start</strong>’. </li>
                    <li>
                        <p>In the <strong>Show For Tasks</strong> column of the new row, type or select the task type you want the bar to represent. Example below - <strong>Not Started</strong>.</p>
                        <p>To exclude tasks with a specific bar type, type <strong>not</strong> before the task type. For example, you can define a bar type as <strong>not milestone</strong> to display only tasks that are not milestone tasks.</p>
                        <p>To display a Gantt bar for tasks of multiple types (such as tasks that are milestones and critical), type a comma (,) after the task category in the text entry box, and type or select a second task category.</p>
                    </li>
                    <li>In the <strong>From</strong> and <strong>To</strong> columns, type or select the fields you want to use to position the start and finish points of the new Gantt bar. For example, to create a symbol that represents a single date, type or select the same field in the <strong>From</strong> and <strong>To</strong> columns.</li>
                    <li>Click the <strong>Bars</strong> tab, and then under <strong>Start</strong>, <strong>Middle</strong>, and <strong>End</strong>, select shapes, patterns or types, and colours for the bar.</li>
                    <li>Click the Text tab and choose the type of information you want to print next to your new Gantt bar</li>
                    <li>Click OK. <a href="/ms-project-training-los-angeles.php">Looking for Project training in LA</a>?</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gantt-bars.jpg" width="750" height="467" alt="Bar styles in Microsoft Project"></p>
                <p><a href="https://my.kettering.edu/sites/default/files/resource.../project-2013-quick-reference.pdf">Get a Project 2013 Cheatsheet</a>.</p>
                <p><img class="imageleft" src="https://www.trainingconnection.com/images/Lessons/project/gridlines.jpg" width="200" height="185" alt="Menu>Gridlines"></p>
                <p>Click the Gridlines command and choose Gridlines to display the Gridline dialog box. You can change the display of the vertical lines for the current date and project finish date line, etc:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gridlines-dialogue-box.jpg" width="600" height="279" alt="Gridline Dialogue Box"></p>
                <p>Click the Layout command to display the Layout dialog box where you can change how Links are displayed as well as the bar height, etc:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/Gantt-bar-layouts.jpg" width="750" height="536" alt="Gantt Bar layout"></p>
                <p>Also see <a href="/project/lessons/edit-tasks.php">Editing Tasks on a Gantt Chart</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite MS Project training</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project student testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>