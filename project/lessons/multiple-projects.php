<?php
$meta_title = "Multiple Projects in MS Project | Training Connection";
$meta_description = "Learning to work with multiple project in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Multiple Projects</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Multiple Projects in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This module explains how to handle multiple projects. You’ll learn how to create links between projects. Since working with a single file is always faster if you can help it, this module will also explain how to consolidate projects. You’ll learn how to view multiple project critical paths and consolidated project statistics. Finally, this module explains how to create a resource pool. Also view <a href="/project/lessons/costs.php">how to add costs to projects</a>.</p>
                <p>For an instructor-led <a href="/ms-project-training.php">Project training classes</a> in Chicago and Los Angeles call us on 888.815.0604. </p>

                <h4>Inserting a Subproject</h4>
                <p>Use the following procedure to link a project in a master project</p>
                <ol>
                    <li>In the blank project, from the <strong>Gantt Chart</strong> view, highlight the row where you want to insert the project. </li>
                    <li>Select the <strong>Project</strong> tab.</li>
                    <li>Select <strong>Subproject</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/subproject.jpg" width="780" height="215" alt="Inserting a Subproject"></p>
                <ol start="4">
                    <li>Highlight the project you want to insert.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/insert-project.jpg" width="732" height="464" alt="Insert Project Dialog Box"></p>

                <p><strong>Note:</strong> To insert multiple projects, hold down CTRL and click the projects in the order that you want to insert them. <a href="/ms-project-training-chicago.php">MS Project classes run in Chicago</a>.</p>
                <ol start="5">
                    <li>Make sure the <strong>Link to project</strong> box is checked. However, if you do not want to update the subprojects with any changes made to the master project, clear the box.</li>
                    <li>Select <strong>Insert</strong>.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/link-to-project.jpg" width="780" height="212" alt="Link to Project Box"></p>

                <p>View <a href="https://support.office.com/en-us/article/Keyboard-shortcuts-for-Project-2013-b3aab44f-3eed-47a4-8cb5-f8d840d4c8cc">shortcuts for Microsoft Project 2013</a></p>

                <h4>Consolidating Projects</h4>
                <p>To consolidate projects for printing, use the following procedure.</p>
                <ol>
                    <li>Open the project files you want to combine.</li>
                    <li>Select <strong>New Window</strong> from the <strong>View</strong> tab on the Ribbon.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/new-window.jpg" width="780" height="105" alt="View>New Window"></p>
                <ol start="3">
                    <li>
                        <p>Highlight the first file you want to appear in the consolidated window. Hold the CTRL key to select subsequent projects in the order you want them to appear.</p>

                        <img src="https://www.trainingconnection.com/images/Lessons/project/order-projects.jpg" width="328" height="260" alt="Order projects ">
                    </li>
                    <li>Select a <strong>View</strong> option from the drop down list.</li>
                    <li>Select <strong>OK</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/multiple-project-added.jpg" width="780" height="426" alt="Multiple Project view"></p>

                <h4>Viewing Multiple Project Critical Paths</h4>
                <p>Use the following procedure to display multiple critical paths.</p>
                <ol>
                    <li>Select the <strong>File</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Options</strong>. </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/file-options.jpg" width="780" height="571" alt="File>Options"></p>

                <p>Project opens the <em>Project Options</em> dialog box.</p>
                <ol start="3">
                    <li>Select the <strong>Advanced</strong> tab.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-options-dialog-box.jpg" width="581" height="474" alt="Options Dialog Box"></p>
                <ol start="4">
                    <li>Check the <strong>Calculate Multiple Critical paths</strong> box.</li>
                    <li>Select <strong>OK</strong>.</li>
                    <li>Select <strong>More Views</strong> from the <strong>View</strong> menu.</li>
                    <li>Select <strong>Detail Gantt</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/more-views.jpg" width="296" height="230" alt="More Views>Detailed Gantt"></p>
                <ol start="8">
                    <li>Select <strong>Apply</strong>.</li>
                </ol>

                <h4>Viewing Consolidated Project Statistics</h4>
                <p>Use the following procedure to review the project statistics.</p>
                <ol>
                    <li>Select <strong>Project Information</strong> from the <strong>Project</strong> tab.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-information-2.jpg" width="780" height="108" alt="Project Information"></p>
                <p>Project opens the <em>Project Information</em> dialog box.  </p>

                <ol start="2">
                    <li>Select the <strong>Statistics</strong> button.</li>
                </ol>
                <p>Project displays the <em>Project Statistics</em> dialog box.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-stats.jpg" width="681" height="327" alt="Project statistics"></p>

                <h4>Creating a Resource Pool</h4>
                <p>Creating a resource pool.</p>
                <ol>
                    <li>The project that includes the resources becomes the resource pool. Open that file, plus the file that will share resources, which is the sharer project.</li>
                    <li>Select the sharer project from the <strong>Window</strong> area of the <strong>View</strong> tab.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/sharer-project.jpg" width="780" height="106" alt="View>Task Usage"> </p>
                <ol start="3">
                    <li>Select the <strong>Resource</strong> tab.</li>
                    <li>Select <strong>Resource</strong> <strong>Pool</strong>.</li>
                    <li>Select <strong>Share</strong> <strong>Resources</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/share-resources.jpg" width="780" height="267" alt="Resources Pool > Share Resources"> </p>

                <p>Project displays the Share Resources dialog box.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/share-resources-box.jpg" width="310" height="204" alt="Share Resources Dialog Box"></p>

                <ol start="6">
                    <li>Indicate whether to use the project’s own resources or the resources from another project. If you select <strong>Use Resources From</strong>, select the project from the drop down list. </li>
                    <li>Select how to handle resource conflicts. This indicates whether to overwrite any duplicate resource information, such as rates.</li>
                    <li>Select <strong>OK</strong>.</li>
                </ol>
                <p>View the Resource Sheet to see that the resources are now available to use in the active project. If your project already had resources entered, the resources from both of the projects are combined.</p>

                <p><ig src="https://www.trainingconnection.com/images/Lessons/project/resource-sheet.jpg" width="780" height="254" alt="Resource Sheet"></p>
                <p>Visit our <a href="/resources/project.php">Project resources page </a>for more tutorials.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Project student reviews</h4>
                <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project trainees' testimonial page</a>.</p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>