<?php
$meta_title = "Adding Tasks in MS Project | Training Connection";
$meta_description = "Learn about adding Tasks in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Tasks in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article,  will delve a little deeper into understanding tasks. Project 2010 introduces manually scheduled tasks. You can also schedule tasks automatically using the Project scheduling engine. We’ll discuss the key terms for understanding tasks in this module. We’ll also learn how to view task information and sort and filter tasks. Finally, we’ll take a look at task indicators.</p>
                <p>For instructor-led <a href="/ms-project-training.php">MS Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Understanding Key Terms</h4>
                <p>The key terms for understanding tasks in Project are:</p>
                <ul>
                    <li>Duration</li>
                    <li>Start date</li>
                    <li>Finish date</li>
                    <li>Gantt Chart</li>
                </ul>
                <ul>
                    <li><strong>Duration</strong> - is the amount of time a task will take to complete. When you are using manually scheduled tasks, you can enter anything that will help you schedule the tasks, such as “a couple of days” or “need to talk to Bob.” For automatically scheduled tasks, you must put a number with a unit indicator, such as d for days, h for hours, or w for weeks.</li>
                    <li><strong>Start Date</strong> - is when the project starts. For manually scheduled projects, this can be any text that will help you schedule tasks. For automatically scheduled tasks, it must be a date.</li>
                    <li><strong>Finish Date -</strong> is when the project will be completed. For manually scheduled projects, this can be any text that will help you schedule tasks. For automatically scheduled tasks, it must be a date.</li>
                    <li><strong>Resources </strong>- are the materials, people, and other costs associated with completing the project. Resources do not affect manually scheduled tasks, but do help Project to determine a schedule for automatically scheduled tasks. </li>
                    <li><strong>Gantt Chart -</strong> the default view is the Gantt Chart. The right side of this view includes bars that represent the task duration.</li>
                </ul>

                <h4>Viewing Task Information</h4>
                <p>Use the following procedure to view task information on the <em>Task Information</em> dialog box. </p>
                <ol>
                    <li>Select the task you want to view. You can select the number to the left to highlight the task.</li>
                    <li>Select <strong>Information</strong> from the <strong>Task</strong> tab on the Ribbon.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-tab.jpg" width="750" height="204" alt="Task Tab"></p>

                <p> Review the <em>Task Information</em> dialog box. <a href="/ms-project-training-los-angeles.php">Microsoft Project classes in Downtown Los Angeles</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-information-dialog-box.jpg" width="750" height="465" alt="Task Information Dialog Box"></p>

                <p>The <em>General</em> tab includes the following information:</p>

                <ul>
                    <li>Task <strong>Name</strong> - the name of the task</li>
                    <li><strong>Percent</strong> <strong>complete</strong> - represents a percentage of how much of the task is complete</li>
                    <li><strong>Schedule</strong> <strong>Mode</strong> - select Manually or Auto</li>
                    <li><strong>Duration</strong> - indicates how long the task will take to complete</li>
                    <li><strong>Estimated</strong> - indicates that the selected duration is an estimate</li>
                    <li><strong>Priority</strong> - indicates a priority level in comparison with other tasks</li>
                    <li><strong>Inactive</strong> - makes the task inactive</li>
                    <li><strong>Start</strong> and <strong>Finish</strong> dates - indicates the start and finish date of the task, according to the indicated duration</li>
                    <li><strong>Display</strong> <strong>on</strong> <strong>Timeline</strong> - indicates that Project should display the task on the timeline</li>
                    <li><strong>Hide</strong> <strong>Bar</strong> - indicates that Project should hide the Gantt chart bar</li>
                    <li><strong>Rollup</strong> - indicates that Project should rollup the task with its summary task</li>
                </ul>

                <p>Review the other tabs briefly.</p>
                <p>The <em>Predecessors</em> tab allows you to work with linked tasks and task relationships.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/predecessors-tab.jpg" width="750" height="465" alt="Predecessors tab"></p>
                <p>The Resources tab lists resources assigned to the task. </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resources-tab.jpg" width="750" height="465" alt="Resources Tab"> </p>
                <p>The <em>Advanced</em> tab allows you to set deadlines and constraints. It also includes the task type, WBS code (work breakdown structure) and the earned value method.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/advanced-tab.jpg" width="750" height="465" alt="Advanced Tab"></p>

                <p>The <em>Notes</em> tab allows you to keep textual notes about the task.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/notes-tab.jpg" width="750" height="465" alt="Notes Tab"> </p>
                <p>The <em>Custom Fields </em>tab allows you to include information in fields you have customized for the project.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/custom-fields-tab.jpg" width="750" height="465" alt="Custom Tab"></p>

                <h4>Sorting and Filtering Tasks</h4>
                <p>The View tab includes tools to sort and filter your task list. This can help you find a task in a long list of items, or filter the view to only include certain parts of the project.  </p>
                <p>Use the following procedure to see how to sort tasks. </p>
                <ol>
                    <li>Select the <strong>View</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Sort</strong>. </li>
                    <li>Select a sorting option.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/view-sort.jpg" width="750" height="400" alt="View Tab - Sort"></p>

                <p>Use the following procedure to view the <em>Sort By</em> dialog box. </p>
                <ol>
                    <li>Select the <strong>View</strong> tab from the Ribbon.</li>
                    <li>Select <strong>Sort</strong>. </li>
                    <li>Select <strong>Sort By</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/sort-by-dialog-box.jpg" width="750" height="398" alt="Sort by Dialog Box"> </p>
                <ol start="4">
                    <li>You can select up to three options for sorting, and determine whether they should be sorted ascending or descending. Select <strong>Sort</strong> when you have finished.</li>
                </ol>

                <h4>Understanding Task Indicators</h4>
                <p>The following list explains some of the indicators associated with tasks:</p>
                <ul>
                    <li>Note Task</li>
                    <li>Hyperlink Task</li>
                    <li>Deadline Task</li>
                    <li>Inflexible Constraint</li>
                    <li>Flexible Constraint</li>
                    <li>Recurring Task</li>
                    <li>Complete Task</li>
                    <li>Task Calendar</li>
                </ul>
                <p>Indicators provide valuable information. Remember to check the indicator column to find out details about a task. Point out that other views include indicators as well, such as the Resource view. For some views, you may need to add the Indicator field to show this information. </p>
                <p>Think of the indicators in the sample project file. When you hover over the indicator, Project displays a hint for the meaning of the indicator. </p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Other resources</h4>
                    <ul>
                        <li><a href="https://books.google.com/books?id=P5ttR3SgsRQC&amp;pg=PA39&amp;source=gbs_toc_r&amp;cad=4#v=onepage&amp;q&amp;f=false" target="_blank">Steps to plan a Project</a></li>
                        <li><a href="https://products.office.com/en-za/Project/project-professional-desktop-software?legRedir=true&amp;CorrelationId=cf0f378d-2fca-4e48-856a-8460613fbdec" target="_blank">Benefits of using Microsoft Project</a></li>
                        <li><a href="https://support.office.com/en-us/article/Office-2013-Quick-Start-Guides-4a8aa04a-f7f3-4a4d-823c-3dbc4b8672a1?ui=en-US&amp;rs=en-US&amp;ad=US" target="_blank">Microsoft Office Quick Start Guides</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Related Project Lessons</h4>
                    <ul>
                        <li><a href="/project/lessons/first-project.php">Starting your first Project</a></li>
                        <li><a href="/project/lessons/adding-resources.php">Adding Resource in MS Project</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonials page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>