<?php
$meta_title = "Adding a Hyperlink to a Task in MS Project | Training Connection";
$meta_description = "Learn how to add a Hyperlink to a Task in Microsoft Project. This lesson is part of our Microsoft Project Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Adding a Hyperlink to a Task</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding a Hyperlink to a Task in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will look at how to add a Hyperlink to a Task within Microsoft Projects.</p>
                <p>For instructor delivered <a href="/ms-project-training.php">Microsoft Project training courses</a> available in Chicago and Los Angeles, call us on 888.815.0604.</p>

                <h4>Adding a Hyperlink to a Task</h4>
                <p>In Microsoft Project, you have ability to link tasks to information available elsewhere, such as an external file or an Internet site. There are two ways to go about this, each of which implements the link differently.</p>
                <p>One method to add a hyperlink to a task is to add it using the Notes tab of the Task Information dialog. In the sample project, double-click “Task 1” to open the Task Information dialog for that task:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/note-tab.jpg" alt="note tab on task info dialog"></p>
                <p>Next, in the Task Information dialog, click the Notes tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/click-tab-note.jpg" alt="click tab note on task info dialog"></p>
                <p>With the Notes tab now open, you can add the hyperlink that you want to add to this task. For this example, type the URL for your own organization:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-hyperlink-task-info.jpg" alt="hyperlink added to task"></p>
                <p>You will see that as you type the full website address (e.g. <a href="http://www.example.org">www.example.org</a>), the hyperlink will be created for you automatically. <a href="/ms-project-training-los-angeles.php">Instructor-led Project training</a>.</p>
                <p>Click OK to apply the new settings:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/apply-new-settings.jpg" alt="new setting applied"></p>
                <p>Returning to the task list on the Gantt Chart, you will see a note icon displayed next to the task in question:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/note-icon-displayed.jpg" alt="note icon on gantt chart"></p>
                <p>If you were to double-click on this note icon, the Task Information dialog will open the Notes tab. From there you can then click on the link. The link would then open in your default Internet browser.</p>
                <p>To add a hyperlink that you can access a little more directly, right-click on the task that you want to add the hyperlink to. For this example, right-click on “Task 2.” Next, from the context menu, click Hyperlink:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/right-click-task.jpg" alt="add hyperlink to task directly"></p>
                <p>The Hyperlink dialog will now be on your screen. Using the controls in this dialog, you can add a link to a file or folder on your computer, an external Internet or network location, or even an e-mail address:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/hyperlink-dialog.jpg" alt="hyperlink dialog box displayed"></p>
                <p>For this example, ensure that “Existing File or Web Page” is selected from the “Link to” column section of this dialog. Next, type the URL of your organization into the Address text box:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/link-to-column.jpg" alt="add url to address box"></p>
                <p>While you can customize the display text for a link by replacing the text inside the “Text to display” text box, leave it unchanged for this example. Click OK to apply the new settings:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/customize-display-text.jpg" alt="change text box"></p>
                <p>A hyperlink icon will now appear next to the task that you have been working with:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/hyperlink-icon-displayed.jpg" alt="hyperlink icon appears"></p>
                <p>Move your cursor over this icon and you can see the information that you entered into the “Text to display” field of the Hyperlink dialog. Click the icon and it will open in your default browser:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/hyperlink-text-appears.jpg" alt="cursor displays text"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Read reviews composed by real students who have undertaken MS Project training right here, on our <a href="/testimonials.php?course_id=22">dedicated Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>