<?php
$meta_title = "Understanding the Microsoft Project Interface | Training Connection";
$meta_description = "Learn all the different components that make up the Microsoft Project interface. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project Interface</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Microsoft Project Interface</h1>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>The default view in MS Project is the <em>Gantt Chart</em> view. This consists of data tableon the left hand side of the screen and a Gantt bar chart on the right. The Divider Bar separates both sections and it can be moved to make each side bigger. The Gantt table looks like an Excel worksheet, consisting of rows, columns and cells. The Gantt bar chart graphically displays your project schedule.</p>
                <p>For an instructor-led <a href="/ms-project-training.php">Microsoft Project class</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-interface.jpg" width="750" height="500" alt="MS Project Screen Interface"></p>

                <h4>The Quick Access Toolbar</h4>
                <p>The Quick Access Toolbar is a fully customizable toolbar that displays above or below the Ribbon. Click the drop down menu on the right to customize the Quick Access toolbar. <a href="/ms-project-training-los-angeles.php">LA-based Project classes</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/quick-access-toolbar.jpg" width="296" height="504" alt="How to customize the Quick Access Toolbar"></p>
                <p><strong>Tip:</strong> to add commands to the Quick Access Toolbar, right click any existing command and from the shortcut menu choose <u>A</u>dd to Quick Access Toolbar:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-to-toolbar.jpg" width="500" height="210" alt="Adding commands to the Quick Access Toolbar"></p>

                <h4>The Ribbon</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-ribbon.jpg" width="750" height="113" alt="The Ribbon Menu"></p>
                <p>The Ribbon, consists of a series of tabs. Each tab contains a set of related commands organized into logical groups. Each tab relates to a type of activity, such as file taks, report tasks, view/layout tasks etc.
                    The Format tab is unique in that the commands will change depending on what view or area of the screen is selected.</p>
                <p>The Ribbon can also be minimized so you only see the tabs. To expand simply click on the tab you want to use, and then click the command you want to use.</p>
                <p><strong>Tip:</strong> To quickly minimize the Ribbon, double-click the name of the active tab. Double-click a tab again to restore the Ribbon. The <strong>Keyboard shortcut</strong>&nbsp;to minimize or restore the Ribbon is CTRL+F1. Read more about using the <a href="https://spscc.edu/sites/default/files/imce/students/using_the_ribbon_in_microsoft_office_2013.pdf">Ribbon in Microsoft Office</a>.</p>

                <h4>The Status Bar</h4>
                <p>The status bar sits at the bottom of the screen and is used to indicate whether your tasks are manually or automatically scheduled or when filters are applied.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/new-tasks.jpg" width="750" height="194" alt="Adding new tasks to status bar"></p>
                <p>The Zoom Slider can be found on the right side of the status bar. You can change the Project views between the 4 main views namley the Gantt Chart view, Task Usage view, Team Planner view and Resource Sheet view - using the using the quick buttons to the left of the Zoom.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/quick-buttons-zoom-slider.jpg" width="750" height="52" alt="Quick Buttons and the Zoom Slider"></p>

                <h4>The Project Timeline</h4>
                <p>You can show or hide the Project timeline using the checkbox on the View tab.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/view-tab.jpg" width="492" height="264" alt="View Tab allows to you hide/show the timeline"></p>
                <p>The Timeline allows you to have a bird's eye view of your project plan. This can also be shared with other Office applications such as PowerPoint and Outlook. A useful resource is to download the MS Office product <a href="https://support.office.com/en-US/article/Office-2013-Quick-Start-Guides-4A8AA04A-F7F3-4A4D-823C-3DBC4B8672A1">quick start guides</a>. </p>
                <p>You can use the Timeline to select different parts of the project to display or to zoom into any one area.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-timeline.jpg" width="750" height="325" alt="Timeline Zoom"></p>
                <p>To format the Timeline first select it and then click the Format tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/timeline-tools.jpg" width="750" height="108" alt="Timeline Format Command"></p>
                <p><strong>Note: </strong>When the Timeline is active, several of the commands on the Resources and Task tabs will be greyed out.Deselect the Timeline by clicking on the Gantt chart or Gantt table and then the commands will be available again.</p>
                <p> To share a copy of your timeline with a colleague or client, click on the Copy Timeline command. This will allow you to paste a picture of your project into an email.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/copy-timeline.jpg" width="472" height="416" alt="Copy Timeline Command"></p>
                <p>Use the Existing Tasks command to display specific tasks on the timeline.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-tasks-to-timeline.jpg" width="676" height="402" alt="Add tasks to Timeline"></p>
                <p><strong>Smart Tip:</strong> You can also drag and drop tasks onto the Timeline.</p>
                <p>You can Right click on the Timeline to change how tasks are displayed. They can either be displayed as a bar or as a callout. </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/tasks-on-the-timeline.jpg" width="750" height="138" alt="Timeline"></p>
                <p>You can also apply formatting to tasks on the Timeline. Simply click the relevant task and using the font commands on the format tab.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/font-commands.jpg" width="750" height="227" alt="Timeline - Font Commands"> </p>

                <h4>The Entry Bar</h4>
                <p>The Entry Bar allows you to add and edit the content of a cell.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/entry-bar.jpg" width="500" height="300" alt="The Entry Bar"></p>
                <p>Unlike previous versions of Project, in Project 2013 the Entry Bar is not displayed by default. To display the Entry Bar use one of the following 4 methods:</p>

                <ol start="1" type="1">
                    <li>Click the File Tab</li>
                    <li>Click Options</li>
                    <li>Click Display</li>
                    <li>Click the option to show the element Entry Bar:</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-options.jpg" width="600" height="489" alt="MS Project Options"></p>
                <p>Also see how to start your <a href="/project/lessons/first-project.php">first project using MS Project</a> or visit <a href="/resources/project.php">additional Microsoft Project resources</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Project student reviews</h4>
                <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a class="project" href="/testimonials.php?course_id=22">Project training testimonial page</a>.</p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>