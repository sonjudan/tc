<?php
$meta_title = "Splitting and Overlapping Tasks in Project | Training Connection";
$meta_description = "Learn how to split and overlap tasks in Microsoft Project. This lesson is a featured element in our in-depth Microsoft Project Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Splitting and Overlapping Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Splitting and Overlapping Tasks in Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For <a href="/ms-project-training.php">MS Project courses available in Chicago and Los Angeles</a> and delivered by friendly instructors, call us on 888.815.0604.</p>
                <p>In this article, we will learn how to:</p>
                <ul>
                    <li>Split tasks</li>
                    <li>Overlap tasks</li>
                </ul>
                <h4>Splitting Tasks</h4>
                <p>When you split a task, you break it into two or more separate parts so that parts of a task can start earlier or later than others. Most typically, a task is split when only part of a task is going to be delayed.</p>
                <p>The first step to splitting a task is to select the task in question. For this example, click to select “Task 2:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/split-task.jpg" alt="select task to split"></p>
                <p>Next, click Task → Split Task ():</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/split-task-option.jpg" alt="click split task"></p>
                <p>Your cursor will change to the split icon. </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/split-ico.jpg" alt="split icon displayed"></p>
              On the Gantt chart you can then click on the area of the task where you want the split to be inserted. For this example, click the middle of the selected task (Task 2) and then drag to the right. As you drag you will see an outline of the second part of the now split task:
                <p></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/second-part-split.jpg" alt="task now split"></p>
                <p>Release your mouse button to split this task and position the second portion of the split. Once the split is inserted, the task time will be extended and the split time will be marked with a series of dots. <a href="/ms-project-training-chicago.php">Chicago MS Project classes</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/split-inserted.jpg" alt="split marked with dots"></p>
                <p>You may remove a split by clicking the Split button again and using your cursor to drag the furthest piece back toward the initial task. You can also use the Undo button if the split was the last action you performed.</p>

                <h4>Overlapping Tasks</h4>
                <p>By overlapping tasks, you can shorten the overall length of your project. For example, suppose that the project that you are working on is the filming of a movie. While ideally the screenplay should be done before the filming begins, you can start filming while the story is still being written.</p>
                <p>Examine the sample project and you will see just such a scenario:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/sample-project.jpg" alt="examine sample"></p>
                <p>Suppose that you want the filming to begin once the writing of the movie is about three quarters complete. Do this by clicking and dragging the Film task’s Gantt bar into an overlapping position with the Write Script task:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/tasks-gantt-bar.jpg" alt="overlapping position of task"></p>
                <p>Release your mouse button. As these two tasks are linked and you moved the Film start date before the Write Script completion date, the Planning Wizard dialog will be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/planning-wizard-for-task.jpg" alt="two tasks linked"></p>
                <p>Click the “Remove the link…” radio button and then click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/remove-the-link.jpg" alt="remove link radio button"></p>
                <p>The two tasks in this project will now overlap one another, but will no longer be linked:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/two-tasks-overlapping.jpg" alt="two tasks overlap"></p>
                <p>Alternatively, you can also overlap tasks without removing a finish-to-start link. If you open the Predecessors tab on the Task Information dialog for the task in question, you can add negative lag time to a linked task to create overlap. Below you can see that negative one month lag time has been added to this task:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/overlap-without-remove.jpg" alt="overlap options"></p>
                <p>This negative lag time will produce the following result:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/negative-lag.jpg" alt="lag results"></p>
                <p>The tasks are still linked, but they overlap as well.</p>
                <p>Always use caution when overlapping tasks because if you overlap tasks that use the same resources, a conflict could occur.</p>
            </div>
        </div>
    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">
            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Read testimonies from thousand of happy students on our <a href="/testimonials.php?course_id=22">student testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>