<?php
$meta_title = "Managing Tasks in MS Project | Training Connection";
$meta_description = "Learn about managing tasks in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Managing Tasks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Managing Tasks in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In this article,  will delve a little deeper into managing tasks within Projects.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p>We will learn how to:</p>
                <ul>
                    <li>Create recurring tasks</li>
                    <li>Import tasks from Microsoft Outlook</li>
                </ul>

                <h4>Creating Recurring Tasks</h4>
                <p>Recurring tasks are tasks that happen regularly during the duration of your project. For example, recurring tasks could be used to indicate the creation of weekly status reports.</p>
                <p>To create a new recurring task, click Task → Task → Recurring Task:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/create-new-recurring-task.jpg" alt="create a new recurring task"></p>
                <p>The Recurring Task Information dialog will open:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/recurring-task-information-dialog.jpg" alt="recurring task information dialog box"></p>

                <p>Use this dialog to enter basic information about the new recurring task. For this example, enter “Weekly Status Report” as the name for this task. Use the controls provided to set this new task to recur every Monday on a weekly basis:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/basic-information-recurring-task.jpg" alt="basic information recurring task dialog"></p>

                <p>The options in the “Range of recurrence” section of this dialog control when this recurring task will start in the lifetime of the project and when it will end. By default, it will start on the date when the project was first created and end when the last task is set to complete. Ensure that the Start date for this task is set to Mon 1/5/15 and the “End by” date is set to Fri 3/27/15. Click OK:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/range-of-recurrence.jpg" alt="range of recurrence"></p>

                <p>After a moment, Project will add all of the new recurring tasks to your project, organized under a new summary task:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/new-summary-task.jpg" alt="new summary task"></p>

                <p>Close Microsoft Project 2013 and save your changes. <a href="/ms-project-training-chicago.php">Small MS Project classes</a>.</p>

                <h4>Importing Outlook Tasks</h4>

                <p>Ensure that Gantt chart view is applied. To complete this exercise, you will need access to Microsoft Outlook 2007 or later. Also, please create a task in Outlook called “Prepare report for meeting” before proceeding.</p>

                <p>As you may know, Microsoft Outlook allows you to create tasks. What you might not know is that you can import these tasks into Microsoft Project.</p>

                <p>To import tasks from Outlook, click Task → Task → Import Outlook Tasks:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/import-outlook-task.jpg" alt="import outlook task"></p>

                <p>This action will open the Import Outlook Tasks dialog. (If you see a security warning, click Allow.) You will see all of the tasks that exist inside your current Microsoft Outlook mailbox or data file. To choose the tasks to import, you can click the Select All button, the folder checkbox, or each task’s corresponding checkbox. For this example, click the “Prepare report for meeting” checkbox. Click OK:</p>

                <img src="https://www.trainingconnection.com/images/Lessons/project/open-import-tasks-dialog.jpg" alt="open import tasks dialog box">

                <p>The Import Outlook Tasks dialog will close and the selected task will be imported into the sample project. The name of the task as well as its duration information (if any) will be included:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/imported-task-list.jpg" alt="imported task list"></p>

                <p>You can now work with the task as you would any other in Project.</p>

                <p>Close Microsoft Project 2013 and save your changes.</p>
            </div>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Microsoft Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>