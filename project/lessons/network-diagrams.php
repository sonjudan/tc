<?php
$meta_title = "Network Diagrams in Microsoft Project | Training Connection";
$meta_description = "Learn how use Network Diagrams in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Network Diagrams</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Network Diagrams in Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>To see the Network Diagram, on the View tab of the Ribbon click Network Diagram.</p>
                <p>The Descriptive Network Diagram view is identical to the Network Diagram view, except for the size, and the detail of the boxes that represent tasks. The boxes on the Descriptive Network Diagram view are larger and can contain labels for the data elements in the box. These larger boxes take up more space, and thus fewer boxes fit on a printed page. </p>
                <p>For <a href="/ms-project-training.php">instructor-led MS  Project training</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p>To display the Descriptive Network Diagram view, click the Other Views command on the View tab, select More Views and then select the Descriptive Network Diagram:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/descriptive-network-diagram.jpg" width="724" height="556" alt="Descriptive Network Diagram"></p>
                <h4>Network Diagrams (the PERT Chart principle)</h4>
                <p>The term PERT is derived from <strong>P</strong>rogramme <strong>E</strong>valuation and <strong>R</strong>eview <strong>T</strong>echnique which was invented for the management of Projects by paper based systems. The NETWORK DIAGRAM which has evolved from the PERT Chart is a diagrammatic view of the tasks where the position of the task and the lines linking them together represent the detailed steps that comprise the project. The Network Diagram view displays tasks and task dependencies in a network or flowchart format. A box (sometimes called a node) represents each task, and a line connecting two boxes represents the dependency between the two tasks. <a href="/ms-project-training-los-angeles.php">Microsoft project classes in LA</a>.</p>
                <p>It is possible to create a new project in the Network diagram or modify an existing project by adding and linking tasks.</p>
                <ul>
                    <li>To create a new task, in an empty part of the Network diagram drag a rectangle shape with your mouse - a new node will appear ready for you to type in the details</li>
                    <li>To create a new task that is linked to an existing task, click the existing task and drag with your mouse to an empty part of the diagram - a new node will appear ready for you to type in the details.</li>
                </ul>

                <h4>The Network Box (or Node)</h4>
                <p>A Network box consists of five fields, as illustrated below. The Task Name, Task ID, Scheduled Start Date, Scheduled Finished Date, and Task Duration are the default fields in the Network box. The Task Name field is white because it is currently ready for user input.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/network-box.jpg" width="750" height="248" alt="Network box or node"></p>

                <h4>Change how the nodes are displayed</h4>
                <p>Note: By default, the network diagram shows critical tasks in red and displays one diagonal line through a task that is in progress and crossed diagonal lines through a completed task.  </p>

                <ol>
                    <li>On the Format tab, click Box Styles.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/box-styles.jpg" width="750" height="654" alt=""></p>

                <ol start="2">
                    <li>In the Style settings for list, click the task category whose content and appearance you want to change. </li>
                    <li>Choose a different Data template - which changes what data is displayed on the node:</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/non-critical-project.jpg" width="750" height="647" alt="Choosing a Data Template"></p>

                <ol start="4">
                    <li>Under Border, choose the shape, colour, width, and gridline options to create the look you want.</li>
                    <li>Under Background, choose the required colour and pattern.</li>
                    <li>Click OK.</li>
                </ol>
                <p><a href="https://www.microsoft.com/en-gb/evalcenter/evaluate-project-professional-2016">Get a 60-day trial version of Project 2016 Professional</a>.</p>


                <h4>Adjust the diagram layout</h4>
                <p>Depending on the structure of your project, the number of summary tasks and subtasks and the number and types of task links, the Network Diagram boxes may not be arranged as you expected. You can modify the layout by applying different box arrangements, adjusting various layout parameters, and applying one of two different link styles:</p>
                <ol>
                    <li>On the Format tab, click Layout.</li>
                    <li>Under Box Layout, in the Arrangement list, click how you want the boxes to be arranged.</li>
                    <li>For rows and columns, specify alignment, spacing, height, and width in the corresponding boxes.</li>
                    <li>To space boxes evenly, click Fixed in the Height and Width boxes.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/layout.jpg" width="750" height="635" alt="Diagram layout"></p>
                <p><strong>Note</strong>: If you can't arrange Network Diagram boxes the way you want, you may want to position them manually: Click Allow manual box positioning, click OK, and then drag the boxes to the location you want.</p>

                <h4>Change the link line style</h4>
                <p>If you have many tasks that are linked to a predecessor or task, the link lines can overlap and be difficult to read in the Network Diagram view. You can change the style of the link lines and arrange them how you want. You can also display task dependency labels on the link lines.</p>
                <p>On the Format tab, click Layout:</p>
                <ul type="disc">
                    <li>To display link lines as straight lines between predecessor and successor task boxes, under Link style, click Straight.</li>
                    <li>To display link lines as horizontal and vertical line segments connected at right angles between predecessor and successor task boxes, under Link style, click Rectilinear.</li>
                    <li>To display labels on link lines identifying the task dependency and any lead or lag time under Link style, select the Show link labels check box.</li>
                    <li>To display link lines with arrows indicating the predecessor and successor of tasks, under Link style, select the Show arrows check box.</li>
                </ul>

                <h4>The Network Diagram in a combination view</h4>
                <p>A combination view contains two views. The view in the bottom pane shows detailed information about the tasks or resources you select in the view in the top pane. For example, you can have the Gantt Chart view in the top pane and the Task Form view in the bottom pane. When you select a task in the upper view, the Task Form view displays detailed information about that task. The same applies to Network Diagrams. </p>
                <p>In the illustration below the selected task is shown in detail in the lower pane where the Task Usage view is displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/critical-task.jpg" width="750" height="326" alt=""></p>

                <h4>To split the screen</h4>
                <ol>
                    <li>On the View tab of the ribbon check the Details box </li>
                    <li>Click the drop down arrow to choose the Detail you wish to display.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/split-screen.jpg" width="424" height="616" alt="To split the screen"> </p>
                <p>Note: to remove the split, uncheck the Details box. </p>

                <h4>Navigating the Network Diagram</h4>
                <p>The table below summarises how to move around the Network Diagram with your keyboard and with your mouse.</p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-4" summary="The table below summarises how to move around the Network Diagram with your keyboard and with your mouse.">
                    <thead>
                        <tr>
                            <td width="201" scope="col"><strong>Movement</strong></td>
                            <td width="97" scope="col"><strong>Keys</strong></td>
                            <td width="368" scope="col"><strong>Mouse</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>To a different NETWORK box</td>
                        <td>Arrow keys</td>
                        <td>Click the NETWORK box</td>
                    </tr>
                    <tr>
                        <td>Next field in NETWORK box</td>
                        <td><strong>Tab </strong>or
                            <strong>Enter</strong></td>
                        <td>Click the field</td>
                    </tr>
                    <tr>
                        <td>Previous field in NETWORK box</td>
                        <td><strong>Shift+Tab
                            </strong>or <strong>Shift+Enter</strong></td>
                        <td>Click the field</td>
                    </tr>
                    <tr>
                        <td>Page up or page down</td>
                        <td><strong>Page Up</strong>
                            or <strong>Page Down</strong></td>
                        <td>On the vertical scroll bar, click the gray area above or below the scroll box</td>
                    </tr>
                    <tr>
                        <td>Page to the left or right</td>
                        <td><strong>Ctrl+Page Up
                            </strong>or
                            <strong>Ctrl+Page Down</strong></td>
                        <td>On the horizontal scroll bar, click the gray area on the left or right of the scroll box to scroll left or right in increments</td>
                    </tr>
                    <tr>
                        <td>To upper-left NETWORK box in project</td>
                        <td><strong>Home</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>To lower-right NETWORK box in project</td>
                        <td><strong>End</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>To upper-left NETWORK box on screen</td>
                        <td><strong>Ctrl+Home</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>To lower-right NETWORK box on screen</td>
                        <td><strong>Ctrl+End</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

                <p>Also see <a href="/project/lessons/tables.php">using tables in MS Project</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite Microsoft Project training</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>