<?php
$meta_title = "Adding Gantt Bars in Microsoft Project | Training Connection";
$meta_description = "Learn how to add customized Gantt Bars in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gantt Charts</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Customising the Gantt Chart in MS Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>The Gantt Chart is a horizontal bar chart that represents each task in the time scale of the project. Each task entered in the project will be shown and by default the name of the resource allocated to the task appear next to the bars.</p>
                <p>Gantt Charts form the significant part of a regular communication about your project and can be quickly formatted to display the critical tasks, the current progress, comparison with the original plan, and the new projected completion.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>


                <h4>Critical Path</h4>
                <p>Critical Path Analysis or <strong>CPA</strong> is an important part of project management. It will enable you to interrogate the tasks in your project to see which tasks form the basis of a successful completion of the project.</p>
                <p>These tasks, should they be delayed or indeed, completed sooner than planned will have a critical and fundamental impact on your project.</p>
                <p>We need to be able to view these statistics from time to time and it will enable us, by viewing them, to either shorten the plan or concentrate on costs. <a href="/ms-project-training-los-angeles.php">Project 2019 training classes</a>.</p>


                <p>When you use Auto Schedule mode, Project schedules the tasks based on the project start or finish date and any task relationships you have defined. Using the scheduled start and finish dates, Project determines which tasks <strong>must finish on time for the project to finish on time.</strong></p>

                <p>Because other tasks are linked to the critical tasks, if a critical task finishes late, it causes all of the tasks that follow, to start and finish late. By extension, the project finishes late. Conversely, if a critical task finishes early, the project can finish early.</p>
                <p>
                    In the example below we have three tasks that are all due to start on the same day and each has varying durations:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/tasks.jpg" width="750" height="748" alt="Tasks"></p>


                <p>Task 1 is the longest task; Tasks 2 and 3 are <strong>not</strong> linked to Task 1, which has the longest duration. So long as they are completed <strong><em>prior</em></strong> to the end of Task 1 the project will not overrun.</p>
                <p>In most projects, you have many tasks linked to one another with numerous relationships. If you link all the tasks with only Finish-to-Start relationships, all of the tasks are critical. The start of one task depends on the completion of the previous task. So, all of the tasks must start and finish on time without the project going astray.</p>
                <p>If, however, you have assigned other types of relationships in your project, some of your tasks will be <strong><em>non-critical</em></strong> tasks:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/non-critical-tasks.jpg" width="750" height="501" alt="Non-critical tasks"></p>

                <h4>To view the critical path in the Gantt view</h4>
                <p>On the Format tab of the ribbon, click the Critical Path check box:   </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/critical-path.jpg" width="750" height="372" alt="Critical Path - Gantt View"></p>

                <h4>Changing Time Scale</h4>
                <p>The time scale of the Gantt Chart is change by using the<strong>&nbsp;Zoom</strong> <strong>Slider</strong> on the Status bar, but you can also use the Timescale dialog box to customise the way the Timescale is displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/time-scale.jpg" width="750" height="576" alt="Changing Time Scale"></p>
                <p>In the Timescale dialog box you can choose to display up to three tiers (Top, Middle and Bottom) and within each of these it is possible to alter the units, the label and the count of the interval.  </p>            <p><a href="https://www.microsoft.com/en-gb/evalcenter/evaluate-project-professional-2016">Get a 60-day trial version of Project 2016 Professional</a>.</p><h4>Formatting the non-working time</h4>
                <p>On the Non-working Time tab of the Timescale dialog box you can change the colour of the non-working time and whether it is displayed behind or in front of tasks. You can also choose which Calendar is displayed on the Gantt Chart: </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/non-working-time.jpg" width="750" height="573" alt="Non-working Time Tab"> </p>

                <p>Also see <a href="/project/lessons/gantt-bars.php">adding a new Gantt Bar</a>. </p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite Project training</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>