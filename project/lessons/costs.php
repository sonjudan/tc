<?php
$meta_title = "Working with Costs in Microsoft Project | Training Connection";
$meta_description = "Learn how to apply costs to projects using Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project Costs</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Costs in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In order for Project to successfully calculate costs for a project, you must enter accurate information, including varying pay rates. A work resource has a standard pay rate and an overtime rate. You can also enter specific pay rates for a certain day or for an assignment. In this module, we’ll look at resource pay rates, including material resource consumption rates. Finally, we’ll look at entering task fixed costs. Other tutorials cover <a href="/project/lessons/adding-resources.php">adding resources</a> and <a href="/project/lessons/adding-tasks.php">adding tasks</a> to projects.</p>
                <p>For an instructor-led <a href="/ms-project-training.php">Microsoft Project training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Adding Pay Rates for a Resource</h4>
                <p>Use the following procedure to enter pay rates.</p>
                <ol>
                    <li>Select <strong>Resource Sheet</strong> from the <strong>View</strong> tab on the Ribbon.</li>
                    <li>Enter the standard and overtime rate for each resource.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-sheet-tools.jpg" width="780" height="192" alt=""></p>

                <h4>Specifying Pay Rates for Different Dates</h4>
                <p>To enter a pay rate for a specific date, use the following procedure.</p>

                <ol>
                    <li>Select <strong>Resource Sheet</strong> from the <strong>View</strong> tab on the Ribbon.</li>
                    <li>Double-click the resource whose pay rate you want to define to open the <em>Resource Information</em> dialog box.</li>
                    <li>Select the <strong>Costs</strong> tab.</li>
                    <li>In the second row, enter the new standard, overtime, or a per use cost rate.</li>
                    <li>Select the down arrow in the <strong>Effective Date</strong> column to select the effective date from the calendar.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/resource-information.jpg" width="523" height="394" alt="Resource information"></p>
                <ol start="6">
                    <li>Click on a lettered tab to enter additional cost rate tables. A is the default table. Repeat steps 4 and 5 for each table.</li>
                    <li>Select <strong>OK</strong> to closet the <em>Resource Information</em> dialog box.</li>
                </ol>
                <p>Download a <a href="https://products.office.com/en-us/Project/project-for-office-365">trial version of Microsoft Project </a>for FREE. View additional <a href="/resources/project.php">MS Project resources</a>.</p>

                <h4>Applying a Different Pay Rate to an Assignment</h4>
                <p>Use the following procedure to apply a different pay rate to an assignment</p>
                <ol>
                    <li>Select <strong>Task Usage</strong> from the <strong>View</strong> tab to open the <em>Task</em> <em>Usage</em> sheet.</li>
                    <li>Double-click on the resource assigned to the task that requires a different pay rate.</li>
                </ol>

                <p>Project displays the <em>Assignment Information</em> dialog box. <a href="/ms-project-training-los-angeles.php">Small Project classes in LA</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/assignment-information.jpg" width="487" height="306" alt="Assignment Information Dialog box"></p>

                <ol start="3">
                    <li>Select the <strong>Cost Rate Table</strong> that you want to use from the drop down list.</li>
                    <li>Select <strong>OK</strong>.</li>
                </ol>
                <p>If desired, return to the Gantt Chart view and show the participants the changed costs based on the new table. Project highlights the changes in the selected Changed Cell highlight color. (Note: the default highlight color is very light. You may want to change it using the Text Styles tool on the Format tab.)</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/gantt-chart-tools.jpg" width="780" height="229" alt="Gantt Chart Tools > Format Tab"></p>

                <h4>Using Material Resource Consumption Rates</h4>
                <p>To assign a material resource to a task, use the following procedure.</p>
                <ol>
                    <li>Double-click on the task to open the <em>Task Information</em> dialog box. In this example, select Task E.</li>
                    <li>Select Resource E for the task. Enter 10 as the units for the material resource.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-information.jpg" width="580" height="362" alt="Task Information"></p>
                <ol start="3">
                    <li>Select <strong>OK</strong> to close the <em>Task Information</em> dialog box.</li>
                    <li>Select <strong>Task Usage</strong> from the <strong>View</strong> tab.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/task-usage.jpg" width="780" height="172" alt="Task Usage"></p>
                <p>You can also manually override the calculated consumption rates by entering a new value in the columns for that resource for that task.</p>

                <h4>Entering Task Fixed Costs</h4>
                <p>Use the following procedure to enter task fixed costs.</p>
                <ol>
                    <li>Select <strong>Table</strong> from the View menu. Select <strong>Cost</strong>.</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/table-costs.jpg" width="780" height="239" alt="View>Tables>Cost"></p>
                <ol start="2">
                    <li>Select the task for which you want to enter a fixed cost.</li>
                    <li>Enter the amount in the <strong>Fixed cost</strong> column.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/fixed-cost.jpg" width="780" height="189" alt="Fixed costs"></p>
                <p>Also learn more about the <a href="/project/lessons/interface.php">Microsoft Project Interface</a>.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Project student reviews</h4>
                <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonials page</a>.</p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>