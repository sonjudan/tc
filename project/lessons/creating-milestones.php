<?php
$meta_title = "Creating Milestones in MS Project | Training Connection";
$meta_description = "Learn about creating milestones in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating Milestones</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating Milestones in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article,  will delve a little deeper into understanding tasks and creating miletones within Projects.</p>
                <p>For instructor-led <a href="/ms-project-training.php">Microsoft Project training in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <h4>Creating Milestones</h4>
                <p>A milestone indicates a significant event in the lifetime of a project. For example, the testing of a prototype could be considered a milestone in the sample project that we have been working with. Typically milestones are zero days in duration. There can also be multiple milestones for each project.</p>
                <p>To create a milestone, first open the Task Information dialog for the task you would like to work with. For this example, open the Task Information dialog for the “Test Prototype” task (#24). Then click the “Mark task as milestone” checkbox under the Advanced tab. Click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/mark-task-as-milestone.jpg" alt="mark task as milestone"></p>
                <p>In the Gantt chart, you will see that the “Test Prototype” task (#24) now indicates that it is a milestone, with a diamond symbol and the date beside it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/test-prototype.jpg" alt="test prototype for gant chart"></p>
                <p>A milestone does not have to be based on an existing task. You can create a milestone from scratch by clicking Task → Milestone:</p>
                <img src="https://www.trainingconnection.com/images/Lessons/project/creating-milestone-from-scratch.jpg" alt="creating milestone from scratch"><p></p>
                <p>The new milestone will then appear on your task list where your cursor was last placed:</p>
                <img src="https://www.trainingconnection.com/images/Lessons/project/new-milestone-in-task-list.jpg" alt="new milestone in task list"><p></p>
                <p>You may then type a new name for this milestone in the field provided. <a href="/ms-project-training-los-angeles.php">Los Angeles MS Project training classes</a>.</p>

                <h4>Setting Deadlines</h4>
                <p>You can use the Advanced tab in the Task Information dialog to set a deadline for a particular task. Open the Advanced tab of the Task Information dialog for the “Test Prototype” task (#24):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/set_deadline-in-task-information.jpg" alt="setting deadline in task information"></p>
                <p>To set a deadline, use the Deadline drop-down menu (or enter a date directly) to specify the date that this task needs to be completed by. For this example, choose March 27th, 2015:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/project/deadline-with-drop-down-menu.jpg" alt="deadline with drop down menu"></p>
                <p>Click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/ok-menu.jpg" alt="ok box"></p>
                <p>Once a deadline has been applied, it will be indicated with a green arrow:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/green-arrow-indicator.jpg" alt="green arrow indication box"></p>
                <p>Close Microsoft Project 2013 and save your changes.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Project testimonials</a> page.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>