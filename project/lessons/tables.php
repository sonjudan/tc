<?php
$meta_title = "Using Tables in Microsoft Project | Training Connection";
$meta_description = "Learn how use Tables to hold data in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tables</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using Tables in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Much of the data the system holds can be entered and/or viewed in a table format. </p>
                <p>Project has predefined sets of columns (called tables) which display specific information. To apply a different table to a sheet view, click the View tab, click Tables, and then select the table you want to apply:</p>
                <p>For <a href="/ms-project-training.php">instructor-led Microsoft Project training</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/table-view.jpg" width="750" height="559" alt="Table tracking in MS Project"></p>

                <h4>Inserting columns</h4>
                <p>It is possible to add/remove columns from any of the tables, for example if you widen the divider bar on the Gantt Chart you will see that the last column is set to <strong>Add New Column:</strong></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/add-new-column.jpg" width="350" height="" alt=""></p>

                <p>Click <strong>Add New Column </strong>to see a list of possible columns you can insert:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/column-types.jpg" width="350" height="" alt="Types of Columns"></p>
                <p><strong>Tip: </strong>You can also right click any existing column heading and choose to Insert a Column. <a href="/ms-project-training-los-angeles.php">MS Project classes in LA</a>.</p>

                <h4>Remove (hide) columns</h4>
                <ul type="disc">
                    <li>Click the column heading and press delete on the keyboard - you will never delete the column, you are simply hiding it from the view.</li>
                    <li>Click undo to immediately undo this action - or insert the column using the steps above.</li>
                </ul>

                <h4>Modifying (or editing) an existing Table using the Table Definition Dialog Box</h4>
                <p>On the View tab of the Ribbon, select Tables - More tables:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/more-tables.jpg" width="500" height="" alt="Table Definition Dialog Box"></p>

                <p>Select from this list the one you want to change/modify and click Edit.</p>
                <p>The <strong>Table Definition</strong> dialog box shows a list of the fields (columns) that make up the table. It is possible to change the width of an existing column, the alignment, or the column title as well as inserting and deleting columns:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/table-definition.jpg" width="750" height="423" alt="Modifying a Table"></p>
                <ul>
                    <li>Position the pointer at the required position and click the <strong>Insert</strong> <strong>[Row/Column] </strong>button.&nbsp; A space will appear and the new field can be selected from the drop down list</li>
                    <li>To remove a field, point to it and press the <strong>Delete</strong> button.</li>
                </ul>
                <p><a href="http://www.oneonta.edu/its/documentation/office/2013/Microsoft%20Project%202013.pdf">Microsoft Project 2013 Quickstart Guide</a>.</p>

                <h4>Creating a New Table</h4>
                <p>New Tables can be created by selecting the <strong>New</strong> button from the <strong>More Tables </strong>dialog<strong> </strong>box.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/creating-table.jpg" width="750" height="420" alt=""></p>
                <p>The structure of the table is built up by selecting the fields in the appropriate order.</p>
                <p><strong>Tip:</strong> Check the box <strong>Show in menu</strong> so that it is easy to apply your new table.</p>

                <h4>Formatting table</h4>
                <p>Click the Text Styles command to display the Text Styles dialog box</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/text-styles.jpg" width="100" height="" alt="Text Styles">  </p>
                <ol start="1" type="1">
                    <li>From the Item to Change drop down list choose the item (for example Summary Tasks)</li>
                    <li>Choose the required Font, Size, Color, etc</li>
                    <li>Click OK</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/formatting-tables.jpg" width="750" height="460" alt="Formatting Tables"></p>
                <p>Also see <a href="/project/lessons/filters.php">Applying Filters in Microsoft Project</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite Project training</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">Microsoft Project testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>