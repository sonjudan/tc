<?php
$meta_title = "Using Filters in Microsoft Project | Training Connection";
$meta_description = "Learn how to apply Filters and Auto Filters in Microsoft Project. This lesson is part of our Project Fundamentals Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/project.php">Project</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Filters</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Project">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Applying Filters in Microsoft Project</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>A filter is used to screen out unwanted tasks for a particular view to identify a particular aspect of the current state of the project, for example the filter can be set to show the tasks that make up the Critical Path.</p>
                <p>For <a href="/ms-project-training.php">hands-on Microsoft Project training</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p>As with tables there are different filters for tasks and resources and depending on the current view the appropriate list of filters will be shown for the selection.</p>
                <ol>
                    <li>Select the view you want to filter (eg Gantt Chart, Resource Sheet, etc)</li>
                    <li>Select the View tab of the Ribbon</li>
                    <li>Click the drop down arrow of the <strong>Filter</strong> command and choose the required Filter:</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/project-filters.jpg" width="592" height="864" alt="Microsoft Project Filters"></p>

                <ol start="4">
                    <li>Remove the filter by choosing [No Filter] </li>
                </ol>
                <h4>Using AutoFilters</h4>
                <p>In addition to standard filters Project 2013 provides AutoFilters, visible in any sheet view where ach column in a sheet view has its own AutoFilter indicated by the drop down arrow on each of the column headings: </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/auto-filters.jpg" width="750" height="69" alt="Auto filters"></p>
                <p>When you choose an option from the AutoFilter drop down arrows, Project displays only those tasks or resources that match the criteria - selecting an AutoFilter does not delete information from your project, but simply hides information from your view. <a href="/ms-project-training-chicago.php">MS Project training in the Chicago</a>.</p>
                <p>Let's say you're reviewing your project tasks and only want to view those tasks that take more than one week to complete:</p>
                <ol>
                    <li>Click the AutoFilter arrow in the Duration field</li>
                    <li>Choose Filters - choose 1 week or longer:</li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/project/auto-filter-duration.jpg" width="744" height="888" alt="Autofilter>Duration"></h4>
                <p>Project now displays only those tasks that will take more than 1 week to complete and Notice that the column heading for the Duration field has a filter icon, indicating that an AutoFilter is in use.</p>


                <p>By selecting AutoFilters for more than one column, you can narrow the information even further. For instance, by setting the AutoFilter for the Duration field to greater than 1 week and setting an AutoFilter in the Resource Name field for a particular person, you can view only those tasks assigned to that person that take more than 1 week to complete.</p>
                <p>To clear a filter, click the filter icon on the column heading and choose Clear Filter from...</p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/project/clear-filter.jpg" width="672" height="560" alt="Clearing a Filter"></p>

                <p>AutoFilters are specific to each column, allowing you to pick from any of the data in that column. For example, when you click the <strong>AutoFilter</strong> arrow for the Resource Name field, you see a list of all the resources in your current project and you can multi select as many of the resources as you like. </p>

                <p>Also see <a href="/project/lessons/network-diagrams.php">Network Diagrams in Microsoft Project</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-project" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft Project Training</h4>
                    <p>Need group training? We deliver <a href="/onsite-training.php">onsite group training in Microsoft Project</a> right across the country.</p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Project student reviews</h4>
                    <p>Every year we train thousands of satisfied students.... read a sample of testimonials at our <a href="/testimonials.php?course_id=22">MS Project testimonial page</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>