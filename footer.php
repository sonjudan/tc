<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/yelp-reviews.php'; ?>

<footer class="main-footer">
    <div class="container">
        <div class="row" data-aos="fade-up" data-aos-delay="200">
            <div class="col-md-8 col-lg-7 col-xl-6 ">

                <div class="widget-footer" >
                    <h3 class="widget-title">Quicklinks</h3>

                    <div class="menu-row">
                        <ul class="footer-menu">
                            <li><a href="/onsite-training.php">Onsite training</a></li>
                            <li><a href="/room-hire.php">Computer Room Rental</a></li>
                            <li><a href="/jobs.php">Jobs at Training Connection</a></li>
                            <li><a href="/hotels.php">Recomemended Hotels</a></li>
                            <li><a href="/terms.php">Cancellation and Rescheduling Terms</a></li>
                        </ul>
                        <ul class="footer-menu">
                            <li><a href="/repeat.php">Repeat Policy</a></li>
                            <li><a href="/resources.php">Resources</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/sitemap.html">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-lg-5 col-xl-6 mb-4">
                <img class="footer-logo" src="/dist/images/logo-tc.svg" alt="Training Connection">
                <p class="copyright">all rights reserved 2019</p>
                <ul class="social-list ">
                    <li><a href="https://www.facebook.com/trainingconnection1/" target="_blank"><i class="icon-facebook"></i></a></li>
                    <li><a href="https://twitter.com/trainingconnect?lang=en" target="_blank"><i class="icon-twitter"></i></a></li>
                    <li><a href="https://www.google.com/maps/place/Training+Connection/@41.8810006,-87.6348979,15z/data=!4m5!3m4!1s0x0:0x4d7a36f4b096e618!8m2!3d41.8810006!4d-87.6348979" target="_blank"><i class="icon-location"></i></a></li>
                    <li><a href="https://www.google.com/maps/place/Training+Connection/@34.0510274,-118.2600668,15z/data=!4m5!3m4!1s0x0:0x5831c6d11713747a!8m2!3d34.0510274!4d-118.2600668" target="_blank"><i class="icon-location"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-cart.php'; ?>

<script type='text/javascript' src='/dist/scripts/main.js?v=1.1.4'></script>
<script src="https://code.iconify.design/1/1.0.2/iconify.min.js"></script>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 11158817;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/11158817/" rel="nofollow">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->

</body>
</html>