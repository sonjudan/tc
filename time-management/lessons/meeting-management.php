<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Meeting Management</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to setup Effective Meetings</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/meeting-management.jpg" width="750" height="500" alt="Effective Meeting Management"></p>
                <p>The first step in making your meeting effective begins with your planning and preparation activity. Determining the purpose of your meeting, the people who should attend, and the place of the meeting will form the foundation on which you will build your agenda, decide what materials you need, and identify the roles each attendee hold in the meeting. In addition, planning and preparing for your meeting helps to reduce the stress that may result from managing a meeting, because you will avoid unexpected incidents and issues that could derail your meeting.</p>
                <p>Need to improve your productivy? Did you know we run <a href="/time-management-training.php">Time Management workshops</a> in Chicago and Los Angeles. To sign-up for a fun and informative day call us on 888.815.0604.</p>
                <h4>Identifying the Participants</h4>
                <p>Determining your meeting participants is an important planning step. You should not approach this casually. Who attends your meeting could help or hinder the meeting dynamics. There is a tendency to invite everyone you know in an effort to cover all angles. This is overkill. Before you think about whom to invite, think about the purpose of the meeting. This will help you determine who should be invited. Be specific when determining the purpose of the meeting. For example, if you are meeting to resolve a problem, invite only those who are capable of providing solutions to the problem. Avoid inviting a high-ranking manager, who could thwart solutions before they are developed. <a href="/time-management-training-chicago.php">Time management classes offered in Chicago</a>.</p>
                <p>On the other hand, if your meeting is to come to a decision on a policy or product, do not invite people who do not have the power to enact those changes. Having people who cannot contribute to the meeting will exclude them and affect the meeting environment. Identifying the purpose of your meeting first will help to determine who should attend. Here are some common reasons to call a meeting:</p>
                <ul>
                    <li>Problem solving</li>
                    <li>Decision making</li>
                    <li>Conflict resolution</li>
                    <li>Project initiation</li>
                    <li>Planning </li>
                    <li>Brainstorming</li>
                </ul>
                <p>Once you determine your meeting purpose, you can list all the names of the participants you wish to attend. Once this list is created, then determine what each participant will contribute to the meeting. If a participant is deemed a non-contributor, they should be removed from the list. When all non-contributors are removed, you should have a good list of participants for your meeting. <a href="/time-management-training-los-angeles.php">LA time management workshops</a>.</p>
                <p>Also read <a href="/time-management/lessons/useful-tips.php">Useful Time Management Tips </a></p>

                <h4>Choosing the Time and Place</h4>
                <p>There are several considerations you must address when planning the time and place of your meeting. For instance, the time of day is essential if your meeting is meant to be a brainstorming session or problem-solving meeting. Setting these types of meetings right after lunch or late in the day could be a frustrating experience. Humans after lunch are usually lethargic and meetings at the end of the day are plagued with participants looking at the clock in anticipation to leave work and go home.</p>
                <p>Meetings that require energy and high level of participation are best scheduled between 8 and 9 AM in the morning. Most workers are not engaged in their daily work yet so you will have their attention and energy for use in your meeting. The next best time for a meeting is around 3 PM. This gives your participants enough time to recuperate from their lunchtime meal. It also gives you at least an hour of cushion before your participants start thinking about going home. Meetings that are low key could be scheduled anytime during the day. Just remember not to schedule them to close to lunch or the end of the workday.</p>
                <p>The location is also important to your meeting dynamics. Try to schedule your meeting in a well-lit spacious room. If you can get a room with windows, do so. Dark and cramped rooms will bog down your meeting. Some people get claustrophobic and are distracted by their surroundings. A couple of other things to consider are the need for privacy or if you intend to have an outside visitor attend. If the meeting topic is of a sensitive nature, then getting a room with more privacy will make participants more comfortable to discuss the issue. Furthermore, if you plan to have an outside visitor attend your meeting, get a room that is closest to the main entrance. This way your visitor does not have to search the halls of your organization in search of your meeting. </p>

                <h4>Creating the Agenda</h4>
                <p>Creating the agenda can be easy if you know what to do in advance. The <strong>SOAP</strong> technique helps to collect the topics, organize them, and select the ones that will contribute the most to your meeting.</p>
                <ul>
                    <li><strong>Seek topics from your participants</strong>: send an email to the list of participants you created, asking for agenda topics. Give a brief explanation of the purpose of the meeting and an idea of what you are looking for in terms of topics. Do not make this the formal invitation. When you make the request, make sure you ask the participants for the time they need to discuss their topic, and provide a deadline to get their topic to you so it can be included on the agenda.</li>
                    <li><strong>Organize topics into a list:</strong> once you receive the topics, organize them into a list along with the time and the name of the presenter. This will give you the ability to scan through the list, narrowing it down to the topics you will select for the agenda.</li>
                    <li><strong>Assess which topics are relevant to the meeting purpose</strong>: with your list organized, determine which topics are the most relevant to the purpose of the meeting. Scratch out those topics you do not intend to use. </li>
                    <li><strong>Pick the number of relevant topics that will fit into your meeting time:</strong>&nbsp;review the time of the remaining topics. Select enough topics to fill the time of your meeting minus ten minutes. Give yourself ten minutes for meeting overrun. If you go over, you will end on time. If you do not, then you get to adjourn your meeting early, making everyone happy. </li>
                </ul>
                <p>Remember to contact the presenter that had their topic removed from the agenda, explaining the reason why it was not put on the agenda and recommending that topic be saved for another meeting. More <a href="http://managementhelp.org/misc/meeting-management.htm">tips to setup a meeting.</a></p>

            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Effective Time Management classes right across the country. Obtain a <a class="time" href="/onsite-training.php">quote for an onsite Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a class="time" href="/testimonials.php?course_id=41">Time Management testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>