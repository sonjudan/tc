<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Productivity tips</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>7 ways to better manage your day</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <img class="alignright" src="https://www.trainingconnection.com/images/Lessons/time-management/time-a-precious-resource.jpg" width="202" height="198" alt="Our most precious resource is Time">
                <p>Managing yourself isn't easy. But follow these tips to help you achieve your personal goals in life and at work.</p>
                <p>For more information on <a href="/time-management-training.php">our  time management classes</a> call us on 888.815.0604.</p>

                <h4>1. <strong>Use a Diary (Planner)</strong></h4>
                <p>Procrastination paralyses many people. Getting started is the hardest part of many tasks. You need to understand that there are no easy fixes for this - you just have to exercise some real willpower and get on with it. But you can help yourself by scheduling a task in your diary, and sticking to it as you would an appointment with someone else. </p>

                <h4>2. Sort out your time</h4>
                <p>Keeping a diary sounds easy, but using it properly is harder. This means not just writing down records of meetings and appointments, but also notes that remind you to block out time for yourself and time for tasks. You can also record deadlines, a list of phone calls to make and correspondence to send. This ensures that your diary is a powerful tool for prioritising each day and getting things done. Then all you have to do is stick to it. <a href="/time-management-training-chicago.php">Group Time Management classes in Chicago</a>.</p>

                <h4>3. List your life</h4>
                <p>Your <strong>TO DO</strong> list is your best friend. Whether you use an electronic organiser, a Filofax or just a notebook, list <strong>EVERYTHING</strong> you need to get done. If these tasks involve phone calls, write down the numbers. Then develop a system for prioritising these tasks and take this list with you everywhere you go. This means that idle time need never be wasted because you can spend it doing odd jobs, such as making calls.  This translates to more immediate action!</p>

                <h4>4. Organise your filing</h4>
                <p>If you can't find what you need more or less instantly, it's too well hidden. Organise papers, files and so on so that you know where everything is. It doesn't have to look neat or follow a recognised system. The essential test is that if you had to give someone instructions to find something over the phone, would you be able to tell them exactly where it is?</p>

                <h4>5. Be ruthless</h4>
                <p>Wouldn't your life be simpler without people? Just think what you could get done. People interrupt, change deadlines and take up your time, so you need to handle them effectively, or they will steal your time in a multitude of ways. Be ruthless, but nice. Tell them you’re too busy. Tell them you can't. Tell them you’ll get back to them. Say 'no' to them - nicely. <a href="/time-management-training-los-angeles.php">Time Management workshops in Los Angeles</a>.</p>

                <h4>6. Plan telephone calls</h4>
                <p>Use the phone as a tool, not an enemy. Make your outgoing calls in blocks. That way, you can plan time to do them and you’ll probably keep conversations shorter. Incoming calls don't <strong>HAVE</strong> to be answered. You can let the call go to voicemail or an answering machine - which gives you the option to return the call immediately if it's urgent, or to add the return call to your <strong>TO DO</strong> list if not. You can even say, 'Can I call you back?' Most importantly, it lets you work without the interruption of an uncontrolled phone call. </p>

                <h4>7. Attend select meetings</h4>
                <p>Meetings drain your time (and energy) if you're not careful. Only attend meetings you really need to be at, and politely decline others. </p>


            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Time Management Lessons</h4>
                    <ul>
                        <li><a href="/time-management/lessons/useful-tips.php">Tips for Time Management</a></li>
                        <li><a href="/time-management/lessons/goal-setting-tips.php">5 Golden rules for effective goal setting</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Effective Time Management classes right across the country. Obtain a <a class="time" href="/onsite-training.php">quote for an onsite Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a class="time" href="/testimonials.php?course_id=41">Time Management testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>