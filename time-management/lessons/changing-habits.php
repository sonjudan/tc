<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Changing Habits</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>3 Ways We Can Change Our Habits</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>When you got up today, you did somethings without even thinking about it. Researchers at the <a href="http://news.mit.edu/2012/understanding-how-brains-control-our-habits-1029">Massachusetts Institute of Technology (MIT)</a> shed great light on this phenomenon while studying how our brains record and develop our most automated yet controllable behaviors. Their studies revealed new insights on how we have the power to influence our ideal results every time we get up! </p>
                <p>For more <a href="/time-management-training.php">information on our time management classes</a> call us on 888.815.0604.</p>
                <p><img class="imageright" src="https://www.trainingconnection.com/images/Lessons/time-management/changing-habits.jpg" width="780" height="419" alt="Ways to Change our habits"></p>
                <p><a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU">Charles Duhigg’s New York Times Best Seller, The Power of Habit: Why We Do What We Do in Life and Business</a> draws from this and other habit focused research to provide </p>
                <p>3 great tips on how we can change our habits: </p>

                <ol>
                    <li>Know Your Habits</li>
                    <li>Keep Your Habits</li>
                    <li>Resist Your Old Habits</li>
                </ol>
                <h4>1. Know Your Habits</h4>

                <p>What does it mean to know your habits? </p>
                <p>According to Duhigg, a habit is a behavior that starts as a choice, and then becomes a nearly unconscious pattern. He breaks down the habit cycle in three parts. First, a habit starts with a cue- (whenever _____ happens), second, it’s followed by a routine - (I do this) and third, it completes with a reward (I gain ‘this’ satisfaction). By these becoming more automated choices, our brains conserve energy to handle other choices that get us get through the day. <a href="/time-management-training-chicago.php">Chicago time management skills training</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/habit-1.jpg" width="618" height="368" alt=""></p>
                <p>For example, when you were learning to drive, you had to think about exactly which steps you needed to follow to effectively start the car and arrive to your desired destination. The cue is your need to go somewhere, the routine is to follow the steps to get your car in gear and the reward is getting to your desired destination.</p>

                <p>By the time you have mastered this skill, you can continue to make conversations with passengers as you start the car, drive the car and reach your destination without even thinking about it. </p>

                <p>Take a moment to consider the habits that are shaping how you to get things done today:
                </p>
                <p>What do you do when you get up?</p>
                <p>What do you do before you leave for work?</p>
                <p>How do you start working?</p>
                <p>What do when you are distracted?</p>
                <p>What do you do when you are hungry?</p>
                <p>What do you do when you arrive to meetings?</p>
                <p>What do you do right before you leave for the day?</p>


                <p>Or check out this table below:</p>


                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <tbody>
                    <tr>
                        <th scope="col">Cues</th>
                        <th scope="col">Routines</th>
                        <th scope="col">Rewards</th>
                        <th scope="col">Results</th>
                    </tr>
                    <tr>
                        <td>When I get up</td>
                        <td>I turn on the TV</td>
                        <td>It wakes up my mind</td>
                        <td>I get caught up in the news instead of…</td>
                    </tr>
                    <tr>
                        <td>When I get mad</td>
                        <td>I give a dirty look</td>
                        <td>It lets others know I disagree</td>
                        <td>I am known for angry expressions</td>
                    </tr>
                    <tr>
                        <td>When I get hungry</td>
                        <td>I look for something sweet</td>
                        <td>Getting something sweet gives me quick energy</td>
                        <td>I am gaining weight!</td>
                    </tr>
                    <tr>
                        <td>When I feel tired</td>
                        <td>I go get a coffee</td>
                        <td>I get a body boost</td>
                        <td>My adrenal glands are shot!</td>
                    </tr>
                    <tr>
                        <td>When someone questions me</td>
                        <td>I get defensive</td>
                        <td>I feel protected</td>
                        <td>I make others defensive</td>
                    </tr>
                    </tbody>
                </table>


                <p>  Now that you see how you are arriving at your current results, how can we change and keep new habits?</p>
                <p>
                    Also read <a href="/time-management/lessons/goal-setting-tips.php">Goal Setting Tips.</a></p>
                <h4>2. Keep your Habit</h4>
                <p>MIT researchers agreed by changing our routines and keeping the same reward, we can set new habits. In order to make a habit stick, we must understand how to make new routines our brain’s preferred new route for responding to a cue. Duhigg shows how it takes more than a new pattern of steps to helps us create new routine patterns. We must understand the importance of creating a craving for the new behavior for the same reward. <a href="/time-management-training-los-angeles.php">LA-based softskills classes</a>.</p>
                <h4>Creating a Craving</h4>
                <p>Experiments training lab mice to crank a lever for a reward of a syrupy sweet serving of juice revealed that even if the juice became less sweet and watered down, the habit to crank the lever only increased the mice anticipation for the reward. Duhigg likens this to how advertisers <a href="https://www.amazon.com/Power-Habit-What-Life-Business-ebook/dp/B0055PGUYU">Claude Hopkins</a> created a marketing campaign for Pepsodent toothpaste that was able to get a nation into the regular habit of brushing not only by suggesting a new routine but by adding a craving for the routine in the toothpaste that made users crave the menthol and frothy scrubbing experience.</p>
                <p>Maybe you always take a specific route to the gym yet always succumb to the urge to eat fast food coming or going. It keeps you honest to the gym yet it works against your results of weight loss. If you decide to take another route to get you to the gym, you will want to consider what will enhance your craving for that route over the other? Is there a family member you could visit on the way coming or going from the gym? Will you save time for other things you might like to do on the way home? Are there healthier options you could purchase when you got to the gym so that you could still get a boost as a reward for taking a different route? Once you create a new craving for the routine, you stop eating fast foods on the way and you now seek healthier rewards for taking the new route. </p>
                <p>In the chart below, check out how craving is added to the replaced routine.</p>



                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <tbody>
                    <tr>
                        <th scope="col">Cues</th>
                        <th scope="col">Routines</th>
                        <th scope="col">Rewards</th>
                        <th scope="col">Create a Craving</th>
                    </tr>
                    <tr>
                        <td>When I get up</td>
                        <td>I head to the kitchen for water</td>
                        <td>Water gets me going</td>
                        <td>I add Crystal Lite to my water</td>
                    </tr>
                    <tr>
                        <td>When I get mad</td>
                        <td>I smile and pause</td>
                        <td>It lets others know disagree</td>
                        <td>I rub my thumbnail as I calm my expression</td>
                    </tr>
                    <tr>
                        <td>When I get hungry</td>
                        <td>I have a snack bar prepared</td>
                        <td>It gives me a quick energy boost</td>
                        <td>I permit myself a bit of chocolate in the bar</td>
                    </tr>
                    <tr>
                        <td>When I feel tired</td>
                        <td>I go for a walk</td>
                        <td>I get a body boost</td>
                        <td>I encourage others to join me and we time it for fun as well as efficiency</td>
                    </tr>
                    </tbody>
                </table>

                <h4>3. Resist old Habits</h4>
                <p><em>"If you think you can or think you can't, either way you are right. If you think you can or think you can't, you're probably right." - Henry Ford</em></p>
                <p>We must believe in the routine. When we replace a routine, researchers uncovered that we never actually remove the old habit. In challenging times, the old habit is still more secure and can resurface in a time of perceived crisis. In order to ensure adherence, Duhigg points out whether you are in AA or a football player, you must believe that the routine works for your benefit and it is having a personal belief and community of those who believe with you that help you to keep your new habits intact when you are challenged. </p>
                <p>Duhigg used <a href="http://www.coachdungy.com/">Tony Dungy’s</a> revival wins with the Tampa Bay Buccaneers football team to show how when crisis strikes, we often revert back to what we know. As Head Coach, Dungy taught his team over and over to take up specific play patterns based on the theory that player’s reactions could become automatic and more efficient in the live game. When players were caught up in the heat of the scores, many would revert to the delayed and guessed response that was negatively affecting their results. When Tony Dungy lost his son to suicide, team members decided to believe together in delivering on their training in context as a show of solidarity. By following what they learned in practice, the Buccaneers were able to turn around their results and automate on field responses that saved them time and improved their trust in Dungy’s leadership.</p>

                <p>When you decide to start a new routine, it helps if you believe the better you get at following it, the better your results will be. If you are already doubting, then your dedication to do what you need to do will already make you unsuccessful to achieve with any given program. Follow the routine and trust the process! Either way, you will be right so why not be right about your belief you can win?</p>

                <p>In summary, by taking personal inventory of your goals, your routine and what you are doing to keep your routine in place especially when times are tough, these three tips can serve you to make changes that agree with how our brain works. </p>

                <p>In the <a href="/time-management-training.php">Time Management class</a> at Training Connection, we review these patterns in real time so you can focus and workout your own obstructive behaviors and thinking patterns that keep you from setting habits that bring you closer to your goals. Check our site to improve your routine use of software and other business skills as well! Believe you can and you are half way there!</p>

            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Effective Time Management classes right across the country. Obtain a <a href="/onsite-training.php">quotation for  onsite Time Management training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>