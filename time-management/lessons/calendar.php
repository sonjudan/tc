<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Calendars</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>The Danger of an Empty Calendar</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/calendar.jpg" width="780" height="499" alt="Time Management calendar"></p>

                <p>When it comes to time management there is nothing more dangerous than an empty calendar. Calendars give us a visual idea of what time we have or do not have to offer. At work, when someone says, “Julia! Wanna join us for lunch at 12?” “Hey Justin, would you take over the events planning committee?” “Do you have a sec? Actually, I need your assistance going over these documents with me.” Without referring to your calendar, you can make yourself vulnerable to not giving a fair consideration to what time or effort you can make for others and still stay on your game. </p>
                <p>For more on our <a href="/time-management-training.php">instructor-led time management workshops</a> call us on 888.815.0604.</p>

                <p>Calendars are just as important to time management as clocks. Even with the presence of high tech portable devices including cell phones, tablets, laptops and watches, a study conducted in 2009 revealed 8 of 10 respondents praised calendar use and 79% of households have a printed calendar primarily used for date checking, making appointments and keeping track of special dates and holidays. Studies conducted by the <a href="https://www.scribd.com/doc/11707676/An-Exploratory-Study-of-Personal-Calendar-Use">Calendar Advertisement Counselor (CAC)</a> also revealed personal calendars included times for activities easily remembered without a calendar, such as picking up kids from school or attending church just to ensure “a&nbsp;visual idea&nbsp;of&nbsp;the entire day.” </p>
                <p>Here are three tips to ensure you are making the most of your calendars.</p>


                <ol>
                    <li>
                        <h4>Get a Calendar and Post it Up Where You Can See it Daily</h4>
                        <p>The CAC study showed that most calendars are placed in the kitchen and second to that would be the home office area. What is most important is that you see it wherever you spend the most time. Having a printed calendar posted allows you to anticipate upcoming events such as trips, major deadlines and any holidays that may affect your priorities. Think of your calendar as a windshield keeping you in the direction of your long-term goals. </p>
                    </li>
                    <li>
                        <h4>Use a portable calendar and block out the hours for task execution</h4>
                        <p>We all have heard it said that if you want to get something done, instead of “finding time” you need to “make time” to get it done. So whether you have the most unpredictable schedule or you already have a habit of doing what you do at the same time everyday, block out the times anyway so that you can rearrange other tasks in the case you have an emergency. Now, you can make sure you reschedule that time block of personal or work or time for someone else to another alternative time in your calendar. This will also ensure you realize the sacrifice you are making when you say yes without checking. It is understandable when you are good at something, many will want your time yet making time for what’s also important to you ensures you maintain your life quality and the acuteness of your talents. Besides appointments and holidays, block out times:</p>

                        <ul>
                            <li>for getting ready</li>
                            <li>for spending time with someone you care about</li>
                            <li>studies and reports</li>
                            <li>calendar review</li>
                            <li>personal fitness</li>
                            <li>sleep</li>
                            <li>specific tasks requiring concentration</li>
                        </ul>

                        <p>Do this at least once weekly and tally up how much time you actually spent in those designated areas. Maybe you take less time getting ready with the TV off or you learn you are not getting enough sleep. Weekly review makes a huge difference in setting new habits. <a href="/time-management-training-chicago.php">Best time management class in Chicago</a>.</p>
                    </li>
                    <li>
                        <h4>Make time for updating your calendar</h4>
                        <p>Designating time for calendar management is crucial to success and keeps you on track with your goals. Start with your large printed calendar, next, add designated times for your values on your <a href="http://momof6.com/mom-organization/7-best-calendars-that-work-families/">portable calendar</a> and third, block out calendar review time to tally how many hours you are putting toward the activities you do. You may surprise yourself and it will help you to become more conscious, focused and able to see where you need help, more discipline or how you can “make time” for people and projects that fuel your creativity for handling all that you do. </p>
                        <p>Next time someone asks for your time, look at a wider view of your calendar and always offer two options that would work for you. As you become more mindful of what you are trading off as well as how you can adjust to become more efficient, you will encourage respect from others to also respect your time. Leaving a calendar empty or changing plans without rescheduling what you do at specific times of the day can render you less effective in your own life. You need sleep! You need head starts on your workload before trips and you need to have a good idea of what trade-offs you are making for your work so you can make those up another way. <a href="/time-management-training-los-angeles.php">Best time management class in LA</a>.</p>
                        <p>Sign up for our Time Management class today for coaching on goal setting, time management implementation and learn other useful tips to keep you on track with getting things done at work and in life.</p>
                    </li>
                </ol>

            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Time Management workshops right across the country. Obtain a quote for an <a href="/onsite-training.php">Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management class reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>