<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Goal Setting</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>5 Golden Rules for Effective Goal Setting</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <img class="alignright" src="https://www.trainingconnection.com/images/Lessons/time-management/dos_and_donts_of_goal_setting.jpg" width="390" height="254" alt="Do's and Don'ts of Goal Setting">

                <h4>1. Make self-love and happiness your No.1 Goal</h4>
                <p>Self-love and inner happiness is the key to unlocking your full potential.</p>
                <p>All other goals such as career, family, wealth, health, relationships and lifestyle all become significantly easier once you have achieved inner-peace and self- acceptance.</p>
                <p>For more on <a class="time" href="/time-management-training.php">goal setting and time management classes</a> call us on 888.815.0604.</p>
                <h4>2. Set goals of varying difficulty</h4>
                <p>Set some challenging goals that stretch you and some easier goals. Easier goals can be achieved faster and will motivate you to go on to achieve more challenging goals.</p>
                <p>Challenging goals, like starting your own business or running a marathon, may seem daunting at first, however they are there to take you out of your existing comfort zone. Once achieved the rewards are enormous.</p>
                <p><em>Jack Welsh (former CEO of General Electric) said, "We have found that by reaching for what appears to be the impossible, we often actually do the impossible; and even when we don't quite make it, we inevitably wind up doing better than we would have done."</em></p>

                <h4>3. Focus on shorter-term goals</h4>
                <p>Having a 5-years plan is IMPORTANT but focus on setting shorter-term goals which can be achieved in 3-12 months. Short-term goals help you prioritize and take more immediate action. You should see your short-term goals as important stepping-stones to achieving your 5-year plan.</p>
                <p>We don’t recommend working on more than 5 goals at a time. <a href="/time-management-training-chicago.php">Chicago time management workshops</a>.</p>

                <h4>4. Set a deadline</h4>
                <p>Napoleon Hill once said, " A goal is a dream with a deadline".</p>
                <p>Procrastination is a success killer. By setting goals with deadlines you establish a sense of urgency that translates to more immediate action! <a href="/time-management-training-los-angeles.php">Los Angeles business skills classes</a>.</p>

                <h4>5. Be very specific</h4>
                <p>Rather than stating:</p>
                <p>“I want to be in a better paying position.” </p>
                <p>Describe in detail the exact kind of person you wish to be in a relationship with.</p>
                <p>“By the end of the 2017, through my hard work and dedication I will be promoted to a management position within my company.”</p>

            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Time Management Lessons</h4>
                    <ul>
                        <li><a class="time" href="/time-management/lessons/useful-tips.php">Tips for Time Management</a></li>
                        <li><a class="time" href="/time-management/lessons/managing-your-day.php">7 Ways to Better manage your Day</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Effective Time Management classes right across the country. Obtain a <a class="time" href="/onsite-training.php">quote for an onsite Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a class="time" href="/testimonials.php?course_id=41">Time Management testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>