<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Energy</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Keeping Yourself Energized at Work</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/energy.jpg" width="750" height="500" alt="Staying energized at work"></p>
                <p>When you sat down at work today, I wonder if you knew you had an unseen plan in addition to your actual plan. <a href="http://www.livescience.com/6557-brain-works-autopilot.html">Our brains record our habits</a> and they play out our productivity for the day with just the cue of sitting down. How do we stay energized and enthusiastic about our work? What habits are always throwing us on or off track to higher productivity?</p>

                <p>Need a time management class? Join an  <a href="/time-management-training.php">instructor-led time management workshop</a> by calling us on 888.815.0604.</p>

                <p>This article is a shopping list of 5 to-do’s you can bring with you to avoid finishing the day without finishing anything due to fatigue, distraction, and interactions with others outside of your control.&nbsp; </p>
                <ol>
                    <li>
                        <h4>Designate times you will get work done</h4>
                        <p>Whether you are prone to daydreaming, multiple interruptions or highly susceptible to checking different message applications throughout the day, unless you remind yourself your plan at the top of your day, you will always be thrown off track. Succumbing and complaining about these challenges never make us more effective but a little effort toward each task can make a big difference in our productivity. <a href="/time-management-training-chicago.php">Chicago group time management training</a>.</p>
                        <p>First, take your daily schedule out and pen in the times you will be focused on tasks from your to-do list for the day. Only you know how long you need to give your tasks and meetings as well as how much time may be interrupted by so many 3 minute calls and visitors at your workstation. Anticipate this! By writing this out in advance, you are creating a visual in your head of when and how long you will give yourself to make a dent in your work and get things done. Success has a lot to do with <a href="http://www.huffingtonpost.com/srinivasan-pillay/the-science-of-visualizat_b_171340.html">seeing your win</a> in your head in advance so focus on the top three things you will definitely check off completing at the end of the day.</p>
                    </li>
                    <li>
                        <h4>Choose break times</h4>
                        <p>Jim Loehr and Tony Schwartz who wrote <a href="https://www.amazon.com/Power-Full-Engagement-Managing-Performance/dp/0743226755">The Power of Engagement</a> describe our most efficient performance is delivered in focused intervals or sprints instead marathons of concentration. When we work non-stop on the same kind of work, we find our wheels spin either by rewriting or rereading a sentence furiously or we suddenly feel the need to eat or focus on other things. When this happens, you are right to stop your work and allow your brain and body a break but many linger at their desks because the thought of getting up seems too much work or they guilt themselves over the abandonment of their work. Let’s rethink this! <a href="/time-management-training-los-angeles.php">Time Management training course</a> in Los Angeles.</p>
                        <p>When you take a break, step away from your work, take your snack with you and go directly to an area designated for break time and snacks. The point here is that eating, checking social media and handling personal calls at your desk compromises your focus whenever you are at your desk. The habit to web surf or daydream also becomes what you also feel like doing when you sit to work. If your work environment does not have a break area per se, then pick another location that does not distract other working peers. Step outside and get some air. Finally, avoid imposing your break time habit on someone else at their workstation no matter how willing they are for the distraction. </p>
                    </li>
                    <li>
                        <h4>Choose quality snacks and beverages</h4>
                        <p>Your snack selection will affect your focus when you return to your seat so instead of hoping the snack machine will have the right foods, prepare energy snacks that do not spike and crash your sugar levels in advance. Foods that are <a href="http://advances.nutrition.org/content/5/2/119.full">high glycemic</a> such as chocolate covered bars or potato chips can put you in a concentration fog or crash later in the day. Choose instead pre-portioned bags of nuts, oatmeal or veggies and dip so that you can avoid a blood <a href="http://www.seattletimes.com/seattle-news/health/how-to-avoid-the-sugar-crash/">sugar crash</a> bound to happen an hour later or from getting into a habit of a constant intravenous desk snack such as a bag trail mix you finish before the day is through. Eating an entire bag of nuts can cost as many calories as a Thanksgiving dinner with seconds! When you do choose your lunch, avoid eating extra hearty or sweet so that you are not returning to your seat sleepy.</p>
                        <p>Drinking as much water through the day is good for your energy, body, and <a href="http://www.sciencedirect.com/science/article/pii/S0195666312003005">mind</a>. Be very wary of getting on so many of these ‘uppers’ daily energy drinks that they cease to work more. It does you no favor to be jittery and unable to focus. While coffee has some great mineral benefits, be careful with so many energy drinks as the overconsumption of these stimulants can lead to <a href="https://www.drlam.com/articles/adrenal_fatigue.asp">adrenal fatigue</a>, boost anxiety, and decrease concentration.</p>
                    </li>
                    <li>
                        <h4>Cut distractions</h4>
                        <p>Turn off email, text notifications and silence your phone. Matter of fact, hide your phone, put your immediate work in full screen and keep a watch or clock somewhere visible so you can make the most of the time designated for your tasks. A kitchen timer can be a great way to manage that 90-120 minute focus Jim Loehr describes in his book. With an office, cubicle or home desk environment, you are bound to have unexpected guests so mentally put a timer on them too! Make it part of your greeting, “I see. I would love to talk about this- I have two minutes, how can I help?” And keep track of your time! If more time is needed, start setting up another time to continue the work or the discussion with that person. When you are at your workstation, be at work!</p>
                        <p>Finally, stop believing that you can watch your show, play your music and catch up with your friend while you are doing work designated for a specific time. <a href="http://www.forbes.com/sites/nickmorrison/2014/11/26/the-myth-of-multitasking-and-what-it-means-for-learning/#1d538c7f684c">Studies</a> show that performing two different unrelated tasks simultaneously increases error and the time it takes to do the task. Focus on one task at a time. </p>
                    </li>
                    <li>
                        <h4>Use physical activity</h4>
                        <p>When we discussed taking breaks, you likely focused on stepping away from your desk to socialize, eat, get coffee or use the restroom. It is true that your brain needs a break but your body needs to move! Since you have been sitting for 2 to 4 hours, you may be underestimating how sitting in that position is adapting your body’s mobility and stability. Even if you are not taking an official away from the desk break, it behooves you to move your body whether while you work or when you step away from your workstation.</p>
                    </li>
                </ol>

                <p><a href="http://www.huffingtonpost.com/the-active-times/sitting-is-the-new-smokin_b_5890006.html">Sitting at a desk all day</a>, then in the car and finally at home on the couch, or hunched over a phone leaning on one hip can make your hip flexors tight, glutes and core weak as well as bring on hip dysfunction, lower back pain and eventually make it hard to perform any lifts from below or overhead. Having poor core and glute strength can lead to back pain even before you try to lift something incorrectly at home or work. Shoulders constantly in a hunched position, slowly lose their mobility and the next thing you know, you get hurt raising a bag into the overhead bin on your flight or you can barely start any pushups when you do finally decide to release some fatigue at the gym. </p>
                <p>Try these stretches for muscle activation: </p>
                <p>Back: Roll your shoulders and pull your elbows back and squeeze your shoulder blades together. Release and repeat 5 times. </p><p>Chest: Windmill your arms in circles or place your hand against a wall and push your chest forward towards and repeat both sides.</p><p>Legs: side lunge and forward lunge. Ensure your pelvis is forward, squeeze the glutes and pull in from your lower abdominals.</p>
                <p>Now you have a new approach for attaining better concentration for your busy days doing what you do. Check out <a href="/">Training Connection</a> Business Etiquette and Time Management classes for a full course of workplace performance, personal branding, personal motivation and how to make a positive impact with those you meet and serve at work.</p>

            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Time Management workshops right across the country. Obtain a quote for an <a href="/onsite-training.php">Onsite Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management class reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>