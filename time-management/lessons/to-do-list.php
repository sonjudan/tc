<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">To-Do-Lists</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Get the Most Out of Your To-Do-List</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/To-do-list.jpg" width="800" height="450" alt="Using a TO-DO-LIST"></p>

                <p>It happened again. It’s the end of the day and you have no idea where the time went or why nothing on your To-Do-List ever got checked off. Even worse, you have no idea what you accomplished. It’s happened to all of us at one point or another. We start the day with good intentions only to get off track with interruption after interruption.</p>
                <p>How do you break that cycle? What are the secrets to getting the most out of your to-do list? In a word, the number one secret is: Focus.</p>
                <p>That might seem general, just like most of our to-do lists get to be, so let’s break down ways you can focus your list for greater efficiency.</p>

                <p>For more ideas and help managing your time, <a href="/time-management-training.php">join us at our next Effective Time Management training in Chicago or Los Angeles.classes</a> call us on 888.815.0604.</p>

                <p><strong>Keep Multiple Lists</strong> That’s right - multiple. Why? Because are you really going to get done today the 150 things that are written on your list right now? One of the reasons we go in circles is there is too much facing us and we can easily reach a place of overwhelm. Keeping multiple lists allows us to know we have a safe place for every item that we can review, add to, and check off as appropriate. I keep a few different lists including Projects, Someday/Maybe, Today, and This Week. </p>
                <p><em>Someday/Maybe</em> serves as a place to capture all of the ideas that come into my mind. Once they are there, I know I can go back and review them at any time and it frees up mental energy to work on my more pressing items. </p>
                <p><em>Projects</em> is a list of projects I am working on, that are to be completed in less than 12 months, and have multiple action steps. <a href="/time-management-training-chicago.php">Productivity training in Chicago</a>.</p>
                <p><em>This Week</em> holds what actions I want to have accomplished by week’s end.</p>
                <p><em>Today</em> is where the magic is. Instead of putting those 150 things on today’s list, only add the top 3-6 items that are of the highest priority. Start working on number one. If you get interrupted by a phone call, urgent request, etc., refocus by going right back to number one until it is completed. Then and only then, move onto number two. Anything left at the end of the day gets moved to the top for tomorrow.</p>
                <p><strong>What are you focusing on?</strong> Being busy is very different from being productive. While multiple lists help you stay organized and focused on the next step in each of your projects they don’t always show you why things aren’t getting checked off. It takes a little investigative work to figure out why time is slipping by and you’re not feeling accomplished. Aside from doing a time log, and analyzing every action you take in a day, there are quicker ways to make a big impact on your efficiency such as prioritizing your list, acknowledging what you’ve done, and deciding what to stop doing.</p>
                <p>Prioritizing your list can look different based on your position and responsibilities. Prioritizing by timing, or when it needs to get done is very common. I encourage you to look at other things as well such as the impact of the task on the business/bottom line. For example, if the project and associated tasks has a projected revenue generation, write the number next to the item and work on the biggest impact items first. <a href="/time-management-training-los-angeles.php">Los Angeles time management classes</a>.</p>
                <p>We’ve all heard the joke about adding things to your list that you’ve completed just so you can feel the euphoria of checking it off. As much as it is joked about, it is a great way to keep track of what you have been doing as well as helping you see what you have accomplished in the day. It can also help with the next tip, the stop doing list.</p>
                <p>There are fillers in all of our days. One of the most powerful tools I was introduced to was the Stop Doing List. Look at what you are doing in the day that can be done by someone else or that simply doesn’t have a need to be done. Pare down your usual lengthy to-do list by committing to stop doing these time wasters. <a href="/time-management-training-chicago.php">Chicago-based time management training</a>.</p>
                <p><strong>Do you attack your list as it comes or do you create the time and space to achieve your daily goals?</strong> Systematizing how you approach your list frees up your mental energy to get to work faster. Here are some quick systems you can set up that make it easier to find what s next to do:</p>
                <ul>
                    <li>Section your list by activity. For example, make a list of phone calls to make, emails to return, etc.</li>
                    <li>Include details. When you add something to list, also note what you need to accomplish it like the phone number is it is a call, or a reminder of what file you’ll need access to.</li>
                    <li>Schedule it out. I’m a big fan of time blocking and find it helpful to schedule time to work on a project as I would a meeting on my calendar. That is my undisturbed time to focus on the task at hand.</li>
                    <li>Make a check list of daily activities. If you have a set of daily activities, make a check list of them and laminate it. Every day use a dry erase marker to check them off as you complete them, and then wipe off the checks at the end of the day. Nothing gets lost, and you start each day with a clean slate.</li>
                </ul>
                <p>About the author: Lena Salonikas is a Certified Life and Health Coach that works with you to achieve peak performance in your work and life. She can be found leading professional development classes at our Chicago location.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Time Management workshops right across the country. Obtain a quote for an <a href="/onsite-training.php">Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management class reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>