<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Getting it done</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Overcoming Procrastination - Just do it!</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Just do it! Why don’t we just do it? The problem with New Year’s Resolutions is that people too easily succumb to what they would rather be doing than what they wish they were doing to achieve the goals in their life.</p>
                <p>To learn more about our <a href="/time-management-training.php">instructor-led Time Management classes</a> call us on 888.815.0604</p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/procrastination.jpg" width="780" height="260" alt="How to stop procrastination"></p>


                <p>As a Time Management Trainer, teaching participants on mindset and prioritization strategies can fall on deaf ears if participants do not identify how they will make their goal-getting enjoyable and foresee in advance other challenges that are a likely to distract or dull their attempt to enjoy the journey of doing what they set out to do. <a href="http://www.biography.com/people/arthur-ashe-9190544">Arthur Ashe</a> said, “Success is a journey, not a destination. The doing is often more important than the outcome.” Let’s look at how “Just do it” affects chronic procrastinators and how we can use Attainability questions when setting goals that ensure meet our basic needs to enjoy getting things done!</p>
                <h4>Why not JUST DO IT?</h4>
                <p>APS Fellow <a href="https://www.psychologicalscience.org/publications/observer/2013/april-13/why-wait-the-science-behind-procrastination.html">Joseph Ferrari</a>, a professor of psychology at DePaul University and pioneer of modern research on procrastination says. “To tell the chronic procrastinator to <em>just do it</em>&nbsp;would be like saying to a clinically depressed person,&nbsp;<em>cheer up</em>.” Social scientists debate whether the existence of this action gap can be better explained by the inability to manage time or the inability to regulate moods and emotions. <a href="/time-management-training-chicago.php">Chicago onsite time management training</a>.</p>
                <p>Tice and Ferrari teamed up to do a study that put the ill effects of procrastination. They told students at the end of a lab session they’d be engaging in a math puzzle. Part of the group was informed the puzzle was a meaningful test of their cognitive abilities and the other group was told the puzzle was designed to be meaningless and fun. Before doing the puzzle, the students had an interim period during which they could prepare for the task or mess around with games like Tetris. As it happened, students who identified as procrastinator only delayed practice on the puzzle when it was described as a cognitive evaluation and behaved no differently from non-procrastinators. </p>
                <p>“Emotional regulation, to me, is the real story around procrastination, because to the extent that I can deal with my emotions, I can stay on task,” says Pychyl. The idea is that procrastinators comfort themselves in the present with the false belief that they’ll be more emotionally equipped to handle a task in the future. </p>
                <p>“The chronic procrastinator, the person who does this as a lifestyle, would rather have other people think that they lack effort than lacking ability,” says Ferrari. “It’s a maladaptive lifestyle.” </p>
                <p>What we can learn from this is that we are avoiding work we don’t enjoy because of how it makes us feel and what the outcomes could mean rather than ensuring we just do it what we set out to do. </p>
                <p>Also read - <a href="/time-management/lessons/prioritizing-time.php">How to Prioritizing Your Time</a></p>

                <h4>HOW TO MAKE YOUR GOAL ATTAINABLE</h4>
                <p>Once you set a goal, it is critical to go through the process of determining attainability. Attainability is one of the letters of <a href="http://topachievement.com/smart.html">SMART</a>, an acronym we use to set goals that are Specific, Measureable, Attainable and Time Bound. </p>
                <ol>
                    <li>Take your Measurable <a href="https://www.theguardian.com/technology/2016/apr/20/seven-of-the-best-to-do-list-apps">To-Do List</a> and consider distractions and emergencies that can interrupt getting things any of these items done</li>
                    <li>Plan what you will do to ensure you get your task done. How will you make it more fun? How will you plan alternating a task in advance? What will you do if something more fun happens during that time? What will you tell yourself when you don’t feel like doing it. </li>
                </ol>

                <p>For example, if you create a measurable “to-do” of going to the gym for 3 times a week for a specific goal losing 20lbs, you will need to consider what distraction or challenge could set you back in advance. <a href="/time-management-training-los-angeles.php">LA-based time management course</a>.</p>
                <p>Let’s say you have a favorite show that comes on during the time you would workout? To make this attainable, what will you do to ensure you get your three workouts in at that time? You could: </p>
                <ol>
                    <li>record and watch the show at another time and let your friends know not to share spoilers</li>
                    <li>enjoy the show when you are doing low impact cardio on a machine </li>
                    <li>exercise at home during the show. (This option probably is the worst because then, you will have only added more convenient distractions!)</li>
                    <li>exercise earlier and affirm that when you finish, you can watch your show</li>
                </ol>
                <p>Still, you will need to determine what matters more for you to do in your life- watch your show or achieve your goals. Instead of putting your work off, find a way to make it more meaningful to do what you can rather than waiting for yourself to feel undistracted by all you could be doing. </p>
                <p>When you discover you have this tendency, it is important for you to ensure that you get the counseling, the personal inventory checkpoints so that you can still achieve your goals. </p>
                <p>Also see <a href="/time-management/lessons/managing-your-day.php">7 Ways to Better manage your Day</a></p>


            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite Effective Time Management classes right across the country. Obtain a <a href="/onsite-training.php">quotation  for Time Management training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>