<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/time-management.php">Time Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Prioritzing Time</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Prioritizing Your Time</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Time management is about more than just managing our time; it is about managing ourselves, in relation to time. It is about setting priorities and taking charge. It means changing habits or activities that cause us to waste time. It means being willing to experiment with different methods and ideas to enable you to find the best way to make maximum use of time. </p>
                <p>Why not join on of our <a href="/time-management-training.php">hands-on practical Time Management classes</a>? </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/priorizing-time.jpg" width="750" height="500" alt="When you Prioritize your Time, all the important stuff happens"></p>

                <h4>The 80/20 Rule</h4>
                <p>The 80/20 rule, also known as Pareto’s Principle, states that 80% of your results come from only 20% of your actions. Across the board, you will find that the 80/20 principle is pretty much right on with most things in your life. For most people, it really comes down to analyzing what you are spending your time on. Are you focusing in on the 20% of activities that produce 80% of the results in your life?</p>
                <p>Also see <a href="/time-management/lessons/goal-setting-tips.php">Goal setting tips </a></p>

                <h4>The Urgent/Important Matrix</h4>
                <p>Great time management means being effective as well as efficient. Managing time effectively, and achieving the things that you want to achieve, means spending your time on things that are important and not just urgent. To do this, you need to distinguish clearly between what is urgent and what is important:</p>
                <ul>
                    <li><strong>Important</strong>: These are activities that lead to the achieving your goals and have the greatest impact on your life.</li>
                    <li><strong>Urgent</strong>: These activities demand immediate attention, but are often associated with someone else’s goals rather than our own.</li>
                </ul>
                <p>This concept, coined the Eisenhower Principle, is said to be how former US President Dwight Eisenhower organized his tasks. It was rediscovered and brought into the mainstream as the Urgent/Important Matrix by Stephen Covey in his 1994 business classic, The Seven Habits of Highly Effective People. The Urgent/Important Matrix is a powerful way of organizing tasks based on priorities. Using it helps you overcome the natural tendency to focus on urgent activities, so that you can have time to focus on what's truly important. <a href="/time-management-training-chicago.php">Chicago skills training classes</a>.</p>
                <p>The Urgent/Important Matrix:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/time-management/time-matrix.jpg" width="756" height="513" alt="Urgent / Important Mitrix"></p>

                <ul>
                    <li><strong>Urgent And Important</strong>: Activities in this area relate to dealing with critical issues as they arise and meeting significant commitments. Perform these duties now.</li>
                    <li><strong>Important, But Not Urgent:</strong> These success-oriented tasks are critical to achieving goals. Plan to do these tasks next.</li>
                    <li><strong>Urgent, But Not Important: </strong>These chores do not move you forward toward your own goals.<strong> </strong>Manage by delaying them, cutting them short, and rejecting requests from others. Postpone these chores.</li>
                    <li><strong>Not Urgent And Not Important: </strong>These trivial interruptions are just a distraction, and should be avoided if possible. However, be careful not to mislabel things like time with family and recreational activities as not important. Avoid these distractions altogether.</li>
                </ul>

                <h4>Being Assertive</h4>
                <p>At times, requests from others may be important and need immediate attention. Often, however, these requests conflict with our values and take time away from working toward your goals. Even if it is something we would like to do but simply don’t have the time for, it can be very difficult to say no. One approach in dealing with these types of interruptions is to use a Positive No, which comes in several forms. <a href="/time-management-training-los-angeles.php">Instructor-led time management classes in Los Angeles</a>.</p>
                <ul>
                    <li>Say no, followed by an honest explanation, such as, “I am uncomfortable doing that because…”</li>
                    <li>Say no and then briefly clarify your reasoning without making excuses. This helps the listener to better understand your position. Example: “I can’t right now because I have another project that is due by 5 pm today.”</li>
                    <li>Say no, and then give an alternative. Example: “I don’t have time today, but I could schedule it in for tomorrow morning.”</li>
                    <li>Empathetically repeat the request in your own words, and then say no. Example: “I understand that you need to have this paperwork filed immediately, but I will not be able to file it for you.”</li>
                    <li>Say yes, give your reasoning for not doing it, and provide an alternative solution. Example: “Yes, I would love to help you by filing this paperwork, but I do not have time until tomorrow morning.”</li>
                    <li>Provide an assertive refusal and repeat it no matter what the person says. This approach may be most appropriate with aggressive or manipulative people and can be an effective strategy to control your emotions. Example: “I understand how you feel, but I will not [or cannot]…” Remember to stay focused and not become sidetracked into responding to other issues.</li>
                </ul>
            </div>

        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Time Management Lessons</h4>
                    <ul>
                        <li><a href="/time-management/lessons/useful-tips.php">Tips for Time Management</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Time Management training</h4>
                    <p>Through our network of local trainers we deliver onsite effective Time Management right across the country. Obtain a quote for an <a href="/onsite-training.php">onsite Time Management class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=41">Time Management student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>