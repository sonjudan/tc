<?php
$meta_title = "Business Communication course in Chicago and Los Angeles | Training Connection";
$meta_description = "Business Communication classes offered in Chicago & Los Angeles. Don't settle for an average class! For great a deal or group quotation call 888.815.0604.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder" aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted" >
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="#" itemprop="url">
                        <span itemprop="title">Business Communication Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-customer-communication.jpg');">
        <div class="container">
            <div class="masterhead-copy">
                <h1 class="mb-2" data-aos="fade-up" >Business Communication Training</h1>

                <div data-aos="fade-up" data-aos-delay="100">
                    <h4 class="mt-0">Instructor-led business communication classes in Chicago and Los Angeles</h4>

                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                        Book Course
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-intro-2" style="background-image:url('dist/images/bg-hero-5.jpg')">
        <div class="container">
            <div class="section-heading aos-init aos-animate" data-aos="fade-up">
                <h2>Effective Business Communication</h2>
            </div>

            <div class="mb-3" data-aos="fade-up" data-aos-delay="50" class="aos-init aos-animate">
                <p>Better communication skills (with colleagues and clients) can give an organization a huge competitive advantage. Not only does it help build a positive working environment with fewer grievances and less staff turnover, but it will also result in greater efficiency, more creativity and improved problem solving.</p>
                <p>This Business Communication course is designed to help participants  improve their  communication  with others in the workplace. The course is interactive, fast-paced, fun, and full of tips and techniques you can use immediately.</p>
            </div>
        </div>
    </div>

    <div id="section-book-course" class="section section-book-class">
        <div class="container">

            <div class="section-heading  w-auto text-center" data-aos="fade-up">
                <h2>Book a Communication Class</h2>
            </div>

            <div class="book-class">
                <div class="copy">
                    <h3 class="title-l2" data-aos="fade-up" data-aos-delay="250">What's Included</h3>
                    <ul data-aos="fade-up" data-aos-delay="250">
                        <li>Class certificate</li>
                        <li>Training materials</li>
                        <li>FREE repeat valid for 6 months</li>
                    </ul>
                </div>
                <div class="price-box" data-aos="fade-up" data-aos-delay="300">
                    <h3>$350</h3>
                    <h4>1 Day</h4>
                    <p>9:00am - 4:30pm</p>
                </div>
            </div>

            <div class="course-actions" data-aos="fade-up" data-aos-delay="350">
                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Business Communication Class - 1 DAY 9:00am 4:30pm" data-price="$350">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>

                <div class="btn-group dropup">
                    <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                        Timetable
                    </button>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Business Communication" >
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable Chicago
                        </a>
                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Business Communication" >
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable LA
                        </a>
                    </div>
                </div>

                <a href="/downloads/business-skills/Effective%20Business%20Communication.pdf" class="btn btn-dark btn-lg" target="_blank">
                    <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                    Download PDF
                </a>

                <a href="#section-course-services" class="btn btn-blue btn-lg js-anchor-scroll">
                    <span class="iconify" data-icon="simple-line-icons:people" data-inline="false"></span>
                    Group Training
                </a>
            </div>
        </div>
    </div>

    <div class="section section-course-detail">
        <div class="container">
            <div class="section-heading aos-init aos-animate w-auto text-center" data-aos="fade-up">
                <h2>Detailed Course Outline</h2>
            </div>
            <h3 class="title-l2">Learning Objectives</h3>

            <div class="row">
                <div class="col-lg-6">
                    <div class="copy copy-ul-sm">
                      <p><strong>Upon successful completion of this course trainees will be able to:</strong></p>
                      <ul class="">
                        <li>Know what makes a great communicator</li>
                        <li>Understand different communication styles and techniques</li>
                        <li>Make a good first impression</li>
                        <li>Build rapport with people</li>
                        <li>Read body language</li>
                        <li>Listen effectively</li>
                        <li>Ask and answer questions</li>
                        <li>Promote ideas and inspire and motivate buy-in</li>
                        <li>Deliver and receive constructive criticism</li>
                        <li>Communicate better with supervisors, colleagues and vendors</li>
                        <li>Understand how communication can define a company's culture.</li>
                      </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="/dist/images/courses/featured-img-business-communication.jpg" alt="Business Communication">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-outline-bs">
        <div class="container">
            <h3 class="title-l2">Lesson Summaries</h3>

            <div class="row">
                <div class="col-md-6 pr-2">
                    <div class="accordion accordion-l2" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading0">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                    How well do you Communicate? <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>In this unit we look at what makes a great communicator, and the 4 basic principles and techniques great communicators all share.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Understand different Types and Styles of Communication <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Understanding the different styles is important not only for tailoring your own communications but for communicating with others that use these styles.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Communication Mastery <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>In this unit, we look at basic human nature to get a better understanding of people. This will allow you to become a skillful communicator, with the ability to build rapport and influence others with ease.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    How to create Clear, Compelling Messages <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>In this unit you learn to match your nonverbal with your message, eliminate barriers, eliminate weak and vague words, and make sure you are fully understood.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading4">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Communication Shutdowns <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Shutdowns (or put downs) are a destructive method of shutting down communication, and discouraging healthy debate.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading5">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Listening Skills <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>In this unit we learn the value of listening before evaluating your current listening skills. We discuss the 4 different listening skills, and identify barriers to effective listening before learning new techniques to improve your listening.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pl-2">
                    <div class="accordion accordion-l2" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading6">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Verbal and Non-Verbal Communication Skills <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>First and last impressions matter. In this unit, we look at how to build rapport, read body language, the power of questions and funding the right voice and tone.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading7">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Use Engaging Language <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>An I-message is a technique used to provide feedback in a manner that avoids  judgment or assigning blame.  Using engaging language will also improve your ability to motivate and inspire others and gain buy-in.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading8">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    Bringing People to your Side <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>In this unit you will learn to speak from the heart (not just the brain). You will The 7 C’s of effective communication etiquette.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading9">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Deliver and receive Constructive Criticism <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>In this unit you will learn the importance of Emotional Intelligence and how to provide constructive criticism, and allowing the recipient to save face. When it’s appropriate to provide verbal versus written criticism. We also discuss intercultural communication and the Politeness theory.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading10">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    Communication in the Workplace <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>Here we discuss some guidelines for communicating with co-workers in  a more respectful way.  Communicating with supervisors can be tricky, so we look at some ways to communicate better with supervisors.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-copy" data-aos="fade-up">
                <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
            </div>
        </div>
    </div>

    <div class="section section-trainers">
        <div class="container">
            <div class="section-heading">
                <h2>Meet the trainers</h2>
                <h3>We have a team of experienced business communication trainers</h3>
            </div>
            <div class="owl-carousel owl-instructors owl-theme instructors-list-block">
                <div class="list-block">
                    <a href=".modal-instrutor-profile-carol" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-carol.jpg')"></i>
                        <span>Carol</span>
                    </a>
                </div>
                <div class="list-block">
                    <a href=".modal-instrutor-profile-secret" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-secret.jpg')"></i>
                        <span>Secret</span>
                    </a>
                </div>
                <div class="list-block">
                    <a href=".modal-instrutor-profile-sandra" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-sandra.jpg')"></i>
                        <span>Sandra</span>
                    </a>
                </div>
            </div>

            <div class="course-rating" data-aos="fade-up" data-aos-delay="150">
                <div class="star-rate star-rate-4_7">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Business Communication classes - 5 rating"></span>
                </div>

                <h3>Google 4.7 star <span>By 516 trainees</span></h3>
            </div>



            <div data-aos="fade-up" data-aos-delay="100">
                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;TI have sent several of my coworkers to this company for training before I came to a class myself. They all raved about the class material, the instructors and how much learned from taking the classes. I would whole-heartedly agree and will make sure to tell everyone that ever needs to take a training class to come here. Juana was wonderful. She listened well, made examples that were appropriate to my life and needs and took the time to work on the difficulties that I specifically had. I can't say enough good things about the instructors. I am walking away from the class with skills that I can really use.&quot;</p>
                            <span><strong>Karen Nelson | Family Health Network</strong></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;TIncredibly refreshing. Most courses I've taken have been so book-oriented, they lost sight of practical instruction. I much prefer the activity / example based method of instruction. Thank you!&quot;</p>
                            <span><strong>Walter Palka | Novartis</strong></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php/?course_id=24" class="btn btn-primary btn-lg">View more</a>
            </div>
        </div>
    </div>

    <div id="section-course-services" class="section section-course-services">
        <div class="container">
            <div class="section-heading">
                <h2>Group Business Communication Training</h2>
                <p>We have trained thousands of employees from all different industries.</p>

                <h3>
                    Improving the communication between   employees has tremendous benefits <br>
                    for any organization.
                </h3>
            </div>

            <div class="service-list row justify-content-center">
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="service-item">
                        <div class="service-icon">
                            <img src="/dist/images/icons/icon-happy.svg" alt="Happier and less stressed workplace" width="72" height="72">
                        </div>
                        <p>Happier and less stressed workplace</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="service-item">
                        <div class="service-icon">
                            <img src="/dist/images/icons/icon-turnover.svg" alt="Low staff turnover" width="91" height="72">
                        </div>
                        <p>Low staff turnover</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="service-item">
                        <div class="service-icon">
                            <img src="/dist/images/icons/icon-cooperation.svg" alt="More efficient workforce" width="130" height="46">
                        </div>
                        <p>More efficient workforce</p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="service-item">
                        <div class="service-icon">
                            <img src="/dist/images/icons/icon-profit.svg" alt="Great profitability" width="102" height="72">
                        </div>
                        <p>Great profitability</p>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <p class="title-l4 mt-3 mb-4">Training can be fully customized to address any existing challenges facing your organization.</p>
                <a href="#section-course-form" class="btn btn-secondary btn-lg js-anchor-scroll">Obtain Pricing</a>
            </div>
        </div>
    </div>

    <div id="section-course-form" class="section section-group-training section-td-arrow">
        <div class="container">
            <div class="section-heading mb-0">
                <h2 data-aos="fade-up">Group Training Quotation</h2>
            </div>

            <div class="section-body">
                <form action="" class="form-group-training">

                    <h4 class="text-center" data-aos="fade-up" data-aos-delay="100">Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>
                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>
                        </label>
                    </div>

                    <hr class="mt-4 mb-4" data-aos="fade-up" data-aos-delay="150">

                    <ul class="list-radio-checkbox row-3 mb-4" data-aos="fade-up" data-aos-delay="200">
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]" checked><i></i> Business Communication</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Etiquette</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Leadership</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Writing</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Conflict Resolution</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Customer Service</label></li>
                       <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Grammar</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Presentations</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Project Management</label></li>
                        <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Time Management</label></li>
                    </ul>

                    <div class="" data-aos="fade-up" data-aos-delay="200">
                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="No. of Trainees*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="First Name*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Last Name*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Phone no*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address 1*">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address 2">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="City*">
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="State*">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Zip*">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-actions"  data-aos="fade-up" data-aos-delay="130">
                        <button class="btn btn-secondary btn-lg"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

    <div class="section section-faq">
        <div class="container">

            <div class="section-heading mb-4">
                <h2 class="section-title">FAQ</h2>
            </div>

            <div class="card-columns card-faq-list">
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                        Where do your Business Communication classes take  place?
                  </h4>
                    <div class="card-body">
                        <p>We have two  training centers, one located in  Chicago (230 W Monroe, Suite                         610, Chicago IL 60606) and the other in Los  Angeles (915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017).                         </p>
                    </div>
                </div>
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                        What are the Business Communication class times?
                  </h4>
                    <div class="card-body">
                        <p>Our Business Communication classes start at 9.00am and finish at approximately 4.30pm. We take an hour lunch break around 12.15pm.</p>
                    </div>
                </div>
                <div class="card card-faq">
                    <h4 class="card-title">
                        <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                        Can you deliver Business Communication training onsite at our location?
                    </h4>
                    <div class="card-body">
                        <p>Yes, we service the greater Chicago and Los Angeles metros. We also provide onsite Business Communication training at client sites right across the country. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Business Communication training.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-resouces-hero l2"  style="background-image:url('/dist/images/bg-hero-8.jpg');">
        <div class="container">
            <div class="section-body">
                <h2>Resources</h2>
                <p>For more resources and articles on <br>effective business communication</p>

                <a href="/resources/business-communication.php" class="btn btn-primary btn-lg"><i class="fas fa-angle-double-right mr-2"></i> Read more</a>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-carol.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-secret.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-sandra.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>