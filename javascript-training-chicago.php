<?php
$meta_title = "JavaScript/jQuery Training Classes | Chicago | Training Connection";
$meta_description = "Need to learn JavaScript/jQuery? Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">JavaScript/jQuery Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-web" style="background-image: url('/dist/images/banner-js.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">JavaScript/jQuery Training</h1>

                    <div data-aos="fade-up">
                        <h4>Chicago</h4>
                        <p>Looking for a JavaScript class in Chicago?</p>
                        <p>We offer hands-on JavaScript classes taught by live instructors present in the room</p>
                        <p>Go beyond HTML and CSS and learn JavaScript so you can add interactivity to your websites such as animated slideshows, lightboxes (image enlargers), show/hide content, form validation, and more.</p>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">

                <div class="mb-4 mt-md-4 float-md-right"  data-aos="fade-up">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>

                <h3>What's Included</h3>
                <ul>
                    <li>JavaScript training manual</li>
                    <li>Certificate of Course Completion</li>
                    <li>Free repeat valid for 6 months (in case you need a refresher)</li>
                </ul>
                <p>
                    <strong>Book a JavaScript training course today. All classes guaranteed to run!</strong> <br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes border-bottom pb-0" data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0"  data-aos="fade-up">
                <h2>JavaScript/jQuery Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-javascript" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="Understanding the JavaScript/jQuery Interface"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    JavaScript and jQuery
                                </a>
                            </h3>
                            <p>This JavaScript class is suitable for beginners. You will learn how to create better user experiences by adding interactive elements for your Web pages. This class will introduce you to JavaScript syntax, before delving into jQuery the World's most popular JavaScript library.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="JavaScript and jQuery" data-price="$1495"
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book this course
                                </a>
                                <a href="/downloads/javascript/JavaScript%20Fundamentals.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="JavaScript and jQuery">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1495
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-outline-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Fundamentals of JavaScript Code</h5>
                                        <p>In this lesson, you will learn some foundational concepts and syntax such as variables, strings, and numbers. These are core concepts used throughout even the most complicated scripts.</p>
                                        <ul>
                                            <li>JavaScript methods (such as alerts)</li>
                                            <li>Variables</li>
                                            <li>The importance of quotes</li>
                                            <li>Numbers vs. strings</li>
                                            <li>Concatenation</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Reusing Code with Functions</h5>
                                        <p>A function is a group of reusable code that performs a specific task (or tasks) at a desired time. In this exercise, you will learn how to write a custom function.</p>

                                        <ul>
                                            <li>Defining functions</li>
                                            <li>Calling functions</li>
                                            <li>Defining parameters &amp; passing arguments</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Targeting HTML Elements</h5>
                                        <p>In this exercise, we will take a look at what JavaScript is doing in the background. We will use Chrome's DevTools to reveal the structure of an HTML document, select individual HTML elements and edit them.</p>
                                        <ul>
                                            <li>Selecting &amp; Working with HTML Elements</li>
                                            <li>Getting &amp; Setting Input Values</li>
                                            <li>Hiding &amp; showing elements with JavaScript</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">If Statements: Clearing Form Fields</h5>
                                        <p>There are times when you only want your JavaScript code to be executed if certain criteria have been met.  In this exercise, you will use  an IF statement to detect   whether a form field contains text and then respond with a hint if necessary.</p>
                                        <ul>
                                            <li>Testing code in the JavaScript Console</li>
                                            <li>Getting &amp; setting properties</li>
                                            <li>Using if statements</li>
                                            <li>Reshowing text hints in empty form fields</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Simple Accordion with JavaScript</h5>
                                        <p>Accordions let you condense a lot of information into a small space by hiding some of it. In this exercise, you are going to build a simple accordion  to hide and show different elements. </p>

                                        <ul>
                                            <li>Targeting elements by ID</li>
                                            <li>Hiding &amp; showing elements with JavaScript</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sharing JavaScript Across Pages</h5>
                                        <p>In this exercise, we will learn how to externalize JavaScript so it can be shared between pages.</p>
                                        <ul>
                                            <li>Externalizing JavaScript</li>
                                            <li>Linking to the JavaScript File</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Introduction to Arrays &amp; the Math Object</h5>
                                        <p>In this exercise, we will learn about arrays and how to create them. You will also learn about Math objects and how to use them in conjunction with arrays to display the values we want.</p>

                                        <ul>
                                            <li>Creating an Array of testimonials</li>
                                            <li>Editing an Array</li>
                                            <li>The Math Object</li>
                                            <li>Using the Math Object to Pick Random Testimonials</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Postcard Generator</h5>
                                        <p>In this exercise, you will build a postcard generator. The user will choose from a list of U.S. states and the page will dynamically generate a postcard for that state. </p>

                                        <ul>
                                            <li>Getting Input From the Menu</li>
                                            <li>Unobtrusive JavaScript</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Introduction to JavaScript Objects &amp; the DOM</h5>
                                        <p>In this exercise you will learn the basics about JavaScript objects. You will create your own object and learn how to access objects inside an HTML document to see how you can manipulate them.</p>
                                        <ul>
                                            <li>Intro to objects</li>
                                            <li>The global object</li>
                                            <li>Breaking open &amp; manipulating objects</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Dynamically Changing Content with Custom Objects</h5>
                                        <p>In this exercise, you will use JavaScript objects to dynamically update content without having to reload the page. In other words, when a  user makes a selection, the information on the page will update without the page having to reload.</p>

                                        <ul>
                                            <li>Checking the functionality of the select menu</li>
                                            <li>Getting the chosen value</li>
                                            <li>Dynamically changing the state name value</li>
                                            <li>Dynamically changing the rest of the values</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Introduction to For Loops</h5>
                                        <p>One of the things that programming is really good for is repeating the same (or similar) actions very quickly. In this exercise, we will look at one way to do this by using a for loop.</p>
                                        <ul>
                                            <li>Creating a for loop</li>
                                            <li>Using the for loop to set dropdown menus</li>
                                            <li>Clearing the To menu</li>
                                            <li>Optional bonus: refining the menu selection experience</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Introduction to jQuery: Showing &amp; Hiding Content</h5>
                                        <p>jQuery is an industry standard JavaScript library.  In this exercise, you will get started using jQuery by learning how to show and hide some content using a few different animations.</strong></span></p>
                                        <ul>
                                            <li>Getting started with jQuery</li>
                                            <li>Using the jQuery Library</li>
                                            <li>Running code when the document is ready</li>
                                            <li>Click events</li>
                                            <li>Using jQuery’s slideToggle() method</li>
                                            <li>Supporting JavaScript disabled users</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Product Color Chooser</h5>
                                        <p>When products are available in multiple colors, users need to be able to choose the the color they selected.</strong></span></p>
                                        <ul>
                                            <li>Getting the Swatch Buttons to Work</li>
                                            <li>Figuring Out Which Color the User Clicked On</li>
                                            <li>Change the Border Color of the Selected Element</li>
                                            <li>Using Hover Instead of Click</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">More Advanced jQuery Showing/Hiding</h5>
                                        <p> In this exercise you will take another look at showing and hiding, but with a new twist. You will learn how to control the speed of jQuery's slideToggle() and learn more about targeting elements with jQuery.</strong></span></p>
                                        <ul>
                                            <li>Adding an animation to reveal hidden content</li>
                                            <li>Targeting the proper div: traversing the document</li>
                                            <li>Swapping the button image with jQuery</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Using jQuery Plugins: Smooth Scroll</h5>
                                        <p>In this exercise, you will learn how to use a jQuery plugin called Smooth Scroll. It lets you create a single page site with navigation that scrolls down the page with a sliding animation.</strong></span></p>
                                        <ul>
                                            <li>Linking to the plugin file</li>
                                            <li>Initializing the plugin</li>
                                            <li>Customizing the plugin with options</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">jQuery Lightbox: A Pop-up Image Viewer</h5>
                                        <p>In this exercise, you will learn how to use a free jQuery lightbox plugin called Magnific Popup. This is a great way for users to view enlarged images.</strong></span></p>
                                        <ul>
                                            <li>Linking to the plugin files</li>
                                            <li>Initializing the pop-up</li>
                                            <li>Grouping the photos into a gallery</li>
                                            <li>Adding captions</li>
                                            <li>Removing the counter</li>
                                            <li>Customizing the appearance</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">jQuery Cycle: A Simple Slideshow</h5>
                                        <p>In this exercise, you will learn how to use the jQuery Cycle plugin to create a simple slideshow. It can cycle through content with a variety of transitions like fades and pushes.</strong></span></p>
                                        <ul>
                                            <li>Initial setup</li>
                                            <li>Defining what content gets cycled</li>
                                            <li>Adding more cycles &amp; exploring options</li>
                                            <li>Reversing the animation</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">jQuery Cycle: Adding Slideshow Controls</h5>
                                        <p>In this exercise, you will add controls to a slideshow which will allow the end user to jump ahead or back to specific slides.</strong></span></p>
                                        <ul>
                                            <li>Preventing a possible "flash of unstyled content"</li>
                                            <li>Enabling the slideshow</li>
                                            <li>Adding &amp; customizing the controls</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">jQuery Form Validation</h5>
                                        <p>Form validation is essential for so many websites. Here we will show you how to set up form validation with the popular jQuery Validation Plugin by Jörn Zaefferer, which is easy, fast, flexible, and well-documented.</strong></span></p>
                                        <ul>
                                            <li>Initializing the plugin &amp; setting options</li>
                                            <li>Customizing the error messages</li>
                                            <li>Changing the location of the error messages</li>
                                            <li>Styling the error messages</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">jQuery Image Carousel</h5>
                                        <p>Carousels are one way to present a series of images and/or text for users to scroll through. The free OWL Carousel plugin makes creating these easy.</strong></span></p>
                                        <ul>
                                            <li>Linking to the plugin files</li>
                                            <li>Creating the carousel</li>
                                            <li>Styling the carousel</li>
                                            <li>An easy way to add prev &amp; next buttons</li>
                                            <li>Adding custom prev &amp; next buttons</li>
                                            <li>Setting how many items are shown</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>

    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Javascript training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>

                    <h3><span>Javascript classes rating:</span> 4.7 stars from 409 reviewers </h3>
                </div>

                <div data-aos="fade-up">
                    <div class="owl-carousel owl-instructors owl-theme instructors-list-block " data-aos="fade-up">
                        <div class="list-block">
                            <a href=".modal-instrutor-profile-chris" data-toggle="modal">
                                <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-chris.jpg')"></i>
                                <span>Chris</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Olivier is a great instructor--he encouraged me to ask questions and helped me understand the subject matter better than any other tutorial or course I've taken on this subject. He is also very personable and fun, but professional as well. I would definitely take another course with Olivier as the instructor.&quot;</p>
                                <span><strong>Melissa Slemin</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I really enjoyed this class. I liked particularly the teaching style of the instructor. I felt I was engaged and participated actively in the process of understanding and learning the material. The concepts were explained in a way that makes me feel that developing sites with Ajax will be equally fun as challenging.&quot;</p>
                                <span><strong>Christina Andonova | Bankers Life And Casualty Company</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Chris was exceptional at gauging our experience level on the first day and completely customizing the course for us. He was very good at working "off script". Basically, he would take our questions and examples of work we were currently doing and tailor the class to help us best meet our objectives.&quot;</p>
                                <span><strong>Ed MacDonald | ARIN</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I liked the instructor's approach in incorporating good coding practices. He was also very helpful and customized the course to serve my objectives with the course. I have very little experience in web development and this was a very good introduction to all the concepts.&quot;</p>
                                <span><strong>Jaisudha Purushothaman | Esri</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Olivier is a great teacher...his knowledge and experience are very apparent, but his teaching process and delivery were right on...excellent for communicating this type of material. I'm looking forward to other classes with him.&quot;</p>
                                <span><strong>Lena Salazar</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=48" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>


    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that JavaScript is a complex programming language and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months.<br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="c">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">JavaScript is not for everyone. If you decide that after Day 1 in class that the class is not for you, we will give you a complete refund. </p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group JavaScript Training</h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in JavaScript. This can be delivered onsite at your premises, at our training center in LOOP or via online webinar.
                    <br>
                    Fill out the form below to receive pricing.
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course[]" checked><i></i> <h3>JavaScript and jQuery</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

    <div class="section section-faq g-text-javascript">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>JavaScript and jQuery Training FAQ</h2>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5><span class="card-title">Do I need to bring my own computer?</span></h5>
                    <p>No, we provide the computers.</p>
                </li>
                <li>
                    <h5><span class="card-title">Do your JavaScript classes have a live trainer in the classroom? </span></h5>
                    <p>Yes. Our JavaScript classes are  taught by  qualified and passionate trainers present in the classroom.</p>
                </li>
                   <li>
                    <h5><span class="card-title">Where do your JavaScript training classes take place?</span></h5>
                    <p>Our training center is located at 230 W Monroe, Suite
                     610, Chicago IL 60606. For more information about directions, parking, trains please <a href="/contact-us-chicago.php">click here</a>.</p>
                </li>
                <li>
                    <h5><span class="card-title">Can you deliver JavaScript onsite at our location?</span></h5>
                    <p>Yes, we service the greater Chicago metro including Arlington Heights, Aurora, Bollingbrook, Chicago Loop, Deerfield, Des Plaines, Evanston, Highland Park, Joliet, Naperville, Schaumburg, Skokie, South Chicago and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver JavaScript  training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite JavaScript training.</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="section section-resouces-hero l2" style="background-image:url('/dist/images/bg-hero-9.png');">
        <div class="container">
            <div class="section-body">
                <h2>Resources</h2>
                <p>For more resources and articles on <br>Web development trainings</p>

                <a href="/resources/web-development.php" class="btn btn-primary btn-lg"><i class="fas fa-angle-double-right mr-2" aria-hidden="true"></i> Read more</a>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-chris.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>