<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>InDesign training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_7">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="InDesign classes - 5 rating"></span>
                </div>

                <h3><span>InDesign classes rating:</span> 4.7 stars from 641 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Jeff was patient and very knowlegable about InDesign. It was my first time using the product, but he made it easy to understand. His expertise with other Adobe products and Windows was helpful in answering questions about how to integrate this training for "real usage" back at the worksite. I highly recommend this course and instructor! &quot;</p>
                            <span><strong>Sandi Fuerte | Toyota</strong></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I am very pleased with the content of the class and even more satisfied with the software and teaching capabilities of Eva. She has been a very patient and knowledgeable instructor who provides valuable insight into what can be daunting software. I hope to take a my next Training Connection class with her.&quot;</p>
                            <span><strong>Rebecca Lund | Integrated Project Management</strong></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;If there was an option better than strongly agree, I would have chosen that. I've taken many software classes, and I thought this was one of the best. It was taught at the right pace, and the practical exercises helped work through real-life questions that may come up. I'm really looking forward to the other two Training Connection courses I'm signed up for.&quot;</p>
                            <span><strong>Bridgette Garthwaite | The John Buck Company</strong></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Eva was an amazing instructor! And since I was the only student enrolled in the advanced section of the class, she was willing to focus to spend extensive time making sure I had what I needed for my specific context. I can't say enough good about this course.&quot;</p>
                            <span><strong>Sarah Glover</strong></span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Chana was an incredible teacher. She knows the InDesign program (as well as the sister programs) so well that you can ask her a question on any aspect of Adobe and she'll be able to answer it... in 5 different ways! Loved how she tailored the class to our questions and pace. I am happy that she'll be teaching the Illustrator class that I signed up for!&quot;</p>
                            <span><strong>Katherine Cresto</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=7" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>