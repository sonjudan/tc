<div class="section section-some-stuff">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Some cool stuff you will create on our classes</h2>
        </div>
        <div class="section-body" data-aos="fade-up" data-aos-delay="150">
            <div class="owl-carousel owl-theme  owl-courses">
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D1.jpg" alt="InDesign Class Project 1">
                </div>
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D2C.jpg" alt="InDesign Class Project 2">
                </div>
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D3.jpg" alt="InDesign Class Project 3">
                </div>
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D3B.jpg" alt="InDesign Class Project 4">
                </div>
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D3C.jpg" alt="InDesign Class Project 5">
                </div>
                <div class="item">
                    <img src="/dist/images/courses/id/ID_Sample_AD_D4.jpg" alt="InDesign Class Project 6">
                </div>
            </div>
        </div>
    </div>
</div>