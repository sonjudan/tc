<div class="modal fade modal-instrutor-profile-kimberly" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-kimberly.jpg')"></span>
                        <h4>Kimberly</h4>
                    </div>
                    <div class="profile-bio copy mb-5 pb-5">
                        <h3>About Kimberly</h3>
                        <p>Kimberly is a results-driven business development coach and trainer. She specializes in group and individual instruction for public speaking, conflict resolution, interpersonal and written communication, customer service, leadership, and staff development.</p>


                        <ul>
                            <li>Certified Life Coach - Coach Training Alliance</li>
                            <li>Lean Six Sigma Green Belt Certification</li>
                            <li>Customer Service Certification - NRF Foundation</li>
                            <li>Bachelor Of General Studies - Interpersonal Communication And Psychology</li>

                        </ul>

                        <p>“The more I am able to bring customer service off the pages and into life simulations the more effective the training.”</p>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>