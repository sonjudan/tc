<div class="modal fade modal-instrutor-profile-beverly" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-beverly.jpg')"></span>
                        <h4>Beverly</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Beverly</h3>
                        <p>Beverly has been an Adobe Certified Instructor for the last 17 years. She started her career working for Adobe Systems as a Business Development manager where she delivered Adobe training and presentations at Adobe conferences and trade shows.</p>
                        <p>For the last 10 years, she as worked as an independent Adobe trainer and a freelance designer focusing on photo-retouching, graphic & logo design, info-graphics creation, publication design & production work, and Acrobat Forms & PDF workflow.</p>

                        <ul>
                            <li>MBA, Loyola Marymount University</li>
                            <li>BA, University of California, Los Angeles</li>
                            <li>Adobe Certified Expert / Instructor</li>
                        </ul>

                        <p>Beverly consistently gets great reviews for her classes. She is very approachable and makes trainees of all abilities feel completely at ease.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Beverly is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>