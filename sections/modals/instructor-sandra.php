<div class="modal fade modal-instrutor-profile-sandra" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-sandra.jpg')"></span>
                        <h4>Sandra</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Sandra</h3>
                        <p>Sandra is a highly energetic, enthusiastic, and engaging facilitator with a high impact and high interactive      training style. She has successfully trained, coached, and/or mentored 10,000+ adult learners.</p>
                        <p>Her strengths are in facilitating leadership, management / non-management, customer service, employee motivation, communication, coaching, and team-building.</p>
                        <ul>
                            <li>Bachelor of Business Studies</li>
                            <li>Master of Arts Organizational Management</li>
                            <li>Business Conflict Resolution Mediator Certification</li>
                            <li>Skillpath / National Training Inc Facilitator/Instructor Certification</li>
                        </ul>
                        <p>“My passion, loyalty, and devotion are in teaching, coaching and mentoring others. The success of my role as a facilitator is predicated on creating an environment of honesty. I keep it REAL, RAW, and RESPECTFUL and invite participants to do the same!”</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was an amazing teacher! He was incredibly patient and informative. He was able to explain things in such an easy way to follow. He clearly is an expert on the topic and was a phenomenal teacher!</p>
                            <span>Elizabeth Militello</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was a fabulous instructor. His knowledge is endless and he was super helpful and patient for all skill levels! I would definitely like to take more classes.</p>
                            <span>Jacob Dudley</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>