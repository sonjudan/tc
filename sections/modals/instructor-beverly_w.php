<div class="modal fade modal-instrutor-profile-beverly_w" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-beverly_washington.jpg')"></span>
                        <h4>Beverly</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Beverly</h3>
                        <p>Beverly is a change infuser. Participants who attend her trainings have stated that                          she is transformative. It’s her creative way of framing content into a simple, easy                          to understand format that is then easy to process and  apply.</p>
                        <p> Beverly is an enthusiastic, believable facilitator who has inspired individuals                          around the world. Those places include the United Kingdom, South Africa,                          Australia, New Zealand and of course the United States.</p>
                        <p> The topics which fall within her skillset are leadership, team-building, emotional                          intelligence, customer service, conflict management and conflict resolution just to 
                        name a few.</p>
                        <p>Beverly is a graduate of the University of Illinois (Urbana-Champaign), Fine and                          Applied Arts; an author of the book titled, Don’t Call Me “Bev”! Things That
                          Work My Last Nerve; Who’s Who of Professional Executive Women; Certified
                          MFBE; Screen Actors Guild.</p>
                        <p>Beverly is the professional facilitator who will make the positive difference during                        any and all training sessions.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Beverly is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>