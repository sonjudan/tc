<div class="modal fade modal-instrutor-profile-chana" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-chana.jpg')"></span>
                        <h4>Chana</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Chana</h3>
                        <p>Fine Artist, Graphic Designer, Adobe Certified Instructor & Software Evangelist.</p>
                        <p>Chana has taught Fine Art and Graphic Design at university level for 25+ years. She has taught as a lead instructor at UCLA - Departments of Visual Arts and the Walt Disney Company.</p>
                        <p>She is an Adobe Certified Expert, and provides specialized training in Adobe Photoshop, Lightroom, XD, Illustrator and InDesign.</p>
                        <p>Chana is a regular presenter at Mac World, AIGA and Adobe Max conferences.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chana was able to cover a lot of aspect of Photoshop that I was unaware. Excellent instructor and I was able to see her passion for using the software!</p>
                            <span>Oscar Carrera  |  Canutillo ISD</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chana was very professional. She knew her material beyond professionalism and she is extremely passionate about her work! I thoroughly enjoyed the class!</p>
                            <span>Sarah Ehrlich</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>The Training Connection staff and instructors were superb. I learned so much in three days and am very confident with practice I will master Photoshop. The facility was very clean as well.</p>
                            <span>Mayra Newbon  |  Westwood Presbyterian Church</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>