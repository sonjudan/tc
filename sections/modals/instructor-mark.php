<div class="modal fade modal-instrutor-profile-mark" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-mark.jpg')"></span>
                        <h4>Mark</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Mark</h3>
                        <p>Since 1992, Mark has designed, developed, and managed custom eLearning and interactive media programs for a variety of clients.  His project roles have varied from group director to project manager, and lead instructional designer to lead programmer.  He is intimately familiar with proven eLearning methodologies.  He has been training Adobe Captivate since 2001.</p>

                        <p>Marks qualifications include:</p>

                        <ul>
                            <li>Bachelor of Science in Industrial Technology | Illinois State University</li>
                            <li>Master of Science in Training | Illinois State University</li>
                            <li>Adobe Certified Expert in Captivate</li>
                            <li>Adobe Certified Instructor</li>
                        </ul>

                        <p>Mark has instructed graduate-level courses on designing and developing eLearning projects, and enjoys speaking at a variety of training and eLearning conferences in the U.S. and Europe.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Mark is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Mark was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Mark was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>