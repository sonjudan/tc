<div class="modal fade modal-instrutor-profile-mckinley" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-mckinley.jpg')"></span>
                        <h4>McKinley</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About McKinley</h3>
                        <p>McKinley has as more than 2 decades of experience working as media arts designer and consultant specializing in sound, video, and interactive multimedia design. Her diverse client list includes Warner Bros, Disney, E! Entertainment Television, the Los Angeles Times; Rhino Records, Universal Music Group, Capitol Records (EMI), the University of California, Nike, Microsoft, NEC,  Seiko of America, the J. Paul Getty Museum, and more.</p>
                        <p>She has been training After Effects, Final Cut Pro, Logic Pro, Premiere Pro for Training Connection for the last 12 years.</p>
                        <p>Her accomplishments include:</p>

                        <ul>
                            <li>Telly Award for her work in video production</li>
                            <li>Ciné Golden Eagle for her work in video production</li>
                            <li>Visual Arts Instructor of the Year award from UCLA-Extension.</li>
                        </ul>

                        <p>McKinley is a phenomenal instructor, our students rave about her classes.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! McKinley is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>McKinley was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>McKinley was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>