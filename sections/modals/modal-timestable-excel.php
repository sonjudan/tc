<!--CHICAGO-->
<div class="modal fade modal-timeable-excel-chicago-1" id="modalTimeable" tabindex="-1" role="dialog" aria-labelledby="modalTimeable" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Chicago Class Timetable </h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable js-modal-data">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 10</div>

                            <a href="#"
                               class="btn btn-secondary"
                               data-target="#book-class-timetable-chicago"
                               data-toggle="modal"
                               data-dismiss="modal"
                               data-course="modal"
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 7</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 11</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 16</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 17</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 18</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>


                    <p class="footer-note"><span class="text-label ">Training Address</span>230 W Monroe Street Suite 610 Chicago IL 60606</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-chicago-2" id="modalTimeable2" tabindex="-1" role="dialog" aria-labelledby="modalTimeable2" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Chicago Class Timetable </h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 11</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 8</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 12</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 17</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 20</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>230 W Monroe Street Suite 610 Chicago IL 60606</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-chicago-3" id="modalTimeable3" tabindex="-1" role="dialog" aria-labelledby="modalTimeable3" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Chicago Class Timetable </h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 12</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 9</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 13</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 18</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>230 W Monroe Street Suite 610 Chicago IL 60606</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-chicago-4" id="modalTimeable4" tabindex="-1" role="dialog" aria-labelledby="modalTimeable4" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Chicago Class Timetable </h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 13</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 10</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 14</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 19</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Dec 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-chicago" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>230 W Monroe Street Suite 610 Chicago IL 60606</p>
                </div>

            </div>
        </div>
    </div>
</div>

<!--LOS ANGELES-->

<div class="modal fade modal-timeable-excel-la-1" id="modalTimeable" tabindex="-1" role="dialog" aria-labelledby="modalTimeable" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Los Angeles Class Timetable</h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 9</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 1</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 21</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 12</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 1</span>Introduction</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-la-2" id="modalTimeable2" tabindex="-1" role="dialog" aria-labelledby="modalTimeable2" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Los Angeles Class Timetable</h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 10</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 2</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 13</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 2</span>Intermediate</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-la-3" id="modalTimeable3" tabindex="-1" role="dialog" aria-labelledby="modalTimeable3" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Los Angeles Class Timetable</h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 11</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 3</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 23</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 14</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 3</span>Advanced</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 22</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>

                    <p class="footer-note"><span class="text-label ">Training Address</span>915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-timeable-excel-la-4" id="modalTimeable4" tabindex="-1" role="dialog" aria-labelledby="modalTimeable4" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-class-timetable">

                    <div class="heading-l3">
                        <h2>Los Angeles Class Timetable</h2>
                        <ul class="class-labels">
                            <li class="class-label green"><span>G</span> Guaranteed to run</li>
                            <li class="class-label orange"><span>L</span> Limited Seats</li>
                            <li class="class-label red"><span>F</span> Full</li>
                        </ul>
                        <p>All classes are 9.00am to 4.30pm</p>
                    </div>

                    <div class="mt-4"></div>

                    <div class="owl-carousel owl-theme owl-timetable">
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Sep 18</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Oct 4</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 15</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                        <div class="item card card-book-course">
                            <h3><span><span class="lbl-l5">Excel Level 4</span>Macros & VBA</span> <span>1 Day</span></h3>
                            <div class="class-label class-label-block green"><span>G</span> Nov 9</div>

                            <a href="#" class="btn btn-secondary" data-target="#book-class-timetable-la" data-toggle="modal" data-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3 4 0 8-1t7-2l481-259q17-9 16-29 0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207-411 221-406-221zm484 412l-95-48-68 37 80 41-411 221-406-221 85-43-68-37-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4 4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44-68 37 75 37-411 221-406-221 79-36-68-37-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5 5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                                Book Course
                            </a>
                        </div>
                    </div>
                    <p class="footer-note"><span class="text-label ">Training Address</span>915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017</p>
                </div>

            </div>
        </div>
    </div>
</div>
