<div class="modal modal-default fade modal-cart" id="modal-cart" tabindex="-1" role="dialog" aria-labelledby="booking-add-package" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-cart-list">
                    <h4>Your Cart</h4>

                    <div class="tbl tbl-checkout mb-1 js-remove-parent">
                        <div class="tbl-col ">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Time Management Class</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019 <br>9:00am 4:30pm</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>

                            <button class="c-close c-close-inline md js-remove l2"><i class="fas fa-times"></i></button>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>
                    </div>
                    <div class="tbl tbl-checkout mb-1 js-remove-parent">



                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Adobe Graphic Design Package</h5>
                            </div>
                            <button class="c-close c-close-inline md js-remove l2"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Photoshop Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell tbl-cell-location"><p>Chicago</p></div>
                            <div class="tbl-cell tbl-cell-date"><p>Dec 5-7, 2019</p></div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Illustrator Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell tbl-cell-location"><p>Los Angeles</p></div>
                            <div class="tbl-cell tbl-cell-date"><p>Dec 12-14, 2019</p></div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">InDesign Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell tbl-cell-location"><p>Chicago</p></div>
                            <div class="tbl-cell tbl-cell-date"><p>Dec 19-21, 2019</p></div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">($1,195.00)</h5>
                            </div>
                        </div>
                    </div>


                    <div class="tbl-col tbl-checkout">
                        <div class="tbl-cell">
                            <h5 class="title-l4">Booking Total</h5>
                        </div>
                        <div class="tbl-cell tbl-cell-last">
                            <span class="h-price">$2,390.00</span>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
