<div class="modal fade modal-instrutor-profile-kristian" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-kristian.jpg')"></span>
                        <h4>Kristian</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Kristian</h3>
                        <p>Kristian has worked as an Adobe Certified Instructor for the last 8 years. He specializes in both graphic design (Photoshop / Illustrator / InDesign) and motion graphics (After Effects, Avid, DaVinci Resolve, Cinema 4D and Premiere Pro)</p>
                        <p>His freelance clients include Disney, Universal, Sony Pictures, and MIT.</p>

                        <ul>
                            <li>BPPE/BPVE Certified</li>
                            <li>Avid Certified Pro</li>
                            <li>Adobe Certified Expert / Instructor</li>
                        </ul>

                        <p>Kristian is an energetic and dynamic trainer who brings a ton real-world experience into his classes. Our trainees love the passion he has for software and teaching.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was an amazing teacher! He was incredibly patient and informative. He was able to explain things in such an easy way to follow. He clearly is an expert on the topic and was a phenomenal teacher!</p>
                            <span>Elizabeth Militello</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was a fabulous instructor. His knowledge is endless and he was super helpful and patient for all skill levels! I would definitely like to take more classes.</p>
                            <span>Jacob Dudley</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>