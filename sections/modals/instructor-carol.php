<div class="modal fade modal-instrutor-profile-carol" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-carol.jpg')"></span>
                        <h4>Carol</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Carol</h3>
                        <p>Carol has 25 years of experience providing outstanding skills training programs for thousands of managers and employees. Her main areas of expertise are Communication, Business Writing, Customer Service, Presentations, Project Management, Time Management and Sales. Before moving into the corporate training field Carol worked for several large companies in various leadership and project management roles. </p>

                       <ul>
                           <li>Bachelor of Business Administration | University of Santa Barbara</li>
                           <li>MOS Certified</li>
                           <li>Certified PMP</li>
                       </ul>

                        <p>Carol’s has an engaging personality and her classes are interactive and fun. She has consistently over the last 10 years received our highest intructor rating.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Carol was WONDERFUL! She provided great information in a concise and meaningful way and was genuinely interested in the topic. I learned SO MUCH more than I anticipated and I know that the skill set I need to be successful in my career.</p>
                            <span>Denise Pichon | LA County DCFS</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>GREAT COURSE! The teacher was amazing and the examples were very relatable and understandable! I am very appreciative of this                                 to anyone! Thank you so much for this opportunity!</p>
                            <span>Kendrick Jones | Charles R. Drew</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>