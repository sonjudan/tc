<!--BOOK CLASES LEVEL-->
<div class="modal modal-default fade" id="book-class-level-1" tabindex="-1" role="dialog" aria-labelledby="book-class-level-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Level 1 - Introduction</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 1 - Introduction</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="book-class-level-2" tabindex="-1" role="dialog" aria-labelledby="book-class-level-2" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Level 2 - Intermediate</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-2"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-2" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 2 - Intermediate</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="book-class-level-3" tabindex="-1" role="dialog" aria-labelledby="book-class-level-3" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Level 3 - Advanced</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-3"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-3" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 3 - Advanced</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="book-class-level-4" tabindex="-1" role="dialog" aria-labelledby="book-class-level-4" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Level 4 - Macros & VBA</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$450.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-3"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-3" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 4 - Macros & VBA</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$450.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$450.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--BOOK PACKAGE DEALS-->

<div class="modal modal-default fade modal-book-package-deal" id="book-package-deal-1" tabindex="-1" role="dialog" aria-labelledby="book-package-deal-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel 2-Level Package</h5>
                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <label>Course</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Class</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select">
                                                <option value=""></option>
                                                <option value="Excel Level 1">Excel Level 1 - Introduction</option>
                                                <option value="Excel Level 2">Excel Level 2 - Intermediate</option>
                                                <option value="Excel Level 3">Excel Level 3 - Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select">
                                                <option value=""></option>
                                                <option value="Excel Level 1">Excel Level 1 - Introduction</option>
                                                <option value="Excel Level 2">Excel Level 2 - Intermediate</option>
                                                <option value="Excel Level 3">Excel Level 3 - Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-lg-right">

                                <div class="l2-inline">
                                    <span class="h-price mr-2">$600.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Excel 2-Level Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 1 – Introduction</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 2 – Intermediate</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">-$100.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$600.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade modal-book-package-deal" id="book-package-deal-2" tabindex="-1" role="dialog" aria-labelledby="book-package-deal-2" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Levels 3 and 4 Package</h5>
                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <label>Course</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Class</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 3">Excel Level 3 - Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 4">Excel Level 4 - Macros & VBA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-lg-right">

                                <div class="l2-inline">
                                    <span class="h-price mr-2">$700.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-2"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-2" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Excel Levels 3 and 4 Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 3 – Advanced</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 4 – Macros & VBA</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$450.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">-$100.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$700.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade modal-book-package-deal" id="book-package-deal-3" tabindex="-1" role="dialog" aria-labelledby="book-package-deal-3" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel 3-Level Package</h5>
                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <label>Course</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Class</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 1">Excel Level 1 - Introduction</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 2">Excel Level 2 - Intermediate</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 2">Excel Level 3 - Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-lg-right">

                                <div class="l2-inline">
                                    <span class="h-price mr-2">$900.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-3"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-3" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Excel Levels 3 and 4 Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 1 – Introduction</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 2 – Intermediate</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 3 – Advanced</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">-$150.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$900.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade modal-book-package-deal" id="book-package-deal-4" tabindex="-1" role="dialog" aria-labelledby="book-package-deal-4" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Excel Levels 2, 3, and 4 Package</h5>
                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <label>Course</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Class</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 2">Excel Level 2 - Intermediate</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 2">Excel Level 3 - Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control mb-2 js-course-select" disabled>
                                                <option value="Excel Level 2">Excel Level 4 - Macros & VBA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-lg-right">

                                <div class="l2-inline">
                                    <span class="h-price mr-2">$1,000.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-4"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-4" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Excel Levels 3 and 4 Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 2 – Intermediate</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 3 – Advanced</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Level 4 – Macros & VBA</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$340.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">-$150.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,000.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--BOOK Class Date -->

<div class="modal modal-default fade" id="book-class-timetable-la" tabindex="-1" role="dialog" aria-labelledby="book-class-level-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">

                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <h5 class="title-l4">Excel Course title</h5>
                                <h6>
                                    <span>Los Angeles</span> <br>
                                    <span>Class date: Dec 5, 2019 - 9.00am to 4.30pm</span>
                                </h6>

                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 text-right">
                                <span class="h-price mr-3">$350.00</span>
                            </div>
                        </div>
                    </div>


                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Excel Course title</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019 <br>9.00am to 4.30pm</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>