<div class="modal fade modal-instrutor-profile-laurie" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-laurie.jpg')"></span>
                        <h4>Laurie</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Laurie</h3>
                        <p>Laurie has been teaching both InDesign, Illustrator, and Photoshop for the last 12 years. She creates brochures, catalogs, web sites, logos, t-shirt graphics and photo collages for a variety of clients. From her diverse graphic design freelance work, Laurie brings a wealth of real-world knowledge into the classroom. She truly enjoys teaching both the basics to the emerging designer as well as tips and tricks to the advanced power users. She has been a speaker at numerous events promoting InDesign and Illustrator at Creative Pro Week 2019 & 2018, the HOW & Vector Conferences, the InDesign Conference, Creative Suite Conference, and Chicago Adobe User Groups.</p>

                        <ul>
                            <li>Bachelor of Arts Degree in Communications & Advertising Valparaiso University</li>
                            <li>Adobe Certified Expert / Instructor</li>
                        </ul>

                        <p>Laurie truly enjoys teaching both the basics to the emerging designer as well as tips and tricks to the advanced power users.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Laurie is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Laurie was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Laurie was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>