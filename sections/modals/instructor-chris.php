<div class="modal fade modal-instrutor-profile-chris" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-chris.jpg')"></span>
                        <h4>Chris</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Chris</h3>
                        <p>Chris has been our senior Web development trainer for the last 12 years - he specializes in teaching HTML, CSS, JavaScript, JQuery, PHP, and Dreamweaver courses. He is a genius when it comes to technology.</p>
                        <p>He has over 30 years of IT experience spanning application development, network support and engineering to project management. His focus in the last 18 years has been in web-based application development, where he works mainly with entrepreneurs and businesses to develop and deli\er their products to the marketplace. The information he presents in his classes is based on the techniques and best practices used in typical production environments.</p>
                        <p>As our Web development classes are typically small in size, so Chris often allows trainees to develop their own apps in class. Not only do they then learn new programming skills but they have a big head start when they get back to the office. Of course, trainees can also follow our standard curriculum too.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Chris is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chris was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chris was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>