<div class="modal modal-default fade" id="book-time-management" tabindex="-1" role="dialog" aria-labelledby="book-class-level-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Time Management Class - 1 DAY 9:00am 4:30pm</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select">
                                            <option value=""></option>
                                            <option value="Chicago">Chicago</option>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Time Management Class</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Chicago</div>
                            <div class="tbl-cell">Dec 5, 2019 <br>9:00am 4:30pm</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="book-time-management-chicago" tabindex="-1" role="dialog" aria-labelledby="book-class-level-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Time Management Class - 1 DAY 9:00am 4:30pm</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Chicago">Chicago</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Time Management Class</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Chicago</div>
                            <div class="tbl-cell">Dec 5, 2019 <br>9:00am 4:30pm</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-default fade" id="book-time-management-la" tabindex="-1" role="dialog" aria-labelledby="book-class-level-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content bg-white">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <h4 class="modal-title">Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-booking-fields">
                    <h5 class="title-l4">Time Management Class - 1 DAY 9:00am 4:30pm</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Location</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Class</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Name</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Student Email</label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-location-select" disabled>
                                            <option value="Los Angeles">Los Angeles</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                            <option value="Dec 5, 2019">Dec 5, 2019</option>
                                            <option value="Oct 9, 2019">Oct 9, 2019</option>
                                            <option value="Jan 16, 2019">Jan 16, 2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 text-right">
                                <div class="mt-4 pt-2 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a href="#" class="btn btn-green mw-13" data-toggle="collapse" data-target="#toggleCartContent-1"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div id="toggleCartContent-1" class="collapse section-booking mt-4 mt-lg-5">
                    <h4>Your Cart</h4>
                    <div class="tbl tbl-checkout mb-2">
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Time Management Class</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a></p>
                            </div>
                            <div class="tbl-cell">Los Angeles</div>
                            <div class="tbl-cell">Dec 5, 2019 <br>9:00am 4:30pm</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                                <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>


                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5 class="title-l4">$0.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5 class="title-l4">Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#" data-dismiss="modal" aria-label="Close">Continue browsing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
