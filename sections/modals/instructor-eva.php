<div class="modal fade modal-instrutor-profile-eva" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-eva.jpg')"></span>
                        <h4>Eva</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Eva</h3>
                        <p>Eva brings an incredible amount of real-world experience into her classes, from her days working in advertising, as a Creative Designer and as an Illustrator.</p>
                        <p>Eva has worked with Adobe software programs (Photoshop, Illustrator, InDesign, and Acrobat) since their inception. She holds the following qualifications:</p>

                        <ul>
                            <li>BFA, Carnegie-Mellon University Illustration</li>
                            <li>MFA, University of Wisconsin-Madison Painting & Printmaking</li>
                            <li>Adobe Certified Expert / Instructor (15 years)</li>
                            <li>CompTIA CTT+ Recognizes excellence in instruction</li>
                        </ul>

                        <p>Regardless of whether you're a complete beginner or you have existing experiencing using Photoshop, Eva will be able to teach a whole bunch of new tricks.<br>
                          Our students love her classes!
                        </p>
                    </div>
                </div>



                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>The course was great! Eva really did a great job mixing discussion and hands experience together. I was the only student in the class so it was awesome to have all the attention on all my weaknesses. I've always thought any classes I've taken at Training Connection were some of the best ever! Training Connection Rocks!</p>
                            <span>Barbara Domagala   |   ENA</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Excellent course - since I had no background in Photoshop, I feel more comfortable in knowing what tools are presented and how they can be utilized. Eva did a very good job in presenting and displayed patience throughout. Her background knowledge in art, color, lighting, etc...hugely helpful! Her real world experience provided credibility. Highly recommended!</p>
                            <span>Fred Phillips   |   LMT Onsrud</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Eva is the BEST instructor! She is a very effective instructor who is able to articulate lessons well. I rave about her and the company in general. I think this is quality training with quality instructors in a great environment. Great equipment, small class sizes work well, and the good coffee is a plus! This is my second class and definitely not my last. Highly recommended, and I've already had two of my colleagues sing up for classes. Thanks Training Connection!</p>
                            <span>Michelle Truong   |   The Options Clearing Corp.</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>