<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Illustrator training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_8">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                </div>

                <h3><span>Illustrator classes rating:</span> 4.8 stars from 391 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This was my third class with Eva at the Training Connection. As with the previous two courses, she proved to be extremely knowledgeable, patient and helpful. The materials provided with the course are very helpful, too. I am also impressed with the Chicago Training Connection facilities and receptionist staff. Everything/one was very professional and kind. Thank you! I will recommend TC to others. It was well worth it!&quot;</p>
                            <span><strong>Heather L. Jencks | Strategic Ascent, Inc.</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Eva was an excellent instructor and I would definitely attend another class she was teaching. She was easy to understand, easy to follow, and she kept a great pace ... but had no issues slowing down and allowing me to catch up when needed! Thank you very much for the awesome learning experience!”&quot;</p>
                            <span><strong>Janelle LaFlamme | Apollo Mechanical Contractors</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This is my second class with Eva, and I must say that her knowledge of the software was once again impressive. She has an extensive understanding of every tool and function that the book lessons touched base on. I would take a third class with Eva in a heartbeat! She deserves a birthday bonus!&quot;</p>
                            <span><strong>Charlie Baldwin | American Academy Of Orthopaedic Surgeons</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Eva was extremely helpful in all the discussions I had and assisted in questions related to job specific projects. The class was beneficial in that I'll be able to apply my new knowledge in my day to day marketing projects. I really enjoyed the class and thankful for the experience.&quot;</p>
                            <span><strong>Jennifer Nally</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This is my second class with Eva, and once again she was terrific. Her teaching method is engaging and she takes the time to explain each step in a very clear and concise manner. I never thought I would conquer Illustrator, but thanks to her, I have.&quot;</p>
                            <span><strong>Tara Roman</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=7" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>