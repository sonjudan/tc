<div class="section section-package-deals">
    <div class="container">
        <div class="section-heading" data-aos="fade-up">
            <h2>Package deals</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">
            <div class="card-deck card-row-sm card-deck-training">

                <div class="card card-package card-package-access">
                    <h3 class="card-title sm"><span>Access Training Package - 2 Levels</span></h3>
                    <div class="card-body">
                        <div class="card-text">
                            <h3 class="card-price">$1145</h3>
                        </div>

                        <p>Book both our Access classes and save $200.</p>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-secondary" data-target="#book-package-deal-1" data-toggle="modal">
                            <i class="fas fa-cart-plus mr-2"></i>
                            Book Package
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>