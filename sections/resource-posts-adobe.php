<article class="post-default g-text-ae">
    <div class="post-img">
        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Ordering Effects in After Effects">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/after-effects/lessons/effects-order.php">Ordering Effects in After Effects</a>
        </h3>
        <p>The order of your effects in the Effects Control panel will determine how things look in After Effects. Here I've applied a Drop Shadow effect to my spaceman and brought the distance away in the effects control panel.
            <a class="post-link" href="/after-effects/lessons/effects-order.php">Read more</a></p>

        <a href="/resources/after-effects.php" class="post-link">More After Effects Learning Resources</a>
    </div>
</article>
<article class="post-default g-text-Id">
    <div class="post-img">
        <img src="/dist/images/icons/icon-adobe-Id.png" alt="Using Conditional Text with Images in InDesign">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/indesign/lessons/conditional-text-images.php">Using Conditional Text with Images in InDesign</a>
        </h3>
        <p>Conditional Text in InDesign is similar to Layer Comps in Photoshop. But in InDesign it is	limited to text only. You can use conditional text with images by changing them to Inline Graphics.
            <a class="post-link" href="/indesign/lessons/conditional-text-images.php">Read more</a></p>

        <a href="/resources/indesign.php" class="post-link">More InDesign Learning Resources</a>
    </div>
</article>
<article class="post-default g-text-Ai">
    <div class="post-img">
        <img src="/dist/images/icons/icon-adobe-Ai.png" alt="Drawing smooth lines with Adobe Illustrator">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/illustrator/lessons/smooth-curves.php">Drawing smooth lines with Adobe Illustrator</a>
        </h3>
        <p>How many times have you looked at the curves of a logo or professional typography and wondered - how they achieved those impossibly smooth curves. If you are a natural born artist and you can sketch or paint - your already ahead of the game.
            <a class="post-link" href="/illustrator/lessons/smooth-curves.php">Read more</a></p>

        <a href="/resources/illustrator.php" class="post-link">More Illustrator Learning Resources</a>
    </div>
</article>
<article class="post-default g-text-Ps">
    <div class="post-img">
        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Useful Shortcuts in Photoshop">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/photoshop/lessons/essential-shortcuts.php">Useful Shortcuts in Photoshop</a>
        </h3>
        <p>Multitasking is king in Photoshop. There will be many times where you will need to perform mutliple commands simultaneously. You might be Zooming In, while changing your Brush Size to Clone part of your image. Knowing how to use shortcuts allows you to do this.
            <a class="post-link" href="/photoshop/lessons/essential-shortcuts.php">Read more</a></p>

        <a href="/resources/photoshop.php" class="post-link">More Photoshop Learning Resources</a>
    </div>
</article>
<article class="post-default g-text-Pr">
    <div class="post-img">
        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Creating a Spot Color effect in Premiere Pro">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/premiere-pro/lessons/spot-color-effect.php">Creating a Spot Color effect in Premiere Pro</a>
        </h3>
        <p>The spot color effect is when everything on the screen is in black and white except for a certain color. This effect can be done with both stills and video.
            <a class="post-link" href="/premiere-pro/lessons/spot-color-effect.php">Read more</a></p>

        <a href="/resources/premiere-pro.php" class="post-link">More Premiere Learning Resources</a>
    </div>
</article>