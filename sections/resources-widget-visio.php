<div class="section bg-light g-text-visio" data-aos="fade-up">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="section-title">Resources</h2>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-visio-tut.jpg" alt="Microsoft Visio Tutorials">
                    </span>
                    <p>In this section you will find step-by-step how to tutorials on Microsoft Visio.</p>
                    <a href="/resources/visio.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/bg-resources-cheatseet.jpg" alt="Cheatseet & Shortcuts">
                    </span>
                    <p>Useful Visio cheatseets, and shortcuts PDFs you can download free of charge.</p>
                    <a href="/visio-cheatsheet.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>