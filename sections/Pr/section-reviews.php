<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Premiere Pro training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_8">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Classes Rating"></span>
                </div>

                <h3><span>Premiere Pro  classes rating:</span> 4.8 stars from 368 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Kristian was incredibly thorough with his lectures and took apart every aspect of the software. It was incredibly helpful to know the ins and outs of Premiere - I feel a lot more comfortable tackling future projects.&quot;</p>
                            <span><strong>Beba Rodriguez</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Rob is fantastic! My knowledge of Premiere was very basic. I'm leaving this course with an enhanced understanding of keyboard shortcuts, editing tricks and how to correct bad footage. I hope to be back next year to take his Adobe After Effects course. Thank you so much!!&quot;</p>
                            <span><strong>Candice Lee | Nationwide Insurance</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;McKinley is an amazing instructor. It's my 2nd time in her class and she rocks it. I have to say she is one of my favorites. She is very helpful and knowledgeable. &quot;</p>
                            <span><strong>Jacquelyn Soria | County Of Los Angeles</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;McKinley was great she has a very good style well suited to instruction. Her real world experience shines through. Great class! I really enjoyed it and learned a lot&quot;</p>
                            <span><strong>Robert Hyman | U.S. Air Force Reserves</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Rob was very patient with all of us. If any of us had issues or questions, he stopped to help. His knowledge of the software was amazing.&quot;</p>
                            <span><strong>Brian Walker | Auxsable Liquid Products</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=61" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>