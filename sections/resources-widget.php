<div class="container">
    <div class="section-heading align-left">
        <h2>More Resources</h2>
    </div>

    <div class="widget-row">
        <div class="widget">
            <h4 class="widget-title">Adobe</h4>
            <ul>
                <li><a href="/resources/after-effects.php">After Effects Resources</a></li>
                <li><a href="/resources/indesign.php">InDesign Resources</a></li>
                <li><a href="/resources/illustrator.php">Illustrator Resources</a></li>
                <li><a href="/resources/photoshop.php">Photoshop Resources</a></li>
                <li><a href="/resources/premiere-pro.php">Premiere Pro Resources</a></li>
            </ul>
        </div>
        <div class="widget">
            <h4 class="widget-title">Microsoft Office</h4>
            <ul>
                <li><a href="/resources/access.php">Access Resources</a></li>
                <li><a href="/resources/excel.php">Excel Resources</a></li>
                <li><a href="/resources/powerpoint.php">Powerpoint Resources</a></li>
                <li><a href="/resources/project.php">Project Resources</a></li>
                <li><a href="/resources/outlook.php">Outlook Resources</a></li>
                <li><a href="/resources/visio.php">Visio Resources</a></li>
                <li><a href="/resources/word.php">Word Resources</a></li>
            </ul>
        </div>
        <div class="widget">
            <h4 class="widget-title">Business Skills</h4>
            <ul>

                <li><a href="/resources/business-communication.php">Business Communication</a></li>
                <li><a href="/resources/business-etiquette.php">Business Etiquette Resources</a></li>
                <li><a href="/resources/business-writing.php">Business Writing Resources</a></li>
                <li><a href="/resources/customer-service.php">Customer Service Resources</a></li>
                <li><a href="/resources/leadership.php">Leadership Resources</a></li>
                <li><a href="/resources/presentations.php">Presentation Resources</a></li>
                <li><a href="/resources/time-management.php">Time Management Resources</a></li>
            </ul>
        </div>
    </div>
</div>