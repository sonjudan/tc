<div class="section bg-light g-text-outlook" data-aos="fade-up">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="section-title">Resources</h2>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-outlook-tut.jpg" alt="Microsoft Outlook Tutorials">
                    </span>
                    <p>In this section you will find step-by-step how to tutorials on Microsoft Outlook.</p>
                    <a href="/resources/outlook.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/bg-resources-cheatseet.jpg" alt="Cheatseet & Shortcuts">
                    </span>
                    <p>Useful Outlook cheatseets, and shortcuts PDFs you can download free of charge.</p>
                    <a href="/outlook-cheatsheet.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-outlook-specialist.jpg" alt="Specialist">

                    </span>

                    <?php if(strpos($_SERVER['REQUEST_URI'], 'chicago') !== false):?>
                        <p>The complete guide on Microsoft Outlook certification in Chicago.</p>
                    <?php elseif(strpos($_SERVER['REQUEST_URI'], 'los-angeles') !== false):?>
                        <p>The complete guide on Microsoft Outlook certification in Los Angeles.</p>
                    <?php else: ?>
                        <p>The complete guide on Microsoft Outlook certification in Chicago and Los Angeles.</p>
                    <?php endif; ?>


                    <a href="/outlook-certification.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>