<div class="modal fade modal-instrutor-profile-eva" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-eva.jpg')"></span>
                        <h4>Eva</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Eva</h3>
                        <p>Eva brinds an incredible amout of Real World experience into her classes, from her days working in advertising, as a Creative Designer and as an Illustrator.</p>

                        <p>Eva has worked with Adobe software programs (Photoshop, Illustrator and InDesign) since their inception. She holds the following Qualifications:</p>

                        <ul>
                            <li>BFA, Carnegie-Mellon University Illustration</li>
                            <li>MFA, Universtiy of Wisconsin-Madison Painting & Printmaking</li>
                            <li>Adobe Certified Expert / Intructor (15 years)</li>
                            <li>CompTIA CTT+ Recognizes excellence in instruction</li>
                        </ul>

                        <p>Regardless of whether you're a complete beginner or you have existing experiencing using Photoshop, Eva will be able to teach a whole bunch of new tricks.</p>
                    </div>
                </div>



                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>The course was great! Eva really did a great job mixing discussion and hands experience together. I was the only student in the class so it was awesome to have all the attention on all my weaknesses. I've always thought any classes I've taken at Training Connection were some of the best ever! Training Connection Rocks!</p>
                            <span>Barbara Domagala   |   ENA</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Excellent course - since I had no background in Photoshop, I feel more comfortable in knowing what tools are presented and how they can be utilized. Eva did a very good job in presenting and displayed patience throughout. Her background knowledge in art, color, lighting, etc...hugely helpful! Her real world experience provided credibility. Highly recommended!</p>
                            <span>Fred Phillips   |   LMT Onsrud</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Eva is the BEST instructor! She is a very effective instructor who is able to articulate lessons well. I rave about her and the company in general. I think this is quality training with quality instructors in a great environment. Great equipment, small class sizes work well, and the good coffee is a plus! This is my second class and definitely not my last. Highly recommended, and I've already had two of my colleagues sing up for classes. Thanks Training Connection!</p>
                            <span>Michelle Truong   |   The Options Clearing Corp.</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>



<!--EVA-->
<div class="modal fade modal-instrutor-profile-beverly" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-beverly.jpg')"></span>
                        <h4>Beverly</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Beverly</h3>
                        <p>Beverly has been an Adobe Certified Instructor for the last 17 years. She started her career working for Adobe Systems as a Business Development manager where she delivered Adobe training and presentations at Adobe conferences and trade shows.</p>

                        <p>For the last 10 years, she as worked as an independent Adobe trainer and a freelance designer focusing on photo-retouching, graphic & logo design, info-graphics creation, publication design & production work, and  Acrobat Forms & PDF workflow.</p>

                        <ul>
                            <li>MBA, Loyola Marymount University</li>
                            <li>BA, University of California, Los Angeles</li>
                            <li>Adobe Certified Expert / Instructor</li>
                        </ul>

                        <p>Beverly consistently gets great reviews for her classes. She is very approachable and makes trainees of all abilities feel completely at ease.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>This course was AWESOME!!! Beverly is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
                            <span>Jennifer Karpinski  |   Summit High School</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was really good at explaining the concepts and the way to use all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it!
                                there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful.
                                Cant wait to come back and learn more of this an other programs.</p>
                            <span>Carlos Torres</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Beverly was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class</p>
                            <span>Sharon Ha</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Kristian-->
<div class="modal fade modal-instrutor-profile-kristian" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-kristian.jpg')"></span>
                        <h4>Kristian</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Kristian</h3>
                        <p>Kristian has worked as an Adobe Certified Instructor for the last 8 years. He specializes in both graphic design (Photoshop / Illustrator / InDesign) and motion graphics (After Effects, Avid, DaVinci Resolve, Cinema 4D and Premiere Pro)</p>
                        <p>His freelance clients include Disney, Universal, Sony Pictures, and MIT.</p>

                        <ul>
                            <li>BPPE/BPVE Certified</li>
                            <li>Avid Certified Pro</li>
                            <li>Adobe Certified Expert / Instructor</li>
                        </ul>

                        <p>Kristian is an energetic and dynamic trainer who brings a ton real world experience into his classes. Our trainees love him.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was an amazing teacher! He was incredibly patient and informative. He was able to explain things in such an easy way to follow. He clearly is an expert on the topic and was a phenomenal teacher!</p>
                            <span>Elizabeth Militello</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Kristian was a fabulous instructor. His knowledge is endless and he was super helpful and patient for all skill levels! I would definitely like to take more classes.</p>
                            <span>Jacob Dudley</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Chana-->
<div class="modal fade modal-instrutor-profile-chana" id="modalTimeableLA" tabindex="-1" role="dialog" aria-labelledby="modalTimeableLATitle" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="section-instructor-profile">
                    <div class="profile-side">
                        <span class="profile-img" style="background-image:url('/dist/images/instructors/profile-chana.jpg')"></span>
                        <h4>Chana</h4>
                    </div>
                    <div class="profile-bio copy">
                        <h3>About Chana</h3>
                        <p>Fine Artist, Graphic Designer, Adobe Certified Instructor & Software Evangelist.</p>
                        <p>Chana has taught Fine Art and Graphic Design at university level for 25+ years. She has taught as a lead instructor at UCLA - Departments of Visual Arts and the Walt Disney Company.</p>
                        <p>She is an Adobe Certified Expert, and provides specialized training in Adobe Photoshop, Lightroom, XD, Illustrator and InDesign.</p>
                        <p>Chana is a regular presenter at Mac World, AIGA and Adobe Max conferences.</p>
                    </div>
                </div>

                <div class="owl-carousel owl-theme owl-reviews">
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chana was able to cover a lot of aspect of Photoshop that I was unaware. Excellent instructor and I was able to see her passion for using the software!</p>
                            <span>Oscar Carrera  |  Canutillo ISD</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>Chana was very professional. She knew her material beyond professionalism and she is extremely passionate about her work! I thoroughly enjoyed the class!</p>
                            <span>Sarah Ehrlich</span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote">
                            <i class="icon-quote"></i>
                            <p>The Training Connection staff and instructors were superb. I learned so much in three days and am very confident with practice I will master Photoshop. The facility was very clean as well.</p>
                            <span>Mayra Newbon  |  Westwood Presbyterian Church</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-michelle.php'; ?>