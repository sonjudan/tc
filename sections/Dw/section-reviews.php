<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Dreamweaver training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_7">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Dreamweaver classes - 5 rating"></span>
                </div>

                <h3><span>Dreamweaver classes rating:</span> 4.7 stars from 429 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Ron was an excellent instructor! I had so much fun and learned a lot of info that I plan to use ASAP! Ron was very clear and did an awesome job at making sure we understood everything. He is VERY well informed of the course content and was excellent at explaining everything. I feel like I learned more in 2 days than I did in some semester long courses during undergrad. GO RON!&quot;</p>
                            <span><strong>Whitney Smith | In The Swim</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Gabe was the best instructor imaginable. We covered a great deal of training in 3 days yet Gabe never allowed anyone to fall to behind. He provided excellent technical training mixed with just the right amount of humor to keep things light. Would recommend Gabe and the Training Connection in a heartbeat.&quot;</p>
                            <span><strong>Robert Simanton | Cunningham Group</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I thoroughly enjoyed the Dreamweaver class. Gabriel was excellent. He was engaging and delivered the material in a clear manner. I feel like after this class I can create creative, user friendly, and productive websites. I will keep Training Connection, and especially Gabriel, in mind for any future training needs. Thank you. &quot;</p>
                            <span><strong>Michael Miller</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This class was great. It was just the right pace for me. I learned the skills I needed to improve my work (and certainly many more). Gabe is an excellent instructor. He is patient, makes the content understandable, and provided the class with real-world scenarios that we could learn from and make mistakes with.&quot;</p>
                            <span><strong>Amber P. Simpson | American Bar Association</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This class was great. It was just the right pace for me. I learned the skills I needed to improve my work (and certainly many more). Gabe is an excellent instructor. He is patient, makes the content understandable, and provided the class with real-world scenarios that we could learn from and make mistakes with.&quot;</p>
                            <span><strong>Jim Suthers | Delray Lighting Inc</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=4" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>