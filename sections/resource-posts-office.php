<article class="post-default g-text-access">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-access.png" alt="Working with Records">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/access/lessons/working-with-records.php">Working with Records</a>
        </h3>
        <p>To add records to a table, you have the option of entering them manually, using a form, or using the import tools on the External Data tab. As the database we are using does not have a form for it yet and we do not have external data we can import into it, we will add records manually.
            <a class="post-link" href="/access/lessons/working-with-records.php">Read more</a></p>

        <a href="/resources/access.php" class="post-link">More Microsoft Access Learning Resources</a>
    </div>
</article>

<article class="post-default g-text-excel">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Changes in MS Excel">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/excel/lessons/tracking-changes.php">Tracking Changes in MS Excel</a>
        </h3>
        <p>When tracking is enabled, Excel will keep track of the changes that you or others who have access to your workbook have made. This includes data insertions, deletions, and more.
            <a class="post-link" href="/excel/lessons/tracking-changes.php">Read more</a></p>

        <a href="/resources/excel.php" class="post-link">More Microsoft Excel Learning Resources</a>
    </div>
</article>



<article class="post-default g-text-powerpoint">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Your First PowerPoint Presentation">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/powerpoint/lessons/first-presentation.php">Your First PowerPoint Presentation</a>
        </h3>
        <p>In this article, you will be shown how to create your first presentation. Presentations consist of slides. Each slide can contain text, graphics, animations, and more. You’ll learn how to add slides, use the content placeholders, and add text to your presentation.
            <a class="post-link" href="/powerpoint/lessons/first-presentation.php">Read more</a></p>

        <a href="/resources/powerpoint.php" class="post-link">More Microsoft PowerPoint Learning Resources</a>
    </div>
</article>



<article class="post-default g-text-project">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-project.png" alt="Adding Tasks in Microsoft Project">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/project/lessons/adding-tasks.php">Adding Tasks in Microsoft Project</a>
        </h3>
        <p>In this article, we'll delve a little deeper into understanding tasks. Project 2010 introduces manually scheduled tasks. You can also schedule tasks automatically using the Project scheduling engine. We’ll discuss the key terms for understanding tasks in this module.
            <a class="post-link" href="/project/lessons/adding-tasks.php">Read more</a></p>

        <a href="/resources/project.php" class="post-link">More Microsoft Project Learning Resources</a>
    </div>
</article>



<article class="post-default g-text-outlook">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-outlook.png" alt="Managing Mail and Auto Archiving in Outlook">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/outlook/lessons/auto-archiving.php">Managing Mail and Auto Archiving in Outlook</a>
        </h3>
        <p>When your Outlook items become out of date, you can store them or delete them. You can archive them automatically after a specified time interval or archive them manually. When you archive your items, you store them in your My Documents folder on your hard drive.
            <a class="post-link" href="/outlook/lessons/auto-archiving.php">Read more</a></p>

        <a href="/resources/outlook.php" class="post-link">More Microsoft Outlook Learning Resources</a>
    </div>
</article>



<article class="post-default g-text-visio">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-visio.png" alt="The Visio Interface">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/visio/lessons/interface.php">The Visio Interface</a>
        </h3>
        <p>When your Outlook items become out of date, you can store them or delete them. You can archive them automatically after a specified time interval or archive them manually. When you archive your items, you store them in your My Documents folder on your hard drive.
            <a class="post-link" href="/visio/lessons/interface.php">Read more</a></p>

        <a href="/resources/visio.php" class="post-link">More Microsoft Visio Learning Resources</a>
    </div>
</article>



<article class="post-default g-text-word">
    <div class="post-img">
        <img src="/dist/images/icons/icon-office-word.png" alt="Creating Styles in Word">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/word/lessons/creating-styles.php">Creating Styles in Word</a>
        </h3>
        <p>When your Outlook items become out of date, you can store them or delete them. You can archive them automatically after a specified time interval or archive them manually. When you archive your items, you store them in your My Documents folder on your hard drive.
            <a class="post-link" href="/word/lessons/creating-styles.php">Read more</a></p>

        <a href="/resources/word.php" class="post-link">More Microsoft Word Learning Resources</a>
    </div>
</article>

