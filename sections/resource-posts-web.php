<article class="post-default g-text-html">
    <div class="post-img">
        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/html/lessons/styles.php">Applying CSS Tags to HTML </a>
        </h3>
        <p>In my HTML/CSS class at Training Connection we learn different ways of adding CSS styles to our HTML documents. There are benefits and drawbacks of each method available but first let's make sure we understand what a CSS Style is and why we use them.
            <a class="post-link" href="/html/lessons/styles.php">Read more</a></p>
    </div>
</article>

<article class="post-default g-text-css">
    <div class="post-img">
        <img src="/dist/images/icons/icon-web-html5b.png" alt="CSS">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/javascript/lessons/scope.php">Creating a Full-Height Web Page Layout</a>
        </h3>
        <p>A common issue that comes up when creating web pages is what happens when the content on a page is smaller than the screen height.
            <a class="post-link" href="/javascript/lessons/scope.php">Read more</a></p>
    </div>
</article>
<article class="post-default g-text-javascript">
    <div class="post-img">
        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/html/lessons/fullHeight.php">JavaScript Variable Scopes</a>
        </h3>
        <p>You know what a function is and you know how to create variables. But, did you know that a variable behaves differently when it is used inside a function? Specifically, we are talking about variable scope. No, not the mouthwash for variables.
            <a class="post-link" href="/html/lessons/fullHeight.php">Read more</a></p>
    </div>
</article>

<article class="post-default pt-5 pb-2">
    <a href="/resources/web-development.php" class="post-link">More Web Development Learning Resources</a>
</article>