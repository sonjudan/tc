<div class="section-location">

    <div class="section-location-item" data-aos="fade-up" data-aos-delay="100">
        <div class="section-location-info">
            <ul>
                <li>
                    <h4>Location</h4>
                </li>
                <li>
                    <span>Address</span>
                    915 Wilshire Blvd, Suite 1800.<br>
                    Los Angeles CA 90017
                </li>
                <li>
                    <span>Phone</span>
                    <a href="tel:8888150604">(888) 815-0604</a>
                </li>
                <li>
                    <span>Email</span>
                    <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                        Show Email
                    </a>
                </li>
            </ul>
        </div>

        <div class="section-location-maps">
            <div  class="embed-responsive embed-responsive-21by9">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13222.883976755706!2d-118.2600668!3d34.0510274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5831c6d11713747a!2sTraining+Connection!5e0!3m2!1sen!2snl!4v1560364559799!5m2!1sen!2snl" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>