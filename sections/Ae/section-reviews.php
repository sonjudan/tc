<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>After Effects training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_8">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                </div>

                <h3><span>After Effects classes rating:</span> 4.8 stars from 549 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I wasn't sure if this boot camp class was going to be too much for me but I was determined. McKinley made all these hard to learn concepts fun and exciting to learn. I would highly recommend her class to my friends and colleagues.&quot;</p>
                            <span><strong>Michael Sage | SGI-USA</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Kristian was an amazing teacher and was incredibly thorough, patient, and knowledgeable. Great guy and great class! Will recommend for sure.&quot;</p>
                            <span><strong>Jeff Porper | DIRECTV</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;McKinley was amazing! So enthusiastic and one of the most tech savvy people I've ever met. What a boss! Thanks so much!&quot;</p>
                            <span><strong>Bel Downie | YouTube Music</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;I had a one on one class with Kristian and he was unbelievably helpful, he went at a perfect pace and was extremely thorough answering all my questions.&quot;</p>
                            <span><strong>Andre Angel-Bello</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;The professor was very knowledgeable about the program and willing to go off topic to teach other aspects of the program when asked questions. I am very pleased with the overall experience of the class and the teacher.&quot;</p>
                            <span><strong>Toni Rawson | Origin POP</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=60" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>