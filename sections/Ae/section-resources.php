<div class="section section-resources">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="section-title">Resources</h2>
        </div>

        <div class="row justify-content-md-center g-text-ae">
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/bg-resources-ps.jpg" alt="After Effects Tutorials">
                    </span>
                    <p>In this section you will find step-by-step how to tutorials on Adobe After Effects.</p>
                    <a href="/resources/after-effects.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/bg-resources-cheatseet.jpg" alt="Cheatseet & Shortcuts">
                    </span>
                    <p>Useful After Effects cheatseets, and shortcuts PDFs you can download free of charge.</p>
                    <a href="/after-effects-cheatsheet.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/bg-resources-adobe.jpg" alt="After Effects Certification">
                    </span>

                    <?php if(strpos($_SERVER['REQUEST_URI'], 'chicago') !== false):?>
                        <p>The complete guide on After Effects certification in Chicago.</p>
                    <?php elseif(strpos($_SERVER['REQUEST_URI'], 'los-angeles') !== false):?>
                        <p>The complete guide on After Effects certification in Los Angeles.</p>
                    <?php else: ?>
                        <p>The complete guide on After Effects certification in Chicago and Los Angeles.</p>
                    <?php endif; ?>

                    <a href="/certificate-after-effects.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>