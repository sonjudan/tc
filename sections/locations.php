<div class="section-location">
    <div class="section-location-item" data-aos="fade-up" >
        <div class="section-location-info">
            <ul>
                <li>
                    <h4>Our Locations</h4>
                </li>
                <li>
                    <span>Address</span>
                    230 W Monroe Street,<br> Suite 610,<br>
                    Chicago IL 60606
                </li>
                <li>
                    <span>Phone</span>
                    <a href="tel:3126984475">(312) 698-4475</a>
                </li>
                <li>
                    <span>Email</span>

                    <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                        Show Email
                    </a>
                </li>

            </ul>
        </div>

        <div class="section-location-maps">
            <div class="embed-responsive embed-responsive-21by9">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.5366596731305!2d-87.63866028391864!3d41.881314319573015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d7a36f4b096e618!2sTraining+Connection!5e0!3m2!1sen!2sph!4v1559752005915!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="section-location-item" data-aos="fade-up" data-aos-delay="100">
        <div class="section-location-info">
            <ul>
                <li>
                    <span>Address</span>
                    915 Wilshire Blvd, <br>Suite 1800.<br>
                    Los Angeles CA 90017
                </li>
                <li>
                    <span>Phone</span>
                    <a href="tel:8888150604">(888) 815-0604</a>
                </li>
                <li>
                    <span>Email</span>
                    <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                        Show Email
                    </a>
                </li>
            </ul>
        </div>

        <div class="section-location-maps">
            <div  class="embed-responsive embed-responsive-21by9">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13222.883976755706!2d-118.2600668!3d34.0510274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5831c6d11713747a!2sTraining+Connection!5e0!3m2!1sen!2snl!4v1560364559799!5m2!1sen!2snl" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>