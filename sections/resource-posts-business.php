<article class="post-default g-text-presentation">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/communication.jpg" alt="How to Handle Critism">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/business-communication/handling-criticism.php">How to Handle Criticism</a>
        </h3>
        <p>
            Some of the best advice I gained in my teens, was from the book “How to Win Friends and Influence People” written by Dale Carnegie. There is an art to being diplomatic in order to win cooperation when giving criticism. When it comes to taking criticism, how do we know we are handling it well?
            <a class="post-link" href="/business-communication/handling-criticism.php">Read more</a></p>
        <a href="/resources/business-communication.php" class="post-link">More Business Communication Training Resources</a>
    </div>
</article>

<article class="post-default g-text-bus-eti">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/business-etiquette-thumb.jpg" alt="How to use Business Cards">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/business-etiquette/business-cards.php">How to use Business Cards</a>
        </h3>
        <p>Networking is not complete without receiving or giving a business card. The business card is a way for you to follow up on the people you have met. Likewise, it is a way for them to contact you for further meetings. More than that, your business card is a way to brand yourself. Professional-looking business cards send the message that you’re professional. Adding your company motto or tagline in your business advertises you and what you’re all about.
            <a class="post-link" href="/business-etiquette/business-cards.php">Read more</a></p>
        <a class="post-link" href="/resources/business-etiquette.php">More Business Etiquette Training Resources</a>
    </div>
</article>

<article class="post-default g-text-bs-bw">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/writing-thumb.jpg" alt="Spelling and Grammar Tips">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/business-writing/basics.php">Spelling and Grammar Tips</a>
        </h3>
        <p>The building blocks of any writing, whether for business or social purposes, are words. Failure to use words properly can affect the over-all impact of your prose. In this article we will discuss the spelling of words and grammar issues in writing.
            <a class="post-link" href="/business-writing/basics.php">Read more</a></p>
        <a class="post-link" href="/resources/business-writing.php">More Business Writing Training Resources</a>
    </div>
</article>

<article class="post-default g-text-Ps">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/service.jpg" alt="Great Customer Service over the Phone ">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/customer-service/phone-service.php">Great Customer Service over the Phone </a>
        </h3>
        <p>When you are talking to someone in person, body language makes up a large part (some would say more than half) of your message. But as soon as you pick up the phone, body language becomes irrelevant. The success of your interactions depends almost entirely on your tone of voice and your choice of words.
            <a class="post-link" href="/customer-service/phone-service.php">Read more</a></p>
        <a class="post-link" href="/resources/customer-service.php">More Customer Service Training Resources</a>
    </div>
</article>


<article class="post-default">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/leadership-thumb.jpg" alt="Techniques To Conduct More Effective Job Interviews">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="#">Techniques To Conduct More Effective Job Interviews</a>
        </h3>
        <p>According to Career Builder, the average cost of a bad hire is almost $15,000. Two out of three workers will later realize that the job they accepted is a bad fit. As an interviewer, you understand that these stats mean that the hiring process is not as efficient as it may seem.
            <a class="post-link" href="/leadership/job-interviewing-techniques.php">Read more</a></p>
        <a class="post-link" href="/resources/leadership.php">More Leadership Training Resources</a>
    </div>
</article>


<article class="post-default g-text-presentation">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/how-to-handle-nervousness.jpg" alt="Overcoming Nerves during a Presentation">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/presentations/nervousness.php">Overcoming Nerves during a Presentation</a>
        </h3>
        <p>Nervousness is normal when giving a presentation. After all, public speaking is the top fear in the top ten lists of fears. Nervousness can strike at different points in a presentation.
            <a class="post-link" href="/presentations/nervousness.php">Read more</a></p>
        <a class="post-link" href="/resources/presentations.php">More Presentation Training Resources</a>
    </div>
</article>



<article class="post-default g-text-bs-bw">
    <div class="post-img">
        <img src="/dist/images/courses/business-skills/time.jpg" alt="5 Golden Goal Setting Tips">
    </div>
    <div class="post-body">
        <h3 class="post-title">
            <a href="/time-management/lessons/goal-setting-tips.php">5 Golden Goal Setting Tips</a>
        </h3>
        <p>1. Make self-love and happiness your No.1 Goal. Self-love and inner happiness is the key to unlocking your full potential. All other goals such as career, family, wealth, health, relationships and lifestyle all become significantly easier once you have achieved inner-peace and self- acceptance.
            <a class="post-link" href="/time-management/lessons/goal-setting-tips.php">Read more</a></p>
        <a class="post-link" href="/resources/time-management.php">More Time Management Training Resources</a>
    </div>
</article>

