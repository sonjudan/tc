<?php if(strpos($_SERVER['REQUEST_URI'], 'access') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Access.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'excel') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Excel.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'powerpoint') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/PowerPoint.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'project') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Project.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'visio') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Visio.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'word') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Word.jpg);"></div>

<?php elseif(strpos($_SERVER['REQUEST_URI'], 'acrobat') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Acrobat.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'after-effects') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/After-effects.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'captivate') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Captivate.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'dreamweaver') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Dreamweaver.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'illustrator') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Illustrator.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'indesign') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/InDesign.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'premiere-pro') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Premiere-pro.jpg);"></div>

<?php elseif(strpos($_SERVER['REQUEST_URI'], 'customer-service') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Customer-service.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'time-management') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Time-management.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'business-communication') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Business-communication.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'business-writing') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Business-writing.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'presentations') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Presentations.jpg);"></div>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'project-management') !== false):?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Project-management.jpg);"></div>

<?php else: ?>
    <div class="section-hero" data-aos="fade-up" style="background-image:url(/dist/images/yelp-reviews/Generic.jpg);"></div>

<!--    <div class="section-hero" data-aos="fade-up">-->
<!--        <img data-aos="fade-up" data-aos-delay="100" src="/dist/images/icons/icon-5of5.png" alt="5 of 5 - yelp" class="icon-5of5">-->
<!--    </div>-->

<?php endif; ?>