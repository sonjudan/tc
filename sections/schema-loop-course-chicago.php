<span itemscope="" itemprop="performer" itemtype="http://schema.org/Organization">
    <meta itemprop="name" content="Training Connection">
</span>
<span class="d-none" itemprop="location" itemscope="" itemtype="http://schema.org/Place">
    <span itemprop="name">Chicago</span>
    <span itemprop="alternateName" style="display:none">Training Connection - Chicago</span>
    <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">230 W Monroe Street</span><span itemprop="addressLocality">Suite 610</span><span itemprop="addressRegion">IL</span><span itemprop="postalCode">60606</span>
    </span>
</span>