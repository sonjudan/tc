<div class="section-author"  data-aos="fade-up">
    <div class="author-img">
        <img src="/dist/images/instructors/profile-billy-gee.jpg" alt="Billy Gee">
    </div>
    <div class="author-info">
        <h5>About the author</h5>

        <h4>Billy Gee</h4>
        <p>Communication, Customer Service, Leadership and Productivity Expert.</p>

        <a class="btn btn-primary" href="#">View profile</a>
    </div>
</div>