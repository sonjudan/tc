<?php if(strpos($_SERVER['REQUEST_URI'], 'chicago') !== false):?>
    <script type="application/ld+json">

        {
            "@context": "http://schema.org",
            "@type": "School",
            "name": "Chicago Training Classes",
            "url":  "https://www.trainingconnection.com/",
            "alternateName": "Training Connection",
            "description": "Chicago Computer and Business Skills Training Classes",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street Suite 610 Chicago IL 60606",
                "addressLocality": "Chicago",
                "addressRegion": "IL",
                "postalCode": "60606",
                "telephone": "(312) 698-4475",
                "addressCountry": {
                    "@type": "Country",
                    "name": "USA"
                }
            }
        }
    </script>
<?php elseif(strpos($_SERVER['REQUEST_URI'], 'los-angeles') !== false):?>
    <script type="application/ld+json">

        {
            "@context": "http://schema.org",
            "@type": "School",
            "name": "Los Angeles Training Classes",
            "url":  "https://www.trainingconnection.com/",
            "alternateName": "Training Connection",
            "description": "Los Angeles Computer and Business Skills Training Classes",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd Suite 1800 Los Angeles CA 90017",
                "addressLocality": "Los Angeles",
                "addressRegion": "CA",
                "postalCode": "90017",
                "telephone": "(888) 815-0604",
                "addressCountry": {
                    "@type": "Country",
                    "name": "USA"
                }
            }
        }
    </script>
<?php else: ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "School",
            "name": "Chicago Training Classes",
            "url":  "https://www.trainingconnection.com/",
            "alternateName": "Training Connection",
            "description": "Chicago Computer and Business Skills Training Classes",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "230 W Monroe Street Suite 610 Chicago IL 60606",
                "addressLocality": "Chicago",
                "addressRegion": "IL",
                "postalCode": "60606",
                "telephone": "(312) 698-4475",
                "addressCountry": {
                    "@type": "Country",
                    "name": "USA"
                }
            }
        }
    </script>

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "School",
            "name": "Los Angeles Training Classes",
            "url":  "https://www.trainingconnection.com/",
            "alternateName": "Training Connection",
            "description": "Los Angeles Computer and Business Skills Training Classes",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "915 Wilshire Blvd Suite 1800 Los Angeles CA 90017",
                "addressLocality": "Los Angeles",
                "addressRegion": "CA",
                "postalCode": "90017",
                "telephone": "(888) 815-0604",
                "addressCountry": {
                    "@type": "Country",
                    "name": "USA"
                }
            }
        }
    </script>
<?php endif; ?>