<div class="section section-package-deal">
    <div class="container">
        <div class="section-heading mb-0" data-aos="fade-up">
            <h2>Package Deals</h2>
        </div>

        <div class="section-content">
            <div class="heading-l2" data-aos="fade-up" data-aos-delay="50">
                <h3>Captivate Package</h3>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-8">
                    <div class="section-card-include">
                        <span class="title-l3" >What’s Included</span>

                        <div class="card-include-deck">
                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Cp.svg" alt="Photoshop Fundamentals">
                                </div>
                                <span>Captivate Fundamentals</span>
                            </div>

                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Cp.svg" alt="Illustrator Fundamentals">
                                </div>
                                <span>Captivate Advanced</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="price-details">
                        <div class="price-details-heading">
                            <span class="title-l3">Total Investment is</span>
                            <span class="price-l2"><sup>$</sup>1695 <sup>.00</sup></span>
                            <span class="price-l3">you save $295</span>
                        </div>
                        <a href="#" class="btn btn-dark btn-lg"  data-target="#book-package-deal-1" data-toggle="modal" >
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book this Package
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>