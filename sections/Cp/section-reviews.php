<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Captivate training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_6">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                </div>

                <h3><span>Captivate classes rating:</span> 4.6 stars from 644 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Jeff was a really good instructor, he was very balanced - could handle many questions out of context and adjusted the content on the fly to meet our needs.&quot;</p>
                            <span><strong>Anne Kimmitt | Northrop Grumman Aerospace Systems</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Excellent material and presentation. Jeff is an excellent instructor. With 25+ years in the instructional career field, I have a keen eye for instructional excellence. He has it! Please retain him on the staff at all cost. Ray Gutierrez &quot;</p>
                            <span><strong>Ray Gutierrez | Northrop Grumman Aerospace Systems</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Ron Bailey, our instructor, was very knowledgeable, highly skilled, and kept the class enjoyable. I don't usually give all top ratings, but Ron deserves it - he was great.&quot;</p>
                            <span><strong>Kris Berkery | Wolters Kluwer</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Jeff provided great feedback, explanations and expertise in all of the Captivate projects and gave excellent examples when needed. He is an amazing instructor. An asset to the training connection.&quot;</p>
                            <span><strong>Denise Moloney</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;This was a great class. Ron's pacing was good and the practice examples really helped me learn the content. Ron is extremely knowledgeable about the content. I'm looking forward to the Advanced Captivate class.&quot;</p>
                            <span><strong>Holly Wilson | Chicago Public Schools</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=13" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>