<div class="section bg-light g-text-excel" data-aos="fade-up">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="section-title">Resources</h2>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-excel-tut.jpg" alt="Microsoft Excel Tutorials">
                    </span>
                    <p>In this section you will find step-by-step how to tutorials on Microsoft Excel.</p>
                    <a href="/resources/excel.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-excel-cheetsheet.jpg" alt="Cheatseet & Shortcuts">
                    </span>
                    <p>Useful Excel cheatseets, and shortcuts PDFs you can download free of charge.</p>
                    <a href="/excel-cheatsheet.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-excel-specialist.jpg" alt="Specialist">

                    </span>
                    <?php if(strpos($_SERVER['REQUEST_URI'], 'chicago') !== false):?>
                        <p>The complete guide on Microsoft Excel certification in Chicago.</p>
                    <?php elseif(strpos($_SERVER['REQUEST_URI'], 'los-angeles') !== false):?>
                        <p>The complete guide on Microsoft Excel certification in Los Angeles.</p>
                    <?php else: ?>
                        <p>The complete guide on Microsoft Excel certification in Chicago and Los Angeles.</p>
                    <?php endif; ?>

                    <a href="/excel-certification.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card-resource">
                    <span class="card-img">
                        <img class="" src="/dist/images/courses/ms-office/resources-thumb-excel-course.jpg" alt="Microsoft Excel Class">
                    </span>
                    <p>Not sure which Excel class to join? The following guide will help you choose the right class.</p>
                    <a href="/excel-class-guide.php" class="text-link-2 stretched-link">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>