<div class="section section-reviews">
    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2>Acrobat training reviews</h2>
        </div>
        <div class="section-body"  >
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                <div class="star-rate star-rate-4_8">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Acrobat classes - 5 rating"></span>
                </div>

                <h3><span>Acrobat classes rating:</span> 4.8 stars from 129 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Michael is great. I would definitely want to sign up for other courses that he teaches. Very enjoyable - willing to answer so many of our questions. I'd also recommend him to others. Also, what a nice training room!&quot;</p>
                            <span><strong>Cyndi Hefler | Freeborn & Peters LLP</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Ms Beverly Houwing is very knowledgeable in Adobe Acrobat Pro DC. She made the class fun. I've been to many other training classes and she is the best instructor I've had!&quot;</p>
                            <span><strong>Paulette Gomez | County Of San Bernardino</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Information taught in the Adobe class was of great value. I learn tremendous amount of information that will help me perform my job more proficiently.&quot;</p>
                            <span><strong>Candace Ellison-Morgan | Watson Laboratories</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Michael did an excellent job. As a non-Adobe user, I was comfortable with the pace of the class, his teaching approach and the found the materials useful in my workflow.&quot;</p>
                            <span><strong>Robin Williams | Freeborn & Peters LLP</strong></span>
                        </div>
                    </div>
                    <div class="item" >
                        <div class="card-quote light">
                            <i class="icon-quote"></i>
                            <p>&quot;Eva was a fantastic instructor. I really enjoyed our two days together and got a lot out of the course. I am looking forward to more courses in the future.&quot;</p>
                            <span><strong>Kristen Newton | Usbergo, Inc- EcoProProductd</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=8" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>