<div class="section section-reviews" itemscope itemtype="http://schema.org/Product">
    <meta itemprop="description" content="Photoshop classes and Photoshop training from Training Connection."/>
    <meta itemprop="brand" content="Photoshop Classes">
    <meta itemprop="mpn" content="17">
    <meta itemprop="sku" content="Photoshop Classes"/>
    <meta itemprop="productID" content="Photoshop Classes"/>
    <meta itemprop="image" content="/dist/images/icons/adobe-Ps.svg"/>

    <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
          <meta itemprop="offerCount" content="4">
          <meta itemprop="lowPrice" content="495">
          <meta itemprop="highPrice" content="1695">
          <meta itemprop="priceCurrency" content="USD">
    </span>

    <div class="container">
        <div class="section-heading  w-auto" data-aos="fade-up">
            <h2 itemprop="name">Photoshop training reviews</h2>
        </div>
        <div class="section-body">
            <div class="course-rating" data-aos="fade-up" data-aos-delay="100" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="5" />
                <meta itemprop="worstRating" content="1"/>
                <meta itemprop="bestRating" content="5"/>
                <meta itemprop="reviewCount" content="685"/>

                <div class="star-rate star-rate-4_7">
                    <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Photoshop classes - 5 rating"></span>
                </div>

                <h3><span>Photoshop classes rating:</span> 4.7 stars from 685 reviewers</h3>
            </div>

            <div data-aos="fade-up" data-aos-delay="150">

                <div class="owl-carousel owl-theme owl-reviews" >
                    <div class="item">
                        <div class="card-quote light" itemprop="review" itemscope itemtype="http://schema.org/Review">
                            <i class="icon-quote"></i>
                            <p itemprop="description">&quot;The course was great! Eva really did a great job mixing discussion and hands experience together. I was the only student in the class so it was awesome to have all the attention on all my weaknesses. I've always thought any classes I've taken at Training Connection were some of the best ever! Training Connection Rocks!&quot;</p>
                            <span><strong itemprop="author">Barbara Domagala | ENA</strong></span>

                            <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                              <meta itemprop="worstRating" content = "1">
                              <meta itemprop="ratingValue" content="5">
                              <meta itemprop="bestRating" content="5">
                            </span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light" itemprop="review" itemscope itemtype="http://schema.org/Review">
                            <i class="icon-quote"></i>
                            <p itemprop="description">&quot;I loved this course! Eva was a fantastic instructor. She was very knowledgeable, and so friendly and pleasant. I came into the class with a very good knowledge of Photoshop already, but Eva taught me MANY other things that will make my workflow easier and better. I am looking forward to taking an Illustrator class with her in 2 weeks! I wouldn't want anyone else!&quot;</p>
                            <span><strong itemprop="author">Susie Moore</strong></span>

                            <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                              <meta itemprop="worstRating" content = "1">
                              <meta itemprop="ratingValue" content="5">
                              <meta itemprop="bestRating" content="5">
                            </span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light" itemprop="review" itemscope itemtype="http://schema.org/Review">
                            <i class="icon-quote"></i>
                            <p itemprop="description">&quot;Excellent course - since I had no background in Photoshop, I feel more comfortable in knowing what tools are presented and how they can be utilized. Eva did a very good job in presenting and displayed patience throughout. Her background knowledge in art, color, lighting, etc...hugely helpful! Her real world experience provided credibility. Highly recommended!&quot;</p>
                            <span><strong itemprop="author">Fred Phillips | LMT Onsrud</strong></span>

                            <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                              <meta itemprop="worstRating" content = "1">
                              <meta itemprop="ratingValue" content="5">
                              <meta itemprop="bestRating" content="5">
                            </span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light" itemprop="review" itemscope itemtype="http://schema.org/Review">

                            <i lass="icon-quote"></i>
                            <p itemprop="description">&quot;I was so pleasantly surprised by how much was covered in three days. There were just two of us in the class, but even if it were 10 of us, I think I'd have the same experience. Eva was so much fun, shared a lot of great information and knowledge and I really appreciated her real-life experiences with us. I can't wait to take Photoshop Advanced.&quot;</p>
                            <span><strong itemprop="author">Traci Toutant | Magnetar Capital</strong></s
                                pan>
                            <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                              <meta itemprop="worstRating" content = "1">
                              <meta itemprop="ratingValue" content="5">
                              <meta itemprop="bestRating" content="5">
                            </span>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-quote light" itemprop="review" itemscope itemtype="http://schema.org/Review">

                            <i lass="icon-quote"></i>
                            <p itemprop="description">&quot;Eva was amazing. She was patient, knowledgeable and she made this training course enjoyable. Her background and examples she brought in to show us really made me interested in learning more. I signed up for the Graphic Design Package and so far have been pleasantly surprised how easy it was to learn, follow along and learn skills that will help me in all different avenues of my life.&quot;</p>
                            <span><strong itemprop="author">Nastasia Tebeck | Coyote Logistics</strong><
                                /span>
                            <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                              <meta itemprop="worstRating" content = "1">
                              <meta itemprop="ratingValue" content="5">
                              <meta itemprop="bestRating" content="5">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php?course_id=6" class="btn btn-primary btn-lg">View all student reviews</a>
        </div>
    </div>
</div>