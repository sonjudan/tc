<div class="section section-package-deal" >
    <div class="container">
        <div class="section-heading mb-0" data-aos="fade-up">
            <h2>Package Deals</h2>
        </div>

        <div class="section-content">
            <div class="heading-l2" data-aos="fade-up" data-aos-delay="50">
                <h3>Adobe Graphic Design Package</h3>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-8">
                    <div class="section-card-include">
                        <span class="title-l3" >What’s Included</span>

                        <div class="card-include-deck">
                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Ps.svg" alt="Photoshop Fundamentals">
                                </div>
                                <span>Photoshop Fundamentals</span>
                            </div>

                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Ai.svg" alt="Illustrator Fundamentals">
                                </div>
                                <span>Illustrator Fundamentals</span>
                            </div>

                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign Fundamentals">
                                </div>
                                <span>InDesign Fundamentals</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="price-details">
                        <div class="price-details-heading">
                            <span class="title-l3">Total Investment is</span>
                            <span class="price-l2"><sup>$</sup>2390 <sup>.00</sup></span>
                            <span class="price-l3">you save $1195</span>
                        </div>
                        <a href="#" class="btn btn-dark btn-lg"  data-target="#book-package-deal-1" data-toggle="modal" >
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book this Package
                        </a>
                    </div>
                </div>
            </div>

            <hr class="mt-4 mb-4" data-aos="fade-up" >

            <div class="heading-l2" data-aos="fade-up">
                <h3>Adobe Bootcamp Package</h3>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-8">
                    <div class="section-card-include">
                        <span class="title-l3">What’s Included</span>

                        <div class="card-include-deck">
                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Ps.svg" alt="Photoshop Fundamentals">
                                </div>
                                <span>Photoshop Bootcamp</span>
                            </div>
                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Ai.svg" alt="Illustrator Fundamentals">
                                </div>
                                <span>Illustrator Bootcamp</span>
                            </div>
                            <div class="card-include">
                                <div class="card-icon">
                                    <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign Fundamentals">
                                </div>
                                <span>InDesign Bootcamp</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="price-details">
                        <div class="price-details-heading">
                            <span class="title-l3">Total Investment is</span>
                            <span class="price-l2"><sup>$</sup>3390 <sup>.00</sup></span>
                            <span class="price-l3">you save $1695</span>
                        </div>
                        <a href="#" class="btn btn-dark btn-lg"  data-target="#book-package-deal-2" data-toggle="modal" >
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book this Package
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>