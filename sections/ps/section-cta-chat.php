<div class="section section-cta-chat" data-aos="fade-up">
    <div class="container">
        <div class="cta-call" data-aos="fade-up" data-aos-delay="100" >
            <span class="cta-lbl">Call us</span>
            <h3 class="cta-phone">(888) 815-0604</h3>
        </div>
        <h2 data-aos="fade-up">STILL NEED APPROVAL?</h2>
        <h4 data-aos="fade-up"  data-aos-delay="50">Want to reserve a seat but you still need to obtain approval?</h4>
        <p data-aos="fade-up"  data-aos-delay="100">No problem, simply call us or reach us using the chat feature, and we will provisionally hold a seat for you.
            <br>
            There is zero risk, if you don't get approval, we simply cancel the reservation.</p>

        <a data-aos="fade-up"  data-aos-delay="200" href="#" class="btn btn-secondary btn-lg"><i class="far fa-comments"></i> Start Chat</a>
    </div>
</div>