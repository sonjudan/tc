<div class="section section-some-stuff">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Some cool stuff you will create on our classes</h2>
        </div>
        <div class="section-body" data-aos="fade-up" data-aos-delay="150">
            <div class="owl-carousel owl-theme  owl-courses">
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D3.jpg" alt="Photoshop Class Project 1">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D4b.jpg" alt="Photoshop Class Project 2">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D4e.jpg" alt="Photoshop Class Project 3">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D5d.jpg" alt="Photoshop Class Project 4">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D5e.jpg" alt="Photoshop Class Project 5">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_AD_D5f.jpg" alt="Photoshop Class Project 6">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D1.jpg" alt="Photoshop Class Project 7">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D1B.jpg" alt="Photoshop Class Project 8">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D2.jpg" alt="Photoshop Class Project 9">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D3.jpg" alt="Photoshop Class Project 10">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D3c.jpg" alt="Photoshop Class Project 11">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D3d.jpg" alt="Photoshop Class Project 12">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D4a.jpg" alt="Photoshop Class Project 13">
                </div>
                <div class="item">
                    <img src="/dist/images/photoshop/PS_Sample_FD_D5C.jpg" alt="Photoshop Class Project 14">
                </div>
            </div>
        </div>
    </div>
</div>