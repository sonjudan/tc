<?php
$meta_title = "Delivering Constructive Feedback That Produces Positive Change | Training Connection";
$meta_description = "Techniques to deliver exceptional service over the phone. This and other topics are covered in our Customer Service training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item"><a href="/resources/leadership.php">Leadership</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Delivering Constructive Feedback</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Delivering Constructive Feedback That Produces Positive Change</h1>
                </div>
            </div>

            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>According to <a href="https://hbr.org/2014/01/your-employees-want-the-negative-feedback-you-hate-to-give" target="_blank">Zenger and Folkman</a>, both positive and negative feedback are important and critical especially when properly delivered. In addition, 92% believed that negative feedback that is properly conveyed is vital to improving one’s performance. <a href="/business-leadership-training.php">New manager training classes</a>.</p>

                <p>Thus, regardless of your position or industry, you must learn how to deliver constructive feedback to elicit positive change in others. However, as most people know, giving feedback is not that easy.</p>

                <p>A lot of people find it difficult and even challenging. Doing it properly can be very tricky too. Below are tips to help you communicate constructive feedback to produce productive and positive change in the workplace:</p>

                <h3>Tips To Deliver Constructive Feedback</h3>

                <h4>1. You Have To Establish Trust</h4>

                <p>It’s vital to establish a trusting relationship with people you’re going to give constructive feedback to. With trust, the tone of your conversations can be set. Plus, this will allow you to appropriately deliver the feedback and help the other person welcome it and follow the suggestions you provide.</p>

                <p>When a person doesn’t trust you, it’s difficult for them to receive criticism or feedback from you. In this case, they wouldn’t believe you actually have their best interests in mind. You have to ensure that the receiver knows you really believe in their abilities and potential and you recognize their efforts at work.</p>

                <p>Once a person trusts you and thinks you provide feedback in their best interest, they’ll see your criticism as constructive. Thus, they will open their channels of communication allowing you to make the exchange more productive and easier later on. <a href="/business-leadership-training-chicago.php">Chicago management and leadership classes</a>.</p>


                <div class="media-full mb-3">
                    <img src="/dist/images/courses/leadership/feedback-survey.jpg" alt="Feedback Survey">
                    <p class="wp-caption-text">Job Interview; <a href="https://pixabay.com/illustrations/feedback-satisfaction-employee-3240007/" target="_blank">Image Credit</a></p>
                </div>

                <h4>2. You Need To Have A Balanced Perspective</h4>
                <p>When presenting someone with a constructive feedback, make sure you’re delivering a well-balanced perspective. This is true even if your assessment is fundamentally negative.</p>

                <p>You don’t necessarily have to paint a different picture of the negative situation especially if there are major issues with the work or behavior of the person in question, however, it will be helpful if you can include some of the positive aspects of the receiver’s attitude or output. You still have to be truthful and not misleading. But a few positive phrases can actually motivate the person you’re talking to, even in the smallest way. <a href="/business-leadership-training-los-angeles.php">Los Angeles classes in Leadership and Management</a>.</p>

                <div class="media-full mb-3">
                    <img src="/dist/images/courses/leadership/face-to-face-assessment.jpg" alt="Face-To-Face Assessment">
                    <p class="wp-caption-text">Job Interview; <a href="https://pixabay.com/illustrations/feedback-group-office-work-talk-3719861/" target="_blank">Image Credit</a></p>
                </div>

                <h4>3. You Have To Be Specific</h4>
                <p>You have to pay attention to specifics when providing feedback. At first, you can tell them that their output needs to be improved. However, if you don’t tell the person “what” actually needs to be improved and how to fix it, then it won’t be helpful. The individual will be frustrated with not knowing the exact thing that needs to happen or be done.</p>

                <p>The same goes for delivering positive feedback. Don’t just say “Great work.” You should specifically tell them what they did right and give them recommendations on what they can improve.</p>

                <h4>4. You Have To Communicate Face-To-Face</h4>
                <p>Avoid delivering feedback through instant messaging apps, phone SMS, or email. Doing so will be a good candidate for cultivating misinterpretations and miscommunications. It’s best to give constructive criticism in-person.</p>

                <p>The great thing about a face-to-face conversation is it’s dynamic. The two parties can ask questions, further suggestions can be explained properly, and digging deep into the issues can be done.</p>

                <h4>5. You Have To Avoid Making It Personal</h4>
                <p>You have to separate the person from their actions at work and focus on the problem at hand. Don’t tell them that they’re lazy since they don’t proofread their reports. Say that there are errors in the spelling and grammar of their reports and they have to address this issue. If you attack them personally, then they will lose trust and shut down completely. In this case, they won’t listen to what you say or even improve their work.</p>

                <h4>6. You Have To Be Consistent</h4>
                <p>The frequency of your feedback will depend on how many times you interact with the individual. Ensuring that you regularly provide feedback in your meetings and conversations can lead to positive results.</p>

                <p>When you do so, you are ensuring that you and the individual are in agreement in terms of the performance and expectations of the job. When an issue or a positive event happens regarding their performance, you should be prepared to provide the necessary constructive feedback. In addition, the other party should be prepared to accept it.</p>

                <h4>7. You Have To Be Timely</h4>
                <p>A lot of supervisors and managers wait for days or even weeks just to give their team members feedback. This is not recommended as you want the conversation to be actionable and relevant during that time. Ideas that come up, processes that happen, and challenges faced at work should still be fresh in your and your team’s minds.</p>

                <h4>8. You Can Take Professional Courses</h4>
                <p>To learn how to deliver constructive feedback that produces positive change, you can opt for Leadership Training as well. Business professionals will tell and show you how to handle assessment of individuals in a tactful manner eliciting a productive outcome on their part.</p>


                <h3>To Wrap It All Up</h3>
                <p>Delivering feedback is not easy thus you must heed our tips to help you provide constructive criticism in a manner that elicits positive change in your colleagues (and employees). To end this article, we would like to say that you should learn to practice providing feedback in the comforts of your own home. Look in the mirror and see if you like how you convey the message. Keep in mind that constant practice goes a long way.</p>

            </div>


            <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/author.php'; ?>

        </div>
    </main>
    <div class="mb-5">&nbsp;</div>

    <div class="section-widget g-text-Ai" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Onsite Leadership training</h4>
                <p>Through our network of local trainers we deliver onsite Leadership classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Leadership class</a>.</p>

                <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=25">Leadership student testimonials</a>. </p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>