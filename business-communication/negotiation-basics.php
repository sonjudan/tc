<?php
$meta_title = "Understanding the basics of negotiation | Training Connection";
$meta_description = "This article looks a different types of negotiation, the phases of every active negotiation and skills needed to be a successful negotiator. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Negotiation Basics</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>The Basics of Negotiation</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In this article we will look at  two  different types of negotiation. We’ll consider the three phases of very negotiation and the skills needed to become an effective negotiator.</p>

                <p>Need to improve your communication skills so you can become highly effective in the workplace? Why not join one our fun and informative <a href="/business-communication-training.php"> business communication workshops</a> offered in Chicago and Los Angeles. Call  us on 888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/negotiation-basics.jpg" width="750" height="500" alt="Negotiating like a pro"></p>

                <h4>Types of Negotiations</h4>
                <p>The two basic types of negotiations require different approaches.</p>
                <ul>
                    <li><strong>Integrativ</strong>e negotiations are based on cooperation. Both parties believe they can walk away with something they want without giving up something important. The dominant approach in integrative negotiations is problem solving. Integrative negotiations involve:</li>
                    <li>Multiple issues. This allows each party to make concessions on less important issues in return for concessions from the other party on more important issues.</li>
                    <li>Information sharing. This is an essential part of problem solving.</li>
                    <li>Bridge building. The success of integrative negotiations depends on a spirit of trust and cooperation.</li>
                    <li><strong>Distributive</strong> negotiations involve a fixed pie. There is only so much to go around and each party wants as big a slice as possible. An example of a distributive negotiation is haggling over the price of a car with a car salesman. In this type of negotiation, the parties are less interested in forming a relationship or creating a positive impression. Distributive relationships involve:</li>
                    <li>Keeping information confidential. For example, you don’t want a car salesman to know how badly you need a new car or how much you are willing to pay.</li>
                    <li>Trying to extract information from the other party. In a negotiation, knowledge truly is power. The more you know about the other party’s situation, the stronger your bargaining position is.</li>
                    <li>Letting the other party make the first offer. It might be just what you were planning to offer yourself!</li>
                </ul>

                <a href="/resources/business-communication.php">View our Library of Business Communication resources.</a>


                <h4>The Three Phases</h4>
                <p> The three phases of a negotiation are:</p>
                <ul>
                    <li>Exchanging Information</li>
                    <li>Bargaining</li>
                    <li>Closing</li>
                </ul>
                <p><a href="/business-communication-training-chicago.php">Chicago workshops on boosting business communication</a>. </p>
                <p>These phases describe the negotiation process itself. Before the process begins, both parties need to prepare for the negotiation. This involves establishing their bargaining position by defining their BATNA, WATNA, and WAP (see Module Three). It also involves gathering information about the issues to be addressed in the negotiation.
                    After the negotiation, both parties should work to restore relationships that may have been frayed by the negotiation process. It is essential to pay attention to all the phases of negotiation. Without the first phase, the exchange of information, and the establishment of bargaining positions, the second phase cannot happen in any meaningful sense because no one knows where they stand. It sets a scene for demands to be manageable and reasonable. Negotiations are, after all, about the art of the possible. Without the third phase, anything that has been decided during phase two cannot be formalized and will not take hold - leading to the necessity for further negotiation or an absolute breakdown in a relationship.</p>

                <p>Also read how to <a href="http://www.pon.harvard.edu/daily/negotiation-skills-daily/how-to-negotiate-under-pressure/">negotiate under pressure</a>.</p>
                <h4>Skills for Successful Negotiating</h4>
                <p>Key skills include:</p>
                <ul>
                    <li>Effective speaking </li>
                    <li>Effective listening </li>
                    <li>A sense of humor</li>
                    <li>A positive attitude</li>
                    <li>Respect</li>
                    <li>Self-confidence</li>
                    <li>Emotional intelligence</li>
                    <li>Persistence</li>
                    <li>Patience</li>
                    <li>Creativity</li>
                </ul>

                <p>Without the above factors, negotiations will be difficult if not impossible. The necessity for negotiation arises because neither party will be able to get everything they want. Knowing that there must be concessions, each party in the negotiation is required to adopt an attitude of understanding that they must get the best deal possible in a way which is acceptable to the other party. The importance of effective speaking and listening is clear; it is necessary to establish what you are looking for and what you are prepared to accept, while understanding what the other parties will be happy with. <a href="/business-communication-training-los-angeles.php">Benefits of communication training in LA</a>.</p>

                <p>A sense of humor and a positive attitude are essential because they allow for a sense of give and take. Negotiations can become fraught, and having the ability to see the other side’s point of view while being sanguine with regard to what you can achieve will be essential. Of course you will want as much as you can get - but the other side needs to achieve what they can, too. Seriously uneven negotiations will simply lead to further problems along the line. An atmosphere of respect is essential. If you do not make concessions while demanding them from your counterpart, it makes for a negotiation which will end in dissatisfaction.</p>
                <p>However important a sense of understanding for your “opponent” may be, it is also necessary to have the confidence to not settle for less than you feel is fair. Good negotiators understand the importance of balance. Yes, you will have to make concessions, but the point of making concessions is to secure what you can get - so you need to pay attention to your bottom line and ensure you are not beaten down to a minimum. Knowing what is realistic, and ensuring that you can get the best deal, relies on being ready to insist upon something that the other side may not be willing to give initially. Emotional intelligence, persistence, patience, and creativity can all play a part here.</p>

                <p>Also read <a href="/business-communication/preparing-for-negotiation.php">Preparing for Negotiation</a> </p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>