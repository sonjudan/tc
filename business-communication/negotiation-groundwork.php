<?php
$meta_title = "Negotiation Groundwork | Training Connection";
$meta_description = "This article covers how to lay the groundwork for a successful negotiation. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Negotiation Groundwork</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Laying the Groundwork for Successful Negotiation</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In the previous article, we looked at the <a href="/business-communication/preparing-for-negotiation.php">importance of establishing your bargaining position</a>. In this article we consider other aspects of preparation: setting the time and place, establishing common ground, and creating a negotiating framework. Even at this early stage it is important to have certain principles in place.</p>
                <p>If you allow them to be compromised, then you will already have put yourself in a position where you can be considered as prey for hostile negotiators. Getting the groundwork in place may seem like a formality, but it is the first stage of negotiations, and therefore as much a part of the arrangements as any other</p>
                <p>Looking for a <a href="/business-communication-training.php">Business Communication training class</a>? Why not join one our interactive and informative workshops offered in Chicago and Los Angeles. Call  us on 888.815.0604 for more information.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/groundwork.jpg" width="750" height="422" alt="How to prepare for a negotiation"></p>
                <h4>Setting the Time and Place</h4>
                <p> Setting the time and place can give you an advantage in a negotiation. People feel most comfortable conducting a negotiation on their home turf. Most people have a particular time of day when they feel most alert and clear-headed.</p>
                <p>Environmental factors can interfere with negotiations, for example:</p>
                <ul>
                    <li>A noisy setting</li>
                    <li>Frequent interruptions</li>
                    <li>Crowded conditions</li>
                    <li>Lack of privacy</li>
                </ul>
                <p>If you are conducting a negotiation at your own site, you have control over most of these things. If you are negotiating at the other party’s site, ask the other party to remedy these conditions as much as possible before negotiations begin. <a href="/business-communication-training-los-angeles.php">Los Angeles classes in Business Communication</a>.</p>
                <p>In sport, every game takes place at a venue, and in most cases one of the parties involved will be the “home team”. In the vast majority of cases, where the parties are evenly matched in terms of talent and preparation, the team that wins will be the home team. They are playing in familiar surroundings, where things such as climate and ambient noise are to their advantage. The away team spends the early part of the game acclimatizing to their unfamiliar surroundings. </p>
                <p>In political negotiations leading on from a war (or trying to prevent one), there is a tendency to hold the discussions in a neutral venue, where both parties are equally unfamiliar with the surroundings, meaning that neither has the advantage and allowing the negotiations to be even-handed. In business, it is rare to have the opportunity to hold negotiations in a neutral venue, and frequently there will be a “home side”.</p>
                <p>The time of negotiations is also important. Human beings are always in some part at the mercy of their “biorhythms” which cause the body and the mind to function differently at different times of day. Some people, as you will know, tend to be “morning people” while others are more comfortable the longer the day goes on. If you want to build in an advantage in negotiations, it is worth making sure either that the negotiations are held at your home venue, at your most comfortable time of day, or both. Sometimes there will be debate about the setting for a negotiation - and often, this is where the first negotiations and concessions will take place.</p>

                <h4>Establishing Common Ground</h4>
                <p> Sometimes the parties in a negotiation begin by discussing the issue on which they are farthest apart. It might seem like they are working hard, but they are not working effectively. </p>
                <p>It is often more effective to begin by discussing what the parties agree on and then move to an issue on which they are close to agreement. Then they can take on progressively tougher issues until they reach the issue on which they are farthest apart. This gradual approach sets a positive tone for the negotiation. It also helps the two parties get into a pattern of thinking about issues in terms of shared interests. <a href="/business-communication-training-chicago.php">Communication skills training in Chicago</a>.</p>
                <p>Momentum is an important thing in negotiations. If the meeting is continually stalled by disputes over the smallest of issues, the outcome is likely to be less desirable for both parties as the goodwill which is necessary to drive negotiations forward will be extremely thin on the ground. For this reason, having an agenda which is stacked in favor of positive items at the beginning is a way that will work best for both sides. Concessions will have to happen in the end, but if both sides are in a positive frame of mind it creates a positive dynamic in which to negotiate.</p>
                <p>Also read <a href="http://www.pon.harvard.edu/daily/batna/10-hardball-tactics-in-negotiation/">10 Hard Bargaining Negotiating Skills and Negotiation Strategies</a>.</p>

                <h4>Creating a Negotiation Framework</h4>
                <p> Both sides in a negotiation bring their own frame of reference based on their experience, values, and goals. For a negotiation to proceed, the two sides have to agree to a common framework. They need to agree on what issues are being addressed. Sometimes the way these issues are stated will influence the course of the negotiation. Each side would like to frame the issues in a way that furthers its goals. From this it is possible to see how involved negotiations can get. Sometimes people will use a phrase to describe preliminary negotiations: “talks about talks” - and this is a fairly interesting phrase, as it sheds light on just how much is up for debate in the average negotiation.</p>
                <p>Before starting negotiations, it is essential to agree on which issues are up for negotiation and which are non-negotiable. Those issues which are non-negotiable are taken off the negotiating table and the parties endeavor to move forward with what they can negotiate on. It can also be decided what form of words will be used in the program for negotiations - making clear to both sides what matters are off limits, and why.</p>
                <p>Without establishing a framework, negotiations can be extremely disorganized and lack direction. It helps to remember that trying to get a negotiated settlement between two parties who have their differences calls for a great deal of patience and acceptance on both sides that there will be some “medicine” to take - you don’t want to take it, but it is necessary - and therefore it is important to make the pill as sweet as possible. Setting a positive framework for negotiations is all about sweetening the pill.</p>
                <p>Also read  <a href="/business-communication/body-language-basics.php">Body Language Basics</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>