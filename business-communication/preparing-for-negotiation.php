<?php
$meta_title = "Preparing for Negotiation | Training Connection";
$meta_description = "This article covers how you can prepare to lead a successful negotiation. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Negotiation Preparations</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Plan for a Negotiation</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Like any challenging task, negotiation requires preparation. Before you begin a negotiation, you need to define what you hope to get out of it, what you will settle for, and what you consider unacceptable. You also need to prepare yourself personally. The key to personal preparation is to approach the negotiation with self-confidence and a positive attitude.</p>
                <p>Without this preparation, you will end up giving more than you get from negotiations. It may be unavoidable that you will have to give up more than you would ordinarily be willing to, but finding the balance between acceptable concessions and getting the best deal for yourself relies on you being ready to go into negotiations with the strongest bargaining position you can.</p>
                <p>Looking for a <a href="/business-communication-training.php">business communication class</a>? Why not join one our fun and informative classes offered in Chicago and Los Angeles. Call  us on 888.815.0604 for more information.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/planning-negotiation.jpg" width="750" height="437" alt="How to prepare for a negotiation"></p>

                <h4>Establishing Your WATNA and BATNA</h4>
                <p> In most negotiations, the parties are influenced by their assumptions about what they think are the alternatives to a negotiated agreement. Often the parties have an unrealistic idea of what these alternatives are, and they are unwilling to make concessions because they think they can do just as well without negotiating. If you do not have a clear idea of your <strong>WATNA</strong> (<strong>Worst Alternative to a Negotiated Agreement) </strong>and <strong>BATNA</strong> (<strong>Best Alternative to a Negotiated Agreement</strong>), you will negotiate poorly based on false notions about what you can expect without an agreement. <a href="/business-communication-training-los-angeles.php">Business communication training in Los Angeles</a>.</p>
                <p>Often the parties in a negotiation need to decide how likely a particular outcome will be. If your WATNA is something that would be difficult for you to accept, but the likelihood of it happening is small, you might not feel compelled to give up much in negotiations. Realism is essential in this situation. If you could have the ideal situation, the “blue sky” scenario, negotiations would not be necessary. In order to focus on the negotiations with a sense of purpose, your WATNA is important. What is often referred to as the “worst case scenario” is something that any sensible person will think about before embarking on any initiative. What if it goes wrong? How will we deal with that? How you feel about the WATNA will dictate how flexible you need to be (and therefore will be) in negotiations.</p>
                <p>The BATNA is almost more important than the WATNA. If you look at your situation in the absence of a negotiated agreement, and find it almost unthinkable, you will be pressed to enter negotiations in the hope of getting a satisfactory agreement. The word “satisfactory” is important here. Is the WATNA better than satisfactory? Is the BATNA worse? Generally, people only enter into negotiations because they feel they have to. They arrive at this conclusion based on analysis of their WATNA and BATNA.</p>
                <p>Visit our <a href="/resources/business-communication.php">Business Communication Resources page</a>.</p>
                <h4>Identifying Your WAP</h4>
                <p> In any negotiation, it is important that you keep your <strong>WAP</strong> (<strong>Walk Away Price</strong>) to yourself, especially if it is significantly less than your initial offer. If the other party knows that you will be willing to take a lot less than you are offering, then you will be negotiating from a position of weakness. If the other party knows, or has an idea of your WAP then it stops being your WAP and simply becomes your price. Establishing a WAP in your mind, and ensuring that those negotiators on your side of the bargain (and only they) know it, allows you to take your strongest possible bargaining position. The other party will try to argue you down from your proposed price, so you will need to remain firm. If they want to pay less, then you may be prepared to agree on a lower price in return for concessions.<a href="/business-communication-training-chicago.php"> Business Communication classes in Chicago</a>.</p>
                <p>The opposing party will then have to consider what is acceptable to them. Rather than push too hard and lose out on a deal which would be beneficial to themselves, they will have their own areas where they are willing to make concessions. However, if they know that you have set a WAP that would save them money, they will simply hold firm at that price. They have no incentive to make concessions to you. In many ways, negotiation is about keeping as much to yourself as you possibly can until you can no longer maintain that position.</p>
                <p>  Once you have set your WAP, it is essential to keep to it. A walk away price becomes absolutely meaningless if you are not prepared to walk away should it not be met. You should give the impression to opponents in negotiation that you could walk away at any time. They will, after all, not be prepared to stop once they get a price which is satisfactory to them - they will look to wring a bit more value out of the deal for themselves, testing you to see what you will give up. A warning against setting your WAP unrealistically low is that the other party will not take you seriously if you are a pushover in negotiations. They will seek to test you at every turn.</p>
                <p>Also read <a href="http://insight.kellogg.northwestern.edu/article/is-your-negotiation-strategy-wrong">is your negotiation strategy wrong</a>.</p>

                <h4>Identifying Your ZOPA</h4>
                <p> In the negotiation for the used car, both parties should feel good about the outcome. Even though the parties might have hoped for a better deal, both got a better price than their WAP.</p>
                <p>This negotiation demonstrates the importance of keeping your WAP to yourself if you want to negotiate the best deal. Your range in this situation falls between the price that you would ideally, realistically get and the WAP you have set. In an ideal world you could demand a million dollars and expect to get it. In a realistic world, you need to be realistic in negotiations. </p>
                <p>You should arrive at your ideal realistic price by seeing what the accepted market value for what you are offering is. By adjusting for your specific negotiating position (whether you are approaching it from a position of need, etc.), you can find your best realistic price. Then think about a price at which it would no longer be worthwhile to strike a deal.</p>
                <p>Your co-negotiator will have done the same. What he hopes to pay and what you hope to get are just that – hopeful. The <strong>ZOPA</strong> (<strong>Zone Of Possible Agreement</strong>) is the area in which the final price will sit, and within that ZOPA you will ideally end up with a price closer to their WAP than yours. If you hint at where your WAP is, the other party will be less likely to come to an agreement that is substantially better than that. </p>


                <h4>Personal Preparation</h4>
                <p> One way to relieve some of the tension you may be feeling before a negotiation is to remind yourself that there is nothing to be afraid of. As long as you understand your position, there is no danger that you will “lose” the negotiation. During and before negotiation you should always be:</p>
                <ul>
                    <li>Polite - It never reduces your argument</li>
                    <li>Firm - Removes Perceptions of Weakness</li>
                    <li>Calm - Facilitates Persuasion and Compromise</li>
                    <li>Do not take things personally</li>
                </ul>
                <p>Knowing your position before entering negotiations means that you are sure of your “red lines”. Things that you are not prepared to consider that would make your position worse than it is now. Many people get pushed into a deal which is unsatisfactory to them because they have failed to prepare for the negotiation in this way. If you go into negotiations with vague ideas, that vagueness will become a weakness in your negotiating position. </p>
                <p>The important thing about your position in negotiations is that you should be the only one who knows what it is. Many people compare negotiation to a game of poker. When playing poker you should always be careful to keep to yourself what kind of hand you have. If your opponent knows your position, they will squeeze you to its very limits, confident that you have no strong impetus to push back.</p>
                <p>When a negotiator knows that their “opponent” has a weak or compromised position, they will instinctively know that they are negotiating with someone who is working from a position of desperation. They will believe “that’s what he’s decided he is willing to settle for, because he needs this deal. Does he need it enough to give me a little bit more leverage?”, and will negotiate from that standpoint.</p>

                <p>Also read <a href="/business-communication/negotiation-groundwork.php">Laying the Groundwork for Successful Negotiation</a>.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>