<?php
$meta_title = "Common Body Language Mistakes | Training Connection";
$meta_description = "Learn how to avoid common Body Language mistakes such as poor posture, invading personal space, quick movements and fidgeting. Body language is an important skill covered in our Business Communication class, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Body Language Mistakes</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Avoid Body Language Mistakes</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>There are different factors that will create false body language signals. This is why it is so important to examine the positions and gestures as a whole when attempting to interpret body language. To prevent body language mistakes, become aware of these factors and think carefully when reading body language. </p>
                <p>For more details on our <a href="/business-communication-training.php">business communication training course</a> call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/personal-space.jpg" width="600" height="420" alt="Avoid invading personal space"></p>

                <h4>Poor Posture</h4>
                <p>Posture can lead to unfair judgments and prejudices. Often, poor posture is seen as a closed body language that people assume is caused by a lack of confidence. There are, however, many different reasons why someone can have poor posture. While it is true that most people can improve on their posture, the changes that can be made to a person’s musculoskeletal structure are limited. Always pay attention to other cues, and do not make rash judgments based solely on posture.</p>
                <h4><strong>Some Causes of Poor Posture</strong></h4>
                <ul>
                    <li><strong>Injury</strong>: Both acute injuries and repetitive motion injuries can alter someone’s posture.</li>
                    <li><strong>Illness</strong>: Autoimmune diseases, such as arthritis, can damage the skeletal structure. </li>
                    <li><strong>Skeletal structure</strong>: Scoliosis and other problems with the spine will affect posture.&nbsp; </li>
                    <li><strong>Temperature</strong>: People may take a closed posture when they are cold.</li>
                </ul>
                <h4>Invading Personal Space</h4>
                <p>Invading personal space is seen as an act of hostility. Western societies typically use five different zones, depending on the social situations. <a href="/business-communication-training-chicago.php">Instructor-led communication classes</a> in Chicago.</p>
                <ul>
                    <li><strong>12 feet</strong>: This zone is for the public. The purpose is to avoid physical interaction. &nbsp;</li>
                    <li><strong>4 feet: </strong>This zone is reserved for social interactions such as business settings. Touching requires the individual to move forward. </li>
                    <li><strong>18 inches: </strong>This is a personal zone. It allows contact, and it is reserved for friends and family.</li>
                    <li><strong>6 inches:</strong>&nbsp; This zone is reserved for close relationships. This zone can be invaded in crowds or sports.</li>
                    <li><strong>0 to 6 inches: </strong>This zone is reserved for intimate relationships.</li>
                </ul>
                <h4>Personal Space Differences</h4>
                <ul>
                    <li><strong>Culture</strong>: Each culture has different boundaries and personal space.</li>
                    <li><strong>Background</strong>: Personal history and background will affect an individual’s concept of personal space.</li>
                    <li><strong>Activity</strong>: Some activities require people to work closely. This should be considered before assuming someone is invading personal space.</li>
                </ul>

                <h4>Quick Movements</h4>
                <p>Quick movements may be interpreted as a sign of nervousness. They may, however, be used to draw attention to specific information when speaking. Consistent jerking movements, however, do not always indicate nerves or negative emotions. Do not make a snap judgment about quick movements. There are reasons why movements may seem quick or jerking. <a href="/business-communication-training-los-angeles.php">Communication classes offered in Los Angeles</a>.</p>
                <h4>May alter movement</h4>
                <ul>
                    <li>Stress</li>
                    <li>Illness</li>
                    <li>Exhaustion</li>
                    <li>Cold</li>
                </ul>

                <h4>Fidgeting</h4>
                <p>Most people fidget from time to time. In interviews and social settings, fidgeting can indicate nervousness, boredom, frustration, stress, or self-consciousness. It is an outlet to release feelings or an attempt at self-comfort. Besides emotions, there are a number of other reasons why people may fidget.</p>

                <h4>Other Reasons for Fidgeting</h4>

                <ul>
                    <li>Attention deficit disorder: ADD and ADHD are often accompanied by fidgeting. </li>
                    <li>Hormone imbalances: These may be accompanied by nervous energy.</li>
                    <li>Blood sugar imbalances: Fidgeting accompanies sugar highs.</li>
                    <li>Imbalanced brain chemistry: These may increase tension.</li>
                    <li>Medications: Steroids and other medications can cause imbalances.</li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Communication Lessons</h4>
                    <ul>
                        <li><a href="/business-communication/reading-body-language.php">Reading Body Language</a></li>
                        <li><a href="/business-communication/body-language-mistakes.php">Body Language Mistakes</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>