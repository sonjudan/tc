<?php
$meta_title = "Ways to Handle Criticism | Training Connection";
$meta_description = "Learn how to handle criticism so you can better yourself. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Handling Criticism</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Ways to handle criticism</h1>
                        <h4>by Allyncia Williams</h4>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Some of the best advice I gained in my teens, was from the book <a href="https://www.amazon.com/How-Win-Friends-Influence-People/dp/0671027034">“How to Win Friends and Influence People”</a> written by Dale Carnegie. There is an art to being diplomatic in order to win cooperation when giving criticism. When it comes to taking criticism, how do we know we are handling it well?</p>
                <p>Not everyone with negative feedback takes the “honey” route, but criticism is a necessary part of self-discovery and growth. Not everyone who has a criticism will bring it to your attention in time for you to address it, so those who do may be doing you a huge favor. When someone criticizes your work, you may take it several ways:</p>
                <ul>
                    <li>All your work is in vain</li>
                    <li>This person does not like you</li>
                    <li>You need to further explain/blame external factors</li>
                    <li>You need to know why</li>
                </ul>
                <p>For more details on <a href="/business-communication-training.php">effective business communication training</a> please call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/handling-critism.jpg" width="780" height="405" alt="How to handle critism"></p>

                <p><em>"A drop of honey catches more flies than a gallon of gall." So with men. If you would win a man to your cause, first convince him that you are his sincere friend. Therein is a drop of honey which catches his heart, which, say what he will, is the high road to his reason.</em> <a href="http://www.biography.com/people/abraham-lincoln-9382540">Abraham Lincoln</a></p>

                <p>Criticism can affect your self-esteem, your choices and the integrity of your professional and personal relationships. When you respond obsessively in any of these ways, you can set a bad precedent for how well you can be trained or grow from negative experiences, which all relationship and journeys in excellence are bound to encounter. <a href="/business-communication-training-los-angeles.php">Click here</a> for more information on Business Communication classes.</p>
                <p>In this article, we will define criticism, look at some famous examples of handling criticism, and consider how we can choose to respond to criticism in the most responsible and effective way.</p>
                <p>First, what is criticism? According to Dictionary.com online: Criticism is the act of passing judgment to the merits of anything. The word historically comes from the estimation of literary worth.</p>

                <h4>Famous examples of handling criticism:</h4>
                <p><a href="http://www.biography.com/people/oprah-winfrey-9534419">Oprah Winfrey</a> began as an evening news reporter for Baltimore’s WJZ-TV and was criticized because she couldn’t separate her emotions from her stories. </p>
                <p><a href="http://www.jkrowling.com/en_US/">
                        J.K. Rowling</a> was criticized when working at the London office of Amnesty International because she would write stories on her work computer all day long. </p>
                <p><a href="http://thoughtcatalog.com/rachel-hodin/2013/10/35-famous-people-who-were-painfully-rejected-before-making-it-big/">
                        Walt Disney</a> was criticized while working for the Kansas City Star in 1919 because, his editor said, he “lacked imagination and had no good ideas.”</p>
                <p><a href="http://www.reelclassics.com/Actors/Astaire/astaire-bio.htm">
                        Fred Astaire</a> was handed a note after an early screen test in 1933 that read: “Can’t sing. Can’t act. Balding. Can dance a little.” He kept, framed and placed it above his fireplace.</p>
                <p>Many of these criticisms cost the celebrity his or her job yet we know their names because they did not give up on their opportunities to learn and grow from criticism. So what is the best way to handle criticism? If Rowling was not doing her job, Oprah was too emotional, Disney’s articles were called boring and Astaire’s age was irreversible, how did these people overcome such strong negative memories and improve their lives?</p>

                <h4>How we respond to criticism</h4>
                <p>According to&nbsp;<a href="http://koso.ucsd.edu/~martin/">Dr. Martin Paulus</a>, Adjunct Professor of Psychiatry at the University of California, San Diego, there are two portions of our brain that determine how we emotionally process and respond to criticism: the amygdala and the medial prefrontal cortex. The amygdala impresses us with what is important and plays a major role in the formation of our emotional memories. The medial prefrontal cortex regulates how we react to emotional stimuli (like criticism). The amygdala also plays a huge part in our fight or flight response,&nbsp;which is why&nbsp;receiving negative news like criticism can often feel threatening. Because of a previous negative experience, criticism can lead to a stress response where we can become disparaging of our worth, defensive, or motivated to escape or take deliberate action. </p>

                <p><strong>So how do we best handle criticism?</strong></p>
                <p>It is only my speculation about these celebrity stories that Oprah learned to embrace her emotional style of news reporting. JK Rowling began to allow survival mode to set in with her love for writing. Astaire and Disney were hurt by negative feedback, but it only made them more tenacious to overcome and stretch what they had to offer. Given what we know about how our perceptions can shape our choices, these examples went on to learn and succeed in spite of criticism. <a href="/business-communication-training-chicago.php">More info</a> on Chicago business communication training.</p>

                <h4>To be empowered by criticism, here are just 3 basic tips</h4>
                <p>Actively listening will help you gain respect and trust from the person you are listening to. It will facilitate better and clearer understanding which in turn will result in stronger bonds and better relationships. Actively listening will help you probe and gain a deeper understanding of the issues, helping you to avoid further issues, and improve your productivity.</p>

                <ol>
                    <li>
                        <p>Manage your <a href="http://psychcentral.com/lib/fight-or-flight/">fight or flight reaction</a>. </p>

                        <p>The Fight or Flight response can actually be a learned response. Therefore, allowing oneself to go into fight or flight survival mode, the result of the sympathetic nervous system being activated due to threatening stimuli every time you hear negative feedback, may not be necessary and actually prevents you from rational decision making. Going out in a rage of glory looks great on a cowboy film but Fred Astaire was going to need to have a reputation of delivering his personal best in numerous auditions before he was  selected as the best and inimitable fit. Astaire grew immune to these words and kept auditioning and honing his craft. In JK Rowling’s case, going into survival modeand doing what she believed in, was a chance for her to see if she could realize her true strengths elsewhere.</p>
                    </li>

                    <li>
                        <p>Avoid taking criticism as a personal attack</p>
                        <p>Feedback, positive or negative, is first just an observation. Maybe someone says you smile too much, but is that a bad thing? It appears in Oprah’s case, the feedback she received  allowed her to discover what comes naturally and to find her best fit in her field. Through this she revolutionized the genre of talk shows. Similarly, Disney could have selected uninteresting topics. This criticism could be what drove him to challenge his own ideas and surround himself with super creative and imaginative people. You may find your criticism is actually a compliment. You may find the criticism will help you discover what it is you are missing, so you can fix that gap.</p>
                    </li>
                    <li>
                        <p>Ask and learn from criticism</p>
                        <p>Fight or flight reaction to criticism can also mean that you internalize all your questions, self-doubt, excuses and or blame. This could affect how you continue to pursue your craft or that you harbor ill feelings and suspicions about everyone you work with because of your constant concern for your survival. Eventually, you will burn out. Focus instead on what you uniquely have to offer and what you can uniquely learn from the criticism. Invite the other party to share expectations without dwelling on what you did wrong. Learning how to acknowledge what is important to someone else can shape not only your skills, but the mark you make in your field. &nbsp;</p>
                    </li>
                </ol>
                <p><a href="/business-communication/active-listening.php">For more tips on “Active Listening” </a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>