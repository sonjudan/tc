<?php
$meta_title = "The Benefits of Building Rapport | Training Connection";
$meta_description = "Rapport Building techniques which anyone can use to greatly improve their communication. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Building Rapport</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Benefits of Building Rapport</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>People like themselves, that is basic human nature. People are also more receptive towards others who are similar to them or have similar backgrounds to them. Once you have rapport with someone, there is a mutual liking and trust. </p><p>The benefits to building rapport with someone is that they will be much more likely to want to do business with you, share information, recommend you to others and support your ideas. Improved sales, productivity and teamwork can all result from building rapport with colleagues and clients.</p>
                <p>For more details on our <a href="/business-communication-training.php">business communication workshops</a> please call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/building-rapport.jpg" width="750" height="413" alt="Building Rapport techniques"></p>

                <h4>How to Build Rapport</h4>

                <p>We all know those natural communicators who seem to get along with everyone. Although they are naturals, building rapport is actually a rather simple skill that anyone can learn.</p>
                <h4>1. Find a Common Connection</h4>
                <p>Remember people like people who are similar to themselves. Observe people closely to find any common ground. Examples include where they are from, which college they attended, which team they support, what hobbies they enjoy, if they have children, where they have traveled to etc.</p>
                <p>
                    Armed with this information you can easily strike up a conversation about a topic that is of interest to the other party. <a href="/business-communication-training-chicago.php">Chicago communication skills classes</a>.</p>
                <h4>2. Mirroring and Matching</h4>
                <p>This is when you adjust your own body language and <a href="/business-communication/verbal-communication.php">spoken language</a> to match that of the person you are trying to build rapport with. Remember it's human nature that people like themselves, and therefore by default, they are also be drawn towards people who are similar to themselves.</p>
                <ol>
                    <li>Mirroring someone else's <a href="/business-communication/body-language-basics.php">body language, gestures and posture</a> is subliminal and very powerful. For example if someone is seated cross-legged then you can do the same or if someone is displays really good posture then mirror that (you can slouch the moment they walk out the room). You can even try and match the way they breath. </li>
                    <li>Use the same words and lingo. If a person has a certain way of saying something, start using the same words. Even matching someone's slang, will build rapport as they will feel like you come from the same place as them, because of the way you speak.</li>
                    <li>Match their voice tone, volume and speed. If someone is a slow and deliberate speaker, then don't trying and rattle off your products 4 best features. Rather talk back to them matching the speed, volume and tone they use.</li>
                    <li>Matching the way they dress, for example a salesman selling to farmers may be better off dressing in jeans and flannel shirt than a suit.</li>
                    <li>Copy someone's energy levels, don't go all energizer bunny on Tommy the tortoise.</li>
                </ol>
                <p>A final word on Mirroring and Matching is to make it subtle, people will see straight through you if you attempt to mirror everything. <a href="/business-communication-training-los-angeles.php">LA-based communcation classes</a>.</p>

                <h4>3. Be Empathic</h4>
                <p>Displaying empathy and demonstrating that your really care about someone is one of the best ways to build rapport. Remember to that person, the most important person in the whole world, is themselves. By showing that you care about them you make them feel special and important. Also read <a href="http://www.cc-sd.edu/blog/10-ways-of-showing-compassion">10 ways of showing compassion</a>.</p>

                <h4>4. Final take aways</h4>
                <p>Don't forget the basics of good communication. </p>
                <ul>
                    <li>Looking people in the eye.</li>
                    <li>Shaking their hand firmly (where culturally acceptable).</li>
                    <li>Smiling when you see them.</li>
                    <li><a href="/business-communication/active-listening.php">Being an active listener</a>.</li>
                    <li>Being sincere.</li>
                    <li>Giving someone your full attention.</li>
                </ul>
                <p>are all basic communication skills that lay a solid foundation for establishing rapport with someone. Also read <a href="https://books.google.com/books?id=NGiEGFmb_XEC&amp;printsec=frontcover&amp;dq=building+rapport&amp;hl=en&amp;sa=X&amp;ved=0ahUKEwj39P_lvsHMAhWCBSwKHbAXAe0Q6AEIMzAA#v=onepage&amp;q=building%20rapport&amp;f=false">Building Rapport with Dummies for Dummies</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Communication Lessons</h4>
                    <ul>
                        <li><a href="/business-communication/reading-body-language.php">Reading Body Language</a></li>
                        <li><a href="/business-communication/body-language-mistakes.php">Body Language Mistakes</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>