<?php
$meta_title = "Verbal Face-to-Face Communication Tips | Training Connection";
$meta_description = "5 useful tipcs how to commincate efectively when talking face to face with a colleague or a client. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Verbal Communication</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Good Face-to-Face Communication Tips</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Communication skills are important skills that help you deal well with other people at work and in your personal life. Being successful and happy at work, in your friendships and in your family relationships all depend on effective communication. </p>

                <p>Communication skills help you to:</p>
                <ul>
                    <li>Build better relationships</li>
                    <li>Learn about yourself</li>
                    <li>Learn about others</li>
                    <li>Solve problems</li>
                    <li>Develop new skills</li>
                    <li>Become more successful.</li>
                </ul>
                <p>Communication takes many forms and can be made up of spoken language (words), gestures and <a href="/business-communication/reading-body-language.php">facial expressions and body language</a>.
                    This article will provide you with useful tips on how to deliver effective face-to-face communication. </p>
                <p>For more details on <a href="/business-communication-training.php">our business communication workshops</a> please call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/verbal-communication.jpg" width="750" height="500" alt="Face to face communication tips"></p>
                <h4>1. Be clear in what you want to express</h4>

                <p>What is it that you want the person you are communicating with to understand? Is there a primary idea, concept or message? We often spent more time waffling and speaking about superficial matters rather than addressing the main concept we actually wish to communicate. To communicate effectively requires inner clarity and a clear understanding of what it is you wish to communicate. </p>
                <p><strong>Tip #1: Check in with yourself frequently to see what it is you want the most from the person or organization you are communicating with.

                    </strong></p>
                <h4>2. Choosing your words carefully</h4>
                <p>Expressing yourself effectively fully involves choosing your words precisely. Words carry huge meaning and sometimes carry meanings beyond their simple dictionary definitions. Words can also carry different meanings to different people. Words can be powerful metaphors that go beyond their "literal meaning". Certain words can be very emotive and provoke strong reactions. <a href="/business-communication-training-chicago.php">Chicago communication training classes</a>.</p>
                <p>When choosing words it is important to consider your audience. Who are they? What is their background? Communicating using words that best resonate with your audience will help you maintain good rapport with them and give them a more favorable opinion of you. This will greatly improve the outcome of what it is you are trying to communicate.</p>
                <p><strong>Tip #2: Understand your audience and communicate using words they can relate to.</strong></p>

                <h4>3. Controlling your voice</h4>
                <p>It is important to maintain good volume control. Talking too loudly can threaten listeners whilst talking too softly could annoy listeners if they can’t hear you and it can lead to misunderstanding. </p>
                <p><strong>Tips #3: Think of your volume as a thermometer. Too hot or too call is bad. Too loud or too soft is bad.</strong></p>


                <h4>4. Using tonality effectively</h4>
                <p>According to studies, the tone of voice we use is responsible for about 35-40 percent of the message we are sending. Tone involves not only the volume you use, but also the type of emotion you communicate with and the words you choose to emphasize. The emphasis you place on the word in any given sentence, will draw the listener’s attention, indicating importance. Emotion can also communicate how you feel, for example an excited tone can demonstrate that you are passionate about a project or a disappointed tone can show you are upset about an outcome. Also read <a href="https://books.google.com/books?id=_RjRnMP4204C&amp;pg=PT55&amp;dq=tonality+in+communication&amp;source=gbs_toc_r&amp;redir_esc=y#v=onepage&amp;q=tonality%20in%20communication&amp;f=false" target="_blank">Words, Tonality and Body Language</a>.</p>
                <p><strong>Tips #4: Displaying emotion when you communicate verbally improves your communication by up to 40%. </strong></p>

                <h4>5. Become an active listener</h4>
                <p>Effective communication needs to be two way communication. Even the best public speakers engage with their audience and get constant feedback. By talking too much, you run the risk of the other person tuning out if they become bored or finding you annoying. People like the sound of their own voice, that is human nature. A good communicator will engage in two way conversation and allow the other party an opportunity to talk. Whilst the other person is talking, they will actively listen to the other party. For more on <a href="/business-communication-training-los-angeles.php">LA Business Communication training</a>.</p>
                <p><strong>Tip #5: Effective communication is always two-ways. <a href="/business-communication/active-listening.php">Learn to become an active listener</a>.</strong> </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>