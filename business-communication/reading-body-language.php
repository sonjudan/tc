<?php
$meta_title = "How to Read Body Language | Training Connection";
$meta_description = "Learn how people communicate with their bodies. Postures, head positions, gestures and eye movements all convey information. Reading body language is an important skill covered in our Business Communication class, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reading Body Language</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Read Body Language</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>We are constantly reading the body language of others, even when we are not aware of it. Actively reading body language, however, will provide valuable insight and improve communication. Pay attention to the positions and movements of people around you. Specifically their head positions, physical gestures, and eyes. </p>
                <p>For more details on our <a href="/business-communication-training.php">body language communication training course</a> call us on 888.815.0604.	          </p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/reading-body-language.jpg" width="500" height="300" alt="Learning to read body language"></p>
                <h4>Head Position</h4>
                <p> The head is an obvious indicator of feelings and thoughts. The position of the head speaks volumes, making it the perfect place to start. While it takes practice to accurately interpret head position, the basic positions, and movements that are not extremely difficult to identify.</p>

                <h4>Movement and Position</h4>

                <ul>
                    <li><strong>Nodding:</strong> Nodding typically indicates agreement. The speed of the nod, however, indicates different things. A slow nod can be a sign of interest or a polite, fake signal. Look to other eyes for confirmation. A fast nod signals impatience with the speaker. </li>
                    <li><strong>Head up:</strong> This position indicates that the person is listening without bias.</li>
                    <li><strong>Head down:</strong> This position indicates disinterest or rejection for what is said. When done during an activity, it signals weakness or tiredness. </li>
                    <li><strong>Tilted to the side:</strong> This means a person is thoughtful or vulnerable. It can signal trust.</li>
                    <li><strong>Head high:</strong> Holding the head high signals confidence or feelings of superiority.</li>
                    <li><strong>Chin up:</strong> The chin up indicates defiance or confidence. </li>
                    <li><strong>Head forward: </strong>Facing someone directly indicates interest. It is a positive signal.</li>
                    <li><strong>Tilted down:</strong> Tilting the head down signals disapproval. </li>
                    <li><strong>Shaking:</strong> A shaking head indicates disagreement. The faster the shaking, the stronger the disagreement.</li>
                </ul>
                <p><a href="/business-communication-training-los-angeles.php">Los Angeles communication training workshops</a>.</p>
                <h4>Translating Gestures into Words</h4>
                <p>In many ways understanding body language is like learning a foreign language. There are a few tips that make learning any language, even a nonverbal one, easier.</p><h4>
                    Translations</h4>
                <ul>
                    <li><strong>Pointing finger</strong>: This is an aggressive movement. When a wink is added, however, it is a positive confirmation of an individual. </li>
                    <li><strong>Finger moves side to side</strong>: This motion acts as a warning to stop something. </li>
                    <li><strong>Finger moves up and down</strong>: This acts as a reprimand or places emphasis on what is said. </li>
                    <li><strong>Thumbs up</strong>: Thumbs up is a sign of approval.</li>
                    <li><strong>Thumbs down</strong>: This is a sign of disapproval. </li>
                    <li><strong>Touch index finger to thumb</strong>: The sign indicates OK.</li>
                </ul>

                <h4>Open Vs. Closed Body Language</h4>
                <p>Body language is often defined as open or closed. Being open or closed has many different causes. Open body language can come from passivity, aggression, acceptance, supplication, or relaxation. Closed body language may be caused by the desire to hide, self-protection, cold, or relaxation</p>
                <h4>Closed body language</h4>
                <ul>
                    <li><strong>Arms crossed: </strong>This stance is often defensive or hostile.</li>
                    <li><strong>Legs crossed when seated: </strong>Cross legs can indicate caution. One leg over the other at the knee may indicate stubbornness. </li>
                    <li><strong>Arm or object in front of the body: </strong>This can coincide with nervousness and is a form of self-protection. </li>
                    <li><strong>Legs crossed when standing:</strong> This may mean someone is insecure when combined with crossed arms. By itself, it can signal interest. </li>
                </ul>
                <h4>Open body language</h4>
                <ul>
                    <li><strong>Legs not crossed:</strong> This is an open, relaxed position.</li>
                    <li><strong>Arms not crossed:</strong> Open arms indicate openness; although the hands may indicate aggression, supplication, or insecurity, depending on their position. </li>
                </ul>
                <p><a href="/business-communication-training-chicago.php">Chicago-based Business Communication course</a>. </p>

                <h4>The Eyes Have It</h4>
                <p> People give a great deal away through their eyes. The eyes are an important factor when reading a person’s body language. When combined with body position, the eyes will provide a more accurate translation of body language. </p>
                <ul>
                    <li><strong>Looking to the left</strong>: Eyes in this direction can mean someone is remembering something. Combined with a downward look, it indicates the self-communication. When looking up, it means facts are being recalled. </li>
                    <li><strong>Sideways</strong>: Looking sideways means someone is conjuring sounds. Right, is associated with imagination, and may mean a story. Left is accessing memory. </li>
                    <li><strong>Looking to the right</strong>: Looks to the right indicates imagination. It can mean guessing or lying. Combined with looking down, it means there is a self-question. Combined with looking up, it can mean lying. </li>
                    <li><strong>Direct eye contact</strong>: When speaking, this means sincerity and honesty. When listening, it indicates interest.</li>
                    <li><strong>Wide eyes</strong>: Widening eyes signal interest.</li>
                    <li><strong>Rolled eyes</strong>: Rolled eyes mean frustration. They can be considered a sign of hostility.</li>
                    <li><strong>Blinking:</strong> Frequent blinking indicates excitement. Infrequent blinking signals a boredom or concentration, depending focus. </li>
                    <li><strong>Winking:</strong> A wink is a friendly gesture or secret joke. </li>
                    <li><strong>Rubbing eyes:</strong> Rubbing eyes may be caused by tiredness. It can also indicate disbelief or being disturbed.</li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Communication Lessons</h4>
                    <ul>
                        <li><a href="/business-communication/reading-body-language.php">Reading Body Language</a></li>
                        <li><a href="/business-communication/body-language-mistakes.php">Body Language Mistakes</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>