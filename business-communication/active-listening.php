<?php
$meta_title = "Active Listening Tips | Training Connection";
$meta_description = "aaActive listening techniques to help you become a better communicator. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angelesa";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Active Listening</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>What is Active Listening?</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Active listening is defined as a <a href="http://www.colorado.edu/conflict/peace/treatment/activel.htm">way of listening and responding to another person that improves mutual understanding</a>. Through active listening you are making a conscious effort to hear not only what the other person is saying but, more importantly, try to understand the complete message being sent.	       Active listening helps build rapport, understanding, and trust. </p><p>
                    For more details on <a href="/business-communication-training.php">our business communication workshops</a> please call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/active-listening.jpg" width="750" height="499" alt="Actively listening methods"></p>

                <h4>How to  Actively Listen</h4>
                <p>The first step is learning to pay <strong>full attention</strong> to the person you are talking to. You can't allow yourself to become distracted by  counter arguments or things going on around you. Have you ever met someone, who loves to  talk, but when it's your turn to talk, they seem  distracted and don't seem to be  listening. We often think of these people as self-centered, untrusting and even boring. It is very hard to get into rapport with someone who is unable to listen. <a href="/business-communication-training-chicago.php">Chicago-based business skills classes</a>.</p>
                <p>
                    <strong>Tip #1:</strong> A useful tip if you find it hard to concentrate is to mentally repeat the words you hear. This will keep you focused. </p>
                <p>The next step is to acknowledge what others are saying. This can be done by nodding your head and other facial expressions such as raising your eyebrows or smiling. Read more about how <a href="/business-communication/body-language-basics.php">body language</a> can be used to acknowledge that you are listening.</p><p>
                    <strong>Acknowledgment</strong> can also be through simple statements or questions such as "uh huh", "and then?" or "Really?". </p>
                <p><strong>Tip #2:</strong> Be careful not to nod too ferociously else it could be interpreted that you are in agreement with the other person, which may not necessarily be the case. Learn to avoid <a href="/business-communication/body-language-mistakes.php">common body language mistakes</a>.</p>

                <h4>Others Techniques used in Active Listening</h4>
                <h4>Summarizing</h4>
                <p>By briefly summarizing what you have heard, you can demonstrate you have been actively listening, for example: "So, if I understand you correctly...." or "What I'm hearing is...". </p>

                <h4>Probing</h4>
                <p>Probing questions can show that you are totally interested in what you are hearing, for example "what happened next" </p>

                <h4>Giving Feedback</h4>
                <p> Giving short feedback without interrupting. "I see what you mean" or "Sounds amazing."</p>

                <h4>Asking questions</h4>
                <p> Ask questions to clarify certain points "What do you mean when you say" or "is this what you mean?" but don't interrupt them unnecessarily. <a href="/business-communication-training-los-angeles.php">Communication and softskills training</a> in Los Angeles.</p>

                <h4>Limiting interruptions</h4>
                <p>Interruptions can be frustrating to the speaker and could limit your understanding of their message. Allow the speaker to finish each point before you interrupt them with a detailed question or a counter argument.</p>

                <h4>Validation</h4>
                <p>Acknowledging someone's issue or problem and displaying empathy, for example "I appreciate your willingness to talk about such a difficult issue. . ."</p>
                <h4>Key Take Aways</h4>
                <p>Actively listening will help you gain respect and trust from the person you are listening too. It will facilitate better and clearer understanding which in turn will result in stronger bonds and better relationships. Actively listening will help you probe and gain a deeper understanding of the issues, helping you avoid further issues, and improving your productivity.</p>

                <p>It takes concentration and effort to learn to become an active listener. But with a bit of practice and perseverance, this will become second nature, and it will greatly improve your overall communication skills!</p><p>
                    Also read <a href="/business-communication/body-language-mistakes.php">Why Actively Listening.</a> </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>