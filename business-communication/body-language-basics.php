<?php
$meta_title = "Body Language Basics | Training Connection";
$meta_description = "Learning body language is a huge competitive advantage. This and other topics are covered in our Business Communication training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-communication.php">Business Communication</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Title</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Body Language Basics</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>The ability to interpret body language is a skill that will enhance anyone’s career. Body language is a form of communication, and it needs to be practiced like any other form of communication. Whether in sales or management, it is essential to understand the body language of others and exactly what your own body is communicating.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-communication/body-language-messages.jpg" width="650" height="513" alt="Non verbal communication"></p>
                <h4>1. Communicating with Body Language</h4>
                <p>We are constantly communicating, even when we are not speaking. Unspoken communication makes up over half of what we tell others and they tell us. It affects our work and personal relationships. Improve negotiating, management, and interpersonal skills by correctly interpreting body language and important signals.</p>
                <p>For more details on <a class="time" href="/business-communication-training.php">training workshops we offer in body language and business communication</a> call us on 888.815.0604.</p>

                <h4>2. Learning a New Language</h4>
                <p>In many ways understanding body language is like learning a foreign language. There are a few tips that make learning any language, even a nonverbal one, easier. <a href="/business-communication-training-los-angeles.php">Communication skills training in LA</a>.</p>

                <h4>Tips</h4>
                <ul>
                    <li>Set Goals: Make sure that your goals are realistic and have specific timelines.</li>
                    <li>Devote time to learning: Schedule time to practice. Do not rely on spare time.</li>
                    <li>Practice daily: Hone skills by continued practice. </li>
                    <li>Enjoy the process: You are not in school. Relax and have fun with your new skill. </li>
                </ul>

                <h4>3. The Power of Body Language</h4>
                <p>Understanding body language does more than improve relationships. You will get insight into the thoughts and feelings of those around you. Because it is not a conscious form of communication, people betray themselves in their body language. Body language is powerful in several ways.</p>

                <ul>
                    <li>It is honest: Body language conveys truth, even when words do not.</li>
                    <li>Creates self-awareness: Understanding body language helps you identify your own actions that hinder success.</li>
                    <li>Understand feelings: Body language shows feelings and motive such as aggression, submission, deception, etc. Use these as cues to your communication. </li>
                    <li>Enhance listening and communication skills: Paying attention to body language makes someone a better listener. Hear between the words spoken to what is being said.</li>
                </ul>

                <h4>4. More than Words</h4>
                <p> Much of the way people communicate is nonverbal. Body language specifically focuses on physical, not tone, or pitch. It includes the following characteristics. </p>

                <ul>
                    <li>Proximity: The distance between people</li>
                    <li>Positioning: Position of a body</li>
                    <li>Facial expression: The eyes are particularly noticed.</li>
                    <li>Touching: This includes objects, people, and themselves.</li>
                    <li>Breathing: The rate of respiration is telling.</li>
                </ul>
                <p><a href="/business-communication-training-chicago.php">Business Communication training in Chicago</a>.</p>

                <h4>5. Actions Speak Louder than Words</h4>
                <p>Our impressions of each other are based on more than words. People can have cordial conversations and not like each other. The actions that we take are stronger than our words. For example, a person may dismiss someone using body language and not saying anything negative. Like it or not, our body language makes a lasting impression on the people around us. </p>
                <ul>
                    <li>Deception</li>
                    <li>Confidence</li>
                    <li>Nerves</li>
                    <li>Boredom</li>
                    <li>Emotions</li>
                    <li>Attraction</li>
                    <li>Being open</li>
                    <li>Being closed off</li>
                </ul>
                <p>Please note that this is not an exhaustive list of what body language can communicate.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Communication Lessons</h4>
                    <ul>
                        <li><a href="/business-communication/reading-body-language.php">Reading Body Language</a></li>
                        <li><a href="/business-communication/body-language-mistakes.php">Body Language Mistakes</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Communication training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Communication classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Business Communication class</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=24">Business Communication student testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>