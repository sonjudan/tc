<?php
function schema_educationEvent($schemas) {

    $output = "";

    foreach ($schemas as $schema) {
        $output .= '<script type="application/ld+json">{';
        $output .= '"@context": "http://schema.org", "@type": "EducationEvent",';
        $output .= '"name": "'. $schema['name'] .'",';
        $output .= '"url": "'. $schema['url'] .'",';
        $output .= '"startDate": "'. $schema['startDate'] .'",';
        $output .= '"endDate": "'. $schema['endDate'] .'",';
        $output .= '"description": "'. $schema['description'] .'",';
        $output .= '"image": "'. $schema['image'] .'",';
        $output .= '"performer": {"@type": "Organization","name": "training Connection"},';
        $output .= '"location": { "@type": "Place",';
        $output .= '"name": "'. $schema['location'] .'",';
        $output .= '"alternateName": "'. $schema['alternateName'] .'",';
        if($schema['location'] == 'Chicago'){
            $output .= '"address": {"@type": "PostalAddress","streetAddress": "230 W Monroe Street", "addressLocality": "Suite 610",  "addressRegion": "IL", "postalCode": "60606"}';
        }else {
            $output .= '"address": {"@type": "PostalAddress","streetAddress": "915 Wilshire Blvd", "addressLocality": "Suite 1800",  "addressRegion": "CA", "postalCode": "90017"}';
        }
        $output .= '},';
        $output .= '"offers": {"@type": "Offer",';
        $output .= '"url": "'. $schema['url'] .'",';
        $output .= '"validFrom": "'. $schema['endDate'] .'T14:50:08",';
        $output .= '"priceCurrency": "USD",';
        $output .= '"availability": "http://schema.org/InStock",';
        $output .= '"price": "'. $schema['price'] .'"';
        $output .= '}';
        $output .=  '}</script>';
    }
    return $output;
}