<?php
$meta_title = "Timetable | Chicago & Los Angeles";
$meta_description = "";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Chicago Timetable</li>
            </ol>
        </nav>


        <div class="page-intro mb-3">
            <div class=" intro-copy">
                <h1 data-aos="fade-up" >Chicago Timetable</h1>
                <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">230 W Monroe Street, Suite 610, Chicago CA 60606.</h3>

                <ul data-aos="fade-up" data-aos-delay="150" class="list-inline">
                    <li>Tel: <a href="#">(888) 815-0604</a></li>
                    <li>Fax: <a href="#">(866) 523-2138</a></li>
                    <li>Email: <a href="#">info@trainingconnection.com</a></li>
                </ul>

            </div>
        </div>


        <div class="timestable-section" data-aos="fade-up" data-aos-delay="250">

            <ul class="nav nav-timestable mb-3" id="timestable-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="timestable-adobe-tab" data-toggle="pill" href="#timestable-adobe" role="tab" aria-controls="timestable-adobe" aria-selected="true">
                        <img src="dist/images/icons/icon-adobe.svg" alt="Adobe Training">
                        <h4>Adobe <span>Training</span></h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="timestable-apple-tab" data-toggle="pill" href="#timestable-apple" role="tab" aria-controls="timestable-apple" aria-selected="false">
                        <img src="dist/images/icons/icon-apple.svg" alt="Apple">
                        <h4>Apple</h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="timestable-office-tab" data-toggle="pill" href="#timestable-office" role="tab" aria-controls="timestable-office" aria-selected="false">
                        <img src="dist/images/icons/icon-office.svg" alt="Microsoft Office">
                        <h4>Microsoft <span>Office</span></h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="timestable-web-tab" data-toggle="pill" href="#timestable-web" role="tab" aria-controls="timestable-web" aria-selected="false">
                        <img src="dist/images/icons/icon-coding.svg" alt="Web Development">
                        <h4>Web <span>Development</span></h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="timestable-business-tab" data-toggle="pill" href="#timestable-business" role="tab" aria-controls="timestable-business" aria-selected="false">
                        <img src="dist/images/icons/icon-business.svg" alt="Business Skills">
                        <h4>Business <span>Skills</span></h4>
                    </a>
                </li>
            </ul>


            <div class="tab-content" id="timestable-tabContent">
                <div class="tab-pane fade show active" id="timestable-adobe" role="tabpanel" aria-labelledby="timestable-adobe-tab">

                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-dark table-hover">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">May</th>
                                <th scope="col">June</th>
                                <th scope="col">July</th>
                                <th scope="col">August</th>
                                <th scope="col">September</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr">
                                <th scope="row">Acrobat DC Fundamentals</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>30–Oct 2</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">After Effects Fundamentals</th>
                                <td>13–15</td>
                                <td>24–26</td>
                                <td></td>
                                <td>12–14</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">After Effects Advanced</th>
                                <td>16–17</td>
                                <td>27–28</td>
                                <td></td>
                                <td>15–16</td>
                                <td>30–Oct 4</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">After Effects Bootcamp</th>
                                <td>13–17</td>
                                <td>24–28</td>
                                <td></td>
                                <td>12–16</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Captivate 2019 Fundamentals</th>
                                <td>6–7</td>
                                <td>24–25</td>
                                <td></td>
                                <td>26–27</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Captivate 2019 Advanced</th>
                                <td>8–9</td>
                                <td>26–27</td>
                                <td></td>
                                <td>28–29</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Dreamweaver 2019 Fundamentals</th>
                                <td></td>
                                <td></td>
                                <td>1–3</td>
                                <td></td>
                                <td>16</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Illustrator 2019 Quickstart</th>
                                <td>20</td>
                                <td></td>
                                <td>8</td>
                                <td>5</td>
                                <td>16–18</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Illustrator 2019 Fundamentals</th>
                                <td>20–22</td>
                                <td></td>
                                <td>8–10</td>
                                <td>5–7</td>
                                <td>19–20</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Illustrator 2019 Advanced</th>
                                <td></td>
                                <td></td>
                                <td>11–12</td>
                                <td>8–9</td>
                                <td>16–20</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Illustrator 2019 Bootcamp</th>
                                <td></td>
                                <td></td>
                                <td>8–12</td>
                                <td>5–9</td>
                                <td>9</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">InDesign 2019 Quick Start</th>
                                <td>6</td>
                                <td>17</td>
                                <td>29</td>
                                <td></td>
                                <td>9–11</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">InDesign 2019 Fundamentals</th>
                                <td>6–8</td>
                                <td>17–19</td>
                                <td>29–31</td>
                                <td></td>
                                <td>12–13</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">InDesign 2019 Advanced</th>
                                <td></td>
                                <td>20–21</td>
                                <td></td>
                                <td>1–2</td>
                                <td>9–13</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">InDesign 2019 Bootcamp</th>
                                <td>6–10</td>
                                <td>17–21</td>
                                <td>29–Aug 2</td>
                                <td></td>
                                <td>23</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Photoshop 2019 Quick Start</th>
                                <td></td>
                                <td>3</td>
                                <td>15</td>
                                <td>19-Aug</td>
                                <td>23–25</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Photoshop 2019 Fundamentals</th>
                                <td></td>
                                <td>3–5</td>
                                <td>15–17</td>
                                <td>19–21</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Photoshop 2019 Advanced</th>
                                <td></td>
                                <td>6–7</td>
                                <td>18–19</td>
                                <td>22–23</td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Photoshop 2019 Bootcamp</th>
                                <td></td>
                                <td>3–7</td>
                                <td>15–19</td>
                                <td>19–23</td>
                                <td>23–25</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Premiere Pro Fundamentals</th>
                                <td></td>
                                <td>10–12</td>
                                <td>22–24</td>
                                <td>19–21</td>
                                <td>26–27</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Premiere Pro Advanced</th>
                                <td>2–3</td>
                                <td>13–14</td>
                                <td>25–26</td>
                                <td>22–23</td>
                                <td>23–27</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Premiere Pro Bootcamp</th>
                                <td></td>
                                <td>10–14</td>
                                <td>22–26</td>
                                <td>19–23</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="timestable-apple" role="tabpanel" aria-labelledby="timestable-apple-tab">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-dark ">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">May</th>
                                <th scope="col">June</th>
                                <th scope="col">July</th>
                                <th scope="col">August</th>
                                <th scope="col">September</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr">
                                <th scope="row">Apple Course title</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Apple Course title 2</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="timestable-office" role="tabpanel" aria-labelledby="timestable-office-tab">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-dark">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">May</th>
                                <th scope="col">June</th>
                                <th scope="col">July</th>
                                <th scope="col">August</th>
                                <th scope="col">September</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr">
                                <th scope="row">Microsoft Office Course title</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Microsoft Office Course title 2</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="timestable-web" role="tabpanel" aria-labelledby="timestable-web-tab">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-dark">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">May</th>
                                <th scope="col">June</th>
                                <th scope="col">July</th>
                                <th scope="col">August</th>
                                <th scope="col">September</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr">
                                <th scope="row">Web Development title</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Web Development title 2</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="timestable-business" role="tabpanel" aria-labelledby="timestable-business-tab">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered table-dark">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">May</th>
                                <th scope="col">June</th>
                                <th scope="col">July</th>
                                <th scope="col">August</th>
                                <th scope="col">September</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr">
                                <th scope="row">Business Skills title</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            <tr class="tr">
                                <th scope="row">Business Skills title 2</th>
                                <td>1-4</td>
                                <td>5-10</td>
                                <td>23-29</td>
                                <td>27</td>
                                <td>30–Oct 2</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>




    </div>

</main>

<?php include_once 'sections/locations.php'; ?>
<?php include_once 'footer.php'; ?>