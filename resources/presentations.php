<?php
$meta_title = "Presentation Learning Resources | Training Connection";
$meta_description = "Articles on how to deliver better Presentations. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Presentation</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/courses/business-skills/presentation.jpg" alt="Presentation">
                    </div>
                    <h1 data-aos="fade-up" >Presentation</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information on how to improve the way you deliver a s presentation. We also run <a class="pres" href="/business-skills/presentation-training.php">presentation classes</a> in Chicago and Los Angeles.<br>
                        </p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/how-to-handle-nervousness.jpg" alt="Overcoming Nerves during a Presentation"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/nervousness.php">Overcoming Nerves during a Presentation</a>
                        </h3>
                        <p>Nervousness is normal when giving a presentation. After all, public  speaking is the top fear in the top ten lists of fears. Nervousness can  strike at different points in a presentation.
                            <a class="post-link" href="/presentations/nervousness.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/presentation.jpg" alt="Using PowerPoint during a Presentation"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/powerpoint.php">Using PowerPoint during a Presentation</a>
                        </h3>
                        <p>Microsoft PowerPoint is a commanding tool for creating visual screens  for a presentation. Visuals created in PowerPoint and projected on a  screen are often easier to see in a large room than information  displayed on a flip chart. Using PowerPoint offers the following  benefits.
                            <a class="post-link" href="/presentations/powerpoint.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/engaging-presentations.jpg" alt="Delivering Engaging Presentations"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/engagement.php">Delivering Engaging Presentations</a>
                        </h3>
                        <p>Bringing your presentation to the next level is something you can  accomplish by  adding some  little touches that will produce a lot of  value during your presentation.
                            <a class="post-link" href="/presentations/engagement.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/presentation-4.jpg" alt="Pause before you Present"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/pause-present.php">Pause before you Present</a>
                        </h3>
                        <p>Watch any game show and you will see that the pressure is on for contestants to articulate their best answer within 30 seconds. While it is not the best practice for corporate communication, this expectation follows many people into the real world.
                            <a class="post-link" href="/presentations/pause-present.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/presentation-5.jpg" alt="Using Visuals to deliver Engaging Presentations"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/dynamic-presentations-part-a.php">Using Visuals to deliver Engaging Presentations</a>
                        </h3>
                        <p>How can we make presentations dynamic, memorable and practical? In this  article, I can show you how you can engage all the senses of a live  audience to capture, keep and ensure your corporate audiences come away  with something they can use.
                            <a class="post-link" href="/presentations/dynamic-presentations-part-a.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/presentation-2.jpg" alt="Using Sounds to deliver Engaging Presentations"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/dynamic-presentations-part-b.php">Using Sounds to deliver Engaging Presentations</a>
                        </h3>
                        <p>Sound is slightly slower than the speed of light therefore it follows  my Part 1 - Using Visuals well. Quality sound can make or break any recorded or live  presentation. It even has biological effects on the body and can affect hormones that control stress and focus.
                            <a class="post-link" href="/presentations/dynamic-presentations-part-b.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/presentation.jpg" alt="Using Movement to deliver Engaging Presentations"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/presentations/dynamic-presentations-part-c.php">Using Movement to deliver Engaging Presentations</a>
                        </h3>
                        <p>When I train, it is important for me to play with proximity, energy of the room, and physical simulations so that the experience is memorable and engages the whole body. After reading Brain Rules by <strong>DR. JOHN J. MEDINA</strong>, a developmental molecular biologist, I was further convinced that getting learners  to move around the room affected learning just as much as bringing well organized informationto the training session.
                            <a class="post-link" href="/presentations/dynamic-presentations-part-c.php">Read more</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-presentation mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>