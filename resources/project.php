<?php
$meta_title = "Microsoft Project Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Project. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-project">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-project.png" alt="Microsoft Project">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Project</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer <a href="/ms-project-training.php">training on Microsoft Project</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Microsoft Project Interface"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/interface.php">Microsoft Project Interface</a></h3>
                        <p>The default view in MS Project is the <em>Gantt Chart</em> view. This consists of  data table on the left hand side of the screen and a Gantt bar chart on the right. The Divider Bar separates both sections and it can be moved to make each side bigger. The Gantt table looks like an Excel worksheet, consiting of  rows, columns and cells. The Gantt bar chart graphically displays your project schedule.
                            <a class="post-link" href="/project/lessons/interface.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Starting your First Project in Microsoft Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/first-project.php">Starting your First Project in Microsoft Project</a></h3>
                        <p>In this article,  you’ll get started using Microsoft Project by creating a basic project and setting up the project schedule. You’ll add tasks to your project and set constraints on those tasks to further customize your project.
                            <a class="post-link" href="/project/lessons/first-project.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Adding Tasks in Microsoft Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/adding-tasks.php">Adding Tasks in Microsoft Project</a></h3>
                        <p>In this article,  we'll delve a little deeper into understanding tasks.  Project introduces manually scheduled tasks. You can also schedule  tasks automatically using the Project scheduling engine. We’ll discuss  the key terms for understanding tasks in this module. We’ll also learn  how to view task information and sort and filter tasks.
                            <a class="post-link" href="/project/lessons/adding-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Adding Resources in Microsoft Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/adding-resources.php">Adding Resources in Microsoft Project</a></h3>
                        <p>This article  introduces you to adding resources in MS Project.  Resources are the people, equipment, and materials needed to complete  your project. This module will give an overview of how resources are  used in Project 2010. You’ll learn how to add resources and view  resource information. You’ll also learn how to assign resources to  tasks.
                            <a class="post-link" href="/project/lessons/adding-resources.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Working with Costs"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/costs.php">Working with Costs</a></h3>
                        <p>In order for Project to successfully calculate costs for a project, you must enter accurate information, including varying pay rates. A work resource has a standard pay rate and an overtime rate. You can also enter specific pay rates for a certain day or for an assignment. In this module, we’ll look at resource pay rates, including material resource consumption rates.
                            <a class="post-link" href="/project/lessons/costs.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Working with Multiple Projects"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/multiple-projects.php">Working with Multiple Projects</a></h3>
                        <p>This module explains how to handle multiple projects. You’ll learn how to create links between projects. Since working with a single file is always faster if you can help it, this module will also explain how to consolidate projects. You’ll learn how to view multiple project critical paths and consolidated project statistics. Finally, this module explains how to create a resource pool.
                            <a class="post-link" href="/project/lessons/multiple-projects.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Working with Gantt Charts"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/gantt-charts.php">Working with Gantt Charts</a></h3>
                        <p>The Gantt Chart is a horizontal bar chart that represents each task in the time scale of the project. Each task entered in the project will be shown and by default, the name of the resource allocated to the task appears next to the bar.
                            <a class="post-link" href="/project/lessons/gantt-charts.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Adding Gantt Bars"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/gantt-bars.php">Adding Gantt Bars</a></h3>
                        <p>If you want to spotlight a particular task <strong>category</strong> that is not represented by its own Gantt bar, you can create a new Gantt bar. For example, you can create a Gantt bar to show available slack or to call attention to delayed tasks.
                            <a class="post-link" href="/project/lessons/gantt-bars.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Editing Tasks on a Gantt Chart"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/edit-tasks.php">Editing Tasks on a Gantt Chart</a></h3>
                        <p>One method of editing tasks is to change them on the Gantt Chart using the mouse and dragging. Positioning the pointer at the beginning of a bar will change the pointer to a % sign and dragging with the mouse to the left will update the percentage completed of the task.
                            <a class="post-link" href="/project/lessons/edit-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Network Diagrams in Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/network-diagrams.php">Network Diagrams in Project</a></h3>
                        <p>To see the Network Diagram, on the View tab of the Ribbon click Network Diagram. The Descriptive Network Diagram view is identical to the  Network Diagram view, except for the size, and the detail of the boxes  that represent tasks. The boxes on the Descriptive Network Diagram view  are larger and can contain labels for the data elements in the box.  These larger boxes take up more space, and thus fewer boxes fit on a  printed page.
                            <a class="post-link" href="/project/lessons/network-diagrams.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Using Tables"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/tables.php">Using Tables</a></h3>
                        <p>Much of the data the system holds can be entered and/or viewed in a table format. Project has predefined sets of columns (called tables)  which display specific information. To apply a different table to a  sheet view, click the View tab, click Tables, and then select the table  you want to apply.
                            <a class="post-link" href="/project/lessons/tables.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Applying Filters"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/filters.php">Applying Filters</a></h3>
                        <p>A filter is used to screen out unwanted tasks for a particular view to  identify a particular aspect of the current state of the project. For  example the filter can be set to show the tasks that make up the  Critical Path.
                            <a class="post-link" href="/project/lessons/filters.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Assigning Resources to Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/assigning-resources.php">Assigning Resources to Tasks</a></h3>
                        <p>In this article we will show you step-by-step how to add resources to tasks in MS Project's Auto Schedule Mode.
                            <a class="post-link" href="/project/lessons/assigning-resources.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Resources Leveling"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/resource-leveling.php">Resources Leveling</a></h3>
                        <p>In this article we take you through 3 examples of how to level  resources in  MS Project. Start by looking at the project in the Resource Usage view.
                            <a class="post-link" href="/project/lessons/resource-leveling.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Task durations in Auto Schedule mode"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/task-duration.php">Task durations in Auto Schedule mode</a></h3>
                        <p>This article discusses how Microsoft Project recalculates task durations when in Auto Schedule mode.
                            <a class="post-link" href="/project/lessons/task-duration.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Managing Tasks in MS Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/managing-tasks.php">Managing Tasks in MS Project</a></h3>
                        <p>In this article,  will delve a little deeper into managing tasks within Projects. We will learn how to create recurring tasks and import tasks from Microsoft Outlook.
                            <a class="post-link" href="/project/lessons/managing-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="osoft Project"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/creating-a-timeline.php">Creating a Timeline in Microsoft Project</a></h3>
                        <p>In this article,  we'll delve a little deeper into how to create a timeline within Projects. In this article, we will learn how to show and hide the timeline, customize timeline tasks, change the font for individual timeline items, modify global text styles for the timeline and copy a timeline.
                            <a class="post-link" href="/project/lessons/creating-a-timeline.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Creating Milestones"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/creating-milestones.php">Creating Milestones</a></h3>
                        <p>A milestone indicates a significant event in the lifetime of a project.  For example, the testing of a prototype could be considered a milestone  in the sample project that we have been working with. Typically  milestones are zero days in duration. There can also be multiple  milestones for each project.
                            <a class="post-link" href="/project/lessons/creating-milestones.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Changing the Task Calendar"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/changing-task-calendar.php">Changing the Task Calendar</a></h3>
                        <p>Tasks can have different calendar types applied to them. This is useful  if you would like to prioritize tasks or assign them to a specific  shift. To adjust a task’s calendar type, first open the Task Information  dialog for the task in question.
                            <a class="post-link" href="/project/lessons/changing-task-calendar.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Inactivating Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/inactivating-tasks.php">Inactivating Tasks</a></h3>
                        <p>Tasks in Project may be marked as inactive to prevent them from having  an effect on the schedule or resource availability without having to  delete them entirely. For example, you could mark a task as inactive if  you cannot complete it now, but you may be able to later in the project.
                            <a class="post-link" href="/project/lessons/inactivating-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Fixed Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/fixed-tasks.php">Fixed Tasks</a></h3>
                        <p>There are three types of tasks in Project: Fixed Duration, Fixed Units,  and Fixed Work. Fixed Duration tasks have a fixed duration that will not  change, Fixed Unit tasks will have constant fixed units, and Fixed  Work tasks will have the work remain constant.
                            <a class="post-link" href="/project/lessons/fixed-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Adding a Hyperlink to a Task"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/adding-hyperlink-to-task.php">Adding a Hyperlink to a Task</a></h3>
                        <p>In Microsoft Project, you have ability to link tasks to information  available elsewhere, such as an external file or an Internet site. There  are two ways to go about this, each of which implements the link  differently.
                            <a class="post-link" href="/project/lessons/adding-hyperlink-to-task.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Splitting and Overlapping Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/splitting-overlapping-tasks.php">Splitting and Overlapping Tasks</a></h3>
                        <p>When you split a task, you break it into two or more separate parts so  that parts of a task can start earlier or later than others. Most  typically, a task is split when only part of a task is going to be  delayed.
                            <a class="post-link" href="/project/lessons/splitting-overlapping-tasks.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Delaying Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/delaying-tasks-assigning-variables.php">Delaying Tasks</a></h3>
                        <p>In this article, we will delve a little deeper into how to delay tasks  as well as assign variable units to a task in MS Project. This multi-purpose guide takes a step-by-step approach to task management within  Microsoft Project.
                            <a class="post-link" href="/project/lessons/delaying-tasks-assigning-variables.php">Read more</a>.
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="Adding Custom Fields to a Task "></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/project/lessons/adding-custom-fields-to-task.php">Adding Custom Fields to a Task </a></h3>
                        <p>In this article, we will take a look at how to add Custom Fields to a  Task within MS Project. This article features easy to follow steps  which will guide you through adding Custom Fields to an MS Project Task.
                            <a class="post-link" href="/project/lessons/adding-custom-fields-to-task.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-project mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>