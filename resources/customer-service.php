<?php
$meta_title = "Customer Service Learning Resources | Training Connection";
$meta_description = "Learning articles on how to deliver better service to your customers. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Customer Service</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-business-tip.png" alt="Customer Service">
                    </div>
                    <h1 data-aos="fade-up" >Customer Service</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful informational articles on how to improve the way you service your customers. We also run <a href="/customer-service-training.php">Customer Service classes</a> in Chicago and Los Angeles.<br>
                        </p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="Great Customer Service over the Phone"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/phone-service.php">Great Customer Service over the Phone</a>
                        </h3>
                        <p>When you are talking to someone in person, body language makes up a  large part (some would say more than half) of your message. But as soon  as you pick up the phone, body language becomes irrelevant. The success  of your interactions depends almost entirely on your tone of voice and  your choice of words.
                            <a class="post-link" href="/customer-service/phone-service.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="Delivering Great In-Person  Service"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/in-person-service.php">Delivering Great In-Person  Service</a>
                        </h3>
                        <p>In-person interactions provide a great opportunity to build rapport with  customers. When you talk to a customer on the phone or you exchange  emails with a customer, it can be difficult sometimes to get a sense of  what the other person is thinking and feeling. But when you talk to a  customer in person, you get constant feedback, both verbal and  nonverbal. It’s easy to tell if you are creating the right impression.  Although in-person interactions can be difficult at times, they offer  exceptional insight into what customers want and need.
                            <a class="post-link" href="/customer-service/in-person-service.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="Great Email Customer Service"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/online-service.php">Great Email Customer Service</a>
                        </h3>
                        <p>A growing number of customer interactions are taking place online.  Younger people in particular prefer to do too much of their business  online rather than in person. But online interactions have limitations.  To provide excellent customer service online, you need to understand  what works and what doesn’t work, and how to make the most of the tools  that are available to you.
                            <a class="post-link" href="/customer-service/online-service.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="Reducing Physical Fatigue In Customer Service"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/physical-fatigue.php">Reducing Physical Fatigue In Customer Service</a>
                        </h3>
                        <p>While it would be easy to blame the customers for how weary you feel  after handling irate customers all day, there are other factors that can  make one less friendly or efficient in Customer Service. In this  article, we will cover how activity level can limit your ability to  enjoy a strong and energetic body throughout your day and in your  personal time.
                            <a class="post-link" href="/customer-service/physical-fatigue.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="The Groupon Effect on Customer Service"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/groupon.php">The Groupon Effect on Customer Service</a>
                        </h3>
                        <p> Groupon is a name that can make or break your business. You can be a  small or large business who wants to expand your customer base but if  you are not prepared to make way for new customers, your business will  be well known but not for the better. Refreshing customer service skills  and experience will be critical to your management of such  instantaneous demand.
                            <a class="post-link" href="/customer-service/groupon.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="3 Ways to Start Positive in Customer Service"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/positive-service.php">3 Ways to Start Positive in Customer Service</a>
                        </h3>
                        <p>When a customer calls about their experience with your business, it is more likely that it is to fix a problem than to compliment you on your service. We are all used to hearing “The Customer is Always Right.” Still, the point of this saying should not be misunderstood. In my classes, I help service agents understand how even if the customer is not always right- they are always important. How important you make your customers feel can be the difference between a viral complaint and a loyal following.
                            <a class="post-link" href="/customer-service/positive-service.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="5 Words Customers Do Not Want to Hear"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/words.php">5 Words Customers Do Not Want to Hear</a>
                        </h3>
                        <p>A customer can start out angry or can start calm and later become angry  yet when you interface either in person or by phone, your role as a  customer service agent is to provide solutions and ameliorate relations.  Often, this is lost in translation when we use words that put a  customer in a defensive state.
                            <a class="post-link" href="/customer-service/words.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="How to Greet Customers"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/greeting.php">How to Greet Customers</a>
                        </h3>
                        <p>Unlike an unresponsive touch screen or automated message system, you have the power to humanize your company’s service and redirect customer relations in a winning direction when interacting with a live customer. In today’s article, cover how you can improve the impression you make when you greet your customer.
                            <a class="post-link" href="/customer-service/greeting.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-business-tip.png" alt="Brand Identity through Customer Service Scripting"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/customer-service/brand-identity.php">Brand Identity through Customer Service Scripting</a>
                        </h3>
                        <p>A recent Ritz-Carlton Case Study reveals how standardizing service language across multiple corporate branches and locations can set a business apart from other businesses by setting an expectation for higher quality communication from service personnel. Â&nbsp;Similar to updating a brand spokesperson or logo look and typography, updating customer service language of your business is also key to connecting with the times.
                            <a class="post-link" href="/customer-service/brand-identity.php">Read more.</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-Ps mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>

