<?php
$meta_title = "Microsoft Word Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Word. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Word</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Microsoft Word">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Word</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer  <a class="word" href="/ms-word-training.php">training on Microsoft Word</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Starting a Document in Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/opening-doc.php">Starting a Document in Word</a></h3>
                        <p>Before you can begin using the many features of Microsoft Word 2013, you  need to know several basic skills and concepts. Identifying word  processing functions and Word screen components, creating and opening a  document, and moving around within a document are the foundations you  will build on in this course.
                            <a class="post-link" href="/word/lessons/opening-doc.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Starting a Document in Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/creating-doc.php">Starting a Document in Word</a></h3>
                        <p>Word lets you begin working in documents in two ways: by typing into a new, blank document, or by <em>opening</em> an existing document. New and Open commands are reached using the  mouse method of navigating, by clicking the File Ribbon Tab first.
                            <a class="post-link" href="/word/lessons/creating-doc.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Moving the Cursor Point"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/moving-cursor.php">Moving the Cursor Point</a></h3>
                        <p>The text you type appears wherever the<em> insertion/cursor point</em> is positioned in the current document. Moving the insertion point around  the screen is a critical part of creating and editing documents. To  move the insertion point around the screen, you can use either the  keyboard or the mouse.
                            <a class="post-link" href="/word/lessons/moving-cursor.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Modifying Page Breaks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/page-breaks.php">Modifying Page Breaks</a></h3>
                        <p>Word automatically determines where each page of a document should begin and end by inserting <em>automatic page breaks</em>.  As you create and edit multipage documents, you might find that you  need to manipulate the automatic page breaks. You can quickly create a  manual (forced) page break by clicking the <strong>Page Break button in the Pages group on the Insert tab</strong>.
                            <a class="post-link" href="/word/lessons/page-breaks.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Selecting Text in Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/selecting-text.php">Selecting Text in Word</a></h3>
                        <p>Before executing many Word commands, you need to highlight, or <em>select</em>,  the section of text to which a command applies. When you select text,  all punctuation, blank lines, and special characters within the  highlighted area are included.
                            <a class="post-link" href="/word/lessons/selecting-text.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Moving and Copying Text"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/moving-copying-text.php">Moving and Copying Text</a></h3>
                        <p>Word enables you to <em>cut</em> and <em>paste</em> blocks of text from  one part of a document to another. When you cut a selected portion of  text, the text is removed from the document and placed on the <em>Clipboard</em>,  a temporary storage area. When you paste text, a copy of the text on  the Clipboard is placed into the document. The cut piece of text remains  on the Clipboard until another block of text is placed on the Clipboard  or until you shut down your computer.
                            <a class="post-link" href="/word/lessons/moving-copying-text.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Sorting in Microsoft Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/sorting.php">Sorting in Microsoft Word</a></h3>
                        <p>In a document, you might need to arrange a list of single line items, a group of multiple line paragraphs, or the rows of items in a table into alphabetical or numerical order. Word lets you <em>sort</em> lines and paragraphs of document text and rows of table information into logically defined sequences.
                            <a class="post-link" href="/word/lessons/sorting.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Calculating in Word Tables"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/calculating.php">Calculating in Word Tables</a></h3>
                        <p>Word lets you perform calculations on numerical table data and display the results in the table. For example, you can add a row or column of numbers, or you can add, subtract, multiply, or divide the contents of individual cells. Entering calculations into a table, instead of typing in the results, ensures that any changes you make to the table’s data are automatically reflected in the calculated results whenever you update the table.
                            <a class="post-link" href="/word/lessons/calculating.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Protecting Word documents"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/protecting.php">Protecting Word documents</a></h3>
                        <p>You can protect a document from being viewed by an unauthorized user, by using a password, which combine upper and lowercase letters, numbers, and symbols. Important note: always use a password that you can remember so that you don't have to write it down.
                            <a class="post-link" href="/word/lessons/protecting.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Tracking Changes in Microsoft Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/tracking-changes.php">Tracking Changes in Microsoft Word</a></h3>
                        <p>In this article, we will learn how to track changes made to your document and navigate through tracked changes. When change tracking is enabled, Word will keep track of the changes  that are made. This includes text insertions, deletions, new images, and  more.
                            <a class="post-link" href="/lessons/tracking-changes.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Tracking Changes in Microsoft Word - Part B"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/tracking-changes-2.php">Tracking Changes in Microsoft Word - Part B</a></h3>
                        <p>In this article, we will learn how to
                            <a class="post-link" href="/lessons/tracking-changes-2.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Creating Templates"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/creating-templates.php">Creating Templates</a></h3>
                        <p>Templates allow you to create a boilerplate document that can be used  over and over. For example, if you look at some of the pre-built  templates included in Microsoft Word, you will see letters, reports,  flyers, and many more document types that you can customize.
                            <a class="post-link" href="/word/lessons/creating-templates.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Working with Multiple Documents in Microsoft Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/multiple-documents.php">Working with Multiple Documents in Microsoft Word</a></h3>
                        <p>In this Module, we will learn how to work with versions, compare and combine documents.
                            <a class="post-link" href="/word/lessons/multiple-documents.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Adding Comments in Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/comments.php">Adding Comments in Word</a></h3>
                        <p>To insert a comment, first select the text that you would like to  comment on. If you want to comment on just one word, you can place your  cursor anywhere inside that word.
                            <a class="post-link" href="/word/lessons/comments.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Changing Styles in Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/changing-styles.php">Changing Styles in Word</a></h3>
                        <p>In Word 2013 style sets are displayed in a gallery, rather than being  hidden in a drop-down command as they were in previous editions. Click  to open the Design tab and you will see this gallery displayed in the  Document Formatting group on the ribbon.
                            <a class="post-link" href="/word/lessons/changing-styles.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Changing the Font Scheme"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/changing-font-scheme.php">Changing the Font Scheme</a></h3>
                        <p>Just as you can change the colors used by the applied style set, you can also change its fonts.
                            <a class="post-link" href="/word/lessons/changing-font-scheme.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Creating Styles"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/creating-styles.php">Creating Styles</a></h3>
                        <p>One of the quickest and simplest ways to create your own custom style is  to base it off of formatting that has been used for existing text in  your document. Use your cursor to highlight the title of the current  sample document.
                            <a class="post-link" href="/word/lessons/creating-styles.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Managing Styles in MS Word"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/managing-styles.php">Managing Styles in MS Word</a></h3>
                        <p>To manage the various styles that exist within your document, first open  the Styles task pane (click Home -&gt; Styles). Next, at the bottom of  the pane, click Manage Styles.
                            <a class="post-link" href="/word/lessons/managing-styles.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Quick Style Gallery and Style Inspector"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/word/lessons/quick-style-gallery-style-inspector.php">Quick Style Gallery and Style Inspector</a></h3>
                        <p>One of the great features about the Quick Style gallery is its  customizability; this includes the ability to remove any styles you do  not use or need. On the Home tab, remove the Module 9 style from the  style gallery by right-clicking on it and clicking Remove from Quick  Style Gallery.
                            <a class="post-link" href="/word/lessons/quick-style-gallery-style-inspector.php">Read more</a>.
                        </p>
                    </div>
                </article>


            </div>
        </div>

    </main>


    <div class="section-widget g-text-word mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>