<?php
$meta_title = "Business Writing Learning Resources | Training Connection";
$meta_description = "Learning articles for better Business Writing. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Business Writing</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/courses/business-skills/writing-thumb.jpg" alt="Business Writing">
                    </div>
                    <h1 data-aos="fade-up" >Business Writing</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information on how to improve your written communicate. We also run <a  href="/business-writing-class.php">business writing classes</a> in Chicago and Los Angeles.<br>
                        </p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/spelling-grammar-tips.jpg" alt="Spelling and Grammar Tips"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-writing/basics.php">Spelling and Grammar Tips</a>
                        </h3>
                        <p>The building blocks of any writing, whether for business or social purposes, are words. Failure to use words properly can affect the overall impact of your prose. In this article we will discuss the spelling  and grammar issues in writing.
                            <a class="post-link" href="/business-writing/basics.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/write-business-proposal.jpg" alt="How to Write a Business Proposal"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-writing/proposals.php">How to Write a Business Proposal</a>
                        </h3>
                        <p>It is not just in face-to-face interactions that we have to put our best  foot forward. The same can be said in written correspondence, more  particularly when you are submitting a business proposal. In this  module, we will discuss the basic structure of a proposal, how to  select a proposal format, and tips on writing a proposal.
                            <a class="post-link" href="/business-writing/proposals.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/writing-4.jpg" alt="How to Write a Business Report"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-writing/reports.php">How to Write a Business Report</a>
                        </h3>
                        <p>Documentation is important in business. Sometimes documentation is the  only way supervisors can monitor the company’s quality of work. At other  times, documentation is the key to spotting best and worst practices. In this article we will discuss the basic structure of  reports, how to choose the right format, and tips on writing reports.
                            <a class="post-link" href="/business-writing/reports.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/constructing-sentences.jpg" alt="Constructing Sentences"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-writing/sentences.php">Constructing Sentences</a>
                        </h3>
                        <p>This article will discuss the parts of a sentence, its proper punctuation, and the four kinds of sentences.
                            <a class="post-link" href="/business-writing/sentences.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/effective-email.jpg" alt="Fast and Effective Emails"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-writing/effective-emails.php">Fast and Effective Emails</a>
                        </h3>
                        <p>Email writing was intended for faster communication. How fast can you  make your point? Considering your reader’s experience with email can  make the difference of whether a person takes action when receiving your  message. In this article, we research and adapt for an audience always  on the go!
                            <a class="post-link" href="/business-writing/effective-emails.php">Read more</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-bs-bw mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>