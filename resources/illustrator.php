<?php
$meta_title = "Adobe After Illustrator Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Adobe Illustrator. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php/">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Illustrator</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-adobe-Ai.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 class="" data-aos="fade-up" >Adobe Illustrator</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ai.png" alt="Drawing smooth lines with Adobe Illustrator"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/illustrator/lessons/smooth-curves.php">Drawing smooth lines with Adobe Illustrator</a>
                        </h3>
                        <p>How many times have you looked at the curves of a logo or professional typography and wondered  how	they achieved those impossibly smooth curves. If you are a natural born artist and you can sketch or paint  your already ahead of the game. What if you are not, and drawing simple curves seems like a hassle? The following are some simple and powerful ways to keep your lines smooth and beautiful!
                            <a class="post-link" href="/illustrator/lessons/smooth-curves.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ai.png" alt="Pattern Madness In Adobe Illustrator"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/illustrator/lessons/patterns.php">Pattern Madness In Adobe Illustrator</a>
                        </h3>
                        <p>It wasn't that long ago that creating patterns was a tedious affair. There were always little applications that could speed things up, but most did not give you adequate creative control. Most pattern creation software was raster or pixel based, which produced some nice patterns, but you were limited if you wanted to scale those patterns. Fast forward to the present and we have some great (and easy) pattern creation features	in Adobe Illustrator. Coupled with the vector power of Adobe  the pattern maker in Illustrator is a	wonderful addition to what is already an amazing application.
                            <a class="post-link" href="/illustrator/lessons/patterns.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-Ai mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>