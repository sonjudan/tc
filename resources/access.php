<?php
$meta_title = "Microsoft Excel Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Excel. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-access">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Access</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-access.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Access</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer  <a class="access" href="/access-training.php">training on Microsoft Access</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="An Introduction to Access Databases"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/introduction-to-databases.php">An Introduction to Access Databases</a></h3>
                        <p>Access is a relational database. A relational database is a collection of data items organized as a set of tables. In this lesson, you’ll get a chance to familiarize yourself with the basic components of the database. First, we’ll look at some common database terms. You’ll also take a closer look at the Navigation pane. We’ll introduce tables, table relationships, queries, forms, and reports.
                            <a class="post-link" href="/access/lessons/introduction-to-databases.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Performing Basic Access Database Tasks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/basic-tasks.php">Performing Basic Access Database Tasks</a></h3>
                        <p>This lesson focuses on tables. You’ll learn how to enter and edit data in the Datasheet view of a table. Next, we’ll look at using the clipboard to cut, copy, and paste data to and from different fields.
                            <a class="post-link" href="/access/lessons/basic-tasks.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Formatting Text in Access Databases"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/formatting-text.php">Formatting Text in Access Databases</a></h3>
                        <p>There are several options for formatting your text in the Datasheet. To change the font face and size using the Ribbon tools, use the following procedure.
                            <a class="post-link" href="/access/lessons/formatting-text.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Getting Started in Access"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/getting-started.php">Getting Started in Access</a></h3>
                        <p>This lesson is for students new to Access 2013, and focuses on getting started with Access 2013, signing into a Microsoft account, creating, saving, and opening a database.
                            <a class="post-link" href="/access/lessons/getting-started.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Access Database Terminology"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/database-terms.php">Access Database Terminology</a></h3>
                        <p>There is specific terminology that is used to refer to the basic elements that databases are comprised of. Let's examine the most commonly used database terminology.
                            <a class="post-link" href="/access/lessons/database-terms.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Access Database Relationships"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/database-relationships.php">Access Database Relationships</a></h3>
                        <p>Relationships define a database. The power of a database lies in the way the tables are related to each other. Nearly every database that contains multiple tables also includes relationships between these different sets of information. There are three types of table relationships: one-to-many, many-to-many, and one-to-one. Let's learn about each type.
                            <a class="post-link" href="/access/lessons/database-relationships.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Working with Tables"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/working-with-tables.php">Working with Tables</a></h3>
                        <p>In this Article, we will learn how to create a table and use table views.
                            <a class="post-link" href="/access/lessons/working-with-tables.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Working with Records"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/working-with-records.php">Working with Records</a></h3>
                        <p>To add records to a table, you have the option of entering them manually, using a form, or using the import tools on the External Data tab. As the database we are using does not have a form for it yet and we do not have external data we can import into it, we will add records manually.
                            <a class="post-link" href="/access/lessons/working-with-records.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Open Database Objects"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/opening-database-objects.php">Open Database Objects</a></h3>
                        <p>In this Article, we will learn how to open database objects in Microsoft Access. To open any objects that are listed inside the Navigation Pane, double-click on their listing.
                            <a class="post-link" href="/access/lessons/opening-database-objects.php">Read more</a>.</p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Managing Objects"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/managing-objects.php">Managing Objects</a></h3>
                        <p>In this Article, we will learn how to manage objects in Microsoft Access. There are a number of ways you can manage your objects using the Navigation Pane in Access 2013. All of these ways may be accessed by right-clicking on the object you would like to work with.
                            <a class="post-link" href="/access/lessons/managing-objects.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-access.png" alt="Formatting Tables in MS Access - Part A"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/access/lessons/formatting-tables-part-a.php">Formatting Tables in MS Access - Part A</a></h3>
                        <p>In this Article, we will learn how to select data and change the height and width of rows and columns. You may select data in a table in a number of different ways
                            <a class="post-link" href="/access/lessons/formatting-tables-part-a.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-access mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>