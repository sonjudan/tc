<?php
$meta_title = "Web Development Learning Resources | Training Connection";
$meta_description = "HTML, CSS and JavaScript learning articles. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Resources</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Web Development</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="Business Etiquette">
                    </div>
                    <h1 data-aos="fade-up" >Web Development</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful informational articles , including tips and tricks, how-to articles etc. Content is updated on a weekly basis.</p>
                    </div>

                </div>
            </div>


            <div class="tab-section" data-aos="fade-up" data-aos-delay="250">

                <ul class="nav nav-timestable mb-3" id="timestable-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="resource-html-tab" data-toggle="pill" href="#resource-html" role="tab" aria-controls="resource-html" aria-selected="true">
                            <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                            <h4>HTML and CSS Learning</h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="resource-javascript-tab" data-toggle="pill" href="#resource-javascript" role="tab" aria-controls="resource-javascript" aria-selected="false">
                            <img src="/dist/images/icons/icon-web-js.png" alt="JavaScript">
                            <h4>JavaScript Learning</h4>
                        </a>
                    </li>
                </ul>

                <div class="tab-content content-default" id="resource-tabContent">
                    <div class="tab-pane fade show active" id="resource-html" role="tabpanel" aria-labelledby="resource-html-tab">
                        <h5 class="show-md">HTML and CSS Learning</h5>

                        <div class="section-list js-load-list">

                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="How to Apply CSS to HTML Tags"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/styles.php">How to Apply CSS to HTML Tags</a>
                                    </h3>
                                    <p>
                                        HTML/CSS class at Training Connection</a> we learn different ways of adding CSS styles to our HTML documents.   There are benefits and drawbacks of each method available but first  let's make sure we understand what a CSS Style is and why we use them.
                                        <a class="post-link" href="/html/lessons/styles.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="How HTML Heredity Works"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/heredity.php">How HTML Heredity Works</a>
                                    </h3>
                                    <p>
                                        Heredity is the passing of traits from parents to their offspring.  Ok,  that's part of the dictionary definition, but what does this have to do  with HTML?  Did you know that there is heredity being passed down  through our HTML tags?  Well, heredity, as in "passing of traits", or in  this case, properties.
                                        <a class="post-link" href="/html/lessons/heredity.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="How HTML Specificity Works"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/specificity.php">How HTML Specificity Works</a>
                                    </h3>
                                    <p>
                                        What is CSS Specificity?  Specificity in CSS is the way a browser will  determine which declarations are applied to HTML elements when there is a  conflict from multiple rules. Ok, so what does that mean exactly?   Let's look at an example:
                                        <a class="post-link" href="/html/lessons/specificity.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Sprite Sheets in HTML/CSS Part 1"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/sprites1.php">Sprite Sheets in HTML/CSS Part 1</a>
                                    </h3>
                                    <p>
                                        Images are used all over a typical web page; for the logo, as icons, badges, buttons, and of course regular pictures. Some may have special effects such as rollover changes. A responsive page may swap out a smaller image with a larger one as the screen size increases, and vice versa.
                                        <a class="post-link" href="/html/lessons/sprites1.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Sprite Sheets in HTML/CSS Part 2"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/sprites2.php">Sprite Sheets in HTML/CSS Part 2</a>
                                    </h3>
                                    <p>
                                        In this part we look at how to create rollover effects using sprite sheets and how to create your own sprite sheet. Using sprite sheets in rollover effects is also pretty easy. First, create a sprite sheet (explained below) that contains both the inactive and active states of the rollover.
                                        <a class="post-link" href="/html/lessons/sprites2.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Using HTML Reset Stylesheets"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/reset.php">Using HTML Reset Stylesheets</a>
                                    </h3>
                                    <p>
                                        One of the challenges in web design is in making your pages look  consistent across the different browsers. Every browser has a user agent  stylesheet which it uses to provide default style values when rendering  a web page. In the absence of any other style rules, the browser falls  back to the user agent stylesheet values to "fill in" missing property  values.
                                        <a class="post-link" href="/html/lessons/reset.php">Read more</a>
                                    </p>
                                </div>
                            </article>
                            <article class="post-default g-text-html d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-html5.png" alt="Creating a Full-Height Web Page Layout"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/html/lessons/fullHeight.php">Creating a Full-Height Web Page Layout</a>
                                    </h3>
                                    <p>
                                        A common issue that comes up when creating web pages is what happens  when the content on a page is smaller than the screen height. For  example, take a look at the following simple layout:
                                        <a class="post-link" href="/html/lessons/fullHeight.php">Read more</a>
                                    </p>
                                </div>
                            </article>


                            <article class="post-default g-text-html pt-5 pb-2">
                                <button type="button" class="post-link js-load-btn">Show more articles</button>
                            </article>

                        </div>
                    </div>

                    <div class="tab-pane fade " id="resource-javascript" role="tabpanel" aria-labelledby="resource-javascript-tab">
                        <h5 class="show-md">JavaScript Learning </h5>


                        <div class="section-list js-load-list">

                            <article class="post-default g-text-javascript d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="JavaScript Variable Scopes"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/javascript/lessons/scope.php">JavaScript Variable Scopes</a>
                                    </h3>
                                    <p>
                                        You know what a function is and you know how to create variables. But, did you know that a variable behaves differently when it is used inside a function? Specifically, we are talking about variable scope. No, not the mouthwash for variables, although sometimes I feel like my variables could use a good cleaning. We are talking about the scope, as in the range of the effect of a variable.
                                        <a class="post-link" href="/javascript/lessons/scope.php">Read more</a>
                                    </p>
                                </div>
                            </article>

                            <article class="post-default g-text-javascript d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="JavaScript Arrays Explained"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/javascript/lessons/buildingArrays.php">JavaScript Arrays Explained</a>
                                    </h3>
                                    <p>
                                        Learning to use Arrays correctly is a fundamental part of your JavaScript journey. If handled incorrectly they can produce unintended results, much like mishandling a manta ray, you can get stung!
                                        <a class="post-link" href="/javascript/lessons/buildingArrays.php">Read more</a>
                                    </p>
                                </div>
                            </article>

                            <article class="post-default g-text-javascript d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="JavaScript Array Methods"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/javascript/lessons/arrayMethods.php">JavaScript Array Methods</a>
                                    </h3>
                                    <p>
                                        One of the most powerful features of an Array is the ability to use its built in methods. A method is a built-in function that acts upon an object, like an Array. A useful method of an Array to know is the 'toString()' method which converts the values of an Array to a comma delimited string:
                                        <a class="post-link" href="/javascript/lessons/arrayMethods.php">Read more</a>
                                    </p>
                                </div>
                            </article>

                            <article class="post-default g-text-javascript d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="Understanding Immediately-Invoked Function Expressions Part 1"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/javascript/lessons/invoked1.php">Understanding Immediately-Invoked Function Expressions Part 1</a>
                                    </h3>
                                    <p>
                                        You may have heard the term "immediately-invoked function expression" but may be unfamiliar with what they actually are or how they're used. An immediately-invoked function expression (or IIFE, pronounced 'iffy' for short) is a Javascript pattern to create a private scope within the current scope. This new scope may or may not be persistent (a closure). The technique uses an anonymous function inside parentheses to convert it from a declaration to an expression, which is executed immediately.
                                        <a class="post-link" href="/javascript/lessons/invoked1.php">Read more</a>
                                    </p>
                                </div>
                            </article>

                            <article class="post-default g-text-javascript d-none">
                                <div class="post-img"><img src="/dist/images/icons/icon-web-js.png" alt="Understanding Immediately-Invoked Function Expressions Part 2"></div>
                                <div class="post-body">
                                    <h3 class="post-title">
                                        <a href="/javascript/lessons/arrayMethods.php">Understanding Immediately-Invoked Function Expressions Part 2</a>
                                    </h3>
                                    <p>
                                        In the first part of this series we covered the basics of what an immediately-invoked function expression (IIFE) is and how it works using some simple examples. In this part we will look at some common ways IIFEs are used in applications.
                                        <a class="post-link" href="/javascript/lessons/arrayMethods.php">Read more</a>
                                    </p>
                                </div>
                            </article>


                            <!--LOAD MORE LINK-->
                            <article class="post-default pt-5 pb-2">
                                <button type="button" class="post-link js-load-btn">Show more articles</button>
                            </article>

                        </div>
                    </div>

                </div>


            </div>

        </div>

    </main>


    <div class="section-widget g-text-html mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

    <script>

        $(function(){
            $('.nav-link[href="#resource-javascript"]').on('click',function(event){
                $('.section-widget').addClass('g-text-javascript');
            });
            $('.nav-link[href="#resource-html"]').on('click',function(event){
                $('.section-widget').removeClass('g-text-javascript');
            });
        });
    </script>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>