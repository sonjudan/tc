<?php
$meta_title = "Adobe Photoshop Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Adobe Photoshop. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ps">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php/">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Photoshop</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-adobe-Ps.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 class="" data-aos="fade-up" >Adobe Photoshop</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-4">Training Resources</h3>

                    <p data-aos="fade-up" data-aos-delay="150">Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer  <a href="/photoshop-training.php">training on Adobe Photoshop</a>.</p>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Getting to know the Photoshop Interface"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/photoshop-interface.php">Getting to know the Photoshop Interface</a>
                        </h3>
                        <p>In this article, you will familiarize yourself with the Adobe Photoshop interface. Also covered are Preferences, Shortcuts, Workspaces and Menu Customization.
                            <a class="post-link" href="/photoshop/lessons/photoshop-interface.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Different File Formats and Resolutions in Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/image-formats.php">Different File Formats and Resolutions in Photoshop</a>
                        </h3>
                        <p>Photoshop format (PSD) is the default file format and the only format, besides the Large Document Format (PSB), that supports	most Photoshop features. Because of the tight integration between Adobe products, other Adobe applications such	as Adobe Illustrator, Adobe InDesign, Adobe Premiere, Adobe After Effects, and Adobe GoLive can directly import PSD files	and preserve many Photoshop features.
                            <a class="post-link" href="/photoshop/lessons/image-formats.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Creative Pathways in Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/creative-pathways.php">Creative Pathways in Photoshop</a>
                        </h3>
                        <p>Two extremely fun and useful features that many Photoshop users tend to overlook are Fill and Stroke Path. Hidden in the depths of the Path Panel menu or accessing the functions through the hollow and solid circle shaped buttons at the bottom of the panel, Fill and Stroke Path offers users both real utility and creative benefits.
                            <a class="post-link" href="/photoshop/lessons/creative-pathways.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="10 Useful often Ignored Features"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/ten-ignored-features.php">10 Useful often Ignored Features</a>
                        </h3>
                        <p>Many of us working with Adobe applications tend to focus on the features and functionalities that we need for our occupation or personal projects. Unfortunately, there are so many great features that get overlooked or just simply ignored. The following is a list of useful features that so many Photoshop users typically ignore. Let us begin!
                            <a class="post-link" href="/photoshop/lessons/ten-ignored-features.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Creating Anaglyph 3D in Adobe Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/anaglyph-3D.php">Creating Anaglyph 3D in Adobe Photoshop</a>
                        </h3>
                        <p>One of the oldest and simplest methods of creating 3D imagery is a technique known as Anaglyph 3D. Monster and alien movies from the 50's, 60's and even 70's used this type of 3D as a gimmick to bring in audiences and make them feel more like they were a part of the film. As the years have passed with  technology progressing and transforming, anaglyph 3D has remained a novelty, maintaining its popularity with movies, series and social media powerhouses like Youtube.
                            <a class="post-link" href="/photoshop/lessons/anaglyph-3D.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Improve your Efficiency using Photoshop Tool Presets"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/workflow-efficiency.php">Improve your Efficiency using Photoshop Tool Presets</a>
                        </h3>
                        <p>Working on Photoshop projects can be tedious and at many times very challenging. No matter what level of Photoshop user you are  useful shortcuts and time-saving features are a must if you are to keep your sanity. One such useful feature is called Tool Presets. Tool Presets can be created using any tool from your Tool Panel and can greatly speed up your  workflow by giving you access to custom tools set up by you. Custom brushes, healing tools, erasers and selection tools like the Lasso tools, Quick Selection or the Magic Wand tools are just a few of the many tools you can use.
                            <a class="post-link" href="/photoshop/lessons/workflow-efficiency.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="The Undo Command"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/undo.php">The Undo Command</a>
                        </h3>
                        <p>Most of us are familiar with the Undo command that is available in many  software programs we use. The Ctrl + Z shortcut is pretty universal and works in most programs. In Photoshop the Undo command functions a little differently.
                            <a class="post-link" href="/photoshop/lessons/undo.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Photoshop File Formats"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/file-formats.php">Photoshop File Formats</a>
                        </h3>
                        <p>You might think "I know how to Save documents." True. You are probably an expert at saving in Microsoft Office and other popular office software. But saving in Photoshop requires knowledge of image formats, the final destination of your image, a little math and more.
                            <a class="post-link" href="/photoshop/lessons/file-formats.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Content Aware Tools"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/content-aware-tools-part-a.php">Content Aware Tools</a>
                        </h3>
                        <p>The Content Aware Tools in Photoshop have been likened to pixel magic. What took hours to correct in the early versions of Photoshop now only takes seconds with the Content Aware Tools. These tools allow the designer/compositor to replace areas of an image with content from other parts of the image. Don't stress about how it's done. Sit back and relax and watch Photoshop do the work for you.
                            <a class="post-link" href="/photoshop/lessons/content-aware-tools-part-a.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="More Content Aware Tools"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/content-aware-tools-part-b.php">More Content Aware Tools</a>
                        </h3>
                        <p>The Content Aware Move Tool allows you to move part of an image and not have to worry about filling in the space behind the moved object. Once again Photoshop approximates the pixels that should fill in the new vacant space in order to keep the photo intact.
                            <a class="post-link" href="/photoshop/lessons/content-aware-tools-part-b.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Creating a Clipping Mask in Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/clipping-masks.php">Creating a Clipping Mask in Photoshop</a>
                        </h3>
                        <p>Adobe Photoshop CCThere are a few go-to tips that can give your design project immediate punch and pizzazz. They add a lot of polish and a stamp of professionalism with minimal difficulty. One my favorite is Creating a Clipping Mask. I'm sure you have seen those movie posters and advertisements where there is a photo coming through the shape of some text. It makes us go 'wow.' You probably think it is an advanced feature of Photoshop but it is really pretty simple. It can be done with a shape or text.
                            <a class="post-link" href="/photoshop/lessons/clipping-masks.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Useful Shortcuts in Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/essential-shortcuts.php">Useful Shortcuts in Photoshop</a>
                        </h3>
                        <p>Adobe Photoshop CCMultitasking is king in Photoshop. There will be many times where you will need to perform mutliple commands simultaneously. You might be Zooming In, while changing your Brush Size to Clone part of your image. Knowing how to use shortcuts allows you to do this.
                            <a class="post-link" href="/photoshop/lessons/essential-shortcuts.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ps.png" alt="Best Practises for Saving in Photoshop"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/photoshop/lessons/saving-files.php">Best Practises for Saving in Photoshop</a>
                        </h3>
                        <p>Adobe Photoshop CCSo you have a JPG image that you selected from a stock image site or a client has sent you via email. You need to make edits to the image for the project you are working on but you don't want to lose pixel quality. The first thing you should do is Open the JPG in Photoshop and Save As a PSD file. It is always best to work on an image in PSD format.
                            <a class="post-link" href="/photoshop/lessons/saving-files.php">Read more.</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-Ps mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>