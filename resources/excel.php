<?php
$meta_title = "Microsoft Excel Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Excel. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Excel</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Excel</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer  <a class="" href="/excel-training.php">training on Microsoft Excel</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Understanding the Excel Interface"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/excel-interface.php">Understanding the Excel Interface</a></h3>
                        <p>The Excel window is illustrated in Figure 1-4. The bulk of the screen is  occupied by the worksheet window. This grid provides a convenient  workspace where you can enter and manage your data. Surrounding the  worksheet window are several command interfaces, each of which allows  you to receive information about, or apply functions to, the data on the  worksheet.
                            <a class="post-link" href="/excel/lessons/excel-interface.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Starting Microsoft Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/starting-excel.php">Starting Microsoft Excel</a></h3>
                        <p>To work with Excel effectively, you need to know several basic skills  and concepts; how to start Excel from the Windows desktop, how to make  the most of the Excel interface, how worksheets are used, and how to  select cells and ranges.
                            <a class="post-link" href="/excel/lessons/starting-excel.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Opening Workbooks in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/opening-excel-files.php">Opening Workbooks in Excel</a></h3>
                        <p>When you first start Excel, a blank default workbook is loaded. You can  use this blank workbook to start a new worksheet. But what if you have  already used (saved and closed) the default workbook? At this point you  have a couple of options.
                            <a class="post-link" href="/excel/lessons/opening-excel-files.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Entering Data into Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/data-entry.php">Entering Data into Excel</a></h3>
                        <p>The first step in creating a useful worksheet is entering data. By  entering data, you are inputting the information that you want Excel to  display, calculate, and store. Data can be entered into a cell or a  range of cells. You can even set up a sequence of data and let Excel  fill in the remainder of the sequence based on your first few entries.
                            <a class="post-link" href="/excel/lessons/data-entry.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Adjusting Columns and Saving Files  in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/columns-saving.php">Adjusting Columns and Saving Files  in Excel</a></h3>
                        <p>On occasion, you will discover that the text or values you have entered  into a cell are not completely visible. This will occur when the number  of characters entered exceeds the width of the column and when data  appears in the cell to its right, as shown in column B of Figure&nbsp;1-8.
                            <a class="post-link" href="/excel/lessons/columns-saving.php">Read more</a>
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Printing and Exiting from Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/printing-exiting.php">Printing and Exiting from Excel</a></h3>
                        <p>You may want to print your worksheet when you’ve finished working with  it. This will give you a hard copy of your data to look over and to  share with others. Best of all, you can get a quick printout of the  active worksheet very easily.
                            <a class="post-link" href="/excel/lessons/printing-exiting.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Building Formulas in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/building-formulas.php">Building Formulas in Excel</a></h3>
                        <p>The backbone of Excel is its ability to perform calculations. There are  two ways to set up calculations in Excel: using formulas or using  functions. Formulas are mathematical expressions that you build  yourself.
                            <a class="post-link" href="/excel/lessons/building-formulas.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Editing and Copying Formulas in Microsoft Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas in Microsoft Excel</a></h3>
                        <p>The spot color effect is when everything on the screen is in black and white except for a certain color.<br>
                            <a class="post-link" href="/excel/lessons/editing-copying-formulas.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using Basic Functions in Microsoft Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/basic-functions.php">Using Basic Functions in Microsoft Excel</a></h3>
                        <p>This tutorial introduces Excel functions, which are a little like  templates for common formulas. There are many different types of  functions. First, we will look at the SUM function. You will learn about  using AutoComplete for entering formulas. We'll look at other basic  common functions such as AVERAGE .
                            <a class="post-link" href="/excel/lessons/basic-functions.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Selections in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/excel-selections.php">Working with Selections in Excel</a></h3>
                        <p>Before you can enter data, you have to select a cell. Before you can  change the data, you have to select it. In this section, you'll learn  effective, easy techniques that enable you to select cells, ranges, and <em>nonadjacent cells</em>.  With one click, you can even select your entire worksheet, which is  definitely a timesaver when it comes to making global changes.
                            <a class="post-link" href="/excel/lessons/excel-selections.php">Read more</a>
                        </p>
                    </div>
                </article>



                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Creating multiple views in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/multiple-views.php">Creating multiple views in Excel</a></h3>
                        <p>In many cases, you might find it helpful to work with different sections  of your worksheet at the same time. You might, for example, want to  keep the labels in row 4 visible while you scroll down to look at  information located in row 35. You do this by applying either split bars  or freezing panes.
                            <a class="post-link" href="/excel/lessons/multiple-views.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Creating Workspaces in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/creating-workspaces.php">Creating Workspaces in Excel</a></h3>
                        <p>You use a <em>workspace</em> when you need to save a configuration of  open workbooks on your system. In other words, let's say you've opened  two or three workbooks, have arranged them satisfactorily, and then  discover that you're out of time. Instead of repeating all the arranging  the next time you start Excel, you simply save the arrangement as a  workspace.
                            <a class="post-link" href="/excel/lessons/creating-workspaces.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Inserting, renaming, and deleting Excel worksheets"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/worksheets.php">Inserting, renaming, and deleting Excel worksheets</a></h3>
                        <p>Inserting a new worksheet into a workbook is easy. Excel inserts a new worksheet before the currently selected worksheet.              Method to insert a new worksheet: From the Insert menu, choose Worksheet
                            <a class="post-link" href="/excel/lessons/worksheets.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Multiple Worksheets and Workbooks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/multiple-workbooks.php">Working with Multiple Worksheets and Workbooks</a></h3>
                        <p>Analyzing and consolidating large amounts of data is one of Excel’s  strongest features. By combining several related worksheets into a  single workbook, you can restructure your data and organize it more  efficiently. By <em>default</em>, a new workbook contains three  worksheets; however, a workbook can contain as many as 255 worksheets or  as few as one worksheet.
                            <a class="post-link" href="/excel/lessons/multiple-workbooks.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Multiple Workbooks cont."></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with Multiple Workbooks cont.</a></h3>
                        <p>Sometimes you need to transfer data from one workbook to another,  perhaps as a way to consolidate information about one client or product.  In order to do this efficiently, you must open and view multiple  workbooks.
                            <a class="post-link" href="/excel/lessons/working-with-multiple-workbooks.php">Read more</a>
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Sharing Data across Worksheets and Workbooks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/data-across-workbooks.php">Sharing Data across Worksheets and Workbooks</a></h3>
                        <p>You can move and copy data between worksheets just as you do within a  single worksheet. Instead of specifying the paste destination in the <em>source worksheet</em>, you specify the paste location in a second worksheet, called the <em>destination worksheet</em>,  which can be in the same or another workbook. When you move or copy  cells or ranges that contain formulas and functions, you must take into  account whether or not they employ relative or absolute references.
                            <a class="post-link" href="/excel/lessons/data-across-workbooks.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Creating a  List in MS Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/creating-lists.php">Creating a  List in MS Excel</a></h3>
                        <p>A <em>list</em> is a sequence of rows of related data. You can use lists  whenever you need to organize large amounts of similar data, such as a  database of names and addresses. You create a list in much the same way  you create a worksheet. You enter information into a list by entering  data into cells. Although you can change list elements after you have  created a list, it is best to spend time planning your list before you  begin entering data.
                            <a class="post-link" href="/excel/lessons/creating-lists.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Maintaining a  List in MS Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/maintaining-lists.php">Maintaining a  List in MS Excel</a></h3>
                        <p>Excel provides an easy-to-use tool called the<em> data form</em> to  maintain lists. Hardened spreadsheet users generally prefer to edit data  in lists in the worksheet cells. In other words, in-cell or in-line  editing. But many Excel users, particularly those fairly new to Excel,  like the Data Forms or simply ‘Form’ feature in Excel.
                            <a class="post-link" href="/excel/lessons/maintaining-lists.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Filtering Lists in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/filtering-lists.php">Filtering Lists in Excel</a></h3>
                        <p><em>Filtering</em> can give you more control over your list,  particularly if your list contains a large number of records. For  example, suppose you operate a small grocery store and have a master  inventory of all the items in the store. Your list would include  everything from dairy products to fresh vegetables to cookies. What if  you suddenly needed to know how many types of cheese were on the shelf?  You could scroll through the entire list, counting the cheeses as you  go, but it would make more sense to filter the list so that it displays  only dairy products, or better yet, only cheeses.
                            <a class="post-link" href="/excel/lessons/filtering-lists.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using Subtotals in an Excel List"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/subtotals-in-lists.php">Using Subtotals in an Excel List</a></h3>
                        <p>When working with a list, you often need to know the bottom line figures  or totals. A list of sales records may include the names of all  salespersons, the different products they have sold, and the units of  each product sold. To get a better idea of each person’s performance by  product, you can use the Subtotals command to get a subtotal for each  product sold by each salesperson.
                            <a class="post-link" href="/excel/lessons/subtotals-in-lists.php">Read more</a>
                        </p>
                    </div>
                </article>



                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Editing Excel Records Using the Data Form"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/editing-records.php">Editing Excel Records Using the Data Form</a></h3>
                        <p>You can edit any data that appears in an edit box in a data form. If  record data appears with no edit box, then you cannot edit this data in  the data form because the field contains a formula.
                            <a class="post-link" href="/excel/lessons/editing-records.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Permissions in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/permissions.php">Working with Permissions in Excel</a></h3>
                        <p>The following tutorial introduces you to the Information tab on the  Backstage View. You’ll learn about marking a workbook as final, which  makes the workbook read-only. You’ll also learn about permissions - both  encrypting the workbook with a password and restricting permissions.  This tutorial explains how to protect both the current sheet and an  entire workbook’s structure.
                            <a class="post-link" href="/excel/lessons/permissions.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Sharing Excel Workbooks"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/sharing-workbooks.php">Sharing Excel Workbooks</a></h3>
                        <p>To share a workbook, use the following procedure.              Select the <strong>Review</strong> tab from the Ribbon. Select <strong>Share</strong> <strong>Workbook</strong>. In the <em>Share Workbook</em> dialog box, check the <strong>Allow changes by more than one user at the same time</strong> box.
                            <a class="post-link" href="/excel/lessons/sharing-workbooks.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Adding Digital Signatures in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/signatures.php">Adding Digital Signatures in Excel</a></h3>
                        <p>To add a digital signature to a workbook, use the following procedure.              Select the <strong>File</strong> tab from the Ribbon to open the Backstage View. Select <strong>Protect</strong> <strong>Workbook</strong>. Select <strong>Add a Digital Signature</strong>. Excel may display an informational message. Select <strong>OK</strong>. In the <strong>Sign</strong> dialog box, select the <strong>Commitment Type</strong> from the drop down list.
                            <a class="post-link" href="/excel/lessons/signatures.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Recording Macros in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/macros.php">Recording Macros in Excel</a></h3>
                        <p>If you perform certain operations over and over in Excel, you can record a  macro which stores all the steps. When you run the macro it will automatically perform each step automatically saving you a lot of manual repetitive work.
                            <a class="post-link" href="/excel/lessons/macros.php">Read more</a>
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Running a Macro in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/running-macros.php">Running a Macro in Excel</a></h3>
                        <p>Once you have recorded a macro, you can play back the keystrokes and  command selections anywhere in the workbook. Many of the macros you  record will run so quickly that they seem to work almost  instantaneously.
                            <a class="post-link" href="/excel/lessons/running-macros.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Editing and Deleting Macros"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/edit-macros.php">Editing and Deleting Macros</a></h3>
                        <p>If you need to make simple changes to a macro, such as inserting text, deleting a command or applying a specific format to a cell, you can edit the macro. You edit a macro in the Visual Basic Editor, shown in Figure&nbsp;2-6. The elements of the Visual Basic Editor are described in the table below.
                            <a class="post-link" href="/excel/lessons/edit-macros.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Hiding and Unhiding Data"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/rows-columns.php">Hiding and Unhiding Data</a></h3>
                        <p>When you develop workbooks for others to use, it may be wise to restrict the access they have to certain cells, worksheets, or even the entire workbook. Some cells may contain formulas that you do not want changed, and certain workbooks may be confidential. You can hide columns and rows within a worksheet, as well as hide worksheets within a workbook.
                            <a class="post-link" href="/excel/lessons/rows-columns.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Data Validation"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/data-validation.php">Data Validation</a></h3>
                        <p>To protect against incorrect data entry, you can use <em>data validation </em>to restrict the type of data that may be entered into a cell. You can specify a list of the valid entries or limit the number of characters in an entry. To further assist in accurate data entry, you can have a data input message appear that informs you of the type of data to be entered in a particular cell.
                            <a class="post-link" href="/excel/lessons/data-validation.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Protecting Excel Worksheets"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/protect.php">Protecting Excel Worksheets</a></h3>
                        <p>By default, all cells in a worksheet are designated as <em>locked</em>. You cannot prohibit changes to locked cells unless you protect the worksheet, after which none of the locked cells can be modified. If you want to be able to modify specific cells in a protected worksheet, you must <em>unlock</em> them before protecting the worksheet. When you modify any unlocked cells, the results in any protected cells that contain formulas dependent upon unlocked cells, will also be modified.
                            <a class="post-link" href="/excel/lessons/protect.php">Read more</a>
                        </p>
                    </div>
                </article>



                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Relative and Absolute Cell Reference"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/absolute-relative-references.php">Relative and Absolute Cell Reference</a></h3>
                        <p>A <strong>cell reference </strong>refers to a single <strong>cell </strong>or range of <strong>cells </strong>on  a Excel worksheet. These cells can be referred to by Excel Formulas  when calcuations are made. In this tutorial you will learn the  differences between relative and absolute cell references. Each behaves  differently when copied and filled to other cells. Relative references <strong>change</strong> when a formula is copied to another cell whereas Absolute references  remain <strong>constant</strong>, no matter where they are copied.
                            <a class="post-link" href="/excel/lessons/absolute-relative-references.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Multiple Cell References"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/multiple-cell-references.php">Multiple Cell References</a></h3>
                        <p>Excel is capable of completing complex calculations relatively quickly.  Most of the time, your calculations in Excel will involve using multiple  pieces of data for each calculation. In order to do this, you will need  to be able to reference multiple cells at the same time.
                            <a class="post-link" href="/excel/lessons/multiple-cell-references.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Array Formulas in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/array-formulas.php">Array Formulas in Excel</a></h3>
                        <p>An array can be defined as any grouping of two or more adjacent cells.  These cells form a square or rectangle. When a selection is defined as  an array, operations called array formulas can be performed on every  cell in the selection rather than on just a single cell. This is a  powerful technique that can be used to enhance Excel formulas and  functions.
                            <a class="post-link" href="/excel/lessons/array-formulas.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Layer Group Graphic Objects"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/layer-group-graphic-objects.php">Layer Group Graphic Objects</a></h3>
                        <p>Once you have added graphical objects to a worksheet, it is important to  know how to organize their positioning in relation to one another.  Using layers, you can choose which object overlaps another, while  grouping allows you to group multiple graphical objects together so that  you can adjust their properties all at the same time. Over the course of  this topic, you will learn all about layering and grouping graphical  objects in Microsoft Excel 2016.
                            <a class="post-link" href="/excel/lessons/layer-group-graphic-objects.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Customize Workbooks in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/customize-workbooks.php">Customize Workbooks in Excel</a></h3>
                        <p>Your workbooks can be customized in a number of different ways. Over the  course of this topic, we will focus on customization through the  addition of comments, hyperlinks, watermarks, and background pictures.
                            <a class="post-link" href="/excel/lessons/customize-workbooks.php">Read more</a>
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Managing Excel Themes"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/managing-themes-microsoft-excel.php">Managing Excel Themes</a></h3>
                        <p>Themes control most of the visual aspects of any workbook and choosing  the right theme is important to how your data is displayed. Over the  course of this topic you will learn about themes in Excel 2016 and how  they can be both changed and customized.
                            <a class="post-link" href="/excel/lessons/managing-themes-microsoft-excel.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Incoporating SmartArt into MS Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/Incorporate-SmartArt.php">Incoporating SmartArt into MS Excel</a></h3>
                        <p>SmartArt combines text-based information with graphics to create a more  appearance-driven look. Using Excel’s tools, you will be able to create  great SmartArt that can be used for a variety of different purposes.  Over the course of this topic, you will learn how to insert SmartArt  into your workbooks, as well as customizing it after it has been inserted.
                            <a class="post-link" href="/excel/lessons/Incorporate-SmartArt.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using Templates in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/create-and-use-templates-microsoft-excel.php">Using Templates in Excel</a></h3>
                        <p>Templates are an excellent resource that you can use to create workbooks  much more quickly than having to create one from scratch. Over the  course of this topic, you will learn more about templates and how to  create them yourself.
                            <a class="post-link" href="/excel/lessons/create-and-use-templates-microsoft-excel.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using IF, AND, OR Functions"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/using-if-and-or-functions.php">Using IF, AND, OR Functions</a></h3>
                        <p>Logic operations play a big part in Excel’s functionality, especially  the IF function. You can use this function to calculate different values  depending on the evaluation of a condition. The structure of an IF  function is as follows.
                            <a class="post-link" href="/excel/lessons/using-if-and-or-functions.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using the FV Function in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/using-FV-function.php">Using the FV Function in Excel</a></h3>
                        <p>Another commonly used financial function is the FV (future value)  function. This function returns the future value of a series of periodic  payments to an investment at a fixed interest rate. For example, if you  put $5,000 a year into an investment that yields 3.5% annual interest,  the FV function can tell you how much your investment will be worth  after a given period.
                            <a class="post-link" href="/excel/lessons/using-FV-function.php">Read more</a>
                        </p>
                    </div>
                </article>



                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="The PMT Function"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/using-PMT-function.php">The PMT Function</a></h3>
                        <p>The PMT (payment) function is a financial function that is used to  calculate loan payments based upon a constant interest rate. For  example, if you take a loan of $10,000 at 6% annual interest over 4  years, you can use the PMT function to calculate what the monthly  payment on the loan will be.
                            <a class="post-link" href="/excel/lessons/using-PMT-function.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="VLOOKUPS and HLOOKUPS"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/using-vlookup-hlookup-functions.php">VLOOKUPS and HLOOKUPS</a></h3>
                        <p>Both the VLOOKUP and HLOOKUP functions are used to find specific item(s)  in a table of data. The VLOOKUP function contains a number of arguments  which are written out like this.
                            <a class="post-link" href="/excel/lessons/using-vlookup-hlookup-functions.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Creating a PivotTable with PowerPivot Data"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/creating-pivottable-powerpivot-data.php">Creating a PivotTable with PowerPivot Data</a></h3>
                        <p>In this article, we will delve a little deeper into how to create a PivotTable with PowerPivot Data in Microsoft Excel. If you see a security warning, click Enable Content. Ensure that the  PowerPivot tab is displayed on the ribbon. If you do not see this tab,  please complete the steps in the “Enabling PowerPivot” topic first...
                            <a class="post-link" href="/excel/lessons/creating-pivottable-powerpivot-data.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Tables in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/working-with-tables.php">Working with Tables in Excel</a></h3>
                        <p>To facilitate data analysis and management, you have the ability to turn any range of cells into a table. You can have multiple tables per worksheet, and tables can be as large or small as the amount of data you want to work with.
                            <a class="post-link" href="/excel/lessons/working-with-tables.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Working with Slicers"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/work-with-slicers.php">Working with Slicers</a></h3>
                        <p>In this article, we will learn how to insert and use a slicer, rename a slicer and change slicer settings. Slicers allow you to quickly filter any data that is displayed within a PivotTable.
                            <a class="post-link" href="/excel/lessons/work-with-slicers.php">Read more</a>
                        </p>
                    </div>
                </article>
                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Changes in MS Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/tracking-changes.php">Tracking Changes in MS Excel</a></h3>
                        <p>When tracking is enabled, Excel will keep track of the changes that you or others who have access to your workbook have made. This includes data insertions, deletions, and more. While this feature is available in a few Microsoft Office products, Microsoft Excel is somewhat unique in that change tracking is available only in shared workbooks. While typically a shared workbook would be stored in a location where others who need to can access it, you can also track changes using a copy only you have access to.
                            <a class="post-link" href="/excel/lessons/tracking-changes.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Tracking Change Options"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/tracking-change-options.php">Tracking Change Options</a></h3>
                        <p>In this article we will look at the different options for Tracking Changes and how to stop tracking changes. As tracking changes has been integrated with the sharing capabilities of Excel 2013, you can find its customization options by first clicking Review → Share Workbook.
                            <a class="post-link" href="/excel/lessons/tracking-change-options.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="Using Comments in Excel"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/excel/lessons/using-comments.php">Using Comments in Excel</a></h3>
                        <p>In this article, we will learn how to insert comments, edit comments, navigate through comments
                            <a class="post-link" href="/excel/lessons/using-comments.php">Read more</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>

    <div class="section-widget g-text-excel mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>