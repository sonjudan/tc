<?php
$meta_title = "Microsoft Outlook Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Outlook. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Outlook</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Microsoft Outlook">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Outlook</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information articles, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer  <a class="outlook" href="/outlook-training.php">training on Microsoft Outlook</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Working with Contacts in Outlook"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/outlook/lessons/contacts.php">Working with Contacts in Outlook</a></h3>
                        <p>Outlook has some great tools for working with contacts. This this article we will show you how to customize a contact’s business card, which can really provide impact if you are including it in messages or sharing it in other ways. We’ll also learn how to forward a contact.
                            <a class="post-link" href="/outlook/lessons/contacts.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Working with Contact Groups in Outlook"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/outlook/lessons/contact-groups.php">Working with Contact Groups in Outlook</a></h3>
                        <p>Outlook has some great tools for working with contacts and contact groups. In the this article we will show you how to  create a contact group and manage its members, as well as how to send an email or meeting invitation to the group and how to forward the contact group.
                            <a class="post-link" href="/outlook/lessons/contact-groups.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Managing Mail and Auto Archiving in Outlook"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/outlook/lessons/auto-archiving.php">Managing Mail and Auto Archiving in Outlook</a></h3>
                        <p>When your Outlook items become out of date, you can store them or delete them. You can <em>archive</em> them automatically after a specified time interval or archive them  manually. When you archive your items, you store them in your My  Documents folder on your hard drive. You can retrieve archived items at  any time.
                            <a class="post-link" href="/outlook/lessons/auto-archiving.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Manually Archiving and Retrieving Items in Outlook"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/outlook/lessons/manually-archiving.php">Manually Archiving and Retrieving Items in Outlook</a></h3>
                        <p>If you want to archive a folder immediately, you use the Archive dialog  box, shown in Figure 1-3, which you access from the File menu. If you  want to archive your Contacts folder, you can only archive it manually.
                            <a class="post-link" href="/outlook/lessons/manually-archiving.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-outlook.png" alt="Adding a Signature in Outlook"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/outlook/lessons/signature.php">Adding a Signature in Outlook</a></h3>
                        <p>Outlook includes several ways to automatically manage different tasks. This article explains how to control your signatures. You can have a different signature for different email accounts, as well as different signatures for both new messages and replies/forwards.
                            <a class="post-link" href="/outlook/lessons/signature.php">Read more.</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-outlook mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>