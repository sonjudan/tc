<?php
$meta_title = "Adobe Premiere Pro Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Adobe Premiere Pro. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Pr">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php/">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Premiere Pro</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-adobe-Pr.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 class="" data-aos="fade-up" >Adobe Premiere Pro</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="Creating a Spot Color effect in Premiere Pro"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/spot-color-effect.php">Creating a Spot Color effect in Premiere Pro</a>
                        </h3>
                        <p>The spot color effect is when everything on the screen is in black and white except for a certain color. This effect can be done with both stills and video
                            <a class="post-link" href="/premiere-pro/lessons/spot-color-effect.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="Repairing Noisy Audio in Audition"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/noisy-audio.php">Repairing Noisy Audio in Audition</a>
                        </h3>
                        <p>If you have noisy audio in Adobe Premiere Pro you can use Audition to clean it up. Right click on the clip in Premiere's Timeline and choose Edit in Adobe Audition.
                            <a class="post-link" href="/premiere-pro/lessons/noisy-audio.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="How to make a Text Template in Premiere Pro"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/text-template.php">How to make a Text Template in Premiere Pro</a>
                        </h3>
                        <p>If you're going to make a multi episode  production you don't have to export the title or re-create the same title each time. Premiere allows you to save your titles as templates which will always be available to you.
                            <a class="post-link" href="/premiere-pro/lessons/text-template.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="Migrating & Archiving your Premiere Pro and After Effects Projects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/migrating-and-archiving.php">Migrating & Archiving your Premiere Pro and After Effects Projects</a>
                        </h3>
                        <p>For video professionals and enthusiasts, Adobe Premiere and After Effects are two of the most popular weapons of choice when it comes to video editing, animating and/or compositing media. When you understand how each application handles its media, you come to realize that there are some interesting challenges when you wish to move your project to another system or archive it for future use. The following are some fool-proof ways you can safely move or store your projects without broken links and other related issues.
                            <a class="post-link" href="/premiere-pro/lessons/migrating-and-archiving.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="Video Files Demystified"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/video-file-formats.php">Video Files Demystified</a>
                        </h3>
                        <p>In the sea of confusing file formats out there, a lot of users find themselves lost - many times choosing to output their videos as files they don’t completely understand. If you find yourself in this situation fear not. This quick, down and dirty guide is designed to give you a fundamental idea of how video files break down and what formats you should be using in various stages of your workflow.
                            <a class="post-link" href="/premiere-pro/lessons/video-file-formats.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Pr.png" alt="Virtual Reality and Premiere Pro"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/premiere-pro/lessons/virtual-reality.php">Virtual Reality and Premiere Pro</a>
                        </h3>
                        <p>The world loves watching powerhouse films like Star Trek, Star Wars, Guardians of the Galaxy, Avengers and so many more because, aside from the action, the hi-tech gadgetry and holographic effects are just so mind-blowingly cool.
                            <a class="post-link" href="/premiere-pro/lessons/virtual-reality.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-Pr mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>