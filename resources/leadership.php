<?php
$meta_title = "Leadership Learning Resources | Training Connection";
$meta_description = "Learning articles for better Leadership. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/leadership.php">Business Leadership</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Leadership</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/courses/business-skills/leadership-thumb.jpg" alt="Leadership">
                    </div>
                    <h1 data-aos="fade-up" >Leadership</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful informational articles on how to improve the way you communicate. We also run <a class="bus-eti" href="/business-leadership-training.php">Leadership classes</a> in Chicago and Los Angeles.<br>
                        </p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img">
                        <img src="/dist/images/courses/business-skills/leadership-thumb.jpg" alt="Techniques To Conduct More Effective Job Interviews">
                    </div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/leadership/job-interviewing-techniques.php">Techniques To Conduct More Effective Job Interviews</a>
                        </h3>
                        <p>According to Career Builder, the average cost of a bad hire is almost $15,000. Two out of three workers will later realize that the job they accepted is a bad fit. As an interviewer, you understand that these stats mean that the hiring process is not as efficient as it may seem.
                            <a class="post-link" href="/leadership/job-interviewing-techniques.php">Read more</a></p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img">
                        <img src="/dist/images/courses/business-skills/how-to-handle-nervousness.jpg" alt="Delivering Constructive Feedback That Produces Positive Change">
                    </div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/leadership/delivering-constructive-feedback.php">Delivering Constructive Feedback That Produces Positive Change</a>
                        </h3>
                        <p>According to Zenger and Folkman, both positive and negative feedback are important and critical especially when properly delivered. In addition, 92% believed that negative feedback that is properly conveyed is vital to improving one’s performance.
                            <a class="post-link" href="/leadership/delivering-constructive-feedback.php">Read more</a></p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img">
                        <img src="/dist/images/courses/business-skills/time-management.jpg" alt="The Biggest Challenges For First-Time Managers">
                    </div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/leadership/biggest-challenges-for-first-time-managers.php">The Biggest Challenges For First-Time Managers</a>
                        </h3>
                        <p>Surveys show that 44% of managers feel they’re unprepared for their role. Plus, 87% wished they’d had more of the necessary training before becoming a manager. This suggests that new managers aren’t getting any or enough training that they need to venture properly into their role.
                            <a class="post-link" href="/leadership/biggest-challenges-for-first-time-managers.php">Read more</a></p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-Ai mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>