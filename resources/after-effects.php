<?php
$meta_title = "Adobe After Effects Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Adobe After Effects. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-ae">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php/">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">After effects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-adobe-Ae.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 class="" data-aos="fade-up" >Adobe After Effects</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a monthly basis. We also offer <a href="/after-effects-training.php">training on Adobe After Effects</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="Ordering Effects in After Effects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/effects-order.php">Ordering Effects in After Effects</a>
                        </h3>
                        <p>The order of your effects in the Effects Control panel will determine how things look in After Effects. Here I've applied a Drop Shadow effect to my spaceman and brought the distance away in the effects control panel.
                            <a class="post-link" href="/after-effects/lessons/effects-order.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="Creating backgrounds and text with animated colors in After Effects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/effects-order.php">Creating backgrounds and text with animated colors in After Effects</a>
                        </h3>
                        <p>Go to Composition/New Composition. Create a composition with the preset of your choice. You can select layer/new layer or use the keyboard shortcuts Command Y or Control Y, then click ok to create a solid.
                            <a class="post-link" href="post-/after-effects/lessons/effects-order.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="Creating Spinning Objects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/spinning-effect.php">Creating Spinning Objects</a>
                        </h3>
                        <p>In After Effects go to Composition/New Composition. Choose the preset of your choice. We are going with HDTV1080 29;97, background color black and then Click OK. We are ready to bring in our planet. We are using a flat planet map of the earth.
                            <a class="post-link" href="/after-effects/lessons/spinning-effect.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="Illustrator to After Effects: 3D Logo Workflow"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/illustrator-after-effects.php">Illustrator to After Effects: 3D Logo Workflow</a>
                        </h3>
                        <p>The ability to take a custom vector logo designed in Illustrator and bring it to After Effects for animation is invaluable. Animated network Idents, logos, lower thirds - the applications are endless. In this quick, down and dirty guide, let us look at the best ways to get your logos from Illustrator to After Effects and prep them to be animated in 3D.
                            <a class="post-link" href="/after-effects/lessons/illustrator-after-effects.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="Superhero Motion Effects In Adobe After Effects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/motion-effects.php">Superhero Motion Effects In Adobe After Effects</a>
                        </h3>
                        <p>The other day I was watching the series The Flash on Netflix and remembered how, when I was younger, I wanted to run "superhero" fast! Who wouldn't want to do that? For a short while, my 7-year-old self was a believer that this was actually possible. Unfortunately, the inevitable reality set in. Combine this with my body type and the fact that I hated physical education. I think you get the idea! Anyway, motion blur and motion trail effects have been a staple of the superhero and fantasy film genres for a very long time.
                            <a class="post-link" href="/after-effects/lessons/motion-effects.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Ae.png" alt="5 Common problems experienced using After Effects"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/after-effects/lessons/5-common-after-effects-problems.php">5 Common problems experienced using After Effects</a>
                        </h3>
                        <p>Whenever you are learning a new application  there are always quirks, oddly programmed features and tricks you must embrace to take full advantage of that application. After Effects, like any professional software, has a number of these "quirks" or problems you need to figure out to truly master this amazing software. The following are common issues, with solutions, that many first-time After Effects users face when learning After Effects.
                            <a class="post-link" href="/after-effects/lessons/5-common-after-effects-problems.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>
</main>


    <div class="section-widget g-text-ae mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>