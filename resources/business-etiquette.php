<?php
$meta_title = "Business Etiquette Learning Resources | Training Connection";
$meta_description = "Learning articles for better Business Etiquette. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-business">Business Skills</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Business Etiquette</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/courses/business-skills/business-etiquette-thumb.jpg" alt="Business Etiquette">
                    </div>
                    <h1 data-aos="fade-up" >Business Etiquette</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information articles on how to improve your business etiquette. We also run <a class="bus-eti" href="/business-etiquette-training.php">business etiquette classes</a> in Chicago and Los Angeles.<br>
                        </p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/business-card.jpg" alt="How to use Business Cards"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/business-cards.php">How to use Business Cards</a>
                        </h3>
                        <p>Networking is not complete without receiving or giving a business card.  The business card is a way for you to follow up on the people you have  met. Likewise, it is a way for them to contact you for further meetings.                 More than that, your business card is a way to brand  yourself. Professional-looking business cards send the message that  you’re professional. Adding a company motto or tagline in your  business advertises you and what you’re all about.
                            <a class="post-link" href="/business-etiquette/business-cards.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/networking-for-sucess.jpg" alt="Building Relationships via Networking"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/networking.php">Building Relationships via Networking</a>
                        </h3>
                        <p>When you’re networking, it is important to make the most of the first  meeting. In this tutorial, we’ll discuss how to create an effective  introduction, make a good first impression and  minimize nervousness.
                            <a class="post-link" href="/business-etiquette/networking.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/greeting-clients.jpg" alt="How to Meet and Greet Clients"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/meet-greet.php">How to Meet and Greet Clients</a>
                        </h3>
                        <p>An introduction is almost always accompanied by a handshake and  conversation. In this lesson, we will discuss the three steps that make  an effective handshake and the four levels of conversation.
                            <a class="post-link" href="/business-etiquette/meet-greet.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/dining-etiquette.jpg" alt="Business Etiquette Dining Rules"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="../business-etiquette/business-dining.php">Business Etiquette Dining Rules</a>
                        </h3>
                        <p>Conducting business over meals is a great way to build business relationships. Meals make for a more casual atmosphere compared to offices, and are therefore more conducive to a relaxed discussion. In this module, we will discuss some of the etiquette rules when dining with business associates such as understanding place setting, rules while eating, and ways to avoid sticky situations.
                            <a class="post-link" href="../business-etiquette/business-dining.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/when-not-to-tip.jpg" alt="When is it okay not to tip?"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/tipping.php">When is it okay not to tip?</a>
                        </h3>
                        <p>What constitutes bad service? A bad attitude? A delay? A spill? A stupid question? Unless a rat runs over your food and they refuse to get you another plate, to express your dissatisfaction by  not tipping at all, says more about your lack of class and education than those you decided to punish by not tipping. In this article we will cover why, how and when not to tip.
                            <a class="post-link" href="/business-etiquette/tipping.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/party-etiquette.jpg" alt="Holiday Party Etiquette"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/party-etiquette.php">Holiday Party Etiquette</a>
                        </h3>
                        <p>The movie Office Christmas Party is sure to give us plenty of laughs over what not to do at your holiday parties at the end of the year. Let’s take a moment to consider valuable tips to ensure you continue creating a positive impression on colleagues and clients you work with  a great night out.
                            <a class="post-link" href="/business-etiquette/party-etiquette.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/courses/business-skills/borrowing-etiquette.jpg" alt="Borrowing Etiquette in the Workplace"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/business-etiquette/borrowing.php">Borrowing Etiquette in the Workplace</a>
                        </h3>
                        <p>It is that awkward feeling every time you see a certain somebody at the  office. The culprit? Guilt. You borrowed something and at some point,  you need to give it back. What you borrowed is possibly something you  still need, keep forgetting to bring in, you have not used yet or maybe  you are not sure where you put it! Maybe you are on the other side of  this.
                            <a class="post-link" href="/business-etiquette/borrowing.php">Read more</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>

            </div>
        </div>

    </main>


    <div class="section-widget g-text-bus-eti mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>