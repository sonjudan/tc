<?php
$meta_title = "Microsoft PowerPoint Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft PowerPoint. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-powerpoint">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">PowerPoint</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-powerpoint.png" alt="Microsoft PowerPoint">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft PowerPoint</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer <a class="powerpoint" href="/ms-powerpoint-training.php">training on Microsoft PowerPoint</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Your First PowerPoint Presentation"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/first-presentation.php">Your First PowerPoint Presentation</a></h3>
                        <p>In this article, you will be shown how to create your first presentation. Presentations consist of slides. Each slide can contain text, graphics, animations, and more. You’ll learn how to add slides, use the content placeholders, and add text to your presentation.
                        <a class="post-link" href="/powerpoint/lessons/first-presentation.php">Read more</a>.</p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Getting to know the PowerPoint Interface"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/interface.php">Getting to know the PowerPoint Interface</a></h3>
                        <p>In this article, we’ll introduce you to the PowerPoint 2013 interface, which uses the Ribbon from the previous two versions of PowerPoint. You’ll get a closer look at the Ribbon, as well as the Navigation pane and the Status bar. You’ll also learn how to manage your Microsoft account right from a new item above the Ribbon.
                            <a class="post-link" href="/powerpoint/lessons/interface.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Working with Text in Microsoft PowerPoint"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/working-with-text.php">Working with Text in Microsoft PowerPoint</a></h3>
                        <p>The PowerPoint 2013 editing tools make editing your presentation a breeze. This article covers how to work with text, including selecting, editing, deleting, cutting, copying, and pasting. It also explains how to use the Office Clipboard. You’ll learn how to use undo and redo and how to find and replace text, such as when you want to change a  phrase throughout your presentation.
                            <a class="post-link" href="/powerpoint/lessons/working-with-text.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Adding Tables in PowerPoint"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/tables.php">Adding Tables in PowerPoint</a></h3>
                        <p>In this article, we will show you step-by-step how to add and edit tables in Microsoft PowerPoint 2013. You will add a border and learn to sort a table.
                            <a class="post-link" href="/powerpoint/lessons/tables.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Charting in PowerPoint"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/charting.php">Charting in PowerPoint</a></h3>
                        <p>In this article, we will show you step-by-step how to add charts in Microsoft PowerPoint, how to edit them, change the chart type, and change the chart attributes.
                            <a class="post-link" href="/powerpoint/lessons/charting.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Drawing in Microsoft PowerPoint"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/drawing.php">Drawing in Microsoft PowerPoint</a></h3>
                        <p>In this article, we will show you step-by-step how to draw shapes, change the fill, the fill effect and the border in Microsoft PowerPoint 2013.
                            <a class="post-link" href="/powerpoint/lessons/drawing.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Using Master Slide - Part A"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/using-slide-masters.php">Using Master Slide - Part A</a></h3>
                        <p>Every PowerPoint presentation contains one or more sets of slide masters that control the layout and appearance of each slide. Using Slide Master view, you can make changes to these slide masters and their related layouts. To open slide master view, click View -&gt; Slide Master.
                            <a class="post-link" href="/powerpoint/lessons/using-slide-masters.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Using Master Slide - Part B"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/using-slide-masters-part-two.php">Using Master Slide - Part B</a></h3>
                        <p>This article is the second part of our tutorial on how to use Slide Masters in Microsoft PowerPoint.
                            <a class="post-link" href="/powerpoint/lessons/using-slide-masters-part-two.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Using Master Slide - Part C"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/using-slide-masters-part-three.php">Using Master Slide - Part C</a></h3>
                        <p>There are some circumstances in which PowerPoint will automatically delete a slide master. This can happen if the slides in the master are deleted or if another layout is applied to those slides. To prevent this from happening, you can preserve slide masters so that they are always saved even if they are not in use.
                            <a class="post-link" href="/powerpoint/lessons/using-slide-masters-part-three.php">Read more</a>.
                        </p>
                    </div>
                </article>



                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Using Master Slide - Part D"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/using-slide-masters-part-four.php">Using Master Slide - Part D</a></h3>
                        <p>If a presentation contains multiple sets of slide masters, the Layout menu will look a little bit different. However, the process of changing the layout is the same. First, select the slide(s) to which you want to apply the master. For this example, ensure that slide one is selected in the Slides pane.
                            <a class="post-link" href="/powerpoint/lessons/using-slide-masters-part-four.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Working with Templates"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/working-with-templates.php">Working with Templates</a></h3>
                        <p>This article details how to work with templates in Microsoft PowerPoint.
                            <a class="post-link" href="/powerpoint/lessons/working-with-templates.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Animation Techniques - Part A"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/advanced-animation-techniques-p1.php">Animation Techniques - Part A</a></h3>
                        <p>In this article, we will learn how to choose a basic animation effect. Set animation effect options,  and customize the trigger for an animation.
                            <a class="post-link" href="/powerpoint/lessons/advanced-animation-techniques-p1.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Animation Techniques - Part B"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/advanced-animation-techniques-p2.php">Animation Techniques - Part B</a></h3>
                        <p>In this article, we will continue to explore advanced animation techniques and how to use them in Microsoft PowerPoint. Editing the Motion path, setting the animation start options and modyining the duration and delay.
                            <a class="post-link" href="/powerpoint/lessons/advanced-animation-techniques-p2.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Animation Techniques - Part C"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/advanced-animation-techniques-p3.php">Animation Techniques - Part C</a></h3>
                        <p>PowerPoint contains some additional options to further customize animations in a presentation. To start, select an object that has an animation applied to it. For this example, click the title text box on the first slide.
                            <a class="post-link" href="/powerpoint/lessons/advanced-animation-techniques-p3.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Animation Techniques - Part D"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/advanced-animation-techniques-p4.php">Animation Techniques - Part D</a></h3>
                        <p>If you’ve used the Format Painter before, the Animation Painter is similar: it lets you copy animations from one object to another. This can be really useful if you have created a custom animation and want to apply it to other objects.
                            <a class="post-link" href="/powerpoint/lessons/advanced-animation-techniques-p4.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-powerpoint.png" alt="Advanced Presentation Techniques"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/powerpoint/lessons/advanced-presentation-techniques.php">Advanced Presentation Techniques</a></h3>
                        <p>In this article, we will demostrate how to use Advanced Presentation Techniques with a simple, easy to follow set-by-set format. In PowerPoint 2013, you have the ability to record timings, narrations, and laser pointer gestures in a single package.
                            <a class="post-link" href="/powerpoint/lessons/advanced-presentation-techniques.php">Read more</a>.
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-powerpoint mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>