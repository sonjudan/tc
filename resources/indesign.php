<?php
$meta_title = "Adobe After InDesign Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Adobe InDesign. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Id">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php/">Adobe</a></li>
                    <li class="breadcrumb-item active" aria-current="page">InDesign</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-adobe-Id.png" alt="Tracking Changes in MS Excel">
                    </div>
                    <h1 class="" data-aos="fade-up" >Adobe InDesign</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Id.png" alt="Using Conditional Text with Images in InDesign"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/indesign/lessons/conditional-text-images.php">Using Conditional Text with Images in InDesign</a>
                        </h3>
                        <p>Conditional Text in InDesign is similar to Layer Comps in Photoshop. But in InDesign it is	limited to text only. You can use conditional text with images by changing them to Inline Graphics.
                            <a class="post-link" href="/indesign/lessons/conditional-text-images.php">Read more</a>
                        </p>
                    </div>
                </article>


                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-adobe-Id.png" alt="Working with Conditional Text in InDesign"></div>
                    <div class="post-body">
                        <h3 class="post-title">
                            <a href="/indesign/lessons/conditional-text.php">Working with Conditional Text in InDesign</a>
                        </h3>
                        <p>1. Open Conditional Text.indd. 2. Choose Window > Type and Tables > Conditional Text to open	the panel. The panel already has conditions created and applied to	various text items in the layout.
                            <a class="post-link" href="/indesign/lessons/conditional-text.php">Read more</a>
                        </p>
                    </div>
                </article>


                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-Id mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>