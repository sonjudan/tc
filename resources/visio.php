<?php
$meta_title = "Microsoft Word Learning Resources | Training Connection";
$meta_description = "Knowledge and Learning articles for Microsoft Word. Instructor-led classes available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-visio">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-office">Microsoft Office</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Visio</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r">
                        <img src="/dist/images/icons/icon-office-visio.png" alt="Microsoft Visio">
                    </div>
                    <h1 data-aos="fade-up" >Microsoft Visio</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">Training Resources</h3>

                    <div class="mt-4"  data-aos="fade-up" data-aos-delay="150">
                        <p>Below you will find useful information, including tips and tricks, how-to articles etc. Content is updated on a weekly basis. We also offer <a class="word" href="/visio-training.php">training on Microsoft Visio</a>.</p>
                    </div>

                </div>
            </div>

            <div class="content-default js-load-list mt-5 mb-5" data-aos="fade-up" data-aos-delay="250">

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="The Visio Interface"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/interface.php">The Visio Interface</a></h3>
                        <p>Visio 2010 has a new interface that builds on interface from the  previous version of Visio. Visio 2010 uses the ribbon interface that was  introduced in Microsoft Office 2007 applications. Each tab in the  ribbon contains many tools for working with your drawing. To display a  different set of commands, click the tab name. Buttons are organized  into groups according to their function.
                            <a class="post-link" href="/visio/lessons/interface.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Setting up the Visio Screen"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/screen.php">Setting up the Visio Screen</a></h3>
                        <p>In this module, you will learn how to set up your Visio screen. You have  different elements to help you create your drawing, which you can show  or hide as needed. This module will explain how to add, move, and delete  a guide. It will also explain how to change the ruler settings and the  grid settings.
                            <a class="post-link" href="/visio/lessons/screen.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Your first Visio Document"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/first-doc.php">Your first Visio Document</a></h3>
                        <p>In this module, you will create your first drawing. Drawings consist of  shapes. This module will cover how to find the right shape and place it  on your drawing. You’ll learn how to add text to shapes. You’ll learn  how to work with shapes, including resizing, moving, and deleting  shapes. This module will also cover using the Tools group, which helps  with refining your shapes..
                            <a class="post-link" href="/visio/lessons/first-doc.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Moving Objects in Visio"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/moving-objects.php">Moving Objects in Visio</a></h3>
                        <p>Once objects have been placed on your drawing, you can move and  rearrange them using the drag and drop method. In the sample drawing,  you can see three rectangle shapes, two of which are connected to each  other.
                            <a class="post-link" href="/visio/lessons/moving-objects.php">Read more.</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Formatting Visio Shapes"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/formatting-shapes.php">Formatting Visio Shapes</a></h3>
                        <p>When creating drawings using templates, you have the ability to adjust  the shape and style that the selected template uses. The sample drawing  shown below was created using the Basic Flowchart template. This  template automatically applies a specific blue shape style to each  individual type of shape used.
                            <a class="post-link" href="/visio/lessons/formatting-shapes.php">Read more</a>
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Understanding the Shapes Pane"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/understanding-the-shapes-pane.php">Understanding the Shapes Pane</a></h3>
                        <p>The Shapes pane is the primary tool that you will use to add shapes to  your diagram. Open by default, you can find this pane on the left-hand  side of the Visio window.
                            <a class="post-link" href="/visio/lessons/understanding-the-shapes-pane.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Adding Connecting Shapes"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/adding-connecting-shapes.php">Adding Connecting Shapes</a></h3>
                        <p>To add shapes to your drawing, you first need to open the stencil in  which the shapes you would like to add are found. For this example,  click More Shapes â†’ General â†’ Basic Shapes.
                            <a class="post-link" href="/visio/lessons/adding-connecting-shapes.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <article class="post-default d-none">
                    <div class="post-img"><img src="/dist/images/icons/icon-office-visio.png" alt="Rotating, Resizing, Duplicating and Deleting Objects"></div>
                    <div class="post-body">
                        <h3 class="post-title"><a href="/visio/lessons/rotating-resizing-duplicating-deleting-shapes.php">Rotating, Resizing, Duplicating and Deleting Objects</a></h3>
                        <p>When you select a shape, you will see the rotate icon appear above it.  This is used to rotate the selected shape in any direction. Click to  select the top-most rectangle shape. Then, click and drag the rotate  icon in a clockwise direction, until it is on its side.
                            <a class="post-link" href="/visio/lessons/rotating-resizing-duplicating-deleting-shapes.php">Read more</a>.
                        </p>
                    </div>
                </article>

                <!--LOAD MORE LINK-->
                <article class="post-default pt-5 pb-2">
                    <button type="button" class="post-link js-load-btn">Show more articles</button>
                </article>
            </div>
        </div>

    </main>


    <div class="section-widget g-text-visio mt-4" data-aos="fade-up">
        <?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/resources-widget.php'; ?>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>