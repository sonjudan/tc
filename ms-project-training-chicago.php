<?php
$meta_title = "Project Training Classes | Chicago | Training Connection";
$meta_description = "Need to learn Microsoft Project? Our Chicago instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Project Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>


    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-project.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">Project Training<br>
                      Chicago
                    </h1>

                    <div data-aos="fade-up">
                        <h4>Project 2013, 2016, 2019 &amp; 365</h4>
                        <p>Looking for a hands-on Microsoft Project class in Chicago?</p>
                        <p>Our Project classes are taught by live instructors present in the classroom. You will be learning step-by-step how to use Microsoft Project in order to successfully deliver projects on time and within budget.</p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-project-2019.png" alt="MS Project 2019 box shot">
                    <img src="/dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">
                <h3>What's Included</h3>
                <ul>
                    <li>Printed MS Project manual</li>
                    <li>Certificate of course completion</li>
                    <li>Small class size</li>
                    <li>6 month FREE repeat (in case you need a refresher)</li>
                </ul>
                <p><strong>Book a Project class today. All classes guaranteed to run!</strong><br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>
                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading"  data-aos="fade-up">
                <h2>Microsoft Project Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-project" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="MS Project Level 1"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Microsoft Project Level 1
                                </a>
                            </h3>
                            <p>In this beginner instructor-led course trainees learn the basic concepts, before proceeding through all the functions required to effectively plan and manage small to medium-size projects. These include scheduling tasks, allocating resources, creating a timeline, tracking costs and reporting.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="Microsoft Project Level 1" data-price="$450">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/project/Project%20Level%201.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Microsoft Project Level 1">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>450
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Basics</h5>
                                        <ul>
                                            <li>Opening Project</li>
                                            <li> Using the Welcome Project</li>
                                            <li> Creating a New Project</li>
                                            <li> Signing In</li>
                                            <li> Saving a Project</li>
                                            <li> Opening a Project</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Setting Up a Project</h5>
                                        <ul>
                                            <li>Entering Project Information</li>
                                            <li> Setting Working Time</li>
                                            <li> Choosing Automatic or Manual Scheduling</li>
                                            <li> Entering Tasks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Scheduling Work</h5>
                                        <ul>
                                            <li>Organizing Tasks into Phases</li>
                                            <li> Linking and Unlinking Tasks</li>
                                            <li> Moving Tasks</li>
                                            <li> Rescheduling Tasks</li>
                                            <li> Splitting Tasks</li>
                                            <li> Deleting Tasks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Managing Resources</h5>
                                        <ul>
                                            <li>Entering Resources</li>
                                            <li> Using the Resource Information Dialog</li>
                                            <li> Assigning Resources to Tasks</li>
                                            <li> Removing and Replacing Resource Assignments</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating a Timeline</h5>
                                        <ul>
                                            <li>Showing and Hiding the Timeline</li>
                                            <li> Customizing Timeline Tasks</li>
                                            <li> Changing the Font for Individual Timeline Items</li>
                                            <li> Modifying Global Text Styles</li>
                                            <li> Copying the Timeline</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Managing Tasks</h5>
                                        <ul>
                                            <li>Creating Recurring Tasks</li>
                                            <li> Importing Outlook Tasks</li>
                                            <li> Inactivating Tasks</li>
                                            <li> Updating Task Completion</li>
                                            <li> Updating the Project</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Deadlines and Constraints</h5>
                                        <ul>
                                            <li>Using the Task Information Dialog</li>
                                            <li> Changing the Task Calendar</li>
                                            <li> Using Lag and Lead Time</li>
                                            <li> Creating Milestones</li>
                                            <li> Setting Deadlines</li>
                                            <li> Creating Constraints</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Data</h5>
                                        <ul>
                                            <li>Sorting Data</li>
                                            <li> Filtering Data</li>
                                            <li> Highlighting Data</li>
                                            <li> Grouping Data</li>
                                            <li> Outlining Data</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating Reports</h5>
                                        <ul>
                                            <li>Creating a Built-In Report</li>
                                            <li> Creating a Dashboard</li>
                                            <li> Creating a Report from Scratch</li>
                                            <li> Creating a Visual Report</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Finishing Touches</h5>
                                        <ul>
                                            <li>Viewing the Entire Project</li>
                                            <li> Checking Your Spelling</li>
                                            <li> Running the Task Inspector</li>
                                            <li> Applying a Gantt Chart Style</li>
                                            <li> Highlighting Critical, Slack, or Late Tasks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Printing and Sharing Your Project</h5>
                                        <ul>
                                            <li>Saving a Project as PDF or XPS</li>
                                            <li> Exporting a Project as an Excel Workbook</li>
                                            <li> Printing a Project</li>
                                            <li> E-mailing a Project</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-project.png" alt="MS Project Level 2"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Microsoft Project Level 2
                                </a>
                            </h3>
                            <p>This course covers the more advanced features of  MS Project which will help you become an expert user. You will learn about advanced tasks and resource pools, baselines and resolving conflicts, working on multiple projects, using project data, and closing out a project.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal"  data-classes="Microsoft Project Level 2" data-price="$450">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/project/Project%20Level%202.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Microsoft Project Level 2">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>450
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">The Quick Access Toolbar and Advanced Calendar Topics</h5>
                                        <ul>
                                            <li>Configuring the Quick Access Toolbar</li>
                                            <li>About advanced calendars</li>
                                            <li>24-hour calendars</li>
                                            <li>Creating a global template from a custom calendar</li>
                                            <li>Assigning a task calendar</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Tasks</h5>
                                        <ul>
                                            <li>Adding notes to tasks</li>
                                            <li>Setting a task deadline</li>
                                            <li>Setting task priority</li>
                                            <li>Grouping tasks</li>
                                            <li>Sorting tasks</li>
                                            <li>Filtering tasks</li>
                                            <li>Highlighting tasks</li>
                                            <li>Adding a fixed cost to a task</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Managing the Project Schedule</h5>
                                        <ul>
                                            <li>Multiple baselines and interim plans</li>
                                            <li>Setting multiple baselines</li>
                                            <li>Setting and using a status date</li>
                                            <li>Resolving resource conflicts and scheduling issues</li>
                                            <li>About resource leveling</li>
                                            <li>Using the Tracking Gantt View</li>
                                            <li>Leveling resources to resolve conflicts and scheduling issues</li>
                                            <li>Delaying a task</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Resources</h5>
                                        <ul>
                                            <li>Grouping resources</li>
                                            <li>Sorting resources</li>
                                            <li>Filtering resources</li>
                                            <li>Highlighting resources</li>
                                            <li>Resource working time and resource pools</li>
                                            <li>About resource non-working time</li>
                                            <li>Setting working time for specific resources</li>
                                            <li>Resource pools for sharing resources</li>
                                            <li>Creating a resource pool</li>
                                            <li>Sharing a resource pool</li>
                                            <li>About resource budget costs</li>
                                            <li>Step 1: Creating a Budget Cost Resource</li>
                                            <li>Step 2: Creating a Budget Work Resource</li>
                                            <li>Step 3: Creating a Budget Material Resource</li>
                                            <li>Displaying Budget Cost Resources</li>
                                            <li>Setting up Task Usage view</li>
                                            <li>Assigning a budget resource to the Project Summary Task</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Other Views and Shortcuts</h5>
                                        <ul>
                                            <li>About task forms</li>
                                            <li>Using task forms</li>
                                            <li>About resource forms</li>
                                            <li>Using resource forms</li>
                                            <li>Compound views and the Relationship Diagram</li>
                                            <li>Keyboard shortcuts</li>
                                            <li>Implementing the Relationship Diagram in a compound view</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Multiple Projects and Project Integrations</h5>
                                        <ul>
                                            <li>Consolidation of multiple projects</li>
                                            <li>Advantages and disadvantages of consolidation</li>
                                            <li>Creating a project schedule consolidation</li>
                                            <li>Creating project snapshots</li>
                                            <li>About Project Server, Project Online, and SharePoint</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Project data and Closing out a Project</h5>
                                        <ul>
                                            <li>Working with Project data</li>
                                            <li>Capturing and using Project data in Word and PowerPoint</li>
                                            <li>Capturing and using Project data in Word and PowerPoint</li>
                                            <li>Annotating a project schedule view</li>
                                            <li>Copying the timeline to a PowerPoint presentation</li>
                                            <li>Closing out a project</li>
                                            <li>Final reports</li>
                                            <li>Creating a cost overruns report with headers and footers</li>
                                            <li>Saving a project as a template and using it to create a new project</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


   


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>


    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Project training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Microsoft Project classes - 5 rating"></span>
                    </div>

                    <h3><span>Project classes rating:</span> 4.7 stars from 933 reviewers </h3>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Pat is an excellent resource and has given me many great tips and tools for the microsoft program as well as project related tips outside of the software that were relevent to me as a I a project manager. I would recommend this class to my other co-workers. &quot;</p>
                                <span><strong>Jackie Gruenes | Alliant Credit Union</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I really enjoyed this class. Rich was a great trainer, and I feel ready to use MS Project more efficiently. I would definitely recommend this class to others&quot;</p>
                                <span><strong>Danielle Davis | Northstar Lottery Group</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Excellent instructor, excellent course material and excellent classroom facilities. I can use what I learned from this course to my job immediately. I'll recommend this class to anyone who uses Project 2010&quot;</p>
                                <span><strong>Lili Tian</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Steve was a great instructor. I really appreciated his tips and tricks and his real examples used. He took time to explain and the pace of the class was good. Definitely will refer other co-workers to attend this class.&quot;</p>
                                <span><strong>Stephanie Kaping | LA County Metropolitan Transport Authority</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=22" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>

    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that  trainees often benefit from repeating their class. Included in your course price is a FREE Repeat valid for 6 months.  Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Money back guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Project is not for everyone. If you decide that after half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Project Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Project. This can be delivered onsite at your premises, at our training center in the LOOP or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" checked><i></i> <h3>Project Level 1 - Introduction</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Project Level 2 - Advanced</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

    <div class="section section-faq g-text-project">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Project Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best Microsoft Project learning experience. Please find answers to some of our most frequently asked Project training related questions below, or contact us should you require further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                  <h5>What version of Project do you teach on?</h5>
                  <p>We are currently teaching our classes on Microsoft Project 2019. Project 2019 is similar to Project 2016 and Project 2013, so the training is suitable for all 3 versions of Project.</p>
                </li>
                <li>
                  <h5>What times are your Project classes?</h5>
                  <p>All our classes run from 9.00am to 4.30pm.</p>
                </li>
                <li>
                  <h5>Where are your Project classes held?</h5>
                  <p>Our Chicago Project classes are held in our Chicago Training center located at 230 W Monroe Street, Suite 610, Chicago IL 60606. For more information about directions, parking, trains please <a href="/contact-us-chicago.php">click here</a>.</p>
                </li>
                <li>
                  <h5>Is this face-to-face training or a webinar?</h5>
                  <p>Our Project classes are taught by live trainers present in the classroom. This is not a webinar. </p>
                </li>
                <li>
                  <h5>Do I need to bring my own computer?</h5>
                  <p>No, we provide the computers our training center in the Chicago Loop.</p>
                </li>
                <li>
                  <h5>Can you deliver Project training onsite at our location?</h5>
                  <p>Yes, we service the greater Chicago metro including Arlington Heights, Aurora, Bollingbrook, Chicago Loop, Deerfield, Des Plaines, Evanston, Highland Park, Joliet, Naperville, Schaumburg, Skokie, South Chicago and surrounding areas.</p>
                  <p>Our trainers can also travel anywhere in the country to deliver Project training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Project training.</p>
                </li>
             
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-project.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>