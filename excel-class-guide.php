<?php
$meta_title = "Microsoft Excel Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Passing Microsoft Excel Certification Exams. Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Choosing the right Excel class guide</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ec" style="background-image: url('/dist/images/banner-excel.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">
                        Choosing the<br>
                        right Excel<br>
                        class guide
                    </h1>
                </div>

                <div class="img-specialist"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/excel-specialist.png" alt="Microsoft Office Excel Specialist" width="340">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-exams">
        <div class="container">
            <div class="section-heading align-left" data-aos="fade-up">
                <h2>Which Excel course should I do?</h2>
            </div>


            <div class="table-responsive course-excel" data-aos="fade-up">
                <table class="table table-compare">
                    <thead>
                        <tr>
                            <th class="bg-white"></th>
                            <th>Excel 1</th>
                            <th>Excel 2</th>
                            <th>Excel 3</th>
                            <th>Excel 4 - VBA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th><p>I am a complete beginner to Excel</p></th>
                            <td><i class="fas fa-check"></i></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th><p>Prerequiste to this Class</p></th>
                            <td>Basic computer skills using keyboard and mouse</td>
                            <td>Excel Level 1 or equivalent experience</td>
                            <td>Excel Level 1 or equivalent experience</td>
                            <td>Excel Level 3 or equivalent experience</td>
                        </tr>
                        <tr>
                            <th><p>I can perform basic calculations & format a spreadsheet. Can I jump straight to Level 2?</p></th>
                            <td></td>
                            <td>YES</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th><p>I need to learn PivotTables</p></th>
                            <td></td>
                            <td><i class="fas fa-check"></i></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th><p>I need to learn Advanced Functions & Formulas </p></th>
                            <td></td>
                            <td></td>
                            <td><i class="fas fa-check"></i></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th><p>I need to learn Basic Macros</p></th>
                            <td></td>
                            <td></td>
                            <td><i class="fas fa-check"></i></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th><p>I need to be an expert on Excel automation (VBA)</p></th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <th><p>I need to prepare for Microsoft Excel Certification</p></th>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td>
                                <a href="/excel-training.php#collapse-outline-1" class="btn btn-secondary btn-md btn-block"><i class="fas fa-pen mr-1"></i> View Outline</a>
                            </td>
                            <td>
                                <a href="/excel-training.php#collapse-outline-2" class="btn btn-secondary btn-md btn-block"><i class="fas fa-pen mr-1"></i> View Outline</a>
                            </td>
                            <td>
                                <a href="/excel-training.php#collapse-outline-3" class="btn btn-secondary btn-md btn-block"><i class="fas fa-pen mr-1"></i> View Outline</a>
                            </td>
                            <td>
                                <a href="/excel-training.php#collapse-outline-4" class="btn btn-secondary btn-md btn-block"><i class="fas fa-pen mr-1"></i> View Outline</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-excel-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-excel.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>