<?php
$meta_title = "How to write a professional business proposal | Training Connection";
$meta_description = "Learn how to write an effective Business Proposal. This and other topics are covered in our Business Writing training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-writing.php">Business Writing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Proposals</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Write a Business Proposal</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>It is not just in face-to-face interactions that we have to put our best foot forward. The same can be said in written correspondence, more particularly when you are submitting a business proposal. In this module, we will discuss the basic structure of a proposal, how to select a proposal format, and tips in writing a proposal.</p>

                <p>Need training to improve your Business writing skills? Why not join one of <a href="/business-writing-class.php">our business writing classes</a>. Call us on 888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-writing/proposal.jpg" width="750" height="435" alt="Compelling Business Proposals"></p>
                <h4>The Basic Structure</h4>
                <p> A business proposal is an unsolicited or solicited bid for business. A proposal is usually written to another company or institution. Companies can also require their local departments to write proposals when suggesting solutions to upper management.</p><p>At its very basic level, a business proposal answers two things: what the other party needs, and how your company can address this need.</p><p>The following is the basic structure of a business proposal:</p>
                <ul>
                    <li>Company Overview</li>
                    <li>Statement of the Problem (also referred to as the Need Statement)</li>
                    <li>Background and Benefits (mostly used for unsolicited proposals)</li>
                    <li>Scope of Services/ Deliverables</li>
                    <li>What</li>
                    <li>Who</li>
                    <li>When</li>
                    <li>Where</li>
                    <li>How</li>
                    <li>How much</li>
                    <li>Contact Details</li>
                    <li>References</li>
                </ul>
                <p>Below is a sample business proposal.</p><p>NOTE: This sample is just a basic draft for the purpose of illustration. The succeeding section gives more information on business proposal formats.</p>
                <h4>Sample</h4>
                <p><strong>Company Overview</strong> <br>Linkages International is South Carolina’s fastest growing technical support company. In our three years of service, we have provided customer assistance for 27 multinational companies, 6 of which are in the Fortune 500 list.</p>
                <p>Chicago-based <a href="/business-writing-class-chicago.php">email and other business writing classes</a>.</p>
              <p><strong>Statement of the Problem</strong> <br>The credibility of many online industries depends on how fast and how efficiently they can respond to telephone calls and email messages. Without an actual physical location that customers can visit, email and phone services becomes even more important. The lack of a top of the line customer support system for phone and email communication can seriously affect your business’ bottom line!</p>
                <p><strong>Scope of Services/ Deliverables</strong></p>
                <p><strong>What? </strong> <br>24 hour customer assistance via telephone and email.</p>
                <p><strong>Who? </strong> <br>Trained and qualified customer service representatives will answer all inquiries.</p>
                <p><strong>When?</strong> <br>Set up can begin immediately. 1 year, 3 year and 5 year contracts available.</p>
                <p><strong>Where? </strong> <br>All customer service personnel and equipment would be hosted by Linkages International’s main office in Jameson Blvd.</p>
                <p><strong>How? </strong> <br>The Linkages International process involves: </p>
                <p><u>Consultation.</u> Linkages International staff would meet with your company to determine your specific needs and preferences.</p><p><u>Staff Training</u>. Customer representatives assigned to your company would be trained in your company profile, product range and other technical knowledge necessary to accomplish the job.</p><p><u>Set-up</u>. Dedicated phone and internet lines would be set up for your company.</p>
                <p><strong>How much?</strong> <br>Please see attached document for our package rates.</p>

                <p><strong>Contact Details</strong><br>
                    Mr. Raymond Daniels<br>
                    Head of Sales and Marketing, Linkages International<br>
                    790 Hampden Park, North Carolina</p>

                <p><strong>References</strong><br>
                    Mr. Jason Orange<br>
                    Director, Mediatrix Corporation<br>
                    4556 Croke Park, New Mexico</p>

                <p>Ms. Rutina Compton<br>
                    Customer Service Head, Joint Ventures Ltd. <br>
                    147 QHU Hall, West Covina</p>

                <p>Also see <a href="http://www.centenary.edu/research/proposal/elements">common business proposal elements</a>. </p>

                <h4>Choosing a Format</h4>
                <p>There is no one universal format for business proposals. The format that you should use depends on:</p>
                <p><strong>Intended recipient of the proposal:</strong><br>
                    Some companies and institutions require formal, structured, and very detailed proposals. For example, government agencies requesting bids for use of public funding require that you outline to the letter how the money would be spent and how the expenditure would go to the target result. They may request additional information like your business plan and financial statement. Proposals like these can be as long as 100 pages. They usually require a cover letter, apart from the proposals itself.</p>
                <p><a href="/business-writing-class-los-angeles.php">Click here</a> for details on Business Writing classes in Los Angeles.</p>
                <p>Others are more relaxed; they can be as short as 1-2 pages. Proposals submitted through online marketplaces, for example, do not require much structure due to the informal medium of communication. If you’ve already established a relationship with the other party, you need not include the ‘selling’ part of your proposal --- you can go straight to pricing and deliverables. Deliverables that don’t require much technical explanation can be presented in bullet form.</p>
                <p><strong>The scale/ scope of the project:</strong><br>
                    As a guide, take your cue from the way the Request for Proposal (to be discussed in a later module) is written. If the request is written in a formal and structured fashion, then respond similarly. If the request is presented in a more relaxed fashion, then you can be more relaxed. If they are asking for specific information from you, then include it in the format.</p>
                <p>If you’re uncertain, inquire with the company/ institution how they’d like your proposal to be presented. There’s nothing wrong in asking!</p>
                <p>If you’re uncertain, inquire with the company/ institution how they’d like your proposal to be presented. There’s nothing wrong in asking!</p>

                <p>More <a href="/resources/business-skills.php">Free Business Skills resources</a>.</p>
                <h4>Writing the Proposal</h4>
                <p> Make sure your business proposal is:</p>
                <ul>
                    <li><strong>Targeted</strong>: On target means that you have carefully studied what the other party’s need is, and you can show that you are their best option in addressing that concern. </li>
                    <li><strong>Well-substantiated</strong>: Substantiated means filled with evidence to back your claims. Decide which facts or statistics best support the project. Substantiation may also come in the form of a carefully thought out project plan.</li>
                    <li><strong>Persuasive</strong>: Always keep your prime selling point in mind and make sure your writing emphasizes it. Keep your tone proactive and optimistic. Don’t give generic content; demonstrate how your proposal is better than others they would receive.</li>
                </ul>
                <p>Unsolicited proposal requires a harder ‘sell’ than solicited ones.</p>
                <ul>
                    <li><strong>Organized</strong>: A winning proposal is easy to evaluate. Picture the evaluator with a checklist in hand going through your proposal --- check, check, check. Give the other party the information that they want, in the order that they want it. Get rid of all unnecessary detail.</li>
                </ul>
                <p>Also read <a href="/business-writing/reports.php">How to Write a Reports</a> </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">On-site Business Writing training</h4>
                    <p>Through our network of local trainers we deliver on-site Business Writing classes right across the country. Click to obtain a <a href="/onsite-training.php">quote for onsite training in business writing</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=29">Business writing testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>