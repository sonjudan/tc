<?php
$meta_title = "Writing Reports | Training Connection";
$meta_description = "Learn how to write an effective Business Report. This and other topics are covered in our Business Writing training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-writing.php">Business Writing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reports</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>How to Write a Business Report</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Documentation is important in business. Sometimes documentation is the only way supervisors can monitor the company’s quality of work. At other times, documentation is the key to spotting best and worst practices. In this article, we will discuss the basic structure of reports, how to choose the right format, and tips on writing reports.</p>

                <p>Need training to improve your  writing skills? Why not join one of our <a href="/business-writing-class.php">business writing workshops</a>. Call us on  888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-writing/report-writing.jpg" width="750" height="211" alt="Report writing skills"></p>
                <h4>The Basic Structure</h4>
                <p>Business reports are used to provide documentation - a written record of a topic, project, or process. <a href="/business-writing-class-chicago.php">Business writing workshop offered in Chicago</a>.</p><p>
                    The following are the basic parts of a business report:</p>
                <ul>
                    <li>Overview</li>
                    <li>Background/ Project Scope</li>
                    <li>Main Body</li>
                    <li>Conclusions</li>
                </ul>
                <p>The main body<strong> </strong>of the report contains all the details of the project or topic, including facts, methods, data, calculations, results, and interpretation. Depending on what type of report you are writing, the conclusion <strong> </strong>provides the final result, recommendation, proposal, or concluding judgment of the report's authors.</p>
                <p>See more <a href="/resources/business-skills.php">Business Skills training resources</a>.</p>
                <h4>Choosing a Format</h4>

                <p>As with the other business documents, such as the business proposal and the business letter, the format and length of business reports vary depending on the situation. <a href="/business-writing-class-los-angeles.php">Business writing training course in LA</a>.</p><p>
                    Be guided by the:</p>
                <ul>
                    <li>The purpose of the report</li>
                    <li>The seniority of your readers</li>
                    <li>Your readers’ technical knowledge</li>
                    <li>The scale of the project</li>
                    <li>Standard protocol in your company</li>
                </ul>
                <p>NOTE: The rationale behind the above criteria is similar to how the same criteria had been explained in previous modules.</p>
                <p>

                    Also download - <a href="http://wac.colostate.edu/teaching/tipsheets/writing_business_reports.pdf">what is a business report and how do I write one</a>? </p>
                <h4>Writing the Report</h4>
                <p>The following are some tips in writing a business report. </p>
                <ul>
                    <li>Keep the purpose of the report in mind when writing your report.</li>
                </ul>
                <p>Ask the person who requested the reports what they are expecting to see in the report and how they plan to use your documentation. This information can guide you in discerning what data to include in the report.</p>
                <ul>
                    <li>Stick to objective data, unless there is a section for personal opinions.</li>
                </ul>
                <p>Reports are meant to be fact-based and impartial; it’s not written so that you can present the company’s performance in the best possible light. It is only when a report is accurate can the report be truly useful. Therefore, make sure that you double-check your content. Watch if you have biases coming through on paper. If your opinions are solicited, stick to giving professional opinions substantiated by facts.</p>
                <ul>
                    <li>Write to your audience.</li>
                </ul>
                <p>Know who you are writing to, and determine how best to address this audience. Define the tone, attitude, and emphasis that are geared towards your readers.</p>
                <p>Also read <a href="/business-writing/basics.php">Spelling and Grammar Tips</a> </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">On-site Business Writing training</h4>
                    <p>Through our network of local trainers we deliver on-site Business Writing classes right across the country. Click to obtain a <a href="/onsite-training.php">quote for onsite training in business writing</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=29">Business writing testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>