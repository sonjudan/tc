<?php
$meta_title = "Constructing Sentences | Training Connection";
$meta_description = "Learning how to construct proper sentences. This and other topics are covered in our Business Writing training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-writing.php">Business Writing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Constructing Sentences</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Constructing Sentences</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>This article will discuss the parts of a sentence, its proper punctuation, and the four kinds of sentences. Also see <a href="/business-writing/basics.php">spelling and grammar</a>.</p>
                <p>Need to improve your Business writing skills? Why not join one of <a href="/business-writing-class.php">our business writing classes</a>. Call us on 888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-writing/sentences.jpg" width="750" height="486" alt="Constructing sentences"></p>
                <h4>Parts of a Sentence</h4>
                <p>A complete sentence has two parts: a <strong>subject</strong> and a <strong>predicate</strong>.</p>
                <p>The subject is what the sentence is about. It is usually a noun or pronoun. The predicate tells something about the subject. It is often indicated by an action verb or a linking verb.</p>
                <p>Example:
                    “The committee recommends a full inquiry over this matter.”</p>
                <p>The <strong>subject</strong> is ‘The committee’ and the <strong>predicate</strong> is ‘recommends a full inquiry over this matter.’</p>
                <p>Subjects and predicate can be simple and complex, so length does not determine what a subject and a predicate is. <a href="/business-writing-class-chicago.php">Chicago business writing classes</a>.</p>
                <p>Complete sentences are advisable in business writing. Aside from subscribing to the more formal format typical in most business document, complete sentences are what make sense. </p>
                <h4>Punctuation</h4>
                <p>Punctuations are standard marks in writing used to separate words, clauses, and sentences. The use of punctuations can affect a text’s readability, flow, and even meaning. <a href="/business-writing-class-los-angeles.php">Los Angeles baed classes in Business Writing</a>.</p>
                <p>Commonly used punctuations include:</p>
                <ul>
                    <li><strong>Period</strong> (.) - used to end a sentence, indicating a full stop. Periods are also used after initials and abbreviations.</li>
                    <li><strong>Question Mark</strong> (?) - used after a question.</li>
                    <li><strong>Exclamation Point</strong> (!) - used after statements expressed with strong emotion.</li>
                    <li><strong>Comma </strong>(,) - used to separate items in a series. Also used before and, but, or, nor, for, so, and yet, when they join independent clauses (unless the clauses are short). It is also used to separate items that interrupt a series. </li>
                    <li><strong>Colon</strong> (:) - used to mean “note what follows,” and is typically succeeded by an elaboration, summation, interpretation of what it precedes. </li>
                    <li><strong>Apostrophe</strong> (‘) - used to show belonging or to indicate the omission of letters in a word.</li>
                    <li><strong>Semicolon</strong> (;) - used to link independent clauses not joined by a coordinating conjunction. As a rule, use a semicolon to end complete sentences in cases where you’re not indicating a full stop.</li>
                </ul>
                <p>Visit our <a href="/resources/business-skills.php">Business Skills Resources Library</a>.</p>
                <h4>Types of Sentences</h4>
                <p>Four Kinds of Sentences:</p>
                <ol>
                    <li><strong>Declarative: </strong>The most commonly used sentence type in business writing, these are sentences that make a statement. They end with a period.</li>
                </ol>
                <p>&nbsp;Example: We are writing to inform you that your account would be expiring in ten days.</p>
                <ol>
                    <li><strong>Interrogative: </strong>These are sentences that ask a question. They end in a question mark. Interrogative questions don’t necessarily follow the format of subject + predicate.</li>
                </ol>
                <p>Example: Would you be format renewing your account this year?</p>
                <ol>
                    <li><strong>Imperative: </strong>These are sentences that give a command or make a request. They usually end with a period, though sometimes they can end with an exclamation point (although to do so is not recommended in business writing).</li>
                </ol>
                <p>Imperative sentences are advisable when you’re making a ‘to-do’ list, creating an agenda or are outlining an instructional manual.</p>
                <p>Example: Please inform Joseph that we would be expecting his payment on Monday.</p>
                <ol>
                    <li><strong>Exclamatory: </strong>These are sentences that express strong feeling. They usually end with an exclamation mark.</li>
                </ol>
                <p>Example: Congratulations for getting promoted to Vice-President!</p>

                <p>Also read <a href="/business-writing/proposals.php">How to write a Business Proposal</a> </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">On-site Business Writing training</h4>
                    <p>Through our network of local trainers we deliver on-site Business Writing classes right across the country. Click to obtain a <a href="/onsite-training.php">quote for onsite training in business writing</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=29">Business writing testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>