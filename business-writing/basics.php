<?php
$meta_title = "Spelling and Grammar Tips | Training Connection";
$meta_description = "Learning how to avoid common spelling and grammar mistakes. This and other topics are covered in our Business Writing training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-writing.php">Business Writing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Spelling and Grammar</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Spelling and Grammar Tips</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>The building blocks of  writing, whether for business or social purposes, are words. Failure to use words properly can affect the overall impact of your prose. In this article we will discuss the spelling of words and grammar issues in writing.</p>

                <p>Need to improve your  Business writing skills? Why not join one of   <a href="/business-writing-class.php">our business writing workshops</a>. Call us on  888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-writing/grammar.jpg" width="750" height="487" alt="Spelling and Grammar lesson"></p>
                <h4>Spelling</h4>
                <p>The use of correctly spelled words is important in all business writing because you are presenting a professional document. A misspelled word can reflect negatively on your image. It may also result in confusion in meaning. <a href="/business-writing-class-los-angeles.php">Los Angeles business writing workshops</a>.</p>
                <p>Here are some tips to improve spelling issues when writing:</p>
                <ol>
                    <li>
                        <p>Familiarize yourself with commonly misused words, particularly sets of words often mistaken for each other.</p>
                        <p>Example: <em>Affect</em> vs. <em>Effect</em></p>
                        <p>Affect is to influence or change. (Our income has been affected by the global recession.)</p>
                        <p>Effect is the impression, result. It can also mean ‘to cause’. (The global recession has a dramatic effect on our income.)</p>
                        <p>This problem also happens with pronouns or pronoun-linking verb contractions which sound alike. Examples: <em>who’s</em> vs. <em>whose</em>, <em>their</em> vs. <em>they’re</em> and <em>your</em> vs. <em>you’re</em>.</p></p>
                    </li>
                    <li>Make sure you pronounce words properly. Colloquial pronunciations can cause people to omit certain letters in writing. Example: writing ‘<em>diffrence</em>’ instead of ‘<em>difference</em>’ because one pronounces this word with a silent first e.</li>
                    <li>Note some friendly rules on spelling. Example:<em> i</em> before <em>e,</em> except after <em>c</em> (e.g. receive, belief)</li>
                    <li>If you’re writing for an international audience, note that there are acceptable spelling variations in the different kinds of English. For example, American and British English tend to have many differences in the spelling of the same words. Notable are the use of -<em>ou</em> instead of -<em>o</em>, as in <em>colour</em> vs. <em>color</em>; -<em>re</em> instead of -<em>er</em>, as in <em>centre</em> vs. <em>center</em>; -<em>ise</em> instead of -<em>ize</em>, as in <em>realise</em> vs. <em>realize</em>.</li>
                    <li>Lastly, use spelling resources! These days, spell checking is as easy as running a spell check command on your word processing software. If you’re still uncertain after an electronic spell check, consult a dictionary.</li>
                </ol>

                <p>Also see <a href="http://crambler.com/10-commonly-misused-words-and-phrases/">10 commonly misused Words and Phrases</a>. </p>

                <h4>Grammar</h4>
                <p> Grammar details rules of language syntax. Like spelling issues, grammar violations in a business document can reflect negatively on a professional or a company. Care should be given that all business documents are grammatically correct. <a href="/business-writing-class-chicago.php">Chicago business class on writing</a>.</p> <p>Here are two grammar issues most business writers have trouble with.</p> <p>NOTE: All grammatical rules discussed here have exceptions and complex forms. </p>
                <ol>
                    <li>
                        <p><strong>Subject-verb agreement</strong>: Singular subjects go with singular verbs, and plural subjects go with plural verbs. The singular form of most subjects contains the suffix -<em>s</em> or -<em>es</em>. The opposite is true for verbs; it’s the singular verbs that end with -<em>s</em>.</p>
                        <p>Note though that some subjects have unusual plural forms (e.g. <em>medium- media, man-men</em>, etc.)</p>
                    </li>
                    <li><strong>Verb tenses</strong>: Modern English has six tenses, each of which has a corresponding continuous tense. The first three: present, past and future are less problematic. </li>
                </ol>

                <p>The other three tenses, <strong>perfect</strong><strong>, <strong>past perfect</strong></strong>, and <strong>future perfect</strong><strong>,</strong> are formed with the helping verbs <em>have, has</em>, and <em>had</em>.
                    <strong>Perfect tense is </strong>used to express an event that happened in the past, but still has an effect on the present. Example: Mr. Michael Johnson <em>has managed</em> this company for the past 5 years.
                    <strong>Past perfect</strong> tense is used to express an event that took place before another action, also in the past. Example: <em>Mr. Myers had been sitting on a meeting when the client called.</em>
                    <strong>Future perfect</strong><strong> </strong>tense is<strong> </strong>used to express an event that will have taken place at some time in the future. Example: I will <em>have finished</em> by 10pm.</p>

                <p>In business writing, there are standard tenses used depending on the type of document you are writing. Business cases  may be written in past or future tense depending on whether the purpose is to discuss how a project was executed, or propose how it would be executed.</p><p>Verb tenses can also vary within the same business document. The Organization Overview section of a proposal may be written in perfect tense, while the Financial Projection Section written in present tense. </p>
                <p>Visit our <a href="/resources/business-skills.php">Business Skills Resources Library</a>.</p>
                <h4>Emails and Acronyms</h4>
                <p> While online mediums of communication have developed their own vocabulary, it’s best to remember that business emails should have the  same formality as any business letter.</p><p>Here are some key things to remember with regards to grammar and the use of acronyms in an email. </p>
                <ul>
                    <li>Always follow the rules of good grammar. You may refer to English writing style guides for these rules. </li>
                    <li>Always use full sentences and words with proper sentence structure. Don’t use text-speak. </li>
                    <li>Example: use “<em>The reports are due on Monday</em>.” instead of “<em>D reports r due Mon</em>”</li>
                    <li>Proper capitalization and punctuation are a must! In email, all caps give the impression that you’re shouting, and small caps are hard to read. Example: use “<em>The report should include an evaluation report</em>.” Instead of “<em>The report SHOULD INCLUDE AN EVALUATION REPORT</em>.</li>
                    <li>In business emails, avoid text-speak abbreviations such as BTW (by the way), IMHO (In my honest opinion,) and LOL (laugh out loud). Avoid the use of emoticons, as well.</li>
                </ul>
                <p>Also read <a href="/business-writing/proposals.php">How to write a Business Proposal</a> </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">On-site Business Writing training</h4>
                    <p>Through our network of local trainers we deliver on-site Business Writing classes right across the country. Click to obtain a <a href="/onsite-training.php">quote for onsite training in business writing</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=29">Business writing testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>