<?php
$meta_title = "How to communicate better via Email | Training Connection";
$meta_description = "Learn how to write fast and effective emails that get read. This and other topics are covered in our Business Writing training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bs-bw">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-writing.php">Business Writing</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Emails</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Fast and Effective Emails</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Email writing was intended for faster communication. How fast can you make your point? Considering your reader’s experience with email can make the difference of whether a person takes action when receiving your message. In this article, we research and adapt for an audience always on the go!</p>
                <p>Need to improve your  Business writing skills? Why not join one of   our <a href="/business-writing-class.php">business writing classes in Chicago or LA</a>. Call us on  888.815.0604 for more information.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-writing/effective-email.jpg" width="780" height="520" alt="How to write Effective Email"></p>

                <p>Here are some great but sobering facts about your readers:</p>
                <ul>
                    <li>The average professional sends and receives 122 business emails each day. By 2019, this number is expected to climb to 126 emails per day. </li>
                    <li>A case study by the Danwood Group found that it takes 1.5 to read and recover from each email.</li>
                    <li>Participants in a study by&nbsp;<a href="http://dx.doi.org/10.1207/s15327590ijhc2103_3">Renaud et al. (2006)</a>&nbsp;claimed to check their email, on average, once an hour. However when the researchers spied on them, it turned out they checked their email every five minutes.</li>
                    <li>The average employee spends 13 hours a week reading and responding to mail. This means roughly 30% of their work me is devoted to managing their inbox. </li>
                </ul>
                <p>More about <a href="http://www.emailmonday.com/mobile-email-usage-statistics">email statistics</a>. </p>
                <p>These facts say a lot about your audience: </p>
                <ul>
                    <li>Your emails must stand out</li>
                    <li>Your emails must be clear enough for the reader to determine action</li>
                    <li>Your emails are being noticed yet ignored</li>
                    <li>Your emails add to other’s workload</li>
                </ul>
                <p>Let’s consider what you can do to ensure your writing is received and acted upon. </p>
                <h4>Your Emails Must Stand Out</h4>
                <p>When viewing a list of emails to respond to, your reader can see the author’s name and the topic. It is your subject line that helps identify the task and timing of the email task you are sending. <a href="/business-writing-class-chicago.php">Chicago business writing and grammar classes</a>.</p>
                <p>If your subject line has “Re: Friday” this only means that the topic is an ongoing discussion yet it does not give direction or assist with urgency. </p>
                <p>Hopefully, we know better than to list every email as URGENT. Instead, make it easy for your readers by updating your subject line with topic requiring action and a due date.</p>
                <p>For example, change “Re: Friday” to “File update for Nov 27th Client Meeting”. </p>
                <p>This reminds the reader if this is something they need to handle now, later today or whether they can defer and schedule a task with that email during their designated email reading time.
                </p>
                <h4>Your Emails Must Be Clear Enough For The Reader To Determine Action</h4>
                <p>Unlike creative writing, you do not need to impress your audience with fancy language or a compelling story. Instead, think of your writing as more of a relay race. Your email’s point has to be clear upfront so that the reader can quickly categorize how to respond. </p>
                <p>Instead of starting with anything that would be followed by “Lately, …..” Write, “I am writing to request _____.” You can explain later in the email. Email preview views in Outlook and Google count on this! </p>
                <p>Since we all communicate and read so much online, most people see a headline and can click if they want more information. </p>
                <p>Consider this:</p>
                <p align="center"><em>67,2% of consumers use a smartphone to check their email, 42,3% use a tablet while 93,3% uses desktop environment. –&nbsp;BlueHornet&nbsp;“Customer Views of email marketing 2015” (2015).
                        Replies sent from mobile devices are 60% shorter than those sent from desktops. Yahoo&nbsp;“Evolution of Conversations in the Age of Email Overload” (April 2015)</em>
                </p>
                <p>So if your reader can assess your request, they can respond faster and remember not to take it personal if the response is short! </p>

                <p>Visit our <a href="/resources/business-skills.php">Business Skills Resources Library</a>.</p>
                <h4>Your Emails Are Being Noticed Yet Ignored</h4>
                <p>The Bees wrote a song, “How deep is your love?” When it comes to emails, “How short is your email?” The longer a person determines they will need to read, the sooner they defer the email. Once a person opens your email, you have their attention for at least 4 seconds!</p>
                <p><a href="/business-writing-class-los-angeles.php">Business Writing training offered in Los Angeles</a>.</p>
                <p>Ideally, your email needed be longer than three or four lines with a space between each line.
                </p>
                <ul>
                    <li>Greeting</li>
                    <li>Why you are writing</li>
                    <li>Any extra detail</li>
                    <li>How to reach you with questions</li>
                </ul>
                <p>The best tip here is to press enter after you have finished your point on these areas. Email writing fields use single spacing which is very uncomfortable for the eye to sort. If your thought is longer than two lines, press enter for the next simple sentence. Use bullet points instead of detailed paragraphs when possible.</p>
                <h4>Your Emails Add To Other's Workload</h4>
                <p>Emails at work are TASKS. Tasks are items that need to be handled according to priority and timeliness. You can avoid adding other challenges for your readers by double checking these below before pressing send:</p>
                <ul>
                    <li>Always list any attachments you attached and check before you send your email</li>
                    <li>Review ‘Send To’ field to ensure that all and only relevant parties will receive the email</li>
                    <li>Ensure you listed your preferred contact information at the bottom of the email for external emails </li>
                    <li>Reread your email, especially if sending via portable devices</li>
                    <li>Avoid sending extra thank you after you receive a thank you in response to your email.</li>
                </ul>
                <p>Getting these habits to stick may take work and a check list. To improve your team communication or your own at work, sign up for a group or private session on Business Writing for Positive Impact class at Training Connection! </p>

                <p>Also read <a href="/business-writing/proposals.php">How to write a Business Proposal</a> </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bs-bw" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">On-site Business Writing training</h4>
                    <p>Through our network of local trainers we deliver on-site Business Writing classes right across the country. Click to obtain a <a href="/onsite-training.php">quote for onsite training in business writing</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=29">Business writing testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>