<?php
$meta_title = "Building JavaScript Arrays | Training Connection";
$meta_description = "Understand how to correctly build and use arrays in JavaScript. This and other JavaScript topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-javascript">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Building JavaScript Arrays</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Build Up Your Data - Building and Using Arrays in JavaScript</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Learning to  build and use Arrays correctly is a fundamental part of your JavaScript journey. So what is an Array and how we deal with them?  An Array is a collection of values.  Think of it as a variable that can store multiple items.  For example, in a variable we could store the name of one person:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> name <span class="token operator">=</span> <span class="token string">'Jeff'</span><span class="token punctuation">;</span></code></pre>

                <p>But, in an array we can store multiples names:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> names <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token string">'Jeff'</span><span class="token punctuation">,</span> <span class="token string">'Kim'</span><span class="token punctuation">,</span> <span class="token string">'George'</span><span class="token punctuation">,</span><span class="token string">'Lucy'</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>We'll get to the syntax of building an array in a bit but in the example above the variable 'names' is storing more than just one string.  Arrays can also store numbers:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> numbers <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token number">4</span><span class="token punctuation">,</span> <span class="token number">2</span><span class="token punctuation">,</span> <span class="token number">7</span><span class="token punctuation">,</span> <span class="token number">9</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>And, you even can mix the types of values:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> items <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token string">'Julie'</span><span class="token punctuation">,</span> <span class="token number">3</span><span class="token punctuation">,</span> <span class="token string">'Tom'</span><span class="token punctuation">,</span> <span class="token number">7</span><span class="token punctuation">,</span> <span class="token number">9</span><span class="token punctuation">,</span> <span class="token string">'Kathy'</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>We are not limited to just four values like our examples.  We can store as many values as needed in our Arrays.</p>
                <p>Ok, so what is this syntax we are seeing?  You start just like you would a variable but instead of declaring a value you assign a list of values inside a pair of square brackets.  Each value is separated by a comma.</p>
                <p>You may also see Arrays created like this:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> items <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Array</span><span class="token punctuation">(</span><span class="token string">'Jeff'</span><span class="token punctuation">,</span> <span class="token string">'Kim'</span><span class="token punctuation">,</span> <span class="token string">'George'</span><span class="token punctuation">,</span> <span class="token string">'Lucy'</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>

                <p>This technique also creates an Array but it is more verbose and can create some interesting issues. If we create an Array with numeric values like this we are fine:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> numbers <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Array</span><span class="token punctuation">(</span><span class="token number">4</span><span class="token punctuation">,</span> <span class="token number">2</span><span class="token punctuation">,</span> <span class="token number">7</span><span class="token punctuation">,</span> <span class="token number">9</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>

                <p>But if we try to create a single value Array using numbers we get an empty Array with that many placeholders:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> numbers <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Array</span><span class="token punctuation">(</span><span class="token number">40</span><span class="token punctuation">)</span><span class="token punctuation">;</span></code></pre>

                <p>This creates an Array named 'numbers' that has 40 values, all null or unassigned.  Using our bracket notation, we do have this problem:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> numbers <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token number">40</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>That creates a 'numbers' Array with a single value of 40.</p>
                <p>In our examples we also assigned the new Array with values, in the brackets.  Much like variables, you don't have to assign values immediately upon creating the Array.  You could initialize the Array without assigning values like this:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> names <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>Later we can come back to the Array and assign values, but first we need to understand how we access those values in the Array.</p>
                <p>We can't just write the Array name to the document like we would a variable.  We have to tell the script which value in the Array we want to retrieve.  Let's take our 'names' Array as an example:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> names <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token string">'Jeff'</span><span class="token punctuation">,</span><span class="token string">'Kim'</span><span class="token punctuation">,</span><span class="token string">'George'</span><span class="token punctuation">,</span><span class="token string">'Lucy'</span><span class="token punctuation">]</span><span class="token punctuation">;</span></code></pre>

                <p>If we would like to access the second name in the Array we would write this:</p>
                <pre class="block-code"><code>names<span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">]</span></code></pre>

                <p>We indicate the Array item we want in square brackets immediately after the name of the Array.  Why not a number 2 in our example?  Good question.  You would think that if we want the second object we would write: names[2], right?  But Arrays use indices, or index numbers, to indicate which positions the item is assigned.  In programing we always start counting with the number 0.  So you see, the first position is index 0, the second position is index 1, and so on.</p>
                <p>Here is how the Array might look to the program.  Not entirely precise but good enough for our example.</p>
                <pre class="block-code"><code>names<span class="token punctuation">[</span><span class="token string">'Jeff'</span><span class="token punctuation">,</span> <span class="token string">'Kim'</span><span class="token punctuation">,</span> <span class="token string">'George'</span><span class="token punctuation">,</span> <span class="token string">'Lucy'</span><span class="token punctuation">]</span>
<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span>    <span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">]</span>     <span class="token punctuation">[</span><span class="token number">2</span><span class="token punctuation">]</span>       <span class="token punctuation">[</span><span class="token number">3</span><span class="token punctuation">]</span></code></pre>

                <p>Below that I put the index number of each item.  Can you see the correlation? So names[1] would produce 'Kim'.</p>
                <p>Now that we understand how to access a particular position's value in Array we can see that we can also change or assign that position a value:</p>
                <pre class="block-code"><code>names<span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token string">'Tom'</span><span class="token punctuation">;</span></code></pre>

                <p>Now our Array looks like this:</p>
                <pre class="block-code"><code>names<span class="token punctuation">[</span><span class="token string">'Jeff'</span><span class="token punctuation">,</span> <span class="token string">'Tom'</span><span class="token punctuation">,</span> <span class="token string">'George'</span><span class="token punctuation">,</span><span class="token string">'Lucy'</span><span class="token punctuation">]</span></code></pre>

                <p>'Tom' replaced 'Kim' in index 1.  Remember when we created the Array without assigning values?  Now we know how to assign values if the Array wasn't initialized with them:</p>
                <pre class="block-code"><code><span class="token keyword">var</span> names <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

names<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token string">'Mike'</span><span class="token punctuation">;</span>
names<span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token string">'Sally'</span><span class="token punctuation">;</span>
names<span class="token punctuation">[</span><span class="token number">2</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token string">'Steve'</span><span class="token punctuation">;</span>
names<span class="token punctuation">[</span><span class="token number">3</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token string">'Chris'</span><span class="token punctuation">;</span></code></pre>

                <p>Whew, that took a little longer than creating the values when the Array was initialized.  So, why would we use this technique instead?  This is useful if we don't know what these values are going to be when the program starts running.  We may need to assign these later based on other data, a calculation, or user input.</p>
                <p>Now that we know a little about creating Arrays let's look at a few more examples:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/NNOjww?height=400&amp;theme-id=0&amp;slug-hash=NNOjww&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="400" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_NNOjww"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Notice how we wrote the Array in the example above.  For long sets of values it is common to put each value on its own line, but also notice the closing parenthesis is on a separate line at the end.  This is considered good coding practice that helps the programmer see exactly where the Array set ends.  Here we can also see how the Array is used in our 'document.write' statement. <a href="/javascript-training-los-angeles.php">LA-based Javacript classes</a>.</p>
                <p>Here is an example of using information entered by the user and storing it in an Array (click the CodePen box to load and run the example, you will be prompted to answer a few questions that will be used in the Array):</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/preview/EKdmLz?height=450&amp;theme-id=0&amp;slug-hash=EKdmLz&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;preview=true&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="450" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EKdmLz"></iframe></div>
                <script async="" src="http://assets.codepen.io/assets/embed/ei.js"></script>
                <p>Here we initialized the Array without values so we can ask the user for some information and store it in the Array.  We then used those values in a 'document.write' statement.  You can see here we were also able to write the 'document.write' statement on multiple lines but note, we can only break the line before or after a concatenation symbol (+), not in the middle of a string.  This helps to make complicated statements more readable for the developer. <a href="/javascript-training-chicago.php">Chicago JavaSript course</a>.</p>
                <p>Unlike variables, Arrays have a property associated to them call the 'length'.  We can use the 'length' property to discover how many values the Array is storing. This is very useful for those times when we are allowing the user's input to build the Array, or an outside resource.  We access properties by attaching the property name to the Array name using the dot(.) syntax.  Observe:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/xVyYbm?height=350&amp;theme-id=0&amp;slug-hash=xVyYbm&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="350" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_xVyYbm"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>We can also use the length property to add a value at the end of an Array because the length would be the next available index at the end:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/preview/zqmRvJ?height=266&amp;theme-id=0&amp;slug-hash=zqmRvJ&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;preview=true&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="266" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_zqmRvJ"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>The 'toString' code you see in this script is an example of a method you can use on Arrays.  To learn more about methods see the article, '<a class="javascript" href="https://www.trainingconnection.com/javascript/lessons/arrayMethods.php">Get Methodical - Using Array Methods in JavaScript</a>'.</p>
                <p>Learning to build Arrays properly will help us store and use larger sets of data without creating, and coming up with, multiple variables. If you would like to learn more about using Arrays and the JavaScript language visit <a class="javascript" href="/javascript-training.php">JavaScript Training Courses</a> on our website for some excellent classes on the subject.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-javascript" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other JavaScript resources</h4>
                    <ul>
                        <li><a href="/javascript/lessons/buildingArrays.php">Building Arrays using JavaScript</a></li>
                        <li><a href="/javascript/lessons/arrayMethods.php">Working with Array Methods </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite JavaScript training</h4>
                    <p>Through our network of local trainers we deliver onsite JavaScript classes right across the country. Obtain a <a href="/onsite-training.php">quote for onsite JavaScript training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=48">Javascript testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>