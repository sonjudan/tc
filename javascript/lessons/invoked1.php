<?php
$meta_title = "Understanding Immediately-Invoked Function Expressions - Part 1 | Training Connection";
$meta_description = "Part 1 - How to use immediately-invoked function expression in JavaScript. This and other JavaScript topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-javascript">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item active" aria-current="page">JavaScript IIFE</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Understanding Immediately-Invoked Function Expressions - Part 1</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>You may have heard the term "immediately-invoked function expression" but may be unfamiliar with what they actually are or how they're used.</p>
                <p>An immediately-invoked function expression (or IIFE, pronounced 'iffy' for short) is a Javascript pattern to create a private scope within the current scope. This new scope may or may not be persistent (a closure). The technique uses an anonymous function inside parentheses to convert it from a declaration to an expression, which is executed immediately.</p>
                <p>The pattern has also been referred to as a "self-invoked (or executed) anonymous function", but the term IIFE was coined by Ben Altman as a more accurate name for the pattern.</p>
                <h4>The basic concept</h4>
                <p>Suppose we had the following code in our program:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/QKAjzN?height=187&amp;theme-id=0&amp;slug-hash=QKAjzN&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="187" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_QKAjzN"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>When we call x, we see that 100 is dumped to the console. Because of the way Javascript's scope chain works, x's scope object is linked to the scope in which it was defined; Therefore x has access to a's value. The value of y is not accessible outside of x, as the scope chain allows access from inner to outer scopes only, thus x's scope is private. <a href="/javascript-training-chicago.php">JavaScript and jQuery classes in Chicago</a>.</p>
                <p>Of course, this is simply a function declaration, not an IIFE, which is why we need to call our function separately after declaring it. Function declarations can't be executed using the invocation operator () after it. The following code produces a syntax error:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/bwrVJV?height=167&amp;theme-id=0&amp;slug-hash=bwrVJV&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="167" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_bwrVJV"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>However, if we were to put the function declaration inside of parentheses, we turn the declaration into an expression, which can then be immediately invoked:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/qaYOGy?height=163&amp;theme-id=0&amp;slug-hash=qaYOGy&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="163" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_qaYOGy"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Enclosing both the declaration as well as the invocation operator inside parentheses also works, and is the style Douglas Crockford recommends:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/WGJQqw?height=165&amp;theme-id=0&amp;slug-hash=WGJQqw&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="165" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_WGJQqw"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Also, since we're immediately running this function as a one-off deal, there's no need to name it, therefore an anonymous function works nicely:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_5" src="https://codepen.io/jwiatt/embed/mALZkk?height=168&amp;theme-id=0&amp;slug-hash=mALZkk&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_5" scrolling="no" frameborder="0" height="168" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_mALZkk"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Although the scope chain allows us to access the outer scope implicitly to get access to a, a clearer and better approach would be to use dependency injection practices and explicitly pass the required values into the IIFE:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_6" src="https://codepen.io/jwiatt/embed/jrxbgV?height=171&amp;theme-id=0&amp;slug-hash=jrxbgV&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_6" scrolling="no" frameborder="0" height="171" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_jrxbgV"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <h4>Great, but so what?</h4>
                <p>So what's the benefit to doing this? This pattern is useful when you want to do some processing but don't want to clutter up the current scope (especially the global scope) with a bunch of objects. The IIFE scope is private since it can't be accessed from the outside, private data can be made persistent using closures, and any non-persistent objects get cleaned up when the IIFE finishes execution. <a href="/javascript-training-los-angeles.php">Instructor-led JavaScript classes in Los Angeles</a>.</p>
                <h4>A simple example</h4>
                <p>Suppose we want to implement a counter, but we don't want our counter variable to be exposed. We can use an IIFE to privatize the data:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_7" src="https://codepen.io/jwiatt/embed/BLxjBN?height=224&amp;theme-id=0&amp;slug-hash=BLxjBN&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_7" scrolling="no" frameborder="0" height="224" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_BLxjBN"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Each time the button is clicked, the counter is incremented. Meanwhile, the counter variable is not accessible from outside the IIFE, so its value stays private. Also, because we have created a reference to counter outside the IIFE scope (inside the function attached to the button handler), we have created a closure, and so the IIFE's scope object is not destroyed once it finishes executing, and so counter is now persistent.</p>
                <p>This also works with multiple items:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_8" src="https://codepen.io/jwiatt/embed/gwzPOY?height=301&amp;theme-id=0&amp;slug-hash=gwzPOY&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_8" scrolling="no" frameborder="0" height="301" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_gwzPOY"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Note that if we want to make this reusable we can simply use a named function instead of an IIFE:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_9" src="https://codepen.io/jwiatt/embed/PGZkqw?height=264&amp;theme-id=0&amp;slug-hash=PGZkqw&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_9" scrolling="no" frameborder="0" height="264" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_PGZkqw"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Note that in the last example, we used an assignment rather than a function declaration. Assignment operators already expect an expression, so enclosing the function in parentheses is optional. Some people recommend you do so for consistency, but most of the time I see the parentheses omitted.</p>
                <p><a class="javascript" href="https://www.trainingconnection.com/javascript/lessons/invoked2.php" rel="next">In Part 2</a> we'll look at some other common ways to use IIFEs in your applications.</p>
                <p>You can learn more about this and other advanced techniques in our <a class="javascript" href="/javascript-training.php">JavaScript training course</a>.
                    To view a sample of  our past students testimonials, please click on the following link: <a class="javascript" href="/testimonials.php?course_id=48">JavaScript Class testimonials</a>. </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-javascript" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other JavaScript resources</h4>
                    <ul>
                        <li><a href="/javascript/lessons/buildingArrays.php">Building Arrays using JavaScript</a></li>
                        <li><a href="/javascript/lessons/arrayMethods.php">Working with Array Methods </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite JavaScript training</h4>
                    <p>Through our network of local trainers we deliver onsite JavaScript classes right across the country. Obtain a <a href="/onsite-training.php">quote for onsite JavaScript training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=48">Javascript testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>