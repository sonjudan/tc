<?php
$meta_title = "JavaScript Variable Scope | Training Connection";
$meta_description = "Understand how variable scope works in JavaScript. This and other JavaScript topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-javascript">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Javascript Variable Scope</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Scope It Out - JavaScript Variable Scope</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>You know what a function is and you know how to create variables.  Did you know that a variable behaves differently when it is used inside a function?  Specifically, we are talking about variable scope.  No, not the mouthwash for variables, although sometimes I feel like my variables could use a good cleaning.  We are talking about the scope, as in the range of the effect of a variable.</p>
                <p>What do we mean by this?  There are two types of variables when we are discussing scope, global and local.  A global variable's value is accessible by the entire program, no matter where the variable is called or assigned.  Global variables can be accessed inside of functions as well as outside.  On the other hand, local variables can only be accessed by the functions that created them, where they are declared.</p>
                <p>Variable declaration is the key here so let's be clear on what we mean.  You declare a variable when the variable name is following the "var" method:</p>
                <p>This would create (declare) a variable with the name "firstName" without a value, or the value of null.</p>
                <pre class="block-code"><code><span class="token keyword">var</span> firstName<span class="token punctuation">;</span></code></pre>
                <p>This would declare the same variable but this time assign it with the string value of "Jeff".</p>
                <pre class="block-code"><code><span class="token keyword">var</span> firstName <span class="token operator">=</span> <span class="token string">'Jeff'</span><span class="token punctuation">;</span></code></pre>

                <p>You could also create the same variable without the "var" method.</p>
                <pre class="block-code"><code>firstName <span class="token operator">=</span> <span class="token string">'Jeff'</span><span class="token punctuation">;</span></code></pre>

                <p>Whether you use the "var" method or not isn't just a matter of convenience, although some developers thought that JavaScript just didn't care and was sticking true to the nature of a loosely coded language.  There are actual differences in how JavaScript is creating the variable depending on whether the "var" method is used.</p>

                <p>When the "var" method is used the variable is treated differently depending on where the variable was created.  If the variable was created outside of a function in the main script, then the variable is created as a global variable.  Remember what we said about global variables?  They can be accessed from anywhere inside the script.</p>

                <p>If a variable is created using the "var" method inside a function, then it is created as a local variable that is accessible by that function only.   The variable's value cannot be accessed outside the function.</p>

                <p>Look at this example:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/EKdxrm?height=300&amp;theme-id=dark&amp;slug-hash=EKdxrm&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EKdxrm"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>When we call the 'firstName' variable inside the function to combine it with 'lastName' to create the value for 'fullName', the function can use it because it was created outside the function [line 1] and is therefore global.  The function can print 'fullName' because it was created inside the function [line 5].  But when we try to access the 'fullName' variable outside the function  we get an invalid object error.  The main script cannot see the 'fullName' variable because it is local to the function that created it, the "createName" function in this case. Notice, I had to write the string '&lt;br&gt;From Outside: ' in a seperate statement because the 'fullName' variable is not recognized and that statment fails.  If I had concatenated the string and the variable on in the same statement, the whole statement would fail and we would get no output from those statements. <a href="/javascript-training-los-angeles.php">Javascript classes offered in Los Angeles</a>.</p>

                <p>Take a look at a different variation of that script:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/wGYKXN?height=300&amp;theme-id=dark&amp;slug-hash=wGYKXN&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="300" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_wGYKXN"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>The difference is we created the "fullName" variable using the "var" method on line 2 without a value.  Within the function we assigned it a value by concatenating the variables "firstName" and "lastName" with a space in between just like the example above but, this time the variable was not created within the function so it is a global variable and accessible by the entire script, both inside and outside the function.</p>

                <p>There is one other nuance regarding variable declaration that we need to understand.</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/wGYKbb?height=330&amp;theme-id=dark&amp;slug-hash=wGYKbb&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="330" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_wGYKbb"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>Again, a slight change in our script.  Here we don't declare the "fullName" variable on line 2 and we don't use the "var" method inside the function.  Both write statements still have access to the variable because when we don't use the "var" method to declare a variable, JavaScript treats it as a global variable regardless of where is was created. </p>

                <p>This is an important distinction to understand.  It's not that JavaScript is such a loosely coded language that it didn't care whether the "var" method was used or not, but JavaScript is treating the creation of the variable differently.</p>

                <p>I have seen a lot of code that was created without using the "var" method at all, seeming to indicate that the method was irrelevant and could be left out without consequences.  As you can see by our examples, this is not true, and we can end up with some unexpected results when we use, or don't use, the "var" method correctly. <a href="/javascript-training-chicago.php">Chicago JavaScript classes</a>.</p>

                <p>Because it is always beneficial to create code that is clear and precise to the developers reading the code, we should not be creating variables without using the "var" method.  It is better practice to declare our variables that we want to be global at the top of the code, and declare our variables that we want to be local to a function at the top of the function.  Although it requires a little more code to do this, our intention is immediately clear.</p>

                <p>In the previous example it is clear that our intention is for the "firstName" and "fullName" variables to be global because they are declared outside the function [line 1 and 2] and the "lastName" variable is meant to be local because it is declared inside the function at the top.</p>

                <p>The benefit of using local variables inside our functions is to reduce the number of variable names we have to maintain.  We can reuse variables names that are just meant to temporarily store a value while the function runs.</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/yOReBO?height=400&amp;theme-id=dark&amp;slug-hash=yOReBO&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="400" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_yOReBO"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>We can use the variable "text" in multiple functions without worrying about the values sticking around from one function to the next.  If all variables were global we would have to create a different variable name for each function that needed a temporary place to store values, i.e. text1, text2, text3, text4, etc.  That not only creates more variables for us to track but also takes more space in memory for each new variable.</p>

                <p>Understanding and using global and local variables correctly will create much more efficient code and help the developer to quickly understand the intended use of the variable. If you would like to learn more about using variables and the JavaScript language visit <a class="javascript" href="https://www.trainingconnection.com/javascript-training.php">JavaScript Training Courses</a> on our website for some excellent classes on the subject.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-javascript" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other JavaScript resources</h4>
                    <ul>
                        <li><a href="/javascript/lessons/buildingArrays.php">Building Arrays using JavaScript</a></li>
                        <li><a href="/javascript/lessons/arrayMethods.php">Working with Array Methods </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite JavaScript training</h4>
                    <p>Through our network of local trainers we deliver onsite JavaScript classes right across the country. Obtain a <a href="/onsite-training.php">quote for onsite JavaScript training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=48">Javascript testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>