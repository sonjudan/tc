<?php
$meta_title = "JavaScript Array Methods | Training Connection";
$meta_description = "How to use Array Methods in JavaScript. This and other JavaScript topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-javascript">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item active" aria-current="page">JavaScript Array Methods</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Get Methodical - Using Array Methods in JavaScript</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>One of the most powerful features of an Array is the ability to use its built in methods. A method is a built-in function that acts upon an object, like an Array.  A useful method of an Array to know is the 'toString()' method which converts the values of an Array to a comma delimited string:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/QNZaaN?height=350&amp;theme-id=0&amp;slug-hash=QNZaaN&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="350" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_QNZaaN"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>One issue you can see above is that there is no space after each comma.  <a href="/javascript-training-los-angeles.php">Click here</a> for more about Los Angeles JavaScript training. You can get a better result by using the join() method.  The join() method allows you to declare what the separation delimiter will be:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/jqeYZM?height=400&amp;theme-id=0&amp;slug-hash=jqeYZM&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="400" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_jqeYZM"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>
                <p>Now, if you are like me, the little fact that my list is grammatically incorrect (no 'and' before the last item), might drive you crazy.  But, there is trick we can use to polish that up.  We turn to the pop() and push() methods for this.  The pop() method will remove and return the last value in the Array, and the push() method will add a new value at the end of the Array.  Take a look:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/bpmajE?height=600&amp;theme-id=0&amp;slug-hash=bpmajE&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_bpmajE"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>One point to remember, when we pop() the Array it removes the last value, so without putting the value back using push() we would not have the original data. <a href="/javascript-training-chicago.php">Chicago JavaScript training classes</a>.</p>
                <p>Similar to pop() and push() are shift() and unshift(). The shift() method removes and returns the first value in the Array and unshift() adds a value at the beginning:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/ONBQxp?height=650&amp;theme-id=0&amp;slug-hash=ONBQxp&amp;default-tab=js%2Cresult&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="650" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_ONBQxp"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>If you would like to learn more about using the Array Method and the JavaScript language visit <a class="javascript" href="/javascript-training.php">JavaScript Training Classes</a> on our website for some excellent classes on the subject.

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-javascript" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other JavaScript resources</h4>
                    <ul>
                        <li><a href="/javascript/lessons/buildingArrays.php">Building Arrays using JavaScript</a></li>
                        <li><a href="/javascript/lessons/arrayMethods.php">Working with Array Methods </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite JavaScript training</h4>
                    <p>Through our network of local trainers we deliver onsite JavaScript classes right across the country. Obtain a <a href="/onsite-training.php">quote for onsite JavaScript training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=48">Javascript testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>