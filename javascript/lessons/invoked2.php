<?php
$meta_title = "Understanding Immediately-Invoked Function Expressions - Part 2 | Training Connection";
$meta_description = "Part 2 - How to use immediately-invoked function expression in JavaScript. This and other JavaScript topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-javascript">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item active" aria-current="page">JavaScript IIFE</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-js.png" alt="Javascript">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Understanding Immediately-Invoked Function Expressions - Part 2</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In the <a href="/javascript/lessons/invoked1.php" rel="prev">first part of this series</a> we covered the basics of what an immediately-invoked function expression (IIFE) is and how it works using some simple examples. In this part we will look at some common ways IIFEs are used in applications.</p>

                <h4>The Module pattern</h4>
                <p>A common use for IIFEs is to create private members that can only be accessed using public methods. For example, consider the following object literal:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/ozVPzR?height=290&amp;theme-id=0&amp;slug-hash=ozVPzR&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="290" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_ozVPzR"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>Because all the members of the object literal are public, the count property can be tampered with easily, making this structure less than ideal.</p>

                <p>The Module pattern takes advantage of the scope chain by using an IIFE to create a private scope, and then returns an object containing the public methods:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/EgrZGz?height=342&amp;theme-id=0&amp;slug-hash=EgrZGz&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="342" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EgrZGz"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <h4>The Revealing Pattern</h4>
                <p>A variation of the standard Module pattern is called the "Revealing" Module pattern. The difference between the two is that all of the methods are defined in the local IIFE scope, and the returned object contains pointers to the methods to be exposed ('revealed') to the public interface:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/jrJvJx?height=409&amp;theme-id=0&amp;slug-hash=jrJvJx&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="409" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_jrJvJx"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>Whether you use the standard Module pattern or the Revealing pattern is mostly a matter of style than functionality. YMMV, or course, so where performance is tantamount, always test, test, test. <a href="/javascript-training-los-angeles.php">JavaScript training</a> in Los Angeles.</p>

                <h4>IIFEs as namespaces</h4>
                <p>IIFEs can also be used as a namespace strategy. A standard best practice is to avoid cluttering up the global scope with your objects. You can limit what you want exposed in the global scope (or even not have anything in the global scope at all, but I find it useful to at least have one name used, commonly my application singleton) by placing all your private data and functions inside the IIFE, while exposing just the public members you need:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/amraxZ?height=378&amp;theme-id=0&amp;slug-hash=amraxZ&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="378" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_amraxZ"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>In this example we create an IIFE and pass in our global App object explicitly instead of relying on the scope chain. This helps keep our namespace module stay loosely coupled from any external code. The way we pass the global object into the IIFE may look odd if you're not familiar with the syntax:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_5" src="https://codepen.io/jwiatt/embed/EgrZzB?height=108&amp;theme-id=0&amp;slug-hash=EgrZzB&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_5" scrolling="no" frameborder="0" height="108" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EgrZzB"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>Technically, all objects defined in the global scope are in fact members of the window object, so writing:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_6" src="https://codepen.io/jwiatt/embed/EgrZqq?height=108&amp;theme-id=0&amp;slug-hash=EgrZqq&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_6" scrolling="no" frameborder="0" height="108" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EgrZqq"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>Is the same as writing:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_7" src="https://codepen.io/jwiatt/embed/qavJBd?height=108&amp;theme-id=0&amp;slug-hash=qavJBd&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_7" scrolling="no" frameborder="0" height="108" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_qavJBd"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <p>The other part of the statement is using an 'or' to either use the current App object if it exists (and is not false) or an empty object. This is particularly useful if you've broken your code up into several files to make the design more modular. The first namespace module loaded will initialize App as an empty object, while subsequent modules simply extend the existing App. <a href="/javascript-training-chicago.php">JavaScript hands-on classes in Chicago</a>.</p>

                <p>A good example of this modular namespacing pattern is in developing jQuery plugins. jQuery best practices dictate that a plugin should only use a single name in the jQuery object. Therefore an IIFE is used to privatize the plugin data and methods, with a single public method added to the jQuery object:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3"><iframe name="cp_embed_8" src="https://codepen.io/jwiatt/embed/EgrdaY?height=258&amp;theme-id=0&amp;slug-hash=EgrdaY&amp;default-tab=js&amp;user=jwiatt&amp;embed-version=2&amp;name=cp_embed_8" scrolling="no" frameborder="0" height="258" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="CodePen Embed" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_EgrdaY"></iframe></div>
                <script async="" src="//assets.codepen.io/assets/embed/ei.js"></script>

                <h4>Conclusion</h4>
                <p>Immediately-invoked function expressions are extremely useful for creating privatized data and modularizing our applications. Keep in mind the following:</p>

                <ul>
                    <li>Not all IIFEs are closures. If there is no external reference to the objects defined in the IIFE scope then the objects are destroyed once the IIFE finishes executing.</li>
                    <li>Passing global objects' (or objects higher up in the scope chain) properties into the IIFE as parameters can increase performance by eliminating internal lookup delays.</li>
                    <li>IIFEs are particularly ideal for singleton-type use. If the function is to be reusable, use a named function (assigned or declared) instead.</li>
                </ul>

                <p>Happy IIFE-ing!</p>

                <p>You can learn more about this and other advanced techniques in our <a class="html" href="/javascript-training.php">Advanced JavaScript training</a> course.
                    To view a sample of  our past students testimonials, please click on the following link: <a class="html" href="/testimonials.php?course_id=48">JavaScript Class testimonials</a>. </p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-javascript" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Other JavaScript resources</h4>
                    <ul>
                        <li><a href="/javascript/lessons/buildingArrays.php">Building Arrays using JavaScript</a></li>
                        <li><a href="/javascript/lessons/arrayMethods.php">Working with Array Methods </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite JavaScript training</h4>
                    <p>Through our network of local trainers we deliver onsite JavaScript classes right across the country. Obtain a <a href="/onsite-training.php">quote for onsite JavaScript training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=48">Javascript testimonials</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>