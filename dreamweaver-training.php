<?php
$meta_title = "Dreamweaver training class | Los Angeles & Chicago";
$meta_description = "Adobe Dreamweaver classes in Chicago & Los Angeles. Don't settle for an average class! For great deals call 888.815.0604.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted" >
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/captivate-training.php" itemprop="url">
                    <span itemprop="title">Dreamweaver Training</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-Dw.jpg');">
    <div class="container">
        <div class="masterhead-copy">
            <h1 data-aos="fade-up" >Dreamweaver Training</h1>

            <div data-aos="fade-up" data-aos-delay="100">

                <h4>Instructor-led Dreamweaver classes in Chicago and Los Angeles</h4>

                <p>
                    Live face-to-face Adobe certified Dreamweaver training. <br>
                    This is NOT a webinar!
                </p>
            </div>

            <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll" data-aos="fade-up" data-aos-delay="200">
                <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                Book Course
            </a>
        </div>
    </div>
</div>


<div class="section section-instructors">
    <div class="container">
        <div class="section-heading mb-0" data-aos="fade-up">
            <h2 class="mb-4">Adobe Dreamweaver Classes</h2>

            <p>Our Dreamweaver classes are taught by live  instructors present in the classroom. They will teach you, <strong>step-by-step how to build a professional website</strong> using Adobe Dreamweaver.
            All classes are taught in brand new, state-of-the-art computer training labs in our Chicago and Los Angeles training centers.</p>

            <p class="pt-3"><strong>We include the following with each Dreamweaver class:</strong></p>
            <ul class="list-bullets inline pb-3">
              <li>Dreamweaver training manual</li>
              <li>Certificate of course completion</li>
              <li>6-month  free repeat.</li>
            </ul>
            <p>
                <strong>Book a Dreamweaver class today. All classes guaranteed to run!</strong><br>
                <a href="onsite-training.php">Onsite Dreamweaver training available countrywide</a>.
            </p>

            <span class="line-separator"></span>

            <h4>Meet our Dreamweaver Instructors</h4>

            <p>Our instructors are very experienced Web developers. With their knowledge and experience they will share with you a wealth of tips and tricks on how to use  Dreamweaver to build stunning websites.</p>
        </div>

        <div data-aos="fade-up">
            <div class="owl-carousel owl-theme owl-instructors instructors-list-block " data-aos="fade-up">
                <div class="list-block">
                    <a href=".modal-instrutor-profile-chris" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-chris.jpg')"></i>
                        <span>Chris</span>
                    </a>
                </div>
                <div class="list-block">
                    <a href=".modal-instrutor-profile-jeff" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-jeff.jpg')"></i>
                        <span>Jeff</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="section-book-course" class="section section-training-options pb-0">
    <div class="container">
        <div class="section-heading align-center mb-0" data-aos="fade-up">
            <h2>Dreamweaver Course Outline</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">

            <div class="accordion-classes g-text-Dw" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default pb-md-5">
                        <div class="post-img"><img src="/dist/images/icons/adobe-Dw.svg" alt="Dreamweaver 2019"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Dreamweaver 2019 Fundamentals
                                </a>
                            </h3>
                            <p>This course teaches beginners, step-by-step, how to create, build and maintain professional looking websites. The course is hands-on with lots of real world projects. No prior experience of Dreamweaver is required.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Dreamweaver 2019 Fundamentals" data-price="$1295">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/dreamweaver/Dreamweaver-Fundamentals.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-md btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="far fa-calendar-alt mr-2"></i>
                                        Timetable
                                    </button>
                                    <div class="dropdown-menu">

                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Dreamweaver 2019 Fundamentals">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable Chicago
                                        </a>
                                        <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="Dreamweaver 2019 Fundamentals">
                                            <i class="far fa-calendar-alt mr-2"></i>
                                            TimeTable LA
                                        </a>

                                    </div>
                                </div>

                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>3 Days</span>
                                <sup>$</sup>1295
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse border-bottom">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Customizing Your Workspace</h5>
                                        <ul>
                                            <li>Touring the workspace</li>
                                            <li> Switching and splitting views</li>
                                            <li> Design view</li>
                                            <li> Code view</li>
                                            <li> Split view</li>
                                            <li> Live view</li>
                                            <li> Live Code</li>
                                            <li> Inspect mode</li>
                                            <li> Working with panels</li>
                                            <li> Minimizing</li>
                                            <li> Selecting a workspace layout</li>
                                            <li> Working with Extract</li>
                                            <li> Adjusting toolbars</li>
                                            <li> Personalizing preferences</li>
                                            <li> Creating custom keyboard shortcuts</li>
                                            <li> Using the Property inspector</li>
                                            <li> Using the HTML tab</li>
                                            <li> Using the CSS tab</li>
                                            <li> Image properties</li>
                                            <li> Table properties</li>
                                            <li> Related files interface</li>
                                            <li> Using tag selectors</li>
                                            <li> Using the CSS Designer</li>
                                            <li> Element Quick View</li>
                                            <li> Element View</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">HTML Basics</h5>
                                        <ul>
                                            <li>What is HTML?</li>
                                            <li> Writing your own HTML code</li>
                                            <li> Understanding HTML syntax</li>
                                            <li> Inserting HTML code</li>
                                            <li> Formatting text with HTML</li>
                                            <li> Applying inline formatting</li>
                                            <li> Adding structure</li>
                                            <li> Writing HTML in Dreamweaver</li>
                                            <li> Frequently used HTML 4 tags</li>
                                            <li> Introducing HTML5</li>
                                            <li> What’s new in HTML5</li>
                                            <li> HTML5 tags</li>
                                            <li> Semantic web design</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">CSS Basics</h5>
                                        <ul>
                                            <li>What is CSS?</li>
                                            <li> HTML vs. CSS formatting</li>
                                            <li> HTML defaults</li>
                                            <li> Browser antics</li>
                                            <li> CSS box model</li>
                                            <li> Previewing the completed file</li>
                                            <li> Formatting text</li>
                                            <li> Cascade theory</li>
                                            <li> Inheritance theory</li>
                                            <li> Descendant theory</li>
                                            <li> Specificity theory</li>
                                            <li> Code Navigator</li>
                                            <li> CSS Designer</li>
                                            <li> Multiples, classes, and ids</li>
                                            <li> Applying formatting to multiple elements</li>
                                            <li> CSS shorthand</li>
                                            <li> Creating class attributes</li>
                                            <li> Creating id attributes</li>
                                            <li> Formatting objects</li>
                                            <li> Width</li>
                                            <li> Borders and backgrounds</li>
                                            <li> Positioning</li>
                                            <li> Height</li>
                                            <li> Margins and padding</li>
                                            <li> Normalization</li>
                                            <li> Final touches</li>
                                            <li> CSS3 overview</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Web Design Basics</h5>
                                        <ul>
                                            <li>Developing a new website</li>
                                            <li> What is the purpose of the website?</li>
                                            <li> Who is the audience?</li>
                                            <li> How do they get here?</li>
                                            <li> Working with thumbnails and wireframes</li>
                                            <li> Creating thumbnails</li>
                                            <li> Creating a page design</li>
                                            <li> Creating wireframes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating a Page Layout</h5>
                                        <ul>
                                            <li>Using the Welcome Screen</li>
                                            <li> Previewing your completed file</li>
                                            <li> Working with predefined layouts</li>
                                            <li> Working with the CSS Designer</li>
                                            <li> Working with type</li>
                                            <li> Using Edge Web Fonts</li>
                                            <li> Building font stacks with web fonts</li>
                                            <li> Specifying font size</li>
                                            <li> Creating a CSS background</li>
                                            <li> Adding a background image</li>
                                            <li> Adding other background effects</li>
                                            <li> Creating custom CSS styling</li>
                                            <li> Modifying existing content</li>
                                            <li> Adding new items to a navigation menu</li>
                                            <li> Styling a navigational menu</li>
                                            <li> Building semantic content</li>
                                            <li> Positioning elements with Element Quick View</li>
                                            <li> Inserting placeholder text</li>
                                            <li> Inserting HTML entities</li>
                                            <li> Validating HTML code</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Designing for Mobile Devices</h5>
                                        <ul>
                                            <li>Responsive design</li>
                                            <li> Media type property</li>
                                            <li> Media queries</li>
                                            <li> Media query syntax</li>
                                            <li> Previewing your completed file</li>
                                            <li> Working with media queries</li>
                                            <li> Identifying media queries</li>
                                            <li> Targeting media queries</li>
                                            <li> Targeting selectors</li>
                                            <li> Troubleshooting styles across media queries</li>
                                            <li> Using Element Quick View</li>
                                            <li> Adding rules to a media query</li>
                                            <li> Edge Inspect</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Templates</h5>
                                        <ul>
                                            <li>Previewing completed files</li>
                                            <li> Moving embedded CSS to an external file</li>
                                            <li> Creating a template from an existing layout</li>
                                            <li> Inserting editable regions</li>
                                            <li> Inserting metadata</li>
                                            <li> Producing child pages</li>
                                            <li> Creating a new page</li>
                                            <li> Adding content to child pages</li>
                                            <li> Updating a template</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Text, Lists, and Tables</h5>
                                        <ul>
                                            <li>Previewing the completed file</li>
                                            <li> Creating and styling text</li>
                                            <li> Importing text</li>
                                            <li> Creating semantic structures</li>
                                            <li> Creating headings</li>
                                            <li> Adding other HTML structures</li>
                                            <li> Creating lists</li>
                                            <li> Creating indented text</li>
                                            <li> Making it responsive</li>
                                            <li> Creating and styling tables</li>
                                            <li> Creating tables from scratch</li>
                                            <li> Copying and pasting tables</li>
                                            <li> Styling tables with CSS</li>
                                            <li> Styling table cells</li>
                                            <li> Controlling table display</li>
                                            <li> Inserting tables from other sources</li>
                                            <li> Adjusting vertical alignment</li>
                                            <li> Adding and formatting caption elements</li>
                                            <li> Making tables responsive</li>
                                            <li> Spell checking web pages</li>
                                            <li> Finding and replacing text</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Images</h5>
                                        <ul>
                                            <li>Web image basics</li>
                                            <li> Vector graphics</li>
                                            <li> Raster graphics</li>
                                            <li> Raster image file formats</li>
                                            <li> Previewing the completed file</li>
                                            <li> Inserting an image</li>
                                            <li> Adjusting image positions with CSS classes</li>
                                            <li> Working with the Insert panel</li>
                                            <li> Using the Insert menu</li>
                                            <li> Inserting non-web file types</li>
                                            <li> Copying and pasting images from Photoshop</li>
                                            <li> Adapting images to smaller screens</li>
                                            <li> Inserting images by drag and drop</li>
                                            <li> Making images responsive</li>
                                            <li> Optimizing images with the Property inspector</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Navigation</h5>
                                        <ul>
                                            <li>Hyperlink basics</li>
                                            <li> Internal and external hyperlinks</li>
                                            <li> Relative vs. absolute hyperlinks</li>
                                            <li> Previewing the completed file</li>
                                            <li> Creating internal hyperlinks</li>
                                            <li> Creating relative links in Design view</li>
                                            <li> Creating a home link</li>
                                            <li> Updating links in child pages</li>
                                            <li> Creating an external link</li>
                                            <li> Creating an absolute link in Live view</li>
                                            <li> Setting up email links</li>
                                            <li> Creating an image-based link</li>
                                            <li> Creating image-based links using Element View</li>
                                            <li> Creating text links using Element View</li>
                                            <li> Targeting page elements</li>
                                            <li> Creating internal targeted links</li>
                                            <li> Creating a link destination using an id</li>
                                            <li> Creating a destination link in Element View</li>
                                            <li> Targeting id-based link destinations</li>
                                            <li> Checking your page</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Adding Interactivity</h5>
                                        <ul>
                                            <li>Learning about Dreamweaver behaviors</li>
                                            <li> Previewing the completed file</li>
                                            <li> Working with Dreamweaver behaviors</li>
                                            <li> Applying a behavior</li>
                                            <li> Applying a Swap Image Restore behavior</li>
                                            <li> Removing applied behaviors</li>
                                            <li> Adding behaviors to hyperlinks</li>
                                            <li> Making it responsive</li>
                                            <li> Working with jQuery Accordion widgets</li>
                                            <li> Inserting a jQuery Accordion widget</li>
                                            <li> Customizing a jQuery Accordion</li>
                                            <li> Editing dynamic jQuery styling</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Web Animation and Video</h5>
                                        <ul>
                                            <li>Understanding web animation and video</li>
                                            <li> Previewing the completed file</li>
                                            <li> Adding web animation to a page</li>
                                            <li> Adding web video to a page</li>
                                            <li> Choosing HTML5 video options</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Publishing to the Web</h5>
                                        <ul>
                                            <li>Defining a remote site</li>
                                            <li> Setting up a remote FTP site</li>
                                            <li> Establishing a remote site on a local or network web server</li>
                                            <li> Cloaking folders and files</li>
                                            <li> Wrapping things up</li>
                                            <li> Putting your site online</li>
                                            <li> Synchronizing local and remote sites</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="inner-section-nobg">
    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>
</div>


<div class="section section-methodology">
    <div class="container">
        <div class="section-heading align-left mb-0">
            <h2 class="mb-4" data-aos="fade-up">Our Training Methodology </h2>
        </div>

        <div class="section-body copy" data-aos="fade-up" data-aos-delay="200">
          <p data-aos="fade-up" data-aos-delay="150">We don't just want you to learn Dreamweaver, <strong>we want you to leave our classes feeling fully  inspired.</strong></p>
            <img src="/dist/images/notebook-Dw.png" alt="Dreamweaver CC 2019 - Fundamentals Course Book" class="pull-right">
          <p data-aos="fade-up" data-aos-delay="150">On our Dreamweaver course you will learn to build a professional looking website from the ground up.<strong></strong></p>

          <p>Additional topics covered include:</p>
            <ul>
              <li>The latest Web design trends</li>
                <li> Best practices for fonts, CSS and content layout</li>
                <li> How to create a solid navigation that users understand</li>
                <li>Tips to turn your website into a marketing tool</li>
                <li>What search engines like Google are looking for when they index your website</li>
                <li>Previewing your website before going live</li>
                <li>Using the built-in FTP function to take your site live.</li>
            </ul>
        </div>
    </div>
</div>



<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/Dw/section-reviews.php'; ?>

<div class="section section-guarantee">
    <div class="container container-sm">
        <div class="section-heading">
            <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
            <p data-aos="fade-up" data-aos-delay="150">We recognize that Dreamweaver is a complex program, and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months.  <br>
            </p>
        </div>

        <div class="section-body">
            <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Our Dreamweaver class Guarantee">
            <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
            <p data-aos="fade-up" data-aos-delay="250">Dreamweaver is not for everyone. If you decide that after the first day in class that the software is not for you, you can leave the class and we will give you a complete refund.</p>
        </div>
    </div>
</div>


<div id="section-course-form" class="section section-course-form">
    <div class="container">
        <div class="section-heading w-auto">
            <h2 class="mb-3" data-aos="fade-up">Group Dreamweaver Training </h2>
            <p data-aos="fade-up" data-aos-delay="100">
                We offer group training in Dreamweaver. This can be delivered onsite at your premises, at our training center in Chicago or Los Angeles or via webinar.
                <br>
                <strong>Fill out the form below to receive pricing.</strong>
            </p>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="200">
            <form action="" class="form-group-training">

                <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                        <h3><input type="radio" name="training">
                            <i></i> Online Webinar</h3>

                    </label>
                </div>



                <div class="row">
                    <div class="col-md-6 pr-1 mb-3">
                        <h4>Step 2: Choose your Course/s</h4>

                        <div class="option-group-training inline-block">
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>Dreamweaver Fundamentals <span>3 Days</span></h3>
                            </label>
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>Custom Dreamweaver Fundamentals Course</h3>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Step 3: Enter Details</h4>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="No. of Trainees*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Company Name*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone no*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="City*">
                        </div>

                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Zip*">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<div class="section section-faq">
    <div class="container">
        <div class="section-heading mb-4">
            <h2 class="section-title">FAQ</h2>
        </div>

        <div class="card-columns card-faq-list">
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Do I need to bring my own computer?</h4>
                <div class="card-body">
                    <p>No, we provide both Windows or Mac computers, and our trainers are  able to provide tuition for both operating systems.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Do your Dreamweaver classes have a live trainer in the classroom?
                </h4>

                <div class="card-body">
                    <p>Yes. Our Adobe Dreamweaver classes are completely live-instructor led by qualified and passionate trainers. They are on hand to provide support, demonstrate concepts, and answer any questions you may have to assist your learning.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Where do your Adobe Dreamweaver training classes take place?
                </h4>
                <div class="card-body">
                    <p>We have two state-of-the-art Dreamweaver training centers, one located in Chicago (230 W Monroe, Suite
                        610, Chicago IL 60606) and the other in Los Angeles (915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017).
                        If you prefer an <a href="#section-course-form" class="js-anchor-scroll">onsite Dreamweaver class</a>, we also provide onsite training at your location countrywide.
                    </p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>What version of Dreamweaver do you train on?</h4>
                <div class="card-body">
                    <p>All our classes are run on the latest version of Dreamweaver Creative Cloud.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    What are the Dreamweaver class times?</h4>
                <div class="card-body">
                    <p>Our classes run from 9.00am to 4.30pm with a lunch hour at 12.15pm. There are lots of eateries within a short walk of the training centers, or we have kitchen facilities should you prefer to bring your own lunch.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Do I need to know Dreamweaver before attending?
                </h4>
                <div class="card-body">
                    <p>Our 3-day Fundamentals and 5-day Bootcamp classes are entry level courses, so you do not need to know Dreamweaver before signing up for either of those courses. The prerequisites for the Advanced course is the Fundamentals course or the equivalent experience.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-chris.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-jeff.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>
