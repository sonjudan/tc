<?php
$meta_title = "Checkout | Training Connection";
$meta_description = "";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>


    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                </ol>
            </nav>


            <div class="page-intro" data-aos="fade-up">
                <div class="copy intro-copy">
                    <h1>Checkout</h1>
                </div>
            </div>

            <div class="checkout-info" data-aos="fade-up">
                <div class="inline-info">
                    <i class="iconify" data-icon="simple-line-icons:lock" data-inline="false"></i>
                    <p>Your personal information is secured via 128-bit SSL encryption with all payments secured by Verisign / Paypal.</p>
                </div>

                <div class="payment-info text-md-right">
                    <p class="mb-2">We accept the following credit <br class="d-none d-md-block">cards (credit card optional).</p>

                    <div class="block-payment">
                        <h4><i class="icon-card icon-card-paypal">Paypal</i></h4>
                        <ul class="payment-list">
                            <li><i class="icon-card icon-card-visa">Visa</i></li>
                            <li><i class="icon-card icon-card-mastercard">MasterCard</i></li>
                            <li><i class="icon-card icon-card-americanexpress">American Express</i></li>
                            <li><i class="icon-card icon-card-discover">Discover</i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php if( isset($_GET['p']) == "1" ) : ?>

                <h6 class="float-right" data-aos="fade-up">Rescheduling Fee: $0.00</h6>

            <?php elseif( isset($_GET['p']) == "2" ) : ?>
                <div class="tbl tbl-checkout" data-aos="fade-up">

                    <div class="tbl-col">
                        <div class="tbl-cell">
                            <h5 class="pr-3">Rescheduling Fee</h5>
                        </div>
                        <div class="tbl-cell tbl-cell-last">
                            <div class="l2-inline">
                                <span class="h-price">$100.00</span>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>

                    </div>


                    <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                    <div class="tbl-col">
                        <div class="tbl-cell">
                            <h5>Booking Total</h5>
                        </div>
                        <div class="tbl-cell tbl-cell-last">
                            <span class="h-price">$100.00</span>
                        </div>
                    </div>
                </div>

            <?php else: ?>
                <div class="tbl tbl-checkout" data-aos="fade-up">

                    <div class="tbl-col">
                        <div class="tbl-cell">
                            <h5 class="title-l4 pr-3">Excel Level 1 – Introduction</h5>
                            <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                            </p>
                        </div>
                        <div class="tbl-cell"><p>Chicago</p></div>
                        <div class="tbl-cell"><p>Dec 5, 2019</p></div>
                        <div class="tbl-cell tbl-cell-last">
                            <div class="l2-inline">
                                <span class="h-price">$350.00</span>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>

                    </div>

                    <div class="tbl-col tbl-col-sub">
                        <div class="tbl-cell">
                            <p>Discount</p>
                        </div>
                        <div class="tbl-cell tbl-cell-last">
                            <span>$0.00</span>
                        </div>
                    </div>


                    <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                    <div class="tbl-col">
                        <div class="tbl-cell">
                            <h5>Booking Total</h5>
                        </div>
                        <div class="tbl-cell tbl-cell-last">
                            <span class="h-price">$350.00</span>
                        </div>
                    </div>
                </div>

            <?php endif; ?>


            <form class="form-checkout is-selected-cc" data-aos="fade-up">

                <div class="tab-content">
                    <div class="tab-pane active" id="payment-step-1" role="tabpanel">

                        <h5 class="title-l4">Step 1 – Booking Contact</h5>

                        <div class="card-payment">
                            <div class="form-row">
                                <div class="form-group col-lg-3">
                                    <label>Enter First Name* </label>
                                    <input type="text" class="form-control" placeholder="First Name">
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Enter Last Name* </label>
                                    <input type="text" class="form-control" placeholder="Last Name">
                                </div>
                                <div class="form-group col-lg-3">
                                    <label>Enter Email* </label>
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group col-lg-3 text-lg-right">
                                    <label class="d-none d-lg-block">&nbsp;</label>
                                    <a class="btn btn-green" id="payment-step-2-tab" data-toggle="tab" href="#payment-step-2" role="tab" aria-controls="payment-step-2">Proceed <i class="fas fa-chevron-circle-right ml-1"></i></a>
                                </div>
                            </div>
                        </div>

                        <ul class="card-payment-nav" role="tablist">
                            <li><a class="is-active" id="payment-step-1-tab" data-toggle="tab" href="#payment-step-1" role="tab" aria-controls="payment-step-1" >1</a></li>
                            <li><a id="payment-step-2-tab" data-toggle="tab" href="#payment-step-2" role="tab" aria-controls="payment-step-2">2</a></li>
                            <li><a id="payment-step-3-tab" data-toggle="tab" href="#payment-step-3" role="tab" aria-controls="payment-step-3">3</a></li>
                        </ul>

                    </div>
                    <div class="tab-pane" id="payment-step-2" role="tabpanel">
                        <h5 class="title-l4">Step 2 – Address</h5>

                        <div class="card-payment">

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company name (if applicable)">
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="Address 1*">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="Address 2">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <input type="text" class="form-control" placeholder="City*">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="Zip*">
                                </div>
                            </div>

                            <a class="btn btn-green" id="payment-step-3-tab" data-toggle="tab" href="#payment-step-3" role="tab" aria-controls="payment-step-3">Proceed <i class="fas fa-chevron-circle-right ml-1"></i></a>

                        </div>

                        <ul class="card-payment-nav" role="tablist">
                            <li><a class="" id="payment-step-1-tab" data-toggle="tab" href="#payment-step-1" role="tab" aria-controls="payment-step-1">1</a></li>
                            <li><a class="is-active" id="payment-step-2-tab" data-toggle="tab" href="#payment-step-2" role="tab" aria-controls="payment-step-2" >2</a></li>
                            <li><a id="payment-step-3-tab" data-toggle="tab" href="#payment-step-3" role="tab" aria-controls="payment-step-3">3</a></li>
                        </ul>

                    </div>
                    <div class="tab-pane " id="payment-step-3" role="tabpanel">

                        <h5 class="title-l4">Step 3 – Payment</h5>

                        <div class="card-payment">

                            <div class="form-payment-option ">

                                <div class="form-group mb-3">
                                    <label class="mt-0 mb-2">Select Payment Type</label>

                                    <label class="custom-radio">
                                        <input id="js-payment-option-cc" type="radio" name="paymentOption" checked><i></i>
                                        <ul class="payment-list sm">
                                            <li><i class="icon-card icon-card-visa">Visa</i></li>
                                            <li><i class="icon-card icon-card-mastercard">MasterCard</i></li>
                                            <li><i class="icon-card icon-card-americanexpress">American Express</i></li>
                                            <li><i class="icon-card icon-card-discover">Discover</i></li>
                                        </ul>
                                    </label>
                                </div>


                                <div class="fields-cc">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>First Name*</label>
                                            <input type="text" class="form-control" >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Last Name*</label>
                                            <input type="text" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        <div class="col-md-10">

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label >Card Type </label>
                                                    <select name="" id="" class="form-control js-card-select">
                                                        <option value=""></option>
                                                        <option value="Visa">Visa</option>
                                                        <option value="Master Card">Master Card</option>
                                                        <option value="American Express">American Express</option>
                                                        <option value="Discover Network">Discover Network</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Card number</label>
                                                    <input type="text" class="form-control" placeholder="Text box – max 16">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Expiration date</label>
                                                    <input type="text" class="form-control" placeholder="E.g. 11/21">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label>CCV</label>
                                            <input type="text" class="form-control" placeholder="CCV">
                                            <i class="icon icon-ccv icon-ccv-r"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group mb-4">
                                    <label class="custom-radio">
                                        <input id="js-payment-option-paypal" type="radio" name="paymentOption" ><i></i>
                                        <ul class="payment-list">
                                            <li><i class="icon-card icon-card-paypal">paypal</i></li>
                                        </ul>
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label class="custom-radio">
                                        <input id="js-payment-option-invoice" type="radio" name="paymentOption" ><i></i>
                                        Bill me (we will email you an invoice)
                                    </label>
                                </div>
                            </div>
                        </div>

                        <ul class="card-payment-nav" role="tablist">
                            <li><a class="" id="payment-step-1-tab" data-toggle="tab" href="#payment-step-1" role="tab" aria-controls="payment-step-1">1</a></li>
                            <li><a class="" id="payment-step-2-tab" data-toggle="tab" href="#payment-step-2" role="tab" aria-controls="payment-step-2">2</a></li>
                            <li><a class="is-active" id="payment-step-3-tab" data-toggle="tab" href="#payment-step-3" role="tab" aria-controls="payment-step-3" >3</a></li>
                        </ul>

                        <div class="copy p-lg-5 pt-5">

                            <h4>Terms</h4>
                            <p>All cancellations and rescheduling requests must be made in writing and emailed to info@trainingconnection.com. </p>
                            <ul>
                                <li>Cancellations made 7 days or less before the commencement of the training, will not be eligible for a refund but will receive a training voucher (equal to the amount paid), which can be used to book other classes. Such vouchers are valid for a period of 6 months from the date of issue. </li>
                                <li>Cancellations made more than 7 days before the commencement of the training, qualify for a refund less a $100 administration fee. </li>
                                <li>We are not able to offer refunds or training vouchers for no-shows or uncompleted courses.</li>
                                <li>Rescheduling requests made 7 days before the commencement of the training are free. Rescheduling that takes place within 7 days of the commencement of the training will incur a $100 administration fee. We do not charge for student substitutions at any time. </li>
                                <li>While we make every effort to deliver classes as scheduled, we reserve the right to reschedule previously confirmed classes for a variety of reasons including but not exclusively, the illness of the Trainer. In such cases, we will endeavor to inform the Client as soon as possible of the new training dates. We are not liable for any additional expenses incurred as a result of rescheduling training. </li>
                            </ul>

                            <label class="custom-checkbox mt-4 mt-lg-5">
                                <input type="checkbox" ><i></i>
                                I have read & agree to these Terms & Conditions
                            </label>


                            <div class="form-actions">
                                <button id="checkout-submit" class="btn btn-green">
                                    <i class="iconify mr-1" data-icon="simple-line-icons:calendar" data-inline="false"></i> <span>Place Booking</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>