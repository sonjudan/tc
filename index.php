<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<div class="masterhead">
    <span class="paralax-bg" style="background-image: url('dist/images/home/banner.jpg');"></span>
    <div class="container">
        <div class="masterhead-copy w-600">
            <h1 data-aos="fade-up">Computer and Business Skills training</h1>

            <a href="/onsite-training.php" class="btn btn-secondary btn-lg" data-aos="fade-up" data-aos-delay="200">
                <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                Book Course
            </a>
        </div>
    </div>
</div>

<div class="section section-intro">
    <div class="container">
        <div class="section-heading">
            <h2 data-aos="fade-up">
                Live instructor-led training
                <span>in Chicago and Los Angeles</span>
            </h2>

            <img data-aos="fade-up" data-aos-delay="100" src="/dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed">
        </div>

        <p data-aos="fade-up" data-aos-delay="180">Whether you looking to learn a new software program like Microsoft Excel or Adobe Photoshop, or you need to brush up on your business skills like customer service or time management, you have come to the right place. We offer  courses in Adobe, Apple, MS Office, Business Skills and more.</p>

        <a href="/onsite-training.php" class="btn btn-secondary btn-lg" data-aos="fade-up" data-aos-delay="200">
            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
            Book Course
        </a>
    </div>
</div>


<div class="section section-classes-dp">
    <div class="container">
        <div class="section-heading">
            <h2 data-aos="fade-up">View classes offered</h2>
        </div>

        <div class="section-body card-classes-list">
            <div class="card-classes" data-aos="fade-up" data-aos-delay="100">
                <div class="card-body">
                    <span class="card-icon">
                        <img src="/dist/images/icons/icon-adobe.svg" alt="Adobe Training">
                    </span>
                    <h4 class="card-title">Adobe <span>Training</span></h4>
                </div>

                <div class="card-dp lg">
                    <ul>
                        <li><a href="#">Acrobat DC Fundamentals</a></li>
                        <li><a href="#">After Effects Fundamentals</a></li>
                        <li><a href="#">After Effects Advanced</a></li>
                        <li><a href="#">After Effects Bootcamp</a></li>
                        <li><a href="#">Captivate 2019 Fundamentals</a></li>
                        <li><a href="#">Captivate 2019 Advanced</a></li>
                        <li><a href="#">Dreamweaver 2019 Fundamentals</a></li>
                        <li><a href="#">Illustrator 2019 Quickstart</a></li>
                        <li><a href="#">Illustrator 2019 Fundamentals</a></li>
                        <li><a href="#">Illustrator 2019 Advanced</a></li>
                        <li><a href="#">Illustrator 2019 Bootcamp</a></li>
                        <li><a href="#">InDesign 2019 Quick Start</a></li>
                        <li><a href="#">InDesign 2019 Fundamentals</a></li>
                        <li><a href="#">InDesign 2019 Advanced</a></li>
                        <li><a href="#">InDesign 2019 Bootcamp</a></li>
                        <li><a href="/photoshop/photoshop-intruction.php">Photoshop 2019 Quick Start</a></li>
                        <li><a href="/photoshop/photoshop-fundamentals.php">Photoshop 2019 Fundamentals</a></li>
                        <li><a href="/photoshop/photoshop-advanced.php">Photoshop 2019 Advanced</a></li>
                        <li><a href="/photoshop/photoshop-bootcamp.php">Photoshop 2019 Bootcamp</a></li>
                        <li><a href="#">Premiere Pro Fundamentals</a></li>
                        <li><a href="#">Premiere Pro Advanced</a></li>
                        <li><a href="#">Premiere Pro Bootcamp</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-classes" data-aos="fade-up" data-aos-delay="150">
                <div class="card-body">
                    <span class="card-icon">
                        <img src="/dist/images/icons/icon-apple.svg" alt="Apple">
                    </span>
                    <h4 class="card-title">Apple</h4>
                </div>

                <div class="card-dp">
                    <ul>
                        <li><a href="#">Final Cut Pro X</a></li>
                        <li><a href="#">Logic Pro X</a></li>
                        <li><a href="#">Keynote</a><a href="#"></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-classes" data-aos="fade-up" data-aos-delay="200">
                <div class="card-body">
                    <span class="card-icon">
                        <img src="/dist/images/icons/icon-office.svg" alt="Microsoft office">
                    </span>
                    <h4 class="card-title">Microsoft <span>office</span></h4>
                </div>

                <div class="card-dp">
                    <ul>
                        <li><a href="#">Microsoft Access</a></li>
                        <li><a href="#">Microsoft Excel</a></li>
                        <li><a href="#">Microsoft Outlook</a></li>
                        <li><a href="#">Microsoft PowerPoint</a></li>
                        <li><a href="#">Microsoft Teams</a></li>
                        <li><a href="#">Microsoft Visio</a></li>
                        <li><a href="#">Microsoft Word</a></li>
                        <li><a href="#"></a><a href="#">SharePoint</a></li>
                        <li><a href="#">Skype for Business</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-classes" data-aos="fade-up" data-aos-delay="250">
                <div class="card-body">
                    <span class="card-icon">
                        <img src="/dist/images/icons/icon-coding.svg" alt="Web Development">
                    </span>
                    <h4 class="card-title">Web <span>development</span></h4>
                </div>

                <div class="card-dp">
                    <ul>
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">JavaScript</a></li>
                        <li><a href="#">PHP</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-classes" data-aos="fade-up" data-aos-delay="300">
                <div class="card-body">
                    <span class="card-icon">
                        <img src="/dist/images/icons/icon-business.svg" alt="Business Skills">
                    </span>
                    <h4 class="card-title">Business <span>Skills</span></h4>
                </div>

                <div class="card-dp">
                    <ul>
                        <li><a href="#">Business Communication</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="section section-training-options">
    <div class="container container-l2">
        <div class="section-heading" data-aos="fade-up">
            <h2>Training Options</h2>
        </div>

        <div class="section-body">
            <div class="card-deck card-row-sm card-deck-training">
                <div class="card card-training" data-aos="fade-up" data-aos-delay="100">
                    <h5 class="card-title"><span>Public Scheduled Classes</span></h5>
                    <div class="card-body">
                        <div class="card-text">
                            <p>Join one of our small public classes in Chicago or Los Angeles</p>
                        </div>
                        <span class="card-icon">
                            <img src="/dist/images/icons/icon-training-public.svg" alt="Public Scheduled Classes">
                        </span>
                    </div>
                </div>
                <div class="card card-training" data-aos="fade-up" data-aos-delay="100">
                    <h5 class="card-title"><span>Private Training Group</span></h5>
                    <div class="card-body">
                        <div class="card-text">
                            <p>Available at our Chicago and Los Angeles Training centers</p>
                        </div>
                        <span class="card-icon">
                            <img src="/dist/images/icons/icon-training-private.svg" alt="Private Training Group">
                        </span>
                    </div>
                    <div class="card-footer">
                        <a href="#section-course-form" class="btn btn-secondary js-anchor-scroll">Obtain Pricing</a>
                    </div>
                </div>
                <div class="card card-training" data-aos="fade-up" data-aos-delay="100">
                    <h5 class="card-title"><span>Onsite Group Training</span></h5>
                    <div class="card-body">
                        <div class="card-text">
                            <p>Our training delivered at your location countrywide</p>
                        </div>
                        <span class="card-icon">
                            <img src="/dist/images/icons/icon-training-onsite.svg" alt="Onsite Group Training">

                        </span>
                    </div>
                    <div class="card-footer">
                        <a href="#section-course-form" class="btn btn-secondary js-anchor-scroll">Obtain Pricing</a>
                    </div>
                </div>
                <div class="card card-training" data-aos="fade-up" data-aos-delay="100">
                    <h5 class="card-title"><span>Group Webinar</span></h5>
                    <div class="card-body">
                        <div class="card-text">
                            <p>Instructor-led remote training via webinar</p>
                        </div>
                        <span class="card-icon">
                            <img src="/dist/images/icons/icon-training-webinar.svg" alt="Group Webinar">
                        </span>
                    </div>
                    <div class="card-footer">
                        <a href="#section-course-form" class="btn btn-secondary js-anchor-scroll">Obtain Pricing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="section-course-form" class="section section-group-training">
    <div class="container">
        <div class="section-heading">
            <h2 data-aos="fade-up">Group Training Quotation</h2>
            <p data-aos="fade-up" data-aos-delay="50">We offer group training. This can be delivered onsite at your premises, online via webinar or at one of our training centers in Chicago or Los Angeles. Fill out the form below to receive pricing.</p>
        </div>

        <div class="section-body">
            <form action="" class="form-group-training">

                <h4 class="text-center" data-aos="fade-up" data-aos-delay="100">Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                        <h3><input type="radio" name="training">
                        <i></i> Online Webinar</h3>

                    </label>
                </div>

                <div class="" data-aos="fade-up" data-aos-delay="100">
                    <div class="form-group">

                        <select class="js-course-select" name="course" placeholder="Select course">
                            <option value=""></option>
                            <option value="Acrobat">Acrobat</option>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">After Effects Quickstart</option>
                                <option value="Acrobat">After Effects Advanced</option>
                                <option value="Acrobat">After Effects Fundamentals</option>
                                <option value="Acrobat">After Effects Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Captivate Quickstart</option>
                                <option value="Acrobat">Captivate Advanced</option>
                                <option value="Acrobat">Captivate Fundamentals</option>
                                <option value="Acrobat">Captivate Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Dreamweaver Quickstart</option>
                                <option value="Acrobat">Dreamweaver Advanced</option>
                                <option value="Acrobat">Dreamweaver Fundamentals</option>
                                <option value="Acrobat">Dreamweaver Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Illustrator Quickstart</option>
                                <option value="Acrobat">Illustrator Advanced</option>
                                <option value="Acrobat">Illustrator Fundamentals</option>
                                <option value="Acrobat">Illustrator Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">InDesign Quickstart</option>
                                <option value="Acrobat">InDesign Advanced</option>
                                <option value="Acrobat">InDesign Fundamentals</option>
                                <option value="Acrobat">InDesign Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Lightroom Quickstart</option>
                                <option value="Acrobat">Lightroom Advanced</option>
                                <option value="Acrobat">Lightroom Fundamentals</option>
                                <option value="Acrobat">Lightroom Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Photoshop Quickstart</option>
                                <option value="Acrobat">Photoshop Advanced</option>
                                <option value="Acrobat">Photoshop Fundamentals</option>
                                <option value="Acrobat">Photoshop Bootcamp</option>
                            </optgroup>
                            <optgroup label="Photoshop">
                                <option value="Acrobat">Premiere Pro Quickstart</option>
                                <option value="Acrobat">Premiere Pro Advanced</option>
                                <option value="Acrobat">Premiere Pro Fundamentals</option>
                                <option value="Acrobat">Premiere Pro Bootcamp</option>
                            </optgroup>
                            <option value="Acrobat">Final Cut Pro</option>
                            <option value="Acrobat">Keynote</option>
                            <option value="Acrobat">Logic Pro</option>
                        </select>

                    </div>
                    <div class="row row-sm">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-actions"  data-aos="fade-up" data-aos-delay="130">
                    <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>

