<?php
$meta_title = "Microsoft Excel Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Passing Microsoft Excel Certification Exams. Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Excel Cheat Sheets & shortcuts</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ec" style="background-image: url('/dist/images/banner-excel.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">
                        Excel 2019<br>
                        Cheat Sheets<br>
                        & shortcuts
                    </h1>
                </div>

                <div class="img-specialist"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/excel-specialist.png" alt="Microsoft Office Excel Specialist" width="340">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-exams">
        <div class="container">
            <div class="section-heading align-left" data-aos="fade-up">
                <h2>Excel 2019 Cheat Sheets & Shortcuts</h2>
            </div>

            <div class="card-deck card-row-sm card-deck-fblock">
                <div class="card card-fblock" data-aos="fade-up">
                    <h3 class="card-title">The Complete Microsoft <br>
                    Excel Cheat Sheet</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>The complete Microsoft <br class="d-sm-none">Excel CheatSheet</p>
                        <a href="/downloads/excel/Excel-CheatSheet-COMPLETE.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
                <div class="card card-fblock" data-aos="fade-up">
                    <h3 class="card-title">The Microsoft excel <br>formulas cheat sheet</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>The complete Microsoft <br>Excel CheatSheet</p>
                        <a href="/downloads/excel/Excel-CheatSheet-Formatting and Formula Shortcuts.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
            </div>

            <div class="card-deck card-row-sm card-deck-fblock">
                <div class="card card-fblock" data-aos="fade-up">
                    <h3 class="card-title">Working <br>with data</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>Microsoft Excel Working with Data Shortcuts</p>
                        <a href="/downloads/excel/Excel-CheatSheet-Working with Data.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
                <div class="card card-fblock" data-aos="fade-up">
                    <h3 class="card-title">Navigation shortcuts</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>Microsoft Excel Navigation Shorcuts</p>
                        <a href="/downloads/excel/Excel-CheatSheet-Navigation Shortcuts.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
                <div class="card card-fblock" data-aos="fade-up">
                    <h3 class="card-title">Formatting & formula shortcuts</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>Microsoft Excel Formatting  & Formula Shortcuts</p>
                        <a href="/downloads/excel/Excel-CheatSheet-The Microsoft Excel Formulas Cheat Sheet.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>