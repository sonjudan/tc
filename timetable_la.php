<?php
$meta_title = "Timetable | Los Angeles";
$meta_description = "";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Chicago Timetable</li>
                </ol>
            </nav>


            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <h1 data-aos="fade-up" >Los Angeles Timetable</h1>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="mb-1">915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017.</h3>

                    <ul data-aos="fade-up" data-aos-delay="150" class="list-inline">
                        <li>Tel: <a href="#">(888) 815-0604</a></li>
                        <li>Fax: <a href="#">(866) 523-2138</a></li>
                        <li>Email: <a href="mailto:info@trainingconnection.com">info@trainingconnection.com</a></li>
                    </ul>

                </div>
            </div>

            <div class="timestable-section" data-aos="fade-up" data-aos-delay="250">

                <ul class="nav nav-timestable mb-3" id="timestable-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="timestable-adobe-tab" data-toggle="pill" href="#timestable-adobe" role="tab" aria-controls="timestable-adobe" aria-selected="true">
                            <img src="/dist/images/icons/icon-adobe.svg" alt="Adobe Training - Timetable">
                            <h4>Adobe <span>Training</span></h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="timestable-apple-tab" data-toggle="pill" href="#timestable-apple" role="tab" aria-controls="timestable-apple" aria-selected="false">
                            <img src="/dist/images/icons/icon-apple.svg" alt="Apple - Timetable">
                            <h4>Apple <span>Training</span></h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="timestable-office-tab" data-toggle="pill" href="#timestable-office" role="tab" aria-controls="timestable-office" aria-selected="false">
                            <img src="/dist/images/icons/icon-office.svg" alt="Microsoft Office- Timetable">
                            <h4>Microsoft <span>Office</span></h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="timestable-web-tab" data-toggle="pill" href="#timestable-web" role="tab" aria-controls="timestable-web" aria-selected="false">
                            <img src="/dist/images/icons/icon-coding.svg" alt="Web Development - Timetable">
                            <h4>Web <span>Development</span></h4>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="timestable-business-tab" data-toggle="pill" href="#timestable-business" role="tab" aria-controls="timestable-business" aria-selected="false">
                            <img src="/dist/images/icons/icon-business.svg" alt="Business Skills - Timetable">
                            <h4>Business <span>Skills</span></h4>
                        </a>
                    </li>
                </ul>


                <div class="tab-section">
                    <div class="tab-content tab-content-timestable" id="timestable-tabContent">
                        <div class="tab-pane fade show active" id="timestable-adobe" role="tabpanel" aria-labelledby="timestable-adobe-tab">

                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-dark table-hover">
                                    <thead>
                                    <tr class="tr">
                                        <th scope="col"></th>
                                        <th scope="col">Jul</th>
                                        <th scope="col">Aug</th>
                                        <th scope="col">Sep</th>
                                        <th scope="col">Oct</th>
                                        <th scope="col">Nov</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tbody>
                                    <tr class="tr">
                                        <th scope="row"><a href="acrobat-training.php">Acrobat DC Fundamentals</a></th>
                                        <td></td>
                                        <td></td>
                                        <td class="hasDate">Sep 5–6</td>
                                        <td></td>
                                        <td class="hasDate">Nov 4–5</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="after-effects/fundamentals.php">After Effects Fundamentals</a></th>

                                        <td class="hasDate">Jul 29–31</td>
                                        <td></td>
                                        <td class="hasDate">Sep 9–11</td>
                                        <td class="hasDate">Oct 14–16</td>
                                        <td class="hasDate">Nov 18–20</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="after-effects/advanced.php">After Effects Advanced</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 1–2</td>
                                        <td class="hasDate">Sep 12–13</td>
                                        <td class="hasDate">Oct 17–18</td>
                                        <td class="hasDate">Nov 21–22</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="after-effects/bootcamp.php">After Effects Bootcamp</a></th>

                                        <td class="hasDate">Jul 29–Aug 2</td>
                                        <td></td>
                                        <td class="hasDate">Sep 9–13</td>
                                        <td class="hasDate">Oct 14–18</td>
                                        <td class="hasDate">Nov 18–22</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="captivate/fundamentals.php">Captivate 2019 Fundamentals</a></th>

                                        <td class="hasDate">Jul 29–30</td>
                                        <td></td>
                                        <td class="hasDate">Sep 9–10</td>
                                        <td class="hasDate">Oct 21–22</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="captivate/advanced.php">Captivate 2019 Advanced</a></th>

                                        <td class="hasDate">Jul 31–Aug 1</td>
                                        <td></td>
                                        <td class="hasDate">Sep 11–12</td>
                                        <td class="hasDate">Oct 23–24</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="dreamweaver-training.php">Dreamweaver 2019 Fundamentals</a></th>
                                        <td></td>
                                        <td></td>
                                        <td class="hasDate">Sep 4–6</td>
                                        <td class="hasDate">Oct 16–18</td>
                                        <td class="hasDate">Nov 25–27</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="illustrator/introduction.php">Illustrator 2019 Quickstart</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 5</td>
                                        <td class="hasDate">Sep 16</td>
                                        <td class="hasDate">Oct 21</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="illustrator/fundamentals.php">Illustrator 2019 Fundamentals</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 5–7</td>
                                        <td class="hasDate">Sep 16–18</td>
                                        <td class="hasDate">Oct 21–23</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="illustrator/advanced.php">Illustrator 2019 Advanced</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 8–9</td>
                                        <td class="hasDate">Sep 19–20</td>
                                        <td class="hasDate">Oct 24–25</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="illustrator/bootcamp.php">Illustrator 2019 Bootcamp</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 5–9</td>
                                        <td class="hasDate">Sep 16–20</td>
                                        <td class="hasDate">Oct 21–25</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="indesign/introduction.php">InDesign 2019 Quick Start</a></th>

                                        <td class="hasDate">Jul 22</td>
                                        <td class="hasDate">Aug 26</td>
                                        <td></td>
                                        <td class="hasDate">Oct 7</td>
                                        <td class="hasDate">Nov 11</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="indesign/fundamentals.php">InDesign 2019 Fundamentals</a></th>

                                        <td class="hasDate">Jul 22–24</td>
                                        <td class="hasDate">Aug 26–28</td>
                                        <td></td>
                                        <td class="hasDate">Oct 7–9</td>
                                        <td class="hasDate">Nov 11–13</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="indesign/advanced.php">InDesign 2019 Advanced</a></th>

                                        <td class="hasDate">Jul 25–26</td>
                                        <td class="hasDate">Aug 29–30</td>
                                        <td></td>
                                        <td class="hasDate">Oct 10–11</td>
                                        <td class="hasDate">Nov 14–15</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="indesign/bootcamp.php">InDesign 2019 Bootcamp</a></th>

                                        <td class="hasDate">Jul 22–26</td>
                                        <td class="hasDate">Aug 26–30</td>
                                        <td></td>
                                        <td class="hasDate">Oct 7–11</td>
                                        <td class="hasDate">Nov 11–15</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="adobe-lightroom-training.php">Adobe Lightroom Fundamentals</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 29–30</td>
                                        <td></td>
                                        <td class="hasDate">Oct 10–11</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="photoshop/introduction.php">Photoshop 2019 Quick Start</a></th>

                                        <td class="hasDate">Jul 8</td>
                                        <td class="hasDate">Aug 12</td>
                                        <td class="hasDate">Sep 23</td>
                                        <td class="hasDate">Oct 28</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="photoshop/fundamentals.php">Photoshop 2019 Fundamentals</a></th>

                                        <td class="hasDate">Jul 8–10</td>
                                        <td class="hasDate">Aug 12–14</td>
                                        <td class="hasDate">Sep 23–25</td>
                                        <td class="hasDate">Oct 28–30</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="photoshop/advanced.php">Photoshop 2019 Advanced</a></th>

                                        <td class="hasDate">Jul 11–12</td>
                                        <td class="hasDate">Aug 15–16</td>
                                        <td class="hasDate">Sep 26–27</td>
                                        <td class="hasDate">Oct 31–Nov 1</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="photoshop/bootcamp.php">Photoshop 2019 Bootcamp</a></th>

                                        <td class="hasDate">Jul 8–12</td>
                                        <td class="hasDate">Aug 12–16</td>
                                        <td class="hasDate">Sep 23–27</td>
                                        <td class="hasDate">Oct 28–Nov 1</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="premiere-pro/fundamentals.php">Premiere Pro Fundamentals</a></th>

                                        <td class="hasDate">Jul 15–17</td>
                                        <td class="hasDate">Aug 19–21</td>
                                        <td class="hasDate">Sep 30–Oct 2</td>
                                        <td></td>
                                        <td class="hasDate">Nov 4–6</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="premiere-pro/advanced.php">Premiere Pro Advanced</a></th>

                                        <td class="hasDate">Jul 18–19</td>
                                        <td class="hasDate">Aug 22–23</td>
                                        <td></td>
                                        <td class="hasDate">Oct 3–4</td>
                                        <td class="hasDate">Nov 7–8</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="premiere-pro/bootcamp.php">Premiere Pro Bootcamp</a></th>

                                        <td class="hasDate">Jul 15–19</td>
                                        <td class="hasDate">Aug 19–23</td>
                                        <td class="hasDate">Sep 30–Oct 4</td>
                                        <td></td>
                                        <td class="hasDate">Nov 4–8</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="adobe-xd-training.php">Adobe XD CC Fundamentals</a></th>
                                        <td></td>
                                        <td></td>
                                        <td class="hasDate">Sep 16–17</td>
                                        <td></td>
                                        <td class="hasDate">Nov 25–27</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="timestable-apple" role="tabpanel" aria-labelledby="timestable-apple-tab">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-dark table-hover">
                                    <thead>
                                    <tr class="tr">
                                        <th scope="col"></th>
                                        <th scope="col">Jul</th>
                                        <th scope="col">Aug</th>
                                        <th scope="col">Sep</th>
                                        <th scope="col">Oct</th>
                                        <th scope="col">Nov</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr class="tr">
                                        <th scope="row"><a href="final-cut-pro-training.php">Apple Final Cut Pro X Fundamentals</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 26–28</td>
                                        <td></td>
                                        <td class="hasDate">Oct 9–11</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="logic-pro-training.php">Apple Logic Pro X Fundamentals</a></th>
                                        <td></td>
                                        <td></td>
                                        <td class="hasDate">Sep 4–6</td>
                                        <td></td>
                                        <td class="hasDate">Nov 11–13</td>
                                    </tr>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="timestable-office" role="tabpanel" aria-labelledby="timestable-office-tab">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-dark table-hover">
                                    <thead>
                                    <tr class="tr">
                                        <th scope="col"></th>
                                        <th scope="col">Jul</th>
                                        <th scope="col">Aug</th>
                                        <th scope="col">Sep</th>
                                        <th scope="col">Oct</th>
                                        <th scope="col">Nov</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr class="tr">
                                        <th scope="row"><a href="access/introduction.php">Access Fundamentals</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 1–2</td>
                                        <td class="hasDate">Sep 4–5</td>
                                        <td class="hasDate">Oct 14–15</td>
                                        <td class="hasDate">Nov 25–26</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="access/advanced.php">Access Advanced</a></th>
                                        <td></td>
                                        <td></td>
                                        <td class="hasDate">Sep 6</td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="excel/introduction.php">Excel Level 1 - Introduction</a></th>

                                        <td class="hasDate">Jul 9,<br>Jul 29</td>
                                        <td class="hasDate">Aug 20</td>
                                        <td class="hasDate">Sep 9</td>
                                        <td class="hasDate">Oct 1,<br>Oct 21</td>
                                        <td class="hasDate">Nov 12</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="excel/intermediate.php">Excel Level 2 - Intermediate</a></th>

                                        <td class="hasDate">Jul 10,<br>Jul 30</td>
                                        <td class="hasDate">Aug 21</td>
                                        <td class="hasDate">Sep 10</td>
                                        <td class="hasDate">Oct 2,<br>Oct 22</td>
                                        <td class="hasDate">Nov 13</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="excel/advanced.php">Excel Level 3 - Advanced</a></th>

                                        <td class="hasDate">Jul 11,<br>Jul 31</td>
                                        <td class="hasDate">Aug 22</td>
                                        <td class="hasDate">Sep 11</td>
                                        <td class="hasDate">Oct 3,<br>Oct 23</td>
                                        <td class="hasDate">Nov 14</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="excel/vba.php">Excel Level 4 - Macros &amp; VBA</a></th>

                                        <td class="hasDate">Jul 12</td>
                                        <td class="hasDate">Aug 23</td>
                                        <td></td>
                                        <td class="hasDate">Oct 4</td>
                                        <td class="hasDate">Nov 15</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="outlook/introduction.php">Outlook Level 1 - Introduction</a></th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="outlook/advanced.php">Outlook Level 2 - Advanced</a></th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="powerpoint/introduction.php">PowerPoint Level 1 - Introduction</a></th>

                                        <td class="hasDate">Jul 22</td>
                                        <td class="hasDate">Aug 26</td>
                                        <td></td>
                                        <td class="hasDate">Oct 3</td>
                                        <td class="hasDate">Nov 6</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="powerpoint/advanced.php">PowerPoint Level 2 - Advanced</a></th>

                                        <td class="hasDate">Jul 23</td>
                                        <td class="hasDate">Aug 27</td>
                                        <td></td>
                                        <td class="hasDate">Oct 4</td>
                                        <td class="hasDate">Nov 7</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="project/introduction.php">Microsoft Project Level 1</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 5</td>
                                        <td class="hasDate">Sep 12</td>
                                        <td class="hasDate">Oct 16</td>
                                        <td class="hasDate">Nov 25</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="project/intermediate.php">Microsoft Project Level 2</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 6</td>
                                        <td class="hasDate">Sep 13</td>
                                        <td class="hasDate">Oct 17</td>
                                        <td class="hasDate">Nov 26</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="visio-training.php">Microsoft Visio Fundamentals</a></th>

                                        <td class="hasDate">Jul 9</td>
                                        <td class="hasDate">Aug 19</td>
                                        <td class="hasDate">Sep 30</td>
                                        <td></td>
                                        <td class="hasDate">Nov 11</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="word/introduction.php">Word Level 1 - Introduction</a></th>

                                        <td class="hasDate">Jul 8</td>
                                        <td class="hasDate">Aug 15</td>
                                        <td class="hasDate">Sep 26</td>
                                        <td></td>
                                        <td class="hasDate">Nov 4</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="word/advanced.php">Word Level 2 - Advanced</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 16</td>
                                        <td class="hasDate">Sep 27</td>
                                        <td></td>
                                        <td class="hasDate">Nov 5</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="timestable-web" role="tabpanel" aria-labelledby="timestable-web-tab">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-dark table-hover">
                                    <thead>
                                    <tr class="tr">
                                        <th scope="col"></th>
                                        <th scope="col">Jul</th>
                                        <th scope="col">Aug</th>
                                        <th scope="col">Sep</th>
                                        <th scope="col">Oct</th>
                                        <th scope="col">Nov</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr class="tr">
                                        <th scope="row"><a href="html/fundamentals.php">HTML5 and CSS Fundamentals</a></th>

                                        <td class="hasDate">Jul 15–17</td>
                                        <td class="hasDate">Aug 26–28</td>
                                        <td></td>
                                        <td class="hasDate">Oct 2–4</td>
                                        <td class="hasDate">Nov 18–20</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="html/advanced.php">HTML5 and CSS Advanced</a></th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="html/responsive.php">Mobile &amp; Responsive Web Design</a></th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="javascript/fundamentals.php">JavaScript and jQuery</a></th>

                                        <td class="hasDate">Jul 15–17</td>
                                        <td></td>
                                        <td class="hasDate">Sep 4–6</td>
                                        <td class="hasDate">Oct 16–18</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="php-mysql-training.php">PHP and MySQL Web Development</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 7–9</td>
                                        <td class="hasDate">Sep 18–20</td>
                                        <td class="hasDate">Oct 30–Nov 1</td>
                                        <td></td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="timestable-business" role="tabpanel" aria-labelledby="timestable-business-tab">
                            <div class="table-responsive">
                                <table class="table table-sm table-bordered table-dark table-hover">
                                    <thead>
                                    <tr class="tr">
                                        <th scope="col"></th>
                                        <th scope="col">Jul</th>
                                        <th scope="col">Aug</th>
                                        <th scope="col">Sep</th>
                                        <th scope="col">Oct</th>
                                        <th scope="col">Nov</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr class="tr">
                                        <th scope="row"><a href="business-communication.php">Effective Business Communication</a></th>

                                        <td class="hasDate">Jul 31</td>
                                        <td class="hasDate">Aug 21</td>
                                        <td class="hasDate">Sep 23</td>
                                        <td class="hasDate">Oct 24</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="business-etiquette-training.php">Business Etiquette for Today's Workforce</a></th>

                                        <td class="hasDate">Jul 25</td>
                                        <td class="hasDate">Aug 12</td>
                                        <td class="hasDate">Sep 12</td>
                                        <td class="hasDate">Oct 14</td>
                                        <td class="hasDate">Nov 15</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="business-leadership-training.php">Business Leadership Skills</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 9,<br>Aug 12</td>
                                        <td></td>
                                        <td class="hasDate">Oct 8</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="business-writing-class.php">Business Writing with Positive Impact</a></th>

                                        <td class="hasDate">Jul 30</td>
                                        <td class="hasDate">Aug 30</td>
                                        <td></td>
                                        <td class="hasDate">Oct 2</td>
                                        <td class="hasDate">Nov 8</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="customer-service-training.php">Exceptional Customer Service</a></th>

                                        <td class="hasDate">Jul 15</td>
                                        <td class="hasDate">Aug 14</td>
                                        <td class="hasDate">Sep 17</td>
                                        <td class="hasDate">Oct 18</td>
                                        <td class="hasDate">Nov 18</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="presentation-training.php">Effective Presentations</a></th>

                                        <td class="hasDate">Jul 19</td>
                                        <td class="hasDate">Aug 13</td>
                                        <td class="hasDate">Sep 13</td>
                                        <td class="hasDate">Oct 15</td>
                                        <td class="hasDate">Nov 19</td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="project-management-training.php">Project Management</a></th>

                                        <td class="hasDate">Jul 16–17</td>
                                        <td class="hasDate">Aug 22–23</td>
                                        <td class="hasDate">Sep 24–25</td>
                                        <td class="hasDate">Oct 28–29</td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr">
                                        <th scope="row"><a href="time-management-training.php">Effective Time Management Techniques</a></th>
                                        <td></td>
                                        <td class="hasDate">Aug 1</td>
                                        <td class="hasDate">Sep 5</td>
                                        <td class="hasDate">Oct 7</td>
                                        <td class="hasDate">Nov 13</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>




        </div>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>