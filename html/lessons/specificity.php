<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php'; ?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="#">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Specificity</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Being Specific. CSS Specificity Demystified!</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>What is CSS Specificity?  Specificity in CSS is the way a browser will determine which declarations are applied to HTML elements when there is a conflict from multiple rules. Ok, so what does that mean exactly?  Let's look at an example:</p>
                <p>Here we have an <strong>&lt;h1&gt;</strong> tag that has a class of '<strong>highlight</strong>':</p>

<pre class="block-code">
<code><span class="tag">&lt;h1 <span class="tprop">class=</span><span class="tval">"highlight"</span>&gt;</span>
</code></pre>

                <p>We have two rules in our CSS styles that effect this tag:</p>

<pre class="block-code">
<code><span class="dec">.highlight {
    <span class="prop">color</span>: <span class="val">white</span>;
    <span class="prop">background</span>: <span class="val">blue</span>;
}
h1 {
    <span class="prop">color</span>: <span class="val">white</span>;
    <span class="prop">background</span>: <span class="val">black</span>;
}</span>
</code></pre>

                <p><a href="/html-training.php">Private and public  HTML classes</a>.</p>
                <p> Both rules have the font color white declaration but they have different background colors. Which background color does the browser choose? This may seem obvious.  If you said the tag will have a blue background you would be correct. All tags that have the class 'highlight' would be in blue, the <strong>&lt;h1&gt;</strong> tags without the class would have a black background.  Let's look at another example:</p>

                <p>Now we have a <strong>&lt;p&gt;</strong> tag with an <strong>id</strong> and a <strong>class</strong>:</p>

<pre class="block-code">
<code><span class="tag">&lt;p <span class="tprop">id=</span><span class="tval">"first"</span> <span class="tprop">class=</span><span class="tval">"special"</span>&gt;</span>
</code></pre>

                <p>And our style sheet:</p>

<pre class="block-code">
<code><span class="dec">p#first {
    <span class="prop">font-family</span>: <span class="val">Arial, sans-serif</span>;
    <span class="prop">font-size</span>: <span class="val">16px</span>;
}
p.special {
    <span class="prop">font-family</span>: <span class="val">Arial, sans-serif</span>;
    <span class="prop">font-size</span>: <span class="val">20px</span>;
}</span>
</code></pre>

                <p>Once again, what will the paragraph's font size be?  Both rules are valid, the paragraph has both the id of '<strong>first</strong>' and the class of '<strong>special</strong>' but since it can't be both font sizes, which does the browser choose? In this case the font size will be 16px because the id of '<strong>first</strong>' is more specific than the class of '<strong>special</strong>'.  Think about it, many elements on the page may have the class of '<strong>special</strong>' but only one element can have the id of '<strong>first</strong>'.  Therefore, because the id is pointing to a very specific case where the class if more general, the id wins.  Also notice, in both cases, order of the rules in the style sheet did not matter.  Order of the rules is handled by the cascade (hence the name Cascading Style Sheets) and is only relevant when the specificity of both rules are equal.  More on that later.</p>

                <p>Let's try another one:</p>

                <p>Here is our HTML:</p>

<pre class="block-code">
<code><span class="tag">&lt;div <span class="tprop">id=</span><span class="tval">"header"</span>&gt;</span>
    <span class="tag">&lt;p <span class="tprop">class=</span><span class="tval">"special"</span>&gt;
    &lt;/p&gt;</span>
<span class="tag">&lt;/div&gt;</span>
</code></pre>

                <p>And our CSS:</p>

<pre class="block-code">
<code><span class="dec">p.special {
    <span class="prop">color</span>: <span class="val">red</span>;
    <span class="prop">font-size</span>: <span class="val">16px</span>;
}
#header p {
    <span class="prop">font-family</span>: <span class="val">Arial, sans-serif</span>;
    <span class="prop">font-size</span>: <span class="val">18px</span>;
}</span>
</code></pre>

                <p>First, let's make sure we understand these rules. The first rule is selecting a <strong>&lt;p&gt;</strong> tag with the class of '<strong>special</strong>'. The second rule is selecting a <strong>&lt;p&gt;</strong> tag that is a child of (or nested in) a tag with the id of '<strong>header</strong>'.  Either rule could apply to the same paragraph in our example.  So how is this particular paragraph going to be formatted? In this case it will be red, Arial, 18px.  Did you see what happened there?  The browser took the font color of red from the first rule and the font type of Arial from the second rule, effectively combining them.  Those properties where not in conflict between the two rules.  The browser only has to make the decision on which font size to choose because that is the conflicting property.  By the way, this is one example of how the cascade works, combining properties from different rules. <a href="/html-training-chicago.php">Chicago-based HTML and CSS training</a>.</p>

                <p>In our example above the '<strong>#header p</strong>' rule won on font size because it is the more specific of the two.  Again, think about it, the rule '<strong>p.special</strong>' could apply to multiple paragraphs that have the class of '<strong>special</strong>' no matter where they exist on the page, but the '<strong>#header p</strong>' rule only applies to paragraphs that are inside the <strong>&lt;div id="header"&gt;</strong> tag, and that can only be in one place on the page.  Remember, IDs can only be used once per page, classes can be used multiple times in different elements.</p>

                <p>If you got that one, good for you, but some of us are still scratching our heads and asking, "How can you know for sure which rule is more specific?".</p>

                <p>Here is a good formula to help you determine just that.  Look at all the selectors in the rule and give them a value based on a point system:</p>

<pre><code>IDs = 100
Classes = 10
Tags = 1
</code></pre>

                <p>Add those values up and you get a <strong>Specificity Value</strong> for the rule.  Whichever rule has the higher value wins.  You treat each type of selector separately, even if they are attached.  For example, the selector of '<strong>div#header</strong>' would have a value of 101, 1 for the tag selector of '<strong>div</strong>' and 100 for the id selector of '<strong>#header</strong>', even though it is only referring to one tag.  Let's look at some for more examples:</p>

<pre><code>#header = 100
.special = 10
div = 1
div.special = 11 (remember, treat each selector separately)
div p.special = 12
div#header.special = 111
div p.special = 12
#header p.special = 112
#content blockquote.special p.first = 122
</code></pre>

                <p>Did you follow that last one?  Here is the HTML it might apply to:</p>

<pre class="block-code">
<code><span class="tag">&lt;div <span class="tprop">id=</span><span class="tval">"header"</span>&gt;</span>
    <span class="tag">&lt;blockquote <span class="tprop">class=</span><span class="tprop">"special"</span>&gt;</span>
            <span class="tag">&lt;p <span class="tprop">class=</span><span class="tval">"first"</span>&gt;
            &lt;/p&gt;</span>
    <span class="tag">&lt;/blockquote&gt;</span>
<span class="tag">&lt;/div&gt;</span>
</code></pre>

                <p>The rule was referring to the &lt;p&gt; tag with a class of 'first', inside of the &lt;blockquote&gt; with the class of '<strong>special</strong>', inside of the &lt;div&gt; with the id of '<strong>header</strong>'.  See how very specific that rule was?</p>

                <p>Using the Specificity Value Formula will always answer that question of which rule is more specific, and this is the actual formula the browser uses.  You can use this knowledge of specificity to create different rules that play well together. It  allows you to understand how the browser evaluates which declarations apply when there is a conflict. <a href="/html-training-los angeles.php">LA HTML and CSS classes</a>.</p>

                <p>Now, what if the two rules have the same specificity value?</p>

<pre class="block-code">
<code><span class="tag">&lt;p class="special first"&gt;</span>
</code></pre>
<pre class="block-code">
<code><span class="dec">p.special {
    <span class="prop">font-size</span>: <span class="val">12px</span>;
}
p.first {
    <span class="prop">font-size</span>: <span class="val">14px</span>;
}</span>
</code></pre>

                <div style="margin-left: 50px;">[Note: tags can have more than one class or id by separating them with spaces in the class value]</div>

                <p>Same value, correct?  Add them up, both have a value of 11.  So in our example above which rule wins?  In this case the cascade takes over and the second rule wins so the paragraph will have the font size of 14px.  Whenever the specificity value is the same, order does matter, the rule listed last wins.  Cascade also applies to embedded style sheets and inline styles. The way the browser sees it, the closest rule to the HTML tag wins.  So, if you had rules that apply to the same tag and the same specificity, one in the attached style sheet, and one in the embedded style sheet, and then one as an inline style, the inline style is closest so it wins.  If you aren't familiar with the different places you can add style to your web pages, see my article, "<a href="https://www.trainingconnection.com/html/lessons/styles.php">Connecting to Style</a>".</p>

                <p>As you can see, understanding how the browser handles conflicts in style declarations through specificity and the cascade will be helpful for not only troubleshooting our rules but will allow us to create more complex styles.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/styles.php">Connecting CSS to HTML</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>