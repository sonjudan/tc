<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php'; ?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="#">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">HTML Heredity</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>HTML/CSS Heredity & Other Relations</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Heredity is the passing of traits from parents to their offspring.  Ok, that's part of the dictionary definition, but what does this have to do with HTML?  Did you know that there is heredity being passed down through our HTML tags?  Well, heredity, as in "passing of traits", or in this case, properties.  Properties are beings passed down to nested, or child tags from the parent.  Now, if it seems we have gotten off track and you feel like you stumbled across a topic on genetics, let's start from the beginning. <a href="/html-training.php">Looking for HTML and CSS training</a>?</p>

                <p>In a standard HTML structure of tags we have a hierarchy of sorts.  Tags can be nested inside of other tags.  The most common nesting of tags is that most of our content tags are nested inside of the <strong>&lt;body&gt;</strong> tag.</p>


<pre class="block-code">
<code><span class="tag">&lt;body&gt;</span>
    <span class="tag">&lt;h1&gt;</span>HTML Heredity &amp; Other Relations<span class="tag">&lt;/h1&gt;</span>
    <span class="tag">&lt;p&gt;&lt;strong&gt;</span>Heredity<span class="tag">&lt;/strong&gt;</span> is the passing of traits from parents to their offspring.<span class="tag">&lt;/p&gt;</span>
<span class="tag">&lt;/body&gt;</span>
</code></pre>

                <p>Look at the code above.  Both the <strong>&lt;h1&gt;</strong> and the <strong>&lt;p&gt;</strong> tag are nested inside of the <strong>&lt;body&gt;</strong> tag.  And, the <strong>&lt;strong&gt;</strong> tag is nested inside of the <strong>&lt;p&gt;</strong> tag.  In HTML we refer to this nesting as a parent/child relationship.  The <strong>&lt;body&gt;</strong> tag is the parent of the <strong>&lt;h1&gt;</strong> and <strong>&lt;p&gt;</strong> tags, and conversely the <strong>&lt;h1&gt;</strong> and <strong>&lt;p&gt;</strong> tags are children of the <strong>&lt;body&gt;</strong> tag.  In our example the <strong>&lt;p&gt;</strong> tag is also a parent of the <strong>&lt;strong&gt;</strong> tag and, you guessed it, the <strong>&lt;strong&gt;</strong> tag is a child of the <strong>&lt;p&gt;</strong> tag.</p>

                <p>Traits, or properties of these HTML tags are passed down to their descendants much in the same way we inherit traits from our ancestors.  If we set a font color of blue for the <strong>&lt;body&gt;</strong> tag, then all of the tag's children will inherit that color.  In the <strong>&lt;strong&gt;</strong> tag's case, it is inheriting the color from its parent, the <strong>&lt;p&gt;</strong> tag, which in turn got it from its parent, the <strong>&lt;body&gt;</strong> tag. This is why we often use the <strong>&lt;body&gt;</strong> tag properties as a way to set the defaults on a web page.  We create a CSS style rule for the <strong>&lt;body&gt;</strong> tag and declare anything we don't want to leave up to the browser to decide, like font color, type, and size, because we know that all of the descendants will inherit those properties. So, if we never set font properties for the <strong>&lt;p&gt;</strong> tags, they will still be set by the <strong>&lt;body&gt;</strong> through inheritance. </p>

                <p>Now you may be thinking, "That's all well and good, but if all the children of the <strong>&lt;body&gt;</strong> inherit font properties, and we set the <strong>&lt;body&gt;</strong> font size to 14px, then how come the <strong>&lt;h1&gt;</strong> tag still show as a larger font-size?" Good question! I'm glad you are paying attention. <a href="/html-training-los angeles.php">Onsite HTML training in Los Angeles</a>.</p>

                <p>By design, some tags do <strong>NOT</strong> inherit certain properties.  Our <strong>&lt;h1&gt;</strong> tag will never inherit font-size from its parent, and for good reason.  How would the reader know it was a heading if it looked like everything else on the page? There are other tags that don't inherit certain properties.  Take the <strong>&lt;a&gt;</strong> tag for example.  It does not inherit font color or text-decoration.  That is why without intervention from an 'a' selector in the CSS styles, the <strong>&lt;a&gt;</strong> tag is blue and underlined by default.  Again, if it just looked like the paragraph around it, how would the reader know it was a clickable link?</p>

                <p>There are also certain properties in general that don't get inherited regardless of tag type.  The margin and padding properties for example.  Just because you set a margin on the <strong>&lt;body&gt;</strong> wouldn't mean that you want ALL the tags to have that same margin.  You can find a complete list of properties at <a class="html" href="https://www.w3.org/TR/CSS21/propidx.html">https://www.w3.org/TR/CSS21/propidx.html</a> that indicates whether a property in general is inherited or not.</p>

                <p>Something else to consider.  If we do set a particular property on a tag through CSS, then that property will not inherit from its parent.  We have overridden that default inheritance.  So, as a general rule you could say that inheritable properties will inherit the value from their parent unless specifically set from a rule in the CSS styles.</p>

                <p>Let's look at an interesting example that comes up my <a class="html" href="/html-training.php">HTML and CSS training class</a> here at <em>Training Connection</em>.  Consider the follow HTML and CSS code:</p>

<pre class="block-code">
<code><span class="tag">&lt;style&gt;</span>
    <span class="dec">p {
        <span class="prop">font-size</span>: <span class="val">small</span>;
        <span class="prop">color</span>: <span class="val">navy</span>;
    }
    .fun {
        <span class="prop">color</span>: <span class="val">#339999</span>;
        <span class="prop">font-family</span>: <span class="val">Georgia, Times, serif</span>;
        <span class="prop">letter-spacing</span>: <span class="val">0.05em</span>;
    }
    blockquote.fun {
     <span class="prop">font-style</span>: <span class="val">italic</span>;
    }</span>
<span class="tag">&lt;/style&gt;</span>

<span class="tag">&lt;body&gt;</span>
    <span class="tag">&lt;blockquote <span class="tprop">class=</span><span class="tval">"fun"</span>&gt;</span>
        <span class="tag">&lt;p&gt;</span>"Four score and seven years ago..."<span class="tag">&lt;/p&gt;</span>
    <span class="tag">&lt;/blockquote&gt;</span>
<span class="tag">&lt;/body&gt;</span>
</code></pre>

                <p>The result of this code will make this quote from the "Gettysburg Address" appear in a navy color, font Georgia, and letter spacing of 0.05em.  (If you don't know what an 'em' is, look at this article, "<a class="html" href="http://alistapart.com/article/emen" target="_blank">The Trouble With EM 'n EN</a>", by <em>Alistapart Magazine</em>.) When my students first apply the class of '<strong>fun</strong>' to the <strong>&lt;blockquote&gt;</strong> they wonder why the color is navy and not #339999 (that's <span style="color: #339999;">teal</span>).</p>

                <p>Well, let's look at the general rule I mentioned above one more time, <strong>"Inheritable properties will inherit the value from their parent unless specifically set from a rule in the CSS styles"</strong>.  Look at the style section of the code.  You see, there is a rule for the <strong>&lt;p&gt;</strong> tag to set the color to navy.  The '<strong>fun</strong>' class was assigned to the <strong>&lt;blockquote&gt;</strong>, not the <strong>&lt;p&gt;</strong>.  The <strong>&lt;p&gt;</strong> tag will not inherit font color from its parent because it has specifically been told to have a font color of navy from a rule in the CSS styles.  If we move the class of 'fun' out of the <strong>&lt;blockquote&gt;</strong> and assign it to the <strong>&lt;p&gt;</strong> tag instead, then the paragraph will take on the teal color.</p>

                <p>If you don't understand why the <strong>&lt;p&gt;</strong> tag with a class of '<strong>fun</strong>' would take on the '<strong>.fun</strong>' rule properties and not the '<strong>p</strong>' rule properties then you should see my article, "<a class="html" href="https://www.trainingconnection.com/html/lessons/specificity.php">Being Specific. CSS Specificity Demystified!</a>". </p>

                <p>As you can see, especially from our example above, that understanding how HTML/CSS inheritance works will go a long way in understanding how the elements render on the web page.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/specificity.php">CSS Specificity</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>