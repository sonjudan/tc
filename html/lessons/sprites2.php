<?php
$meta_title = "Creating and Using a Sprite Sheet - Part 2 | Training Connection";
$meta_description = "How to create a sprite sheet in CSS part 2. This and other HTML and CSS topics are covered in our hands-on workshops, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">HTML Sprite Sheets Cont.</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating and Using a Sprite Sheets in HTML and CSS - Part 2</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>In <a href="/html/lessons/sprites1.php">Part 1 of this series </a>we covered what a sprite sheet is and how they work in web design. In this part we look at how to create rollover effects using sprite sheets and how to create your own sprite sheet.</p>

                <h4>Rollover effects using sprite sheets</h4>

                <p>Using sprite sheets in rollover effects is also pretty easy. First, create a sprite sheet (explained below) that contains both the inactive and active states of the rollover. Here's a simple example:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/html/buttonexp.png" alt="buttons example"></p>

                <p>Here I want the green button text to be the inactive state and the red text to be the active state. All we need to do is change the background-position on a CSS :hover state to trigger the rollover:</p>

<pre class="block-code">
<code><span class="dec">.rollover {
    <span class="prop">background</span>: <span class="val">url('rollover1.png') no-repeat</span>;
    <span class="prop">width</span>: <span class="val">100px</span>;
    <span class="prop">height</span>: <span class="val">35px</span>;
    <span class="prop">display</span>: <span class="val">inline-block</span>;
    <span class="prop">background-position</span>: <span class="val">0 0</span>;
}</span>
</code></pre>

<pre class="block-code">
<code><span class="dec">.rollover:hover {
    <span class="prop">background-position</span>: <span class="val">0 -35px</span>;
}</span>
</code></pre>

                <p>Now in the browser, the rollover div displays the green button text. When you move the mouse over the div, the red button text is displayed.</p>

                <p>Remember, you can also combine multiple rollover images together into a single larger sprite sheet for better efficiency.</p>

                <h4>Creating your own sprite sheet</h4>
                <p>Creating your own sprite sheet is also fairly straightforward, especially if you're using a drawing tool like Illustrator or Photoshop that provides layer and/or guidelines:</p>

                <ol>
                    <li>Determine the size you want your images to be. For example, let's say I want to make nine 30 pixel square icons.</li>
                    <li>Figure out the spacing between each icon in your sprite sheet. To keep the math simple I'm going to use a 10 pixel spacing. </li>
                    <li>Create a guide grid by placing guides or by drawing the grid on a separate, semi-opaque layer. This grid should ideally start in from the edges of the sheet. Each grid cell should be the total size of the icon plus the spacing, so 40 pixels in our example.</li>
                    <li>Draw (or copy) the icon images inside the grid cells, aligned to the top left corner of each cell.
                        It should be pretty simple to work out the offsets for each cell at this point. Assuming we used a 20 pixel margin around a 3 x 3 grid, the offsets in our example should be:
                        <ul>
                            <li>Top row: 20px 20px, 60px 20px, 100px 20px</li>
                            <li>Middle row: 20px 60px, 60px 60px, 100px 60px</li>
                            <li>Bottom row: 20px 100px, 60px 100px, 100px 100px</li>
                        </ul>
                    </li>
                </ol>
                <p>Don't forget to use negative values when setting the background-position property for each icon. <a href="/html-training-chicago.php">Hands-on HTML training in Chicago</a>.</p>
                <p>Keep in mind that the icons, chicklets, buttons, or whatever other graphics you want do NOT have to be the same size. As long as you know the size of the graphic element, and its offsets in the sprite sheet, you can have several mixed-sized buttons in the same sprite sheet, like this one:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/html/sprite2.png" alt="sprite sheet 2"></p>
                <p>Courtesy <a href="http://free-web-buttons.com/free-web-site-buttons.html">http://free-web-buttons.com/free-web-site-buttons.html</a></p>

                <h4>Using a sprite sheet generator</h4>
                <p>If you don't want to go to the trouble of creating your own sprite sheet, especially if you already have your graphics as separate files, you can use one of the many sprite sheet generators available online. Many of these tools are web-based applications, like <a href="http://spritegen.website-performance.org/">CSS Sprite Generator</a> or <a href="http://zerosprites.com/">ZeroSprites</a>.</p>

                <p>I've noted that a lot of the download tools are more focused on creating image assets for games rather than for website use, so they may not be as suitable for web design.</p>

                <p>Note that most of these tools require that you have already created the images you want to combine into your sprite sheet. If you're starting completely from scratch, you may want to just put all your icons on a single sheet right at the outset. However, you have more flexibility if you initially create each graphic in a separate file and then combine them into your sprite sheet once you've finalized each one. <a href="/html-training-los angeles.php">HTML classes offered in Los Angeles</a>.</p>

                <h4>Conclusion</h4>
                <p>Sprite sheets are a great way to optimize a site that uses many smaller images by consolidating them into a single, larger image and controlling it using CSS instead of using a Javascript loading script.</p>
                <p>Creating sprite sheets isn't difficult, even when creating them from scratch. There are tools out there that make their creation even easier by automatically or manually consolidating all your graphics files into a single larger image.</p>


                <p>You can learn more about this and other advanced techniques in our <a href="/html-training.php">Advanced HTML/CSS</a> course.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/heredity.php">CSS heredity</a></li>
                        <li><a href="/html/lessons/styles.php">Applying CSS to HTML </a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>