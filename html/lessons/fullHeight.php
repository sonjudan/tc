<?php
$meta_title = "Creating a Full-Height Web Page Layout | Training Connection";
$meta_description = "Learn the different ways to embed CSS styles into HTML. These techniques are taught in detail on our HTML Fundamentals course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Full-Height Layout</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating a Full-Height Web Page Layout</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>A common issue that comes up when creating web pages is what happens when the content on a page is smaller than the screen height. For example, take a look at the following simple layout:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_1" src="https://codepen.io/jwiatt/embed/KNyPPX?height=600&amp;theme-id=0&amp;slug-hash=KNyPPX&amp;default-tab=html%2Ccss&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%201&amp;name=cp_embed_1" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 1" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_KNyPPX"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script><p></p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_2" src="https://codepen.io/jwiatt/embed/KNyPPX?height=600&amp;theme-id=0&amp;slug-hash=KNyPPX&amp;default-tab=result&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%201&amp;name=cp_embed_2" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 1" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_KNyPPX"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script><p></p>
                <p>We can see that the content is not large enough to fill the screen, leaving a gap below the footer. While this is functional, it's not particularly attractive. Ideally, even when there's very little content on a page, it would be nice if the page were at least as large as the viewport, so the footer would be at the bottom of the screen. Fortunately, this is relatively simple to accomplish. <a href="/html-training.php">HTML 5 instructor-led classes</a>.</p>

                <h4>Setting the Layout Height</h4>
                <p>Although it's common to use the <strong>width</strong>, <strong>min-width</strong>, and <strong>max-width</strong> properties in layouts, it's less common to use the <strong>height</strong>, <strong>min-height</strong>, and <strong>max-height</strong> properties. It makes sense, since most of the time in our layouts, we want to have the <strong>height</strong> property left at its default auto value so the element is sized to whatever content it contains. Sometimes, though, that can backfire on us, leaving us with some unsightly gaps such as the one in the example above.</p>

                <p>The first step is to add the <strong>min-height</strong> property to the wrapper container:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_3" src="https://codepen.io/jwiatt/embed/yVPBzq?height=250&amp;theme-id=0&amp;slug-hash=yVPBzq&amp;default-tab=css&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%202&amp;name=cp_embed_3" scrolling="no" frameborder="0" height="250" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 2" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_yVPBzq"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>The <strong>min-height</strong> property prevents the element from becoming any shorter than the property’s value. Because we can't know the viewport's exact size, we use "100%" as the size.</p>

                <p>Also note that the <strong>box-sizing</strong> property has been set to "border-box" instead of the default "content-box" setting. This is a good idea if you decide to do things such as add padding and/or border (but not margin) to the wrapper later, as "border-box" sets the height value to equal the content box height plus padding and border. It just makes things easier, especially with full-height layouts.</p>

                <p>Anyway, we're not done yet. Just setting the wrapper's <strong>min-height</strong> to "100%" won’t do anything. Any time you use the percent unit in layout properties, you're setting that element's property relative to the element's parent property. In this case, we're setting the wrapper to the full height of the BODY element.</p>

                <p>The problem is that the BODY's height property is also set to "auto", and thus can't contribute a value for wrapper's <strong>min-height</strong> to work. We have to give all of the wrapper element's ancestors a <strong>height</strong> value:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_4" src="https://codepen.io/jwiatt/embed/bBYGdj?height=203&amp;theme-id=0&amp;slug-hash=bBYGdj&amp;default-tab=css&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%203&amp;name=cp_embed_4" scrolling="no" frameborder="0" height="203" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 3" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_bBYGdj"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>Why do we need to do this? Because we're still using the percent unit for the height value. So the wrapper is at least the full height of the BODY element, which is the full height of the HTML element, which gets its height from the viewport. Once we do that, we begin seeing the fruits of our labor:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_5" src="https://codepen.io/jwiatt/embed/gLXOrv?height=600&amp;theme-id=0&amp;slug-hash=gLXOrv&amp;default-tab=result&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%204&amp;name=cp_embed_5" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 4" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_gLXOrv"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>It's not quite perfect, but we're making progress! </p>

                <p>Now all we have to do is get the footer to the bottom of the page.</p>

                <h4>Repositioning the Footer</h4>

                <p>We can get the footer where we want it with a bit of positioning CSS:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_6" src="https://codepen.io/jwiatt/embed/ENbxZK?height=450&amp;theme-id=0&amp;slug-hash=ENbxZK&amp;default-tab=css&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%205&amp;name=cp_embed_6" scrolling="no" frameborder="0" height="450" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 5" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_ENbxZK"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>First we set the wrapper to be <strong>position: relative</strong>. This is because when we set an element's positioning to "absolute", its positioning will be anchored to either its closest relatively-positioned ancestor or to the document if no such ancestor is found. So the "pagefooter" element will be anchored to the wrapper element position instead of the document. Otherwise setting the pagefooter's width to 100% would be 100% of the overall document width (the viewport width in this case) instead of 100% of the wrapper's width.</p>


                <p>If you would like to learn more about using CSS Styles and HTML, Training Connection offers a three-day <a class="html" href="/html-training-chicago.php">HTML/CSS class</a> in our Chicago and Los Angeles locations.</p>

                <p>The result is seen here:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_7" src="https://codepen.io/jwiatt/embed/NbwWgo?height=600&amp;theme-id=0&amp;slug-hash=NbwWgo&amp;default-tab=result&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%206&amp;name=cp_embed_7" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 6" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_NbwWgo"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>Looking much better! One final thing to do is to add padding (not margin) to the bottom of the wrapper equal to the height of the footer so that any content longer than the viewport height doesn't get obscured by the absolutely-positioned footer:</p>

                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_8" src="https://codepen.io/jwiatt/embed/rWYNpx?height=265&amp;theme-id=0&amp;slug-hash=rWYNpx&amp;default-tab=css&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%207&amp;name=cp_embed_8" scrolling="no" frameborder="0" height="265" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 7" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_rWYNpx"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>After adding additional paragraphs to the page we see that everything looks ok:</p>
                <div class="embed-responsive embed-responsive-21by9 mb-3">
                    <iframe name="cp_embed_9" src="https://codepen.io/jwiatt/embed/LbOYQL?height=600&amp;theme-id=0&amp;slug-hash=LbOYQL&amp;default-tab=result&amp;user=jwiatt&amp;embed-version=2&amp;pen-title=HTML%20Example%208&amp;name=cp_embed_9" scrolling="no" frameborder="0" height="600" allowtransparency="true" allowfullscreen="true" allowpaymentrequest="true" title="HTML Example 8" class="cp_embed_iframe " style="width: 100%; overflow:hidden; display:block;" id="cp_embed_LbOYQL"></iframe>
                </div>
                <script async="" src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

                <p>There we have it – a full-height layout that works across all modern (IE 8+) browsers. <a href="/html-training-los angeles.php">LA-based HTML training classes</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/specificity.php">CSS Specificity</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>