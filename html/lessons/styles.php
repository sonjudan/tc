<?php
$meta_title = "Connecting CSS StyleSheets to HTML | Training Connection";
$meta_description = "Learn the different ways to embed CSS styles into HTML. These techniques are taught in detail on our HTML Fundamentals course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Connecting to Style</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Connecting CSS StyleSheets to HTML</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In my <a href="/html-training.php">HTML/CSS class at Training Connection</a> we learn different ways of adding CSS styles to our HTML documents.  There are benefits and drawbacks to each method available but first let's make sure we understand what a CSS Style is and why we use them. CSS Styles are used to add style to our HTML pages.  You can think of this as the formatting for the page.  CSS Styles are instructions to the browser on how we would like our content to appear.  For example, we may want the main body of our content, our paragraphs, to appear in Arial, size 14, using a grey color.  But, we would like our level one headings to appear in Times, size 24, in the color navy.  This is the type of information we can add to our styles and these instructions can be connected to our pages using different techniques.  Let us start with the most common method of adding style instructions.</p>

                <h4>Attached Style Sheets</h4>

                <p>The most common method of using CSS Styles is attaching an external style sheet to each page on our website.  The external sheet contains information in CSS language about what formatting we want to apply to the web page and which element (tag) gets which formatting. For example, if we want all of our <strong>&lt;h1&gt;</strong> tags to have the formatting we mentioned above, Times, size 24, in the color navy, then we would add the following CSS code to the external style sheet:</p>


<pre class="block-code">
<code>h1 {
    font-family: Times;
    font-size: 24px;
    color: navy;
}</code></pre>

                <p>In this case, the style information is applied to every <strong>&lt;h1&gt;</strong> tag on the page.  The '<strong>h1</strong>' before the opening curly brace is called the selector.  It is the tag name (without the &lt;&gt;) that we have selected that will use the property value pairs within the curly braces.  Inside the curly braces are the declarations, each having a property name and a value we wish to assign the property. The property and value are separated by a colon and the declaration ends in a semicolon.  As you can see, you can have multiple declarations for the same selector, unlimited in fact.  The entire set, selector and declarations, are called a CSS Rule.</p>

                <p>Another example, if you want all of your paragraphs, the <strong>&lt;p&gt;</strong> tag, to be Arial, size 14, using a grey color, you would add the '<strong>p</strong>' selector to the page:</p>

<pre class="block-code">
<code>h1 {
    font-family: Times;
    font-size: 24px;
    color: navy;
}
p {
    font-size: 14px;
    color: grey;
}</code></pre>

                <p>You may list as many rules to the style sheet as you need.</p>

                <p>There are other types of selectors besides the tag selector.  There are <strong>id</strong> selectors and <strong>class</strong> selectors that look like the following:</p>

                <h6>#header – A tag with the id of '<strong>header</strong>'.  In the HTML it might be a <strong>&lt;div&gt;</strong> tag with this id.  It would look like this:</h6>


<pre class="block-code"><code><span class="tag">&lt;div <span class="tprop">id=</span><span class="tval">"header"</span>&gt;</span></code></pre>


                <h6>.special – A tag with the class of 'special'. It would look like this in the HTML if it were a <p> tag: </h6>

<pre class="block-code">
<code><span class="tag">&lt;p <span class="tprop">class=</span><span class="tval">"special"</span>&gt;</span>
</code></pre>

                <p>You can use the same type of declarations with these selectors.  For example, if you had a <strong>&lt;div&gt;</strong> tag with the id of '<strong>header</strong>' as above and you wanted to give it a background color of blue and a font color of white:</p>

<pre class="block-code">
<code>#header {
    background-color: blue;
    color: white;
}</code></pre>

                <p>The style sheet is then attached to each web page you wish to use the style rules for, using a <strong>&lt;link&gt;</strong> tag.   The <strong>&lt;link&gt;</strong> tag is added to the header section like this:</p>

<pre class="block-code">
<code><span class="tag">&lt;head&gt;</span>
    <span class="tag">&lt;link <span class="tprop">rel=</span><span class="tval">"stylesheet"</span> <span class="tprop">type=</span><span class="tval">"text/css"</span> <span class="tprop">href=</span><span class="tval">"styles.css"</span>&gt;</span>
<span class="tag">&lt;/head&gt;</span>
</code></pre>

                <p>The '<strong>href</strong>' value points to the name of the external style sheet.  It can be named anything you want as long as it has a .css extension.  The '<strong>rel</strong>' and '<strong>type</strong>' values essentially tell the browser that this is a CSS Style sheet. You may have different style sheets attached to different pages on your site or multiple style sheets attached to the same page but this an advanced technique that goes beyond the scope of this short tutorial. <a href="/html-training.php">HTML classes in Chicago</a>.</p>

                <h4>Embedded Style Sheet</h4>

                <p>The second way of using styles is using an Embedded Style Sheet.  First, the word "sheets" in the name can be misleading.  When we think of a sheet in web design we think of something separate, like the different pages in our website or like the style method above (external style sheet).  The term "sheet" is used loosely here, it simply means a separate section in our web page, hence the word "embedded".  An Embedded Style Sheet is a section in each page that contains the same style information that we used in our external style sheet but it applies only to the page the rules are embedded in.  It is usually enclosed in the <strong>&lt;head&gt;</strong> section at the beginning of the page.  Here is an example of applying the same paragraph formatting we did in the previous example, the embedded style section is highlighted in red:</p>

<pre class="block-code">
<code><span class="tag">&lt;head&gt;</span>
    <span class="tag">&lt;style&gt;</span>
        <span class="dec">p {
            <span class="prop">font-family</span>: <span class="val">Arial</span>;
            <span class="prop">font-size</span>: <span class="val">14px</span>;
            <span class="prop">color</span>: <span class="val">grey</span>;
        }</span>
    <span class="tag">&lt;/style&gt;</span>
<span class="tag">&lt;/head&gt;</span></code></pre>

                <p>Remember, this appears on the web page that has the content with HTML.  We can use Embedded Style Sheets to declare how we want the entire page to appear by adding more CSS Rules like this:</p>

<pre class="block-code">
<code><span class="tag">&lt;head&gt;</span>
    <span class="tag">&lt;style&gt;</span>
        <span class="dec">p {
          <span class="prop">font-family</span>: <span class="val">Arial</span>;
          <span class="prop">font-size</span>: <span class="val">14px</span>;
          <span class="prop">color</span>: <span class="val">grey</span>;
        }
        h1 {
            <span class="prop">font-family</span>: <span class="val">Times</span>;
              <span class="prop">font-size</span>: <span class="val">24px</span>;
              <span class="prop">color</span>: <span class="val">navy</span>;
        }</span>
    <span class="tag">&lt;/style&gt;</span>
<span class="tag">&lt;/head&gt;</span></code></pre>

                <p>Here we have added format information for all of our <strong>&lt;h1&gt;</strong> tags on the page.  The new rule is highlighted in red. Notice I changed the order of the rules in this section compared to the external style sheet.  For the most part the order does not matter except in circumstances where you have conflicting style information for the same tag from separate rules.  See the article, "Being specific. CSS Specificity demystified!".</p>
                <p>
                    Also see <a href="https://firstsiteguide.com/html-for-beginners/" target="_blank">HTML for beginners</a></p>

                <h4>Inline Styles</h4>

                <p>Lastly, the least used way of adding styles to a web page is adding style information directly to the HTML tag, hence the name Inline Style.  Here is an example of adding the style we discussed above to a paragraph tag:</p>

<pre class="block-code">
<code><span class="tag">&lt;p <span class="tprop">style=</span><span class="tval">"<span class="prop">font-family</span>: <span class="val">Arial</span>; <span class="prop">font-size</span>: <span class="val">14px</span>; <span class="prop">color</span>: <span class="val">grey</span>;"</span>&gt;</span>
</code></pre>

                <p>This formatting would apply to the entire content of this one paragraph tag.  If we want all our paragraphs to appear the same we would have to add the same Inline Style to each and every paragraph tag on all of our pages.  This could be a lot of work depending on the amount of content we have.</p>

                <p>As you can see by our example we use the CSS '<strong>property: value;</strong>' syntax but we leave out the declaration as we declare what tag is to be effected by inserting the '<strong>style</strong>' property directly in the tag itself. The benefit of using Inline Styles is the formatting information is kept in the same page and close to the content it is effecting.  It also has the added benefit of overriding information from the other sources of styles discussed above.  It is however more difficult to maintain, especially with larger web sites.</p>

                <p>So there you have it, three different methods of using styles in our web site.  An attached style sheet, External Style Sheet, an embedded styles section, Embedded Style Sheet, and Inline Styles, putting the format information in a '<strong>styles</strong>' attribute of the tag itself.</p>

                <p>If you would like to learn more about using CSS Styles and HTML, Training Connection offers a three-day <a href="/html-training.php">HTML/CSS class</a> in our Chicago and Los Angeles locations.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/specificity.php">CSS Specificity</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>