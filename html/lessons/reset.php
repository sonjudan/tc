<?php
$meta_title = "Using HTML Reset Stylesheets | Training Connection";
$meta_description = "Learn how to use HTML Reset Stylesheets. This is taught in our HTML Fundamentals course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reset Stylesheets</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Using HTML Reset Stylesheets</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>One of the challenges in web design is in making your pages look consistent across the different browsers. Every browser has a user agent stylesheet which it uses to provide default style values when rendering a web page. In the absence of any other style rules, the browser falls back to the user agent stylesheet values to "fill in" missing property values. <a href="/html-training.php">Instructor-led HTML training</a>.</p>
                <p>For example, here is a very simple HTML page with no styles other than the browser's (Chrome in this case) user-agent stylesheet applied:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/html/reset1.png" alt="HTML Reset Stylesheets"> </p>
                <p>The problem is, there is no "universal" default style – the different browsers each have their own ideas of how the various elements should look. Although many of the modern browsers' default styles look pretty close, there are some variations between browser stylesheets that could subtly throw off your design from browser to browser. <a href="/html-training-chicago.php">Public HTML classes in the Chicago Loop</a>.</p>
                <p>While the goal of web design should not be to make everything pixel-perfect, we should strive for as much cross-browser consistency in rendering and behavior as possible. One method used to accomplish this is through the use of "reset" stylesheets.</p>
                <h4>What is a reset stylesheet?</h4>
                <p>Reset styles were first used in 2004 as a way to standardize how the browsers rendered your web pages. Some of the earliest did nothing more than eliminate the margin and padding from all the elements (called a "hard" reset) while others went a bit further, resetting element font sizes, stripping text decorations, and so on.
                </p>
                <p>The results after applying the reset style were just plain-looking text that was all the same size – not very pretty at all. This is the same page as above, with <a class="html" href="http://meyerweb.com/eric/tools/css/reset/" target="_blank">Eric Meyer's Reset CSS</a>  reset stylesheet applied:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/html/reset2.png" alt="Eric Meyer's reset style sheet"> </p>
                <p>The intent behind using a reset style is that the designer should have full control over how their web page gets rendered by the browser. By using a reset style, then defining their own styles, the designer can be assured of what CSS property values the browsers are using for every element.</p>
                <p>A common criticism of using reset stylesheets is that they make your CSS redundant, which violates what's known as the DRY (Don't Repeat Yourself) principle. Using a reset style to zero out an element's properties, only to have to redefine them later, is extra, unnecessary work, when the style could simply have been set once to begin with. <a href="/html-training-los angeles.php">Los Angeles classes for HTML</a>.</p>
                <p>Others argue that this redundancy is a good thing, in that it forces the designer to properly account for the elements and their style properties used on the site. If an element is missed it quickly becomes apparent when the design is rendered. It also allows for separation of the reset stylesheet from the styles used in a particular project.</p>
                <h4>CSS Normalization</h4>
                <p>Having to completely define the styles on the elements after using a reset style is admittedly a bit of a chore to do for each project. It would be much easier if we could just let the user-agent stylesheets do most of the heavy lifting, and then we simply change what we need. Although we can’t rely on the browsers' stylesheets to give us a consistent look, we have another option.</p>
                <p>In response to the need to make site design a bit easier, CSS normalization stylesheets began to be developed. These stylesheets went a step beyond simply resetting the styles and defined their own base styling, much like the user-agent stylesheets, but integrating many of the most common styling practices.</p>
                <p>Here's our sample page again, this time using <a class="html" href="http://jonathantneal.github.io/sanitize.css/" target="_blank">Sanitize.css</a>:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/html/reset3.png" alt="CSS Normalization"> </p>
                <p>As you can see, there's a good bit less work involved in defining our projects' custom styling when using a normalization stylesheet instead of just a plain reset style.</p>
                <h4>Beyond Reset and Normalization Stylesheets</h4>
                <p>Some of these boilerplate stylesheets eventually evolved even further (or got incorporated) into larger frameworks that not only normalized the page's CSS, but also provide pre-built UI components, often with the help of some JavaScript. Some libraries just include style themes and UI components, like <a class="html" href="http://jqueryui.com/" target="_blank">jQuery UI</a>. Others provide a framework for building layouts, including responsive layouts, such as <a class="html" href="https://html5boilerplate.com" target="_blank">HTML5Boilerplate</a> and <a class="html" href="http://getbootstrap.com/" target="_blank">Bootstrap</a>.</p>
                <h4>Which Should You Use?</h4>
                <p>The choice of whether or not to use some kind of reset style really depends on your (or your team's) preference as well as the project requirements. I do suggest at least starting with one of the normalization stylesheets instead of a basic reset stylesheet to make your life easier. If you're developing a new design that will be responsive (and it should be), Bootstrap is a popular choice.</p>



                <h4>Some popular reset stylesheets:</h4>
                <ul>
                    <li><a class="html" href="http://meyerweb.com/eric/tools/css/reset/" target="_blank">Eric Meyer's Reset CSS 2.0</a></li>
                    <li><a class="html" href="http://html5doctor.com/html-5-reset-stylesheet/" target="_blank">HTML5 Doctor Reset Stylesheet</a></li>
                    <li><a class="html" href="http://html5reset.org/" target="_blank">HTML5 Reset</a> (Actual CSS file is <a class="html" href="https://github.com/murtaugh/HTML5-Reset/blob/master/assets/css/reset.css" target="_blank">here</a>)</li>
                </ul>
                <h4>CSS Normalization stylesheets:</h4>
                <ul>
                    <li><a class="html" href="https://matthewblode.com/marx/" target="_blank">Marx</a></li>
                    <li><a class="html" href="http://necolas.github.io/normalize.css/" target="_blank">Normalize.css</a></li>
                    <li><a class="html" href="http://jonathantneal.github.io/sanitize.css/" target="_blank">Sanitize.css</a></li>
                </ul>
                <h4>Toolkits and Frameworks:</h4>
                <ul>
                    <li><a class="html" href="http://jqueryui.com/" target="_blank">jQuery UI</a></li>
                    <li><a class="html" href="https://html5boilerplate.com" target="_blank">HTML5Boilerplate</a></li>
                    <li><a class="html" href="http://getbootstrap.com/" target="_blank">Bootstrap</a></li>
                </ul>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/fullHeight.php">Creating a Full-Height Web Page Layout</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>