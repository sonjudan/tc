<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php#resource-web">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Title</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Title</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/specificity.php">CSS Specificity</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>