<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php'; ?>

    <main class="page-single-content g-text-html">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/web-development.php">Web Development</a></li>
                    <li class="breadcrumb-item"><a href="#">HTML</a></li>
                    <li class="breadcrumb-item active" aria-current="page">HTML Sprite Sheets</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-web-html5.png" alt="HTML">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating and Using a Sprite Sheet in HTML and CSS - Part 1</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Images are used all over a typical web page; for the logo, as icons, badges, buttons, and of course regular pictures. Some may have special effects such as rollover changes. A responsive page may swap out a smaller image with a larger one as the screen size increases, and vice versa.</p>
                <p>Having all of these images can come at a cost. The browser has to  fetch each of these images the first time they're rendered. On network connections that are slower or have high latency, it may take some time for all the images to appear. There can also be a noticeable delay the first time a rollover is used. If a page's javascript is using window.onload instead of a DOM ready handler — such as jQuery's $(document).ready() function — there can be delays before the script starts.</p>
                <p>Because of issues such as the above, particularly the rollover delay, a common technique was to use a preloading script that would fetch all the images before they were used. That way the image would already be available and the browser wouldn't need to fetch the rollover image when activating the effect. This technique has its advantages and disadvantages, but the main point is that it's not the only method available. If you have a lot of smaller graphics, like icons or buttons, it may be more optimal to use a sprite sheet instead.</p>
                <p>Looking for <a class="html" href="/html-training.php">HTML training class in Chicago or Los Angeles</a>? </p>

                <h4>What's a Sprite Sheet?</h4>
                <p>The term sprite originated in 2D game design. A character to be animated had its animation drawn in cell-style frames. These frames were then consolidated into a single large image asset (the sprite sheet), as in this example:</p>

                <p><img class="img50Width" src="images/sprite1.png" alt="sprite sheet"></p>
                <p>Image courtesy <a class="html" href="http://son-isaki.deviantart.com/art/ETA-Sprite-Sheet-Free-to-Use-416711648" target="_blank">http://son-isaki.deviantart.com/art/ETA-Sprite-Sheet-Free-to-Use-416711648</a></p>

                <p>The computer would then render the frames in rapid sequence just like in cell-based cartoon animation. The result was the character — called a sprite — appeared to be animated.</p>

                <h4>Using sprite sheets in web design</h4>

                <p>Web design applies the sprite sheet concept in a similar manner, just without the animation (most of the time, anyway). Instead of having dozens of small images that each require a round trip to the server to retrieve, all the graphics are placed onto a single large file like this one:</p>

                <p><img class="img50Width" src="images/wpzoom_developer.png" alt=""></p>
                <p>Courtesy <a class="html" href="http://www.wpzoom.com/wpzoom/new-freebie-wpzoom-developer-icon-set-154-free-icons/" target="_blank">http://www.wpzoom.com/wpzoom/new-freebie-wpzoom-developer-icon-set-154-free-icons/</a></p>

                <p>The advantage to a sprite sheet over many separate smaller files is that the computer only needs to make a single request to the server to retrieve all of the icon image assets, instead of repeated smaller requests. Even though the single image is larger than the smaller separate files, the total retrieval time may be quicker depending on network latency. <a href="/html-training-los angeles.php">HTML training classes in Los Angeles</a>.</p>

                <p>In the example, each of the icons are 48x48px, evenly spaced. We need to know the offsets of each of the icon images we want to use. In Windows, we can open the PNG or JPG file in Paint and simply move the cursor crosshairs to line up with the top left corner of each icon. I've drawn some blue guidelines to illustrate what I'm referring to:</p>

                <p><img class="img100Width" src="images/icons-punched-paint.png" alt="icons sample"></p>

                <p>In addition to the blue guidelines I also noted the x and y pixel offsets (from the top left corner of the image) for the icons I'll be using in this example. I wasn't too precise here, which is probably why the numbers aren't at consistent intervals. Usually when making a sprite sheet like this the icons will be evenly spaced, so calculating the offsets are much simpler. <a href="/html-training-chicago.php">Instructor-led HTML training in Chicago</a>.</p>

                <p>Now that we know the offsets, we then have to set up a few classes in our CSS. The first class sets the background to the sprite map and defines the icon size:</p>


<pre class="block-code">
<code><span class="dec">.icon {
    <span class="prop">background</span>: <span class="val">url('icons-punched.png') no-repeat</span>;
    <span class="prop">width</span>: <span class="val">48px</span>;
    <span class="prop">height</span>: <span class="val">48px</span>;
    <span class="prop">display</span>: <span class="val">inline-block</span>;
}</span>
</code></pre>


                <p>Make sure those are proper single (or double) quotes, of course. Also note the display: inline-block setting. That's so the element you use as the container for the icon can have its width and height set but still be treated as inline content.</p>

                <p>Next we define classes that adjust the position of the background for each specific icon we want to use:</p>


<pre class="block-code">
<code><span class="dec">.icon-facebook {
    <span class="prop">background-position</span>: <span class="val">-80px -70px</span>;
}</span>
<span class="dec">.icon-twitter {
    <span class="prop">background-position</span>: <span class="val">-187px -70px</span>;
}</span>
<span class="dec">.icon-envelope {
    <span class="prop">background-position</span>: <span class="val">-80px -178px</span>;
}</span>
</code></pre>

                <p>Notice that when we use background-position we use negative values for the x and y positioning. That's because when you set the positioning values, positive numbers move the item towards the opposite edge you're working from. So if you were to specify a positioning value of top: 50px, you're actually moving the item down (towards the bottom) by 50 pixels. A negative value has the opposite effect. So specifying left: -80px will actually move the item to the left 80 pixels. Specifying the x and y values of background-position as we did here is actually specifying the left and top edges respectively.</p>

                <p>The last thing to do is to use these classes in our HTML. I'll use simple DIV elements here:</p>


<pre class="block-code">
<code><span class="tag">&lt;div class="icon icon-facebook"&gt;&lt;/div&gt;</span>

<span class="tag">&lt;div class="icon icon-twitter"&gt;&lt;/div&gt;</span>

<span class="tag">&lt;div class="icon icon-envelope"&gt;&lt;/div&gt;</span>
</code></pre>

                </div>

                <p>This gives us the following in the browser:</p>

                <p><img class="img50Width" src="images/browser1.png" alt="browser sample"></p>

                <p>Pretty straightforward, right?</p>

                <p><a class="html" href="/html/lessons/sprites2.php" rel="next">In Part 2 of this series</a> we conclude with creating rollover effects and how to create your own sprite sheets.</p>

                <p>You can learn more about this and other advanced techniques in our <a class="html" href="/html/advanced.php">Advanced HTML/CSS</a> course.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-html" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related HTML/CSS Lessons</h4>
                    <ul>
                        <li><a href="/html/lessons/specificity.php">CSS Specificity</a></li>
                        <li><a href="/html/lessons/heredity.php">CSS Heredity</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite HTML/CSS training</h4>
                    <p>Through our network of local trainers we deliver onsite HTML/CSS classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite HTML/CSS class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=15">HTML/CSS reviews</a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>