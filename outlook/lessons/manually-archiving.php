<?php
$meta_title = "Manually Archiving and Retrieving Items in Outlook | Training Connection";
$meta_description = "Learn how manage manual archive and retrieve items in Outlook and setup AutoArchiving. This module is part of our Level 2 Outlook Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/outlook.php">Outlook</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Manually Archiving</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Outlook">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Manually Archiving and Retrieving Items in Outlook</h1>
                        <h5>These topics are covered in our Advanced Microsoft Outlook course.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h4>Archiving Manually</h4>
                <p>If you want to archive a folder immediately, you use the Archive dialog box, shown in Figure&nbsp;1-3, which you access from the File menu. If you want to archive your Contacts folder, you can only archive it manually.</p>
                <p><a href="/outlook-training.php"> Looking for Microsoft Outlook group training in Chicago and Los Angeles?</a></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/outlook2-figure1-3.jpg" width="450" height="" alt="Import Properties Dialogue Box"></p>

                <h4>Method</h4>
                <p>To archive manually:</p>
                <ol>
                    <li>From the Folder tab, choose AutoArchive Settings button or
                        Right Click on the desired folder and choose Properties. Then choose the AutoArchive tab in the Properties window.</li>
                    <li> The Archive dialog box will appear. In the Archive dialog box, select the <em>Archive this folder and all subfolders:</em> option button.</li>
                    <li> From the <em>Archive items older than:</em> drop-down Date Navigator, select a date.</li>
                    <li>Choose OK.</li>
                </ol>
                <p><a href="/outlook-training-chicago.php">Outlook training classes in Chicago</a>.</p>
                <h4>Exercise</h4>
                <p>In the following exercise, you will archive manually.</p>
                <ol>
                    <li>Right click on the Tasks folder and choose Properties, then Autoarchive. <em>[The Archive dialog box appears.]</em></li>
                    <li>Make sure the <em>Archive this folder and all subfolders:</em> option button is selected.</li>
                    <li>From the <em>Archive items older than:</em> drop-down Date Navigator, select today’s date two months ago.</li>
                    <li>In the Archive file text box, make sure <strong>C:\My Documents\archive.pst</strong> appears.</li>
                    <li>Choose OK. <em>[The items in the Tasks folder older than two months are archived.]</em></li>
                    <li>If a message box appears that says, <em>AutoArchive is ready to archive now. Do you want to have old items archived now?</em> choose No.</li>
                </ol>

                <h4>Retrieving Archived Items</h4>
                <p>Once the aging period has been reached, Outlook sends you a message at startup that asks if you want it to AutoArchive your files. Your Yes response activates archiving, and Outlook stores your old files for you.</p>
                <p>If you need an old file, you can retrieve&nbsp;it from the archives using the Import and Export Wizard. Outlook stores your archives in your My Documents folder as <strong>archive.pst</strong>. As with other Microsoft wizards, this wizard walks you step-by-step through the importing process. <a href="/outlook-training-los-angeles.php">MS Outlook training in Los Angeles</a>.</p>
                <p>Before you complete the final step in the Import and Export Wizard, Outlook lets you select whether to replace duplicates with imported files, to allow duplicates to be created, or not to import duplicates. You make your selection with the option buttons shown in Figure&nbsp;1-4. If you indeed have duplicates, you might find it helpful to allow duplicates to be created so that you can examine both items to decide which one you want to keep. Then you can delete the unwanted item. You might also decide that any duplicate in your active file is more current than what you have stored in your archives, in which case you would not want to import duplicates at all. If you are indeed searching for an older version of an active item, then you might prefer to replace duplicates with imported files.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/outlook2-figure1-4.jpg" width="450" height="" alt="Import MS Outlook Data File"></p>

                <h4>Method</h4>
                <p>In the following exercise, you will retrieve archived items.</p>
                <ol>
                    <li>From the file menu, choose Open and Export, choose Import/Export.</li>
                    <li> In the first screen of the Import and Export Wizard, choose <em>Import from another program or file:</em> </li>
                    <li> Choose Next.</li>
                    <li>Then choose from <em>Select file type to</em> <em>Import from: Outlook Data File (.pst).</em></li>
                    <li> Choose Next.</li>
                    <li> In the File to import area, select the correct path and folder by pressing Browse and navigating to the Outlook data file.</li>
                    <li>In the Options area, select a duplicate import option depending on user preference.</li>
                    <li> Choose Next.</li>
                    <li> In the next Import Personal Folders screen, accept the selections, and then choose Finish.</li>
                </ol>
                <h4>Exercise</h4>
                <p>In the following exercise, you will archive manually.</p>
                <ol>
                    <li>Make sure the Tasks folder is open and sorted by due date.</li>
                    <li>From the File menu, Open &amp; Export, choose Import. <em>[The Import and Export Wizard appears.]</em></li>
                    <li>In the <em>Choose an action to perform:</em> area, select <em>Import from another program or file (each of these areas give a <strong>Description</strong> at the bottom of wizard dialogue box as to what this action will do and in this case this action also includes the import of Outlook Data Files (.PST))</em></li>
                    <li>Choose Next. <em>[The first Import a File screen of the Import and Export Wizard appears].</em></li>
                    <li>From the <em>file type to import from: </em>Select <em>Outlook Data File (.pst)</em></li>
                    <li>Choose Next. </li>
                    <li>The Import Personal Folders screen will appear.</li>
                    <li>In the File to import area, make sure <strong>C:\My&nbsp;Documents\archive.pst</strong> is selected.</li>
                    <li>In the Options area, select the <em>Allow duplicates to be created </em>option button.</li>
                    <li>Choose Next. <em>[The second Import Personal Folders screen appears].</em></li>
                    <li>Accept the selections, and then choose Finish. <em>[The archives are retrieved and reappear in your task list.</em>] </li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-outlook" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Related Microsoft Outlook Lessons</h4>
                    <ul>
                        <li><a href="/outlook/lessons/auto-archiving.php">Auto Archiving</a></li>
                        <li><a href="/outlook/lessons/categories.php">Creating and Applying Categories</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Outlook courses</h4>
                    <ul>
                        <li><a href="/outlook/introduction.php">Outlook Level 1 - Introduction</a></li>
                        <li><a href="/outlook/advanced.php">Outlook Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>