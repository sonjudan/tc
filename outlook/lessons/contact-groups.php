<?php
$meta_title = "Setting up a Contact Group in Microsoft Outlook | Training Connection";
$meta_description = "Learn how to create contact groups and send emails and invitations to group. This topic and others are taught in our Level 2 Outlook Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/outlook.php">Outlook</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Groups</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Outlook">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Contact Groups in Outlook</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Outlook has some great tools for working with contacts and contact groups. In the this article we will show you how to create a contact group and manage its members, as well as how to send an email or meeting invitation to the group and how to forward the contact group.</p>
                <p>These topics are covered in our Advanced Microsoft Outlook class.</p>
                <p>Looking for group training? We run private instructor-led <a href="/outlook-training.php">Outlook group classes</a>. These classes will help you master Outlook and improve your productivity. </p>
                <p>Call us on 888.815.0604 and we'll talk you through a few options that best suits your training requirements.</p>

                <h4>Creating a Contact Group</h4>
                <p>To create a new contact group, use the following procedure.</p>
                <ol>
                    <li>
                        <p>In Contacts view in Outlook, select <strong>New Contact Group</strong> from the <strong>Home</strong> tab on the Ribbon.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/new-contact-group.jpg" width="717" height="404" alt="Creating a new Contact Group"></p>
                    </li>
                    <li>
                        <p>In the <em>Contact</em> <em>Group</em> window that opens, enter a <strong>Name</strong> for the group to help you remember its purpose. Also see <a href="https://kb.iu.edu/d/aoqw">advantages and disadvantages of the different methods of organization my Contacts</a>.</p>
                        <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/Outlook/contact-group.jpg" width="534" height="360" alt="Outlook contact groups screen"></p>
                    </li>
                    <li>You can enter notes about the group by selecting <strong>Notes</strong> from the Contact Group tab on the Ribbon.</li>
                    <li>Select <strong>Add</strong> <strong>Members</strong> to build the group membership. Select one of the following:
                        <ul>
                          <li>From Outlook Contacts</li>
                            <li>From Address Book</li>
                            <li>New Email Contact</li>
                        </ul>
                    <li>If you selected <strong>From Outlook Contacts</strong> or <strong>From</strong> <strong>Address</strong> <strong>Book</strong>, choose the name(s) from the <em>Select Members</em> dialog box.
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/select-members.jpg" width="489" height="345" alt="Select group memebers"></p>
                        <p>If you selected <strong>New</strong> <strong>Email</strong> <strong>Contact</strong>, enter the information in the <em>Add New Member </em>dialog box. Select <strong>OK</strong>. <a href="/outlook-training-chicago.php">MS Outlook classes in Chicago</a>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/add-members.jpg" width="321" height="175" alt="Add new member dialog box"></p>
                    </li>
                    <li>When you have finished adding members, select <strong>Save &amp; Close </strong>to save your contact group information.</li>
                </ol>

                <h4>Managing Contact Group Members</h4>
                <p>To add members to the contact group, use the following procedure.</p>
                <ol>
                    <li>
                        <p>Open the Contact Group by double-clicking on it in the Contacts view in Outlook.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/open-contact-group.jpg" width="714" height="205" alt="Edting a Contact Group"></p>
                    </li>
                    <li>
                        <p>Select <strong>Add</strong> <strong>Members</strong>. Select one of the following:</p>
                        <ul>
                            <li>From Outlook Contacts</li>
                            <li>From Address Book</li>
                            <li>New Email Contact</li>
                        </ul>
                    </li>
                    <li>If you selected <strong>From Outlook Contacts</strong> or <strong>From</strong> <strong>Address</strong> <strong>Book</strong>, choose the name(s) from the <em>Select Members</em> dialog box.
                        <p>If you selected <strong>New</strong> <strong>Email</strong> <strong>Contact</strong>, enter the information in the <em>Add New Member </em>dialog box. Select <strong>OK</strong>.</p>
                    </li>
                    <li>When you have finished adding members, select <strong>Save &amp; Close </strong>to save your contact group information. <a href="/outlook-training-los-angeles.php">LA-based Microosft Outlook course</a>.</li>
                </ol>

                <p>To remove members, use the following procedure.</p>
                <ul>
                    <li>Open the Contact Group by double-clicking on it in the Contacts view in Outlook.</li>
                    <li>Highlight the contact you want to delete in the list.
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/remove-member.jpg" width="534" height="360" alt="Remove Member"> </p>
                    </li>
                    <li>Select <strong>Remove</strong> <strong>Member</strong>. The member is immediately removed and there is no undo.</li>
                    <li>When you have finished managing the group, select <strong>Save &amp; Close.</strong></li>
                </ul>

                <h4>Sending an Email or a Meeting Invitation to a Contact Group</h4>
                <p>To send an email to a contact group, use the following procedure.</p>
                <ol>
                    <li>Open the Contact Group by double-clicking on it in the Contacts view in Outlook.</li>
                    <li>
                        <p>Select <strong>Email</strong> from the <strong>Contact</strong> <strong>Group</strong> tab on the Ribbon.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/email-contact-group.jpg" width="536" height="362" alt="Email an email to your contact group"></p>
                    </li>
                    <li>
                        <p>Outlook opens a new email with the contact group already added in the <strong>To</strong> field.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/email-to-field.jpg" width="566" height="340" alt="Add contact group to the To field"></p>
                    </li>
                </ol>


                <p>Note that you can also simply type in the group name when addressing a new, reply to or forward email.</p>
                <p>To send a meeting invitation to a contact group, use the following procedure.</p>

                <ol>

                    <li>Open the Contact Group by double-clicking on it in the Contacts view in Outlook.</li>
                    <li>
                        <p>Select <strong>Meeting</strong> from the <strong>Contact</strong> <strong>Group</strong> tab on the Ribbon.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/meeting-invitation.jpg" width="536" height="362" alt="Sending a meeting invitation to a group"></p>
                    </li>
                    <li>Outlook opens a new meeting invitation with the To field already completed with the group.</li>
                </ol>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/new-meeting-invitation.jpg" width="457" height="298" alt="New Meeting invitation"></p>
                <p><strong>Note</strong> that you can also simply type in the group name when creating a meeting invitation.</p>
                <p>Also read <a href="http://www.labnol.org/internet/gmail-vs-outlook/24531/">10 reasons to love the new Outlook.com</a>.</p>

                <h4>Forwarding a Contact Group</h4>
                <p>To forward a contact group, use the following procedure.</p>

                <ol>
                    <li>Open the Contact Group by double-clicking on it in the Contacts view in Outlook.</li>
                    <li>
                        <p>From the <strong>Contact Group</strong> tab on the Ribbon, select <strong>Forward</strong>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/forward-contact-group.jpg" width="531" height="250" alt=""></p>
                    </li>
                    <li>
                        <p>Select one of the following:</p>
                        <ul>
                            <li>In Internet Format - to forward the information to someone else as a vcard.</li>
                            <li>As an Outlook Contact - to forward the information to someone else in Outlook format.</li>
                        </ul>
                    </li>
                    <li>Outlook opens a new email message with the contact group information attached in the format you selected.</li>
                </ol>

                <p>Also see <a href="/outlook/lessons/signature.php">Adding a Signature in Outlook</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-outlook" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Outlook courses</h4>
                    <ul>
                        <li><a href="/outlook/introduction.php">Outlook Level 1 - Introduction</a></li>
                        <li><a href="/outlook/advanced.php">Outlook Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>