<?php
$meta_title = "Auto-archiving items in Outlook | Training Connection";
$meta_description = "Learn how to manage items in Outlook and setup AutoArchiving. This module is part of our Level 2 Outlook Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/outlook.php">Outlook</a></li>
                    <li class="breadcrumb-item active" aria-current="page">AutoArchiving</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Outlook">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Auto Archiving in Outlook</h1>
                        <h5>These  topics are covered  in  our  Microsoft Outlook Advanced course.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h4>Managing Items</h4>
                <p>When your Outlook items become out of date, you can store them or delete them. You can <em>archive</em> them automatically after a specified time interval or archive them manually. When you archive your items, you store them in your My Documents folder on your hard drive. You can retrieve archived items at any time.</p>
                <p>Another way to make item management easier for yourself is to specify <em>categories</em> for your items. A category is a keyword or a phrase you add to an item to help identify it. Designating categories helps you find and group your items. In addition, by creating your own filters, you can show and hide items according to your own criteria.</p>
                <p><a href="/outlook-training.php"> Looking for group Outlook training in Chicago and Los Angeles?</a></p>

                <h4>Setting Up AutoArchiving</h4>
                <p>In no time, you can collect a tremendous amount of Outlook data, especially e-mail messages that can crowd your folders and make it difficult to find wanted items. You can manually transfer individual files to a storage area, but it is more efficient to let Outlook archiveolder data on a regular basis and store the data for you in your My Documents folder. </p>
                <p>Setting up AutoArchiving is a two-step process: First you set the intervals in the Options dialog box, shown in <strong>Figure&nbsp;1-1</strong>. Then you set the options in the Properties dialog box for each folder. You set the options in each folder's Properties dialog box except for the Contacts folder, which cannot be AutoArchived. <a href="/outlook-training-chicago.php">Outlook training classes in Chicago Loop.</a></p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/outlook2-figure1-1.jpg" width="500" height="" alt="Outlook Options"></p>
                <p>The default <em>aging period</em> for Calendar, Tasks, and Journal is six months; for Sent Items and Deleted Items, it is two months. By default, Outlook archives your data every two weeks. This means that every two weeks Outlook checks your data files to see if any are older than their aging period. When Outlook finds files that fit this profile, it asks you at start up whether to archive them. You can change the aging period for each folder on the AutoArchive page of a folder’s Properties dialog box.</p>
                <p>If you want to AutoArchive the data in your Inbox and Notes folders, you must turn on the AutoArchive feature for these folders yourself. You turn on the AutoArchive feature for these folders using the AutoArchive page of the Properties dialog box for the folder you want to archive. Figure&nbsp;1-2 shows the AutoArchive page of the dialog box. <a href="/outlook-training-los-angeles.php">Los Angeles Outlook training workshops</a>.</p>
                <p>After you set up AutoArchiving, the feature automatically activates whenever you start Outlook. If the aging period for a folder has expired, Outlook displays a message box at startup that asks, <em>Do you want to AutoArchive now? </em>You then choose whether to complete the archiving process. If you do not choose to archive at the time of the notice, Outlook does not notify you about archiving again until the AutoArchive period for the next folder has expired.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/outlook2-figure1-2.jpg" width="500" height="" alt="AutoArchive"></p>

                <h4>Method</h4>
                <ol>
                    <li> From the File menu, choose Options.</li>
                    <li> In the Options dialog box, select the Advanced button.</li>
                    <li> On the AutoArchive section, select the <em>AutoArchive Settings </em>button.</li>
                    <li> The AutoArchive dialog box will appear.</li>
                    <li> Place a tick in the <em>Run AutoArchive</em> <em>every</em> check box.</li>
                    <li> In the <em>AutoArchive every ... day(s) at startup</em> spin box, enter the number of days.</li>
                    <li> Choose OK. And OK again.</li>
                    <li> On the Navigation Bar, right-click an Outlook folder in Mail&nbsp;and select Properties The Properties dialog box for that folder will appear.</li>
                    <li>In the Properties dialog box, select the AutoArchive tab.</li>
                    <li> On the AutoArchive page, if necessary, select the Archive this folder using these settings.</li>
                    <li> In the <em>Clean out items older than</em> spin box, enter the number of units, and from the drop-down list, select the calendar period.</li>
                    <li> If necessary, select Move old items to: choose Browse, and in the Find Personal Folders dialog box, select the drive and path for the archive file. Choose OK </li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will set up AutoArchiving.</p>
                <ol>
                    <li>Start Outlook and log on. <em>[Reminders might appear.]</em></li>
                    <li>If necessary, dismiss all reminders.</li>
                    <li>Open the Tasks folder. <em> [The task list appears in the Task Navigation Pane on the left hand side of your screen.]</em></li>
                    <li>Make sure the Current View is Simple List.</li>
                    <li>Sort the list by Due Date, displaying the oldest task first.</li>
                    <li>From the File menu, choose Options. <em>[The Options dialog box appears.]</em></li>
                    <li>Select the Advanced button. <em>[The AutoArchive option appears here.]</em></li>
                    <li>Make sure in the <em>AutoArchive every</em> check box is selected.</li>
                    <li>In the <em>AutoArchive every ... day(s) at startup</em> spin box, enter <strong>7</strong></li>
                    <li>Make sure the <em>Default archive file:</em> area displays the file path
                        <strong>C:\My Documents\archive.pst</strong></li>
                    <li>Choose OK and OK again.</li>
                    <li>On the Outlook Bar, right-click the Journal shortcut. <em>[The shortcut menu appears.]</em></li>
                    <li>Choose Properties. <em>[The Journal Properties dialog box appears.]</em></li>
                    <li>Select the AutoArchive tab. <em>[The AutoArchive page appears.]</em></li>
                    <li>In the <em>Clean out items older than</em> spin box, enter <strong>2</strong>, and in the drop-down list box, make sure Months is selected.</li>
                    <li>Select Move old items to: option and Choose Browse.</li>
                    <li>In the Find Personal Folders dialog box, select <strong>C:\My Documents\archive.pst</strong></li>
                    <li>Choose OK. <em>[The data in the Journal older than two months is set to be archived the next time you open Outlook].</em></li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-outlook" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Related Microsoft Outlook Lessons</h4>
                    <ul>
                        <li><a href="/outlook/lessons/manually-archiving.php">Manually Archiving</a></li>
                        <li><a href="/outlook/lessons/manually-archiving.php#retrieving-archived-items">Retrieving Archived Items</a></li>
                        <li><a href="/outlook/lessons/categories.php">Creating and Applying Categories</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Outlook courses</h4>
                    <ul>
                        <li><a href="/outlook/introduction.php">Outlook Level 1 - Introduction</a></li>
                        <li><a href="/outlook/advanced.php">Outlook Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>