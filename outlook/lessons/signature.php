<?php
$meta_title = "Adding an Email signature in Outlook | Training Connection";
$meta_description = "Learn how to managing signature sin Mircosoft Outlook. This topic and others are taught in our Level 2 Outlook Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/outlook.php">Outlook</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Email Signature</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Outlook">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding a Email Signature in Outlook</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Outlook includes several ways to automatically manage different tasks. This article explains how to control your signatures. You can have a different signature for different email accounts, as well as different signatures for both new messages and replies/forwards. </p>
                <p>These topics are covered in our Outlook advanced class.</p>
                <p>Did you know, we are an industry leading software training company and we offer <a href="/outlook-training.php">instructor-led Outlook training</a> at client sites in Chicago and Los Angeles. For a friendly, fun and informative day out why not give us a call on 888.815.0604</p>

                <h4>Managing Signatures</h4>
                <p>To manage a signature, use the following procedure.</p>

                <ol>
                    <li>In a new message, select <strong>Signatures</strong> from the Message tab on the Ribbon. (You can also access Signatures from the Mail tab on the Outlook Options dialog box).</li>
                    <li>Select <strong>Signatures</strong> to open the <em>Signatures and Stationery </em>dialog box.
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/signatures.jpg" width="410" height="182" alt="Select Signature in Message Tab"></p>
                    </li>
                    <li>In the <em>Signatures and Stationery </em>dialog box, select the signature that you want to edit.</li>
                  <li>With the signature name selected, you can change the contents or formatting of the signature. \</li>
                    <li>ou can select <strong>Rename</strong> to change the signature name. In the Rename Signature dialog box, type, and new name and select <strong>OK</strong>.
                        <p>Also see <a href="https://en.wikipedia.org/wiki/Microsoft_Outlook">what is Microsoft Outlook?</a></p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/rename-signature.jpg" width="178" height="90" alt="remane signature"></p>
                  </li>
                    <li>
                        <p>You can select <strong>Delete</strong> to remove the signature. Outlook displays a warning message. Select <strong>Yes</strong> to continue. <a href="/outlook-training-los-angeles.php">Onsite Outlook training in Los Angeles</a>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/signature-warning.jpg" width="556" height="110" alt="Delete signature warning message"></p>
                    </li>
                    <li>
                        <p>If you have multiple email accounts, select the email account for which you want to add an automatic signature. <a href="/outlook-training-chicago.php">Private Outlook training in Chicago</a>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/automatic-signature.jpg" width="426" height="288" alt="Automatic signatures in Outlook"></p>
                    </li>
                    <li>Select the signature that you want to use on new messages from the <strong>New</strong> <strong>message</strong> drop down list.</li>
                    <li>Select the signature that you want to use on reply to or forwarded messages from the <strong>Replies</strong>/<strong>Forwards</strong> drop down list.</li>
                    <li>Select <strong>OK</strong>.</li>
                </ol>
                <p>Also see <a href="/outlook/contacts.php">Working with Contacts in Outlook</a></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-outlook" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Outlook courses</h4>
                    <ul>
                        <li><a href="/outlook/introduction.php">Outlook Level 1 - Introduction</a></li>
                        <li><a href="/outlook/advanced.php">Outlook Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>