<?php
$meta_title = "Working with Contacts in Microsoft Outlook | Training Connection";
$meta_description = "Learn how to create business card contacts in MS Outlook and how to forward them to others. This topic and others are taught in our Level 2 Outlook Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-outlook">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/outlook.php">Outlook</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contacts</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-outlook.png" alt="Outlook">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Contacts in Outlook</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Outlook has some great tools for working with contacts. This article will show you how to customize a contact’s business card, which can really provide impact if you are including it in messages or sharing it in other ways. We’ll also learn how to forward a contact. </p>
                <p>These  topics are covered  in our  Advanced Microsoft Outlook course.</p>
                <p>Need to maximize you use of the tools available in Microsoft Outlook? Our private <a href="/outlook-training.php">Outlook training workshops</a> are a great way to master Outlook and improve your productivity.</p>
                <p>Call us on 888.815.0604 and we'll talk you through a few options that best suits your training requirements. <a href="/outlook-training-chicago.php">Chicago Outlook training classes</a>.</p>

                <h4>Modifying a Business Card</h4>
                <p>To edit a business card, use the following procedure.</p>
                <ol>
                    <li>Open the contact for whom you want to edit the business card.</li>
                    <li>Select <strong>Business</strong> <strong>Card</strong>.
                      <a href="/outlook-training-los-angeles.php">Microsoft Outlook classes in Los Angeles</a>.
<p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/business-card.jpg" width="485" height="359" alt="Business Card"></p>
                    </li>
                    <li>
                        <p>In the <em>Edit Business Card </em>dialog box, you can view the business card in the upper left corner. Download an <a href="http://web.cgu.edu/media/oit/tutorials/CustomGuide/outlook-2013-quick-reference.pdf">Outlook Cheat Sheet</a>.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/edit-business-card.jpg" width="294" height="259" alt="Editing a Business Card"></p>
                    </li>
                    <li>
                        <p>The upper right section allows you to change the <strong>Card</strong> <strong>Design</strong>.</p>
                        <ul>
                            <li>To include an image, select <strong>Change</strong>.</li>
                        </ul>

                        <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/card-picture.jpg" width="355" height="250" alt="Add a card Picture"></p>
                        <ul>
                            <li>In the Add Card Picture dialog box, navigate to the picture you want to use and select <strong>OK</strong>.</li>
                            <li>Select an option from the <strong>Layout</strong> drop down list to adjust the image position on the card. </li>
                            <li>Enter or select a percentage of space from the <strong>Image</strong> <strong>area</strong> field to adjust the image size.</li>
                            <li>Select an option from the <strong>Image Align</strong> drop down list to further adjust the image position on the card, in relation to the layout.</li>
                            <li>Select <strong>Background</strong> to choose a color for the business card.<br>
                            </li>
                        </ul>
                    </li>
                  <li>The bottom section allows you to include additional information and change the formatting of each field.</li>

                    <ul>
                        <li>Highlight the <strong>Field</strong> <strong>Name</strong> on the left side. You can add or remove fields and rearrange the order of the fields using the buttons at the bottom of the list.</li>
                        <li>Edit the information for that field, if desired.</li>
                        <li>Use the text size, enhancements, alignment, and color tools to adjust the text for that field.</li>
                    </ul>
                    <p><a href="/outlook-training-chicago.php">Chicago-based Outlook training classes</a>.</p>
                </ol>

                <h4><img src="https://www.trainingconnection.com/images/Lessons/Outlook/card-fields.jpg" width="294" height="259" alt="Editing Business Card Fields"> </h4>
                <ol start="6">
                    <li>Select <strong>OK</strong> when you have finished editing the business card. Or select <strong>Reset</strong> <strong>Card</strong> to remove any changes you have made.</li>
                </ol>
                <p>Download a <a href="https://products.office.com/en-us/outlook/email-and-calendar-software-microsoft-outlook">FREE trial of Microsoft Outlook</a>.</p>

                <h4>Forwarding a Contact</h4>
                <p>To forward a contact, use the following procedure.</p>
                <ol>
                    <li>Open the contact that you want to send to another person.</li>
                    <li>From the <strong>Contact</strong> tab on the Ribbon, select <strong>Forward</strong>.</li>
                    <li>
                        <p>Select one of the following:</p>
                        <ul>
                            <li>As a Business Card - to forward the information as a business card attachment in internet format.</li>
                            <li>In Internet Format - to forward the information to someone else as a vcard.</li>
                            <li>As an Outlook Contact - to forward the information to someone else in Outlook format.</li>
                            <li>As a Text Message - to forward the information to someone else in SMS (text message) format. This option is only available if your account has SMS enabled.</li>
                            <li>As a Multimedia Message - to forward the information to someone else in MMS format through a service provider. This option is only available if your account has this service.</li>
                        </ul>
                    </li>
                </ol>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Outlook/forwarding-business-card.jpg" width="410" height="304" alt=""></p>

                <ol start="4">
                    <li>Outlook opens a new email message with the contact information attached in the format you selected. </li>
                </ol>

                <p>Also see <a href="/outlook/lessons/contact-groups.php">Outlook Contact Groups</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-outlook" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Outlook courses</h4>
                    <ul>
                        <li><a href="/outlook/introduction.php">Outlook Level 1 - Introduction</a></li>
                        <li><a href="/outlook/advanced.php">Outlook Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>