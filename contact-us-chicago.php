<?php
$meta_title = "Contact Us | Training Connection | Chicago";
$meta_description = "Contact details for our Chicago Training Center";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>


    <main class="page-single-content">

        <div class="room-hire-map">

            <div class="embed-responsive embed-responsive-21by9 sm">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.5366596731305!2d-87.63866028391864!3d41.881314319573015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d7a36f4b096e618!2sTraining+Connection!5e0!3m2!1sen!2sph!4v1559752005915!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>


        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                </ol>
            </nav>

            <div class="page-intro room-hire-heading mt-0">
                <div class="heading-l4">
                    <h2>Chicago</h2>
                    <h4>Training Center</h4>
                </div>
                <ul class="room-hire-cdetails">
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        <a href="https://www.google.com/maps?ll=41.881001,-87.634898&z=16&t=m&hl=en&gl=PH&mapclient=embed&cid=5582835112641750552" target="_blank">
                            230 W Monroe Street, Suite 610, Chicago, IL, 60606
                        </a>
                    </li>
                    <li><i class="fas fa-phone"></i>
                        <a href="tel:3126984475"><span class="link-secondary">(312) 698-4475</span></a>
                    </li>
                    <li><i class="fas fa-fax"></i> (866) 523-2138</li>
                    <li><i class="fas fa-envelope-square"></i>
                        <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                              Show Email
                        </a>
                    </li>
                </ul>
            </div>



            <div class="room-hire-body copy-sm">
                <div class="room-hire-col">

                    <div class="rh-block">
                        <h4><i class="fas fa-directions"></i> Directions</h4>
                        <p>We are located on the corner of W Monroe and Franklin Street in Downtown Chicago. [The building's main entrance is in Franklin Street.]</p>
                    </div>

                    <div class="rh-block">
                        <h4><i class="fas fa-car"></i> By Car</h4>

                        <div class="card-l2">
                            <h5>From the North</h5>
                            <p>Take I-90/94 South (Kennedy Expressway) to Monroe East exit.</p>
                        </div>
                        <div class="card-l2">
                            <h5>From the South</h5>
                            <p>Take I-90/94 North to Monroe East exit.</p>
                        </div>
                        <div class="card-l2">
                            <h5>From the West</h5>
                            <p>Take I-290 East (Eisenhower Expressway) and exit at Wacker/Franklin. </p>
                        </div>
                    </div>

                    <div class="rh-block copy">
                        <h4><i class="fas fa-parking"></i> Parking</h4>
                        <p>There are several public parking bays located within easy walking distance of our training facility. These can be located at:</p>
                        <ul>
                            <li>Corner of Wells and Monroe.</li>
                            <li>Corner of Wells and Madison.</li>
                        </ul>
                    </div>
                </div>


                <div class="room-hire-col">
                    <div class="rh-block">
                        <h4><i class="fas fa-subway"></i> Trains</h4>

                        <div class="post-list-sm">
                            <article class="post-inline-sm">
                                <div class="post-img">
                                    <img src="/dist/images/placeholders/post-1.jpg" alt="CTA - Blue and Red Lines - Monroe Station">
                                </div>
                                <div class="post-body">
                                    <h4>CTA - Blue and Red Lines - Monroe Station</h4>
                                    <p>Walk approximately 4 blocks west on Monroe Street to Franklin Street.</p>
                                </div>
                            </article>

                            <article class="post-inline-sm">
                                <div class="post-img">
                                    <img src="/dist/images/placeholders/post-2.jpg" alt="CTA - Orange, Pink, Purple and Brown Lines - Quincy station">
                                </div>
                                <div class="post-body">
                                    <h4>CTA - Orange, Pink, Purple and Brown Lines - Quincy station</h4>
                                    <p>Walk approximately 1.5 blocks north on Wells Street, turn left (west) on Monroe Street, and go to corner of Franklin Street.</p>
                                </div>
                            </article>

                            <article class="post-inline-sm">
                                <div class="post-img">
                                    <img src="/dist/images/placeholders/post-1.jpg" alt="CTA - Green Line">
                                </div>
                                <div class="post-body">
                                    <h4>CTA - Green Line</h4>
                                    <p>Swap at Clark for Blue line or Roosevelt for Orange line.</p>
                                </div>
                            </article>

                            <article class="post-inline-sm">
                                <div class="post-img">
                                    <img src="/dist/images/placeholders/post-3.jpg" alt="Metra - Union Station">
                                </div>
                                <div class="post-body">
                                    <h4>Metra - Union Station</h4>
                                    <p>Walk 3 blocks east on West Adams (crossing river). Turn left (North) on Franklin and walk 1 block to Monroe Street.</p>
                                </div>
                            </article>

                        </div>
                    </div>
                </div>

            </div>



            <div class="section-footer copy-sm mt-5">
                <p><i class="fas fa-wheelchair"></i> This center is wheel  chair friendly. For more details please call us.</p>
            </div>
        </div>

    </main>

    <div class="mt-5 pt-2"></div>




<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>