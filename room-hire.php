<?php
$meta_title = "Computer Lab Rentals | Chicago and Los Angeles";
$meta_description = "We have some of the nicest computer training labs available for rental in Chicago and Los Angeles. Reasonable daily rates.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content section-content-l3">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Computer Room Rental</li>
            </ol>
        </nav>


        <div class="page-intro">
            <div class=" intro-copy">
                <h1 data-aos="fade-up" >Room Rental</h1>
                <h5 class="" data-aos="fade-up" data-aos-delay="100">Rent a computer lab for $600/day.</h5>
            </div>
        </div>

        <div class="copy pb-5 pt-0" data-aos="fade-up">

            <h5>Features</h5>

            <ul>
                <li>Chicago or LA locations</li>
                <li>Friendly and efficient sign-in process.</li>
                <li>Very modern spacious training facilities. (A grade buildings)</li>
                <li>Broadband internet</li>
                <li>Intel Core Duo Computers with 20" monitors.</li>
                <li>Data Projectors and white boards.</li>
                <li>Student Break out rooms with filtered water, premium coffee, tea and hot chocolate.</li>
                <li>Nearby restaurants for students during lunch.</li>
                <li>Close to several train stations and parking bays</li>
                <li>Max room size - 12 students</li>
            </ul>


            <div class="action-group">
                <a href="#modalRoomChicago" class="btn btn-dark btn-lg" data-toggle="modal"><i class="fas fa-map-marked-alt mr-1"></i> Chicago Map</a>
                <a href=".modal-room-hire-la" class="btn btn-secondary btn-lg" data-toggle="modal"><i class="fas fa-map-marked-alt mr-1"></i> Los Angeles Map</a>
            </div>

        </div>

    </div>

</main>


<div class="section-hero-contact">
    <div class="container">
        <h5>Contact</h5>
        <p>Please call us on <a href="tel:8888150604" class="link-secondary">(888) 815-0604</a> to make a reservation.</p>
    </div>
</div>

<!--MODAL - Chicago: ROOM HIRE-->
<div class="modal fade modal-room-hire-chicago" id="modalRoomChicago" tabindex="-1" role="dialog" aria-labelledby="modalRoomChicago" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>


            <div class="secion-room-hire">
                <div class="room-hire-map">
                    <div class="embed-responsive embed-responsive-21by9">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.5366596731305!2d-87.63866028391864!3d41.881314319573015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d7a36f4b096e618!2sTraining+Connection!5e0!3m2!1sen!2sph!4v1559752005915!5m2!1sen!2sph" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>


                <div class="room-hire-content">

                    <div class="room-hire-heading">
                        <div class="heading-l4">
                            <h2>Chicago</h2>
                            <h4>Training Center</h4>
                        </div>
                        <ul class="room-hire-cdetails">
                            <li>
                                <i class="fas fa-map-marker-alt"></i>
                                <a href="https://www.google.com/maps?ll=41.881001,-87.634898&z=16&t=m&hl=en&gl=PH&mapclient=embed&cid=5582835112641750552" target="_blank">
                                    230 W Monroe Street, Suite 610, Chicago, IL, 60606
                                </a>
                            </li>
                            <li><i class="fas fa-phone"></i>
                                <a href="tel:3126984475"><span class="link-secondary">(312) 698-4475</span></a>
                            </li>
                            <li><i class="fas fa-fax"></i> (866) 523-2138</li>
                            <li><i class="fas fa-envelope-square"></i>
                                <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                                    Show Email
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="room-hire-body">
                        <div class="room-hire-col">

                            <div class="rh-block">
                                <h4><i class="fas fa-directions"></i> Directions</h4>
                                <p>We are located on the corner of W Monroe and Franklin Street in Downtown Chicago. [The building's main entrance is in Franklin Street.]</p>
                            </div>

                            <div class="rh-block">
                                <h4><i class="fas fa-car"></i> By Car</h4>

                                <div class="card-l2">
                                    <h5>From the North</h5>
                                    <p>Take I-90/94 South (Kennedy Expressway) to Monroe East exit.</p>
                                </div>
                                <div class="card-l2">
                                    <h5>From the South</h5>
                                    <p>Take I-90/94 North to Monroe East exit.</p>
                                </div>
                                <div class="card-l2">
                                    <h5>From the West</h5>
                                    <p>Take I-290 East (Eisenhower Expressway) and exit at Wacker/Franklin. </p>
                                </div>
                            </div>

                            <div class="rh-block copy">
                                <h4><i class="fas fa-parking"></i> Parking</h4>
                                <p>There are several public parking bays located within easy walking distance of our training facility. These can be located at:</p>
                                <ul>
                                    <li>Corner of Wells and Monroe.</li>
                                    <li>Corner of Wells and Madison.</li>
                                </ul>
                            </div>
                        </div>


                        <div class="room-hire-col">
                            <div class="rh-block">
                                <h4><i class="fas fa-subway"></i> Trains</h4>

                                <div class="post-list-sm">
                                    <article class="post-inline-sm">
                                        <div class="post-img">
                                            <img src="/dist/images/placeholders/post-1.jpg" alt="CTA - Blue and Red Lines - Monroe Station">
                                        </div>
                                        <div class="post-body">
                                            <h4>CTA - Blue and Red Lines - Monroe Station</h4>
                                            <p>Walk approximately 4 blocks west on Monroe Street to Franklin Street.</p>
                                        </div>
                                    </article>

                                    <article class="post-inline-sm">
                                        <div class="post-img">
                                            <img src="/dist/images/placeholders/post-2.jpg" alt="CTA - Orange, Pink, Purple and Brown Lines - Quincy station">
                                        </div>
                                        <div class="post-body">
                                            <h4>CTA - Orange, Pink, Purple and Brown Lines - Quincy station</h4>
                                            <p>Walk approximately 1.5 blocks north on Wells Street, turn left (west) on Monroe Street, and go to corner of Franklin Street.</p>
                                        </div>
                                    </article>

                                    <article class="post-inline-sm">
                                        <div class="post-img">
                                            <img src="/dist/images/placeholders/post-1.jpg" alt="CTA - Green Line">
                                        </div>
                                        <div class="post-body">
                                            <h4>CTA - Green Line</h4>
                                            <p>Swap at Clark for Blue line or Roosevelt for Orange line.</p>
                                        </div>
                                    </article>

                                    <article class="post-inline-sm">
                                        <div class="post-img">
                                            <img src="/dist/images/placeholders/post-3.jpg" alt="Metra - Union Station">
                                        </div>
                                        <div class="post-body">
                                            <h4>Metra - Union Station</h4>
                                            <p>Walk 3 blocks east on West Adams (crossing river). Turn left (North) on Franklin and walk 1 block to Monroe Street.</p>
                                        </div>
                                    </article>

                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="section-footer">
                        <p><i class="fas fa-wheelchair"></i> This center is wheel  chair friendly. For more details please call us.</p>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<!--MODAL - LA: ROOM HIRE-->
<div class="modal fade modal-room-hire-la" id="modalRoomLA" tabindex="-1" role="dialog" aria-labelledby="modalRoomLA" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>


            <div class="secion-room-hire">
                <div class="room-hire-map">
                    <div class="embed-responsive embed-responsive-21by9">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13222.883976755706!2d-118.2600668!3d34.0510274!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5831c6d11713747a!2sTraining+Connection!5e0!3m2!1sen!2snl!4v1560364559799!5m2!1sen!2snl" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>


                <div class="room-hire-content">

                    <div class="room-hire-heading">
                        <div class="heading-l4">
                            <h2>Los Angeles</h2>
                            <h4>Training Center</h4>
                        </div>
                        <ul class="room-hire-cdetails">
                            <li><i class="fas fa-map-marker-alt"></i>
                                <a href="https://www.google.com/maps?ll=34.051027,-118.260067&z=14&t=m&hl=en&gl=NL&mapclient=embed&cid=6355079150534095994" target="_blank">
                                    915 Wilshire Blvd, Suite 1800, Los Angeles, CA 90017
                                </a>
                            </li>
                            <li><i class="fas fa-phone"></i>
                                <a href="tel:8888150604"><span class="link-secondary">(888) 815-0604</span></a>
                            </li>
                            <li><i class="fas fa-fax"></i> (866) 523-2138</li>
                            <li><i class="fas fa-envelope-square"></i>
                                <a href="#" class="hiddenMail" data-email="infoATtrainingconnectionDOTcom" target="_blank">
                                    Show Email
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="room-hire-body">
                        <div class="room-hire-col">

                            <div class="rh-block">
                                <h4><i class="fas fa-directions"></i> Directions</h4>
                                <p>The easiest access is from the 110 (Harbor) Freeway</p>

                                <div class="card-l2 lg">
                                    <p>
                                        If traveling North-bound on <span>110N Fwy</span><br>
                                        Take the <span>9th St exit</span><br>
                                        Turn <span>Left</span> at <span>Figueroa Street</span><br>
                                        Turn <span>Left</span> into <span>Wilshire Blvd</span>
                                    </p>
                                    <p>
                                        <span>915 Wilshire</span> is on the <span>Right</span> (look for the <span>Bank of the West</span> located in our lobby)
                                    </p>
                                </div>


                                <div class="card-l2 lg">
                                    <p>If traveling South-bound on <span>110N Fwy</span>
                                        Take the <span>4th St exit</span> towards <span>WILSHIRE blvd / 6TH St / 3RD St</span><br>
                                        Turn <span>left</span> at <span>Wilshire Blvd</span>
                                    </p>

                                    <p>
                                        <span>915 Wilshire</span> is on the <span>Left</span> (look for the <span>Bank of the West</span> located in our lobby)
                                    </p>

                                    <p>The entrance to our parking garage is at the light of Franciso St.</p>
                                </div>

                            </div>
                        </div>


                        <div class="room-hire-col">
                            <div class="rh-block">
                                <h4><i class="fas fa-subway"></i> Trains</h4>
                                
                                <div class="copy">
                                    <p><img src="/dist/images/hire-train/train.jpg" alt="Trains"></p>
                                    <p>We are easily accessable by train via the Metro Red, Purple, and Blue lines. The nearest train station is the 7th Street / Metro Center Station. Also available are the Metrolink commuter trains. Take Metrolink to Los Angeles Union Station and transfer for free to the Metro Red or Purple line.</p>
                                    <p>Visit LA Metro and Metrolink websites for schedule and fare information. Please view the Google Map above for directions from the 7th Street / Metro Center Station to our facility.</p>
                                    <p>The nearest train station is the 7th Street / Metro Center Station. Also available are the Metrolink commuter trains. Take Metrolink to Los Angeles Union Station and transfer for free to the Metro Red or Purple line.</p>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="rh-block copy">
                        <h4><i class="fas fa-parking"></i> Parking</h4>
                        <p>Parking is available at our location for $35/day.</p>

                        <h5 class="h5 mt-4">Alternative Parking</h5>

                        <div class="row row-sm">
                            <div class="col-lg-6 col-xl-4">
                                <div class="card-l2 lg">
                                    <p>
                                        <strong>616 S Figueroa</strong> <br>
                                        Big sign out front says 811 Wilshire Northeast corner of Figueroa and Wilshire <br>
                                        Indoor lot <br>
                                        Early Bird rate before 8:30am - $15, $30 after
                                        2 minute walk
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xl-4">
                                <div class="card-l2 lg">
                                    <p>
                                        <strong>1055 W 7th St</strong> <br>
                                        Entrance also at 690 Bixel St On the Northeast corner of 7th and Bixel <br>
                                        Indoor Lot <br>
                                        Early Bird rate before 9:00am - $10, $35 after
                                        4 minute walk
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="section-footer">
                        <p><i class="fas fa-wheelchair"></i> This center is wheel  chair friendly. For more details please call us.</p>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>