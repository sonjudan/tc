<?php
$meta_title = "Etiquette for Business Dining | Training Connection";
$meta_description = "Business dining rules and good etiquette. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Business Dining</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Business Dining Etiquette</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Conducting business over meals is a great way to build business relationships. Meals make for a more casual atmosphere compared to offices, and are therefore more conducive for a relaxed discussion. In this module, we would discuss some of the etiquette rules when dining with business associates such as understanding place setting, etiquette rules while eating, and ways to avoid sticky situations.</p>
                <p>For more details on <a href="/business-etiquette-training.php">business dining etiquette training</a> call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/dining-etiquette.jpg" width="750" height="500" alt="Dining Etiquette Rules"></p>
                <h4>Understanding Your Place Setting</h4>
                <p>Place setting is the arrangement of the drinking vessels (glasses, mugs), food receptacles (plates, bowls, and saucers), and utensils (spoons, forks, knives) that will be used during a meal. </p>
                <p>Place settings differ depending on the menu and the formality of the dining event. The more informal you intend your meal to be, the less rigidly you have to adhere to the rules of place setting. <a href="/business-etiquette-training-los-angeles.php">Etiquette workshops in Los Angeles</a>.</p><p>Here are a few basics to remember:</p>
                <ul>
                    <li>Solids on the left, liquids on the right.</li>
                </ul>
                <p>Plates are always placed on the left, while glasses are on the upper right. This guide can help you find which place settings are yours. </p>
                <ul>
                    <li>Forks are on the left, knives, and spoons on the right side of the plate.</li>
                </ul>
                <p>In general, forks are placed to the left of the plate with the exception of the oyster fork which is placed on the right. </p>
                <ul>
                    <li>Work your way inwards with the utensils. </li>
                </ul>
                <p>The rule for utensils is to work inward toward your plate as the meal progresses. Place settings are organized so that, with each new course presented, the guest can use the outermost utensil(s). For instance, the salad fork would be leftmost, before the dinner fork, as the salad comes before the main course. </p>
                <ul>
                    <li>Follow the ‘rule of three’s.’</li>
                </ul>
                <p>If you’re hosting the dinner, don’t clutter the table with too many implements. Set at most three of anything (e.g. three glasses, three forks etc.). If more than three would be used, then the additional implement would come as the new meal is presented.</p>
                <p>Also see <a href="/business-etiquette/meet-greet.php">How to Meet and Greet</a></p>

                <h4>Using Your Napkin</h4>
                <p> Here are some etiquette guidelines to using your napkin:</p>
                <ul>
                    <li>When everyone is seated, gently unfold your napkin and place it on your lap. If the napkin is large, fold the napkin in half first.</li>
                    <li>Your napkin remains on your lap throughout the entire meal. If you need to use your napkin to clean something on your lips, just dab it lightly. </li>
                    <li>If you leave the table during a dinner, place your napkin on your chair to signal to the server that you will be returning. </li>
                    <li>When you are finished dining, place your napkin neatly on the table to the left side of the plate. </li>
                    <li>If you drop your napkin on the floor, discreetly ask the waiter or host for another one.</li>
                </ul>

                <h4>Eating Your Meal</h4>
                <p> Basic etiquette guidelines when eating:</p>
                <ul>
                    <li>Don’t talk business during the meal proper, unless the senior members want to do so. Otherwise, business matters should be addressed either before the meal or after it. </li>
                    <li>Take a cue from the host, or the most senior in the table, where to sit yourself.</li>
                    <li>Take your cue from the host, or the most senior in the table, when to begin eating. </li>
                    <li>Keep elbows off the table while eating. Elbows on the table are acceptable in between meals. </li>
                    <li>Don’t talk with your mouth full. Chew quietly. Don’t slurp your liquids.</li>
                    <li>Don’t apply make-up or comb your hair while dining. </li>
                    <li>Don’t pick your teeth at the table. </li>
                    <li>If you need something that is not within your reach, politely ask the person next to you to pass it to you. Food is typically passed from left to right. </li>
                    <li>Try to pace yourself so that you can finish at the same time as everyone else. When you have finished eating, you can let others know that you have by placing your knife and folk together, with the tines on the fork facing upwards, on your plate. </li>
                </ul>
                <p>Don’t forget to thank your host for the meal! <a href="/business-etiquette-training-chicago.php">Chicago-based etiquette training classes</a>.</p>

                <h4>Sticky Situations and Possible Solutions</h4>
                <p>Here are some awkward dining situations and how to deal with them:</p>
                <ul>
                    <li>Having put something in your mouth that doesn’t agree with you: Ask the waiter for a paper napkin and discreetly spit the food out. Crumple the napkin and place it under the sides of your plate. Keep the food you had spit out away from the other’s view.</li>
                    <li>You accidentally spilt food or drinks on a guest: Don’t panic. Apologize sincerely first. Use the cloth napkin and water to gently wipe the spill. You may also guide the guest to the wash room.</li>
                    <li>A guest commits a faux pas: If you notice that a colleague or a subordinate is using the wrong utensil, the best way to let them know is by using the right one yourself. Don’t correct them, it would just cause embarrassment. </li>
                    <li>You’ve noticed a bug in your food: Discreetly send it out to the server. You don’t have to tell everyone as it might ruin their appetite.</li>
                    <li>You have dietary limitations: If you cannot eat a certain type of food or have some special needs, tell your host several days before the dinner party. This can help avoid awkward situations like not being able to eat what was served because of a health issue or religious conviction.</li>
                </ul>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>