<?php
$meta_title = "Meeting and Greeting with Clients | Training Connection";
$meta_description = "Learn how to make the best impression when you meet a client for the first time. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Meet and Greet</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Meeting and Greeting Clients</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>An introduction is almost always accompanied by a handshake and conversation. In this lesson, we would discuss the three steps that make an effective handshake and the four levels of conversation.</p>

                <p>For more details on our <a href="/business-etiquette-training.php">business etiquette training classes</a> call us on 888.815.0604.</p>

                <p class="imgCenter"><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/meeting-and-greeting.jpg" width="646" height="363" alt="Meeting and Greeting clients"></p>

                <h4>The Three-Step Process</h4>
                <p>A handshake is a part of many social interactions. It’s a way to introduce one’s self, offer congratulations and even a way to conclude a business deal. A handshake is a gesture of goodwill.</p>
                <p>The Three-Step Process to Handshake:</p>
                <ol>
                    <li><strong>Facial Expression: </strong>Start non-verbal's that show openness and sincerity. Maintain eye contact. Smile. </li>
                    <li>
                        <p><strong>Shake Hands: </strong>Your handshake gives an impression. If your grip is too lax, you send the message that you’re hesitant and possibly indecisive. If your grip is too tight, you might come across as too brash, even intimidating. Go for a grip that’s in between. It sends the message that you’re confident.</p>
                        <p>For most occasions, two or three pumps of the hand are appropriate. Longer handshakes can make some people feel uncomfortable. But there are people who do prefer longer handshakes. If uncertain, go with the flow, and follow the lead of the other person. If you feel that it’s time to let go, just relax your hand to signal the other person. </p>
                    </li>
                    <li><strong>Greet the Person: </strong>Talk to the person whose hand you are shaking. A simple "hello" or "how do you do" is appropriate. </li>
                </ol>

                <h4>The Four Levels of Conversation</h4>
                <p>The real art of conversation is not only to say the right thing at the right time, but to leave unsaid the wrong thing at a tempting moment. It requires sensitivity at this stage of a relationship, the context of the conversation and the comfort level of the person you are talking to depend on it. <a href="/business-etiquette-training-los-angeles.php">Los Angeles softkills training classes</a>.</p>
                <p>There are four levels of conversation based on the degree and amount of personal disclosure. They are:</p>
                <ol>
                    <li>
                        <p><strong>Small Talk:</strong> This is commonly referred to as the 'exchange of pleasantries' stage. In this level, you talk only about generic topics, subjects that almost everyone is comfortable discussing. These subjects include the weather, the location you're both in and current events. </p>
                        <p>The small talk stage establishes rapport; it makes a person feel at ease with you. It's also a safe and neutral avenue for people to subtly 'size up' one another, and explore if it's a conversation or relationship that they'd want to invest in. </p>
                        <p>If the small talk goes well, you can proceed into the next level: fact disclosure.</p>
                    </li>
                    <li><strong>Fact Disclosure:</strong> In this stage, you tell the other person some facts about you such as your job, your area of residence, and your interests. This is a 'getting-to-know' stage, and it aims to see if you have something in common with the other person. It's also a signal that you are opening up a little bit to the other person while still staying on neutral topics. If the fact disclosure stage goes well, you can proceed to sharing viewpoints and opinions.</li>
                    <li>
                        <p><strong>Viewpoints and Opinions:</strong> In this stage of the conversation, you can offer what you think about various topics like politics, the new business model or even the latest blockbuster. It helps then to read and be curious about many things, from politics to entertainment to current events.</p>
                        <p>Sharing viewpoints and opinions require the 'buffering effect' of the first two stages for two reasons: </p>
                        <p>First, a person needs rapport with another before they can discuss potentially contentious statements, even if they're having a healthy debate. </p>
                        <p>Second, sharing viewpoints and opinions opens a person to the scrutiny of another, and this requires that there is some level of safety and trust in a relationship.</p>
                        <p>The controversial, and therefore potentially offensive, nature of an opinion exists in a range; make sure that you remain within the 'safe' zone in the early stages of your relationship. <a href="/business-etiquette-training-los-angeles.php">Click here</a> for details on business etiquette classes in LA.</p>
                    </li>
                    <li>
                        <p><strong>Personal Feelings:</strong> The fourth stage is disclosure and acknowledgment of personal feelings. For instance you can share about your excitement for the new project, or your worry about your son's upcoming piano recital. </p>
                        <p>Depending on the context and the level of the friendship, you can disclose more personal subjects. This stage requires trust, rapport, and even a genuine friendship, because of the intimate nature of the subject. </p>

                    </li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Etiquette Lessons</h4>
                    <ul>
                        <li><a href="/business-etiquette/networking.php">Networking for Success</a></li>
                        <li><a href="/business-etiquette/business-cards.php">Business cards and Remembering names</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>