<?php
$meta_title = "When is it okay not to tip? | Training Connection";
$meta_description = "This article discusses the etiquette around tippping in the US. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tipping</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>When is it okay not to tip?</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>What constitutes bad service? A bad attitude? A delay? A spill? A stupid question? Unless a rat runs over your food and they refuse to get you another plate, to express your dissatisfaction by by not tipping at all says more about your lack of class and education than those you decided to punish by <a href="https://www.thrillist.com/eat/nation/why-you-should-stop-tipping-reasons-not-to-tip">not tipping</a>. In this article we will cover why, how and when not to tip.</p>
                <p>For more details on <a href="/business-etiquette-training.php">business etiquette and dining etiquette training</a> call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/tipping.jpg" width="780" height="390" alt="Tipping Etiquette"></p>
                <h4>Why do we tip?</h4>

                <ol>
                    <a href="https://en.wikipedia.org/wiki/Gratuity">Wikipedia</a> tell us the history of the synonym for tipping, "gratuity", dates back either to the 1520s, from "graciousness", from the French&nbsp;<em>gratuite </em>(14th century) or directly from Medieval Latin&nbsp;<em>gratuitas</em>, "free gift", probably from earlier Latin&nbsp;<em>gratuitus</em>, "free, freely given". The meaning "money given for favor or services" is first attested in the 1530s. What we know as tipping is also attributed to customs in the taverns of 17th Century England. T.I.P was short for “To Insure Promptitude.”
                </ol>

                <p>According to&nbsp;<a href="http://foodwoolf.com/2010/07/service-101-gratuity-included.html">Michael Lynn</a>, a professor at the Cornell University School of Hotel Administration, tipping became popular by wealthy Americans traveling abroad to Europe who brought the aristocratic custom back with them to “show off,” or prove their elevated education and class.</p>

                <p>In 1916, William Scott, who wrote “The Itching Palm,” opposed the policy for paying service twice (once for the employer and once for the employee) because it created “a servile attitude for a fee.” He further posited, “If <a href="http://www.foodwoolf.com/2010/08/history-of-tipping.html">tipping</a> is un-American, someday, somehow, it will be uprooted like African slavery.” Here is something from his book:</p>

                <p>In the American democracy to be servile is incompatible with citizenship. Every tip given in the United States is a blow at our experiment in democracy. The custom announces to the world…that we do not believe practically that “all men are created equal.” Unless a waiter can be a gentleman, democracy is a failure. If any form of service is menial, democracy is a failure. Those Americans who dislike self-respect in servants are undesirable citizens; they belong in an aristocracy.</p>
                <p><a href="/business-etiquette-training-los-angeles.php">Looking to an Etiquette class in Los Angeles</a>?</p>
                <p>According to the&nbsp;<a href="https://en.wikipedia.org/wiki/Fair_Labor_Standards_Act" title="Fair Labor Standards Act">Fair Labor Standards Act</a>&nbsp;tippable employees are individuals who customarily and regularly receive tips of $30 or more per month. Federal law permits employers to include tips towards satisfying the difference between employees' hourly wage and minimum wage, although some states and territories provide more generous provisions for tipped employees. </p>

                <p>The&nbsp;<a href="https://en.wikipedia.org/wiki/National_Restaurant_Association" title="National Restaurant Association">National Restaurant Association</a> shows only a handful of restaurants in the United States have adopted a no-tripping model and some restaurants who have adopted this model returned to tipping due to loss of employees to competitors.</p>
                <p>Clearly, the reasons we have it are contentious because on one hand, it can incentivize employee performance but on the other hand, it can just be a method of cutting overhead for employers at the expense of employee well-being. Either way, these are the terms so call your Senator if you don’t like it but instead of taking it out on a tip based employee, learn how to tip and do the right thing!</p>

                <p><a href="http://emilypost.com/advice/general-tipping-guide/">Here are guidelines for tipping in the US. </a></p>

                <h4>When is it okay not to Tip? </h4>
                <p>Given the expectation in the system I just described, your tip is not required but it’s minimum is figured into the well-being of an employee in a tip-based position so consider these 5 factors:</p>
                <p>First, punishing your server’s well-being by withholding any tip does not guarantee improved future service. <a href="/business-etiquette-training-chicago.php">Chicago dinning and business etiquette training</a>.</p>
                <p>Depending on the state and the restaurant, you will find the norm for pay is different and some businesses train employees and some allow them to be weeded out by performance alone. For example, in Illinois, tip based workers can make as little as 2-7$ an hour whereas California laws require at least minimum wage plus tips though the cost of living is 2 times higher between LA and Chicago. When you decide to underpay a person based on your mood or expectations, you place the responsibility for correction completely on the server and at the expense of their responsibilities which could be children! Again, it’s a voluntary consideration.</p>

                <p>Second, since tips are subjectively determined, anything petty that can go wrong when dining out does not make giving less than the minimum fair. </p>
                <p>A customer’s evaluation of an experience should happen between 15-20% since it is subjective and there is no accountability whether the specifications were met for that job. This means the employee’s subjective evaluations can outshine true outstanding service. For instance, the waiter may liked or disliked over their display of intelligence, world view, accent, or attractiveness which is more about likability than actual performance. Therefore, it makes sense that at least the minimum is sufficient. By all means, if the performance is so outstanding and you want to pay over 20% to make a statement, do it! Some new celebrities dish out random high tips just because they can in that moment.</p>
                <p><a href="http://www.usmagazine.com/celebrity-news/news/amy-schumer-tips-hamilton-bartenders-1000-w167789">Amy Schumer leaves a $1000 Tip</a></p>
                <p>Third, giving a minimum tip and using your leverage as the customer can result in compensatory treatment in three ways.</p>
                <p>If you are a business person who dines out with clients, it is a good idea invest in establishments where you would like to be able to take clients for a quality experience. Knowing the menu, some names you can drop and even the extra tips given to employees parking your car or reserving your table can become allies when you need a favor to make a great impression. </p>
                <p>Another way to leverage your position as a customer is to get the manager’s attention. Remember to start with compliments about previous experiences or reviews and then mention how it turned into a sub-par experience. Often, this can result in some kind of immediate reparation to ensure you keep boasting about the place. </p>
                <p>Writing a review on a popular review site can lead to the business reaching out to you to make amends as well. Now, if a person writes on Yelp or other widely used social media channels, managers are tracking this feedback for damage control and may offer extra services so that you will return and change your review. Again, be careful to be fairly diplomatic so the owner can make good on your compliments. </p>

                <p>Fourth, there are two reasons why you may pay nothing. First, you simply cannot or second, your well-being as a customer was placed at risk by the business and nothing was done about it.</p>
                <p>If you realize that you simply cannot possibly pay your tip, pay something! Give what you have or let the service person you will return with the tip and be sure to give the place and persons an actual referral and or post a super positive review. This is an embarrassing route because it does not pay their bills to have your word but if they see your true effort to improve their business reputation, it may get you extra grace for your return able to do the right thing. </p>
                <p>If you get sick, the server becomes physically aggressive with you, or you see something completely unsanitary affect your food, by all means let the management know and likely, you may not need to pay for any of the experience at all. If you are forced to pay, then, you can decide not to tip on that same day. </p>
                <p>Fifth, out of respect for this expectation, avoid going to a place you know you cannot pay the minimum charge for adequate service. </p>
                <p>Call in advance to see if the service charge is added automatically since some businesses may already have an added service charge. Download an app for tipping on your iPhone or Android phone so you can estimate your tip in advance.</p>
                <p>Tipping is about etiquette. You are not legally required to tip yet employers are permitted to pay these positions with the presumption the public will honor this unwritten code to at least pay the minimum tip of these professions. A whole other article could be written about professions you should not tip too! Now that you know why we do it, how we do it and when it is okay to pay less or nothing, you can continue to show a higher level of class than those who do not see the larger scheme of tipping in the US. Regardless of your position, investing in others can always give you favor when you are on the way up or on the way down so be ready and generous with your gratuity and fair with your choice not to follow code.</p>

                <p>Also read <a href="/business-etiquette/business-dining.php">Business Dining Etiquette</a></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>