<?php
$meta_title = "Using Business Cards and Remembering Names | Training Connection";
$meta_description = "Present your business card with confidence and remember everyone you meet. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Business Cards</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Using Business Cards Effectively</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Networking is not complete without receiving or giving a business card. The business card is a way for you to follow up on the people you have met. Likewise, it is a way for them to contact you for further meetings.
                    More than that, your business card is a way to brand yourself. Professional-looking business cards send the message that you’re professional. Adding your company motto or tagline in your business advertises you and what you’re all about.</p>

                <p>For more details on <a href="/business-etiquette-training.php">the business etiquette training classes we offer</a> call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/business-cards.jpg" width="744" height="486" alt="How to use Business cards Effectively"></p>
                <h4>5 Tips on Using Business Cards Effectively:</h4>
                <ol>
                    <li>Never be without your business cards! (Make sure there’s always a stack in your office desk, and in your wallet. You never know; even a trip to the grocery story can present an opportunity to network. </li>
                    <li>Follow the protocol on hierarchy. Cards should not be given to senior executives that you meet, unless they’ve asked for one. </li>
                    <li>Time the presentation of your card.

                        <p>Don’t just hand over your business card at any random moment. Handing over a business card in the middle of a discussion can be an interruption, as parties would need to take a moment to look at it. You also want to make sure that your card is perused at point when the other person can give it his or her full attention. <a href="/business-etiquette-training-los-angeles.php">Looking for Business Etiquette training in Los Angeles</a>?</p>
                        <p> The best moment to hand over a card is when you’re asked for one, when you’re asked to repeat your name, or when someone offers to send you something. </p>
                        <p>If the two organizations that you each represent are well-known to each other, although you haven’t met your host before, offering your card is probably best left to the end of the meeting. If your host is unfamiliar with your company, offering your card at the beginning of the meeting is good practice.</p>
                    </li>
                    <li>
                        <p>Accompany your business card with an explanation of what you can offer them. </p>
                        <p>When you hand another person your card, give a brief "action recommendation." This can increase the likelihood of them contacting you again. For instance you may say: “I think I can help with your PR concerns, Mr. Johnston. Here is my card.” </p>
                        <p>You may also ask for referrals. Invite the other person to send your contact details to anyone they know who can use your services or products.</p>
                    </li>
                    <li>
                        <p>When receiving a business card, show the other person that you value their card. </p>
                        <p>Look at the business card for a few seconds. Comment about the card. Let them see that you take care in storing their card as well, instead of just jamming it in your pocket. <a href="/business-etiquette-training-chicago.php">Chicago business etiquette classes</a>.</p>
                    </li>
                </ol>

                <h4>Remembering Names</h4>
                <p>Remembering names may be difficult for some people, but it’s not impossible. It’s a skill: something that you can improve with constant application. </p>
                <p>Here are some ways to remember names:</p>

                <ul type="disc">
                    <li><strong>Repeat: </strong>When someone is introduced to you, repeat their name. “It’s a pleasure to meet you, Mark.” This can help reinforce your memory of the name. You may also introduce them to someone else so that you can create an opportunity to use their name.</li>
                    <li><strong>Use mental imagery: </strong>We think in pictures, therefore associating an image with a name can help in assisting recall. </li>
                </ul>
                <p>For example, after meeting Bill the plumber, imagine the word Bill spelled with pipes. If Jason Smith is marathon runner, imagine Jason running on a treadmill in a gym called Jason’s. Or just imagine a person’s name written on their forehead. Pick an image that works for you. The more striking or exaggerated your mental picture, the bigger  the chance of recall.</p>

                <ul>
                    <li><strong>Put it on paper: </strong>Write the name down as soon as you can. Or write their details on the business card they give you so that you will would remember them the next time you see them. (Just make sure you don’t let the person see you writing on their business card.)</li>
                    <li><strong>Use their name in creative sentences: </strong>Mentally construct sentences that are fun and a bit frivolous, to make name recall less stressful. Alliterations, or repeating consonant sounds in succession, are a great way to remember names. For example, to remember Jane who sells kitchen ware, you can repeat in your head: <em>Jane makes jam and juice in January.</em></li>
                    <li><strong>Be genuinely interested: </strong>Remembering names begins with attitude. If you are sincerely interested in a person, then they would make an impact on you. If you adopt the attitude that everyone is interesting, and could  potentially be an ally in business, then remembering names will come as second nature. </li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Etiquette Lessons</h4>
                    <ul>
                        <li><a  href="/business-etiquette/networking.php">Networking for Success</a></li>
                        <li><a  href="/business-etiquette/meet-greet.php">Appropriate Levels of Conversation</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>