<?php
$meta_title = "Borrowing Etiquette at Work | Training Connection";
$meta_description = "This article discusses good etiquette for borrowing and lending in the workplace. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Borrowing</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Borrowing Etiquette in the Workplace</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>It is that awkward feeling every time you see a certain somebody at the office. The culprit? Guilt. You borrowed something and at some point, you need to give it back. What you borrowed is possibly something you still need, keep forgetting to bring in, you have not used yet or maybe you are not sure where you put it! Maybe you are on the other side of this. You have a friend at work and you got extra inspired to bring him or her your favorite book or maybe after the company party, you spot a co-worker an Uber ride after confirming "You can get it next time!" and it’s June but you have not seen that person in a long while.</p>
                <p>Borrowing and lending can put one in a person in precarious situations in the workplace. In this article, we will cover the rules and how to observer the rules and gracefully navigate uncomfortable scenarios that can easily ensue when in a tight spot</p>
                <p>For more details on our <a href="/business-etiquette-training.php">hands-on business etiquette classes</a> call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/borrowing.jpg" width="780" height="439" alt="Borrowing in the workplace"></p>

                <h4>Quick History on Borrowing</h4>
                <p>The history of borrowing is closely tied to our <a class="bus-eti" href="http://www.pbs.org/wgbh/nova/ancient/history-money.html">money system</a>. First, dating back to 14000 BC people were bartering goods for other goods, then by 2000 BC loans with interest between peoples of the same religions and then, the Romans laid the foundation for what we know as banks. You can borrow with and without collateral yet because of written terms, there are penalties for going into default. </p>
                <p>Without getting any more complicated, individuals tend to lend and borrow without written terms and just barely confirming verbally what works best for both parties. “I’ll hit you back later, bruh!” Thus, why we have so many versions of the <a class="bus-eti" href="http://lawstreetmedia.com/issues/entertainment-and-culture/verdict-tv-court-shows/">People’s Court</a>! <a class="bus-eti" href="https://www.themuse.com/advice/the-6-unwritten-company-rules-you-wont-find-in-the-employee-handbook">When expectations are not clear</a> or we give something we could not afford to give, these unwritten arrangements can snowball into a series of unnatural and awkward relationships in the workplace. For more information on <a href="/business-etiquette-training-chicago.php">Chicago Etiquette classes</a>.</p>

                <h4>The Rules on Borrowing and Lending</h4>
                <ol>
                    <li>
                        <h5>Rule: Avoid Borrowing!</h5>
                        <p>This excerpt from From Edith E. Wiggin, in 1884's “Lessons on Manners / For School and Home Use”&nbsp;perfectly explains that staying out of debt with others can save your reputation but if you have to, make sure you make good on your word.</p>

                        <p>“<strong>It is not polite to keep a borrowed article long; and if a time for returning it is specified, we should be careful not to neglect doing it when the time comes. If possible, we should return it ourselves, not give it to the owner to carry home or send it by another; and we should never omit to thank the lender.&nbsp;</strong>"</p>
                        <p>To compel the owner to send for his property is a gross violation of good manners on the part of the borrower. The owner should not send unless he feels that he can wait no longer, or unless the borrower is habitually careless and needs to be taught a lesson.&nbsp;</p>

                        <p>"I never ask a gentleman to return money he has borrowed," said one man to another.&nbsp;</p>
                        <p>"How then do you get it?" asked his friend.&nbsp;</p>
                        <p>"After a while," was the answer, "I conclude he is not a gentleman, and then I ask him."&nbsp;</p>

                        <p>This reasoning will apply in case of lending other things as well as money.&nbsp;</p>
                        <p>When we lend we should do so with cordial politeness and not spoil the favor by the half-hearted way in which we offer or grant it; but borrowing should be regarded as a necessary evil, to be resorted to only when it cannot well be avoided. The habitual borrower is a burden to society.</p>
                        <ul>
                            <li>Avoid borrowing </li>
                            <li>Set written terms for lending/borrowing before doing so</li>
                            <li>Thank the lender and if they haven’t set terms, set terms yourself</li>
                            <li>Be prompt to return when promised at equal or better condition than lended</li>
                            <li>Communicate with your lender should you need more time</li>
                            <li>Avoid asking for what you lend unless you need it</li>
                            <li>Avoid behaving like you own someone or owe someone &nbsp;</li>
                            <li>Never lend what you cannot afford to lose</li>
                        </ul>
                    </li>
                    <li>
                        <h5>Special Scenarios</h5>

                        <h6>An Item is Broken</h6>
                        <p>Offer to fix it, make payment arrangements or provide an alternate way you can compensate for the damages. </p>
                        <p>You need to borrow but owner is not available to request: </p>
                        <p>Avoid taking something that is not yours unless both have set clear terms for sharing with the owning party.
                        </p>
                        <h6>You need more time: </h6>
                        <p>Go and speak to the lender to request more time and let them know you have not forgotten your debt. If the lender asks for an item you have sooner and you have it, be willing to return the item and be prompt. </p>
                        <h6>
                            You cannot afford to lend something:</h6>
                        <p>Just state your policy. Skip the story of how someone took advantage of your trust but simply state, “I understand. This is something I never lend. I can however suggest…”</p>
                        <h6>
                            You require terms and conditions: </h6>
                        <p>Here are my terms and conditions. Please sign here as a statement of our terms. If they do not want to sign, then maybe you should not lend!
                        <a href="/business-etiquette-training-los-angeles.php">Los Angeles softskill and etiquette classes</a>.</p>
                        <p>In conclusion, borrowing puts you in a vulnerable position with your peers at work and the more you borrow, the more pressure you will have to recover your freedom to do nice things for yourself because someone at work knows you owe them. In the case you are <a class="bus-eti" href="http://www.huffingtonpost.com/diane-gottsman/the-etiquette-of-borrowin_b_5696139.html">hard pressed</a>, ensure you thank the person, agree on terms and follow through with those as soon as possible. If you are the lender, anticipate default. You may have to write it off as a gift so do not give more than you can lose. If you are the borrower, pay up as soon as possible and keep in touch with the lending party. Finally, it is better to be the gift giver yet feel no regret about refusing to lend something you cannot afford to lose or will feel extra ownership over the borrower since that presumption was not an agreed upon term. </p>

                    </li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>