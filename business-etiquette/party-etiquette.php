<?php
$meta_title = "Holiday Party Etiquette | Training Connection";
$meta_description = "This article discusses the etiquette around Holiday Parties. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Party Etiquette</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Holiday Party Etiquette</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>The movie <a class="bus-eti" href="https://www.youtube.com/watch?v=g_itBfEC_TI">Office Christmas Party</a> is sure to give us plenty of laughs over what not to do at your holiday parties at the end of the year. Let’s take a moment to consider valuable tips to ensure you plan, show up and continue a positive impression on colleagues and clients you work with after all the feasting, drinking and Uber rides get you home after a great night out.</p>

                <p>For more details on <a class="bus-eti" href="/business-etiquette-training.php">business etiquette training</a> call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/holiday-party.jpg" width="780" height="484" alt="Tipping Etiquette"></p>

                <h4>Planning</h4>
                <p>In his book <a class="bus-eti" href="https://www.amazon.com/FINE-DINING-MADNESS-realities-dining/dp/0595670067"><em>Fine Dining Madness: The Rules and Realities of Fine</em> <em>Dining</em>, John Galloway</a>, calls out many mistakes people make while dining out recounting his five star experience as a professional waiter. He explores all sides of the business of fine dining so it is clear that the attendee and the host are responsible to prepare for the common sense that is not so common when dining out.</p>
                <p>As a host, consider these 5 tips:</p>
                <ol>
                    <li>Confirm a date (and venue) 6 weeks in advance to give adequate notice.</li>
                    <li>Choose a venue where you have good relationships with the staff or pre-visit a place where most of the attendees might prefer to go as an alternate option. Investigate the best table, confirm the menu for special food choices, special seating needs, wine list and ambiance.</li>
                    <li>Set the start time slightly early to anticipate late-comers and remind them to let you know if they will be arriving late.</li>
                    <li>Make reservations online or call during non-crunch hours (10-12pm or 2-5pm) for a 90 second call with questions prepared and remember who took the call.</li>
                    <li>Have another place in mind or booked- but be sure to cancel with the alternative venue as soon as possible since even reservations are not always a sure guarantee with restaurants you do not know.</li>
                </ol>

                <p>As a guest, consider these 5 tips:</p>
                <ol>
                    <li>Respond to the RSVP as soon as possible and note any special preferences, BYO(B) (for bringing any food or alcohol), plus one policy or mention any possibility of arriving later in a separate message written directly to the host.</li>
                    <li>Look up the menu, directions, dress code and parking as well and determine what time you will need to leave to be on time. </li>
                    <li>Have a snack and drink plenty of water in advance to arriving in case food is late or alcohol is served right away. Bring cash, breath mints, hand sanitizer, safety pin and tooth picks and brush as applicable.</li>
                    <li>Have an exit strategy to leave at a respectful time as well ensure safe transportation alternatives such as an active Uber or Lyft app on your phone. (Sometimes companies provide van services to ensure employees get home)</li>
                    <li>Let the host know if you need to cancel your attendance especially for direct/private invites with a host you know. </li>
                </ol>
                <h4>Table Etiquette</h4>
                <p>You made it on time! You see your colleagues waiting near the entrance for the table. <a href="/business-etiquette-training-chicago.php">Business Etiquette training classes in Chicago</a>.</p>
                <p>Here are 5 tips to ensure you show up ready to have fun with those you know from work.</p>
                <ol>
                    <li>Greet the host and introduce yourself to any unfamiliar faces as you patiently wait for the venue host or the Maitre’D to direct the group to seating or private party area. </li>
                    <li>Be courteous to the staff and avoid topics tipping, diet, table etiquette, politics or detailed personal health issues. Anything you wouldn’t share at work, avoid sharing at this party. Keep it positive and be willing to redirect conversations turning glib or argumentative.</li>
                    <li>Step away from the group to take any phone calls, have a smoke or to perform any personal primping. You need not say why you are stepping away, yet it is polite to nod to someone, “If you will excuse me, I need to step away- I’ll be right back.” &nbsp;</li>
                    <li>Be chivalrous. Offer to get any doors, wait to sit or help seat others and place a napkin on your lap before eating. Do use silver from the outside in, note your drink is the one on the right and place your napkin on the chair or folded to your left before walking away. Choose your drinks carefully and be fine to say when you have had enough.</li>
                    <li>Do your best to avoid work talk and if offered to toast, keep it professional and brief. Consider 3 quick points or less for your impromptu speech. </li>
                </ol>
                <p><a href="/business-etiquette-training-los-angeles.php">Los Angeles etiquette and skills training</a>.</p>

                <h4>Departure</h4>
                <p>In William Shakespeare’s <a class="bus-eti" href="http://www.folger.edu/romeo-and-juliet"><em>Romeo and Juliet</em></a>, Juliet proclaims, “Parting is such sweet sorrow.” In the case of a work party, parting can be a sneak operation since leaving the team at just the right time before things become overly familiar or risk any unprofessional memories - is an art! Have an exit strategy as well as rules for how you will ensure you hear about the rest of the night that happened without being the news of what happened that night.</p>
                <ol>
                    <li>Find and thank the host for inviting you and say your goodbyes to any significant groups or persons you had a great time speaking with during the event. </li>
                    <li>Make sure if you ordered anything separate from any corporate covered party bill, you have closed and tipped that tab. </li>
                    <li>Should you have any complaints about the experience, send a direct message to the corporate host to manage any mishap follow-up with the restaurant and remain courteous with staff.</li>
                    <li>Be sure to check you have all your things- jackets, phones, or cards that may still be on the table or in the party booth.</li>
                    <li>Send a follow up thank you note and feel free to post any positive review for the restaurant in the case of any extraordinary experience for their service handling your group.</li>
                </ol>

                <p>For more detailed training on <a class="bus-eti" href="/">Business Etiquette</a> in and outside the office, contact us at Training Connection to sign up for a course and live experience. </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>