<?php
$meta_title = "Networking for Success | Training Connection";
$meta_description = "Learn how network, with a strong introduction and making a great first impression. This and other topics are covered in our Business Etiquette training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-bus-eti">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/business-etiquette.php">Business Etiquette</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Networking</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Networking for Success</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>When you’re networking, it is important to make the most of the first meeting. In this tutorial, we’ll discuss how to create an effective introduction, make a good first impression and  minimize nervousness.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/business-etiquette/networking-event.jpg" width="748" height="500" alt="Networking and First Impressions"></p>
                <h4>Creating an Effective Introduction</h4>
                <p>Three steps to introducing yourself effectively:</p>
                <ol>
                    <li>
                        <p>Project <strong>warmth</strong> and <strong>confidence</strong>.</p>
                        <p>Many people size you up even before you say a word, which is why it’s important to mind your body language. When you introduce yourself, stand up straight, relax, and establish eye contact.</p>
                    </li>
                    <li>
                        <p><strong>State your first name and your last name</strong>. Depending on the situation, you may also state your affiliation or your position in the company.</p>
                        <p>Example: <em>“Hello. I’m Jill Smith. I’m the Quality Control Officer.”</em></p>
                    </li>
                    <li>
                        <p>When the other person has given their name, <strong>repeat it</strong> in acknowledgment.</p>
                        <p><em>“It’s nice to meet you, Mr. Andrews.” </em>or <em>“It’s nice to meet you, Joseph.”</em> Repeating their name is an acknowledgment that you heard their introduction.</p>
                    </li>
                </ol>

                <p>When you are networking is not just about presenting yourself. You may also find yourself introducing two strangers to one another. Here are some guidelines to introducing others:</p>
                <ul>
                    <li><strong>Take note of the pecking order</strong>. In business, introductions are made based on a person’s seniority in a company. This is regardless of age and gender. When you make an introduction, present a person with the lesser status to the person with the higher status. </li>
                </ul>
                <p>Example: <em>“Caroline Daniels, I would like you to meet President Andrews. Caroline is the head of the Public Relations Department."</em></p>

                <ul>
                    <li><strong>Introduce strangers first</strong>: If you are introducing two persons of equal rank to one another, start with the person that you don’t know. This way you can use the introduction to make the newcomer feel welcome.</li>
                    <li><strong>Mind titles</strong>: Unless invited otherwise, stick to using formal address such as "Mr. Gallagher" or "Attorney Louis Harris".</li>
                </ul>
                <p>For more details on <a href="/business-etiquette-training.php">training workshops we offer in business etiquette</a> call us on 888.815.0604.</p>

                <h4>Making a Great First Impression</h4>
                <p>If you want to make a good impression, know that you need to project 3 C's. <a href="/business-etiquette-training-chicago.php">Chicago Business Etiquette workshops</a>.</p>

                <h4>Confidence</h4>
                <ul>
                    <li>Having a straight but relaxed posture. Hold your head high and steady. Don’t slouch or slump. </li>
                    <li>Moving in a natural, unaffected manner. </li>
                    <li>Maintaining eye contact with the people you are talking to. </li>
                </ul>
                <h4>Competence</h4>
                <ul>
                    <li>Having a straight but relaxed posture. Hold your head high and steady. Don’t slouch or slump. </li>
                    <li>Moving in a natural, unaffected manner. </li>
                    <li>Maintaining eye contact with the people you are talking to. </li>
                </ul>
                <h4>Confidence</h4>
                <ul>
                    <li>Having a straight but relaxed posture. Hold your head high and steady. Don’t slouch or slump. </li>
                    <li>Moving in a natural, unaffected manner. </li>
                    <li>Maintaining eye contact with the people you are talking to. </li>
                </ul>

                <h4>Minimizing Nervousness</h4>
                <p>Meeting people can be anxiety-provoking. The need to impress another person can be a lot of pressure.</p>
                <p><a href="/business-etiquette-training-los-angeles.php">LA-based etiquette classes</a>.</p>
                <p>Here are some ways to minimize nervousness while in a social situation:</p>
                <ul>
                    <li><strong>Be informed: </strong>If possible, take time to research about the people you’re going to meet: their work, values, and preferences. Knowing what is expected from you can prepare you adequately. Nervousness is amplified by going into a situation blind.</li>
                    <li><strong>Practice! Practice! Practice: </strong>Networking is a skill, which means that you can develop it with practice. Practice your introduction in front of a mirror and note what you need to improve. You can also practice with peers. Get feedback from others about the kind of impression you give. Try to meet as many people as you can! The more you do it, the easier it gets!</li>

                    <li><strong>Learn relaxation techniques: </strong>There are many activities that can help relax a nervous person. These activities include:</li>
                    <li>Meditation</li>
                    <li>Self-talk</li>
                    <li>Visualization </li>
                    <li>Breathing exercises</li>
                    <li>Listening to music.</li>
                    <li><strong>Identify your triggers: </strong>If nervousness is a real problem for you, it is recommended that you identify what triggers your nervousness. Is it lack of confidence? Is it fear of authoritative people? Awareness can help you catch yourself in time and respond accordingly.</li>
                    <li><strong>Believe in what you have to offer: </strong>It’s easy to get intimidated by how successful or famous the other person is. But remember, they’re people--- just like you! They would be willing to listen to someone who can offer them something that they want or need. Have faith in your business. Have faith in your personal worth. Adopt the mindset that you are doing them a service, and it’s your duty to not let them miss the opportunity of meeting you!</li>
                </ul>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Business Etiquette Lessons</h4>
                    <ul>
                        <li><a href="/business-etiquette/business-cards.php">Business cards and Remembering names</a></li>
                        <li><a href="/business-etiquette/meet-greet.php">Meeting and Greeting</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Business Etiquette training</h4>
                    <p>Through our network of local trainers we deliver onsite Business Etiquette classes right across the country. Obtain a <a  href="/onsite-training.php">quote for  onsite Business Etiquette training</a>.</p>
                    <p>
                        To view a sample of  our past students testimonials, please click on the following link: <a  href="/testimonials.php?course_id=38">Business Etiquette student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>