<?php
$meta_title = "Adobe After effects Certification Exams | Chicago & Los Angeles | Training Connection";
$meta_description = "Passing Adobe After effects Certification Exams. Our face-to-face instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">After effects Cheat Sheets & shortcuts</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ec" style="background-image: url('/dist/images/banner-after-effects.jpg');">
        <div class="container">
            <div class="book-training-holder d-flex align-items-sm-center">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">
                        After effects 2019<br>
                        Cheat Sheets<br>
                        & shortcuts
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-exams">
        <div class="container">
            <div class="section-heading align-left" data-aos="fade-up">
                <h2>After effects 2019 Cheat Sheets & Shortcuts</h2>
            </div>

            <div class="card-deck card-row-sm card-deck-fblock">
                <div class="card card-fblock card-fblock-ae" data-aos="fade-up">
                    <h3 class="card-title">The Complete Adobe <br>
                    After effects Cheat Sheet</h3>
                    <div class="card-body">
                        <span class="icon icon-pdf-lg"></span>
                        <p>The complete Adobe <br class="d-sm-none">After effects CheatSheet</p>
                        <a href="/downloads/after-effects/AE-CheatSheet.pdf" class="btn btn-secondary stretched-link" target="_blank"><i class="fas fa-file-pdf mr-1"></i> Download</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>