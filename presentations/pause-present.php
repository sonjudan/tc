<?php
$meta_title = "Learn to Pause before you Present | Training Connection";
$meta_description = "When giving a presentation one of the most engaging non-verbal skills you could use is to pause before you speak. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Engaging Presentations</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Pausing before you Present</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Watch any game show and you will see that the pressure is on for contestants to articulate their best answer within 30 seconds. This expectation follows many people into the real world while it is not the best practice for corporate communication. </p>

                <p>For more details on our <a href="/presentation-training.php">instructor-led presentation training classes</a> call us on 888.815.0604. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/engaging-presentation.jpg" width="748" height="500" alt="Pausing before you present"></p>
                <p>When giving a presentation one of the most engaging non-verbal skills you could use is to pause before you speak, in the midst of your speaking and especially after asking any rhetorical questions.</p>
                <p>Using a Pause does 5 things:</p>
                <ol>
                    <li>It shows you are in control</li>
                    <li>You are about to say something super important</li>
                    <li>What you have said needs to sink in</li>
                    <li>It shows you are thoughtful</li>
                    <li>It allows people to applaud</li>
                </ol>

                <p>Take for instance, watch this clip from</p>
                <div class="embed-responsive embed-responsive-16by9 mb-3"><iframe width="560" height="315" src="https://www.youtube.com/embed/3vDWWy4CMhE" frameborder="0" allowfullscreen=""></iframe></div>

                <p>In <a href="http://www.nobelprize.org/nobel_prizes/peace/laureates/1964/king-bio.html">Martin Luther King</a> Jr’s “I Have a Dream” speech, he made a powerful impression speaking on equality that was clear and full of loaded pauses to allow the audience time to take in what was so important. </p>
                <p>When you get before an audience or in a meeting room or you are sitting on the interview chair to share what you are passionate about, you can pause for all same 5 reasons. </p>
                <p>Check out this clip for Steve Job’s iPhone 2007 presentation. <a href="/presentation-training-chicago.php">Presentation training classes in Chicago</a>.</p>

                <div class="embed-responsive embed-responsive-16by9 mb-3"><iframe width="560" height="315" src="https://www.youtube.com/embed/vN4U5FqrOdQ" frameborder="0" allowfullscreen=""></iframe></div>
                <p>Clearly, he appears in control of his information and allows for the audience to realize the depth of what he says by letting there be silence between his words. </p>
                <p>Here are some exercise you can practice answering with a pause to make a better impression as well as a point when you speak impromptu or when delivering a major presentation.</p>
                <p>Practice answering these while incorporating pauses:</p>
                <ol>
                    <li>
                        <p><strong>Tell me about yourself</strong></p>
                        <p>This question is the most abused question of all interviews! Be sure to stop and think before you speak. Consider your context! Before you share your funny habits and life story, consider how your answer is affecting how the asker can see value in what you bring to a project at work. </p>
                        <p>Before answering, pause and think of three things you will say. Give yourself at least 3 counts and then speak. Finally, enumerate what you are  going to say before you elaborate. </p>
                        <p>For example, “I like to make things. I became an engineer because I had a knack for understanding the process of taking an idea and making it useful to others.” </p>
                    </li>
                    <li>
                        <p><strong>Explain how serious you take being environmentally conscious at home</strong></p>
                        <p>Some may answer a question at a meeting with a story up front but in business communication, answer the question first. <a href="/presentation-training-los-angeles.php">Presentation training offered in LA</a>.</p>
                        <p>Be comfortable with unapologetic silence. That is also the power of a pause! Instead of “Wow! That’s a good question. I really don’t think about that but recently, an event transpired where I realized how much better I could be at conserving energy.”</p>
                        <p>Determine are you or aren’t you environmentally conscious and what you are going to say. “I am environmentally conscious about recycling goods for reuse at home, gas and lighting and I am also a big advocate for solar and wind power alternatives.” The truth is that people want the answer upfront and you can then share any related stories to support your answer instead of people sitting through a story to get your answer.</p>
                    </li>
                    <li>
                        <p><strong>Give an impromptu presentation about new technology that has improved processes in your office</strong></p>
                        <p>This challenge will allow you to consider your topic and how many opportunities you will have to practice the power of pauses while you share what is impressive about the topic of your choice. </p>
                        <p>Watch a good presenter, such as <a href="http://www.forbes.com/sites/carminegallo/2012/10/04/11-presentation-lessons-you-can-still-learn-from-steve-jobs/#18a0ae241516">Steve Jobs</a>, and walk through what you will say and when you can pause for any of the 5 reasons listed at the top of the article. Pausing is deliberate silence so breath and look at your audience making them responsible for these extra three to 5 seconds to think!”</p>
                    </li>
                </ol>
                <p>Also see <a href="/presentations/nervousness.php">Overcoming Nervousness on Presentations</a></p>

                <p>Going forward, in order to incorporate effective pauses, Steve Jobs was known for going over his presentations multiple times to ensure he knew exactly when he wanted to make the audience eager or thinking or applaud. Practice the next time you need to tell a story over drinks! Watch for it when you see commentaries or debates. Silence is powerful! Use it and you will be surprised by the difference!</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>