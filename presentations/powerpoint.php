<?php
$meta_title = "Creating PowerPoint Slides for a Presentation | Training Connection";
$meta_description = "Tips and Tricks to create complelling PowerPoint slides you can use in a presentation. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using PowerPoint Slides</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Creating Compelling PowerPoint Presentations</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Microsoft PowerPoint is a commanding tool for creating visual screens for a presentation. Visuals created in PowerPoint and projected on a screen are often easier to see in a large room than information displayed on a flip chart. Using PowerPoint offers the following benefits:</p>
                <ul>
                    <li>Allows you to add emphasis to important concepts, helping to increase retention of information</li>
                    <li>Adds variety to your presentation</li>
                    <li>Makes it easier to display images, charts, or graphs possibly too complex for a flip chart.</li>
                </ul>
                <p>Also, PowerPoint files can easily be shared with participants or others after the session. For more details on <a href="/presentation-training.php">our presentation workshops</a> call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/powerpoint-slide.jpg" width="750" height="422" alt="Powerpoint screen shot"></p>
                <h4>Required Tools</h4>
                <p>To create and use a Microsoft PowerPoint file to support your presentation outline, you will need:</p>
                <ul>
                    <li>Microsoft Office PowerPoint software for Microsoft Windows or Macintosh OS</li>
                    <li>A Windows or a MAC computer equipped with the minimum hardware and software specifications for your version of PowerPoint</li>
                    <li>An LCD or DLP projector</li>
                    <li>A projection screen</li>
                </ul>
                <p>Optionally, you may wish to add the following to your toolkit:</p>
                <ul>
                    <li>Storage media such as a USB memory stick or CD-R disc</li>
                    <li>An extension cord</li>
                    <li>A laser pointer for emphasis during the discussion of a PowerPoint slide.</li>
                </ul>
                <p><a href="/presentation-training-chicago.php">Chicago Presentation training</a>.</p>

                <h4>Tips and Tricks</h4>
                <p> Use the following suggestions to enhance the benefit of your PowerPoint presentation.</p>

                <h4>Overall Appearance</h4>
                <ul>
                    <li>Display only one major concept on each slide</li>
                    <li>Use short phrases or bullet points rather than paragraphs</li>
                    <li>Limit each line of text to no more than 7-8 words</li>
                    <li>Allow only 7-8 lines of text per slide</li>
                    <li>Use images sparingly; one or two per slide</li>
                    <li>Leave a good amount of blank space in your presentation</li>
                    <li>Create a title for each slide</li>
                    <li>Use effects, transitions animation, and sound very sparingly.</li>
                </ul>
                <h4>Fonts and Color</h4>
                <ul>
                    <li>Use simple sans serif fonts such as Helvetica or Arial for readability</li>
                    <li>Select a point size of 32 or larger for titles, and 20 points for body text</li>
                    <li>Use colors that work well together, such as yellow or white on a dark blue background.</li>
                    <li>Check the readability and visibility of your fonts and color choices with the lighting in the room in which you will present.</li>
                </ul>

                <h4>Preparation</h4>
                <ul>
                    <li>Make sure to match your slides to the purpose of the presentation </li>
                    <li>Develop a template and stick to it for a consistent look and feel<strong></strong>.</li>
                </ul>
                <h4>Computer</h4>
                <ul>
                    <li>Check your equipment, computer settings, and room lighting in advance</li>
                    <li>Before your presentation, turn off screensavers, instant messaging, and email notifications</li>
                    <li>Make sure that your computer’s power management console will not automatically shut the system down after a set amount of time.</li>
                </ul>
                <p><a href="/presentation-training-los-angeles.php">Interactive Presentation classes in Los Angeles</a>.</p>

                <h4>Creating a Plan B</h4>
                <p>While technology allows you to make great enhancements to a presentation, it also offers more opportunities for technical trouble. Here are some suggestions to keep your presentation moving along, even if the technology isn’t.</p>
                <ul>
                    <li>Make one or more backup copies of your PowerPoint file on the computer on which you plan to show the presentation.</li>
                    <li>Before the presentation, download and install the free Microsoft PowerPoint Viewer available at <a href="http://www.microsoft.com">www.microsoft.com</a>. In the event that your PowerPoint software won’t run, you will still be able to use the viewer to show your PowerPoint slides.</li>
                    <li>Copy your PowerPoint file onto a USB Drive. That way, if you have a computer problem, you can move the file to another one, if available. </li>
                    <li>Bring sufficient printed copies of your presentation for participants. If logistics prevent that, plan to have at least one copy available for photocopying on site.</li>
                    <li>If all else fails, write your key points on a flip chart.</li>
                </ul>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Presentation Lessons</h4>
                    <ul>
                        <li><a href="/presentations/nervousness.php">Overcoming Nervousness</a></li>
                        <li><a href="/presentations/engagement.php">Engaging with your Audience</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>