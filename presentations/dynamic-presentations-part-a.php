<?php
$meta_title = "Using Visuals to create engaging presentations | Training Connection";
$meta_description = "This article series discusses how to create dynamic presentations that engage all the senses. Part A looks at Visuals. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using Visuals</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Using Visuals in Presentations</h1>
                        <h5>by Allyncia Williams</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Bueller, Bueller... </p>

                <p>Ferris Bueller’s Day Off has to be one of my favorite movies from childhood. Ferris Bueller, a high school senior, basically plans the most exciting day off ever in Chicago, city of my birth. Since my days home from school were spent watching reruns, thermometer checking and waiting for Oprah to come on, this movie was pure brilliance of what adventure could be had if one was resourceful and adventurous enough. The funniest scene for me is when his monotone voiced teacher continues to repeat his name to confirm attendance while the scene cuts away to Ferris having the time of his life. Everyone laughs and agrees, ‘What a terrible presenter!’</p>

                <p>For more details on our <a href="/presentation-training.php">presentation training classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/engaging-presentation.jpg" width="748" height="500" alt="Using Visuals in the presentations"></p>

                <p>How can we make presentations dynamic, memorable and practical? In this article, I can show you how you can engage all the senses of a live audience to capture, keep and ensure your corporate audiences come away with something they can use. </p>

                <p>Since starting my teaching career in Sunday school, I realized once you stand in front of a group, you can lead them anywhere. For instance, if I say, “Everybody, get up!” They get up. If say, “Now, we are going to do this..." They prepare their minds for what’s next. After teaching and guiding some exercise, if I say, “How did we learn to solve this scenario?” The participants recite back what we did. I use visuals, sound, movement spacing and change of modalities because without even studying attention span or brain theory, my background as a singer taught me, you need to keep your audience active no matter the topic! <a href="/presentation-training-los-angeles.php">For more information on Presentation training in Los Angeles</a>.</p>
                <p>Let’s look at ways you too can engage your audience with sound, movement, visuals, nonverbal and change modalities:</p>

                <h4>Visuals</h4>
                <p>We’ll start with what you see since the speed of light is faster than all of these dynamics. The first effect you have on your audience is how you and your presentation environment looks. How are you dressed? What is on the board? How are the seats placed? Are there documents already placed in position before they enter the room? Is it clear you are in charge or about to take charge? </p>
                <ul>
                    <li>Like a theater stage, you are in front of everyone. Unlike a theater stage, everyone is on stage with you. Arrive first so that you can set up any notes on the board, documents on the desks and test the technology. Be sure to keep your personal belonging out of sight and have your phone on silent with no vibration.</li>
                    <li>Be sure to wear something comfortable and tasteful so that it is easy to turn completely around to be viewed at all angles. Practice reaching up, bending over, jacket on or off to test whether you can remain poised in what you wear. </li>
                    <li>Keep multimedia visuals simple! Of course, we love all the imagery, animations and transitions we can impress the eye with but the real star of the show should be you. Take our <a href="/powerpoint-training.php">PowerPoint class</a> where I break down basic design theory for making online visuals easy on the eye and for the brain to process. </li>
                    <li>Set up the seating in a way that is conducive to an active audience. If you are working with people hiding behind laptops, random tables and chairs or a meeting room set up, don’t be afraid to start moving items around to allow for more movement or closeness.</li>
                </ul>

                <p>Visuals include you, the room, the seating and any stations or workspace available for switching gears. I’ve held active seminars in small health clinic lobbies to large 100 seater conference halls with multiple wall size screens. In each scenario, it is important to block out how you will engage the audience to get out of ‘seminar bus’ mode and create an experience for the participants they weren’t expecting when they heard they were going to sit through a presentation.</p>
                <p>In the next article we will consider <a href="/presentations/dynamic-presentations-part-b.php" rel="next">Sound in delivering  Presentations that Engage the Senses</a>.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>