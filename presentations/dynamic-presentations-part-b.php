<?php
$meta_title = "Using Sound to create engaging presentations | Training Connection";
$meta_description = "This article series discusses how to create dynamic presentations that engage all the senses. Part B looks at incorporating Sound. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using Sounds</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Using Sounds in Presentations</h1>
                        <p>by Allyncia Williams</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Since reading <a href="/presentations/dynamic-presentations-part-a.php" rel="prev">part I of this series</a> on Presentations Engaging the  Senses, hopefully, you looked up Ferris Bueller’s Day Off and had a laugh at the scene where Ferris Bueller’s somber history teacher continues to repeat his name in a monotone voice while the scene cuts away to how Ferris is mischievously skipping school and having the time of his life. The scene is painfully funny to anyone who has ever had to give a presentation or sit through a presentation with poor presentation dynamics.</p>
                <p>The scene: <a href="https://www.youtube.com/watch?v=NP0mQeLWCCo">https://www.youtube.com/watch?v=NP0mQeLWCCo</a></p>
                <p>For more details on our <a href="/presentation-training.php">presentation training in Chicago and LA</a> call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/engaging-presentation.jpg" width="748" height="500" alt="Using Sounds in the presentations"></p>

                <h4>Sound</h4>
                <p>Sound is slightly less fast than the speed of light therefore it follows my part 1 well. Quality sound can make or break any recorded or live presentation. <a href="https://bengreenfieldfitness.com/2012/05/how-you-can-use-sound-and-music-to-change-your-brain-waves-with-laser-accuracy-and-achieve-huge-focus-and-performance-gains/">It even has biological effects on the body</a> and can affect hormones that affect stress and focus. </p>
                <p>Like Bueller’s teacher, you may not realize how sounds impact the audience interest in what you say. &nbsp;I have studied languages and possess years of operatic vocal training so I have learned the value of pace, pronunciation and even the biomechanics of good tone yet it was broadcasting skills coaching with <a href="http://www.huffingtonpost.com/reyne-haines/the-inside-scoop-on-jonat_b_693462.html">Jon Novak</a> of <a href="@abc7%2520Eyewitness%2520News%2520Meteorologist%2520%2520%2520Los%2520Angeles">ABC Channel 7 LA Eyewitness News Meteorologist</a> that helped me understand the complexity of sounding natural while presenting spoken material to a live or online audience. We cover this in many of our <a href="/business-communication-training.php">Business Communication</a> and <a href="/customer-service-training.php">Exceptional Customer Service classes</a> at Training Connection.</p>
                <p>Here are tips for engaging the audience with the dynamics of sound: </p>
                <ul>
                    <li>Project your voice and enunciate your words.</li>
                </ul>
                <p>Speak slightly louder, watch your pace and enunciate. People often speak faster when they are confident, misunderstood or rushing only to be asked, “What?” again. We could easily blame regional accents and phrasing but did you know that nearly a fifth of Americans over 12 years old have hearing loss so severe it may make communication difficult, according to <a href="http://www.hopkinsmedicine.org/news/media/releases/one_in_five_americans_has_hearing_loss">Johns Hopkins researchers</a>. You may be speaking louder but not any more clearly so slow down and pronounce words fully. <a href="/presentation-training-chicago.php">Click here</a> for details about Presentation class in Chicago.</p>
                <ul>
                    <li>Use a microphone if you have a soft voice or a big room to test for any adjustments to base or reverb of your microphone or media speakers in advance. There is nothing good about magnifying a bad sound or having your technology cut in and out because you didn’t get the settings to a <a href="https://spinditty.com/instruments-gear/Speaker-Watt">comfortable level</a> before you began.</li>
                </ul>
                <ul>
                    <li>
                        <p>Put your vocal tendencies in check!</p>
                        <ul>
                            <li>When you speak, you are giving cues on the timing such as the start, the main points and the finale of your speech. These inflections, while regional or habitual in personality, can send messages on how confident, emotional and interested you are in what you say. Get someone to hear you rehearse and better yet, get training so you know how to manage your intonation with the audience. </li>
                            <li>Avoid reading directly from the slide, teleprompter or materials in monotone. Practice what you want to say so that you can be ready in the case of difficult words, names or subject matter. </li>
                            <li>Cut out distracting tendencies like clearing one’s throat, redundant words “like, okay, so, and um, you know etc” These may make you feel natural but it gives your audience a less polished impression of your confidence and credibility.</li>
                        </ul>
                    </li>
                    <li>
                        <p>Silence is a vital part of your presentation</p>

                        <ul>
                            <li>Pause to allow for thoughtful consideration, a response or applause to a statement or question.</li>
                            <li>Allow silence for note taking silence especially if you have asked them to complete some task.</li>
                            <li>When asked a question, stop-think- then answer! Your word may be used against your credibility so instead of responding like you are at a game show, think about what you want to say first, then speak.</li>
                        </ul>
                    </li>
                    <li>
                        <p>Use repetition.</p>

                        <ul>
                            <li>When a participant speaks, repeat their question aloud. This shows the person you understood and ensures the rest of the audience hears what you were asked. </li>
                            <li>Like posting a logo on all your materials, repeating a phrase can help listeners retain a concept or remember a brand when you speak or present a sound bite in any multimedia tools you  use.</li>
                            <li>Ask your audience to repeat or participate by answering a question you just answered since this also improves retention.</li>
                        </ul>
                    </li>
                    <li>Consider the sound for mood. If energy is down, change things up! You can invoke laughter, thoughtfulness or generate a comfortable buzz by putting on music or allowing participants to have some talking time amongst themselves. Since music affects so many parts of the brain, studies are showing music has incredible benefits for learning as well. I have incorporated chants, rhymes, song references and even sung Sinatra songs to create schema or memorable learning connections to teach concepts on software functions to pronunciation! The more you do a presentation, the better you can plan for this. </li>
                </ul>


                <p>Check out The <a href="http://jmt.oxfordjournals.org/content/early/recent">Journal of Music Therapy</a> and this article on <a href="http://www.cracked.com/article_18405_7-insane-ways-music-affects-body-according-to-science.html">7 Ways Music Affects the Body</a>.</p>
                <p>Like all sound artists, you can break rules if you get the fundamentals right. Be heard, be understood and provide variation of voice, media and even use silence to keep your audience with you for the long or short haul. </p>
                <p>In <a href="/presentations/dynamic-presentations-part-c.php" rel="next">Part III in this series</a>, we will look at  movement which includes interaction with props and spacing.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>