<?php
$meta_title = "Using Movement to create engaging presentations | Training Connection";
$meta_description = "This article series discusses how to create dynamic presentations that engage all the senses. Part C looks at incorporating movement. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using Movement</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Using Movement in Presentations</h1>
                        <h5>by Allyncia Williams</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>This article is the third in a series on giving  Presentations Engaging the 5 Senses. If you too laugh every time you see the often quoted scene from Ferris Bueller’s Day Off, then you know how important it is to make your presentations engaging beyond asking “Anyone, Anyone?” in a monotone voice as does the boring history professor in the movie. We have covered <a href="/presentations/dynamic-presentations-part-a.php" rel="prev">visuals in Part 1</a>, <a href="/presentations/dynamic-presentations-part-b.php" rel="prev">sound in Part 2</a> and now we will discuss how movement affects the dynamics of your presentation. </p>
                <p>In part 1 of this article series, I used the expression of ‘seminar bus’ and what I mean by that is when you are learning something new, training is designed to take you from point A to B. In a seminar, a presenter is in front of the audience and leads by a monologue to transmit the information. During the ride, you may experience opportunities to be randomly called into participation and finally, you are held responsible to review and apply the learning later as a result of exposure or an actual test. Do you remember what you learned the last time you sat through a boring movie? You may not even recall the name of the movie you saw! </p>

                <p>For more details on our <a href="/presentation-training.php">presentation training course</a> in Chicago and LA call us on 888.815.0604. </p>



                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/engaging-presentation.jpg" width="748" height="500" alt="Using movement during a presentation"></p>

                <h4>Movement</h4>
                <p>(Here is a sample of click through presentation <a href="https://youtu.be/RrpajcAgR1E">Self Identity 2.0</a> Notice how the Presenter stays at the computer clicking away as the slides do all the connecting.)</p>
                <p>When I train, it is important for me to play with proximity, energy of the room, and physical simulations so that the experience is memorable and engages the whole body. After reading Brain Rules by <a href="http://www.brainrules.net/exercise"><strong>DR. JOHN J. MEDINA</strong></a>, a developmental molecular biologist, I was further confirmed in my teaching strategies that getting learners and myself to move around the room affected learning just as much as bringing well organized information for the brain to retain. In his studies, Medina states Brain Rule #1: Exercise Boosts Brain Power and even goes on to promote the use of walking while reading in his book, Brain Rules. <a href="/presentation-training-chicago.php">Instructor-led presentation classes</a> offered in Chicago.</p>
                <p>Here are tips for engaging the audience with the dynamics of movement in three ways: </p>

                <p>Block out how you will move within the space!</p>
                <p>Before paper, which would not be invented until 200 years later in China, it is said that <a href="https://hbr.org/2011/05/presenters-guide-to-remembering">Cicero, a first century Roman philosopher</a>, statesman, and orator and likely his contemporaries used the marble columns of the forum as memory triggers while giving speeches for hours on end. Each column represented a single subject and its related ideas. Orators walked from column to column and subject to subject, using the visual prompts to remind them of a group of related ideas. </p>
                <p>We know that presentations given today are cued with notes at a podium, on a clipboard or cards and now, for the love of projectors and software presentation tools, people click away at slides from the front of the room. Clearly, all of this has brought movement in the room to a standstill until only the presenter’s index finger on the mouse is hard at work and people can only focus on the information for <a href="https://www.trainingindustry.com/blog/blog-entries/attention-span-and-performance-improvement.aspx">20 minutes</a> before their brains stop recording the information.</p>

                <p>Instead of staying at the front of the room, change your participant movement, focus and proximity to keep your audience’s brain active. <a href="/presentation-training-los-angeles.php">LA-based presentation workshop</a>.</p>
                <p>How you can use movement to keep participants active:</p>
                <ul>
                    <li>By moving yourself, the audience needs to change positions at their seats to focus on you. </li>
                    <li>By moving the audience, participants can shake off the fatigue of sitting and better absorb new information. </li>
                    <li>By using proximity- how close people are to one another- you can bring energy to areas the room where people may be more distracted. </li>
                </ul>

                <p>Choose at least three areas in the room or on a huge stage you will visit during the course of your presentation. Here are some ways to  get moving:</p>
                <ul>
                    <li>When changing the modality of training. (A topic we’ll cover in my 4th article in this series). </li>
                    <li>When going directly to participants to assist with any corresponding technology or props </li>
                    <li>By asking participants to change seats to get into teams or partner off in groups </li>
                    <li>Sending participants to another room entirely to come back and interact</li>
                    <li>Placing participants at the front of the room to use the board, computer or present</li>
                    <li>Asking participants to play out a scenario or create something using props</li>
                    <li>Asking participants to use handwriting for notes (it’s more kinestic!) (especially if their study is on a computer)</li>
                    <li>Asking participants to come up and write on the board</li>
                    <li>Asking participants to demonstrate a prop in another area of the room or pass around at their desks</li>
                </ul>

                <p>For instance, I know when I teach Time Management seminars I am already looking for what side of the room I will use for an exercise that takes up half of the class area. When I teach Customer Service and Business Writing, I have volunteer exercises asking for participants to switch seating so many times they are never in the same seat for the entire class. If I’m teaching presentations or even software, I become the instructor from the back of the room and often, I request students to lead from the front! This movement changes the experience from all eyes on you to actually allowing your participants to experience the information in context! </p>
                <p>Being a live singer, I was not paid much or at all for opera or jazz but it greatly influenced being a highly paid wedding singer. I read in many biographies on <a href="http://www.npr.org/2015/04/07/397877385/billie-holiday-a-singer-beyond-our-understanding">Billy Holiday</a> how she worked at a bar where she had to pick up her tips off the tables in the audience. She hated that! I felt equally awkward in Japan when asked to please shake hands with the guests as I sang songs they picked out for the reception. But what it did for the guests, to have live interaction with me as their songstress influenced my teaching style for years to come. Getting close, making eye contact, bringing human energy to the back of the distracted room or key important guests and getting participants to move and act out what they learn with you, gives you an advantage that science has yet to rival even with the sharpest witted robot or video game. </p>
                <p>Make sure your dynamic presentation makes you move and moves your audience!</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>