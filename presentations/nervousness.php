<?php
$meta_title = "Overcoming Nervousness During Presentations | Training Connection";
$meta_description = "Tips and Tricks on how to conquer nervousness and deliver knockout presentations. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">How to handle Nervousness</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Overcoming Nervousness during Presentations</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Nervousness is normal when giving a presentation. After all, public speaking is the top fear in the top ten list of fears. Nervousness can strike at different points in a presentation</p>
                <ul>
                    <li>At the beginning</li>
                    <li>If you feel the audience has slipped away from you</li>
                    <li>If your memory betrays you.</li>
                </ul>
                <p>This module will provide you with concrete strategies for overcoming presentation jitters.</p>

                <p>For more details on <a href="/presentation-training.php">our presentation classes</a> call us on 888.815.0604.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/nerve-free-presentation.jpg" width="750" height="500" alt="Confident nerve-free presentation"></p>
                <h4>Preparing Mentally</h4>
                <p>Visualization is the formation of mental visual images. It is an excellent way to prepare your mind before a presentation. There are several types of visualization:</p>
                <ul type="disc">
                    <li><strong>Receptive Visualization</strong>: Relax, clear your mind, sketch a vague scene, ask a question, and wait for a response. You might imagine you are on the beach, hearing and smelling the sea. You might ask, “Why can’t I relax?”, and the answer may flow into your consciousness.</li>
                    <li><strong>Programmed Visualization</strong>: Create an image, giving it sight, taste, sound, and smell. Imagine a goal you want to reach, or a healing you wish to accelerate. Jane used visualization when she took up running, feeling the push of running the hills, the sweat, and the press to the finish line.</li>
                    <li><strong>Guided Visualization:</strong> Visualize again a scene in detail, but this time leave out important elements. Wait for your subconscious to supply missing pieces to your puzzle. Your scene could be something pleasant from the past.</li>
                </ul>
                <h4>The process for Effective Visualization</h4>
                <ul>
                    <li>Loosen your clothing, sit or lie down in a quiet place, and close your eyes softly.</li>
                    <li>Scan your body, seeking tension in specific muscles. Relax those muscles as much as you can.</li>
                    <li>Form mental sense impressions. Involve all your senses; sight, hearing, smell, touch and taste.</li>
                    <li>Use affirmations. Repeat short, positive statements and avoid negatives such as “I am not tense”; rather, say “I am letting go of tension.”</li>
                    <li>Use affirmations. Repeat short, positive statements that affirm your ability to relax now. Use present tense and positive language. As an example:</li>
                    <li>Tension flows from my body</li>
                    <li>I can relax at will.</li>
                    <li>I am in harmony with life.</li>
                    <li>Peace is within me.</li>
                </ul>
                <p>Visualize three times a day. It’s easiest if you visualize in the morning and at night while lying in bed. Soon, you will be able to visualize just about anywhere, especially before a presentation. The <a href="/presentation-training-chicago.php">best presentation training in Chicago</a>.</p>

                <h4>Physical Relaxation Techniques</h4>
                <p>People who are nervous tend to breathe many short, shallow breaths in their upper chest. Breathing exercises can alleviate this. You can do most breathing exercises anywhere. Below are some exercises that will assist you in relaxing.</p>
                <ul>
                    <li><strong>Breathing Exercises: </strong>Deliberately controlling your breathing can help a person calm down. Ways to do this include: breathing through one’s nose and exhaling through one’s mouth, breathing from one’s diagram, and breathing rhythmically. <strong></strong></li>
                    <li><strong>Meditation: </strong>Meditation is a way of exercising mental discipline. Most meditation techniques involve increasing self-awareness, monitoring thoughts, and focusing. Meditation techniques include prayer, the repetition of a mantra, and relaxing movement or postures. <strong></strong></li>
                    <li><strong>Progressive Muscle Relaxation (PMR): </strong>PMR is a technique of stress management that involves mentally inducing your muscles to tense and relax. PMR usually focuses on areas of the body where tension is commonly felt, such as the head, shoulders, and chest area. It’s a way to exercise the power of the mind over the body.<strong> </strong></li>
                    <li><strong>Visualization: </strong>Visualization is the use of mental imagery to induce relaxation. Some visualization exercise involves picturing a place of serenity and comfort, such as a beach or a garden. Other visualization exercises involve imagining the release of anger in a metaphorical form. An example of this latter kind of visualization is imagining one’s anger as a ball to be released to space.</li>
                </ul>
                <p><a href="/presentation-training-los-angeles.php">Presentation workshops held in Los Angeles</a>.</p>

                <h4>Appearing Confident in Front of the Crowd</h4>
                <p>In addition to everything we’ve discussed, below are some tips for maintaining your confidence when you’re “on”.</p>
                <ul type="disc">
                    <li>Get a good night’s sleep</li>
                    <li>Practice your words along with your visuals</li>
                    <li>Have a full “dress rehearsal”</li>
                    <li>If you are traveling to a new site out of town, try to arrive early in the evening and locate th esite. That way you won’t be frazzled in the morning, trying to locate the venue.</li>
                </ul>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-bc" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Presentation Lessons</h4>
                    <ul>
                        <li><a href="/presentations/powerpoint.php">Incoporating PowerPoint into your Presentations</a></li>
                        <li><a href="/presentations/engagement.php">Delivering Engaging Presentations</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>