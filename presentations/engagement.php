<?php
$meta_title = "How To Engage with your Audience during a Presentation | Training Connection";
$meta_description = "Tips and Tricks on how best to engage with your audience, using humor, discussion to conquer nervousness and deliver knockout presentations. This and other topics are covered in our Presentation training course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-presentation">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/presentations.php">Presentations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Engaging Presentations</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">

                    <div data-aos="fade-up">
                        <h1>Delivering Engaging Presentations</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">

                <p>Bringing your presentation to the next level is something you can accomplish by  adding some  little touches that will produce a lot of value during your presentation.</p>
                <p>For more details on our <a href="/presentation-training.php">presentation training classes</a> call us on 888.815.0604.  </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/presentations/engaging-presentation.jpg" width="748" height="500" alt="Interact with your audience"></p>
                <h4>Make Them Laugh a Little</h4>
                <p>Humor is a popular way to liven up a presentation. It makes the audience align with you, and sends a signal that you are in charge. Handled properly, humor enriches a presentation. The<a href="/presentation-training-los-angeles.php"> best presentation class in Los Angeles</a>.</p><p>
                    When considering humor, make sure that whatever content you choose meets four criteria:</p>
                <ul>
                    <li>You think the joke or lines are funny</li>
                    <li>You can repeat the piece confidently and comfortably</li>
                    <li>Your choice is not offensive to anyone (gender, race, age, disability, politics)</li>
                    <li>Your audience will understand and appreciate what you are saying.</li>
                    <li>A joke should have a punch line, delivered with all you’ve got.</li>
                </ul>
                <p>Here are some tips for collecting and using humour:</p>
                <ul>
                    <li>Jot down jokes as you hear them in everyday life; classify them as your collection grows</li>
                    <li>Deliver any humor verbally only, and keep things light</li>
                    <li>Match your humor to the demographics of the audience</li>
                    <li>Research and consider using local humor if you’re working off-site</li>
                </ul>
                <p>If a joke or delivering humor with words isn't within your comfort level, consider sharing a lighthearted cartoon, doing a simple magic trick, or doing something else that is unexpected and evokes a reaction and some emotion from the participants. </p>

                <h4>Ask Them a Question</h4>
                <p>Questions can be used in many ways, and at just about any time during your presentation.</p>
                <ul>
                    <li>As an opener</li>
                    <li>To check whether the desired learning is occurring, or to extend the learning experience</li>
                    <li>To diffuse a difficult or uncomfortable situation</li>
                    <li>To fill a long pause</li>
                    <li>To get a feel about the mood in the room.</li>
                </ul>

                <h4>Encouraging Discussion</h4>
                <p>Much of the discussion during your presentation will be structured to fit with the learning exercises. If a remark or question is made during a discussion that is off topic or something that should not be dealt with at the time, you can always add it to the parking lot, and return to it during the wrap-up to bring closure. <a href="/presentation-training-chicago.php">Presentation training in the Chicago Loop</a>.</p>



                <h4>Dealing with Questions</h4>
                <p><strong>Q&amp;A Sessions: </strong>If time permits in your presentation, you may choose to hold a general question-and-answer session. Since as the presenter you are in control, you can decide when to stop the discussion. In a large room, be prepared to repeat each question. If no questions arise, be prepared to ask one yourself.</p><p>
                    You can use an open question to begin the session: “What questions do you have?”</p><p>
                    <strong>Restating Negative Questions: </strong>If a question is phrased negatively, restate it. For example, "Why have so many of his staff displayed chronic absenteeism?" can be restated as "Let's explore what we can do to reduce absenteeism in the team."</p><p>
                    <strong>Off-topic: </strong>Don't forget about the parking lot if you receive an off-topic question.</p><p>
                    <strong>Leveraging experience in the room: </strong>There may be situations when you wish to redirect a question to one of the participants. Again, you are in charge, so call upon someone and keep the discussion moving on afterward.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-presentation" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Presentation Lessons</h4>
                    <ul>
                        <li><a href="/presentations/powerpoint.php">Using PowerPoint during your Presentations</a></li>
                        <li><a href="/presentations/nervousness.php">Overcoming Nervousness during Presentations</a></li>
                    </ul>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Presentation training</h4>
                    <p>Through our network of local trainers we deliver onsite Presentation classes right across the country. Obtain a <a href="/onsite-training.php">quote for an onsite Presentation class</a>.</p>
                    <p>To view a sample of  our past students testimonials, please click on the following link: <a href="/testimonials.php?course_id=80">Presentation student reviews</a>. </p>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/footer.php'; ?>