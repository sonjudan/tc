<?php
$meta_title = "Word Training Classes | Chicago | Training Connection";
$meta_description = "Need to learn Microsoft Word? Our Chicago instructor-led classes are still the best way to learn Word. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted">
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Word Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>


    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-word.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up">Word Training<br>
                      Chicago
                    </h1>

                    <div data-aos="fade-up">
                        <h4>Word 2013, 2016, 2019 &amp; 365</h4>
                        <p>Looking for Microsoft Word class in Chicago?</p>
                        <p>Our hands-on Word classes are taught by live instructors present in the classroom. They will teach you step-by-step how to master Microsoft Word, with demonstrations,  hands-on exercises and be available if you need any one-on-one help. This is still the most effective way to learn Word.</p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-word-2019.png" alt="MS Word 2019 box shot">
                    <img src="/dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>
        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">
                <h3>What's Included</h3>
                <ul>
                    <li>Printed MS Word training manual</li>
                    <li>Certificate of course completion</li>
                    <li>Small class size (typically 3 students)</li>
                    <li>and most importantly a 6 month free repeat.</li>
                </ul>
                <p><strong>Book a Word class today. All classes guaranteed to run!</strong><br>
                    <a class="" href="/onsite-training.php">Onsite training available countrywide.</a>
                </p>
            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>
                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0" data-aos="fade-up">
                <h2>Microsoft Word Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-word" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="MS Word Level 1 - Introduction"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Word Level 1 - Introduction
                                </a>
                            </h3>
                            <p>On this hands-on beginner class you will learn to create, edit, and format standard business documents using Microsoft Word. You will work with text, paragraphs, tables, lists, graphics and learn control over page appearance, before printing or sharing your documents.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Word Level 1 - Introduction" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/word/Word%20Level%201.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Word Level 1 Introduction">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable Chicago
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Opening Word</h5>
                                        <ul>
                                            <li>Opening Word</li>
                                            <li>Using the Recent List</li>
                                            <li>Opening Files</li>
                                            <li>Creating a Blank Document</li>
                                            <li>Creating a Document from a Template</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with the Interface</h5>
                                        <ul>
                                            <li>Understanding the Ribbon and the Status Bar</li>
                                            <li>About Your Account</li>
                                            <li>Using Backstage View</li>
                                            <li>Saving Files</li>
                                            <li>Closing Files</li>
                                            <li>Closing Word</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Your First Document</h5>
                                        <ul>
                                            <li>Typing Text</li>
                                            <li>Selecting Text with the Mouse or Keyboard</li>
                                            <li>Editing and Deleting Text</li>
                                            <li>Dragging and Dropping Text</li>
                                            <li>Inserting a Symbol or Number</li>
                                            <li>Starting a New Page</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title"> Basic Editing Tasks</h5>
                                        <ul>
                                            <li>Using Cut, Copy, and Paste</li>
                                            <li>Using Undo and Redo</li>
                                            <li>Finding and Replacing Text</li>
                                            <li>Setting Paste Options</li>
                                            <li>Checking Your Spelling</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Basic Formatting Tasks</h5>
                                        <ul>
                                            <li>Understanding Levels of Formatting</li>
                                            <li>Changing Font Face and Size</li>
                                            <li>Changing the Font Color</li>
                                            <li>Highlighting Text</li>
                                            <li>Adding Font Enhancements</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Formatting Paragraphs</h5>
                                        <ul>
                                            <li>Changing Spacing</li>
                                            <li>Setting the Alignment</li>
                                            <li>Using Indents and Tabs</li>
                                            <li>Adding Bullets, Numbering, and Multilevel Lists</li>
                                            <li>Adding Borders and Shading</li>
                                            <li>Using the Paragraph Dialog</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Formatting Tasks</h5>

                                        <ul>
                                            <li>Changing Case</li>
                                            <li>Using the Format Painter</li>
                                            <li>Using the Font Dialog</li>
                                            <li>Clearing Formatting</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Styles</h5><ul>
                                            <li>About Styles</li>
                                            <li>Applying a Style</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Formatting the Page</h5>
                                        <ul>
                                            <li>Formatting Text as Columns</li>
                                            <li>Changing Page Orientation</li>
                                            <li>Changing the Page Color</li>
                                            <li>Adding a Page Border</li>
                                            <li>Adding Headers and Footers</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with the Word Window</h5>
                                        <ul>
                                            <li>Using Zoom</li>
                                            <li>An Overview of Word's Views</li>
                                            <li>Arranging Windows</li>
                                            <li>Splitting a Document</li>
                                            <li>Using the Navigation Pane</li>
                                            <li>Customizing the Ribbon and the Quick Word Toolbar</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Editing and Formatting Tasks</h5>
                                        <ul>
                                            <li>Using the Office Clipboard and the Selection Pane</li>
                                            <li>Using Character Borders and Shading</li>
                                            <li>Enclosing characters</li>
                                            <li>Showing Formatting Marks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Illustrations</h5>
                                        <ul>
                                            <li>Inserting a Picture from a File</li>
                                            <li>Adding WordArt</li>
                                            <li>Drawing Shapes</li>
                                            <li>Inserting a Screenshot</li>
                                            <li>Moving or Deleting a Picture</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Formatting Pictures</h5>
                                        <ul>
                                            <li>Using the Picture tools Tab</li>
                                            <li>Adding a Border</li>
                                            <li>Removing a Picture's Background</li>
                                            <li>Positioning Pictures and Wrapping Text</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Adding Tables</h5>
                                        <ul>
                                            <li>Inserting a Table</li>
                                            <li>Adding Text to a Table</li>
                                            <li>About the Table Tools Tabs</li>
                                            <li>Altering Rows and Columns</li>
                                            <li>Applying a Table Style</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Printing and Sharing your Document</h5>
                                        <ul>
                                            <li>Previewing and Printing Your Document</li>
                                            <li>E-Mailing Your Document</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="MS Word Level 2 - Advanced"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Word Level 2 - Advanced
                                </a>
                            </h3>
                            <p>This hands-on advanced Word class covers  collaborating on documents, managing document versions, table of contents, comments and tracking, mail merges, templates, securing documents and more. Trainees should have completed our Level 1 Introduction course or have similar                         experience prior to attending this course.</p>

                            <div class="group-action-inline">
                                <a href="#" class="btn btn-md btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Word Level 2 - Advanced" data-price="$350">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/word/Word%20Level%202.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <a href="#" class="btn btn-md btn-primary js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-classes="Word Level 2 Advanced">
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with SmartArt</h5>
                                        <ul>
                                            <li>Inserting SmartArt</li>
                                            <li>Adding Text to SmartArt</li>
                                            <li>Using the SmartArt Tools Tabs</li>
                                            <li>Moving and Deleting SmartArt</li>
                                            <li>Using SmartArt Layout and Style Options</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Document References</h5>
                                        <ul>
                                            <li>Inserting a Caption</li>
                                            <li>Adding a Table of Contents</li>
                                            <li>Adding Footnotes, Endnotes, and Citations</li>
                                            <li>Managing Sources</li>
                                            <li>Inserting a Bibliography</li>
                                            <li>Creating an Index</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Reviewing Your Document</h5>
                                        <ul>
                                            <li>Using Define, Thesaurus and Word Count</li>
                                            <li>Setting Proofing Language and Language Preferences</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Using Comments and Tracking</h5>
                                        <ul>
                                            <li>Adding a Comment</li>
                                            <li>Reviewing Comments</li>
                                            <li>Tracking Changes</li>
                                            <li>Reviewing Changes</li>
                                            <li>Comparing Documents</li>
                                            <li>Combining Documents</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Document Information and Word Customization</h5>
                                        <ul>
                                            <li>Setting Word Options</li>
                                            <li>Protecting a Document</li>
                                            <li>Checking for Issues</li>
                                            <li>Managing Versions</li>
                                            <li>Working with Properties</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Reusable Content</h5>
                                        <ul>
                                            <li>Saving Selection as Autotext</li>
                                            <li>Inserting a Quick Part</li>
                                            <li>Creating Customized Building Blocks</li>
                                            <li>Editing a Building Block</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Templates</h5>
                                        <ul>
                                            <li>About Templates</li>
                                            <li>Modifying an Existing Template</li>
                                            <li>Creating a New Template</li>
                                            <li>Applying a Template to an Existing Document</li>
                                            <li>Managing Template Styles</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Sections and Linked Content</h5>
                                        <ul>
                                            <li>Using Sections</li>
                                            <li>Customizing Page Numbers in Sections</li>
                                            <li>Using Multiple Page Formats in a Document</li>
                                            <li>Using Different Headers and Footers in a Document</li>
                                            <li>Linking and Breaking Links for Text Boxes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Managing Versions and Tracking Documents</h5>
                                        <ul>
                                            <li>Merging Different Versions of a Document</li>
                                            <li>Tracking Comments in a Combined Document</li>
                                            <li>Reviewing Comments in a Combined Document</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Mail Merges</h5>
                                        <ul>
                                            <li>Creating a Mail Merge</li>
                                            <li>Sending Personalized Email Messages to Multiple Recipients</li>
                                            <li>Using Other Data Sources for Mail Merge</li>
                                            <li>Creating Labels</li>
                                            <li>Creating Envelope and Label Forms</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Master Documents</h5>
                                        <ul>
                                            <li>Creating a Master Document</li>
                                            <li>Inserting a Subdocument</li>
                                            <li>Creating a Subdocument</li>
                                            <li>Expanding and Collapsing Subdocuments</li>
                                            <li>Unlinking a Subdocument</li>
                                            <li>Merging and Splitting Subdocuments</li>
                                            <li>Locking a Master Document</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Working with Macros</h5>
                                        <ul>
                                            <li>Recording a Macro</li>
                                            <li>Running a Macro</li>
                                            <li>Applying Macro Security</li>
                                            <li>Assigning a Macro to a Button or Key</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>

                    <div class="accordion-item">
                        <article class="post-default">
                            <div class="post-img"><img src="/dist/images/icons/icon-office-word.png" alt="Microsoft Word Level 2 - Advanced"></div>
                            <div class="post-body">
                                <h3 class="post-title">
                                    <a href="#collapse-outline-3"  data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                        Formatting Legal Documents using Microsoft Word
                                    </a>
                                </h3>
                                <p>This specialized Microsoft Word class is aimed at legal professionals.  Trainees will learn how to format documents, prepare a pleading, review  and track changes, work with document references, and improve efficiency  by working faster and smarter in Word.</p>

                                <div class="group-action-inline">
                                    <a href=".section-course-form" class="btn btn-md btn-secondary js-anchor-scroll">
                                        <i class="fas fa-users mr-2"></i>
                                        Obtain a quotation for group training
                                    </a>
                                    <a href="/downloads/word/Formatting%20Legal%20Documents%20using%20Microsoft%20Word%202016.pdf" class="btn btn-md btn-dark" target="_blank">
                                        <i class="far fa-file-pdf mr-2"></i>
                                        Download PDF
                                    </a>
                                    <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                        <i class="fas fa-list-ul mr-2"></i>
                                        Detailed Outline
                                    </button>
                                </div>

                            </div>
                            <div class="post-side">
                                <div class="accordion-action">
                                    <a href="#collapse-outline-3" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                        <span class="c-open"><i class="fas fa-plus"></i></span>
                                        <span class="c-close"><i class="fas fa-times"></i></span>
                                    </a>
                                </div>
                                <div class="price-lbl">
                                    <span>1 Day</span>
                                    <sup></sup>
                                </div>
                            </div>
                        </article>

                        <div id="collapse-outline-3" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                            <div class="post-accordion-collapse">
                                <h3>
                                    <span>Detailed Course Outline</span>
                                    <a href="#collapse-outline-3" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-1">
                                        Hide outline
                                    </a>
                                </h3>

                                <div class="card-columns">
                                    <div class="card card-list">
                                        <div class="card-body copy">
                                            <h5 class="card-title">Lesson 1: Everyday features demystified</h5>
                                            <ul>
                                                <li>General Formatting Tips</li>
                                                <li>Paragraph Formatting</li>
                                                <li>Justification (Alignment)</li>
                                                <li>Using Left, Center and Right Alignment on One Line</li>
                                                <li>Adjusting Line Spacing</li>
                                                <li>Changing Margins</li>
                                                <li>Indenting Paragraphs</li>
                                                <li>Setting and Deleting Tabs</li>
                                                <li>Inserting Footnotes</li>
                                                <li>Working With Headers and Footers</li>
                                                <li>Watermarks</li>
                                                <li>Page Numbering</li>
                                                <li>Creating Bulleted Lists</li>
                                                <li>Font Formatting</li>
                                                <li>Applying Fonts to Entire Documents</li>
                                                <li>Applying Fonts to Certain Parts of a Document</li>
                                                <li>Font Attributes</li>
                                                <li>Changing Case</li>
                                                <li>Inserting Symbols</li>
                                                <li>Copying and Pasting Techniques</li>
                                                <li>Opening PDF files Directly into Word</li>
                                                <li>The Navigation Pane</li>
                                                <li>Using the Traditional “Find” Dialog</li>
                                                <li>Using Paste Special</li>
                                                <li>Spell-Checking</li>
                                                <li>Creating a Custom Dictionary</li>
                                                <li>Advanced Grammar Settings</li>
                                                <li>Sorting Text, Numbers or Dates</li>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="card card-list">
                                        <div class="card-body copy">
                                            <h5 class="card-title">Lesson 2: Working smarter and faster / automating Word</h5>
                                            <ul>
                                                <li>Saving Time by Using Keyboard Shortcuts</li>
                                                <li>Keyboard Shortcuts - Miscellaneous Tips</li>
                                                <li>Using Field Codes</li>
                                                <li>Using Fill-in Fields</li>
                                                <li>Creating a Simple Macro to Print the Current Page</li>
                                                <li>Creating and Using "QuickParts" (Formerly Called AutoText)</li>
                                                <li>Working With Styles</li>
                                                <li>The Style Gallery</li>
                                                <li>Creating Your Own Templates</li>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="card card-list">
                                        <div class="card-body copy">
                                            <h5 class="card-title">Lesson 3: Working with mailings and forms</h5>
                                            <ul>
                                                <li>Mail Merges</li>
                                                <li>Using the Mail Merge Wizard</li>
                                                <li>Form Files</li>
                                                <li>Previewing the Merge</li>
                                                <li>Performing the Merge</li>
                                                <li>Setting up and Printing Labels</li>
                                                <li>Setting up and Printing Envelopes</li>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="card card-list">
                                        <div class="card-body copy">
                                            <h5 class="card-title">Lesson 4: Working with pleadings and contracts</h5>
                                            <ul>
                                                <li>Aligning Text With Pleading Line Numbers</li>
                                                <li>Using Line Breaks (Soft Returns) to Fix Formatting in Pleadings</li>
                                                <li>Working With Tables</li>
                                                <li>Using the Numbering Button</li>
                                                <li>Multilevel Lists</li>
                                                <li>Using the "SEQ" Field for Automatic Numbering in Discovery Headings</li>
                                                <li>Cross-Referencing Exhibit Letters (or Numbers) and Automatic Paragraph Numbers</li>
                                                <li>Creating and Troubleshooting a Table of Contents (TOC)</li>
                                                <li>Marking Citations and Generating a Table Authorities (TOA)</li>
                                                <li>Modifying the TOA Styles</li>
                                                <li>Comparing Documents (Redlining)</li>
                                                <li>Tracking Changes</li>
                                                <li>Removing Metadata with the Documents Inspector</li>
                                                <li>Restricting Editing (Apply a Password for Editing)</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="section-note">
                                    <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>


    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Word training testimonials</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>

                    <h3><span>Word classes rating:</span> 4.8 stars from 452 reviewers </h3>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews">
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I am so happy that three of my biggest areas of feeling absolutely no confidence in have been taught in a way I finally understand. Tabs, indents and mail-merge finally make sense and I'm looking forward to trying them. Even getting stuck in the elevator didn't stop us from learning. I will recommend others to take this class.&quot;</p>
                                <span><strong>Karen Townsend | Hanson Material Services</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Allyncia was a fantastic instructor. Her knowledge of Microsoft Word is extremely deep. Her attention to details and coverage of class examples was impeccable. I highly recommend Allyncia to anyone who is interested becoming more proficient in Microsoft Word.&quot;</p>
                                <span><strong>Darrell Johnson</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;WOW! What an amazing experience. Sandy was a real treat to work with. Her knowledge, experience, and training style was what I would want any future instructor to have. I could not have asked for a better experience and will be sure to recommend her to any one looking to step up their Microsoft Application skills&quot;</p>
                                <span><strong>Alberto Rodriguez | Rust-Oleum Corporation</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;I always enjoy Sandy's training sessions! I was her only student in this class, so I got the bonus of a one on one session with her. It was great as I leaned even more than I normally would. Excellent instuctor :)&quot;</p>
                                <span><strong>Zonda Zschau | American College Of Healthcare Executives</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;The instructor was excellent. He knew the material well and provided great examples throughout. He even sent us away with additional reference materials that are sure to come in handy down the road. Great class. Thank You!&quot;</p>
                                <span><strong>Ryan Collin</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=23" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>

    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that  trainees often benefit from repeating their class. Included in your course price is a FREE Repeat valid for 6 months.  Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Money back guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Word is not for everyone. If you decide that after half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Word Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Word. This can be delivered onsite at your premises, at our training center in the LOOP or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training span</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>

                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" checked><i></i> <h3>Word Level 1 - Introduction</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course"><i></i> <h3>Word Level 2 - Advanced</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

    <div class="section section-faq g-text-word">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Word Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best Word classes in Chicago. Please find answers to some of our most frequently asked Word training related questions below, or contact us should you require any further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5>Is this face-to-face training or a webinar?</h5>
                    <p>Our Word classes are taught by live trainers present in the classroom. This is not a webinar. All our trainers are Word experts and passionate teachers.</p>
                </li>
                <li>
                    <h5>What are the class times?</h5>
                    <p>Our MS Word classes start at 9.00am and finish at approximately 4.30pm. Lunch hour typically starts at 12.15pm.</p>
                </li>
                <li>
                    <h5>Do I need to bring my own computer?</h5>
                    <p>No, we provide the computers at our training center in the Chicago Loop.</p>
                </li>
                <li>
                    <h5>Is this training suitable for Mac users?</h5>
                    <p>Yes, there are barely any differences between Word 2016 and Word 2019 on both platforms. We have a lot of Mac users who attend our classes. You can even bring your Mac laptop to use in class.</p>
                </li>
                <li>
                    <h5>Can you deliver Word training onsite at our location?</h5>
                    <p>Yes, we service the greater Chicago metro including Arlington Heights, Aurora, Bollingbrook, Chicago Loop, Deerfield, Des Plaines, Evanston, Highland Park, Joliet, Naperville, Schaumburg, Skokie, South Chicago and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver Word training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Word training.</p>
                </li>
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-word.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>