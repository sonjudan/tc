<?php
$meta_title = "Creating and Editing Docs in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to open and close documents in Microsoft Word. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating Docs</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating and Opening Documents in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Word lets you begin working in documents in two ways: by typing into a new, blank document, or by opening an existing document. New and Open commands are reached, using the mouse method of navigating, by clicking the File Ribbon Tab first. </p>
                <p>When you open a document, Word places a document that was previously saved to disk in a document window. If another document is already open when you start or open a document, Word opens an additional document window and places the newly opened document on top of the previous one. The document on top becomes the current document. Using the Windows navigation status bar, at the foot of the screen, you can switch between open Word documents (open windows) just like you switch between different programmes.</p>
                <p>For more on <a href="/ms-word-training.php">Word training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-3.jpg" width="230" height="" alt="The File Tab"></p>

                <h4>Navigating Open documents on the Status Bar</h4>
                <p>For each document open, although they are overlaid so only one is visible, there is an icon on the status Bar. If you click the different icons on the status bar you can quickly switch between open documents.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-4.jpg" width="750" height="87" alt="Word Status Bar"></p>
                <p>To open a document, you first need to access the Open dialog box, illustrated in Figure&nbsp;1-3. This dialog box enables you to specify the filename, as well as the name of the folder, or organizational location on your disk, in which the file is saved. <a href="/ms-word-training-los-angeles.php">Small hands-on Word classes</a> in Los Angeles.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-5.jpg" width="750" height="592" alt="Open Dialog Box"></p>

                <h4>Method</h4>
                <h6>To create a new document:</h6>

                <ol>
                    <li>Click the <strong>File Tab Ribbon</strong> and then click the <strong>New</strong> option.</li>
                    <li>
                        <p>This opens the <strong>Available Templates</strong> dialog box.</p>
                        <p>Note: There are other ways to start Word from Windows. If necessary, follow your instructor’s directions to start the programme.</p>
                        <p align="left"><img src="https://www.trainingconnection.com/images/Lessons/word/word-6.jpg" width="700" height="497" alt="MS Word templates"></p>
                    </li>
                    <li>This opens the <strong>Available Templates</strong> dialog box.</li>
                </ol>

                <h6>To open an existing document:</h6>

                <ol>
                    <li>Click the File Tab Ribbon and then click the <strong>Open</strong> button. Select Recent Documents or Computer.</li>
                    <li>In the Open dialog box, in the Look in drop-down list box, make sure the desired folder is displayed.</li>
                    <li>In the Files of type drop-down list, make sure Word Documents is selected.</li>
                    <li>From the filename list box, select the desired file.</li>
                    <li>
                        <p>Choose Open.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-7.jpg" width="750" height="327" alt="List of recent docs in Word"></p>
                    </li>
                </ol>



                <h6>To switch between open documents:</h6>

                <ol>
                    <li>Use the <strong>status bar</strong> to select open documents' individual icons.</li>
                </ol>

                <h4>Exercise</h4>

                <p>In the following exercise, you will create and open documents.</p>
                <ol>
                    <li>Click the <strong>File Ribbon Tab</strong> and then click <strong>New</strong>. [A new document window opens, and a blank document is displayed.<strong> Document2 </strong>appears in the title bar].</li>
                    <li>Click the <strong>File Ribbon Tab</strong> and then click the <strong>Open</strong> button.[The Open dialog box appears].</li>
                    <li>In the <strong>Open</strong> dialog box, in the Look in drop-down list box, make sure the <strong>Data</strong> folder is displayed. [A list of the filenames in the <strong>Data</strong> folder appears in the filename list box].</li>
                    <li>In the Files of type drop-down list, make sure Word Documents is selected. [Filenames of Word documents are displayed in the filename list box].</li>
                    <li>From the filename list box, scroll down and select <strong>Regional Information.</strong></li>
                    <li>Choose Open. [The document opens, and the filename <strong>Regional Information</strong> appears in the title bar].</li>
                    <li>On the Windows status bar click to choose (view) <strong>Document2</strong>. [The document window containing <strong>Document2 </strong>is displayed].</li>
                    <li>On the Windows status bar click to choose (view) <strong>Regional Information</strong>. [The document window containing <strong>Regional Information </strong>is displayed].</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Word Lessons</h4>
                    <ul>
                        <li><a href="/word/lessons/opening-doc.php">Starting Word</a></li>
                        <li><a href="/word/lessons/moving-cursor.php">Moving cursor in Word</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Word training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Word training right across the country. View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">obtain a quote for an onsite word class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>