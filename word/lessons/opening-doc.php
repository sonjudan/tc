<?php
$meta_title = "Starting Microsoft Word Documents | Training Connection";
$meta_description = "This tutorial will teach you how to start using Microsoft Word and learn and includes an introduction to the Word interface. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Starting Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Starting a Microsoft Word Document</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h4>Starting Documents</h4>
                <p>Before you can begin using the many features of Microsoft Word 2013, you need to know several basic skills and concepts. Identifying word processing functions and Word screen components, creating and opening a document, and moving around within a document are the foundations you will build on in this course.</p>

                <p>For more on <a href="/ms-word-training.php">MS Word training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Defining Word Processing</h4>
                <p>Word is a <em>word processing programme</em>. Like a typewriter, a word processing programme is used to create <em>documents</em> such as letters, memos, or reports. As you type on your computer keyboard, text is displayed on your screen and is stored in your computer’s <em>memory</em>, which is its temporary electronic storage area.</p>
                <p>Unlike a typewriter, a word processor enables you to make changes to a document easily, without unnecessary retyping. For example, you can insert and delete words, move paragraphs, and correct spelling errors. As your document develops, you can save the changes you have made on a disk or on another storage medium. When the document is complete, you can save and print it. <a href="/ms-word-training-chicago.php">Click here</a> for details of MS Word training in Chicago.</p>
                <p>Any formatting that can be done on a typewriter, such as setting margins and line spacing or changing tabs, can be duplicated in a word processing programme. Word processing programmes use many of the same or comparable keystrokes as a typewriter, such as pressing <strong>Enter</strong> (Return on a typewriter) to move to a new line, pressing <strong>Tab</strong> to move to the next tab setting, and pressing <strong>Caps Lock</strong> (Shift Lock on a typewriter) to type exclusively in capital letters.</p>
                <p>Word processing provides many additional functions to make document creation much easier than on a typewriter. One of these features is <em>word-wrap</em>, which moves the entire last word of a line down to the next line if it extends beyond the right margin. Word processors also provide automatic reformatting if you change a document’s margins or tabs after text has been entered. Additionally, most word processors provide tools to correct spelling and grammar errors.</p>

                <h4>Starting Word</h4>
                <p>To begin working in Word, start the programme from within the Windows environment or ‘workspace’. You can usually start Word by choosing Microsoft Word from the Start menu’s Programmes submenu.</p>

                <h4>Method</h4>
                <p>To start Word:</p>
                <ol>
                    <li>On the Windows taskbar, choose Start.</li>
                    <li>From the Programs submenu, choose Microsoft Word 2013.</li>
                </ol>
                <p><em>Note:</em> There are other ways to start Word from Windows. If necessary, follow your instructor’s directions to start the programme. <a href="/ms-word-training-los-angeles.php">Microsoft classes in Downtown Los Angeles</a>.</p>

                <h4>Exercise</h4>
                <p>In the following exercise, you will start Word.</p>
                <ol>
                    <li>On the Windows taskbar, choose Start. <em>[The Start menu appears].</em></li>
                    <li>From the Programs submenu, choose Microsoft Word 2013. <em>[Word starts.]</em></li>
                </ol>


                <h4>Identifying Components of the Word Screen</h4>
                <p>When you start working in Word, your monitor displays what appears to be a blank piece of paper on your screen with a blinking black bar on the left. This screen, the<em> document window,</em> is where you enter and manage your document text. Different command interfaces and informational display areas, illustrated in Figure&nbsp;1-1, surround the document window. Each of these items allows you to receive information about, or apply features to, your document.</p>

                <img src="https://www.trainingconnection.com/images/Lessons/word/word-1.jpg" width="750" height="700" alt="Components of Word Screen">

                <p>Microsoft Word wants you to manage, organise, and create things with words. That's the essence of word processing. What you see on the screen, on Word's <em>interface</em>, is designed to make writing an easy and effortless task. The largest portion of the Word screen is for composing text. It's blank and white, just like a fresh sheet of paper. That's where you compose and format your text. </p>
                <p>Surrounding the text-composing area is a bewildering host of goobers. Despite their overwhelming appearance, the things that cling to the Word programme window are there to help you write. The following list offers a quick top-to-bottom explanation of the tabs, toolbars, buttons, and other gizmos you see on the screen. Use Figure 1 for reference.</p>

                <ul>
                    <li><strong>The title bar</strong> lists the document's title or merely Document1 until you give the document a title by saving it to disk. </li>
                    <li><strong>The File Ribbon Tab replaces the traditional File menu of most Windows programmes.</strong> Clicking the File Ribbon Tab displays the File Ribbon Tab menu, a list of commands that deal with files and documents. It's the big round button at the top left of the screen, with four colourful squares in it. </li>
                    <li><strong>Tabs</strong> organise Word's various and sundry commands into groups based on word-processing activities. Tabs appear and disappear depending on what you're doing in Word. </li>
                    <li><strong>Groups and command buttons</strong> help keep commands for the various tabs organised. Each group contains command buttons that do specific things to your text. </li>
                    <li><strong>The Ruler</strong> may or may not be visible. When the Ruler is visible, it helps you set margins and tabs. To show or hide the Ribbon select View Tab then Ruler.</li>
                </ul>
                <p>Below the writing area dwells the status bar. This informative strip of graphical goodness contains trivial information about your document, as well as the following ornaments:</p>
                <ul>
                    <li><strong>Document information</strong> lists optional data specific to your document. </li>
                    <li><strong>The View buttons</strong> specify how the blank page appears in the window. </li>
                    <li><strong>The Zoom slider</strong> sets how large or small your document appears inside the window. </li>
                </ul>
                <p>Don't worry about all these things now. The most important components are shown in Figure 1. What's important now is that you recognise the names of things so that you don't get lost later.</p>
                <ul>
                    <li>The <strong>tabs, groups, and command buttons</strong> change as you take on various activities in Word. Although this may seem disruptive, it's quite handy. </li>
                    <li>You can hide the <strong>Ribbon</strong> if you would rather have more room to write: Right-click anywhere on the Ribbon and choose the Collapse the Ribbon command from the pop-up menu. To restore the Ribbon, right-click any tab and choose the Collapse the Ribbon command again.</li>
                    <li>Another part of the window, not shown in Figure 1, is the <strong>navigation pane.</strong> It shows up when it's needed, to offer more choices, options, or information. </li>
                    <li>The <strong>Windows taskbar</strong>, located at the bottom of the screen, is a part of Windows itself and not Word. However, as you open documents in Word, buttons representing those documents appear on the Windows taskbar. </li>
                </ul>
                <p>Both the Ribbon and Quick Access Bar can be customised.</p>

                <h4>The Word 2013 Ribbon</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-2.jpg" width="750" height="392" alt="MS Word ribbon"></p>

                <p>The three parts of the Ribbon are tabs, groups, and commands. </p>
                <p>There are three basic components to the Ribbon. It's good to know what each one is called so that you understand how to use it.</p>
                <ul>
                    <li><strong>Tabs</strong>. There are ten basic ones across the top. Each represents an activity area.</li>
                    <li><strong>Groups</strong>. Each tab has several groups that show related items together.</li>
                    <li><strong>Commands</strong>. A command is a button, a box to enter information, or a menu.</li>
                </ul>
                <p>Everything on a tab has been carefully selected according to user activities. For example, the Home tab contains all the things you use most often, such as the commands in the Font group for changing text font: <strong>Font, Font Size, Bold, Italic</strong>, and so on.</p>
                <p>The <strong>dialogue launcher</strong> is located at the bottom right of each group when clicked will open a dialogue box, such as font or paragraph (familiar in previous version of the software) and relevant to the tab group.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Word Lessons</h4>
                    <ul>
                        <li><a href="/word/lessons/creating-doc.php">Creating and Opening Docs in Word</a></li>
                        <li><a href="/word/lessons/moving-cursor.php">Moving the Cursor around in Word</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Word training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=23">Word training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Word class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>