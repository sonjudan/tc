<?php
$meta_title = "Sorting Items in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to sorting Lists, Paragraphs and Tables in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Sorting</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Sorting in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In a document, you might need to arrange a list of single line items, a group of multiple line paragraphs, or the rows of items in a table into alphabetical or numerical order. Word lets you <em>sort</em> lines and paragraphs of document text and rows of table information into logically defined sequences.</p>

                <p>Need training in Microsoft Word, we offer onsite training countrywide and <a href="/ms-word-training.php">public Word classes in Chicago and Los Angeles</a>. </p>

                <h4>Sorting Lists and Paragraphs</h4>
                <p>By default, Word sorts the entire active document when you sort standard text. Text is separated into individual sort items at each hard return, and items are sorted by the letter(s) or word(s) that appear at the beginning of each.</p>
                <p>If you want to sort only a portion of the document, such as a list or a group of paragraphs, select the desired text before you begin the sorting process. Then, on the Home tab, in the Paragraph group, click Sort. Word opens the Sort Text dialog box, in which you define the desired sort options. <a href="/ms-word-training-los-angeles.php">MS Word training in LA</a>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/sort-text.jpg" width="520" height="399" alt="Sort Text Dialog Box"></p>

                <h4>Method</h4>

                <p><strong>To sort lists and paragraphs:</strong></p>

                <ol>
                    <li>If desired, select the text to be included in the sort.</li>
                    <li>
                        <p>From the Home tab, in the Paragraph group, choose Sort.</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/word/sort.jpg" width="49" height="49" alt="Sort Button"></p>
                    </li>
                    <li>In the Sort Text dialog box, in the Sort by list box, make sure Paragraphs appears.</li>
                    <li>In the Type list box, make sure Text appears.</li>
                    <li>Select the desired order option button.</li>
                    <li>In the <em>My list has</em> area, select the appropriate header option button.</li>
                    <li>Choose OK.</li>
                </ol>

                <p>See <a href="https://support.microsoft.com/en-us/help/290938/keyboard-shortcuts-for-microsoft-word" target="_blank">Keyboard Shortcuts for Word</a></p>

                <h4>Sorting Tables</h4>
                <p>You can sort rows of table information by any column in the table. You can perform a table sort by a single column in a table very quickly. Place the insertion point in the column you want to sort, then under Table Tools, on the Layout Tab, in the Data group, click Sort. In the Sort Dialog box select Sort Ascending or Sort Descending button, select if your list has a Header row or No Header row, and then click OK. <a href="/ms-word-training-chicago.php">Need Word training in Chicago</a>?</p>
                <p>To sort an entire table, move the pointer over the table until the table move (+) handle appears. Click the table move handle to select the table you want to sort. Under Table Tools, on the Layout tab, in the Data group, click Sort. In the Sort dialog box click the Sort by button and select the column you want to sort by, then select Sort Ascending or Sort Descending button. You must also select if your list has a Header row, or No Header row. When you do this, Word immediately sorts the table by the column you have chosen. <a href="/ms-word-training-los-angeles.php">Microsoft Word training workshop in Los Angeles</a>.</p>

                <h4>Method</h4>

                <h6>To sort a table by a single column:</h6>

                <ol>
                    <li>In the document, select the entire column you want to sort by</li>
                    <li>Under Paragraph, on the Home tab, in the Data group, click Sort </li>
                    <li>Decide if you want to Sort Ascending or Sort Descending</li>
                    <li>In the <em>My list has</em> area, select the Header Row option button if the table contains a header row</li>
                    <li>Choose OK.</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>