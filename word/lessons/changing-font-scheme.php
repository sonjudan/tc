<?php
$meta_title = "Changing Font Schemes in Microsoft Word | Training Connection";
$meta_description = "Learn how to change font schemes in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Changing Font Schemes in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Changing Font Schemes in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Are you looking for Microsoft Word training? Training Connection offer onsite training countrywide as well as local <a href="/ms-word-training.php">Microsoft Word classes</a> in Chicago and Los Angeles.</p>

                <h4>Changing Your Font Scheme</h4>

                <p>Just as you can change the colors used by the applied style set, you can also change its fonts. Click Design → Fonts:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/change-font.jpg" alt="change font in word"></p>

                <p>From this drop-down command there are a variety of font schemes to choose from. Move your cursor over each to see a preview of how they will look once applied to your current document. For this example, click to apply the Franklin Gothic font scheme:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/choose-font-scheme.jpg" alt="select font scheme"></p>

                <p>The new font scheme will now have been applied to your current document:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-scheme-applied.jpg" alt="new font scheme displayed"></p>

                <p>If the default font schemes that are available do no meet your needs, you have the option to create a new one from scratch. Click Design → Fonts → Customize fonts:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/customize-font.jpg" alt="customize font options"></p>

                <p>This action will open the Create New Theme Fonts dialog:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/create-new-theme-fonts.jpg" alt="create new theme font dialog box"></p>


                <p>Using the two drop-down menus provided on this dialog you can choose a font for both headings and body font types. A preview of how the selected fonts will look once chosen is shown on the right-hand side of this dialog. Once you are ready to save the new font scheme, you would type the name of this font into the Name text box and then click Save. In this case, click Cancel to return to your document. <a href="/ms-word-training-chicago.php">Certified Word 2019 classes</a>. </p>
                <h4>Making Changes Permanent</h4>
                <p>After having selected a style set and customized its color and font schemes, you are able to set these settings as the default style settings so that they are automatically used for all new documents. Do this by clicking Design → Set as Default:</p>


                <p><img src="https://www.trainingconnection.com/images/Lessons/word/set-default-style.jpg" alt="save as default style"></p>

                <p>A warning dialog will be displayed that will ask you to confirm your choice to change the default style set and theme settings. Normally if you were to continue you would click Yes, but for this example click No:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/warning-dialog-box.jpg" alt="confirm theme settings"></p>

                <p>Be careful when using this command as there is no reset or undo option. The only way to return to the previous default settings is to select them manually and apply them as the new default.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Microsoft Word training</h4>
                    <p>We deliver Microsoft Word training onsite across the country through our network of national trainers . View our <a href="/testimonials.php?course_id=23">Microsoft Word testimonials</a> page.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>