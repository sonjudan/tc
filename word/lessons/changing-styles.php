<?php
$meta_title = "Changing Styles in Microsoft Word | Training Connection";
$meta_description = "Learn how to change styles in Microsoft Word documents. Training Connection offers MS Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Changing Styles in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Changing Styles in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Do you want to further your Microsoft Word knoweldge? Training Connection offer classroom based <a href="/ms-word-training.php">Microsoft Word courses in Chicago and Los Angeles</a> as well as onsite training countrywide.</p>
                <p>In this article you will learn how to change your style set and your color scheme.</p>
                <h4>Changing Your Style Set</h4>


                <p>In Word 2013 style sets are displayed in a gallery, rather than being hidden in a drop-down command as they were in previous editions. Click to open the Design tab and you will see this gallery displayed in the Document Formatting group on the ribbon:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/change-style-set.jpg" alt="gallery displayed style set"></p>

                <p>Using this gallery you can see a small picture of how these style sets will look once applied. Move your cursor over each of the options presented here to see a preview applied to your document for each item:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/style-preview.jpg" alt="use gallery to preview"></p>

                <p>Once you have found the style set that you would like to apply, click on it. For this example, click the Lines (Simple) style set:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/select-style-set-required.jpg" alt="apply style set"></p>

                <p>The new style set will now have been applied to the current document:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-style-set-applied-to-doc.jpg" alt="new style created"></p>

                <p>Note that while a document can only have one style set applied to it at a time, you can change this set at any time. <a href="/ms-word-training-los-angeles.php">Los Angeles onsite Word training</a>.</p>
                <h4>Changing Your Color Scheme</h4>

                <p>Once you have applied a style set, you do not have to stick with its default colors. Apply a new color scheme to your document by clicking Design → Colors:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/apply-new-color-scheme.jpg" alt="new color scheme options"></p>


                <p>The Colors drop-down command will list a wide variety of color schemes from which to choose from. Move your cursor over them to see a preview of how they will look applied to your document. When you’re ready, click the Paper color scheme to apply it:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/color-drop-down.jpg" alt="color drop down options"></p>

                <p>The new color scheme will now have been applied to your current document:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-color-scheme-applied-doc.jpg" alt="new color scheme displayed"></p>

                <p>Note that this color scheme will now be used for any style set you apply to your document.</p><p>If you do not like any of the default color schemes, create your own by clicking Design → Colors → Customize Colors:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-color-scheme-applied-doc.jpg" alt="new color scheme displayed"></p>
                <p>This will open the Create New Theme Colors dialog, which will allow you to create a custom color scheme:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/create-new-theme-color.jpg" alt="new theme color dialog box"></p>
                <p>As you can see, this dialog includes controls to change the colors used for each component of a color scheme. The color choices you choose will be represented in the Sample area of this dialog. Once you have decided on the colors you would like to you use, you would type a name for this color scheme into the Name field and then click Save.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>We deliver Microsoft Word training onsite across the country to thousands of happy students. View our <a href="/testimonials.php?course_id=23">MS Word student reviews</a> page.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>