<?php
$meta_title = "Tracking Changes in Microsoft Word - Part 2 | Training Connection";
$meta_description = "This tutorial is part 2 of our article teaching you how to track changes in documents in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tracking Changes in MS Word - Part 2</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Tracking Changes in Microsoft Word Part 2</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will learn how to:</p>
                <ul>
                    <li><a href="/word/lessons/tracking-changes.php" rel="prev">Read Part 1 of this article here</a></li>
                    <li>Accept and reject changes</li>
                    <li>Show and hide markup</li>
                </ul>

                <p>Need Microsoft Word training? We offer onsite training countrywide and public <a href="/ms-word-training.php">Word training in Chicago and Los Angeles</a>.</p>

                <h4>Accepting and Rejecting Changes</h4>
                <p>The real strength of markup in Word is the ability to accept and reject changes made to a document. This feature allows you to have others work on your document, but still give you control over any changes that they make.</p>
                <p>To accept or reject a change, you must first select a piece of markup. Do this by clicking Review → Next (in the Changes group):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/track-review.jpg" align="center" alt="reviewing tracked changes in Word"></p>
                <p>With the first change selected, click Review → Accept:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/review-accept.jpg" alt="accept changes" align="center"></p>
                <p>The change that was identified by the markup will now be applied and the next change will be selected. Click Review → Reject to reject this change:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/review-reject.jpg" align="center" alt="reject change"></p>
                <p>The selected change will now be reversed and the next change will automatically be selected:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/next-change.jpg" align="center"></p>
                <p>Note that both the Accept and Reject drop-down menus contain commands to accept or reject all changes at once. You will also find commands to Accept (or Reject) all changes and stop tracking, as well as Accept (or Reject) changes without moving to the next item. <a href="/ms-word-training-chicago.php">MS Word classes in Chicago</a>.</p>

                <h4>Showing and Hiding Markup</h4>
                <p>While markup can be very useful, it can get pretty messy and in the way. The quickest way to hide all markup in a document is to click the vertical line in the margin:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/hide-markup.jpg" alt="hide comment markup" align="center"></p>
                <p>If you would like to specify exactly what kinds of markup are shown, click Review → Show Markup:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/review-show-markup.jpg" align="center"></p>
                <p>This menu presents you with a number of options to choose from:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/menu-markup-options.jpg" align="center"></p>
                <p>The list of items at the top of the menu allows you to show and hide specific types of markup. (If a checkmark is present beside an item, this indicates that it will be shown.) Using the Balloons sub-menu, you can choose to show all revisions in balloons, show them inline, or show only comments and formatting in balloons (the default setting). Near the bottom of this menu, the Specific People option lets you narrow the markup shown by who made the change. <a href="/ms-word-training-los-angeles.php">Word course offered in Los Angeles</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of national trainers we deliver Microsoft Word training onsite across the country. View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">request a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>