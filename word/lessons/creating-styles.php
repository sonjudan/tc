<?php
$meta_title = "Creating Styles in Microsoft Word | Training Connection";
$meta_description = "Learn how to create styles in Microsoft Word documents. Training Connection offers MS Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating Styles in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating Styles in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Training Connection run dedicated classroom based <a href="/ms-word-training.php">MS Word training courses</a> in Chicago and Los Angeles as well as onsite training countrywide. For more infomation call us on 888.815.0604.</p>
                <p>In this article, we will learn how to create new styles from existing text and by using the task pane.</p>

                <h4>Creating Styles from Existing Text</h4>
                <p>One of the quickest and simplest ways to create your own custom style is to base it off of formatting that has been used for existing text in your document. Use your cursor to highlight the title of the current sample document:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/highlight-title-on-document.jpg" alt="highlight title to create style"></p>
                <p>Next, on the Home tab, click the More button () that appears within the lower right-hand corner of the Styles gallery:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/more-button.jpg" alt="click more button"></p>
                <p>From the menu that appears, click Create a Style:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/click-select-style.jpg" alt="click create a style"></p>
                <p>The new style set will now have been applied to the current document:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/create-new-style-from-formatting.jpg" alt="new style formatting"></p>
                <p>In the preview section of this dialog you will see that the new style you are creating is the same as the one that has been applied to the text you currently have selected. If you wanted to modify the attributes of this style you could click the Modify button; in this example that is unnecessary. <a href="/ms-word-training-chicago.php">MS Word classes in the Chicago Loop</a>.</p>
                <p>In the Name text box type “Module 9” and then click OK to create the new style.</p<p><img src="https://www.trainingconnection.com/images/Lessons/word/modify-style-attributes.jpg" alt="select attributes"></p>
                <p>The new style that you have just created will now be listed inside the style gallery:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-style-listed.jpg" alt="style listed in gallery"></p>

                <h4>Creating Styles Using the Task Pan</h4>
                <p>You can also create your own style with the Styles task pane. Open the Styles pane by clicking Home → Styles ():</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/create-own-style.jpg" alt="apply own style"></p>
                <p>At the bottom of the pane click the New Style button:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/click-new-style-on-pane.jpg" alt="pane displayed"></p>
                <p>The “Create New Style from Formatting” dialog will now be displayed:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-style-formatting-dialog.jpg" alt="new formatting displayed"></p>
                <p>As your cursor was placed on the first line of the document, the style information for that text will be displayed. Using the variety of controls found on this dialog you can customize virtually any aspect of your style. For this example, use the color drop-down menu to choose a shade of green:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/customize-style-aspects.jpg" alt="customize aspects available"></p>
                <p>Next, give this new style a name. In the Name text box type “Module 9:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/name-new-style.jpg" alt="apply name to new style"></p>
                <p>Once you’re ready to create the style, click OK to save it:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/click-ok-apply-new-style.jpg" alt="click ok to create new style"></p>
                <p>Your new style will now be listed inside the style gallery:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/new-style-in-gallery.jpg" alt="style now in gallery"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word Reviews</h4>
                    <p>Each year thousands of students receive MS Word training through Training Connection. View our <a href="/testimonials.php?course_id=23">MS Word testimonials</a> page to find out more.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>