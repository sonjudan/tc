<?php
$meta_title = "Modifying Page Breaks in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how manipulate break breaks in Microsoft Word documents. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Page Breaks</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Modifying Page Breaks in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Word automatically determines where each page of a document should begin and end by inserting automatic page breaks. As you create and edit multipage documents, you might find that you need to manipulate the automatic page breaks. You can quickly create a manual (forced) page break by clicking the <strong>Page Break button in the Pages group on the Insert tab</strong>. </p>
                <p>To see more Breaks options, on the <strong>Page Layout tab</strong>, in the Page Setup group click the Breaks button to see a drop down viewing pane of Page Breaks options and definitions (see Figure&nbsp;below). This subject is covered in more detail in Module 3 of the course.</p>
                <p>Manual page breaks override the automatic page breaks created by Word.</p>
                <p>For more details about our <a href="/ms-word-training.php">instructor-led Microsoft Word  classes</a>  in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/page-breaks.jpg" width="300" height="" alt="Cut Copy and Paste Text"></p>
                <p>Page breaks are displayed in different ways on your screen depending on which view is activated. <a href="/ms-word-training-chicago.php">Hands-on MS Word classes in Chicago</a>.</p>
                <p>When your document is displayed in <strong>Print Layout</strong> view, both types of page breaks, automatic and manual, are indicated by the separation of each page in the document window. </p>
                <p>When your document is displayed in <strong>Draft</strong> view, automatic page breaks are indicated by a single horizontal line across the page, and manual page breaks are indicated by a single horizontal line with the words Page Break in the centre of the line. </p>
                <p>If you want to delete a manual page break, display the document in Draft view first. <a href="/ms-word-training-los-angeles.php">Los Angeles MS Word 2019 training</a>.</p>

                <h4>Method to Modify Page Breaks</h4>

                <p><strong>To insert a manual page break using the Break dialog box:</strong></p>
                <ol>
                    <li>Place the insertion point at the desired location.</li>
                    <li>Click the Page Break button in the Pages group on the Insert tab.</li>
                </ol>
                <p><strong>To insert a manual page break using the keyboard:</strong></p>

                <ol>
                    <li>Place the insertion point at the desired location.</li>
                    <li> Press <strong>Ctrl</strong>+<strong>Enter</strong></li>
                </ol>

                <p><strong>To change the view of a document:</strong><strong></strong></p>

                <ol>
                    <li> On the View ribbon tab, choose the desired view.</li>
                </ol>

                <p><strong>To delete a manual page break:</strong></p>

                <ol>
                    <li> Make sure the document appears in Draft view.</li>
                    <li>Place the insertion point on the manual page break.</li>
                    <li>Press <strong>Delete</strong></li>
                </ol>

                <p>Also see <a href="/word/lessons/selecting-text.php">How to Select Text in  Word</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget w-auto">
                    <h4 class="widget-title">Onsite Microsoft  Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. View our <a href="/testimonials.php?course_id=23">word student testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite MS Word training.</a></p>
                </div>
                <div class="widget w-auto">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>