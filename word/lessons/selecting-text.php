<?php
$meta_title = "Selecting Text in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to select text in Microsoft Word documents. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Selecting Text</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Selecting Text in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Before executing many Word commands, you need to highlight, or <em>select</em>, the section of text to which a command applies. When you select text, all punctuation, blank lines, and special characters within the highlighted area are included.</p>
                <p>To select text, drag the mouse pointer over the desired text. When the mouse pointer is positioned over an area that has been selected, the pointer changes from an I-beam to a diagonal left pointing arrow. Alternatively, you can use the keyboard method to select text, which requires placing the insertion point in the desired location and then using the arrow keys to highlight the text. Word also lets you use mouse shortcuts, outlined in Table&nbsp;below, to select text. To deselect selected text, simply click anywhere outside the selected area in your document or press a directional arrow key.</p>
                <p>For more details about our <a href="/ms-word-training.php">informative hands-on Microsoft Word training classes</a>  in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/selecting-text-in-word.jpg" width="600" height="336" alt="Selecting Text in Word"></p>


                <h4>Mouse Text Selection Shortcuts</h4>
                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <tbody>
                    <tr>
                        <td>Double-click a word</td>
                        <td>Selects the word.</td>
                    </tr>
                    <tr>
                        <td>Hold <strong>Ctrl</strong> and click in a sentence</td>
                        <td>Selects the sentence.</td>
                    </tr>
                    <tr>
                        <td>Triple-click in a paragraph</td>
                        <td>Selects the paragraph.</td>
                    </tr>
                    <tr>
                        <td>Move the pointer in the left margin until it changes to a diagonal right pointing arrow, then click</td>
                        <td>Selects the corresponding line.</td>
                    </tr>
                    <tr>
                        <td>Move the pointer in the left margin until it changes to a diagonal right pointing arrow, then drag up or down</td>
                        <td>Selects the corresponding lines.</td>
                    </tr>
                    <tr>
                        <td>Move the pointer in the left margin until it changes to a diagonal right pointing arrow, then triple-click</td>
                        <td>Selects the entire document.</td>
                    </tr>
                    </tbody>
                </table>

                <h4>Method to select text</h4>

                <p><strong>To select text:</strong></p>
                <h6>Keyboard method</h6>
                <ol>
                    <li>Place the insertion point where you want to begin selecting.</li>
                    <li>Press and hold <strong>Shift</strong>.</li>
                    <li> Use the directional arrow keys to highlight the desired text.</li>
                </ol>
                <p><a href="/ms-word-training-chicago.php">Chicago MS Word training classes</a>.</p>
                <h6>Mouse method</h6>
                <ol>
                    <li>Position the mouse pointer where you want to begin selecting.</li>
                    <li> Drag the mouse to highlight the desired text.</li>
                </ol>
                <h6>Mouse shortcut method</h6>
                <ol>
                    <li>Position the mouse pointer over any part of the desired text.</li>
                    <li> Position the mouse pointer over the appropriate location in the left margin until it changes into a diagonal right pointing arrow.</li>
                    <li> Drag or click the mouse button the appropriate number of times.</li>
                </ol>
                <p><a href="/ms-word-training-los-angeles.php">Certified Word training in Los Angeles</a>.</p>
                <h6>To deselect text:</h6>

                <ol>
                    <li>Click anywhere outside the selection.or</li>
                    <li>Press any directional arrow key.</li>
                </ol>

                <p>Also see <a href="/word/lessons/moving-copying-text.php">Moving and Copying Text in Word</a>. </p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft  Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. View our <a href="/testimonials.php?course_id=23">student testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>