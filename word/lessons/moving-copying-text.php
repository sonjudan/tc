<?php
$meta_title = "Moving and Copying Text in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to both copy and paste text in Microsoft Word documents. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Moving and Copying Text</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Moving and Copying Text in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Word enables you to cut and paste blocks of text from one part of a document to another. When you cut a selected portion of text, the text is removed from the document and placed on the Clipboard, a temporary storage area. When you paste text, a copy of the text on the Clipboard is placed into the document. The cut piece of text remains on the Clipboard until another block of text is placed on the Clipboard or until you shut down your computer. As long as the text remains on the clipboard, you can continue to paste the same text in different locations throughout your document. </p>

                <p>For more details about our <a href="/ms-word-training.php">instructor-led MS Word  classes</a>  in Chicago and Los Angeles call us on 888.815.0604.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/copy-text.jpg" width="514" height="347" alt="Cut Copy and Paste Text"></p>

                <p>Word also enables you to copy and paste text from one part of the document to another. When you copy a selected portion of text, a duplicate of the text is placed on the Clipboard, but the text is not removed from the document. As when cutting and pasting text, you can continue to paste the same portion of text throughout the document as long as the text remains on the Clipboard.</p>
                <p>To move text quickly over a short distance, you can select the text and then drag it to the desired location. As you drag, the selection appears to stay in its original position, and the mouse pointer becomes an arrow with a shaded box and insertion point drops down line by line on the page as you drag down (or the reverse if you drag up). The shaded box represents the selected text. You position the insertion point at the desired location in the document. When you release the mouse button, the text moves to its new position. This method is called drag-and-drop editing.</p>
                <p>You can also drag to copy text. To do this, you hold down the <strong>Ctrl</strong> key as you drag the selection to an additional location. <a href="/ms-word-training-chicago.php">Best Microsoft Word training in Chicago</a>.</p>

                <h4>Method to Copy and Paste Text</h4>
                <p><strong>To move text:</strong></p>
                <h6>Cut and paste method</h6>
                <ol>
                    <li>Select the text.</li>
                    <li> On the Clipboard group on the Home tab, click the Cut button. or</li>
                    <li>Right click and choose Cut.</li>
                    <li> Place the insertion point where you want the text to be inserted.</li>
                    <li> On the Clipboard group on the Home tab, click the Paste button.or</li>
                    <li>Right click and choose Paste.</li>
                </ol>

                <h6>Drag-and-drop editing method</h6>

                <ol>
                    <li> Select the text.</li>
                    <li> Drag the selected text, placing the mouse pointer’s insertion point at the desired location.  </li>
                </ol>

                <p><strong>To copy text:</strong></p>
                <h6>Copy and paste method</h6>

                <ol>
                    <li>Select the text.</li>
                    <li>On the Clipboard group on the Home tab, click the Copy button. or</li>
                    <li>Right click and choose Copy.</li>
                    <li> Place the insertion point where you want the text to be inserted.</li>
                    <li>On the Clipboard group on the Home tab, click the Paste button. or</li>
                    <li>Right click and choose Paste.</li>
                </ol>

                <h6>Drag-and-drop editing method</h6>

                <ol>
                    <li>Select the text.</li>
                    <li>Press and hold the <strong>Ctrl</strong> key as you drag the selected text, placing the mouse pointer’s gray line at the desired location.</li>
                </ol>

                <p>Also see <a href="/word/lessons/page-breaks.php">Modifying Page Breaks in Word</a>. </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. View our <a href="/testimonials.php?course_id=23">student testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>