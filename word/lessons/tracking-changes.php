<?php
$meta_title = "Tracking Changes in Microsoft Word - Part 2 | Training Connection";
$meta_description = "This tutorial is part 1 of our article teaching you how to track changes in documents in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tracking Changes in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Tracking Changes in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will learn how to:</p>

                <ul>
                    <li>Track changes made to your document</li>
                    <li>Navigate through tracked changes</li>
                    <li><a href="/word/lessons/tracking-changes-2.php" rel="next">Read Part 2 of this article here</a></li>
                </ul>

                <p>Need training in Microsoft Word? We offer onsite training countrywide and public <a href="/ms-word-training.php">Word training in Chicago and Los Angeles</a>.</p>

                <h4>Tracking Changes</h4>
                <p>When change tracking is enabled, Word will keep track of the changes that are made. This includes text insertions, deletions, new images, and more. To enable change tracking, click Review → Track Changes:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/tracking-changes.jpg" align="center" alt="tracking changes in Word"></p>

                <p>Once Track Changes has been enabled, you can work with the document as normal. However, all of your changes will be recorded and noted. These changes are referred to as <strong>markup</strong>, as they literally mark up your document (as if you were editing it on paper). <a href="/ms-word-training-chicago.php">Chicago MS Word training</a>.</p>
                <p>Delete the first sentence in the sample document (“Before you begin, you must know what you’re branding.”). You will see that markup will appear where you made the changes:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/markup.jpg" align="center"></p>
                <p>Click on this line to display markup details. You will see the changes that were made:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/markup-2.jpg" align="center"></p>
                <p>Click the markup line once again to hide the details. You can disable this feature at any time by clicking the Track Changes button again.</p>
                <p>Save the changes that you have made to the current document and close Microsoft Word 2013. <a href="/ms-word-training-los-angeles.php">LA-based Word training</a>.</p>

                <h4>Navigating Through Tracked Changes</h4>
                <p>As you can see, this document contains some markup. Click on any of the vertical lines to view the details of this markup:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/markup-3.jpg" align="center"></p>
                <p>The details of the markup will be shown. Here is a breakdown of what each type represents:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/markup-details.jpg" align="center"></p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <tbody>
                    <tr>
                        <td style="width:300px">Formatting Changes (1)</td>
                        <td>A line will connect the text to information in the margin about the changes that were made.</td>
                    </tr>
                    <tr>
                        <td>Deletions (2)</td>
                        <td>Deleted text is given a different color and strikethrough effect.</td>
                    </tr>
                    <tr>
                        <td>Insertions (3)</td>
                        <td>Inserted text is underlined and a different color.</td>
                    </tr>
                    <tr>
                        <td>All Changes (4)</td>
                        <td>A vertical line in the left margin will denote changed text somewhere in the adjacent line(s). Clicking on this line will hide or show the details.</td>
                    </tr>
                    </tbody>
                </table>

                <p>Word also offers a method to easily navigate through the markup. Click Review → Next (in the Changes group):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/review-changes.jpg" align="center"></p>

                <p>The first markup in the document will be selected:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/review-changes-2.jpg" align="center"></p>
                <p>Clicking this command again will select the next piece of markup on your document, while clicking Review → Previous will select the previous piece of markup. Once you reach the end of your document, clicking Next will select the first piece of markup on your page once again.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Word training across the country. View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">request a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>