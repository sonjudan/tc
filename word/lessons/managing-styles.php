<?php
$meta_title = "Managing Styles in Microsoft Word | Training Connection";
$meta_description = "Learn how to manage styles in Microsoft Word documents. Training Connection offers MS Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Managing Styles in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Managing Styles in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For classroom based, <a href="/ms-word-training.php">instructor-led Microsoft Word training</a> in Chicago and Los Angeles, call us on 888.815.0604.</p>
                <h4>Managing Styles</h4>
                <p>To manage the various styles that exist within your document, first open the Styles task pane (click Home → Styles []). Next, at the bottom of the pane, click Manage Styles:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/style-task-pane.jpg" alt="manage style task pane"></p>

                <p>The Manage Styles dialog will now be shown:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/manage-styles-dialog.jpg" alt="manage styles displayed"></p>

                <p>This dialog offers numerous ways in which you can edit existing styles within your document as well as any documents based upon the current template. On the Edit tab you are able to modify the attributes of a particular style as well as create whole new ones. <a href="/ms-word-training-los-angeles.php">Best MS Word training in Los Angeles</a>.</p>

                <p>Click to open the Recommend tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/recommend-tab.jpg" alt="open recommend tab"></p>>

                <p>The controls that are located on the Recommend tab allow you to organize and sort the order in which styles appear, as well as whether the style appears in the recommended list.</p>

                <p>Click to open the Restrict tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/restrict-tab.jpg" alt="open restrict tab"></p>

                <p>The controls on this tab allow you to restrict the usage of styles in the document. For example, you can choose to block a user of this document from switching to different style sets.</p>

                <p>Click to open the Set Defaults tab:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/set-default-tab.jpg" alt="open set default tab"></p>
                <p>Here, you can use the controls to set the default paragraph and font settings for the current document. Click Cancel to close this dialog without saving any changes that you have made:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/use-control-set-default.jpg" alt="default setting options"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">MS Word Training Reviews</h4>
                    <p>Find out why thousands of students trust Training Connection to deliver MS Word training each year by reading our <a href="/testimonials.php?course_id=23">MS Word testimonies</a> page.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>