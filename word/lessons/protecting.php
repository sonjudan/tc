<?php
$meta_title = "Protecting documents in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to secure and protect your documents in Microsoft Word. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Protecting Docs</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Protecting a document in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>You can protect a document from being viewed by an unauthorized user, by using a password, which combine upper and lowercase letters, numbers, and symbols. Important note, always use a password that you can remember so that you don't have to write it down.</p>
                <p>We provide <a href="/ms-word-training.php">Microsoft Word training classes in Chicago and Los Angeles</a>, and onsite classes countrywide.</p>

                <h4>Method</h4>
                <ol>
                    <li>Open the file. </li>
                    <li>Click on the File tab</li>
                    <li>
                        <p>Click on Info then on Protect Document</p>
                        <p><img src="https://www.trainingconnection.com/images/Lessons/word/protecting-files.jpg" width="564" height="354" alt="Protecting files in Word"></p>
                    </li>
                    <li>
                        <p>Select Encrypt with Password</p>
                        <img src="https://www.trainingconnection.com/images/Lessons/word/encrypt.jpg" width="344" height="330" alt="Encrypt with Password">
                    </li>
                </ol>

                <p>Create a password to open</p>
                <ol>
                    <ol>
                        <li>In the Encrypt Document box, type a password, and then click OK. </li>
                        <li>In the Reenter Password to box, type the password again, and then click OK. </li>
                    </ol>
                </ol>

                <h4>Protecting a document from unauthorised changes and access</h4>
                <p>You can protect your documents from unauthorised access and changes by creating a password in the same format as seen in the last method. <a href="/ms-word-training-chicago.php">Best MS Word training in Chicago</a>.</p>

                <h4>Method</h4>
                <ol>
                    <li>Open the file</li>
                    <li>Click on the File Tab</li>
                    <li>Click Save As</li>
                    <li>Select General Options from the Tools box</li>
                    <li>In the password to open box, create your password</li>
                    <li>In the password to modify box, create your password</li>
                    <li>
                        <p>If you do not want any accidental changes to be made to your document, you can set it as Read only Recommended. &nbsp;This will ask each user if they want to open the file as read-only.</p>
                        <img src="https://www.trainingconnection.com/images/Lessons/word/general-options.jpg" width="440" height="329" alt="General Options from the Tools box">
                    </li>
                    <li>If a user opens the document as read-only and changes it, that person can save the document only by giving it a different file name. <a href="/ms-word-training-los-angeles.php">Certified Word training</a> in Los Angeles.</li>
                </ol>

                <h4>Restrict Editing Method</h4>
                <ol>
                    <li>On the Review tab, Select Restrict Editing</li>
                    <li>Select Formatting restrictions or Editing restrictions</li>
                    <li>Select Start enforcement.</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. See what our students are <a href="/testimonials.php?course_id=23">saying about our word training</a> or obtain a <a href="/onsite-training.php">quotation for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>