<?php
$meta_title = "Quick Style Gallery and Style Inspector in Microsoft Word | Training Connection";
$meta_description = "Learn how to use the quick style gallery and style inspector in Microsoft Word documents. Training Connection offers MS Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">BTQuick Style Gallery and Style Inspector in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Quick Style Gallery and Style Inspector in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>For instructor delivered <a href="/ms-word-training.php">Microsoft Word classes in Chicago and Los Angeles</a>, call us on 888.815.0604.</p>
                <h4>Quick Style Gallery and the Style Inspector</h4>
                <p>One of the great features about the Quick Style gallery is its customizability; this includes the ability to remove any styles you do not use or need. On the Home tab, remove the Module 9 style from the style gallery by right-clicking on it and clicking Remove from Quick Style Gallery:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/remove-from-style-gallery.jpg" alt="quick style gallery options"></p>

                <p>The selected style will immediately have been removed from the styles gallery:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/deleted-from-gallery.jpg" alt="selected style removed"></p>

                <p>You can also right-click on any style listed within the style gallery to rename it or even add the entire gallery to the Quick Access toolbar. Using the Style task pane you can also add styles to the gallery by right-clicking on the style in question and then clicking Add to Quick Style Gallery. <a href="/ms-word-training-chicago.php">Instructor-led Word classes in Chicago</a>.</p>

                <h4>Using the Style Inspector</h4>
                <p>The Style Inspector lets you see exactly what formatting has been applied to any selected text. This can be very useful if you’ve received a document and you want to see what formatting settings have been used for specific lines of text.</p><p>Start the Style Inspector by first opening the Styles task pane (click Home → Styles []). Next, at the bottom of the pane, click Style Inspector:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/open-style-task-pane.jpg" alt="click style inspector"></p>

                <p>The Style Inspector will open in its own task pane. Inside of it you will see exactly what style has been applied to the currently selected text or the current line your cursor is on:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/preview-style-task-pane.jpg" alt="style applied to selected text"></p>

                <p>As long as the Style Inspector is open you can click anywhere in your document to see its style details. Click on the Writing the Policy line to view its style details:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/style-details-inspection.jpg" alt="inspector details"></p>
                <p><a href="/ms-word-training-los-angeles.php">Small Word classes offered in LA</a>.</p>
                <p>View the formatting details for this text by clicking Reveal Formatting on the Style Inspector pane:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reveal-formatting.jpg" alt="view formatting details"></p>

                <p>Now the Reveal Formatting task pane will open on the right-hand side of the Word 2013 window (you may need to move the Styles pane to prevent overlap):</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reveal-formatting-task-pane" alt="reveal formatting task pane open"></p>
                <p>Inside of this pane all formatting attributes for the current text are organized into different categories. You can click the blue links to open the appropriate dialog to change these attributes.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Microsoft Word Student Testimonials</h4>
                    <p>Training Connection deliver training to thousands of students each year. Find out what our students have to say about the MS Word training, here on the <a href="/testimonials.php?course_id=23">Microsoft Word reviews</a> page.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>