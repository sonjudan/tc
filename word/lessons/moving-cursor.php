<?php
$meta_title = "Placing the Cursor in Microsoft Word | Training Connection";
$meta_description = "This tutorial teaches you how navigate the cursor around in Microsoft Word. This is covered as part of our Level 1 Word Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cursor</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Moving the Cursor Point in Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>The text you type appears wherever the insertion/cursor point is positioned in the current document. Moving the insertion point around the screen is a critical part of creating and editing documents. To move the insertion point around the screen, you can use either the keyboard or the mouse. Table 1-1 lists ways to move the insertion point by using keystrokes.</p> <p>
                    For more on <a href="/ms-word-training.php">Microsoft Word classes</a> in Chicago and Los Angeles call us on 888.815.0604.
                </p>
                <p>To use the mouse to move the insertion point, first position the desired section of text in the document window by using the scroll bars. Then, position the mouse pointer in the desired location and click the left mouse button. If you are using a Microsoft IntelliMouse, you can display and move the insertion point to a different part of the document by using the wheel button.</p>
                <p>Do not use the <strong>Enter</strong> key to move the insertion point unless you want to add hard returns, or blank lines, to your document. If, however, you want to move the insertion point to an area of your document into which you have not yet entered text, you need to press the <strong>Enter</strong> key until the insertion point is positioned in the desired location. <a href="/ms-word-training-chicago.php">Chicago Microsoft Office training</a>.</p>


                <table class="table table-bordered table-dark table-hover table-l2 mb-4">
                    <thead>
                        <tr>
                            <td scope="col"><strong>Keystroke</strong></td>
                            <td scope="col"><strong>Result</strong></td>
                        </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td><strong>Up</strong>, <strong>Down</strong>, <strong>Left</strong>, or <strong>Right Arrow</strong></td>
                        <td>Moves the insertion point to the previous or next line or character.</td>
                    </tr>
                    <tr>
                        <td><strong>Page Up</strong></td>
                        <td>Moves the insertion point up one screen.</td>
                    </tr>
                    <tr>
                        <td><strong>Page Down</strong></td>
                        <td>Moves the insertion point down one screen.</td>
                    </tr>
                    <tr>
                        <td><strong>Home</strong></td>
                        <td>Moves the insertion point to the beginning of the current line.</td>
                    </tr>
                    <tr>
                        <td><strong>End</strong></td>
                        <td>Moves the insertion point to the end of the current line.</td>
                    </tr>
                    <tr>
                        <td><strong>Ctrl+Home</strong></td>
                        <td>Moves the insertion point to the beginning of the document.</td>
                    </tr>
                    <tr>
                        <td><strong>Ctrl+End</strong></td>
                        <td>Moves the insertion point to the end of the document.</td>
                    </tr>
                    <tr>
                        <td><strong>Ctrl+ Page Down</strong></td>
                        <td>Moves the insertion point to the top of the next page.</td>
                    </tr>
                    <tr>
                        <td><strong>Ctrl+Page Up</strong></td>
                        <td>Moves the insertion point to the top of the previous page.</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Table 1-1:</strong> Moving the Insertion Point by Using Keystrokes</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-10.jpg" width="600" height="" alt="Find and Replace Tab"></p>
                <p>You can also use Word’s Find feature to help you move the insertion point. Instead of scrolling or paging through the document to find occurrences of specific text, you can have Word perform the search by defining the desired text on the Find and Replace dialog box’s Find tab. Figure 1-4 illustrates the Find tab. Finding is not limited to letters and numbers; you can search for a phrase, including punctuation and spaces, as well.</p>
                <p>When Word finds the desired text in the document, it highlights the text, suspends the search, and leaves the Find and Replace dialog box open. You can then place the insertion point at the found text, move to the next occurrence of the text, or end the search. If the text cannot be found in the current document, Word displays a message box indicating the text does not appear in the document. <a href="/ms-word-training-los-angeles.php">MS Word 2019 training classes in Los Angeles</a>.</p>
                <p>In Word 2013 the find option also activates the Navigation Pane to search within a documents. </p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-11.jpg" width="600" height="" alt="Replace Tab"></p>
                <p>The Replace option helps to replace the particular word selected or it changes the particular word throughout the document. This feature saves our time in searching the word and replacing one by one.</p>

                <h4>Method</h4>
                <p>To move the insertion point using the keyboard:</p>
                <ol>
                    <li>Press the appropriate directional arrow, key, or keystroke combination.</li>
                </ol>
                <p>To move the insertion point using the mouse:</p>
                <ol>
                    <li>    Position the mouse pointer at the desired location in the document.    </li>
                    <li> Click the left mouse button.</li>
                </ol>
                <p>To move the insertion point using the Find feature:</p>
                <ol>
                    <li> On the Editing group on the Home ribbon tab, choose Find.</li>
                    <li> In the Find and Replace dialog box, make sure the Find tab is selected.</li>
                    <li> On the Find tab, in the Find what text box, type the desired word or phrase.</li>
                    <li> Choose Find Next to find the next occurrence of the text.</li>
                    <li> Repeat step 4 as required.
                        or</li>
                    <li> In the information box, choose OK.
                        or</li>
                    <li> In the Find and Replace dialog box, choose Cancel.</li>
                    <li> If the desired text is found, in the document, click the highlighted text.</li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will move the insertion point.</p>
                <ol>
                    <li>Make sure <strong>Regional Information </strong>is open in the current window</li>
                    <li>Press <strong>Ctrl+End. </strong>[The insertion point moves to the end of the document].</li>
                    <li>Press <strong>Ctrl+Page Up</strong> until you reach the top of the document. [The insertion point moves to the top of each page as you move up through the document.]</li>
                    <li>At the top of the document, position the mouse pointer at the beginning of the first paragraph (beginning with In addition to).</li>
                    <li>Click the mouse button.  [The insertion point is placed at the beginning of the paragraph].</li>
                    <li>Press the <strong>Down Arrow</strong> key as many times as necessary to move to the beginning of the second paragraph (beginning with Syon House). [The insertion point moves to the new location].</li>
                    <li>Press the <strong>Right Arrow</strong> key as many times as necessary to move the insertion point to the beginning of the word park.</li>
                    <li>Press <strong>Home</strong>. [The insertion point moves to the beginning of the line].</li>
                    <li>Press <strong>End</strong>. [The insertion point moves to the end of the line].</li>
                    <li>Press <strong>Page Down</strong>. [The insertion point moves down one screen].</li>
                    <li>Press <strong>Ctrl+Home</strong>. [The insertion point moves down one screen].</li>
                    <li>The Editing group on the Home ribbon tab, choose Find. [The Find and Replace dialog box appears].</li>
                    <li>In the Find and Replace dialog box, make sure the Find tab is selected.</li>
                    <li>On the Find tab, in the Find what text box, type <strong>London.</strong></li>
                    <li>Choose Find Next. [Word locates the first occurrence of the phrase <strong>London </strong>and highlights it in the document].</li>
                    <li>Choose Find Next. [Word locates the next occurrence of the phrase<strong> London </strong>and highlights it in the document.]</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-5 clearfix"> </div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Word Lessons</h4>
                    <ul>
                        <li><a href="/word/lessons/creating-doc.php">Creating and Opening Docs in Word</a></li>
                        <li><a href="/word/lessons/opening-doc.php">Starting Word</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Word training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Word training right across the country. View our <a href="/testimonials.php?course_id=23">Word training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Word class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>