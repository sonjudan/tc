<?php
$meta_title = "Adding Comments in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to add comments to documents in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Comments in Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Adding Comments in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h6>In this Module, we will learn how to:</h6>

                <ul>
                    <li>Insert comments</li>
                    <li>Edit and reply to comments</li>
                    <li>Delete comments</li>
                </ul>

                <p>Need training in Microsoft Word? We offer onsite training countrywide and public <a href="/ms-word-training.php">Word classes in Chicago and Los Angeles</a>.</p>

                <h4>Insert Comments</h4>
                <p>To insert a comment, first select the text that you would like to comment on. (If you want to comment on just one word, you can place your cursor anywhere inside that word.) For this example, select the “Products and Features” heading. Next, click Review → New Comment:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/comments.png" align="center" alt="adding a comment in Word"></p>

                <p>You will then see a balloon appear on the right-hand side of the screen. Inside this balloon, type your comment. For this example, type “Expand?”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/expand.png" align="center" alt="expanding a comment in Word"></p>
                <p>Once you’re done typing your comment, click outside the balloon to continue editing the document. Save the changes that you have made to the current document and close Microsoft Word 2013.</p>

                <h4>Editing and Replying to Comments</h4>
                <p>To begin, open Module 3-2 using Microsoft Word 2013. <a href="/ms-word-training-chicago.php">Word 2013/2016/2019 classes in Chicago</a>.</p>

                <h6>To edit a comment, click inside of it:</h6>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reply.png" align="center" alt="replying to a comment in word"></p>
                <p>Now you can work with the contents of the comment as you see fit. For this example, replace the existing text with “Add more detail:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reply-2.png" align="center" alt="replying to a comment in word step 2"></p>
                <p>Note that you have the ability to edit any comment at any time, including comments you did not create. You also have the option to reply to comments. Click the Reply button in the upper right-hand corner of the comment in the sample document:&nbsp;</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reply-3.png" align="center" alt="replying to a comment in word step 3"></p>
                <p>A new comment will appear below and indented to the right of the original comment. Type “Revised” into this comment: </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/reply-4.png" align="center" alt="replying to a comment in word step 4"></p>

                <h4>Deleting Comments</h4>
                <p>To delete a comment, place your cursor in the commented text or in the comment itself. Try this with the comment in the sample document. Next, click Review → Delete:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/delete-comment.png" align="center" alt="deleting a comment in Microsift Word"></p>
                <p>The comment will now be completely removed from the document. Note that you can also click the Review → Delete drop-down arrow to view more deletion options: </p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/delete-comment-2.png" align="center" alt="deleting a comment in Microsift Word step 2"></p>
                <p>Save the changes that you have made to the current document and close Microsoft Word 2013.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>