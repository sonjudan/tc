<?php
$meta_title = "Working with Multiple Documents in Microsoft Word | Training Connection";
$meta_description = "Learn how to work with multiple documents in Microsoft Word. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working with Multiple Documents in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Working with Multiple Documents in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <h6 class="mb-3">In this Module, we will learn how to:</h6>
                <ul>
                    <li>Work with versions</li>
                    <li>Compare and combine documents</li>
                </ul>

                <p>Need further Microsoft Word training? Training Connections offer onsite training countrywide and public <a href="/ms-word-training.php">Word training classes</a> in Chicago and Los Angeles.</p>
                <h4>Working with Versions</h4>
                <p>As you are working on a document, Microsoft Word will automatically save it. To see these versions, click File → Info → and look in the Versions section:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/word-versions-screen.jpg" align="center" alt="word version screen in Word"></p>
                <p>You can right-click a version to open it, delete it, or compare it with the current version:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/version-screen-right-click.jpg" alt="right click Word versions screenshot" align="center"></p>
                <p>This can be a very handy feature if you make unwanted changes to your document and you find that you cannot reverse them. <a href="/ms-word-training-chicago.php">Small Micosoft Word classes</a>.</p>

                <h4>Comparing Documents</h4>
                <p>To compare documents, first open both of the files that you want to compare. Then, in either of the documents, click Review → Compare → Compare:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/comparing-documents.jpg" align="center" alt="comparing documents screenshot"></p>
                <p>The Compare Documents dialog will now be displayed. Choose the original document and the revised document against which you are comparing it. (You will have the choice of using currently open documents and recent documents. You can also click the Browse icon to choose a document not shown in the menu.)</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/revised-document.jpg" align="center"></p>
                <p>The two documents will now be compared against each other. The results will be displayed in a series of panes. The final, marked up document will be shown in the middle of the Word window:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/compared-documents.jpg" alt="compared documents screenshot" align="center"></p>
                <p>Any changes will be flagged in a similar manner as tracked changes. <a href="/ms-word-training-los-angeles.php">Instructor-led classes on Microsoft Word</a>.</p>
                <p>On the left-hand side, you will see the Reviewing pane (also known as the Revisions pane). This pane will list every difference found between the two documents:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/compare-revisions-panel.jpg" align="center" alt="compare revisions panel in Word"></p>
                <p>On the right-hand side, you will see the two original documents that were compared:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/compared-original-documents.jpg" align="center" alt="compared original documents on right of screen"></p>
                <p>Using the Revisions pane, you can choose to accept or reject specific changes. For example, right-click on the first change listed in the Revisions pane and click Accept Format Change:</p>

                <p>The saved document will now incorporate all of the changes that you accepted. It will list the other changes so that you can accept or reject them later:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/accept-or-reject.jpg" align="center"></p>
                <h4>Combining Documents</h4>

                <p>To combine all revisions from two versions of the same document, you first need to open both of these documents. Next, click Review → Compare → Combine from inside either document:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/combine-from-document.jpg" alt="combine from document screenshot" align="center"></p>
                <p>The Combine Documents dialog will be displayed. Using the provided drop-down menus, choose the two documents that you would like to combine. For this example, select Module 6-3 as the original document and Module 6-3-1 as the revised version. (If you do not see this file listed, click the Browse icon and choose it manually.) Click OK:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/selecting-combined-documents.jpg" align="center"></p>
                <p>A dialog will now be displayed that explains that Word can only store one set of formatting changes in the final merged document. You need to choose which set of formatting changes to keep. Ensure that the first option is selected and click “Continue with Merge:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/combined-document-dialogue-box.jpg" align="center" alt="combined document dialog box"></p>
                <p>The Word window will show the combined document, the Reviewing pane, and the original documents:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/save-as-dialogue.jpg" align="center"></p>
                <p>The template will now be saved and available from the File → New gallery, under the Personal category:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/multiple-documents-view-all.jpg" align="center"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>We deliver Microsoft Word training onsite across the country through our network of national trainers. View our <a href="/testimonials.php?course_id=23">MS Word testimonials</a> or <a href="/onsite-training.php">request a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>