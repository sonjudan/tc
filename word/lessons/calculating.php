<?php
$meta_title = "Calculations in tables in Microsoft Word | Training Connection";
$meta_description = "This tutorial will teach you how to perform calculations in a Microsoft Word table. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Calculations</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Calculating in Tables in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>Word lets you perform calculations on numerical table data and display the results in the table. For example, you can add a row or column of numbers, or you can add, subtract, multiply, or divide the contents of individual cells. Entering calculations into a table, instead of typing in the results, ensures that any changes you make to the table’s data are automatically reflected in the calculated results whenever you update the table.</p>
                <p>We provide <a href="/ms-word-training.php">MS Word training classes in Chicago and Los Angeles</a>, and onsite classes countrywide.</p>
                <p>Word uses the location, or <em>cell reference,</em> of specific cells in the table to perform calculations on their numerical data. In Word tables, columns are identified as A, B, C, and so on from left to right, and rows are identified as 1, 2, 3, and so on from top to bottom. A cell is identified by the letter and number of the column/row intersection at which it appears, as illustrated in table below. For example, the cell reference of the cell located in the second column and second row of a table is B2.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/table-calculation.jpg" width="600" height="" alt="Calucations in Word"></p>

                <h4>Using AutoSum</h4>
                <p>When you insert an <em>AutoSum field </em>into a cell, the sum of the numbers in the column above the cell or in the row to its left is automatically calculated. This enables you to add a row or column of numbers quickly. If the column above and the row to the left of the AutoSum cell both contain numerical data, the sum of the numbers in the column is calculated.</p>

                <p>If a blank cell appears in the summed column or row, AutoSum calculates only the numbers in the cells after the blank cell, not the entire row or column. You can type zeros in blank cells to have AutoSum calculate the entire row or column. If you change the value of one of the summed numbers, you can update the AutoSum field to display the new results. <a href="/ms-word-training-los-angeles.php">Los Angeles Word training workshop</a>.</p>
                <h4>Method</h4>
                <h6>To insert an AutoSum field:</h6>
                <ol>
                    <li>Place the insertion point in the desired cell</li>
                    <li>From the Layout tab, in the Table Tools tab, select the Formula button</li>
                    <li>
                        <h6 class="mb-1">Type one of the following:</h6>
                        <p>=SUM(ABOVE) adds the numbers in the column above the cell you’re in. <br>
                            =SUM(LEFT) adds the numbers in the row to the left of the cell you’re in. <br>
                            =SUM(BELOW) adds the numbers in the column below the cell you’re in. <br>
                            =SUM(RIGHT) adds the numbers in the row to the right of the cell you’re in.</p>
                    </li>
                </ol>

                <h6>To update AutoSum fields:</h6>
                <ol>
                    <li>Select the table to update all AutoSum fields. or</li>
                    <li>Select the AutoSum field cell you want to update.</li>
                    <li>Right Mouse click and Select Update Field Or Press F9</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Group Word training</h4>
                    <p>Through our network of local trainers we deliver onsite Word training right across the country. Read a sample of  <a href="/testimonials.php?course_id=23">Word training testimonials</a> or obtain a <a href="/onsite-training.php">quotation for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>