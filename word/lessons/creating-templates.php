<?php
$meta_title = "Creating Templates in Microsoft Word | Training Connection";
$meta_description = "Learn how to create templates in Microsoft Word documents. Training Connection offers Word Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-word">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/word.php">Word</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating Templates in MS Word</li>
                </ol>
            </nav>

            <div class="page-intro mt-0">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-word.png" alt="Word">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Creating Templates in Microsoft Word</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-4" data-aos="fade-up" data-aos-delay="150">
                <p>In this article, we will learn how to create, save, use, and edit templates.</p>
                <p>Need Microsoft Word training? Training Connection offer onsite training countrywide and public <a href="/ms-word-training.php">Word training</a> in Chicago and Los Angeles.</p>

                <h4>Creating a Template</h4>
                <p>Templates allow you to create a boilerplate document that can be used over and over. For example, if you look at some of the pre-built templates included in Microsoft Word, you will see letters, reports, flyers, and many more document types that you can customize.</p>
                <p> Let’s create a report template. To start, click Insert → Cover Page → Integral:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/insert-cover-page.jpg" align="center" alt="insert cover page in Word"></p>
                <p>Place your cursor on the second page. Click References → Table of Contents → Automatic Table 2:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/table-of-contents.jpg" alt="insert table of contents" align="center"></p>
                <p>Click OK in the dialog that appears:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/dialogue-box.jpg" align="center" alt="dialog box"></p>
                <p>Press Ctrl + Enter to create a new page. Click the Home tab and click the Heading 1 style:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/heading-style.jpg" align="center"></p>
                <p>Type “Main Title:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/main-title.jpg" alt="adding a main title in Word" align="center"></p>

                <p>Press Enter. Click the Heading 2 style on the Home tab:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/heading-2.jpg" align="center" alt="adding a heading 2 in Word"></p>
                <p>Type “Sub Title:”</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/subtitle.jpg" align="center" alt="adding a subtitle in Word"></p>
                <p>Now, click Insert → Footer → Integral:</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/word/insert-footer.jpg" alt="adding a footer in Word" align="center"></p>

                <p>We now have a basic format for a document. Leave the document open and continue to the next topic. <a href="/ms-word-training-chicago.php">Looking for Word training in Chicago?</a></p>
                <h4>Saving a Template</h4>
                <p>After having constructd your template, you need to save it in a special format. Click File → Save As:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/save-as-format.jpg" alt="saving as format in Word" align="center"></p>
                <p>With the Save As category selected, you have the option to save the template to your computer or to your OneDrive account (if you have signed into Office using a Microsoft account). Click the Computer option and then click Browse:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/save-as-options.jpg" align="center"></p>
                <p>The Save As dialog will open. Click the “Save as type” drop-down menu and choose “Word Template:”</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/save-as-type.jpg" align="center"></p>
                <p>When the Word Template option is chosen, the Save As dialog will automatically navigate to the Custom Word Templates folder. Type “Module 1-2 Complete” in the “File name” field and then click Save:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/save-as-dialogue.jpg" align="center"></p>
                <p>The template will now be saved and available from the File → New gallery, under the Personal category:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/word/finding-saved-template.jpg" alt="finding a saved template" align="center"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-word" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Microsoft Word training</h4>
                    <p>We deliver Microsoft Word training onsite across the country through our network of national trainers . View our <a href="/testimonials.php?course_id=23">Word testimonials</a> or <a href="/onsite-training.php">request a quote for onsite Word training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Word courses</h4>
                    <ul>
                        <li><a href="/word/introduction.php">Word Level 1 - Introduction</a><br></li>
                        <li><a href="/word/advanced.php">Word Level 2 - Advanced</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>