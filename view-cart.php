<?php
$meta_title = "Cart | Training Connection";
$meta_description = "";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">


            <div class="section">
                <h4>Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-add-course mb-5">

                    <h5 class="title-l4">Excel Level 1 - Introduction</h5>
                    <div class="card-payment pt-3 pb-3 mb-3">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <label>Location</label>
                                        <select name="" id="" class="form-control mb-2 js-location-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Class</label>
                                            <select name="" id="" class="form-control js-class-select">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label>Student Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label>Student Email</label>
                                        <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3 text-right">
                                <div class="mt-4 l2-inline">
                                    <span class="h-price mr-2">$350.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-green mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

            </div>

            <hr>

            <div class="section">
                <h4>Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-add-course mb-5">

                    <h5 class="title-l4">Excel 2-Level Package</h5>
                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Course</label>
                                            <select name="" id="" class="form-control mb-2 js-course-select">
                                                <option value=""></option>
                                            </select>
                                            <select name="" id="" class="form-control js-course-select">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Location</label>
                                        <select name="" id="" class="form-control mb-2 js-location-select">
                                            <option value=""></option>
                                        </select>
                                        <select name="" id="" class="form-control mb-2 js-location-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Class</label>
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                        </select>
                                        <select name="" id="" class="form-control mb-2 js-class-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-right">

                                <div class="l2-inline">
                                    <span class="h-price mr-2">$600.00</span>
                                    <button class="c-close c-close-inline md"><i class="fas fa-times"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-green mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

            </div>

            <hr>

            <div class="section">
                <h4>Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-add-course mb-5">

                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-md-9">
                                <h5>Excel Level 1 - Introduction</h5>
                                <h6>
                                    <span>Chicago</span> <br>
                                    <span>Class date: Dec 5, 2019</span>
                                </h6>

                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee name*">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Student Email</label>
                                            <input type="text" class="form-control" placeholder="Enter Trainee email* ">
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="col-md-3 text-right">
                                <span class="h-price mr-3">$350.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-green mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div class="section-cart">

                    <h4>Your Cart</h4>

                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Excel 2-Level Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Excel Level 1 – Introduction</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                                </p>
                            </div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Excel Level 2 – Intermediate</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                                </p>
                            </div>
                            <div class="tbl-cell">Dec 5, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$350.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5>Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5>-$100.00</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$600.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#">Continue browsing</a>
                    </div>

                </div>
            </div>

            <hr>


            <div class="section">
                <h4>Your Booking</h4>

                <div class="booking-step">
                    <div class="booking-step-item active">
                        <i>1</i>
                        <span>Add Course</span>
                    </div>
                    <div class="booking-step-item">
                        <i>2</i>
                        <span>Checkout</span>
                    </div>
                    <div class="booking-step-item">
                        <i>3</i>
                        <span>Complete</span>
                    </div>
                </div>

                <div class="section-add-course mb-5">
                    <h5 class="title-l4">Adobe Graphic Design Package</h5>

                    <div class="card-payment mb-3">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Photoshop Fundamentals</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-location-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-class-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Illustrator Fundamentals</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-location-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-class-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>InDesign Fundamentals</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-location-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <select name="" id="" class="form-control js-class-select">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-right">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-green mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Add to Cart</a>
                    </div>
                </div>

                <div class="section-cart">

                    <h4>Your Cart</h4>

                    <div class="tbl tbl-checkout mb-2">

                        <div class="tbl-col tbl-col-thead">
                            <div class="tbl-cell">
                                <h5 class="title-l5">Adobe Graphic Design Package</h5>
                                <button class="c-close md"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Photoshop Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                                </p>
                            </div>
                            <div class="tbl-cell">Dec 5-7, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Illustrator Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                                </p>
                            </div>
                            <div class="tbl-cell">Dec 12-14, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>
                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>InDesign Fundamentals</h5>
                                <p>Paul Bean <br><a href="mailto:paul@paulbean.com">paul@paulbean.com</a>
                                </p>
                            </div>
                            <div class="tbl-cell">Dec 19-21, 2019</div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$1,195.00</span>
                            </div>
                        </div>

                        <div class="tbl-col tbl-col-sub">
                            <div class="tbl-cell">
                                <h5>Discount</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <h5>($1,195.00)</h5>
                            </div>
                        </div>


                        <div class="tbl-col tbl-hr"><div class="tbl-cell"></div></div>

                        <div class="tbl-col">
                            <div class="tbl-cell">
                                <h5>Booking Total</h5>
                            </div>
                            <div class="tbl-cell tbl-cell-last">
                                <span class="h-price">$2,390.00</span>
                            </div>
                        </div>
                    </div>

                    <div class="section-actions">
                        <a class="btn btn-secondary mw-13" href="/checkout.php"><span class="iconify" data-icon="simple-line-icons:basket" data-inline="false"></span> Proceed to Checkout</a>
                        <a class="btn btn-dark mw-13" href="#">Continue browsing</a>
                    </div>

                </div>
            </div>


        </div>

    </main>

    <script>
        $('input[name=paymentOption]').change(function(){
            if($(' #js-payment-option-cc').is(":checked")) {
                $('.form-checkout').addClass('is-selected-cc');
            }else {
                $('.form-checkout').removeClass('is-selected-cc');
            }
            if($(' #js-payment-option-paypal').is(":checked")) {
                $('.form-checkout').addClass('is-selected-paypal');
                $('#checkout-submit').html('<i class="iconify mr-1" data-icon="simple-line-icons:basket" data-inline="false"></i> <span>Checkout with Paypal</span>');
            }else {
                $('.form-checkout').removeClass('is-selected-paypal');
                $('#checkout-submit').html('<i class="iconify mr-1" data-icon="simple-line-icons:calendar" data-inline="false"></i> <span>Place Booking</span>');
            }

            if($(' #js-payment-option-paypal').is(":checked")) {
                $('.form-checkout').addClass('is-selected-paypal');
            }else {
                $('.form-checkout').removeClass('is-selected-paypal');
            }
        });
    </script>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>