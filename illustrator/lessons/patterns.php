<?php
$meta_title = "Creating amazing patterns in Illustrator | Training Connection";
$meta_description = "Learn how to create stylish patterns in Adobe Illustrator";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/illustrator.php">Illustrator</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Patterns</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ai.png" alt="Adobe Illustrator">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Pattern Madness In Adobe Illustrator</h1>
                        <h6>By Kristian Gabriel, Adobe Expert/ACI</h6>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">
                <p>It wasn't that long ago that creating patterns was a tedious affair. There were always little applications that	could speed things up, but most did not give you adequate creative control. Most pattern creation	software was raster or pixel based, which produced some nice patterns, but limited you if you wanted to scale	those patterns. Fast forward to the present and we have some great (and easy) pattern creation features	in Adobe Illustrator. Coupled with the vector power of Adobe, the pattern maker in Illustrator is a	wonderful addition to what is already an amazing application.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/000_Header_pattern.jpg" alt="Creating amazing Patterns in Adobe Illustrator"></p>
                <p>Need to learn Adobe Illustrator? Join one of our small <a class="illustrator" href="/illustrator-training.php">Adobe Certified  Illustrator classes</a>? We trainers are talented graphic designers. Public classes offered in Chicago and Los Angeles. Onsite training available country wide. Obtain a quote for <a href="/onsite-training.php">onsite Illustrator training</a>.</p>
                <h4>Methods To The Pattern Madness in Illustrator</h4>
                <p>There are several methods to create patterns in Illustrator, but the main ones are: the Illustrator Pattern	Maker and manual design. Manual design involves extra work in manipulating and reflecting parts of a	design to create a seamless pattern. This takes a lot of know-how in how different types of patterns work.	Hardcore pattern makers still use these methods today. The other method is, of course, using Illustrator's Pattern Maker which is a full-featured tool that facilitates the pattern making process through easy to use	drop down menus, checkboxes and an amazing way to preview your new patterns. Let's explore	Illustrator's Pattern Maker and give you a foothold on one of the most powerful tools in this vector application.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/001_NewPatternAdded.jpg" alt="Pattern in Swatches panel"></p>
                <h4>Creating Your First Pattern</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/002_CreatePattern.jpg" alt="Pattern project"></p>
                <p>To create a pattern, all you need to do is go to OBJECT>	PATTERN > MAKE to enter Pattern Editing Mode. This will instantly bring up a notification dialogue saying that it has added your pattern to the Swatches panel. This will be a	pattern "placeholder" if you haven't created any graphics yet. <a href="/illustrator-training-chicago.php">Chicago certified Illustrator training</a>.</p>
                <p>The dialogue also tells you that any modifications in the Pattern	Editing Mode will dynamically be added to your pattern. At this point, just hit OK. Your screen should essentially be empty with	a small blue box in the middle of your document and the PATTERN OPTIONS dialogue box with options to tweak your pattern. That small blue box is your pattern. At this point, anything you draw or place in that blue box, will create and	add to that pattern. Everything outside of the blue box is a	repeat of your live pattern and can give a you an idea of how your pattern looks tiled. I just used the Star	Tool to draw two stars with the tile, however, you can use any Shape Tools, Brush Tools, Symbols and	pretty much any tool to contribute to the creation of your tile so have at it!</p>
                <h4>Tweaking Your Pattern</h4>

                <p><img class="" src="https://www.trainingconnection.com/images/Lessons/Illustrator/003_PatternOptions.jpg" alt="Pattern Tile Tool"></p>
                <p>Now that you know how to create a pattern, now it's time to tweak your pattern! To do that, you need to use the Pattern Options panel in Pattern Editing Mode. When you enter Pattern Editing Mode, this panel should automatically pop up. If it doesn't, click DONE in the upper left hand corner of the window and then reopen your pattern in Pattern Editing Mode by Double Clicking on your pattern in the Swatches Panel.</p>
                <p>The Pattern Options panel has many options for tweaking your pattern and ways of tweaking the preview of your tile. Let's step through the different options that will really take your pattern to the next level!</p>
                <p>In the upper part of the panel, there is a button that activates the Pattern Tile Tool. This tool allows you to resize your tile by dragging the corners of your tile. This tool also allows you to manually adjust the spacing if you have chosen to make your tile a brick pattern.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/004_PatternOptions2.jpg" alt="Pattern Options"></p>
                <p>The Name input box allows you to name your pattern, though you can change this at any time in your Swatches panel. The feature is the Tile Type. Tile type gives you five tiling layout options.</p>
                <ul>
                    <li>Grid: This the standard 2-dimension tiling option</li>
                    <li>Brick By Row: This will offset every other line of tiles horizontally</li>
                    <li>Brick By Column: This will offset every other line of tiles vertically</li>
                    <li>Hex By Column: This offsets your tiling layout based on a Hexagonal pattern (by Column/Vertical)</li>
                    <li>Hex by Row: This offsets your tiling layout based on a Hexagonal pattern (by Row/Horizontal)</li>
                </ul>
                <p>Brick Offset affects Brick By Row and Brick By Column and determines by how much tile width and height the centers of tiles in adjacent rows and columns are out of vertical and horizontal alignment.</p>
                <p><a href="/illustrator-training-los-angeles.php">Adobe Illustrator classes in Los Angeles</a>.</p>
                <p>Width and Height is a more precise way of dialling in a specific width and height of your tile.</p>
                <p>Size Tile To Art will resize your tile to fit the size of your artwork.</p>
                <p>Move Tile with Art will move your tile box when you move your graphics content within your pattern.</p>
                <p>H.Spacing/V.Space adjusts the space between rows and columns.</p>
                <p>Overlap determines which tiles appear in front when overlapping.</p>
                <p>Copies will extend the tiling preview area to give you more tiles.</p>

                <p>Dim Copies is another feature that allows you to change the transparency of the preview tiles, allowing you to focus on your primary tile for a better, less confusing layout when designing your tile.</p>
                <p>Show Tile Edge displays a box around the tile.</p>
                <p>Show Swatch Bounds shows the actual portion of the tile that is used to repeat the pattern.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/005_PatternDesign.jpg" alt="Pattern settings"></p>

                <h4>Designing A Simple Pattern</h4>
                <p>As stated earlier, any type of artwork can make up the contents of your pattern. If you are an illustrator you can even turn your hand-drawn patterns and drawings into tiles as well. The options are endless. Let's
                    go ahead a create a simple creative pattern like the one above. Once again, feel free to modify and experiment.</p>
                <h6>Do the following:</h6>
                <ol>
                    <li>Create a new illustrator document</li>
                    <li>Create a perfect circle with fill and stroke (Fill R: 255 G: 0 B: 255, Stroke: R: 251 G: 176 B: 59)</li>
                    <li>Go to Effects > Distort & Transform > Pucker & Bloat. Then, set the slider to -55%</li>
                    <li>Select your artwork and then go to OBJECT > PATTERN > MAKE</li>
                    <li>In the Pattern Editor Mode, set Tile Type to Brick By Row (Brick Offset 1/3)</li>
                </ol>
                <p>That's it! Feel free to copy any additional properties in the picture above. I also added one small circle as you can see in the upper right corner of the tile box. Before you draw an additional element like this, first deselect Size Tile to Art. If you don't, the tile box will keep expanding every time you manipulate the size of your artwork within. Now go and make some cool patterns!</p>
                <p>All images created and composited by Kristian Gabriel using Adobe Stock, Techsmith Snag-It and Adobe Photoshop.</p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Ai" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Illustrator training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Illustrator. We have trained hundreds of students to use Illustrator like a pro. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=7">Illustrator student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Illustrator courses</h4>
                    <ul>
                        <li><a href="/illustrator/fundamentals.php">Illustrator Fundamentals</a></li>
                        <li><a href="/illustrator/advanced.php">Illustrator Advanced</a></li>
                        <li><a href="/illustrator/bootcamp.php">Illustrator Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>