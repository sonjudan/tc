<?php
//$meta_title = "tc";
//$meta_description = "tc";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/illustrator.php">Illustrator</a></li>
                    <li class="breadcrumb-item active" aria-current="page">BTITLE</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ai.png" alt="Adobe Illustrator">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Title</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Ai" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Illustrator training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Illustrator. We have trained hundreds of students to use Illustrator like a pro. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=7">Illustrator student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Illustrator courses</h4>
                    <ul>
                        <li><a href="/illustrator/fundamentals.php">Illustrator Fundamentals</a></li>
                        <li><a href="/illustrator/advanced.php">Illustrator Advanced</a></li>
                        <li><a href="/illustrator/bootcamp.php">Illustrator Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>