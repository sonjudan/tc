<?php
$meta_title = "Creating beautiful curves in Illustrator | Training Connection";
$meta_description = "Learn how to draw smooth curves in Adobe Illustrator";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-Ai">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="/resources/adobe.php">Adobe</a></li>
                    <li class="breadcrumb-item"><a href="/resources/illustrator.php">Illustrator</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Smooth Curves</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-adobe-Ai.png" alt="Adobe Illustrator">
                    </div>

                    <div data-aos="fade-up">
                        <h1>Smooth Moves In Adobe Illustrator</h1>
                        <h5>By Kristian Gabriel, Adobe Expert/ACI</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy" data-aos="fade-up" data-aos-delay="150">

                <p>How many times have you looked at the curves of a logo or professional typography and wondered how they achieved those impossibly smooth curves. If you're a natural born artist and you can sketch or paint, you're already ahead of the game. But, what if you're not, and drawing simple curves seems like a hassle? The following are some simple and powerful ways to keep your lines smooth and beautiful!</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/000_Header.jpg" alt="Smooth curve drawing" width="839" height="536" title="Applying a Drop Shadow Effect"></p>

                <p>Need to learn Adobe Illustrator? Why not join one of our hands-on <a href="/illustrator-training.php">Adobe Illutrator classes</a>? Classes taught by professionals offered monthly  in Chicago and Los Angeles, and our trainers can deliver onsite training  right across the country. Obtain a quote for <a href="/onsite-training.php">onsite Illustrator training</a>.</p>

                <h4>Freehand with Brushes and Pencils</h4>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/001_BrushFreehand.jpg" width="678" height="268" alt="Freehand Brush Tool"></p>

                <p>Typically, when drawing freehand with a mouse, you might get what you see above. An imperfect curved line. Unless you are drawing cracks or grungy stick figures this is typically what you get. It is based on how steady your hand is with a mouse. Most digital artists would use something like a Wacom tablet or a touch screen device with a pen to organically draw a smooth, curved line but if you don't have these things, a mouse is a poor substitute. Luckily, Adobe has thought of this and has built in some amazing features to help you smooth your lines. One such feature is the ability to adjust the <strong>Fidelity</strong> of a tool. Fidelity adjusts your tool's accuracy or exactness. Let us select the <strong>Paintbrush Too</strong>l. Now, to adjust the Paintbrush Tool's fidelity, <strong>Double-Click your Paintbrush Tool icon</strong> in the Tool Panel, so you can bring up your <strong>Paintbrush Tool Options</strong>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/003_BrushwithHighFidelity.jpg" width="678" height="268" alt="Maximizes smoothness option"></p>

                <p>Much, much better! Now if you continue to draw  maximum smoothness will be applied. Remember that if the smoothing is too much, you can always dial back the <strong>"intensity"</strong> using the <strong>Fidelity slider</strong>.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/003_BrushwithHighFidelity.jpg" width="678" height="268" alt="Maximizes smoothness option"></p>



                <p>Once you've drawn or traced your object and have created paths, you can add some interesting brush  styles by going to your <strong>BRUSHES PANEL</strong> and clicking on the brush stroke desired. For more brushes, click  on the Brushes Panel menu in the upper right hand corner and go down to the<strong> Brushes Library</strong>. <a href="/illustrator-training-chicago.php">Chicago Adobe Illustrator classes</a>.</p>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/005_WidthTool.jpg" width="839" height="268" alt="Width tool"></p>

                <p>Additional stylization of your logos and/or lettering can be fine-tuned with tools such as the <strong>WIDTH TOOL</strong>.  The Width Tool allows you to change the thickness of the stroke at different parts of the path segment. Variable stroke widths can greatly enhance a stroke when it is too uniform. Other tools that can help you  manipulate your path artistically are the Warp, Twirl, Pucker, Bloat, Scallop, Crystallize and Wrinkle Tools. <a href="/illustrator-training-los-angeles.php">Los Angeles Adobe Illustrator classes</a>.</p>

                <h4>Alternate Methods to Smoothing Pathways</h4>
                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/006_FreehandPencilTool.jpg" width="839" height="268" alt="Freehand pencil tool"></p>

                <p>To be clear, any path can be smoothed. In most cases, there is more than one way to do it. For example, let’s use the <strong>PENCIL TOOL</strong>. You can usually find this tool by <strong>right-clicking on the SHAPER TOOL</strong> in your Tool Panel and then selecting the tool right below it (this can also be activated by pressing ‘N’ on your keyboard).</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Illustrator/007_SmoothTool.jpg" width="839" height="268" alt="Smooth tool"></p>

                <p>Drawing with the Pencil Tool is always an adventure and unless you have a super steady hand - you might end up hating this tool. Draw a curved line. Now, we can fix this line by adjusting the Pencil Tool's Fidelity, just like we change it on our Paintbrush, but we are going to use a different method. This method is called the <strong>SMOOTH TOOL.</strong> Whether you are using a Brush, Pencil Tool or the Pen Tool the Smooth Tool allows you to smooth a pre-existing path.</p>


                <p>First off, make sure your path is selected. Then, select the Smooth Tool in the same category as your Pencil Tool. It's right below it! Just <strong>right-click on the Shaper Tool or your Pencil Tool</strong> and you will see a "striped" pencil. Select this. </p>

                <p>Then, to alter the intensity of the smoothing - <strong>double-click on the Smooth Tool Icon</strong> in the Tool Panel (as illustrated above) and slide it to the left or right. Now you can use the Smooth Tool to draw across the rough parts of your line and smooth it out by hand!</p>


                <p>Beyond these methods, you can always just get better at drawing with the Pen Tool, which is a  more challenging, but a more precise or "mechanical" way of creating smooth lines. No smoothing needed!</p>

                <h4>Illustrator training reviews</h4>

                <p>Live face-to-face training is the most effective way to learn Adobe Illustrator. We have trained hundreds of students to use Illustrator like a pro. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=7">Illustrator student testimonials</a>. </p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-Ai" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Illustrator training reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Adobe Illustrator. We have trained hundreds of students to use Illustrator like a pro. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=7">Illustrator student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Illustrator courses</h4>
                    <ul>
                        <li><a href="/illustrator/fundamentals.php">Illustrator Fundamentals</a></li>
                        <li><a href="/illustrator/advanced.php">Illustrator Advanced</a></li>
                        <li><a href="/illustrator/bootcamp.php">Illustrator Bootcamp</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>