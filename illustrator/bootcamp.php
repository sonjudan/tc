<?php
$meta_title = "Adobe Illustrator 2019 Bootcamp | Hands-on beginner training";
$meta_description = "1-day Quick start Illustrator class in Chicago and LA. Hands-on learning from Illustrator experts.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="../illustrator-training.php">Illustrator Training</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Bootcamp</li>
                </ol>
            </nav>

            <div class="course-intro">
                <div class="copy intro-copy">
                    <h1 data-aos="fade-up">Illustrator 2020</h1>
                    <h3 data-aos="fade-up" data-aos-delay="50">Bootcamp Training Course</h3>

                    <div data-aos="fade-up" data-aos-delay="100">
                        <p>This is our most comprehensive Illustrator course, taking you from beginner to master in 5 days. Our trainer will take you step-by-step through a series of Real World projects and show you how the best professional designers use Adobe Illustrator in the Real World. You will learn hundred of tips, tricks, techniques and shortcuts so that you master this program. By the end of the week, you'll be designing complex illustrations.</p>
                    </div>

                    <div class="mt-lg-3 mb-lg-2" data-aos="fade-up"  data-aos-delay="150">
                        <h3>What's Included</h3>
                        <ul>
                            <li>Certificate of Course Completion</li>
                            <li>Training Manual</li>
                            <li>FREE Class Repeat</li>
                        </ul>
                    </div>

                    <div class="price-lbl" data-aos="fade-up">
                        <span>5 Days</span>
                        <sup>$</sup>1695<sup>.00</sup>
                    </div>

                    <div class="course-actions" data-aos="fade-up">

                        <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Illustrator Bootcamp" data-price="$1695">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>

                        <div class="btn-group dropup">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                Timetable
                            </button>
                            <div class="dropdown-menu">

                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable" data-toggle="modal" data-timetable="Chicago" data-classes="Illustrator Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable Chicago
                                </a>
                                <a href="#" class="dropdown-item js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Illustrator Bootcamp" >
                                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                                    TimeTable LA
                                </a>
                            </div>
                        </div>

                        <a href="/downloads/illustrator/TC_Illustrator_2019_Bootcamp.pdf" class="btn btn-primary btn-lg" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download
                        </a>
                    </div>
                </div>

                <div class="course-side">
                    <div class="featured-img" data-aos="fade-up" data-aos-delay="250">
                        <img src="../dist/images/courses/cans/adobe-illustrator-advanced.png" alt="Illustrator 2019 - Bootcamp Training Course" class="" width="270">
                    </div>
                    <img src="../dist/images/courses/ribbon-authorised-adobe.jpg" alt="Authorised Training Centre" class="ribbon-authorised" width="200" data-aos="fade-up">
                </div>
            </div>
        </div>

        <div class="section section-intro" style="background-image:url('../dist/images/bg-hero-4.jpg')">
            <div class="container">
                <div class="section-heading d-block" data-aos="fade-up" >
                    <h2>Live face-to-face instructor.</h2>
                    <h4>No minimum class size - all classes guaranteed to run!</h4>
                </div>

                <div data-aos="fade-up" data-aos-delay="100">
                    <p>This course  is ideal for designers, marketers and anyone who is pursuing a career in graphic design. No prior experience of Illustrator is needed. Training available on both Mac and PC.</p>
                    <p>View our full range of <a href="/illustrator-training.php">Adobe Illustrator courses</a>, or see below for the detailed outline for Illustrator Bootcamp.</p>
                </div>

                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Illustrator Bootcamp" data-price="$1695" data-aos="fade-up" data-aos-delay="200">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>

        <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/illustrator/sections/course-outline-bootcamp.php'; ?>

    </main>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>