<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Introduction: Interface & Navigation <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Explore the interface and navigation of Adobe Illustrator - the World's no. 1 vector editing program.</strong></p>
                                    <ul>
                                        <li>What’s new with Illustrator 2020</li>
                                        <li>How Illustrator works with media</li>
                                        <li>Raster vs Vector graphics</li>
                                        <li>Create a New Document</li>
                                        <li>Navigate the Illustrator Interface</li>
                                        <li>The Tool Panel</li>
                                        <li>Working with vector graphics</li>
                                        <li>Working with Workspaces</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Working with Illustrator Shape Tools <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn to work with the Illustrator Shape Tools and Object Transform features to create basic icons for buttons, logos or other professional and personal purposes.</strong></p>
                                    <ul>
                                        <li>Work with Various Shapes & Lines</li>
                                        <li>Transform Shapes</li>
                                        <li>Combine and Manipulate Shapes</li>
                                        <li>Work with Object Transforms</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    The Pen Tool: Tracing & Coloring Artwork <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use the ultimate precision path creation tool in Illustrator…the Pen Tool.</strong></p>
                                    <ul>
                                        <li>Learn about Vector Paths</li>
                                        <li>Learn to use the Pen Tool</li>
                                        <li>Other Path Manipulation Tools</li>
                                        <li>Path simplification in AI 2020</li>
                                    <ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Typography Fundamentals <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn the fundamentals of formatting text using the Character Panel.</strong></p>
                                    <ul>
                                        <li>Get familiar with the Character Panel</li>
                                        <li>Working with various classifications of fonts</li>
                                        <li>Font filtering, searching and installation</li>
                                        <li>Working with spacing properties</li>
                                        <li>Create two basic types of text</li>
                                        <li>Auto Spell-Check 2020</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Event Vector Poster Design <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Combine what you have learned (and learn new techniques) to create an event poster.</strong></p>
                                    <ul>
                                        <li>Properly setting up your project</li>
                                        <li>Practice Creating and Arranging Text</li>
                                        <li>Work with and Modify Vector Graphics</li>
                                        <li>Learn One Way how to Distort Text Creatively</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Exporting a Project for Web & Beyond <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Explore the best way to finish your artwork and export it for websites, social media and beyond.</strong></p>
                                    <ul>
                                        <li>Saving and Archiving Projects</li>
                                        <li>Prepping your Artwork for Export</li>
                                        <li>Export As function</li>
                                        <li>Learn about Save for Web Legacy</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>