<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                    Introduction: Interface & Navigation <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Explore the interface and navigation of Adobe Illustrator - the World's no. 1 vector editing program.</strong></p>
                                    <ul>
                                        <li>What’s new with Illustrator 2020</li>
                                        <li>How Illustrator works with media</li>
                                        <li>Raster vs Vector graphics</li>
                                        <li>Create a New Document</li>
                                        <li>Navigate the Illustrator Interface</li>
                                        <li>The Tool Panel</li>
                                        <li>Working with vector graphics</li>
                                        <li>Working with Workspaces</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Working with Illustrator Shape Tools <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn to work with the Illustrator Shape Tools and Object Transform features to create basic icons for buttons, logos or other professional and personal purposes.</strong></p>
                                    <ul>
                                        <li>Work with Various Shapes & Lines</li>
                                        <li>Transform Shapes</li>
                                        <li>Combine and Manipulate Shapes</li>
                                        <li>Work with Object Transforms</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    The Pen Tool: Tracing & Coloring Artwork <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use the ultimate precision path creation tool in Illustrator…the Pen Tool.</strong></p>
                                    <ul>
                                        <li>Learn about Vector Paths</li>
                                        <li>Learn to use the Pen Tool</li>
                                        <li>Other Path Manipulation Tools</li>
                                        <li>Path simplification in AI 2020</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Typography Fundamentals <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn the fundamentals of formatting text using the Character Panel.</strong></p>
                                    <ul>
                                        <li>Get familiar with the Character Panel</li>
                                        <li>Working with various classifications of fonts</li>
                                        <li>Font filtering, searching and installation</li>
                                        <li>Working with spacing properties</li>
                                        <li>Create two basic types of text</li>
                                        <li>Auto Spell-Check 2020</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Event Vector Poster Design <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Combine what you have learned (and learn new techniques) to create an event poster.</strong></p>
                                    <ul>
                                        <li>Properly setting up your project</li>
                                        <li>Practice Creating and Arranging Text</li>
                                        <li>Work with and Modify Vector Graphics</li>
                                        <li>Learn One Way how to Distort Text Creatively</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Exporting a Project for Web & Beyond <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Explore the best way to finish your artwork and export it for websites, social media and beyond.</strong></p>
                                    <ul>
                                        <li>Saving and Archiving Projects</li>
                                        <li>Prepping your Artwork for Export</li>
                                        <li>Export As function</li>
                                        <li>Learn about Save for Web Legacy</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Compounding Shapes <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn how to combine shapes into a single shape known as compound shape.</strong></p>
                                    <ul>
                                        <li>What are Compound Shapes</li>
                                        <li>Creating Compound Shapes</li>
                                        <li>Use Compound Shapes to Create Complex Art</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Advanced Shape Construction <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Pathfinder Tool to create compound and other types of unique custom shapes for professional projects.</strong></p>
                                    <ul>
                                        <li>Access and Use the Pathfinder Panel</li>
                                        <li>Shape Modes vs Pathfinders</li>
                                        <li>Use the Pathfinder to Create Compound Shapes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    The Pathfinder in Practice <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Harness the power of the Pathfinder Panel and some new techniques to create unique logo art.</strong></p>
                                    <ul>
                                        <li>The Power of Outlining Text</li>
                                        <li>Create vector textures to texturize text and other vector graphics</li>
                                        <li>Logo Workflows</li>
                                        <li>Managing layers in the Layers Panel</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Illustrator Symbols <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Save your art and then distribute it in various ways using Symbols.</strong></p>
                                    <ul>
                                        <li>What are Symbols?</li>
                                        <li>Create Symbols</li>
                                        <li>Use Symbols in your Artwork</li>
                                        <li>Working with Symbol Tools</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    Typography In-Depth <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Delve deeper into the world of typography and explore more panels and text path effects to expand your artistic abilities.</strong></p>
                                    <ul>
                                        <li>Character & Paragraph Panel Properties</li>
                                        <li>Advanced spacing understanding</li>
                                        <li>Add Text inside of Paths</li>
                                        <li>Add Text to Paths</li>
                                        <li>Manipulate Text on Paths</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
                                    The Gate: Feature Film <i></i>
                                </a>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use what you have learned in previous lessons combined with new techniques to create a title for a feature film.</strong></p>
                                    <ul>
                                        <li>Coming up with title concepts</li>
                                        <li>Choose a base font</li>
                                        <li>Making your text trademarkable</li>
                                        <li>Add Texture to a Title in Illustrator</li>
                                        <li>Manipulate Text using erasing and cutting tools</li>
                                        <li>Create a custom shatter effect</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
                                    Color like a Pro <i></i>
                                </a>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Jump into the Illustrator Swatches panel to manage and create color. We will also explore setting up a more convenient color workflow for projects.</strong></p>
                                    <ul>
                                        <li>Learn about the Swatches Panel</li>
                                        <li>Create new Colors and Groups</li>
                                        <li>Learn more efficient Color Workflows</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    Social Media: Design & Workflow <i></i>
                                </a>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use your skills in Illustrator and the power of multiple artboards to create several social posts tailored for the most popular networks.</strong></p>
                                    <ul>
                                        <li>Working across Multiple Artboards</li>
                                        <li>Working with Standard Social Media Sizes for Artwork</li>
                                        <li>Batch Export your Social Media Posts efficiently</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
                                    Dance for Me: Puppet Warp Project <i></i>
                                </a>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Explore one of the newer features known as the Puppet Warp Tool which allows you to articulate vector images in a fluid way.</strong></p>
                                    <ul>
                                        <li>What is Puppet Wrap Tool</li>
                                        <li>What Type of Art is best for the Puppet Wrap Tool</li>
                                        <li>Apply and Use the Puppet Wrap Tool</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                    Tool Exploration <i></i>
                                </a>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use Illustrator's strange WARP tools to enhance text and other graphic elements. </strong></p>
                                    <ul>
                                        <li>Learn about all the Warp Tools</li>
                                        <li>Explore some of the Warp Tool Options for more creative power</li>
                                        <li>Create ripped and torn edges</li>
                                        <li>Re-model graphics like clay</li>
                                        <li>Create decorative florals and other clip art</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
                                    Cross-Application Roundtripping <i></i>
                                </a>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Take art created in the previous lesson and move it to Photoshop to add texture and more.</strong></p>
                                    <ul>
                                        <li>Moving Vector Content to Photoshop</li>
                                        <li>Enhancing Vectors in Photoshop</li>
                                        <li>Sending Information from Photoshop to Illustrator</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse17">
                                    Zombie Party! Multi-page Brochure <i></i>
                                </a>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use all that you've learned as well as learn a few more tricks to create a Zombie Party brochure for print.</strong></p>
                                    <ul>
                                        <li>Setting up a Project for Print</li>
                                        <li>Create and Use Multiple Graphic Elements</li>
                                        <li>Finish and Save your File for Print</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse18">
                                    Faux 3D Buttons - Advanced Design Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Appearance panel to create advanced, layered artwork. </strong></p>
                                    <ul>
                                        <li>Working with the Appearance Panel</li>
                                        <li>Use the Gradients Panel to create faux-ganic textures</li>
                                        <li>Manage Layers and Groups of Layers for Better Design</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">
                                    Infographic Vector Design <i></i>
                                </a>
                            </div>
                            <div id="collapse19" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use Illustrator’s graphing tools and other features to create both 2D and 3D infographics.</strong></p>
                                    <ul>
                                        <li>Working with the Graphing Tools</li>
                                        <li>Get Creative with Gradients to Create Shadow Effects</li>
                                        <li>Work 3D to Enhance Graphs</li>
                                        <li>Create infographic bands and other creative elements</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse20" aria-expanded="true" aria-controls="collapse20">
                                    Advanced Typography-Based Design Work <i></i>
                                </a>
                            </div>
                            <div id="collapse20" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use several tools and techniques to create a Typography-based poster.</strong></p>
                                    <ul>
                                        <li>Work with Warping Text</li>
                                        <li>Use the Appearance Panel to Create Multiple Strokes</li>
                                        <li>Use Tools and Techniques to Change Visibility of Text </li>
                                        <li>Advanced Tracing - Image Trace & Live Paint</li>
                                        <li>Explore Illustrator's powerful tracing feature known as Image Trace to convert raster art into vector art.</li>
                                        <li>ImageTrace and all Image Trace Options</li>
                                        <li>Live Paint and Add Color to Vector Art</li>
                                        <li>Learn about the Types of Art that are better for conversion</li>
                                        <li>Use Photoshop to prep artwork for Illustrator image tracing</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                    Repetitive Art - Patterns & Transformations <i></i>
                                </a>
                            </div>
                            <div id="collapse21" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use Illustrator's amazing Pattern Maker as well as a few hidden features to create amazing repetitive artwork.</strong></p>
                                    <ul>
                                        <li>How to work with the Pattern Maker</li>
                                        <li>Tweaking Pattern Options</li>
                                        <li>Use Anchor Points and Object Transforms to create circular art</li>
                                        <li>Use the Transform Effects</li>
                                        <li>Importing and managing Pattern swatches and more</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse22" aria-expanded="true" aria-controls="collapse22">
                                    Advanced Blending Modes <i></i>
                                </a>
                            </div>
                            <div id="collapse22" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use the infamous Blending Modes to create complex blending effects on your vector images.</strong></p>
                                    <ul>
                                        <li>Learn about Blending Modes</li>
                                        <li>Apply Blending Modes to your Layers</li>
                                        <li>What are the most Popular Blending Modes for design</li>
                                        <li>How to manage blending modes in final artwork</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse23" aria-expanded="true" aria-controls="collapse23">
                                    Text & Graphics: Formatting Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse23" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn advanced methods to combine text and graphics to create an info one-page article.</strong></p>
                                    <ul>
                                        <li>Format and Connect Text Boxes</li>
                                        <li>Wrap Text around a Graphic</li>
                                        <li>Use a Gradient Mesh to Create surreal clip art and Graphics</li>
                                        <li>Use clipping masks to crop artwork</li>
                                        <li>Create useful graphic art to enhance text</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Working with Vector Brushes <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use vector brushes in Illustrator, you will learn how to create them.</strong></p>
                                    <ul>
                                        <li>Use and Define Brushes</li>
                                        <li>Add Brush Art to Existing Paths</li>
                                        <li>The different types of vector brushes</li>
                                        <li>Create Vector Brushes</li>
                                        <li>Managing and editing brushes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Vector 3D Art <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use 3D Effects in Illustrator to create two very different types of 3D vector artwork.</strong></p>
                                    <ul>
                                        <li>Learn about the Extrude & Bevel Effect</li>
                                        <li>Learn about the Revolve Effect</li>
                                        <li>Position your 3D Artwork</li>
                                        <li>Working with Map Art</li>
                                        <li>Light and Shadow</li>
                                        <li>Manipulating highlights</li>
                                        <li>What are Steps?</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Raster & Vector - Mixing Artwork <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to combine Raster and Vector graphic design together and learn how to manage their relationship in Illustrator.</strong></p>
                                    <ul>
                                        <li>Learn more about Raster Graphics in Illustrator</li>
                                        <li>Work with the Mighty Appearance Panel in combination with the Layers Panel</li>
                                        <li>Pro Layering Workflows</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Advanced Color - Conversions & beyond <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn to use some of the most powerful coloring features in Illustrator to alter and convert your artwork.</strong></p>
                                    <ul>
                                        <li>Create Professional Color Combinations</li>
                                        <li>Convert and Alter Colors</li>
                                        <li>Learn how to Reduce Colors in your Artwork</li>
                                        <li>Spot Color conversion and management</li>
                                        <li>Color Rules</li>
                                        <li>Adobe Color</li>
                                        <li>Color Profiles and how they affect your outputs</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Perspective - Design Workflow <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Perspective Grid in Illustrator to create 3D looking perspective vector artwork.</strong></p>
                                    <ul>
                                        <li>Learn about the Perspective Grid</li>
                                        <li>Create a perspective Logo from Scratch</li>
                                        <li>Manipulate Elements on the Grid</li>
                                        <li>Approaching product design</li>
                                        <li>Using multiple artboard for a more efficient workflow</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>