<div class="section section-course-details">
    <div class="container">
        <div class="section-heading" data-aos="fade-up" >
            <h2>Detail course outline</h2>
        </div>

        <div class="course-details" data-aos="fade-up" data-aos-delay="250">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails">
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    Faux 3D Buttons - Advanced Design Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Appearance panel to create advanced, layered artwork. </strong></p>
                                    <li>Working with the Appearance Panel</li>
                                    <li>Use the Gradients Panel to create faux-ganic textures</li>
                                    <li>Manage Layers and Groups of Layers for Better Design</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Infographic Vector Design <i></i>
                                </a>
                            </div>
                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use Illustrator’s graphing tools and other features to create both 2D and 3D infographics.</strong></p>
                                    <li>Working with the Graphing Tools</li>
                                    <li>Get Creative with Gradients to Create Shadow Effects</li>
                                    <li>Work 3D to Enhance Graphs</li>
                                    <li>Create infographic bands and other creative elements</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Advanced Typography-Based Design Work <i></i>
                                </a>
                            </div>
                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use several tools and techniques to create a Typography-based poster.</strong></p>
                                    <li>Work with Warping Text</li>
                                    <li>Use the Appearance Panel to Create Multiple Strokes</li>
                                    <li>Use Tools and Techniques to Change Visibility of Text </li>
                                    <li>Advanced Tracing - Image Trace & Live Paint</li>
                                    <li>Explore Illustrator's powerful tracing feature known as Image Trace to convert raster art into vector art.</li>
                                    <li>Image Trace and all Image Trace Options</li>
                                    <li>Live Paint and Add Color to Vector Art</li>
                                    <li>Learn about the Types of Art that are better for conversion</li>
                                    <li>Use Photoshop to prep artwork for Illustrator image tracing</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Repetitive Art - Patterns & Transformations <i></i>
                                </a>
                            </div>
                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use Illustrator's amazing Pattern Maker as well as a few hidden features to create amazing repetitive artwork.</strong></p>
                                    <li>How to work with the Pattern Maker</li>
                                    <li>Tweaking Pattern Options</li>
                                    <li>Use Anchor Points and Object Transforms to create circular art</li>
                                    <li>Use the Transform Effects</li>
                                    <li>Importing and managing Pattern swatches and more</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                    Advanced Blending Modes <i></i>
                                </a>
                            </div>
                            <div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Use the infamous Blending Modes to create complex blending effects on your vector images.</strong></p>
                                    <li>Learn about Blending Modes</li>
                                    <li>Apply Blending Modes to your Layers</li>
                                    <li>What are the most Popular Blending Modes for design</li>
                                    <li>How to manage blending modes in final artwork</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Text & Graphics: Formatting Techniques <i></i>
                                </a>
                            </div>
                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p><strong>Learn advanced methods to combine text and graphics to create an info one-page article.</strong></p>
                                    <li>Format and Connect Text Boxes</li>
                                    <li>Wrap Text around a Graphic</li>
                                    <li>Use a Gradient Mesh to Create surreal clip art and Graphics</li>
                                    <li>Use clipping masks to crop artwork</li>
                                    <li>Create useful graphic art to enhance text</li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="accordion" id="courseDetails2">
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Working with Vector Brushes <i></i>
                                </a>
                            </div>
                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to use vector brushes in Illustrator, you will learn how to create them.</strong></p>
                                    <li>Use and Define Brushes</li>
                                    <li>Add Brush Art to Existing Paths</li>
                                    <li>The different types of vector brushes</li>
                                    <li>Create Vector Brushes</li>
                                    <li>Managing and editing brushes</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    Vector 3D Art <i></i>
                                </a>
                            </div>
                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use 3D Effects in Illustrator to create two very different types of 3D vector artwork.</strong></p>
                                    <li>Learn about the Extrude & Bevel Effect</li>
                                    <li>Learn about the Revolve Effect</li>
                                    <li>Position your 3D Artwork</li>
                                    <li>Working with Map Art</li>
                                    <li>Light and Shadow</li>
                                    <li>Manipulating highlights</li>
                                    <li>What are Steps?</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
                                    Raster & Vector - Mixing Artwork <i></i>
                                </a>
                            </div>
                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn how to combine Raster and Vector graphic design together and learn how to manage their relationship in Illustrator.</strong></p>
                                    <li>Learn more about Raster Graphics in Illustrator</li>
                                    <li>Work with the Mighty Appearance Panel in combination with the Layers Panel</li>
                                    <li>Pro Layering Workflows</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                    Advanced Color - Conversions & beyond <i></i>
                                </a>
                            </div>
                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Learn to use some of the most powerful coloring features in Illustrator to alter and convert your artwork.</strong></p>
                                    <li>Create Professional Color Combinations</li>
                                    <li>Convert and Alter Colors</li>
                                    <li>Learn how to Reduce Colors in your Artwork</li>
                                    <li>Spot Color conversion and management</li>
                                    <li>Color Rules</li>
                                    <li>Adobe Color</li>
                                    <li>Color Profiles and how they affect your outputs</li>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                    Perspective - Design Workflow <i></i>
                                </a>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p><strong>Use the powerful Perspective Grid in Illustrator to create 3D looking perspective vector artwork.</strong></p>
                                    <li>Learn about the Perspective Grid</li>
                                    <li>Create a perspective Logo from Scratch</li>
                                    <li>Manipulate Elements on the Grid</li>
                                    <li>Approaching product design</li>
                                    <li>Using multiple artboard for a more efficient workflow</li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="card-copy">
            <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
        </div>
    </div>

</div>