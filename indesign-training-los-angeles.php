<?php
$meta_title = "Adobe InDesign 2020 Training Classes in LA | Training Connection";
$meta_description = "Need to learn Adobe InDesign? Our Los Angeles instructor-led classes are still the best way to learn. Call 888.815.0604 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted" >
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/indesign-training.php" itemprop="url">
                    <span itemprop="title">InDesign Training</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-Id.jpg');">
    <div class="container">
        <div class="masterhead-copy">
            <h1 data-aos="fade-up" >InDesign Training</h1>

            <div data-aos="fade-up" data-aos-delay="100">
                <h4>Instructor-led InDesign classes in Los Angeles</h4>
                <p>
                    Live face-to-face Adobe certified InDesign training. <br>
                    This is NOT a webinar!
                </p>
            </div>

            <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll" data-aos="fade-up" data-aos-delay="200">
                <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                Book Course
            </a>
        </div>
    </div>
</div>


<div class="section section-instructors">
    <div class="container">
        <div class="section-heading mb-0">
            <h2 class="mb-4" data-aos="fade-up" >Adobe InDesign Classes</h2>

            <p>Our <strong> InDesign classes are truly unique</strong>, in that we are one of only a few <strong>Adobe Authorized Training Centers </strong> in the country that develops its own courseware. We want our trainees  to feel totally inspired by what they create in class, so our  projects are constantly refreshed and always incorporate the latest  design trends. </p>
            <p>All classes are taught in <strong>brand new state-of-the-art graphics training labs</strong> in Downtown Los Angeles.</p>
            <p> Our instructors will take you <strong>step-by-step through a series of Real World Projects, </strong>which include brochures, newsletters, posters,  corporate reports, advertisements and much more. There are 4 InDesign courses to choose from.</p>
            <p><strong>Don't settle for an average InDesign training experience!</strong> <strong>Sign-up for a Training Connection class today!</strong><br>
              <a href="onsite-training.php">Onsite InDesign training available countrywide</a>.</p>
            <p data-aos="fade-up">&nbsp;</p>

            <span class="line-separator" data-aos="fade-up"></span>

          <h4 data-aos="fade-up">Meet our InDesign Instructors</h4>

            <p data-aos="fade-up">Our Aadobe certified instructors are all  experienced and talented graphic designers. You will be learning InDesign from a pro!</p>
        </div>

        <div data-aos="fade-up">
            <div class="owl-carousel owl-theme owl-instructors instructors-list-block " data-aos="fade-up">
                <div class="list-block">
                    <a href=".modal-instrutor-profile-beverly" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-beverly.jpg')"></i>
                        <span>Beverly</span>
                    </a>
                </div>
                <div class="list-block">
                    <a href=".modal-instrutor-profile-kristian" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-kristian.jpg')"></i>
                        <span>Kristian</span>
                    </a>
                </div>
                <div class="list-block">
                    <a href=".modal-instrutor-profile-chana" data-toggle="modal">
                        <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-chana.jpg')"></i>
                        <span>Chana</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>

<div id="section-book-course" class="section section-training-options">
    <div class="container">
        <div class="section-heading" data-aos="fade-up">
            <h2>Choose a class that fits your needs</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">
            <div class="card-deck card-row-sm card-deck-training">
                <div class="card card-package card-training-plans">
                    <h3 class="card-title">1 Day</h3>
                    <div class="card-body">
                        <div class="card-text">
                            <p>If you're looking for a brief introduction to InDesign, our Quickstart class is perfect</p>
                        </div>
                        <h3 class="card-price">$495</h3>
                        <a href="indesign/introduction.php" class="card-label">
                            <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign">
                            <span>InDesign <br>Quickstart</span>
                        </a>
                    </div>
                    <div class="card-footer">
                        <a href="/downloads/indesign/TC_InDesign_2020_Quickstart.pdf" class="btn btn-dark" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download PDF
                        </a>
                        <a href="#" class="btn btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="InDesign Quickstart">
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable
                        </a>
                        <a href="#" class="btn btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Quickstart" data-price="$495">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>
                    </div>
                </div>
                <div class="card card-package card-training-plans">
                    <h3 class="card-title">3 Days</h3>
                    <div class="card-body">

                        <div class="card-text">
                            <p>Fundamentals, our Beginner to Intermediate InDesign class</p>
                        </div>
                        <h3 class="card-price">$1195</h3>
                        <a href="/indesign/fundamentals.php" class="card-label">
                            <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign">
                            <span>InDesign <br>Fundamentals</span>
                        </a>
                    </div>
                    <div class="card-footer">
                        <a href="/downloads/indesign/TC_InDesign_2020_Fundamentals.pdf" class="btn btn-dark" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download PDF
                        </a>
                        <a href="#" class="btn btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="InDesign Fundamentals">
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable
                        </a>
                        <a href="#" class="btn btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Fundamentals" data-price="$1195">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>
                    </div>
                </div>
                <div class="card card-package card-training-plans">
                    <h3 class="card-title">2 Days</h3>
                    <div class="card-body">

                        <div class="card-text">
                            <p>Advanced InDesign training for more  experienced users </p>
                        </div>
                        <h3 class="card-price">$895</h3>
                        <a href="/indesign/advanced.php" class="card-label">
                            <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign">
                            <span>InDesign <br>Advanced</span>
                        </a>
                    </div>
                    <div class="card-footer">
                        <a href="/downloads/indesign/TC_InDesign_2020_Advanced.pdf" class="btn btn-dark" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download PDF
                        </a>
                        <a href="#" class="btn btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-classes="InDesign Advanced">
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable
                        </a>
                        <a href="#" class="btn btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Advanced" data-price="$895">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>
                    </div>
                </div>
                <div class="card card-package card-training-plans">
                    <h3 class="card-title">5 Days</h3>
                    <div class="card-body">

                        <div class="card-text">
                            <p>And our most popular class our 5-day Zero to Hero Bootcamp</p>
                        </div>
                        <h3 class="card-price">$1695</h3>

                        <a href="/indesign/bootcamp.php" class="card-label">
                            <img src="/dist/images/icons/adobe-Id.svg" alt="InDesign">
                            <span>InDesign <br>Bootcamp</span>
                        </a>
                    </div>
                    <div class="card-footer">
                        <a href="/downloads/indesign/TC_InDesign_2020_Bootcamp.pdf" class="btn btn-dark" target="_blank">
                            <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                            Download PDF
                        </a>
                        <a href="#" class="btn btn-primary js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="" data-classes="InDesign Bootcamp">
                            <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                            TimeTable
                        </a>
                        <a href="#" class="btn btn-secondary js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="InDesign Bootcamp" data-price="$1695">
                            <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                            Book Course
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-package-deals.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/Id/section-class-porfolio.php'; ?>

<div class="section section-methodology ">
    <div class="container">
        <div class="section-heading align-left mb-0">
            <h2 class="mb-4" data-aos="fade-up">Our Training Methodology </h2>
        </div>

        <div class="section-body copy" data-aos="fade-up" data-aos-delay="200">
            <p data-aos="fade-up" data-aos-delay="150">We don't just want you to learn InDesign, <strong>we want you to leave our classes feeling fully  inspired.</strong></p>
            <img src="/dist/images/notebook-Id.png" alt="InDesign CC 2019 - Fundamentals Course Book" class="pull-right">
            <p data-aos="fade-up" data-aos-delay="150">We are one of a few training companies that develops its own unique courseware. We do this so that our materials are always up to date with the <strong>newest features in Creative Cloud</strong> and the very <strong>latest design trends.</strong></p>
            <p>The training is based on a series of Real World Projects, so you will learn:</p>
            <ul>
              <li>How the various tools and panels work together.</li>
              <li> All the important concepts such as typography, text flow, page layout, adding images, adding graphics and effects, multi-page layouts, publishing and more.</li>
              <li>Proper and efficient workflow.</li>
              <li>Shortcuts to speed up production.</li>
              <li>And great design techniques.</li>
            </ul>
            <p>Upon class completion, students will receive an Adobe Certificate of Course Completion.</p>
        </div>
    </div>
</div>



<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/Id/section-reviews.php'; ?>

<div class="section section-guarantee">
    <div class="container container-sm">
        <div class="section-heading">
            <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
            <p data-aos="fade-up" data-aos-delay="150">We recognize that InDesign is a complex program, and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months. Often the repeat class can be with a different trainer too. <br>
            </p>
        </div>

        <div class="section-body">
            <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Class guarantee">
            <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
            <p data-aos="fade-up" data-aos-delay="250">InDesign is not for everyone. If you decide that after the first day in class (or by lunchtime on the Quickstart classes) that the software is not for you, you can leave the class and we will give you a complete refund.</p>
        </div>
    </div>
</div>


<div id="section-course-form" class="section section-course-form">
    <div class="container">
        <div class="section-heading w-auto">
            <h2 class="mb-3" data-aos="fade-up">Group InDesign Training </h2>
            <p data-aos="fade-up" data-aos-delay="100">
                We offer group training in InDesign. This can be delivered onsite at your premises, at our training center in Los Angeles or via webinar.

                <br>
                <strong>Fill out the form below to receive pricing.</strong>
            </p>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="200">
            <form action="" class="form-group-training">

                <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                        <h3><input type="radio" name="training">
                            <i></i> Online Webinar</h3>

                    </label>
                </div>



                <div class="row">
                    <div class="col-md-6 pr-1 mb-3">
                        <h4>Step 2: Choose your Course/s</h4>

                        <div class="option-group-training inline-block">
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course"><i></i> <h3>InDesign Quick start <span>1 Day</span></h3>
                            </label>
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>InDesign Fundamentals <span>3 Days</span></h3>
                            </label>
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>InDesign Advanced <span>2 Days</span></h3>
                            </label>
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>InDesign Bootcamp <span>5 DAYS</span></h3>
                            </label>
                            <label class="custom-checkbox">
                                <input type="checkbox" name="course" ><i></i> <h3>Custom InDesign Course</h3>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Step 3: Enter Details</h4>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="No. of Trainees*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Company Name*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone no*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="City*">
                        </div>

                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Zip*">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>

<div class="section section-faq">
    <div class="container">

        <div class="section-heading mb-4">
            <h2 class="section-title">FAQ</h2>
        </div>

        <div class="card-columns card-faq-list">
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Are your classes suitable for Mac users?
                </h4>
                <div class="card-body">
                    <p>Yes, students are able to train on either a Mac or PC, with our trainers being able to provide tuition for both operating systems.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Do your InDesign classes have a live trainer in the classroom?
                </h4>

                <div class="card-body">
                    <p>Yes. Our Adobe InDesign classes are  taught by  qualified and passionate trainers present in the classroom.  They are on hand to provide support, demonstrate concepts, and answer any questions you may have to assist your learning.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Where do your Adobe InDesign training classes take place?
                </h4>
                <div class="card-body">
                    <p>Our training center is located at 915 Wilshire Blvd, Suite 1800, Los Angeles CA 90017.
                        For more information about directions, parking, trains please <a href="/contact-us-los-angeles.php">click here</a>.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    What are the class times?</h4>
                <div class="card-body">
                    <p>Our InDesign classes start at 9.00am and finish at 4.30pm. Lunch break is taken at 12.15pm. There are lots of eateries within walking distance or we have kitchen facilities (microwave &amp; fridge) should you prefer to bring your own lunch.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    What version of InDesign do you train on?</h4>
                <div class="card-body">
                    <p>All our classes are run on the latest version of InDesign Creative Cloud.</p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>Can you deliver InDesign training onsite at our location?</h4>
                <div class="card-body">
                    <p>Yes, we service the greater Los Angeles metro including Anaheim, Burbank, Covina, Downtown, Fullerton, Irvine, Long Beach, Northridge, Pasadena, San Bernardino, Santa Monica, Van Nuys, Ventura and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver InDesign training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite InDesign training.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/Id/section-resources.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-beverly.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-kristian.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-chana.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/schema-courses.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>
