<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Dreamweaver Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our Dreamweaver training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/dreamweaver-training.php">Dreamweaver classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>429 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelly Manning</span>
                    <b>|</b>
                    <span class="company">Abbott</span>
                </h3>
                <p itemprop="reviewBody">I think Gabe did a fantastic job teaching the course.  In only three days, with very little experience, I feel comfortable using Dreamweaver on my own. I also though the class was fun because we worked on popular company's web sites. I would definitly recommend this class to others. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Julie Heidt</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed this class and my instructor fantastic. The class provided me with a solid foundation for understanding dreamweaver and web design! Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.3</h3>

                    <div class="star-rate star-rate-sm star-rate-4_3">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Elaine Hyland</span>
                    <b>|</b>
                    <span class="company">InterMedius</span>
                </h3>
                <p itemprop="reviewBody">Gabe is an excellent teacher.  He keeps the class engaged and makes building sites simple. Gabe's technique of getting the class to copy the layout of a site really helps the student to see what areas they are strong or weak in. Please have Gabe teach a Dreamweaver advanced class I would definitely attend.  Well worth the money!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tamela Box</span>
                    <b>|</b>
                    <span class="company">GrowTech, Inc.</span>
                </h3>
                <p itemprop="reviewBody">This course surpassed my expectations! I learned an invaluable amount of information in three days; more than I've learned on my own in the last 4 years!  Gabriel was an excellent teacher.  He explained everything so effortlessly, was extremely patient with everyone in class, and had enrichment exercises ready for those who were ahead of the rest of the group.  I hope he teaches the advanced course.  Gabriel is  brilliant, muscular, and clearly talented. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Drew Weinstein</span>
                </h3>
                <p itemprop="reviewBody">Gabriel is an amazing teacher.  I learned more from him than I did the last time that I took the course.  He was patient, funny, and had a good sense of humor.  He did some cool little magic tricks with cards that were pretty awesome.  He is like the Harry potter, or Dumbledore of dreamweaver.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carrie Dolan</span>
                    <b>|</b>
                    <span class="company">Hamilton Circulation Supplies</span>
                </h3>
                <p itemprop="reviewBody">This course was great! I was so nervous my first day because I had never made a website before, but Gabriel was an incrediby helpful and patient teacher - I would recommend this course to anyone. All of the staff was so friendly! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Daniel Byers</span>
                    <b>|</b>
                    <span class="company">Illinois State Geological Survey</span>
                </h3>
                <p itemprop="reviewBody">Ron did a great job making sure fundamental concepts were understood before moving on to the next topic and still kept the pace moving to cover all the scheduled topics within the time allotted. I appreciated also the order of the information which filled in holes in my understanding of fundamentals prior to taking the class. His scope of understanding of the topics extends past the software we were working with which was a fantastic learning opportunity.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sue Doebler</span>
                    <b>|</b>
                    <span class="company">SLD Web Designs.com</span>
                </h3>
                <p itemprop="reviewBody">Gabe was charming, and obviously talented,. In fact, I would say, a genius of sorts. He seemed modest about his skills, and I would highly recommend him for featured "Instructor Spotlight" award on your web site. He is extremely knowledgeable in his field, and patient with those that had questions regarding the material covered.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kirstin Vizthum</span>
                </h3>
                <p itemprop="reviewBody">Excellent class. Practical exercises. Focused lectures. Great facilities. I'm leaving here way more confident in DreamWeaver than I thought possible in just 3 days!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Steven Mancione</span>
                    <b>|</b>
                    <span class="company">Silver Hammer</span>
                </h3>
                <p itemprop="reviewBody">This is the first time I actually looked forward to coming to class.  Gabriel was very patient with everyone and still kept the class moving at a comfortable pace. I have a great confidence leaving here that I have acquired a fantastic skill and will strongly consider coming back for additional classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Matt Bouchard</span>
                    <b>|</b>
                    <span class="company">Institute For Transfusion Medicine</span>
                </h3>
                <p itemprop="reviewBody">Gabriel was fantastic. Sometimes you'll attend a workshop hoping to walk away with a greater knowledge base and you'll be disappointed. This was the exact opposite. I learned way more than I expected i would and Gabriel's teaching style really helps. I would recommend this to anyone looking to learn dreamweaver and i feel very comfortable with the programming now. Thanks Gabriel!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Thomas Felgar</span>
                    <b>|</b>
                    <span class="company">American Bar Association</span>
                </h3>
                <p itemprop="reviewBody">The information that was required for me to effectively do my work in Dreamweaver was covered appropriately.  Additionally, the instructor took time to answer questions that may not normally have been part of the class and provided examples.  I thought he had a wealth of knowledge and the course was very valuable in helping me to do my job and has gotten me interested in further pursuing web design topics.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mindy Meyer</span>
                    <b>|</b>
                    <span class="company">Abbott Labs</span>
                </h3>
                <p itemprop="reviewBody">He was awesome!  Even though he told us to say this, it is true!  I have taken classes with other schools, I like the fact he works for a real company and also takes time to teach here...Rather than just teaching.  The actual hands on learning I find the best and having a full day dedicated to that was amazing.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mark Loewenstein</span>
                    <b>|</b>
                    <span class="company">Loewy Consulting</span>
                </h3>
                <p itemprop="reviewBody">This was awesome.  Gabriel was patient, clear on what he taught, and best of all he was responsive to each students' questions and  problems.  He explained things in an organized, concise fashion that made pretty much everything easy to understand.  The course was pretty much what I expected and more.  It gave me a whole new outlook on how to approach building websites after 10 years of doing it my self-taught way.  WOW!  What an eye opener.  Thanks for an informative and valuable 3 days!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michaela Anderson</span>
                    <b>|</b>
                    <span class="company">Dramatic Publishing</span>
                </h3>
                <p itemprop="reviewBody">Gabriel is an excellent teacher, and has made me understand Dreamweaver in a way that I never thought was possible. It is extremely hard to find a talented teacher who is easy to follow and has the ability to teach in a way that gets the information across, and Gabriel definitely did that. I will definitely return to his future training classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Elaine Hyland</span>
                    <b>|</b>
                    <span class="company">InterMedius</span>
                </h3>
                <p itemprop="reviewBody">I found no fault with this class or facility.  I have taken Dreamweaver courses before and was unable to create a site when I went home.  This class was fantastic, I really feel that I understand the concepts taught.  I liked that the instructor had the class create a site on our own in class so that  we could see the level of our own understanding of the material taught. Great class! great instructor! Thanks.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jamie Grahn</span>
                    <b>|</b>
                    <span class="company">Allstate</span>
                </h3>
                <p itemprop="reviewBody">While I have loved and benefited from every class that I have taken here, I believe this was one of the best!  Gabe challenged us to try things on our own so that we are prepared when we leave the classroom.  I would definitely take another of Gabe's classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin Reidy</span>
                    <b>|</b>
                    <span class="company">General Dynamics Information Technology</span>
                </h3>
                <p itemprop="reviewBody">Jeff was on point with classroom help, examples from outside the courseware and knowledge of the program. I feel confident that I can walk away from this course with the knowledge I need to create professional, well-designed websites.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sarah Vonder Reith</span>
                </h3>
                <p itemprop="reviewBody">The facility was immaculate, and provided any and all needs that the students may have wanted. Great instructer, and staff. I would take additional classes here any time.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nicole Keesecker</span>
                    <b>|</b>
                    <span class="company">University of Chicago</span>
                </h3>
                <p itemprop="reviewBody">This class was great.  It was just the right pace for me.  I learned the skills I needed to improve my work (and certainly many more).  Gabe is an excellent instructor.  He is patient, makes the content understandable, and provided the class with real-world scenarios that we could learn from and make mistakes with.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tamela Box</span>
                    <b>|</b>
                    <span class="company">GrowTech, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I traveled over 500 miles to attend this course, and I wasn't disappointed.  Gabriel did a great job letting us explore and practice, and I learned so much from that and having him help me through the problems I encountered!  We have more than enough information to do some damage... er... headway on the website!   </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Karen Boylan</span>
                    <b>|</b>
                    <span class="company">Newly Weds Foods</span>
                </h3>
                <p itemprop="reviewBody">This course was a great blend of teaching examples and hands-on. I liked that even when our instructor was teaching something we were following along. Gabriel was an excellent teacher, he had a very easy presentation style that kept your interest but did not overwhelm you. The small class size was a definite benefit in the learning process. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eva</span>
                </h3>
                <p itemprop="reviewBody">Gabe is an awesome guy! I feel that I learned a great deal, and feel confident that I could easily build a website based on the information and the work we did together. He worked at a pace that was perfect for our class. He was patient with all of our questions. He was confident and knew his materials. It was joy to take this class, which I was actually slightly fearful of before the class started. I would take another class with him in the future. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Stockwell</span>
                    <b>|</b>
                    <span class="company">John Sterling Corporation</span>
                </h3>
                <p itemprop="reviewBody">Before taking this class I had only worked through Dreamweaver on my own and participated in some random online tutorials.  After this class, I have a much better understanding of the basic functionality of each Dreamweaver tool and feature.  Rick is very informative providing thought provoking questions and exercises.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Annalise Johnson</span>
                    <b>|</b>
                    <span class="company">Scholle Packaging</span>
                </h3>
                <p itemprop="reviewBody">This class taught me in three days what I wasn't able to learn in three years from other courses. Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Belinda Wendt</span>
                    <b>|</b>
                    <span class="company">GrowTech, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Great introduction to DreamWeaver! It all seems so simple now... We have a lot of great new ideas for enhancing our current websites now - I can't wait to start working on them! The instructor was extremely knowledgeable on the subject matter and was very patient helping us through our many challenges. He was brilliant, muscular, and clearly talented.Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Heinemann</span>
                    <b>|</b>
                    <span class="company">TPG Software, Inc.</span>
                </h3>
                <p itemprop="reviewBody">When I enrolled for the Dreamweaver class, I hoped for the best and expected the worst since I was relying on student testimonials more than any other criteria. This course actually exceeded anything I could have hoped for and Gabriel is an excellent instructor. The class was well paced and he was available both before and after class. I would have given the class an A, but his daily magic trick after lunch has earned it an A+. If you're completely new to Dreamweaver and web design, you would be hard pressed to find a better class. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Neal Cox</span>
                </h3>
                <p itemprop="reviewBody">This was a terrific overview, with a top-notch instructor. I feel I have a solid handle on the material and can't wait to put it to use.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Judy Twarogal</a>
                </h3>
                <p itemprop="reviewBody">Like others have stated, this course really surpassed my expectations.  Gabe is one of those rare individuals who is a Master of the subject matter, yet is able to teach the course at a level that can be followed by beginners as well as individuals already familiar with web development and design.  Upon completion of this course, everyone, no matter what their level will be able to develop a site.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John McKenzie</a>
                </h3>
                <p itemprop="reviewBody">The course was great.  It made dreamweaver, which originally seems complex, quite easy to understand and, although the class was fast paced, the pace was modified to accommodate our learning abilities and Gabriel made sure we understood each piece before moving on.  I am very happy with this course and am impressed with the sheer volume of knowledge Gabriel was able to  provide us with.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

