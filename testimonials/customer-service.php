<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Customer Service Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our Customer Service classes training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/customer-service-training.php">Customer Service classes classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>470 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sheila Mims</span>
                    <b>|</b>
                    <span class="company">Federal Reserve Police Dept</span>
                </h3>
                <p itemprop="reviewBody">Great Class! Even though I had over 20 years of customers, the instructor cover lot of topics in dealing with our management staff. Great Instructor - knowledgeable!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Melissa Minkalis</span>
                    <b>|</b>
                    <span class="company">Rana Meal Solutions, LLC</span>
                </h3>
                <p itemprop="reviewBody">Lena was very knowledgeable about the course content and was very enthusiastic about her work. Her passion for customer service and coaching really showed. I would absolutely take a class from her again.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carolyn Cooper</span>
                    <b>|</b>
                    <span class="company">Nordent Manufacturing, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Excellent instruction - I felt the instructor was very thorough and explained the to do's and not to do's in a very professional manner.  Very relaxed atmosphere.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Steven Shawver</span>
                    <b>|</b>
                    <span class="company">Marc Realty</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed my class today, I think that I will be able to use what i have learned on Friday when I return to work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ricardo Chavez</span>
                    <b>|</b>
                    <span class="company">PSSI</span>
                </h3>
                <p itemprop="reviewBody">I truly enjoyed the training course, there are many tools that I learned while here that I can take back to use in my work place and can teach my coworkers. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bryn Jones</span>
                    <b>|</b>
                    <span class="company">Vinesse</span>
                </h3>
                <p itemprop="reviewBody">Allyncia was a very energetic speaker and keeps your attention. Definitely not boring and she is very cool. Learned a lot of tips to improve myself about things I would not have noticed in my everyday mannerisms.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maria Castillo</span>
                    <b>|</b>
                    <span class="company">National Able Network, Inc.</span>
                </h3>
                <p itemprop="reviewBody">This was an excellent training, I learned so much that will help me do a better job. Thank you so much Juana you are the best instructor I have ever had!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Malinda Demery</span>
                    <b>|</b>
                    <span class="company">National Able Network, Inc.</span>
                </h3>
                <p itemprop="reviewBody">This training was exceptional, very informative and easy to understand. I definitely recommend this training and Juana is Great.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenny Chavez</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">I haven't taken classes in a very long time. I am very happy with this class.  I have learned a lot. Many things I didn't know nor realized I needed. I definitely would love to take another class with instructor Carol. I think time management class would be lovely with her.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alecia Harris</span>
                    <b>|</b>
                    <span class="company">CICS-Loomis Primary Academy</span>
                </h3>
                <p itemprop="reviewBody">This was a great training and the instructor was on-point. I love her energy. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Summer McQuoid</span>
                    <b>|</b>
                    <span class="company">National Restaurant Association</span>
                </h3>
                <p itemprop="reviewBody">The Customer Service class was smart and organized. The positive attitude from the instructor and interactive situations made learning fun again!! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mike Klein</span>
                    <b>|</b>
                    <span class="company">Maurice Sporting Goods, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Rich is a real professional. I took 5 pages of notes and truly will take lots of great ideas back to the office to share with others and to implement right away. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maria Gonzalez</span>
                    <b>|</b>
                    <span class="company">Softline Solutions</span>
                </h3>
                <p itemprop="reviewBody">Carol is an amazing person, she excelled in all teaching ability's. I will definitely come back and join another one of Carol's classes. she's great!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Linda Gothar</span>
                    <b>|</b>
                    <span class="company">Vinesse</span>
                </h3>
                <p itemprop="reviewBody">I thought Allyncia was great. She mixed humor with the course which keeps it interesting. Thank ou</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cheryl Walsh</span>
                    <b>|</b>
                    <span class="company">Maurice Sporting Goods, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I do appreciate that our company has invested in a number of us to participate in building a world class Customer Service Team.  Having been in a customer service role for most of my career, Rich Kerr help me today with a fresh reminder of the importance a quality customer service.  Being the Customer Service Lead for our division, today's class will help me to raise our level and expectations of our team. Thank You</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.3</h3>

                    <div class="star-rate star-rate-sm star-rate-4_3">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                I
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Irma Sanchez</span>
                    <b>|</b>
                    <span class="company">Telepacific Communications</span>
                </h3>
                <p itemprop="reviewBody">Everything was wonderful and my instructor was the best.  I would highly recommend this Customer Service training.  Looking forward to more training sessions with you in the future.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michael Moss</span>
                    <b>|</b>
                    <span class="company">GCAP Services</span>
                </h3>
                <p itemprop="reviewBody">"Grandma Carol" was exceptional.  She did a fantastic job of presenting the course materials in a way that was clear, concise and consistent throughout the day.  I found value in the information provided and the way in which it was taught.  I would definitely recommend this course to others.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mariya Pozikov</span>
                </h3>
                <p itemprop="reviewBody">absolutely wonderful class and instructor, will definitely take another course</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ernst Palomo</span>
                    <b>|</b>
                    <span class="company">Global Capacity</span>
                </h3>
                <p itemprop="reviewBody">I really liked this class, instructor engaged with each student and gave good examples.The information was broken down into more details that where very helpful. Great Job!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bunny Hoskins</span>
                    <b>|</b>
                    <span class="company">MG Design</span>
                </h3>
                <p itemprop="reviewBody">this course made you aware of all senses that need to be used in communicating with client's. What to do, versus - not what to do Very helpful and insightful. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Clancy McLaughlin</span>
                    <b>|</b>
                    <span class="company">AFN</span>
                </h3>
                <p itemprop="reviewBody">This was exactly what I was looking for.  Not role playing but theory and making sure to distinguish between internal/ external, empathy above all, outlining the cycle, listening actively.  I also enjoyed the personal attention in both what we do well and what we need to work on.  Success starts with our own attitude. Thank you so much - I know I can bring this back to AFN and add to the culture of success.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brittany Barber</span>
                    <b>|</b>
                    <span class="company">Johnny Was</span>
                </h3>
                <p itemprop="reviewBody">The instructor was a great listener and also very efficient with providing the information in the lesson. She also made sure to meet each student at their level and was non judgmental in her comments and criticism. She made us all feel comfortable. Great class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ruquaiyah Wilson</span>
                    <b>|</b>
                    <span class="company">Federal Reserve Bank of Chicago</span>
                </h3>
                <p itemprop="reviewBody">Lena was very knowledgeable and helpful with questions regarding every aspect of customer service. I appreciated Lena's technique for dealing with situations. She had the students to act out situations which helped us overcome some of the obstacles we as Customer Service Reps deal with on a daily basis. I highly recommend this course to others in my workplace. I will love to attend other classes taught by Lena. I will stay in contact with her for sure. Thanks for having me!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Taneka Feliciano</span>
                    <b>|</b>
                    <span class="company">CICS-Loomis Primary Academy</span>
                </h3>
                <p itemprop="reviewBody">Our instructor Ms. Wooldridge was amazing!  She made sure we knew core fundamentals of customer service as well as used real life experiences to get across her many points! All exercises and stories alike helped me in understanding me as a person and how to use the strategies to better assist others... </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alonso Parra</span>
                    <b>|</b>
                    <span class="company">REME</span>
                </h3>
                <p itemprop="reviewBody">Very insightful, clear and valuable training. I've gone through various other training courses and I'm glad to learn new things to add to my Customer Service toolkit. Carol was awesome and very personable and relatable. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Blanca Melgar</span>
                    <b>|</b>
                    <span class="company">Watts Healthcare Corporation</span>
                </h3>
                <p itemprop="reviewBody">Allyncia was great in every way.  Learned so much for her. Would strongly recommend her to anybody. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">L Wiley</span>
                    <b>|</b>
                    <span class="company">Dental Assisting National Board</span>
                </h3>
                <p itemprop="reviewBody">Juana is so good at what she does I loved this class. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                I
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Iveth Figueroa</span>
                    <b>|</b>
                    <span class="company">Kerri S Parks Professional</span>
                </h3>
                <p itemprop="reviewBody">I really appreciated the examples we used for the exercises. I am Really happy that i took this class. I know realised how to be more knowledgable about my speaking.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jasmyne Robertson</span>
                    <b>|</b>
                    <span class="company">Productivity Point Global</span>
                </h3>
                <p itemprop="reviewBody">I was pleasantly surprised with this session, it was incredibly helpful and she applied many understandable real-life applications while teaching. I thought it was extremely helpful, and I'll likely pass on things I took away to others as well. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Memorie Camargo</span>
                    <b>|</b>
                    <span class="company">Amanecer Community Counseling Services</span>
                </h3>
                <p itemprop="reviewBody">Carol was a delight! coming in to this training I mentally prepared myself to be bored, but not once did Carol not have my attention. From beginning to end she was enthusiastic, knowledgeable &amp; as sweet as can be. I have never been to a training that I enjoyed as much as this one. I love that the class is intimate and interactive. If I could hire Carol as a life coach I would. Thank you so much for this great experience and all of the great information.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

