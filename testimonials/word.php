<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Microsoft Word Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Microsoft Word  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/ms-word-training.php">Microsoft Word  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>452 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Karen Townsend</span>
                    <b>|</b>
                    <span class="company">Hanson Material Services</span>
                </h3>
                <p itemprop="reviewBody">I am so happy that three of my biggest areas of feeling absolutely no confidence in have been taught in a way I finally understand.  Tabs, indents and mail-merge finally make sense and I'm looking forward to trying them.  Even getting stuck in the elevator didn't stop us from learning.  I will recommend others to take this class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Z
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Zonda Zschau</span>
                    <b>|</b>
                    <span class="company">American College of Healthcare Executives</span>
                </h3>
                <p itemprop="reviewBody">I always enjoy Sandy's training sessions! I was her only student in this class, so I got the bonus of a one on one session with her.  It was great as I leaned even more than I normally would. Excellent instuctor :) </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Z
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Zonda Zschau</span>
                    <b>|</b>
                    <span class="company">ACHE</span>
                </h3>
                <p itemprop="reviewBody">I was delighted to learn that Sandy would be my instructor again!  I have had 2 instructors for courses here and while both are excellent.  Sandy's teaching style is the very best!  I was the only student in this class and enjoyed the tailor made instruction.  The tips and shortcuts were an added benefit!  I have and will continue to recommend that people take courses here.  Thank you.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Annmarie Mignini</span>
                    <b>|</b>
                    <span class="company">Los Angeles World Airports</span>
                </h3>
                <p itemprop="reviewBody">Excellent training.  Nicely paced. We moved along quickly but not rushed if we got lost or needed additional help.  Very friendly teacher.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stephen Best</span>
                    <b>|</b>
                    <span class="company">Michael Alexander &amp; Associates</span>
                </h3>
                <p itemprop="reviewBody">This was my first and hopefully not my last Training Connection course.  I very much appricated the real world application examples and the instant rapport I felt with Sandy.  I am interested in the June Excel course she spoke of.  The office was tidy and impressive and well organized.  Everyone was easily approchable and I enjoyed my experiece here today.  Thank you!  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Vivian Muro</span>
                    <b>|</b>
                    <span class="company">Baldwin Park Unified School District</span>
                </h3>
                <p itemprop="reviewBody">Ms. Allyncia was very energetic which made the class time go by smooth. She is really friendly and very knowledgeble. Will like to take another class with her!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ryan Collin</span>
                </h3>
                <p itemprop="reviewBody">The instructor was excellent. He knew the material well and provided great examples throughout. He even sent us away with additional reference materials that are sure to come in handy down the road. Great class. Thank You!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Darrell Johnson</span>
                </h3>
                <p itemprop="reviewBody">Allyncia was a fantastic instructor.  Her knowledge of Microsoft Word is extremely deep.  Her attention to details and coverage of class examples was impeccable.  I highly recommend Allyncia to anyone who is interested becoming more proficient in Microsoft Word.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mark Bonenfant</span>
                </h3>
                <p itemprop="reviewBody">Excellent instructor. Wealth of good material presented in systematic fashion at good pace. Good emphasis on the more difficult topics. Instructor was personable, patient and good to work with for the day. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cheryl Eugenio</span>
                    <b>|</b>
                    <span class="company">Economic Club of Chicago</span>
                </h3>
                <p itemprop="reviewBody">Everything was wonderful and very insightful. Went a good pace and answered any questions that I had. Overall, a very good experience covering basics.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eddy Travis</span>
                    <b>|</b>
                    <span class="company">Hanson Material Services</span>
                </h3>
                <p itemprop="reviewBody">Again, I am thoroughly enjoying the learning experience.  I no longer feel intimidated with MS Word--mainly because of the level of teaching that I have received from the instructor.  Thanks.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christina Turnbull</span>
                    <b>|</b>
                    <span class="company">Union Bank</span>
                </h3>
                <p itemprop="reviewBody">Allyncia went above and beyond helping me resolve issues with templates.  Her knowledge was and is greatly appreciated.  I will definitely recommend her to my co-workers. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jon Newkirk</span>
                </h3>
                <p itemprop="reviewBody">Thorough course, very clean facility and friendly staff. Instructor was great, although got stumped a few times. She explained everything well and could reference anything I asked.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eddy Travis</span>
                    <b>|</b>
                    <span class="company">Hanson Material Services</span>
                </h3>
                <p itemprop="reviewBody">It as been quite a learning experience being taught by Lena.  She was thorough and patient in answering questions, repeating topics that seemed a bit unclear on the next day of class, and quite helpful in researching answers to related topics.  I would definitely recommend the class to my co-workers; and return for future classes. Also, I would like to thank the Office Manager, Allyncia, for such great hospitality and professionalism.  She was always checking to make sure that all our needs were being met. Thanks to both of them for a pleasant learning experience.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kathleen Condon</span>
                    <b>|</b>
                    <span class="company">BP Products North America, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Class was amazing.  I learned so much!  Thank you and I will make referrals.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">DION L. TILLMAN</span>
                </h3>
                <p itemprop="reviewBody">Sue was great!!!  As an Instructor, she trully excells...no pun intended.  She explained all the components of the Excel Program clearly. Thank You, Sue</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Suzie Carlson</span>
                    <b>|</b>
                    <span class="company">Department of Water Resources California</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is a wonderful instructor and I would love to take all classes here with her! She makes this easy : )</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Toni Donahue</span>
                    <b>|</b>
                    <span class="company">PLS Financial</span>
                </h3>
                <p itemprop="reviewBody">Sandy is a great instructor. She took her time was hands on and upbeat. She showed great respresentation for Training Connection. I'm walking away more knowledgeble and able to go back to my workplace and apply what i've learned.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristy Stachelski</span>
                    <b>|</b>
                    <span class="company">Ricondo &amp; Associates</span>
                </h3>
                <p itemprop="reviewBody">Lena was an excellent instructor. She thoroughly explained the material, gave great examples of how I would use the program in the workplace, and also answered any questions that I had. I would highly recommend Training Connection and/or Lena to anyone who is considering taking a course in Microsoft Word or PowerPoint.  After the knowledge that I gained from my course today, I feel better prepared to tackle large documents and projects within my position at my new job.  Thank you so much!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Roman Perez</span>
                    <b>|</b>
                    <span class="company">RANA MEAL SOLUTIONS LLC</span>
                </h3>
                <p itemprop="reviewBody">Great job instructor Sandy! I will totally take another course with you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Vicki Miller</span>
                    <b>|</b>
                    <span class="company">Litchfield Cavo</span>
                </h3>
                <p itemprop="reviewBody">I have taken other training classes and this by far was the best because the instructor took the time to explain any questions we had and made sure we understood the concepts taught. thank you for this training.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Amy Bogl</span>
                    <b>|</b>
                    <span class="company">Economic Club of Chicago</span>
                </h3>
                <p itemprop="reviewBody">Terrific class with a great teacher...I learned how to edit others documents, something I had been wanting to learn for a couple of years.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Amy Bogl</span>
                    <b>|</b>
                    <span class="company">Economic Club of Chicago</span>
                </h3>
                <p itemprop="reviewBody">This was a terrific class. I learned something that I had been struggling with at my job for 3 years.....leaders : )</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelly Craig</span>
                    <b>|</b>
                    <span class="company">MullinTBG</span>
                </h3>
                <p itemprop="reviewBody">Allyncia, was great. She made learning Word fun.  She taught me tons of tips to make working with Word much easier. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alberto Rodriguez</span>
                    <b>|</b>
                    <span class="company">Rust-Oleum Corporation</span>
                </h3>
                <p itemprop="reviewBody">WOW! What an amazing experience.  Sandy was a real treat to work with.  Her knowledge, experience, and training style was what I would want any future instructor to have.  I could not have asked for a better experience and will be sure to recommend her to any one looking to step up their Microsoft Application skills. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelly Johnston</span>
                    <b>|</b>
                    <span class="company">Berkeley Research Group LLC</span>
                </h3>
                <p itemprop="reviewBody">The instructor gave great examples to help me figure out every aspect of the way to use each command.  She was very helpful and an awesome communicator.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kate Gayton</span>
                    <b>|</b>
                    <span class="company">SSC Construction</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is wonderful! Friendly and very competent. What a great experience! I will definitely be back-</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kyla Whaley</span>
                    <b>|</b>
                    <span class="company">Johnson Controls, Inc</span>
                </h3>
                <p itemprop="reviewBody">Again the training was top notch... I will not go any where else to be trained. I will be recommending this company to others at my job.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Karen Townsend</span>
                    <b>|</b>
                    <span class="company">Hanson Material Services</span>
                </h3>
                <p itemprop="reviewBody">Even though it was alot of information to learn in three days, I was totally absorbed in the content. I can't wait to get back in the office to "show off" some of the things I learned this week and use the features I always dreaded like MAIL MERGE:)  Even tracking changes which truly intimidated me when I saw it in other peoples documents will be fun to put to use.  Lena was an excellent trainer, very patient with all our "oh my gosh" comments.  I enjoyed sharing lunch and break times with Allyncia and Lena.  Allyncia was a wonderful hostess - very cheerful and impressed me with her friendliness and helpfulness each day. I will recommend this training series to other employees who will be looking for training as they look for new jobs in the near future.  Thank you for a great boost in my confidence as a Word user.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

