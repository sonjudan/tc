<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Adobe InDesign Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Adobe InDesign  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/indesign-training.php">Adobe InDesign  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>128 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Paige Gast</span>
                    <b>|</b>
                    <span class="company">WJN Enterprises</span>
                </h3>
                <p itemprop="reviewBody">This is the 4th course I have taken with Eva and it was just as informative as the first 3. She goes at a great pace and is obviously very knowledgeable when it comes to the Adobe suite products. I appreciate being able to learn such amazing programs from someone who knows them so well.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelly Rogers</span>
                    <b>|</b>
                    <span class="company">Alvord Unified School District</span>
                </h3>
                <p itemprop="reviewBody">The training was extremely beneficial.  The instructor, Beverly, explained each concept thoroughly, while providing real-word examples.  The pace was perfect - Beverly allowed enough time to complete each step and absorb the information, while still moving quick enough to cover a large amount of material.  She was incredibly patient and took the time to help each member of the course individually when needed.  I would definitely recommend the class to anyone looking to explore InDesign!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bridgette Garthwaite</span>
                    <b>|</b>
                    <span class="company">The John Buck Company</span>
                </h3>
                <p itemprop="reviewBody">If there was an option better than strongly agree, I would have chosen that. I've taken many software classes, and I thought this was one of the best.  It was taught at the right pace, and the practical exercises helped work through real-life questions that may come up. I'm really looking forward to the other two Training Connection courses I'm signed up for.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kirsten Reynen</span>
                </h3>
                <p itemprop="reviewBody">Chana is absolutely fantastic! She has a lot of patience and has a wealth of information to share. I would take all her classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michelle Medina</span>
                    <b>|</b>
                    <span class="company">Jet Propulsion Laboratory</span>
                </h3>
                <p itemprop="reviewBody">Chana is an excellent instructor.  She moves at a steady pace and interacts with students. I have had her for my Photoshop course and look forward to having her again for my Illustrator course.  I learned much more than I expected.  Chana goes that extra mile for her students.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Roxanne Price</span>
                    <b>|</b>
                    <span class="company">BCSP</span>
                </h3>
                <p itemprop="reviewBody">Thank you, Eva! You are a gifted teacher with just the perfect combination of enthusiasm, patience and knowledge.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Melissa Ziemba</span>
                    <b>|</b>
                    <span class="company">TranSystems</span>
                </h3>
                <p itemprop="reviewBody">I very much enjoyed this class. I learned a lot of valuable information that i will be able to use in my day to day activities at my job. Eva was great! She was very friendly and made the learning atmosphere fun and keep it interesting, while at the same time paying the same amount of personnel attention to each student. I highly recommend this course.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kate Hightower</span>
                    <b>|</b>
                    <span class="company">Strategic Mobility Group</span>
                </h3>
                <p itemprop="reviewBody">This class was absolutely wonderful. Eva was very knowledgeable and very helpful. The staff was friendly and approachable. I would absolutely take another course here again.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Roxanne Price</span>
                    <b>|</b>
                    <span class="company">Board of Certified Safety Professionals</span>
                </h3>
                <p itemprop="reviewBody">This was my third class at Training Connections, so clearly I am very pleased with the excellent Adobe training! Every class I have taken has added to knowledge that I need to perform my job well. Eva is an amazing trainer and always very helpful and patient. Thanks, Training Connections and Eva!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sandi Fuerte</span>
                    <b>|</b>
                    <span class="company">Toyota</span>
                </h3>
                <p itemprop="reviewBody">Jeff was patient and very knowlegable about InDesign.  It was my first time using the product, but he made it easy to understand. His expertise with other Adobe products and Windows was helpful in answering questions about how to integrate this training for "real usage" back at the worksite.  I highly recommend this course and instructor!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Diane Serowka</span>
                    <b>|</b>
                    <span class="company">Michael J Graft Builder, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Eva provided an extremely beneficial training course.  She taught the concepts, helped all students apply these concepts to their real world situations, and made the experience very enjoyable.  Her friendly and upbeat personality was truly appreciated and kept the information lighthearted!  She is a valuable asset to your corporation!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bridget Napleton</span>
                    <b>|</b>
                    <span class="company">Napleton Dealership Group</span>
                </h3>
                <p itemprop="reviewBody">I loved this class, I learned a lot, Eva was a wonderful teacher and I can't wait to come back in October to learn even more!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maurica Manibusan</span>
                    <b>|</b>
                    <span class="company">CAHELP JPA</span>
                </h3>
                <p itemprop="reviewBody">Chana is an excellent instructor. She managed a multi-level type course for the 5-day period with ease and proficiency. I would definitely sign up for another training with her.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">David W. Brooker</span>
                </h3>
                <p itemprop="reviewBody">Beverly is an amazing instructor. I've been able to not only learn and understand many of the advanced features of InDesign, but Beverly helped place these features into the context of our unique work environment. Something that I initially did not expect - but was wonderfully pleased about! Her real life experiences tie into the class work lending an additional layer to her training that is interesting and unique. She is extremely engaging, personable, knowledgeable, and talented - I highly recommend her as an instructor at Training Connection.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rebecca Lund</span>
                    <b>|</b>
                    <span class="company">Integrated Project Management</span>
                </h3>
                <p itemprop="reviewBody">I am very pleased with the content of the class and even more satisfied with the software and teaching capabilities of Eva. She has been a very patient and knowledgeable instructor who provides valuable insight into what can be daunting software. I hope to take a my next Training Connection class with her.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenna Coyle</span>
                    <b>|</b>
                    <span class="company">Stryker</span>
                </h3>
                <p itemprop="reviewBody">This course will be save me countless hours as I implement the concepts I learned into my existing workflow. The first three days of the bootcamp reinforced the skills I already had and helped me brush up on those I hadn't quite mastered. The advanced portion of the curriculum dove deeper into automation, GREP, and other workflow options that will make my work faster, easier, and more professional. This is exactly what I hoped to get out of the program, and I'm excited to put what I've learned into practice.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Silvia Danna</span>
                </h3>
                <p itemprop="reviewBody">Bev is a highly knowledgeable instructor. Thank you Bev for sharing your passion with us!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Marissa Hunter</span>
                    <b>|</b>
                    <span class="company">enVision Consulting Group, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Chana is an amazing instructor. I would take all of her classes!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Natalie Vega-Finn</span>
                    <b>|</b>
                    <span class="company">Near North Montessori</span>
                </h3>
                <p itemprop="reviewBody">This is my second time taking this class as I took advantage of the re-take within 6 months. I had never used any of the products in this suite before taking the class the first time. I found that when I went to apply those skills, I was overwhelmed. I tinkered with it for a month or so and then enrolled to re-take the class. I found that everything makes much more sense to me the second time around because I was a little more familiar with the product. This was a great class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shari Weiner</span>
                </h3>
                <p itemprop="reviewBody">Jeff Wiatt is a wonderful instructor.  I wouldn't hesitate for a moment to take another class with him.  He was very helpful and I learned a great deal.  I can't wait to go home and practice what I learned today.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                H
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Heather Fairclough</span>
                    <b>|</b>
                    <span class="company">Accelerated Learning Group</span>
                </h3>
                <p itemprop="reviewBody">Learning new computer programs and concepts can be very hard at times. However, the instructor was the best I have had so far in computer training. She spoke clearly, knew what she was talking about, was able to relate it to the real world, took time to help those who needed it, was very prepared, and created examples that were easy to understand and follow. I would definitely take another course from her.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sonja Cothran</span>
                    <b>|</b>
                    <span class="company">Ameron Pole Products Division</span>
                </h3>
                <p itemprop="reviewBody">The information coverage exceeded my expectations.  Facility is inviting and comfortable.  Staff is extremely pleasant.  I have enjoyed the experience.  I hope to follow up with more classes.  Thank you! p.s. ...  Also, love the Starbucks coffee and tea, Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                H
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Hollie Benedik</span>
                    <b>|</b>
                    <span class="company">American Academy of Orthopaedic Surgeons</span>
                </h3>
                <p itemprop="reviewBody">Eva was a wonderful teacher. She is really nice and really cares about the students' needs. She made me want to learn even more about InDesign!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rebecca Duggan</span>
                    <b>|</b>
                    <span class="company">South California Edison</span>
                </h3>
                <p itemprop="reviewBody">Very informative and well thought out course.  Each project built on skills learned from the previous which helped with the reinforcement.  Excellent training materials.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kara Mitchell</span>
                    <b>|</b>
                    <span class="company">Fairhaven Baptist Church</span>
                </h3>
                <p itemprop="reviewBody">Loved the class!  I came into the class with very little experience in InDesign and now feel like I could actually begin designing my own material using this program.  I enjoyed Eva's personality and teaching style.  She was very positive and patient with each student.  I look forward to taking another class with Training Connection!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Amy Knowles</span>
                    <b>|</b>
                    <span class="company">Fairhaven Baptist Church</span>
                </h3>
                <p itemprop="reviewBody">Eva was great. She definitely knows this program inside and out. She didn't miss a beat whether it was unusual questions from the students or rephrasing the examples in the book to help us better understand the project.  I have been using InDesign for a few years, but the hints and techniques learned in this class will be an invaluable asset.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eric Warren</span>
                    <b>|</b>
                    <span class="company">CDC</span>
                </h3>
                <p itemprop="reviewBody">Eva had an encyclopedic knowledge of InDesign.  Amazing training.  If you need training in Adobe products, make sure you get Eva.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christina Andersen</span>
                    <b>|</b>
                    <span class="company">Esri</span>
                </h3>
                <p itemprop="reviewBody">Chana was very patient and a true teacher at nature. No indication that asking questions or going over a step was ever a problem or inconvience...love her :)</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Dorothy Witkowski</span>
                    <b>|</b>
                    <span class="company">Environmental Resources Management (ERM)</span>
                </h3>
                <p itemprop="reviewBody">Eva is a fantastic trainer!  This was a great experience.Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kimberley Mull</span>
                    <b>|</b>
                    <span class="company">State of Michigan</span>
                </h3>
                <p itemprop="reviewBody">Very knowledgeable instructor. She was very helpful with questions I posed and was also good at making suggestions for using the software capabilities in ways I hadn't thought of.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

