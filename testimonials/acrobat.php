<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Adobe Acrobat Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Adobe Acrobat  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/acrobat-training.php">Adobe Acrobat  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>129 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Candace Ellison-Morgan</span>
                    <b>|</b>
                    <span class="company">Watson Laboratories</span>
                </h3>
                <p itemprop="reviewBody">Information taught in the Adobe class was of great value.  I learn tremendous amount of information that will help me perform my job more proficiently.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Robin Williams</span>
                    <b>|</b>
                    <span class="company">Freeborn &amp; Peters LLP</span>
                </h3>
                <p itemprop="reviewBody">Michael did an excellent job.  As a non-Adobe user, I was comfortable with the pace of the class, his teaching approach and the found the materials useful in my workflow.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lisa Kimura</span>
                    <b>|</b>
                    <span class="company">Hawaii State Senate</span>
                </h3>
                <p itemprop="reviewBody">The facilities are top notch and very clean.  Jeff was an excellent instructor.  I liked the fact that he knew his material well and didn't have to go through the book page by page.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristen Newton</span>
                    <b>|</b>
                    <span class="company">Usbergo, Inc- EcoProProductd</span>
                </h3>
                <p itemprop="reviewBody">Eva was a fantastic instructor. I really enjoyed our two days together and got a lot out of the course. I am looking forward to more courses in the future. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carla Machuca</span>
                    <b>|</b>
                    <span class="company">roundpeg, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I loved the class.  I even feel a little guilty for taking so much of the class time.  She was so gracious!  I can't wait to take her InDesign class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ana Valenzuela</span>
                    <b>|</b>
                    <span class="company">Guidance Software, Inc.</span>
                </h3>
                <p itemprop="reviewBody">The instructor took the time to ask how much of the subject I knew and what were my interests or things I hoped to learn by the time I left the class that way he knew what topics to concentrate on.  We had a very small class therefore we were able to customize and personalize it.  I liked that very much and feel I walked out knowing a lot more on the subject.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anzanique Luckey</span>
                    <b>|</b>
                    <span class="company"></span>
                </h3>
                <p itemprop="reviewBody">I requested a one-on-one session to meet the specific requirements of a project that I was working on.  Jeff provided me with the perfect level of training allowing me to complete my current project, apply what I learned to other projects, and save myself a lot of time and energy!!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Marina Kibardina</span>
                    <b>|</b>
                    <span class="company">City Colleges of Chicago</span>
                </h3>
                <p itemprop="reviewBody">I have taken quite a few courses here and every time am very pleased with the outcomes and the quality of the instruction, facilities, etc.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stephanie Biles</span>
                    <b>|</b>
                    <span class="company">Paul Reilly Company</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the class, I learned more than I planned, and would like to sign up for additional classes</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christina Andersen</span>
                    <b>|</b>
                    <span class="company">Esri</span>
                </h3>
                <p itemprop="reviewBody">I love the Training Connection, I taken three courses and I have been satisfied with every course taken. The instructors are very skilled and great teachers. Yes, I would highly recommend the TC to others.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christine Meister</span>
                    <b>|</b>
                    <span class="company">Goldberg Kohn Ltd.</span>
                </h3>
                <p itemprop="reviewBody">Eva was able to provide real world examples from her own experiences that made the concepts come to life.  It turned out to be a wonderful advantage that I was the only class attendee, as we were able be flexible with the pace and materials.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Erin Emerson</span>
                    <b>|</b>
                    <span class="company">DeVry University</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the hands-on approach to this course as it reinforced my learning and further understanding of the topic. I would come back to the Training Connection for other training courses as I thoroughly enjoyed the course. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michelle Feinberg</span>
                    <b>|</b>
                    <span class="company">Healthnet</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the adobe acrobat class. I learned a lot and Jeff was a great instructor.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cyndi Hefler</span>
                    <b>|</b>
                    <span class="company">Freeborn &amp; Peters LLP</span>
                </h3>
                <p itemprop="reviewBody">Michael is great.  I would definitely want to sign up for other courses that he teaches. Very enjoyable - willing to answer so many of our questions.   I'd also recommend him to others. Also, what a nice training room!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Paulette Gomez</span>
                    <b>|</b>
                    <span class="company">County of San Bernardino</span>
                </h3>
                <p itemprop="reviewBody">Ms Beverly Houwing is very knowledgeable in Adobe Acrobat Pro DC.  She made the class fun.  I've been to many other training classes and she is the best instructor I've had! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Audrey Borner</span>
                    <b>|</b>
                    <span class="company">City of Pomona - Police Department</span>
                </h3>
                <p itemprop="reviewBody">I found this training class online and signed up due to course description.  I was fortunate enough to be put in a one on one class and I am pleased to say that it went beyond what I was expecting.  The class went more into what I use the program.  I also learned extras related to the program which was pretty cool.  The instructor was pretty knowledgeable and awesome!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>


