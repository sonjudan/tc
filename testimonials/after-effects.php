<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">After Effects Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our After Effects training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/after-effects-training.php">After Effects classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>535 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Evan Sabourin</span>
                    <b>|</b>
                    <span class="company">DIRECTV</span>
                </h3>
                <p itemprop="reviewBody">Love this course. Kristian Gabriel is the best. Friendly, patient and great teaching techniques. Very creative. This is my second time here. Had so much fun the first time I had to come back. This training has really changed my career as an editor. Looking forward to taking more Adobe courses here in the future.  Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alexandria  Zech</span>
                </h3>
                <p itemprop="reviewBody">Kristin was very patient and a pleasure! He was very knowledgeable and great at troubleshooting! I enjoyed him very much and wish we could take more classes with him!!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Dix</span>
                </h3>
                <p itemprop="reviewBody">I enjoyed this class very much. I have never used After Effects before, but after the first day of this class I felt like I could make really good basic effects. I will be able to use everything that I learned this week.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Evan Sabourin</span>
                    <b>|</b>
                    <span class="company">DIRECTV</span>
                </h3>
                <p itemprop="reviewBody">This class was awesome. The only regret that I have was that I didn't do it sooner! Kristian was awesome! He far surpassed any expectations I had. His skills and teaching methods were top notch. His patience and concern for the students to learn was great. I would highly recommend this class to anyone. So glad I got to do this. Thanks Kristian!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Megan Russell</span>
                    <b>|</b>
                    <span class="company">Groupon, Inc</span>
                </h3>
                <p itemprop="reviewBody">Rob Shultz was an absolutely wonderful teacher and I walked away with more knowledge in 5 days than I could've ever figured out from any book or googling. He had helpful hints and in-depth knowledge of almost all questions and subjects After Effects related and was able to cater the experience to each student's specific needs.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Charles Conklyn</span>
                </h3>
                <p itemprop="reviewBody">Mckinley was fantastic. She kept us engaged and was very knowledgable in the subjects she taught as well as other related subjects. Her enthusiasm for After Effects was contagious. I can't wait to get home and apply some of knowledge I've learned the past week on my own projects.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristin Smith</span>
                    <b>|</b>
                    <span class="company">Kivvit</span>
                </h3>
                <p itemprop="reviewBody">Rob was very patient, and made it easy to ask questions... I had a lot of them, and he never made me feel like I was disrupting his teaching. He spoke slowly and re-explained directions enough for the material to sink in. I will definitely recommend to my team!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin Kearney</span>
                </h3>
                <p itemprop="reviewBody">I've taken this course before and decided to take the refresher course before my 6 month period was over and am very glad I did.  It allowed me to revisit concepts that I hadn't thought about since first learning them some months ago and think progressively as of how to apply them for future projects versus the regular tools/techniques I found myself falling into. McKinley is an incredible instructor and very thorough in going through the material that the book leaves out. Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Gerri Angelo</span>
                    <b>|</b>
                    <span class="company">Sherpa Pictures</span>
                </h3>
                <p itemprop="reviewBody">We covered a lot of material in a few short days. McKinley really knows her stuff and it made all the difference. The class was both informative and fun. I would definitely take another course with the Training Connection and recommend to others.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jacquelyn Soria</span>
                    <b>|</b>
                    <span class="company">County of Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">Mckinley was an amazing instructor and I would definitely love to take another class with her. She was very informative and also entertaining throughout the course. She explained each step clearly and also assisted us when we needed her help. She was great. I would recommend her to anyone. Triple thumbs up as an instructor. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Toni Rawson</span>
                    <b>|</b>
                    <span class="company">Origin POP</span>
                </h3>
                <p itemprop="reviewBody">The professor was very knowledgeable about the program and willing to go off topic to teach other aspects of the program when asked questions. I am very pleased with the overall experience of the class and the teacher.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Antwan Johnson</span>
                    <b>|</b>
                    <span class="company">Storm King Productions</span>
                </h3>
                <p itemprop="reviewBody">Once again, McKinley Marshall was an excellent teacher for the AE Boot Camp class. I had the privilege of taking her Premiere Pro Boot Camp class as well. McKinley is very informative, friendly, and funny. I would take another class with her in heartbeat. She takes time out to make each student feel confident when taking her class. The staff is very friendly and the facility is top notch. I would highly recommend Training Connection to anyone.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sean Tom</span>
                    <b>|</b>
                    <span class="company">USC School of Pharmacy</span>
                </h3>
                <p itemprop="reviewBody">I can't say enough how fantastic McKinley is as an instructor. Her real world experience coupled with examples that are extremely useful, make this seem more like personal one-on-one training rather than a class. I am definitely going to look into the other classes she teaches and see about signing up for them.  She is very professional and extremely pleasant and I will recommend to colleagues that they take any class she teaches.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joshua Erikson</span>
                    <b>|</b>
                    <span class="company">Airsoft Gi</span>
                </h3>
                <p itemprop="reviewBody">McKinley was an excellent instructor and had a vast amount of knowledge to answer all of my questions. I would highly recommend this to anyone who is looking for real world education. Not a simple follow the book and do your home work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anthony Gamboa</span>
                    <b>|</b>
                    <span class="company">DIRECTV</span>
                </h3>
                <p itemprop="reviewBody">McKinley Marshall is now the best teacher I have ever had going back to elementary.  She did a wonderful job in everything this course had to offer. She kept everyone engaged and worked at a pace that was perfect for beginners to advanced AE editors. The course was everything I needed to execute my ideas in my profession. Thank you to McKinley for doing such a great job with everything and I look forward to taking more classes here at Training Connections.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Gail Braymen</span>
                    <b>|</b>
                    <span class="company">First Army</span>
                </h3>
                <p itemprop="reviewBody">Rob Schultz is an absolutely outstanding instructor -- extremely knowledgeable, very patient, highly skilled. I can't believe how much I've learned about After Effects in just a few days. This course will pay for itself many times over in terms of increased productivity and much higher production levels. Now I really want to take the Premiere Pro course!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michael Sage</span>
                    <b>|</b>
                    <span class="company">SGI-USA</span>
                </h3>
                <p itemprop="reviewBody">I wasn't sure if this boot camp class was going to be too much for me but I was determined. McKinley made all these hard to learn concepts fun and exciting to learn. I would highly recommend her class to my friends and colleagues.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kyle Arends</span>
                    <b>|</b>
                    <span class="company">zZounds/Mbira</span>
                </h3>
                <p itemprop="reviewBody">I was very impressed by Rob's knowledge and instructional abilities. The pacing of this class was fast enough to keep things interesting but Rob allowed plenty of time to review information if needed. This course has exceeded my expectations and I would love to take the Premier course with Rob if I had the opportunity to do so!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Chris Jones</span>
                    <b>|</b>
                    <span class="company">USC School of Pharmacy</span>
                </h3>
                <p itemprop="reviewBody">Excellent course that has opened up many new possibilities for us work wise. The instructor shared many real world concepts that will help us apply these new skills to what we do on a regular basis.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bob Anderson</span>
                </h3>
                <p itemprop="reviewBody">An absolutely amazing class!  I came thinking I knew a lot, and quickly found out - not so much!  McKinley, thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Adam Kohr</span>
                    <b>|</b>
                    <span class="company">Cutters</span>
                </h3>
                <p itemprop="reviewBody">My mind was blown. I cant stop thinking about what i'm going to do with the knowledge that Rob has taught me. Thanks -Adam</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alex Zech</span>
                    <b>|</b>
                    <span class="company">CitizenGlobal</span>
                </h3>
                <p itemprop="reviewBody">This After Effects Fundamentals class is an absolutely fantastic class.  I feel confident that I know have the basic skill-set and hands on experience to be able to navigate the program with confidence. I am also aware that the key to success for a lot of these programs is practice and patience. McKinley was a patient kind and fun teacher. She maintained a great level of enthusiasm and instruction;  she is extremely knowledgeable and shared a high level of confidence in her skill-set and professionalism.  Overall, this was an awesome way to learn and to spend a day! Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jeff Baur</span>
                </h3>
                <p itemprop="reviewBody">Instructor has a very relatable and easy teaching style. He never talks over your head about topics and provides easy to understand explanations to questions. He's very passionate and enthusiastic about the material which makes the class more interesting.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Peterson</span>
                    <b>|</b>
                    <span class="company">City of Shakopee MN</span>
                </h3>
                <p itemprop="reviewBody">Great class with great information. Rob does not just 'follow the book' which is great. Learned a lot about After Effects and more. Excited to use After Effects when I get back home.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Steve Price</span>
                    <b>|</b>
                    <span class="company">HO-CHUNK NATION</span>
                </h3>
                <p itemprop="reviewBody">Rob was awesome. Great Job Rob! I lam hoping to attending future trainings ith TC.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anonymous</span>
                </h3>
                <p itemprop="reviewBody">Kristian was a great instructor, very knowledgeable, entertaining and extremely well organized. He was able to keep the class moving while adding interesting sidebars that made it enjoyable to attend.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cheryl Lee</span>
                    <b>|</b>
                    <span class="company">Regional Occupational Program</span>
                </h3>
                <p itemprop="reviewBody">The level of detail offered was beyond what I expected; I greatly appreciated that. The instructor's knowledge of the software and all it's applications, including how it interacts with other programs is astounding. Her knowledge of video and audio technology, industries standards, and etc. is impressive. In addition, McKinley Marshall is a comprehensive instructor; she provides real world examples and uses humor as well. Terrific class</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sean Farley</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">McKinley was incredibly knowledgeable in the field of video graphics, as well as having expertise in complimentary areas to give a full understanding. She was very patient with questions and was able to find alternative ways of explaining concepts so everyone could understand. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Marco Huerta</span>
                </h3>
                <p itemprop="reviewBody">Excellent. Best Adobe training that we have received. Kristian is extremely adept at tailoring the coursework to align with career goals.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                W
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Whitney Bauer</span>
                    <b>|</b>
                    <span class="company">FCIM/FSU</span>
                </h3>
                <p itemprop="reviewBody">One of the best instructors I have ever had for learning a new software. Explained everything thoroughly and paused at anytime to help a student if they had a problem. Will try and take another course with this instructor.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

