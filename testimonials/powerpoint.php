<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Microsoft PowerPoint  Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Microsoft PowerPoint  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/ms-powerpoint-training.php">Microsoft PowerPoint  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>1,053 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Valeria Rubio</span>
                    <b>|</b>
                    <span class="company">American Honda Motor Co</span>
                </h3>
                <p itemprop="reviewBody">Carol was awesome! She really made the class fun and entertaining and I learned so many different tricks to PowerPoint. Can't wait to show my employers! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eileen Lovett</span>
                    <b>|</b>
                    <span class="company">Symphony IRI Group</span>
                </h3>
                <p itemprop="reviewBody">Sandy is a fantastic teacher. This was my third course with Sandy as the instructor and I would not hesitate to take others. She is very easy to follow and gives great examples on how to use concepts at work. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Marina Kibardina</span>
                    <b>|</b>
                    <span class="company">City Colleges of Chicago</span>
                </h3>
                <p itemprop="reviewBody">It the second class I am taking with this instructor. He is superb! I've learned so many new tricks that will help me to enhance my Power Point presentations and make them outstanding! Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tonya Watts-Daniels</span>
                    <b>|</b>
                    <span class="company">City Colleges of Chicago</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very knowledgeable. I thoroughly enjoyed the class and learned some very valuable tips that will improve my skills in all of Microsoft Office Programs. Although the class was somewhat long, the instruction was  lively and easy to follow along. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nicole Moi</span>
                    <b>|</b>
                    <span class="company">Equity International</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is a great instructor with a bright, enthusiastic attitude.  She was willing to review certain topics if any members of the class felt they required reinforcement, and she was also happy to provide her contact information for any questions we may have in the future.  The course was quite informative, and I certainly had a great time.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jennifer Alvarez</span>
                </h3>
                <p itemprop="reviewBody">Another great class with Carol.  She made PowerPoint easy &amp; fun to learn.  I want to take the next class to learn more. Thanks!!  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brian Holler</span>
                    <b>|</b>
                    <span class="company">Global Eagle Entertainment</span>
                </h3>
                <p itemprop="reviewBody">Carol was amazing! I look forward to taking more trainings with her and Training Connection!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">China Gerstner</span>
                    <b>|</b>
                    <span class="company">AcademyX</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very accessible.  I loved the small group feel helped us learn.  The instructor was dynamic in encouraging and it felt like a learning session.  She was thoughtful and very engaging!  Exactly what I needed in my high stress field!  I feel like I can showcase my skills that I learned now!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stacy Jaffa</span>
                    <b>|</b>
                    <span class="company">Wonderful Citrus</span>
                </h3>
                <p itemprop="reviewBody">Carol was the best computer software instructor I've ever had! She made the class fun (something I've never thought possible). She happily recreated slides I brought as examples and showed us some super handy shortcuts. I hope I can take more classes with her! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Marjorie Shapiro</span>
                    <b>|</b>
                    <span class="company">Channel Pricing Associates</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is a wonderful instructor and I would highly recommend this class (and teacher).  I am looking forward to using all the tricks of the trade and am confident with the new skills.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jessica Dekruiff</span>
                    <b>|</b>
                    <span class="company">Veolia Water</span>
                </h3>
                <p itemprop="reviewBody">Very helpful! Small class size allowed for personalized attention on real-life issues experienced with the software. Instructor was highly knowledgeable in PowerPoint and very friendly. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Angela Arnold</span>
                    <b>|</b>
                    <span class="company">JVS So Cal</span>
                </h3>
                <p itemprop="reviewBody">Carol is extremely knowledgeable and makes even the most technologically challenged feel like a star!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eileen Lovett</span>
                    <b>|</b>
                    <span class="company">Symphony IRI Group</span>
                </h3>
                <p itemprop="reviewBody">Sandy is a great instructor. She is easy to follow and keeps the class going at a good pace.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Julie Morales</span>
                    <b>|</b>
                    <span class="company">THQ</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is a wonderful instructor - she has great technical expertise and knowledge and was able to guide me in applying the concepts to my particular needs - and she makes the class fun!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cristina Hasbun</span>
                    <b>|</b>
                    <span class="company">AcademyX</span>
                </h3>
                <p itemprop="reviewBody">Carol was GREAT! Kind, caring, and engaging teacher.  I honestly learned so much in a days class. I would highly recommend this class to anyone who needs a refresh and wants to learn more in PowerPoint. Thank you! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jessica Teetaert</span>
                    <b>|</b>
                    <span class="company">Hyatt Hotels &amp; Resorts</span>
                </h3>
                <p itemprop="reviewBody">Even though I am familiar with ppt, this class was beneficial to learn the basics and pointers.  It also helped familiarize me with charts and images which I didn't know much about.  I felt extremely comfortable asking questions as the class was small and the instructor very approachable.  Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Antonette Hernandez</span>
                    <b>|</b>
                    <span class="company">Easter Seals Inc</span>
                </h3>
                <p itemprop="reviewBody">Sandy has been a wonderful and insightful instructor with tons of patience. I am really interested in taking another class for level two but noticed there is no date for PowerPoint level two. Could you please schedule a PowerPoint Schedule two in order for me and some of my coworkers? Thank you so much for having such a great and interesting teacher.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tim Sergi</span>
                    <b>|</b>
                    <span class="company">The Claro Group</span>
                </h3>
                <p itemprop="reviewBody">Rich does an excellent job of using relevant examples and keeping a steady pace that is easy to follow along with. He actively checks in with us to make sure we understand the concepts.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cynthia Arlt</span>
                    <b>|</b>
                    <span class="company">AcademyX</span>
                </h3>
                <p itemprop="reviewBody">What a fabulous class!  Allyncia was a great instructor, and I learned so much.  This was by far the best computer training class and facility I have been to. I cannot wait to prepare my own PowerPoint presentations.   </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Adam Powers</span>
                    <b>|</b>
                    <span class="company">Toastmasters International</span>
                </h3>
                <p itemprop="reviewBody">The course was excellent because there were only two individuals taking it. Very intimate and personal. We were able to go at our pace, asking questions when needed and getting answers immediately.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.3</h3>

                    <div class="star-rate star-rate-sm star-rate-4_3">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Katie Trimble</span>
                    <b>|</b>
                    <span class="company">IMC-Chicago</span>
                </h3>
                <p itemprop="reviewBody">Great course, very educational. Second class taught by this instructor, would definitely recommend to others. The examples were very hands on in learning how to use the system.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alyssa Dominguez</span>
                    <b>|</b>
                    <span class="company">AcademyX</span>
                </h3>
                <p itemprop="reviewBody">Carol is absolutely fantastic! Very thorough, patient and a wonderful instructor. I honestly can't believe how much I learned in such a short amount of time. Can't wait to take the next level of the PowerPoint course. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rowena Docuyanan</span>
                </h3>
                <p itemprop="reviewBody">Thank you so much Allyncia!  This was a very helpful training.  I enjoyed the small class structure as well as the pace of the instruction.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Annmarie Mignini</span>
                    <b>|</b>
                    <span class="company">LAWA</span>
                </h3>
                <p itemprop="reviewBody">I had the good fortune of being the only student in class today and Carole catered the class to meet my needs.  We reviewed everything I needed, plus we covered all of Part 2 with practice!! Carole has all the knowledge and patience of a master teacher.  A great experience - thank you. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carole Dalcourt</span>
                </h3>
                <p itemprop="reviewBody">Learned beyond expectations as to all the functions and tools of Power point including inclusion of a You Tube video. Thank you</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kathy Bright</span>
                    <b>|</b>
                    <span class="company">GC America Inc</span>
                </h3>
                <p itemprop="reviewBody">This class was very informative and Rich did a wonderful job answering ALL of the questions that were asked.  He didn't make "me" feel like I was bothering him with ALL of the questions that I had.  I was even able to ask him questions about other Office Applications and he was more than willing to answer them for me.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cynthia LeShay</span>
                </h3>
                <p itemprop="reviewBody">Thank you!  Very enjoyable and professional experience.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jessica Dekruiff</span>
                    <b>|</b>
                    <span class="company">Veolia Water</span>
                </h3>
                <p itemprop="reviewBody">GREAT follow up to level one. Reiterated concepts to make sure we took in lessons from session 1 and introduced new concepts. Personalized attention made it extremely helpful for asking questions about real problems I've encountered. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">James Haenes</span>
                    <b>|</b>
                    <span class="company">YMCA USA</span>
                </h3>
                <p itemprop="reviewBody">Great job, Leah! I had self-taught myself prior to taking your class but have learned a lot of basics that I let slip through the cracks. Your instruction style and pace are highly commendable. I enjoyed it, and I'll be able to design better presentations quickly and efficiently. --James H. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Drew Moore</span>
                    <b>|</b>
                    <span class="company">Educational Insights</span>
                </h3>
                <p itemprop="reviewBody">Excellent session, with good practical advice, beyond just the instruction manual. Good delivery and friendly atmosphere.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

