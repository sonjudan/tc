<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Premiere Pro Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Premiere Pro  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/premiere-pro-training.php">Premiere Pro  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>364 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Candice Lee</span>
                    <b>|</b>
                    <span class="company">Nationwide Insurance</span>
                </h3>
                <p itemprop="reviewBody">Rob is fantastic! My knowledge of Premiere was very basic. I'm leaving this course with an enhanced understanding of keyboard shortcuts, editing tricks and how to correct bad footage. I hope to be back next year to take his Adobe After Effects course. Thank you so much!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Meredith Lee</span>
                    <b>|</b>
                    <span class="company">CBS Chicago</span>
                </h3>
                <p itemprop="reviewBody">Rob was an amazing teacher. He really took the time to go over lessons and go over topics in detail. We had a lot of questions and he was excellent at answering every one of them. I learned a lot and look forward to using it at work. Thanks so much!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tabi Prochazka</span>
                    <b>|</b>
                    <span class="company">Panhandle Public Health District</span>
                </h3>
                <p itemprop="reviewBody">I learned enough in the first three hours to make it worth the trip.  The instructor is fabulous and very field knowledgeable - gave us some wonderful tips and ideas.  Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jordan Catapano</span>
                    <b>|</b>
                    <span class="company">This Girl Walks Into a Bar</span>
                </h3>
                <p itemprop="reviewBody">McKinley was a fabulous instructor and I never ever could have learned what she taught on my own, or with online tutorials. Thank you so much!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Paul Pinner</span>
                    <b>|</b>
                    <span class="company">Boeing Company</span>
                </h3>
                <p itemprop="reviewBody">I came into this class knowing nothing and McKinley did a great job explaining the video work flow to me. I feel very confident that I could create a project that I would be proud of.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cornelius Mack</span>
                    <b>|</b>
                    <span class="company">CBS Chicago</span>
                </h3>
                <p itemprop="reviewBody">Enjoyed working with Rob.  He as pleasant and informative. His style of teaching was conducive to those of us who have to convert old habits into new formats. He was very enthusiastic about the software and really dug into the nuances that will help us absorb the info.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Peter Brown</span>
                </h3>
                <p itemprop="reviewBody">Kristian killed it. The whole course was super useful and had a great exercises and examples. Kristian new every topic to a tee and helped everyone with whatever they needed... He created a fun environment to learn in and it was epic! Stoked!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nick Blaising</span>
                    <b>|</b>
                    <span class="company">Nick Blaising Photography LLC</span>
                </h3>
                <p itemprop="reviewBody">The course exceeded my expectations as far as the amount of in depth information that I obtained in Premiere After Effects and Even Audition. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Gail Braymen</span>
                    <b>|</b>
                    <span class="company">First Army</span>
                </h3>
                <p itemprop="reviewBody">This is my second class with instructor Rob Schultz, and I am just as pleased this time as I was the first time. Not only is he extraordinarily knowledgeable, but patient and considerate, as well. Attending the After Effects and Premiere Pro bootcamps has boosted my efficiency and skillset enormously and been a huge payoff for my organization. I recommend these classes to everyone I know!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brian Walker</span>
                    <b>|</b>
                    <span class="company">Auxsable Liquid Products</span>
                </h3>
                <p itemprop="reviewBody">Rob was very patient with all of us. If any of us had issues or questions, he stopped to help. His knowledge of the software was amazing.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Dan Camarillo</span>
                    <b>|</b>
                    <span class="company">IVS Computer Technology</span>
                </h3>
                <p itemprop="reviewBody">Thank you for the oppurtunity.  McKinley Marshall is a true professional and expert at the class I was in.  She had throuogh knowledge as well as many different ways of doing each .</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                F
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Frank Blanquet</span>
                    <b>|</b>
                    <span class="company">San Bernardino Community College Dist.</span>
                </h3>
                <p itemprop="reviewBody">Great Training. Kristian answers questions and shows great patience. Goes above and beyond to offer one on one help during breaks, lunch and abit of time after the session.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bradley Shapiro</span>
                    <b>|</b>
                    <span class="company">Lumenbrite Training</span>
                </h3>
                <p itemprop="reviewBody">The fact of the matter is that there's just a lot of topics to discuss within each Adobe program. With that in mind, my instructor did a fantastic job providing summaries for each of the tools within each application. When I entered this class, I already had a pretty large experience with the Adobe Suite, especially Premiere. So, for a newcomer, this class is perfect! For someone like me who just wanted a more thorough understanding of certain processes, this class was also great! There were one or two topics that I wish I had more time to discuss (namely color correction), but I don't think that that's a fault of the teacher. He had a lot to cover as is, and he did a phenomenal job teaching the course! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jacquelyn Soria</span>
                    <b>|</b>
                    <span class="company">County of Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">McKinley is an amazing instructor. It's my 2nd time in her class and she rocks it. I have to say she is one of my favorites. She is very helpful and knowledgeable. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maurice Givens</span>
                    <b>|</b>
                    <span class="company">Springfield Baptist Church</span>
                </h3>
                <p itemprop="reviewBody">I have been very pleased with McKinley.  Very informing, and answered my question in the detail that I could understand.  I would gladly recommend this training for the novice as well as the professional.  All will gain much knowledge.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Z
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Zoila Gonzalez</span>
                    <b>|</b>
                    <span class="company">CBS Chicago</span>
                </h3>
                <p itemprop="reviewBody">Great Experience. Can't wait to use what I learned.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tom Dougherty</span>
                    <b>|</b>
                    <span class="company">Endeavor Communications</span>
                </h3>
                <p itemprop="reviewBody">Rob is an awesome trainer. He is very knowledgeable and very patient! I would recommend the training connection to anyone!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Robert Weigand</span>
                </h3>
                <p itemprop="reviewBody">The course covered material in a very practical format from the ground up (basics to advanced methods). The teacher was clear and receptive to all questions, and also made sure to clarify and answer to the best ability. He came off as well-versed and I would gladly take another course with him.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lonny Dill</span>
                    <b>|</b>
                    <span class="company">Sharktooth</span>
                </h3>
                <p itemprop="reviewBody">I really thought MCkinly did a great job.  My favorite is the tricks and tips that I feel could help separate my work from the average editor.  She really covered some technical information that I would have never thought that the course would have covered.  A big Thumbs Up!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ryan Davis</span>
                </h3>
                <p itemprop="reviewBody">Kristian is a veritable Premiere Pro God.  There wasn't one question that he wasn't able to answer and I feel like I can competently work on editing film and video.  He provided career advice and advised on realistic goals.  I will definitely keep in contact with my instructor and refer others who are eager to learn to training connection.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelvin Thomas</span>
                    <b>|</b>
                    <span class="company">CDCR</span>
                </h3>
                <p itemprop="reviewBody">This class was great. Kristian is an Awesome instrutor. He gave me the proper insight into this software that makes me want to continue learning an growing in it. I want to study under him again. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tamara Belgard</span>
                    <b>|</b>
                    <span class="company">Cascade Corporation</span>
                </h3>
                <p itemprop="reviewBody">McKinley is a wonderful instructor. Her mastery of the subject and friendly but confident teaching style made her class a real pleasure to participate in. I am inspired to take what I have learned and see what I can do on my own. Hopefully in the future, I'll have the opportunity to take additional classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Scott Tucker</span>
                    <b>|</b>
                    <span class="company">SKU WIZ</span>
                </h3>
                <p itemprop="reviewBody">McKinley is an astute, knowledgeable instructor specifically in the category of the media for which we are training.  She obviously knows her stuff.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mark Lefkowitz</span>
                </h3>
                <p itemprop="reviewBody">Mckinley was very knowledgeable and answered all my questions and tailored the course exactly to my needs. Her scope and speed were perfectly aligned to my ability to absorb and gain the maximum possible for the time we were in class. I leave with all the fundamentals and more so I can experiment to my creative desires. I will return for other advanced classes. Thank You Mark Lefkowitz</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Debra Tolchinsky</span>
                    <b>|</b>
                    <span class="company">Northwestern University</span>
                </h3>
                <p itemprop="reviewBody">Rob knows the program and is extremely patient/calm. If you don't understand a concept he looks for an alternative way to explain. He doesn't appear easily flapped even when you forget the same thing he just explained for the 100th.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Robert Thompson</span>
                    <b>|</b>
                    <span class="company">CBS Chicago</span>
                </h3>
                <p itemprop="reviewBody">Great course, it certainly creates a good baseline for those of us who have never used Adobe products.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.0</h3>

                    <div class="star-rate star-rate-sm star-rate-4_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Amanda Gordon</span>
                    <b>|</b>
                    <span class="company">Federal Reserve Bank of Chicago</span>
                </h3>
                <p itemprop="reviewBody">Rob Shultz was a wonderful instructor.  He was very informative, knew the products and was able to answer our questions in-depth and fully. While the class covered a lot of information there was a good balance of instruction and hands-on activities. I would recommend this class and instructor to fellow co-workers and others. I was impressed with the level of professionalism and knowledge this training facility and instructor brought. I am very pleased with the level of service and will be able to use this knowledge when I get back to the office. I look forward to the next training class I get to take here. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shaun Lee O?Sullivan</span>
                    <b>|</b>
                    <span class="company">O'Sullivan Pictures</span>
                </h3>
                <p itemprop="reviewBody">The Adobe Premiere Bootcamp was awesome. A ton of information presented clearly and accessibly. The hands on approach allowing you to follow step by step is priceless. And the instructor, Kristian, was always patient and happy to slow down or repeat anything as needed. He constantly checked with us to make sure we were following along, asking if we had any questions. He also volunteered his time outside of the classroom hours to discuss any other specifics etc. At the beginning of the week it seemed like an overwhelming amount to learn, but each step unlocks your understanding and it all starts falling into place. Highly reccommended!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Abel Sanchez</span>
                    <b>|</b>
                    <span class="company">CBS Chicago</span>
                </h3>
                <p itemprop="reviewBody">Rob had the perfect demeanor and style for our group based on our experience with other editing programs. I truly enjoyed the course. Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Laura Sheehy</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">McKinley is fantastic. I loved her energy and she really understands the program and has a passion for editing that is obvious and really helpful as we progressed through the class. I will definitely be back to take from her again. GREAT boot camp! Please thank her :) </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

