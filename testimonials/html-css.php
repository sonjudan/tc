<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">HTML/CSS Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our HTML/CSS  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/html-training.php">HTML/CSS  classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>308 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joshua Miller</span>
                </h3>
                <p itemprop="reviewBody">I just took the HTML/CSS course with Chris at the Chicago location and couldn't be happier.  He explained everything in a way that was fun and easy to understand.  At first I was just looking into front end, but now I will be looking into taking the PHP and Java script courses as well.  This is a great place to learn in a relaxed environment.  I highly recommend Training Connection to anyone looking into getting into programming.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christine Radley</span>
                    <b>|</b>
                    <span class="company">Bizzuka</span>
                </h3>
                <p itemprop="reviewBody">Chris was great!  He was very knowledgeable and was able to break down items and re-word them when I expressed confusion or interest in further details on a particular item.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brynn Hammond</span>
                    <b>|</b>
                    <span class="company">Institute for Applied Management &amp; Law, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Jeff was an excellent instructor! He made it very simple to understand the information. I would definitely take another training course with The Training Connection just because of this class and the instructor. I am a beginner with HTML/CSS and feel that in the last 3 days I have learned a significant amount of information I can take with me. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maggie Hayes</span>
                    <b>|</b>
                    <span class="company">Peerless AV</span>
                </h3>
                <p itemprop="reviewBody">My instructor was extremely thorough and educated on HTML &amp; CSS and did a great job teaching such a vast subject within three short days. He was helpful when we had questions and made sure we understood the concepts.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Richard Loos</span>
                    <b>|</b>
                    <span class="company">Froeter Design Company</span>
                </h3>
                <p itemprop="reviewBody">HTML/CSS class with Chris L. He was a fantastic teacher. I would absolutely without a doubt take another class with him. I'm going to be taking the Javascript/jQuery class in a month and look forward to learning more.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anny Jamieson</span>
                    <b>|</b>
                    <span class="company">Radiological Society of North America</span>
                </h3>
                <p itemprop="reviewBody">Really enjoyed having Chris as my instructor. He was enthusiastic and kept me engaged. We covered a lot of material at a good pace. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Adam Moore</span>
                    <b>|</b>
                    <span class="company">Craig Bachman Imports, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the class. It was very informative and the instructor (Chris) was very knowledgeable and patient. I would recommend this class to anyone that is wanting to learn more about html/css.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristin Finch</span>
                    <b>|</b>
                    <span class="company">CAbi</span>
                </h3>
                <p itemprop="reviewBody">Jeff is an amazing instructor! He is extremely knowledgable and engages with the students. I would definitely take another class from him...hoping to come back for the Dreamweaver class! Highly, highly recommend Training Connection to learn HTML/CSS and learn it quickly!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nicole Larson</span>
                    <b>|</b>
                    <span class="company">Premier Farnell</span>
                </h3>
                <p itemprop="reviewBody">I learned a lot in this class and will be able to take with me to use at work everyday.  I think a few of the other students were catching on a bit faster than I was - but I think will be a matter of me jumping in and using HTML more in everyday work tasks.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rebecca Rasmussen</span>
                    <b>|</b>
                    <span class="company">Best Case Solutions</span>
                </h3>
                <p itemprop="reviewBody">Great overview of html and gives you a platform to use various tools including Dreamweaver etc. Enjoyed the course and would recommend it to others just starting out. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Roger Lee</span>
                    <b>|</b>
                    <span class="company">UCLA</span>
                </h3>
                <p itemprop="reviewBody">I am very pleased with this course and the instructor. I have learned much more than I had expected and feel that I'm leaving this course with a strong enough base to move forward in HTML. Instructor (Jeff Wiatt) was excellent in his approach and lessons.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Meghan Carrel</span>
                    <b>|</b>
                    <span class="company">Celtic Marketing, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Prior to coming to this class I wasn't sure if I would be able to walk away and feel confident about designing a web site from scratch and be able to retain all the info that would be discussed in only two days. Rick did an amazing job relaying all this info and breaking it down so that it was easy to understand. I highly recommend taking his class, his teaching approach was more than helpful and successful in the fact that I now feel confident in creating my own code.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mike Haleas</span>
                    <b>|</b>
                    <span class="company">Hallmark Data Systems</span>
                </h3>
                <p itemprop="reviewBody">The course was very informational and helpful. It was easy to follow and will help in my current work projects. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sherree Lewis</span>
                    <b>|</b>
                    <span class="company">CAbi</span>
                </h3>
                <p itemprop="reviewBody">Jeff does an amazing job of explaining concepts as opposed to simply telling you how to do something, which makes it so much easier to learn!  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristi Herd</span>
                    <b>|</b>
                    <span class="company">hybris software</span>
                </h3>
                <p itemprop="reviewBody">Great class, I loved it. Jeff is a great instructor. This will help me in my job immensely. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Patrick Bologa</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Fantastic class!  I loved everything about it.  Jeff clearly explained all concepts and was very patient.  I would take any class he was teaching in a heart beat.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Neel Sukhadia</span>
                    <b>|</b>
                    <span class="company">Classified Ventures</span>
                </h3>
                <p itemprop="reviewBody">Excellent, above and beyond coverage of the curriculum with very good customization to meet student's individual needs. The trainer was very experienced not only in their subject matter but also in teaching it in a very effective manner and was consistently able to understand my questions and answer them to my complete satisfaction! Keep up the good work!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Glenda Singleton</span>
                    <b>|</b>
                    <span class="company">U.S. Department of Energy</span>
                </h3>
                <p itemprop="reviewBody">I thoroughly enjoyed the real world experiences and applicability to our specific issues on the topic.  Chris made each of our issues important and applicable to the content we were going over. The class was relaxed, yet extremely intensive and packed with information. Thank you! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Aimee Deegan</span>
                    <b>|</b>
                    <span class="company">Panduit</span>
                </h3>
                <p itemprop="reviewBody">Gabriel covered a lot of material in the two days of training. He provided excellent examples each step of the way;  effectively guiding us through the steps. I came into this class thinking the subject matter was something I could never grasp. I am leaving with a sound understanding of what HTML is and how,combined with CSS, I am able to design for the web, </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joshua Miller</span>
                </h3>
                <p itemprop="reviewBody">I took the HTML/CSS with Chris at the Chicago location and couldn't be happier.  He explained everything in a way that was fun and easy to understand.  Before this class I was just interested in the front end aspect of programming, but I will now look into taking the PHP and Javascript courses.  This is a great place to learn with a relaxed enviroment.  I highly recommend Training Connection to those looking at getting in programming.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lisa Timmons</span>
                    <b>|</b>
                    <span class="company">Children's Hospital Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">I would highly recommend this class to anyone with an interest in HTML and CSS. The level of experience in our class was varied, but I think the instructor did a great job of reaching each audience.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Adam Papinchock</span>
                    <b>|</b>
                    <span class="company">Alliant Credit Union</span>
                </h3>
                <p itemprop="reviewBody">Gabriel was awesome!  He was very hands on and patient.  He showed us everything a beginner would need to know to work with HTML and building a website from scratch.  While it was a ton of information, it was very informative and we touched on what he was teaching us constantly throughout the course.  He brought real world examples to what we were learning.  Not only was he a good instructor, but he had magic...literally!!  He did a couple tricks and amazed us all!  They should call him The Fabulous Gabe =o)</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Emmanuel Masongsong</span>
                    <b>|</b>
                    <span class="company">UCLA</span>
                </h3>
                <p itemprop="reviewBody">Jeff is excellent, very friendly and we had lots in common so learning was very relaxed and pleasant.  He wasn't feeling well but still put in 100% and kept us ahead of schedule.  I would highly recommend this course to others, especially to have Jeff as instructor.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jeff Baxter</span>
                    <b>|</b>
                    <span class="company">LEK Consulting</span>
                </h3>
                <p itemprop="reviewBody">I was the only student in the class which was somewhat unusual, but it worked out well.   The instructor had a great command of the subject matter and was very flexible in matching the course progress to my skill level.  This will be very helpful to increasing my efficiency at work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Seema Bajaj</span>
                    <b>|</b>
                    <span class="company">Marshall Gerstein &amp; Borun</span>
                </h3>
                <p itemprop="reviewBody">Rick did a great job! He was able to break complex subject matter into an easy to understand format. His examples were based on real world examples which made them easy to relate to. I would highly recommend this class to anyone looking to learn about HTML coding!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jason Vargas</span>
                    <b>|</b>
                    <span class="company">Putman Media Inc.</span>
                </h3>
                <p itemprop="reviewBody">I thought that Walter did a fantastic job teaching us about HTML. I knew a bit about HTML but he made it fun and very interesting; and brought my knowledge of HTML up-to-date. He didn't over-complicate the topic and brought it to a world that we would understand. Thanks Walter.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Chris Lane</span>
                </h3>
                <p itemprop="reviewBody">Jeff was a terrific instructor and made learning HTML and CSS code fun. I will definitely be back for the Dreamweaver class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Robert Marshall</span>
                    <b>|</b>
                    <span class="company">KLM Labs</span>
                </h3>
                <p itemprop="reviewBody">This was an awesome class.  I can't wait to go back to my work and improve our web front.  Jeff is a great teacher with an excellent ability to describe the difficult concepts and properties of HTML / CSS.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin Equitz</span>
                    <b>|</b>
                    <span class="company">Experian</span>
                </h3>
                <p itemprop="reviewBody">Rick did a great job, I really feel optimistic and well-equipped with what I've learned here. Everyone here has been courteous and helpful. A great experience all around. Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Suzanne OReilly</span>
                    <b>|</b>
                    <span class="company">American Academy of Orthopaedic Surgeons</span>
                </h3>
                <p itemprop="reviewBody">I started course not truly understanding how html structure relates to website and email design. I now feel confident reviewing html and applying skills I've learned by Chris in a website and email environment. Thank you! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

