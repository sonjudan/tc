<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Business Etiquette  Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our Business Etiquette  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/business-etiquette-training.php">Business Etiquette  classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>149 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anna Grimm</span>
                    <b>|</b>
                    <span class="company">CJ Corp</span>
                </h3>
                <p itemprop="reviewBody">I am so glad I came in for this refreshing course. I am taking away valuable lessons in communication and a better sense of where I am going and what I am capable of in the world of business. I will definitely be taking more courses through Training Connection.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristina McLain</span>
                    <b>|</b>
                    <span class="company">America's Auto Body Inc</span>
                </h3>
                <p itemprop="reviewBody">I was very pleased with the overall conduction of this course.  Juana was fantastic! I would definitely recommend this course to my colleagues, and look forward to taking another class with Juana in my near future.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stephanie Chavez</span>
                    <b>|</b>
                    <span class="company">preferred freezer</span>
                </h3>
                <p itemprop="reviewBody">Carol was a blessing and wonderful instructor. She made me feel very comfortable and I am walking out today with a better understanding on how to carry myself in the business world. Really appreciate the class overall and would highly recommend it to someone or friend. Thank you, Stephanie Chavez </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kendra Rowland</span>
                    <b>|</b>
                    <span class="company">Ace Hardware</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed Juana's class. She has a wonderful, engaging personality that really lent to the topic. We broached many aspects of Communication and Business Etiquette. I think she does a great job and I have a lot to take back personally and to my team.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Pat Mahoney</span>
                    <b>|</b>
                    <span class="company">Hill Mechanical Operations</span>
                </h3>
                <p itemprop="reviewBody">Once again this instructor (Juana) has given me the necessary tools to succeed. "Silence is wisdom when applied correctly" There is only one direction for my success is up. Thank you PAT </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christina Martinez</span>
                    <b>|</b>
                    <span class="company">Illinois Tool Works</span>
                </h3>
                <p itemprop="reviewBody">Class was very good, great examples and great instructor, would take her courses as much as needed.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">David Troy</span>
                    <b>|</b>
                    <span class="company">Cancer Treatment Centers of America</span>
                </h3>
                <p itemprop="reviewBody">Covered the exact amount of information needed, didn't dwell on things that were irrelevant.  Good instructor.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christopher Livingston</span>
                    <b>|</b>
                    <span class="company">Steel Warehouse</span>
                </h3>
                <p itemprop="reviewBody">Great instructor well versed!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John E. Jones</span>
                    <b>|</b>
                    <span class="company">Los Angeles World Airports</span>
                </h3>
                <p itemprop="reviewBody">Very courteous and knowledgeable instructor who delivered the information in a way that was very easy to understand.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tom Louzon</span>
                    <b>|</b>
                    <span class="company">The Strive Group</span>
                </h3>
                <p itemprop="reviewBody">Lena is an excellent coach/teacher and I would recommend her to anyone. She was both knowledgeable and fun to work with during the training course. She's certainly the best of the best. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shawn Clancy</span>
                    <b>|</b>
                    <span class="company">Link121</span>
                </h3>
                <p itemprop="reviewBody">Allencya was awesome, highly skilled and articulate.  I loved when she had us circle up in chairs or stand for exercises. Perhaps because of my experience (58 years old) I was looking for more than just common stuff which is pretty much how I experienced the class.  Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.2</h3>

                    <div class="star-rate star-rate-sm star-rate-4_2">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jill Rupprecht</span>
                    <b>|</b>
                    <span class="company">Pauline Center for Media Studies</span>
                </h3>
                <p itemprop="reviewBody">I thought Allyncia was a great teacher for this topic.  We had a great one-on-one experience with conversation and questions and examples. I would definitely recommend this course to others.  Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bree Walrath</span>
                    <b>|</b>
                    <span class="company">Spierer Woodward Corbalis and Goldberg</span>
                </h3>
                <p itemprop="reviewBody">I would take another course from this instructor and this company again.  Everything was great!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">TANYA MUNCEY</span>
                    <b>|</b>
                    <span class="company">BRASSTECH INC</span>
                </h3>
                <p itemprop="reviewBody">Allyncia provided a great class, learned very many new things that I was never aware of. Will incorporate these into my daily work &amp; home life. It is also good knowledge to pass down to the kids in my household. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sarah Geissler</span>
                </h3>
                <p itemprop="reviewBody">Juana was absolutely amazing.  I enjoyed the way she conducted her class - very professional yet personable - and she touched on everything I could have expected and more.  I am confident I will thrive with the insight, wisdom, and advice she shared with me and the knowledge she gave me.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Steven Shawver</span>
                    <b>|</b>
                    <span class="company">Marc Realty</span>
                </h3>
                <p itemprop="reviewBody">I came into this course to learn about me as well as what i can do to be better at my job, it has been  excellent so far !!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stuart Grinell</span>
                    <b>|</b>
                    <span class="company">Ambassador Education Solutions</span>
                </h3>
                <p itemprop="reviewBody">Juana made the class enjoyable and easy to digest</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stacey Pitts</span>
                    <b>|</b>
                    <span class="company">Nye Partners In Women's Health</span>
                </h3>
                <p itemprop="reviewBody">I very much enjoyed this training course and would highly recommend this course to anyone. I thoroughly enjoyed my instructor and look forward to any other courses.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Grant Crosslin</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the class. I have a lot of experience in business, and this class provided me with some new ideas for presenting and my speaking.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Keith Lipman</span>
                    <b>|</b>
                    <span class="company">BigHand Inc</span>
                </h3>
                <p itemprop="reviewBody">I know I put strongly agree for the questions but Rich did an excel-ant job. Rich was well spoken and was solid with the content material. i would defiantly recommend this class  to others especially for college students</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maria Perez</span>
                    <b>|</b>
                    <span class="company">American College of Healthcare Executives</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed this course. I have attended several courses but none which I learned new ways to help me with my daily tasks. I am looking forward to attending another course in August and I will highly recommend this course to others. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Damaris Soto</span>
                    <b>|</b>
                    <span class="company">Weber Stephen Products Co.</span>
                </h3>
                <p itemprop="reviewBody">I am very glad that i attended this course. i definitely learned a lot and recommend this course to others. The instructor was a very nice friendly detailed oriented instructor. Took the time to explain in detail step by step. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lamya AlShammary</span>
                </h3>
                <p itemprop="reviewBody">This is a great experience, great time spend with valuable information and instructions.. the best part is the exercises part, when Ms. Juana ask as to talk more about ourselves.. our goals in life, our mission and our fear and how to improve  the way we speak and the way we act in business field.. thank you a lot and I wish to see you and attend your training sessions on another time.. thank you  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sarala Bindu</span>
                    <b>|</b>
                    <span class="company">Siemens</span>
                </h3>
                <p itemprop="reviewBody">Juana was a pleasure to interact with. I learned so much from her , not just about being "proper" at work, but also about being a good human being!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nicole Hatcher</span>
                    <b>|</b>
                    <span class="company">Baxter Credit Union</span>
                </h3>
                <p itemprop="reviewBody">Juana was a phenomenal instructor! She was well versed in all the topics and was able to clarify the questions I had. I am going to recommend this course to my organization.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Angela Zhang</span>
                    <b>|</b>
                    <span class="company">NVA</span>
                </h3>
                <p itemprop="reviewBody">Great course. Instructor has very high energy. Enjoyed session on how to introduce people and exit/enter conversations. The practice / acting out networking scenarios was very helpful, would enjoy more of that. Would enjoy more training on how to modulate voice in a business setting, especially for women.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.1</h3>

                    <div class="star-rate star-rate-sm star-rate-4_1">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

