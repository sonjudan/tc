<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Business Writing Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our Business Writing  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/business-writing-training.php">Business Writing  classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-5 star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>463 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jarred Jernigan</span>
                    <b>|</b>
                    <span class="company">After School Matters</span>
                </h3>
                <p itemprop="reviewBody">I highly recommend Training Connections as a resource to improve and increase writing skills.   My instructor, Allyncia was wonderful. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lindsay Parilman</span>
                    <b>|</b>
                    <span class="company">HIMSS</span>
                </h3>
                <p itemprop="reviewBody">This course was excellent and very thorough.  I was very pleased with the attention given by the instructor and the materials available.  The addition of information materials and instructors contact information was phenomenal. Thank you very much! Lindsay</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carlos Aguilar-Solis</span>
                    <b>|</b>
                    <span class="company">County of Los Angeles - ISD</span>
                </h3>
                <p itemprop="reviewBody">Amazing trainer! Would definitely take another course with her. Presents very well, she is very well informed and she always smiles! :)</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Burkhead</span>
                    <b>|</b>
                    <span class="company">HCVT</span>
                </h3>
                <p itemprop="reviewBody">Ms. Carol is amazing. She brings energy that is hardly ever used anymore. No one looks forward to training especially when it takes up the entire day. However, I am very pleased to have attended. Thank you so much!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tarella Crawford</span>
                    <b>|</b>
                    <span class="company">Clarity Group Inc</span>
                </h3>
                <p itemprop="reviewBody">Juana was a great instructor. Pace of her teaching style was warm, open, inviting and clear. I feel confident that I can apply this in my life. Great job!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mustafa Miskinyar</span>
                    <b>|</b>
                    <span class="company">Comp West Insurance</span>
                </h3>
                <p itemprop="reviewBody">Really enjoyed this class and the instructor, she kept it fun yet educational. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brian Wall</span>
                    <b>|</b>
                    <span class="company">Orbitz</span>
                </h3>
                <p itemprop="reviewBody">I got a great deal more than initially expected out of this course.  The instructor's energy, knowledge and positive attitude provided the perfect atmosphere for learning.  I appreciated all of the real life applications and personal insights that were provided.  I would strongly recommend this course and instructor to anyone looking to brush up on their writing skills. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Katie Stone</span>
                    <b>|</b>
                    <span class="company">Kohler Interiors Group</span>
                </h3>
                <p itemprop="reviewBody">I have thoroughly enjoyed this course and would recommend it to anyone looking for business writing skills.  It has been energetic and informative.  Jeanette is amazing!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jelica Augustus</span>
                    <b>|</b>
                    <span class="company">County of Los Angeles - ISD</span>
                </h3>
                <p itemprop="reviewBody">The Instructor was very good, engaging</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Miyako Matsumura</span>
                    <b>|</b>
                    <span class="company">The Japan Foundation</span>
                </h3>
                <p itemprop="reviewBody">It was a great workshop.  The instructor, Allyncia was excellent.  I had no pressure or stress in a classroom.  I got a lot of essential ideas of writing and communicating in Business in English. Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jia Kimoto</span>
                    <b>|</b>
                    <span class="company">ICANN – Internet Corporation for Assigned Names and Numbers</span>
                </h3>
                <p itemprop="reviewBody">Ms Carol Spear provided helpful tips and gave real-world examples to the classroom. I would highly recommend this class to anyone who wants to improve their business writing skills. She listened to the classmates and customized the course to our needs. I would love to take more of her classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jordan Bradshaw</span>
                    <b>|</b>
                    <span class="company">ABOMS</span>
                </h3>
                <p itemprop="reviewBody">This is hands down one of the best classes I have taken. The instructor was extremely helpful and listened to our individual reasons for being here and catered the lesson plan to help benefit that. Thank you so much Ms. Wooldridge.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alleen Wilson</span>
                    <b>|</b>
                    <span class="company">CompWest Insurance</span>
                </h3>
                <p itemprop="reviewBody">Allyncia is an engaging, energetic instructor.  I enjoyed her course and learned real life scenarios applicable to my job.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alicia St. Clair</span>
                    <b>|</b>
                    <span class="company">Center for Economic Progress</span>
                </h3>
                <p itemprop="reviewBody">Juana is a great instructor. I was the only student in the class, which was great because I was able to get one on one training and great pointers on my writing skills. I truly appreciated the training.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin Pearson</span>
                    <b>|</b>
                    <span class="company">Los Angeles Fire Department</span>
                </h3>
                <p itemprop="reviewBody">I learned a lot.  I plan to apply techniques taught throughout the course.  Instructor answered all related questions.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                W
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">William Craig</span>
                    <b>|</b>
                    <span class="company">Funai</span>
                </h3>
                <p itemprop="reviewBody">The class was better than anticipated; Allyncia provided me with tools that I can immediately utilize. Her real world expertise was relevent to what I was looking for. I was able to use one of our class exercises to negotiate an opportunity with my organization.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin B Dryman</span>
                    <b>|</b>
                    <span class="company">Federal Reserve Bank of Chicago</span>
                </h3>
                <p itemprop="reviewBody">Allyncia was an EXCELLENT instructor.  She kept the class well informed as well as keeping the class interesting and fun!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Raquel Kalfus</span>
                    <b>|</b>
                    <span class="company">Aboms</span>
                </h3>
                <p itemprop="reviewBody">I learned a lot from this course, many information can be applied to personal writing skills. Business writing involves many areas, and the instructor gave great examples about all.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.0</h3>

                    <div class="star-rate star-rate-sm star-rate-4_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Edith Mendoza</span>
                    <b>|</b>
                    <span class="company">Time Warner Cable</span>
                </h3>
                <p itemprop="reviewBody">My instructor was very informative and to the point. I learned a lot from her and I appreciate her patience and the time she took to explain things in detail.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Myria Stanley</span>
                    <b>|</b>
                    <span class="company">American Academy of Orthopaedic Surgeons</span>
                </h3>
                <p itemprop="reviewBody">This course instructor was awesome!  I will use what I've learned in my day to day activities.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rodric Hurdle-Bradford</span>
                    <b>|</b>
                    <span class="company">CIGNA Health Care</span>
                </h3>
                <p itemprop="reviewBody">As a ten-year professional journalist, I was very impressed with the new strategies of written communication taught by Allyncia. The results-oriented focus make the lessons applicable to daily business life and interactions. The communications math formula is a great way to express dialogue, whether it is written or oral. Her tremendous resources with books, websites and authors encourage further learning after the course as well. Excellent course and I will recommend others!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joseph Matz</span>
                    <b>|</b>
                    <span class="company">Segal</span>
                </h3>
                <p itemprop="reviewBody">Juana was a very good instructor and really made the class and learning fun. It was great to review and go back to basics.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.3</h3>

                    <div class="star-rate star-rate-sm star-rate-4_3">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carolina Zhang</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Instructor had many real world experiences to share with the classroom. I would highly recommend this course to my colleagues.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shellriece Parker</span>
                    <b>|</b>
                    <span class="company">Wintrust Mortgage</span>
                </h3>
                <p itemprop="reviewBody">I enjoyed it, this will help me be very effective within my new position.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kim Sallee</span>
                    <b>|</b>
                    <span class="company">Medline</span>
                </h3>
                <p itemprop="reviewBody">I had a number of classes for writings, however this class was very informative. The style was very refreshing.   </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Albert Gonzalez</span>
                    <b>|</b>
                    <span class="company">Feeding America</span>
                </h3>
                <p itemprop="reviewBody">I am very impressed with the level of knowledge, depth and span of the instructor's handle in the subject, not to speak about her high energy and charisma. I am definitely recommending this session to others - thank you so much, I already feel empowered with knowledge!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Patrick Bayard</span>
                    <b>|</b>
                    <span class="company">Grand Victoria Foundation</span>
                </h3>
                <p itemprop="reviewBody">This course exceeded my expectations.  It is a good resource for professional people .</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">GuanJr Guo</span>
                    <b>|</b>
                    <span class="company">Guo GuanJr Film</span>
                </h3>
                <p itemprop="reviewBody">The class with Carol is perfect! It made me have further knowledge in business writing. I do think structure and how you draft an email is very important. It helps a lot in my event planning career.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Y
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Yakema Decatur</span>
                    <b>|</b>
                    <span class="company">City of Inglewood</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very enthusiastic and informative.  She provided just the right training to compliment your professional experiences as it pertains to business writing. I would definitely recommend this class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sam Smith</span>
                    <b>|</b>
                    <span class="company">Productivity Point Global</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed the way in which the material was presented.  The environment was very conversational, rather than lecture-based.  Great instructor with great enthusiasm for the topics presented.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

