<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Business Communication Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our Business Communication  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/business-communication-training.php">Business Communication  classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>330 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                H
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Hayley Kuniansky</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Carol was incredible! I came into this experience optimistic that I'd learn something but honestly Carol exceeded my expectations and I intend to take more of her classes! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stephen Schobel</span>
                    <b>|</b>
                    <span class="company">Lasalle Street Securities</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very insightful and answered my questions that I had involving situations concerning confrontations and how to avoid negative conflicts. I am recommending my company to send more people for this class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">David Castillo</span>
                    <b>|</b>
                    <span class="company">Mauser USA LLC</span>
                </h3>
                <p itemprop="reviewBody">i found this course to be highly beneficial to my objectives. I knew from the first 5 minutes of the course that it would aid me in achieving the improvements that i required.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Catalina Gaete</span>
                    <b>|</b>
                    <span class="company">Inheriting Wisdom</span>
                </h3>
                <p itemprop="reviewBody">Juana was very inspirational and very passionate about her training. She clearly cares about the success of her students and used personal and vulnerable stories to connect with the class. Communication is definitely her strong point, and she is engaging in every way. Was a great class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                W
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Walter Palka</span>
                    <b>|</b>
                    <span class="company">Novartis</span>
                </h3>
                <p itemprop="reviewBody">Incredibly refreshing.  Most courses I've taken have been so book-oriented, they lost sight of practical instruction.  I much prefer the activity / example based method of instruction.  Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bruce Kutnick</span>
                    <b>|</b>
                    <span class="company">AccuQuote</span>
                </h3>
                <p itemprop="reviewBody">Excellent course, great instructor!  Real world examples, instructor enthusiasm plus strong interaction with the students made for a great course!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kayla Muscari</a>
                </h3>
                <p itemprop="reviewBody">This was the best experience I have ever had in a class room. This course taught me many things I did not know. Carol even took the time to help me with my phone call anxiety I have! It was fantastic!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michael Carrington</span>
                    <b>|</b>
                    <span class="company">BSSI2 LLC</span>
                </h3>
                <p itemprop="reviewBody">This class was unique in the fact I was the only student , I appreciated the personal touch and pace. The Instructor address any and all questions. She is very professional and is truly a special person. Being the only student allowed for more in depth discussion. THANK YOU!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kendrick Jones</span>
                    <b>|</b>
                    <span class="company">Charles R. Drew</span>
                </h3>
                <p itemprop="reviewBody">GREAT COURSE! The teacher was amazing and the examples were very relatable and understandable! I am very appreciative of this learning experience and will recommend this course to anyone! Thank you so much for this opportunity!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Thomas Schriber, Jr.</span>
                    <b>|</b>
                    <span class="company">Donahue Schriber</span>
                </h3>
                <p itemprop="reviewBody">I would recommend this course to certain people in certain positions.  Especially customer or service orientated positions.  I thought the instructor was great and very enthusiastic.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.1</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jade Dorynek</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Lena was extremely personable, and a great facilitator.  I really enjoyed how the class topics were tailored to our individual needs.  It felt much more like a workshop/breakout session than other traditional professional development classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Amy Venner</span>
                    <b>|</b>
                    <span class="company">Organic by Nature</span>
                </h3>
                <p itemprop="reviewBody">I think the class was of great value and accomplished what I was hoping for with 3 key employees.  I think that each of them recognized things that they were doing and hopefully will take the knowledge learned today to correct the unprofessional behaviors.  Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tina Garcia</span>
                    <b>|</b>
                    <span class="company">Abbvie</span>
                </h3>
                <p itemprop="reviewBody">Juana is brilliant. She was able to cover different types of issues and give relatable answers and suggestions. I would take another class with her. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Bruce Kutnick</span>
                    <b>|</b>
                    <span class="company">AccuQuote</span>
                </h3>
                <p itemprop="reviewBody">fantastic class; the instructor worked with me on real world issues that I am dealing with that will undoubtably help me!  Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Krista Blatt</a>
                </h3>
                <p itemprop="reviewBody">The instructor was very sincere and energetic.  The class is a good starting point to understanding your personal communication style and beginning to learn how to adapt to other people's styles.  I look forward to continuing to practice the techniqes that I learned.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lindsay Pompa</span>
                    <b>|</b>
                    <span class="company">Kane County ITD</span>
                </h3>
                <p itemprop="reviewBody">Allyncia was a wonderful instructor.  This is my second training class with her and I am very impressed by her knowledge.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                W
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Wendy Schneider</span>
                    <b>|</b>
                    <span class="company">Radiant Solutions Corp.</span>
                </h3>
                <p itemprop="reviewBody">The instructor was amazing!  She got us involved and had real world experiences to help us retain the knowledge and see the bigger teacher.  I would definietly take a class again at Training Connection and absolutley take a class with this instructor Jeannette again!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michelle Hennan</span>
                    <b>|</b>
                    <span class="company">Regional Transportation Authority</span>
                </h3>
                <p itemprop="reviewBody">Great day.  Training was geared to focus on each student's particular needs.  I look forward to coming back. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jennifer Mielnicki</span>
                    <b>|</b>
                    <span class="company">Mayer Brown LLP</span>
                </h3>
                <p itemprop="reviewBody">Jenette is an engaging and informational instructor.  She has great knowledge in communication and can provide real world examples to use immediately.  She tailors her class to her students.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jared Johnson</span>
                    <b>|</b>
                    <span class="company">S.G. Capital Management</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very informative on the subject of business communication.  She made the class enjoyable while still touching over the material.  Overall the class was a great experience. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jamie Witvoet</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very incredible! She connects on a personal level, which made learning easy. I would definitely take another class with her! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Karen Nelson</span>
                    <b>|</b>
                    <span class="company">Family Health Network</span>
                </h3>
                <p itemprop="reviewBody">I have sent several of my coworkers to this company for training before I came to a class myself.  They all raved about the class material, the instructors and how much learned from taking the classes.  I would whole-heartedly agree and will make sure to tell everyone that ever needs to take a training class to come here.  Juana was wonderful.  She listened well, made examples that were appropriate to my life and needs and took the time to work on the difficulties that I specifically had. I can't say enough good things about the instructors.  I am walking away from the class with skills that I can really use.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Katy Ann Searcy</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">This class was so personalized and tailored to my needs. She went above and beyond of my expectations! I feel prepared to go back to my workplace with a renewed since of confidence and excitement. We worked to help me communicate with my supervisor which is something I really struggle with and I feel so much better after taking this course. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Armando Molina</span>
                    <b>|</b>
                    <span class="company">emnos</span>
                </h3>
                <p itemprop="reviewBody">As a first timer, I found this course to be extremely helpful in understanding why we (as people in the workplace/business) react and behave the way we do, and how each style applies to me. This course was so great that I am looking forward to a follow up and/or re-take it again. Juana puts everything in proper perspective and makes the course enjoyable. :)</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Erica Denning</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Grandma Carol is magical. She made the class fly by. She is smart and passionate and kind and thoughtful. I wish I could take her home with me. Her class was so thoughtful on all subject matters that I want to send my Assistant here to take her class and even speak to my HR department about possible group session for other colleagues. What she teaches is often over looked with the newer generation of people and she offers value for any growing or tenure professional. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Y
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Yanet Alejandre</span>
                    <b>|</b>
                    <span class="company">Michael Baker Jr., Inc.</span>
                </h3>
                <p itemprop="reviewBody">This class was truly great. I was not expecting to learn so much. Juana was very engaging and I kept wanting to hear more. The concepts I learned I know I will use immediately. I hope to come back to another session soon and learn more.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Crystal Stephens</span>
                    <b>|</b>
                    <span class="company">Organic by Nature</span>
                </h3>
                <p itemprop="reviewBody">Our instructor was awesome, full of energy and positive.  I felt very welcomed and would love to attend more classes with her.  I also learned a lot and hope to implement what I have learned at work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Suky Lawlor</span>
                    <b>|</b>
                    <span class="company">Allianz Global Corporate &amp; Specialty</span>
                </h3>
                <p itemprop="reviewBody">Jeannette did a great job of making the lessons and examples relatable.  I especially liked her conversational tone.  I would highly recommend this class to any of my co-workers or friends.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jacqueline Gutierrez</span>
                    <b>|</b>
                    <span class="company">Robert Wayne Footwear</span>
                </h3>
                <p itemprop="reviewBody">Thank you so much. I really appreciate all the content that was brought up. I would really recommend this class for everyone. I have taken four courses here and it has really been a pleasant experience. Thank you </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sun Chae</span>
                    <b>|</b>
                    <span class="company">Steris</span>
                </h3>
                <p itemprop="reviewBody">This was an amazing class.  Taught me effective ways to communicate to specific target audiences.  Would highly recommend to anyone where effective communication is critical to success of corporate projects!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

