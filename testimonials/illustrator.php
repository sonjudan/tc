<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Adobe Illustrator  Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Adobe Illustrator  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/illustrator-training.php">Adobe Illustrator  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.8 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_8">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>385 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ainee Guevara</span>
                </h3>
                <p itemprop="reviewBody">Kristian was outstanding A+++, I'll definitely take another class with him. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenni Draime</span>
                </h3>
                <p itemprop="reviewBody">Chana is the best teacher! I have learned so much from her and hopefully will take a few more classes with her!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Karim Oyarzabal</span>
                </h3>
                <p itemprop="reviewBody">I was an outstanding training week, Eva my instructor enlightened me in what before this class was a black box, Illustrator is not anymore. I'll come back for Photoshop.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sandra Chavez</span>
                </h3>
                <p itemprop="reviewBody">This is the second course I have taken with Chana and it was the best by far. Illustrator was a program I thought I knew. She opened new avenues to doing work more efficiently and effectively.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Katherine Cresto</span>
                </h3>
                <p itemprop="reviewBody">Chana is a fantastic teacher who knows Illustrator like the back of her hand, maybe better. She is very patient and can answer any question you have. She can explain a concept in five different ways, if necessary. I would recommend her as a teacher to anyone!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Clint Richter</span>
                    <b>|</b>
                    <span class="company">GSI Group</span>
                </h3>
                <p itemprop="reviewBody">Fantastic class!  Eva was very knowledgeable and enthusiastic.  Very fun and very informative.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristen Thies</span>
                    <b>|</b>
                    <span class="company">St. Baldrick's Foundation</span>
                </h3>
                <p itemprop="reviewBody">Beverly is a fabulous teacher who moves at a perfect pace - slow enough to learn and critically think about each step, but not too slow where it's wasted time. I would highly recommend her to someone who is in need of a refresher course, or even someone who has never touched an Adobe product!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jacki Beem</span>
                    <b>|</b>
                    <span class="company">GSG Corporation</span>
                </h3>
                <p itemprop="reviewBody">The entire staff was very friendly and fun, and the instructor was very professional, knowledgeable, and helpful. I would definitely take another class with her, and I would recommend this class to friends.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tearssa Ramirez</span>
                    <b>|</b>
                    <span class="company">Nordson MEDICAL</span>
                </h3>
                <p itemprop="reviewBody">The Illustrator Fundamentals course with Chana was very informative and helpful in learning what Illustrator has to offer. She was very helpful and willing to stop and answer questions or help everyone get up to speed. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jennifer Nally</span>
                </h3>
                <p itemprop="reviewBody">Eva was extremely helpful in all the discussions I had and assisted in questions related to job specific projects.  The class was beneficial in that I'll be able to apply my new knowledge in my day to day marketing projects.  I really enjoyed the class and thankful for the experience.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Devra Juraco</span>
                    <b>|</b>
                    <span class="company">Belcan Engineering</span>
                </h3>
                <p itemprop="reviewBody">Eva Sandor was a fantastic instructor. She was great at breaking down the material to a level that was easy to digest. I was extremely pleased with the course overall!! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Peyton Nealis</span>
                    <b>|</b>
                    <span class="company">Sunset Gower &amp; Sunset Bronson Studios</span>
                </h3>
                <p itemprop="reviewBody">I have now taken the Illustrator and InDesign classes with Chana and I think that she is amazing. I'm so glad that I took her two classes as well as the Photoshop class with Beverly. I'm now hooked to Adobe and I can't wait to practice at home! I wish that you guys had night or weekend classes because I don't think that my work would let me take more time off. :(</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Brown</span>
                </h3>
                <p itemprop="reviewBody">Chana is a great instructor.  She has so many real life experiences that bring all of the exercises together in a way that makes sense for practical application. Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sarah Scanlon</span>
                    <b>|</b>
                    <span class="company">Bodhi Spiritual Center</span>
                </h3>
                <p itemprop="reviewBody">Eva was wonderful. I was incredibly intimidated by Illustrator and by the time I left I felt like I had a great starting point at which to go home and practice my new skills. She made it accessible, fun, and a comfortable environment to ask questions in. I always felt like I had the right amount of attention when I needed it. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nancy Berger</span>
                </h3>
                <p itemprop="reviewBody">Retook Illustrator with Kristian and it was so worth it! The concepts I learned the first time were definitely reinforced and extended. Kristian is awesome and so willing to help and go over things when anyone needs help. Thanks!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                H
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Heather L. Jencks</span>
                    <b>|</b>
                    <span class="company">Strategic Ascent, Inc.</span>
                </h3>
                <p itemprop="reviewBody">This was my third class with Eva at the Training Connection.  As with the previous two courses, she proved to be extremely knowledgeable, patient and helpful. The materials provided with the course are very helpful, too. I am also impressed with the Chicago Training Connection facilities and receptionist staff. Everything/one was very professional and kind. Thank you! I will recommend TC to others. It was well worth it!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mary Ann Gogots</span>
                    <b>|</b>
                    <span class="company">Carefusion</span>
                </h3>
                <p itemprop="reviewBody">This is the second time I took this class &amp; both times I was very impressed with the training concept &amp; the trainer. She is very knowledgeable &amp; answered all questions that were asked patiently.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tara Roman</span>
                </h3>
                <p itemprop="reviewBody">This is my second class with Eva, and once again she was terrific. Her teaching method is engaging and she takes the time to explain each step in a very clear and concise manner. I never thought I would conquer Illustrator, but thanks to her, I have.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Nastasia Tebeck</span>
                    <b>|</b>
                    <span class="company">Coyote Logistics</span>
                </h3>
                <p itemprop="reviewBody">Illustrator is definitely a meaty program and Eva was able to streamline the basics very well while adding in further detail of how to maximize the use of all the tools. I can't wait to dive in further on my own time and really master this program. I also look forward to taking any other advanced Illustrator courses Training Connection may offer in the future. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joel Loera</span>
                    <b>|</b>
                    <span class="company">American National</span>
                </h3>
                <p itemprop="reviewBody">Honestly, this class was awesome. I have always wanted to learn more about Illustrator and have been frustrated that my creativity was limited by my understanding of the program. After taking this bootcamp class, I have an entirely new understanding of Illustrator and I'm excited to be able to put these skills to use in my job. Eva was the sweetest teacher and was always available to answer questions and show us real-world examples of projects and ways to use our new skills. I couldn't recommend this program more highly - very grateful to have had this opportunity. Looking forward to returning to take the Photoshop bootcamp class! Thanks! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Virginia Glass</span>
                    <b>|</b>
                    <span class="company">ExactTarget</span>
                </h3>
                <p itemprop="reviewBody">I learned so much! I would recommend this instructor for anyone who wants to take Illustrator training. She has great real-world experience and an incredible education for teaching this class.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Dante Morreale</span>
                </h3>
                <p itemprop="reviewBody">Eva is a great instructor.  For our private classes, she tailors the curriculum to add in our own companies challenges and obstacles, not just simply following a pre-assigned work assignment.  We look forward to the Photoshop class and presenting our own examples so we can take away the very most from our training experience.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Linda Rizzone</span>
                    <b>|</b>
                    <span class="company">SARA LEE</span>
                </h3>
                <p itemprop="reviewBody">This is my 3rd time taking a class with Michael's and I have learned so much from him. I have recommended Training Connection to others. I hope to be back soon.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mathew Ward</span>
                </h3>
                <p itemprop="reviewBody">I thoroughly enjoyed my two day course that I attended and look forward to the skills and inspiration I gained. Now I will be able to implement these skills in personal illustration projects and graphic design work. Thanks. Cheers!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Emily Parkin</span>
                </h3>
                <p itemprop="reviewBody">The small class size allowed me to go over questions with my instructor that had come up at my job. I was able to apply what I learned in the course to what I will be doing back at work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Greg Miller</span>
                    <b>|</b>
                    <span class="company">Township High School District # 214</span>
                </h3>
                <p itemprop="reviewBody">Eva was awesome.  She is extremely knowledgeable and teaches very well. She is very clear, thorough, and very helpful.  We'll be here again.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jennifer Demmitt</span>
                    <b>|</b>
                    <span class="company">Wiscon Corp/Caputo Cheese</span>
                </h3>
                <p itemprop="reviewBody">Eva was a wonderful teacher and incredibly knowledgeable. She knows Illustrator inside and out and was very helpful with applying real world things she has used it for. I had no clue how much I really didnt know about Illustrator and am excited to go to work tomorrow and apply the new things I have learned to my job! Thank you!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michael Davis</span>
                    <b>|</b>
                    <span class="company">DeVry University</span>
                </h3>
                <p itemprop="reviewBody">Chana was very knowledgable and willingly shared her knowledge of Illustrator and other Adobe features. Chana was polite, punctual and personable. I would highly recommend her as an instructor for any graphic design related topics.  Thank you Chana!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Courtney Nelson</span>
                    <b>|</b>
                    <span class="company">YWCA Metropolitan Chicago</span>
                </h3>
                <p itemprop="reviewBody">As someone with previous Illustrator experience, I found myself surprised by the number of new concepts, tips, and tricks I learned from the Training Connection course. Eva was knowledgeable, friendly, upbeat, and had a wonderful way of making the material approachable and easy to absorb.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Grace Ko</span>
                </h3>
                <p itemprop="reviewBody">I'm very glad that I took this course. After only 3 days, I feel like I have a full grasp of Adobe Illustrator tools and how to best use them. I learned many tips and tricks that I will apply in real world situations. Gabriel was a wonderful instructor. He made the class fun, informative, and went above and beyond with every aspect of the class. I can't wait to take more classes with him! I learned more in these short 3 days than I learned in all of my online courses on sites like lynda.com. There's no substitute for a live teacher where you can ask all of the questions you want and get detailed answers on the spot. Thank you to Jeff, Gabriel and Training Connection Team!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

