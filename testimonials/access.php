<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Microsoft Access Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Microsoft Access  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/access-training.php">Microsoft Access  classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>1,895 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Monyrotana Keo</span>
                    <b>|</b>
                    <span class="company">Paramount Unified School District</span>
                </h3>
                <p itemprop="reviewBody">Jeff was great, going above and beyond, and helping us with specific questions that we had without taking away from the learning experience of others.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mike McCallister</span>
                    <b>|</b>
                    <span class="company">Maricopa Comm College Dist</span>
                </h3>
                <p itemprop="reviewBody">I enjoyed everything about this class.  Your staff member here was so enjoyable.  The Instructor "Chris" was very nice and was able to work well the diverse amount of knowledge and mixed abilities of the students</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Laura Flenoury</span>
                    <b>|</b>
                    <span class="company">Cal State Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">Chris is excellent! He answered all of our questions and maintained a good pace for us to learn MS Access. I hope to see him again when I take the Intermediate level of MS Access.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tony Prince</span>
                    <b>|</b>
                    <span class="company">120 Venture Construction</span>
                </h3>
                <p itemprop="reviewBody">After having Jeff as my instructor yesterday, I was a bit weary of having a new instructor for day 2, but after being in George's class all weariness went away. Just like Jeff, George was able to teach the Access class in a way that was extremely easy to understand and follow and even took extra time to help out with some real life scenarios that I was having trouble implementing into MS Access. Great instructor!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kay Walters</span>
                </h3>
                <p itemprop="reviewBody">Great class! Great Instructor!  I'm from Georgia and I plan to come again to take courses with Training Connection.  Best money I ever spent for a class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christopher Quiroga</span>
                    <b>|</b>
                    <span class="company">Kaiser Permanente</span>
                </h3>
                <p itemprop="reviewBody">Very good course.  The instructor is very knowledgeable and willing to cater lessons to students' requests or practical needs.  Where any topic is somewhat unclear or a student falls behind, the instructor clarifies quickly and gets everyone on the same track.  Highly recommended.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.3</h3>

                    <div class="star-rate star-rate-sm star-rate-4_3">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ted Hixson</span>
                    <b>|</b>
                    <span class="company">BPI group</span>
                </h3>
                <p itemprop="reviewBody">Great intro course! Took me from knowing nothing to feeling like I could go out and construct my own database! Looking forward to learning more at higher levels.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                Y
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Yashanad Mhaskar</span>
                    <b>|</b>
                    <span class="company">Prairie Education &amp; Research Cooperative</span>
                </h3>
                <p itemprop="reviewBody">Instructor (Rich Kerr) is very experienced and knowledgeable.  Juana Wooldridge (Front Desk) is highly professional and Welcomed us. A great place to learn. Thank you!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kathleen Foy-Asaro</span>
                </h3>
                <p itemprop="reviewBody">excellent instructor. Very engaging, helpful, and knowledgeable on ACCESS. Chris welcomed questions, and assisted all students as needed. I would definitely recommend this course and others to my co-workers. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rose Reid</span>
                    <b>|</b>
                    <span class="company">AcademyX</span>
                </h3>
                <p itemprop="reviewBody">The instructor, George Kuck, went to great lengths to personalize the class for me and my classmate.  I was able to ask specific questions about my troubleshooting and got the answers to my questions, as well as an explanation to the larger, overall picture.  This class helped me understand Access language as well as how to tackle specific problems.  I enjoyed the class very much and would like to come back!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                O
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Oke Liimatta</span>
                    <b>|</b>
                    <span class="company">HDT Robotics</span>
                </h3>
                <p itemprop="reviewBody">Rich was easy to talk to and more than willing to adapt the examples and subject matter to meet the students' needs mor</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                F
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Fernando Serrano</span>
                    <b>|</b>
                    <span class="company">Brady Corporation</span>
                </h3>
                <p itemprop="reviewBody">Excellent instructor.  Rich was able to break down Access database topics into a very understandable format.  I'm confident I'll be able to apply to my work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cruz Gustafson</span>
                </h3>
                <p itemprop="reviewBody">Thank you for being so patient and informative with us. You never made any of us feel inept - especially for those of us just learning the behind the scenes of "Access" (like me!)</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kristi Zike</span>
                    <b>|</b>
                    <span class="company">Svoboda Capital Partners</span>
                </h3>
                <p itemprop="reviewBody">Excellent excellent excellent class all around.  I'm excited to come back for the level 2 and 3 classes tomorrow and Friday.  Rich is a fantastic instructor- very engaging and the instructions were easy to understand.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                N
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Neil Alcorn</span>
                    <b>|</b>
                    <span class="company">Country Financial</span>
                </h3>
                <p itemprop="reviewBody">Give Rich a raise!!  He was very knowledgeable and an excellent facilitator.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jim Koczot</span>
                    <b>|</b>
                    <span class="company">WW Grainger</span>
                </h3>
                <p itemprop="reviewBody">I deeply appreciated your ability to made this a custom course for me.  I have never experienced this type of customer service. I will definitely take another course with you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jamie Adkins</span>
                </h3>
                <p itemprop="reviewBody">This instructor and course were amazing.  It was great that we were able to do hands on.  Thank you so much!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Allison Harriman</span>
                    <b>|</b>
                    <span class="company">The Claro Group</span>
                </h3>
                <p itemprop="reviewBody">I came into the class with no knowledge of VBA and have never worked in the application before and feel like I really learned a lot. I learned a lot of real-world examples I can apply to my current work and can expand upon what I've currently been doing in access.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maria Larson</span>
                    <b>|</b>
                    <span class="company">University of Illinois</span>
                </h3>
                <p itemprop="reviewBody">Rich was ready to answer all questions and very knowledgeable.  He gave us additional resources to refer to after the class and his email address to refer to him in case we have any questions.  I will definitely be referring to this class to anyone who needs Access training.  I enjoyed this class and feel more confident using Microsoft Access.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Janice Varley</span>
                    <b>|</b>
                    <span class="company">Montrose Travel</span>
                </h3>
                <p itemprop="reviewBody">I am extremely impressed with the levels of technology, professionalism, and detail of this course.  George is a fantastic instructor.  Thank you so much for providing a great environment for learning!  I can't wait to finish the the rest of the courses in this series, and I will definitely recommend Training Connection to anyone looking to improve their skills.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Benjamin Tiffany</span>
                    <b>|</b>
                    <span class="company">Zurich American Insurance Company</span>
                </h3>
                <p itemprop="reviewBody">Great examples. Class flowed nicely and I learned the concepts I thought would be covered. Rich really knows his stuff and the staff is very friendly. Highly recommended.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shannen Herbert</span>
                    <b>|</b>
                    <span class="company">BSC Management</span>
                </h3>
                <p itemprop="reviewBody">Chris was a wonderful teacher. Access had confused me in the past... but he put it in a way that I would understand! I really enjoyed this class... and would definitely recommend it to other coworkers. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sasha Garrett</span>
                    <b>|</b>
                    <span class="company">LA Harbor Dept</span>
                </h3>
                <p itemprop="reviewBody">Jeff was an excellent instructor! I definitely feel more confident with my Access skills! He was very thorough and patient. I had a great time!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sarah Bettenhausen</span>
                    <b>|</b>
                    <span class="company">John Burns Construction Co.</span>
                </h3>
                <p itemprop="reviewBody">Rich is really a great teacher and has a superb knoweledge of access and was able to make every task understandable as well as answer everyone's questions without a problem.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Madeleine Mino</span>
                    <b>|</b>
                    <span class="company">Suzlon Wind Energy</span>
                </h3>
                <p itemprop="reviewBody">Pat was such a great instructor and knows this software very well.  I was very happy with how he relayed his knowledge to the class - very engaging!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Justin Platt</span>
                    <b>|</b>
                    <span class="company">The Bradford-Hammacher Group</span>
                </h3>
                <p itemprop="reviewBody">I would recommend the Access training series to anyone looking to get familiarized with the software.  Bob made the material easy to digest and was willing to stick around and answer any miscellaneous questions.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kevin Gresham</span>
                    <b>|</b>
                    <span class="company">Los Angeles World Airports</span>
                </h3>
                <p itemprop="reviewBody">Great job George! I'll be requesting the Level 2 class for Microsoft Access, and I hope you are the instructor. You broke down the concepts in Level 1 so that they were easy and clear.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Maria G. Chagoya</span>
                    <b>|</b>
                    <span class="company">Turning Point Foundation</span>
                </h3>
                <p itemprop="reviewBody">Chris was a great instructor! I came in with limited amount of ACCESS knowledge and I am now walking out with enough ACCESS training to begin queries and reports that I can then exercise for my work requirements. Thanks Chris! I will now recommend that my company also sends me to the LEVEL 2 training ... I hope to be here in August!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Emily Lanyon</span>
                    <b>|</b>
                    <span class="company">Lincoln Park Zoo</span>
                </h3>
                <p itemprop="reviewBody">Learning in Level 1 was a great beginning and will greatly benefit me at work. Also, it helped me to realized that Access was incredibly integrated and that I needed to take Levels 2 &amp; 3. See you tomorrow!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Dawn Craven</span>
                    <b>|</b>
                    <span class="company">Toyota Motor Sales, U.S.A.</span>
                </h3>
                <p itemprop="reviewBody">George does a great job of adjusting to the level of the students in the class.  He adjusts the speed of the class accordingly and throws in extra examples when appropriate.  Very helpful course.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>




    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

