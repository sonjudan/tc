<div class="page-intro page-intro-row mb-3">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Adobe Photoshop Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">

            Below is a sample of testimonials from students who recently completed one of our Adobe Photoshop training courses. Please click on the following link to read more about our
            <a href="#">Adobe Photoshop classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>779 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">

        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Viktoria McCracken</span>
                    <b>|</b>
                    <span class="company">Hudson Highland Group</span>
                </h3>
                <p itemprop="reviewBody">The instructor is very knowledgeable and a very good teacher. The course was very well structured. The instructor was well prepared. Overall - a great course. I would definitely recommend it to others! Great job, Eva!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                G
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Giuseppe Calabrese</span>
                    <b>|</b>
                    <span class="company">Pricegrabber.com</span>
                </h3>
                <p itemprop="reviewBody">I am very impressed with the overall experience.  Everything from the location, facility, staff, instructor, course materials, etc.  Definitely would recommend to anyone and definitely will come back for more training.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">SARAH EHRLICH</span>
                </h3>
                <p itemprop="reviewBody">Chana was very professional. She knew her material beyond professionalism and she is extremely passionate about her work! I thoroughly enjoyed the class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Katy Burns</span>
                    <b>|</b>
                    <span class="company">American Dental Association</span>
                </h3>
                <p itemprop="reviewBody">Eva was excellent. I have had some average at best training experiences, but this was simply excellent in every way!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tory Mehta</span>
                </h3>
                <p itemprop="reviewBody">Eva is amazing and wonderful!!!!  She taught at a great pace and was patient.  Eva is super knowledgeable about the subject. Love her-she's perfect!!!!!  I took Illustrator and Photoshop and am completely happy with everything.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenna Inskeep</span>
                    <b>|</b>
                    <span class="company">The Alliance</span>
                </h3>
                <p itemprop="reviewBody">I took InDesign back in the the summer of '10 and had the opportunity to come back to learn Photoshop. I was to pick any place I wanted to do my training and I chose Training Connection! Eva is such a fun instructor! I learned so much with her and I will definitely come back in the future for other courses!!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Carlos Torres</span>
                </h3>
                <p itemprop="reviewBody">Beverly was really good at explaining the concepts and the way to sue all the tools. Very nice facilities with high end computers and coffee area... believe me, well need it! there is lots to cover and to learn in a short period of time (relatively) I really like that we can take the course again in the 6 moth period, that i think is very helpful. Cant wait to come back and learn more of this an other programs. Thanks!!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jennifer Karpinski</span>
                    <b>|</b>
                    <span class="company">Summit High School</span>
                </h3>
                <p itemprop="reviewBody">This course was AWESOME!!! Beverly is an incredible teacher. Her style is very informative and helpful. The handouts and files are very comprehensive and super helpful for when I want to remember all these great Photoshop techniques later. I can't wait to take the Illustrator and InDesign courses!!!!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michelle Truong</span>
                    <b>|</b>
                    <span class="company">The Options Clearing Corp.</span>
                </h3>
                <p itemprop="reviewBody">Eva is the BEST instructor! She is a very effective instructor who is able to articulate lessons well. I rave about her and the company in general. I think this is quality training with quality instructors in a great environment. Great equipment, small class sizes work well, and the good coffee is a plus! This is my second class and definitely not my last. Highly recommended, and I've already had two of my colleagues sing up for classes. Thanks Training Connection!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Susie Moore</span>
                </h3>
                <p itemprop="reviewBody">I loved this course!  Eva was a fantastic instructor.  She was very knowledgeable, and so friendly and pleasant.  I came into the class with a very good knowledge of Photoshop already, but Eva taught me MANY other things that will make my workflow easier and better.  I am looking forward to taking an Illustrator class with her in 2 weeks!  I wouldn't want anyone else!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Laura Krain</span>
                    <b>|</b>
                    <span class="company">Illinois Holocaust Museum &amp; Education Center</span>
                </h3>
                <p itemprop="reviewBody">Eva was an excellent teacher. She taught me practical skills that I can use in my day-to-day work. She added information to the book to make sure I understood the background and reasoning behind certain techniques, as well as adding additional tips and tricks. I would love to take additional classes with her.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alejandro Mora</span>
                </h3>
                <p itemprop="reviewBody">Learned a metric ton in this class. There were parts that I need step by step instructions as reminders but Kristian is happily going to share some instructions. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenna Sather</span>
                    <b>|</b>
                    <span class="company">Papaya Inc</span>
                </h3>
                <p itemprop="reviewBody">Both the instructor and staff were extremely friendly and helpful.  I thoroughly enjoyed my time in this course and it met all my expectations.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Richard Lau</span>
                    <b>|</b>
                    <span class="company">Electronic Arts</span>
                </h3>
                <p itemprop="reviewBody">Beverly Houwing was a terrific and very informative teacher.  She provided excellent examples of real world projects and was always willing to answer any questions that I may have had.  She's definitely a teacher I would take again!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Troy Lazaris</span>
                </h3>
                <p itemprop="reviewBody">This was my second time taking the photoshop course and I must say I liked more than the fist time. Beverly was very kind and extremely patient with all my questions and absolutely knowledgable. I am highly recommend training connection. I am a makeup artist and I am very capable retouching my own pics and that's incredible. Last but not least Jeff was such an incredible host :) Always happy and always welcoming everyone. I will definitely take more courses in the future :) Troy xoxo</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                W
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">William Reach</span>
                    <b>|</b>
                    <span class="company">Citrus County Sheriff's Office</span>
                </h3>
                <p itemprop="reviewBody">Even though I have used Photoshop for many years, I learned so much valuable new information.  The class was well worth the investment.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ben Siegler</span>
                </h3>
                <p itemprop="reviewBody">our instructor, hannah, was terrific, kind, down-to-earth, and very knowledgable.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jacob Dudley</span>
                </h3>
                <p itemprop="reviewBody">Kristian was a fabulous instructor.  His knowledge is endless and he was super helpful and patient for all skill levels! I would definitely like to take more classes.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sharon Ha</span>
                    <b>|</b>
                    <span class="company">Course Horse</span>
                </h3>
                <p itemprop="reviewBody">Beverly was great. Energetic, passionate and engaging enough to keep the amount of information and long hours interesting! She is incredibly knowledgeable in Photoshop, and provided awesome tips and shortcuts I wouldn't have come across if I had continued being self-taught. So glad I took this class. Two small points of improvement: content might be a bit too photography focused, but helpful information nonetheless. Second, the Bootcamp makes information towards the end of the week harder to retain since it's a ton of information condensed into 5 days, but the lesson materials and free class review option is great to have! Would definitely do a training here again. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Emily Parkin</span>
                </h3>
                <p itemprop="reviewBody">Chana was so helpful in answering all of my questions. She even explained how I could apply the class concepts to my personal projects I have been working on.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Shannon Hinkle</span>
                    <b>|</b>
                    <span class="company">Chicago Sport &amp; Social Club</span>
                </h3>
                <p itemprop="reviewBody">Eva was very knowledgeable, friendly and helpful throughout our training course. The small class size allowed for a lot of personalized attention which was much appreciated. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                V
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Vanessa Skillman</span>
                    <b>|</b>
                    <span class="company">Children's Hospital Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">Thank you Beverly for the Photoshop training. I took it once before and didn't get the hang of it. With your fabulous teaching, I am confident I have a better skill set and understanding of the program. I thought the class was great and Beverly is very helpful and friendly and knowledgeable in the arena of photography.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lisa Timmons</span>
                    <b>|</b>
                    <span class="company">Children's Hospital Los Angeles</span>
                </h3>
                <p itemprop="reviewBody">This was my repeat of the course and I was very thankful to have the opportunity to do so. Because it's so much information at once, the second time around, I really feel like I got a good handle on concepts that eluded me the first time around. I appreciated the different styles of each instructor and would recommend them both.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Dan O'Brien</span>
                </h3>
                <p itemprop="reviewBody">Beverly is an outstanding instructor. Very friendly and helpful, and full of helpful information and insights to the business of graphic design and photography. One of the best teachers I have ever had.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                F
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Fred Phillips</span>
                    <b>|</b>
                    <span class="company">LMT Onsrud</span>
                </h3>
                <p itemprop="reviewBody">Excellent course - since I had no background in Photoshop, I feel more comfortable in knowing what tools are presented and how they can be utilized. Eva did a very good job in presenting and displayed patience throughout. Her background knowledge in art, color, lighting, etc...hugely helpful! Her real world experience provided credibility. Highly recommended!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kimberly Bugos</span>
                    <b>|</b>
                    <span class="company">Integrated Project Management Company, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Eva is extremely engaging and patient. I took her Illustrator class as well and would highly recommend her courses to anyone.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Debra Willis</span>
                    <b>|</b>
                    <span class="company">American Dental Association</span>
                </h3>
                <p itemprop="reviewBody">Ava Sandor the instuctor was wonderful! She was so knowledgeable and such a great teacher. Juana was great too! Thank you both for such a wonderful experience. I can't wait to get back to the office and try out my new skills! Thank you both again for your knowledge and hospitality. See you next time! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Peyton Nealis</span>
                    <b>|</b>
                    <span class="company">Sunset Gower &amp; Sunset Bronson Studios</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed taking this course and I am pleasantly surprised at how well I think I will do on my own with Photoshop. I'm very excited to take the other two courses that I have already signed up for, but I am sad that I will have a different instructor because I think that Beverly taught at a wonderful and challenging pace.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Brenda Hartman</span>
                    <b>|</b>
                    <span class="company">Chrysler</span>
                </h3>
                <p itemprop="reviewBody">The course included much more information than what I expected. The instructor is one of the best ever! Thank You! Very useful information, I got my monies worth.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Anne Calvin</span>
                    <b>|</b>
                    <span class="company">Oak Park School District 97</span>
                </h3>
                <p itemprop="reviewBody">I am very happy with my experience in this class and am looking forward to the next two classes I will be taking. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>