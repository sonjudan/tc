<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">JavaScript Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <div data-aos="fade-up" data-aos-delay="100">
            <p>Below is a sample of testimonials from students who recently completed one of our JavaScript  training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="/javascript-training.php">JavaScript  classes in Chicago and Los Angeles</a>.</p>
        </div>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>406 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Corinna Ace</span>
                    <b>|</b>
                    <span class="company">ICANNICANN</span>
                </h3>
                <p itemprop="reviewBody">Jeff was a great instructor. He covered the course material thoroughly, shared helpful experience and examples, and was very engaging. The mix of hands-on work, lecture, and discussion helped me grasp new concepts throughout the course. Overall positive experience!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lena Salazar</span>
                    <b>|</b>
                    <span class="company">Chicago Public Schools</span>
                </h3>
                <p itemprop="reviewBody">Olivier is a great teacher...his knowledge and experience are very apparent, but his teaching process and delivery were right on...excellent for communicating this type of material. I'm looking forward to other classes with him.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sheila Schook</span>
                    <b>|</b>
                    <span class="company">Pre-Paid Legal Services Inc.Chicago Public Schools</span>
                </h3>
                <p itemprop="reviewBody">The instructor (Chris) did a spectacular job! Besides giving me a better understanding of AJAX, I was also given a JS refresher (which proved to be quite useful!). He was very patient throughout and made the "unknown" seem less intimidating. I will definitely recommend this instructor/course to others. Thanks Chris! A+</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Michael Smith</span>
                    <b>|</b>
                    <span class="company">Illinois Workers CompensationPre-Paid Legal Services Inc.</span>
                </h3>
                <p itemprop="reviewBody">I strongly feel that I wouldn't have grasped the concepts in this class with another instructor. I really enjoyed the class and now feel that I will move forward in my work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joshua Radetski</span>
                    <b>|</b>
                    <span class="company">Bankers Life and Casualty Company</span>
                </h3>
                <p itemprop="reviewBody">Chris was wonderful.  Our small class was able to move faster than normal and Chris challenged us to learn more. I saw a number of "similar" courses offered in the area, and I am extremely pleased that I made the right choice.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Charles Rawls</span>
                    <b>|</b>
                    <span class="company">MCLSIllinois Workers Compensation</span>
                </h3>
                <p itemprop="reviewBody">Once again the instructor did not disappoint  .  The information was explained in a way that I understood the flow and could relate the concept of the material to my current job  duties.  I plan on updating my skill level with this instructor.  This class was well design to allow me to update certain areas that I am familiar while l adding elements that I was not previously aware of.  Thanks once again.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                I
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ian Law</span>
                    <b>|</b>
                    <span class="company">Loyola Marymount University</span>
                </h3>
                <p itemprop="reviewBody">This particular class really started to get into some interesting stuff - these are very cool tips and skills to learn and I'm excited to apply them on my own. Chris is a great instructor and deserves a lot of credit for tailoring his classes so well to fit the students and their skill sets. Very well prepared as well.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christina Andonova</span>
                    <b>|</b>
                    <span class="company">Pepperdine UniversityBankers Life and Casualty Company</span>
                </h3>
                <p itemprop="reviewBody">I really enjoyed this class. I liked particularly the teaching style of the instructor. I felt I was engaged and participated actively in the process of understanding and learning the material. The concepts were explained in a way that makes me feel that developing sites with Ajax will be equally fun as challenging.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mark Szidik</span>
                    <b>|</b>
                    <span class="company">Sony Pictures TelevisionMCLS</span>
                </h3>
                <p itemprop="reviewBody">great instructor. he clearly has a great deal of knowledge.  We never stumped him with all of our questions.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kenneth Simon</span>
                    <b>|</b>
                    <span class="company">Fuji America CorporationLoyola Marymount University</span>
                </h3>
                <p itemprop="reviewBody">My lack of a programming background put me at a disadvantage coming in, but thanks to Oliver's help, I learned a lot. I now have the foundation I needed for understanding JavaScript. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tony Merritt</span>
                    <b>|</b>
                    <span class="company">Samatamason</span>
                </h3>
                <p itemprop="reviewBody">The JavaScript course was exactly what I needed and more! I should have done this years ago. Chris is the best instructor and very effective in applying the course content to my specific situations as well as meeting me at my level of expertise. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                D
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Daniel Weber</span>
                    <b>|</b>
                    <span class="company">Western Tube &amp; Conduit CorpPepperdine University</span>
                </h3>
                <p itemprop="reviewBody">I thoroughly enjoyed the course.  Helped immensely with real world application to javaScript!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.5</h3>

                    <div class="star-rate star-rate-sm star-rate-4_5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Andy Wachter</span>
                    <b>|</b>
                    <span class="company">Computer System Innovations, Inc.Sony Pictures Television</span>
                </h3>
                <p itemprop="reviewBody">I thought the course was great and the fact that I was able to have a 1-on-1 session with Jeff was very valuable. We could move at my own pace and he did a great job of tailoring a lot of the lessons to specific things that I was interested in. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.6</h3>

                    <div class="star-rate star-rate-sm star-rate-4_6">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Casilda King</span>
                    <b>|</b>
                    <span class="company">Univar</span>
                </h3>
                <p itemprop="reviewBody">Chris is a superb instructor, I would definitely recommend him to anyone who's interested in coding and especially in JavaScript/jQuery. He's an amazing teacher and is extremely knowledgeable.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rod Sampson</span>
                    <b>|</b>
                    <span class="company">Chicago Public SchoolsFuji America Corporation</span>
                </h3>
                <p itemprop="reviewBody">Once again, Chris allowed the students to shape the course content and was still able to offer a thorough understanding of the AJAX technology.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Eric Guerber</span>
                    <b>|</b>
                    <span class="company">AECOM</span>
                </h3>
                <p itemprop="reviewBody">Chris was an excellent instructor, very informative and knowledgeable. The class covered everything from Html, CSS, and JavaScript basics that I was unfamiliar with, to advanced techniques that I plan to put into practice in our application in the near future. Thanks a lot for the great class. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.4</h3>

                    <div class="star-rate star-rate-sm star-rate-4_4">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Alex Collins</span>
                    <b>|</b>
                    <span class="company">AT&amp;T Services, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I really liked this course and the instructor. The class size was small, so that helped move us along quickly. Chris also tailored the class to help us achieve exactly what we wanted to get out of it.  Chris has a lot of real-world experience which always is superior to an "academic" who has no battle wounds. Chris's affable style makes you feel at ease. Also the AJAX+CSS class bundle is a no-brainer. Great deal. My only complaint really has to do with the other students in the class who sometimes slowed things down a bit from the super-fast pace that I hoped, but that is nitpicking. The pre-reqs (such as javascript/html knowledge) should be followed more closely. But what can ya do.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Chuck Steneck</span>
                    <b>|</b>
                    <span class="company">University of Southern CaliforniaSamatamason</span>
                </h3>
                <p itemprop="reviewBody">Shaun is a wealth of knowledge!  He truly knows his code. He answered every question, and explained concepts clearly .  A+ rating !  Will definitely take another class !  Thank you !</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Manny Schare</span>
                    <b>|</b>
                    <span class="company">US Citizenship and Immigration ServicesWestern Tube &amp; Conduit Corp</span>
                </h3>
                <p itemprop="reviewBody">This was one of the best classes I have taken with great examples that I can take back to my office.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Rachel Watters</span>
                    <b>|</b>
                    <span class="company">Inforum 2008Computer System Innovations, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Chris did a great job in answering our questions and addressing the current project issues we are all working with. I work with developers everyday, so gaining knowledge from Chris, whom actually knows how to explain the concepts for even me to understand, is very enjoyable.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.1</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Kuhl</span>
                    <b>|</b>
                    <span class="company">City of London Investment Management CompanyUnivar</span>
                </h3>
                <p itemprop="reviewBody">The training was very informative and exactly the start I needed with JavaScript. I am very pleased with the result!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Sharon D Lawson</span>
                    <b>|</b>
                    <span class="company">Chicago Public Schools</span>
                </h3>
                <p itemprop="reviewBody">Chris is an excellent instructor he explain the materials and gave great example and it was a lot of participation within the class. I would strongly recommend Training Connection to my fellow co- workers at Chicago Public Schools. Sharon D.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tom Smalara</span>
                    <b>|</b>
                    <span class="company">AECOM</span>
                </h3>
                <p itemprop="reviewBody">The class was great. Chris is very knowledgeable, personable, and was very good at changing the pace to ensure that we were challenged, but not overwhelmed. I'm definitely interested in taking the advanced class when it becomes available.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Steve</span>
                </h3>
                <p itemprop="reviewBody">Great class, Chris was very good at helping students with different level of JavaScript experience and I thought he handled the questions and flow from the different level very well. Also very good breaking down how the code works. Code classes can be boring and this was not boring at all.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                E
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Elizabeth Hornbostel</span>
                    <b>|</b>
                    <span class="company">AT&amp;T Services, Inc.</span>
                </h3>
                <p itemprop="reviewBody">It was fantastic that Chris customized the class to our specific needs and level of expertise.  It made the class more informative and enjoyable.  He made sure to get a feeling for what folks were expecting out of the class and what they thought they would be using the skills for back at their job and tailored the lessons accordingly.  Excellent!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                U
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Udhaya Thoppae</span>
                </h3>
                <p itemprop="reviewBody">I got hands-on training on Ajax with good working examles.  Also I got good hands on javascript especially using DOM.  Instructor was good at giving real time examples to work on.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Luke Sheppard</span>
                    <b>|</b>
                    <span class="company">University of Southern California</span>
                </h3>
                <p itemprop="reviewBody">I have yet to stump Jeff when I ask hard questions in these various classes! Great class as always. I'm READY to go crank out some javascript now.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Paul Novak</span>
                    <b>|</b>
                    <span class="company">US Citizenship and Immigration Services</span>
                </h3>
                <p itemprop="reviewBody">I liked the first day of class, the material was very good.  The second day was also good for hands-on examples, although it was quite challenging and a bit of a stretch for my javascript knowledge.  The third day seemed a bit random though, I would have liked a bit more structure.  I think it would have helped to write the ajax.js program during the course.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ali Alkhafaji</span>
                    <b>|</b>
                    <span class="company">Inforum 2008</span>
                </h3>
                <p itemprop="reviewBody">Instructor was more than helpful in every aspect of the class. The topics he covered were both detailed and impressive.  He always made sure that everyone in the class understands the subject before moving on to a new topic.  I fully recommend him to any person interested in AJAX.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Mike White</span>
                    <b>|</b>
                    <span class="company">City of London Investment Management Company</span>
                </h3>
                <p itemprop="reviewBody">I had Chris for PHP and Javascript/JQuery training and he does a great job tailoring the courses to address challenges I am facing with projects I am working on. I would definitely recommend Chris to anyone looking for this type of training.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>

    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

