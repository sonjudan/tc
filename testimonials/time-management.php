<div class="page-intro page-intro-row mb-0 mt-0">
    <div class=" intro-copy">
        <h4 data-aos="fade-up">Time Management Classes Training Courses</h4>
        <h1 data-aos="fade-up" >Testimonials</h1>
        <p data-aos="fade-up" data-aos-delay="100">
            Below is a sample of testimonials from students who recently completed one of our Time Management Classes training courses. Please click on the following link to read more about our <a itemprop="mainEntityOfPage" href="https://www.trainingconnection.com/time-management-training.php">Time Management Classes classes in Chicago and Los Angeles</a>.
        </p>
    </div>

    <div class="intro-icon-r rating-l2 mb-0"  data-aos="fade-up" data-aos-delay="200">
        <h2>4.7 <span>of 5</span></h2>

        <div class="star-rate star-rate-sm star-rate-4_7">
            <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
        </div>
        <p itemprop="reviewBody">based on <br>516 reviews</p>
    </div>
</div>

<div class="content-default" data-aos="fade-up">

    <div class="section-list">
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                B
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Ben Zanjani</span>
                    <b>|</b>
                    <span class="company">TMEIC</span>
                </h3>
                <p itemprop="reviewBody">Carol is Amazing! Her training will not only help me manage my time better at work..it will help me be a better person. Thank you very much for your great wisdom. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jonathan Lias</span>
                    <b>|</b>
                    <span class="company">Showtime Networks, Inc.</span>
                </h3>
                <p itemprop="reviewBody">I truly enjoyed Caro's instruction on Time Management and the skills we reviewed. Carol made each topic relatable and offered real world examples for me to connect with. I can tell she has a passion for teaching. The collaboration and dialogue made learning concepts easy and simple to use.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Susan Kivila</span>
                    <b>|</b>
                    <span class="company">Jet Propulsion Laboratory</span>
                </h3>
                <p itemprop="reviewBody">This is one of the most important and life changing class I have taken. The principles taught on this class are critical to my success as a manager and in attaining my life goals. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cecilia Bonilla</span>
                    <b>|</b>
                    <span class="company">Public Counsel</span>
                </h3>
                <p itemprop="reviewBody">The insight given by our lovely instructor Carol really put everything into perspective. I can definitely say I can apply everything I learned regarding time management in both my personal and professional life. The approach used was very helpful to my learning and overall comfort in participating today in class. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Art Floruti</span>
                    <b>|</b>
                    <span class="company">Provivi</span>
                </h3>
                <p itemprop="reviewBody">Very informative course. Helped me to identify my weaknesses in time management and suggestions as well as techniques on how to improve on what I get done in my time. Thank you Carol!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                L
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Lissa Cooper</span>
                    <b>|</b>
                    <span class="company">PetPoint Solutions</span>
                </h3>
                <p itemprop="reviewBody">Thank you for the insight on how to manage my time more efficiently. I discovered that it is not about the tools we use in our everyday life necessarily, but more on your own personal outlook on the tasks at hand.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Scott Diussa</span>
                    <b>|</b>
                    <span class="company">Nikon Inc.</span>
                </h3>
                <p itemprop="reviewBody">Juana is a very good and passionate teacher. I enjoyed her personalization of the class and the fact that the class was more interactive and not just a rehearsed reading of a PowerPoint presentation. I was expecting this to be more "work" related but it turned out to be more "life" related and I feel that will be more helpful in the long run. I will be looking into other programs that she teaches as well. Thank you! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kyle Andorf</span>
                    <b>|</b>
                    <span class="company">Aon FPE</span>
                </h3>
                <p itemprop="reviewBody">This course was helpful for me to recognize things that I can work on to improve my productivity and also recognize tasks that reduce my productivity.  I think that I have identified some tools/processes that I will be able to apply to my everyday work functions.  I believe that by studying what I am doing now and working to implement some of the ideas learned here today, I will become better at managing time and prioritizing key tasks and general tasks. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                P
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Pattie Mathison</span>
                    <b>|</b>
                    <span class="company">WICKLANDER-ZULAWSKI &amp; ASSOCIATES INC</span>
                </h3>
                <p itemprop="reviewBody">A completely different approach and outlook to time management that relates to real-world life.  Loved it! </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Macharia Brown</span>
                    <b>|</b>
                    <span class="company">Aeroflex Inc</span>
                </h3>
                <p itemprop="reviewBody">Alyncia, gave a great presentation full of examples, demonstrations, and thought provoking insight.  I feel that this class has afforded me the opportunity to make a leap in my career.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                R
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Robin Sais</span>
                    <b>|</b>
                    <span class="company"></span>
                </h3>
                <p itemprop="reviewBody">Instructor was amazing and very caring. I felt very comfortable with the participation and I usually dislike that part. The group of people were great too! I thought I would be embarrassed about having to take this type of class but I'm really glad I attended. I think it will help me in my personal life even more than at my job.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kellie Stephens</span>
                    <b>|</b>
                    <span class="company">Camfil Farr</span>
                </h3>
                <p itemprop="reviewBody">This was an excellent course. It was not the "typical" time management course that I expected and really helped the employees that attended. Lena was fantastic! Thank you.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Tracy Hagenmeyer</span>
                    <b>|</b>
                    <span class="company">Georg Fischer Signet</span>
                </h3>
                <p itemprop="reviewBody">Carol was amazing. She had a very organized and clear way of describing things. She was always open to questions and comments, but also kept us on track. So many of the ideas and things she taught us will really apply to my situation and help me to be more focused and successful.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Justin Biggs</span>
                    <b>|</b>
                    <span class="company">Australian Consulate General</span>
                </h3>
                <p itemprop="reviewBody">Juana was great, instructed when required and was happy to listen as well, which provided a great and valuable training experience all round.  Thanks Juana, great tips and good meeting you...</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Spencer Wolf</span>
                    <b>|</b>
                    <span class="company">Toppers Pizza</span>
                </h3>
                <p itemprop="reviewBody">This course was exactly what I needed to help with my time organizational skills. I'm excited to implement these concepts in my life and hope that it will turn some things around for me as far as staying on top of my work load. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                M
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Matt Cooper</span>
                    <b>|</b>
                    <span class="company">Clune Construction</span>
                </h3>
                <p itemprop="reviewBody">I thought the instructor was very good. She was very willing to help us work through our individual Time Management issue and even work with us in her off hours to finish the training. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cynthia Carridine-Andrews</span>
                    <b>|</b>
                    <span class="company">Chicago Park District</span>
                </h3>
                <p itemprop="reviewBody">Great class Juana is very informative and passionate about her training.  I definitely enjoyed the course. Thank you</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                K
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Kelli Duffy</span>
                    <b>|</b>
                    <span class="company">New Belgium Brewing Company</span>
                </h3>
                <p itemprop="reviewBody">This training was amazingly helpful and insightful. I would definitely take another course with Allyncia. She was fun, engaging, and really made sure the message of the course was understood. I"m very glad i took this course. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Arti Ishak</span>
                    <b>|</b>
                    <span class="company">MSDSonline, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Juana was extremely engaging and spoke with clarity and conviction. Her enthusiasm on the topic was contagious and made the class want to participate and share, which enriched what we all learned from each other as students.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Cesar Oliva</span>
                    <b>|</b>
                    <span class="company">Akzonobel</span>
                </h3>
                <p itemprop="reviewBody">i was very impressed with the course, I had my eyes opened on several topics and will use them to better manage my time.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Stefani Villanueva</span>
                    <b>|</b>
                    <span class="company">eshots</span>
                </h3>
                <p itemprop="reviewBody">I think this course could have been a snooze fest but Lena made sure it wasn't! I was engaged the entire time. I had some major take aways that blew me away and I will use in my work and life. She was intuitive, very intelligent and hands-on I had fun learning new time management skills and very pleased with this course. She had a way to get to the core of what our struggles were and how to achieve success in careers and well being. Thanks Lena!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Aisha Brent</span>
                    <b>|</b>
                    <span class="company">IMRI</span>
                </h3>
                <p itemprop="reviewBody">This was a great course, better than I expected it to be. I think that we received valuable information and this course gave me information to evaluate my personal time and my work time. It showed me the importance of balance and provided great tips and physical tools to manage my time. I have great information to take back with me and already have an action plan to execute when I return. I am excited and less overwhelmed, I feel prepared to improve and succeed. Thank You Carol, you were a great instructor today. </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                S
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Saundra Briggs Robertson</span>
                    <b>|</b>
                    <span class="company">Louisville Convention &amp; Visitors Bureau</span>
                </h3>
                <p itemprop="reviewBody">Thank you to Lena, I plan to take this information and use everyday with hopes on making my job easier. Also thanks for  reopened my eyes to trying to figure out exactly what I want for my future goals. this class was GREAT!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                A
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Aman Sharma</span>
                    <b>|</b>
                    <span class="company"></span>
                </h3>
                <p itemprop="reviewBody">Juana was great ,She is just a great personality, explaining ,defining each topic , and asking us questions engaging us in each topic was great . Definitely recommend this course particularly from Juana.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John Briers</span>
                    <b>|</b>
                    <span class="company">Schneider Electric</span>
                </h3>
                <p itemprop="reviewBody">The instructor was very good at providing real life examples of the concepts that were discussed. There were very useful tips provided for Outlook use. Also, several worksheets alternatives that are likely to be useful in my daily work.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                T
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Thomas Westley</span>
                    <b>|</b>
                    <span class="company">Adelman Travel Group</span>
                </h3>
                <p itemprop="reviewBody">I really liked how the instructer used personal experiences to enforce the material.  She explained the material really well.  I would recommend her to anyone.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">John-Anthony Valle</span>
                    <b>|</b>
                    <span class="company">Medline Industries, Inc.</span>
                </h3>
                <p itemprop="reviewBody">Thank you for sharing tools and techniques that will vastly improve my professional and overall well-being. Great class!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">5.0</h3>

                    <div class="star-rate star-rate-sm star-rate-5_0">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Joshua Lieberman</span>
                    <b>|</b>
                    <span class="company">Northwestern Mutual</span>
                </h3>
                <p itemprop="reviewBody">Jeannette Grace was very engaging and helpful. she shared with me several ideas that I will implement into my daily life immediately.  I would recommend her to anyone looking to prioritize and maximize their efficiency.</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.9</h3>

                    <div class="star-rate star-rate-sm star-rate-4_9">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                J
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Jenny Giustino</span>
                    <b>|</b>
                    <span class="company">North American Corporation</span>
                </h3>
                <p itemprop="reviewBody">Juana was terrific.  She clearly has a positive outlook on life and made everyone in the class aware that they could have a positive outlook as well.  </p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.8</h3>

                    <div class="star-rate star-rate-sm star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
        <article class="post-default d-none" itemprop="review" itemscope="" itemtype="https://schema.org/Review">
            <div class="post-img profile-img-post">
                C
            </div>
            <div class="post-body">
                <h3 class="post-title" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
                    <span class="post-name" itemprop="name">Christine McConaghy</span>
                    <b>|</b>
                    <span class="company">U.S&gt; Environmental Protection Agency</span>
                </h3>
                <p itemprop="reviewBody">The instructor (Jeannette) was very energetic and inspiring.  I am leaving the course with a specific goal to work on and several other helpful suggestions, and many ideas for improvement.  Thanks!</p>
            </div>
            <div class="post-side">
                <div class="rating-l3">
                    <h3 itemprop="ratingValue">4.7</h3>

                    <div class="star-rate star-rate-sm star-rate-4_7">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Class rating"></span>
                    </div>
                </div>
            </div>
        </article>
    </div>


    <article class="post-default pt-5 pb-2">
        <button type="button" class="post-link js-load-btn">Show more testimonials</button>
    </article>

</div>

