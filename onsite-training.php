<?php
$meta_title = "Onsite Training Request | Training Connection";
$meta_description = "Request a Quote for Onsite training. Have one of outstanding trainers come to your offices to deliver private training to your team.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Onsite Training</li>
            </ol>
        </nav>


        <div class="page-intro">
            <div class="copy intro-copy">
                <h1 data-aos="fade-up" >Onsite Training</h1>
                <h3 class="mb-4" data-aos="fade-up" data-aos-delay="100">Available countrywide</h3>

                <p data-aos="fade-up" data-aos-delay="200">Have a group that needs face-to-face instructor-led training?</p>
                <p data-aos="fade-up" data-aos-delay="200">We have a team of dedicated local trainers across the country. Complete the quotation below for a group rate. Instructor-led live webinar training is also available. This is particularly useful if you have staff located in different locations.</p>

            </div>
        </div>


        <form class="form-steps">
            <div class="step-item" data-aos="fade-up" data-aos-delay="200">
                <h4>STEP 1: <span>Choose a Format*</span></h4>

                <ul class="list-radio-checkbox">
                    <li><label class="custom-checkbox"><input type="checkbox" name="step1" checked><i></i> Onsite at my company</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step1"><i></i> Private class at Training Connection (Chicago and Los Angeles only)</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step1"><i></i> Live private online class (Suitable for groups of 2 or more)</label></li>

                </ul>
            </div>
            <div class="step-item" data-aos="fade-up" data-aos-delay="100">
                <h4>STEP 2: <span>Choose Course Category*</span></h4>

                <ul class="list-radio-checkbox row-2">
                    <li><label class="custom-radio"><input type="radio" name="step2" checked><i></i> Adobe Training</label></li>
                    <li><label class="custom-radio"><input type="radio" name="step2"><i></i> Web Development</label></li>
                    <li><label class="custom-radio"><input type="radio" name="step2"><i></i> Apple</label></li>
                    <li><label class="custom-radio"><input type="radio" name="step2"><i></i> Business Skills</label></li>
                    <li><label class="custom-radio"><input type="radio" name="step2"><i></i> Microsoft Office</label></li>
                    <li><label class="custom-radio"><input type="radio" name="step2"><i></i> Other</label></li>

                </ul>
            </div>
            <div class="step-item" data-aos="fade-up" data-aos-delay="100">
                <h4>STEP 3: <span>Select a Course/s*</span></h4>

                <ul class="list-radio-checkbox row-2">
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3" checked><i></i> Acrobat DC Fundamentals</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Dreamweaver 2019 </label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> After Effects Fundamentals</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Fundamentals</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> After Effects Advanced</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Illustrator 2019 Quickstart</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> After Effects Bootcamp</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Illustrator 2019 Fundamentals</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Captivate 2019 Fundamentals</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Illustrator 2019 Advanced</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Captivate 2019 Advanced</label></li>
                    <li><label class="custom-checkbox"><input type="checkbox" name="step3"><i></i> Illustrator 2019 Bootcamp</label></li>

                </ul>
            </div>
            <div class="step-item" data-aos="fade-up" data-aos-delay="100">
                <h4>STEP 4: <span>Add details*</span></h4>


                <div class="row">
                    <div class="col-md-8 col-lg-5">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter number of trainees*">
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add additional comments (e.g. any customization)"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="step-item" data-aos="fade-up" data-aos-delay="100">
                <h4>STEP 5: <span>Add contact information</span></h4>

                <div class="row row-sm">
                    <div class="col-md-6 col-lg-5">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Company Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Mailing Address 1*">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2*">
                        </div>

                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Zip**">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone*">
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email*">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions mt-4 text-left" data-aos="fade-up" data-aos-delay="100">
                <button class="btn btn-primary btn-lg">
                    <span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span>
                    Obtain Quote
                </button>
            </div>
        </form>

    </div>

</main>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>