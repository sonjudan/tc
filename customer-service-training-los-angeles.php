<?php
$meta_title = "Customer service course in Los Angeles";
$meta_description = "Customer Service classes offered in Downtown Los Angeles. For great a deal or group quotation call 888.815.0604.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder" aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted" >
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="#" itemprop="url">
                        <span itemprop="title">Customer Service</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>


    <div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-3.jpg');">
        <div class="container">


            <div class="masterhead-copy">
                <h1 class="mb-2" data-aos="fade-up" >Customer Service Training</h1>

                <div data-aos="fade-up" data-aos-delay="100">
                    <h4 class="mt-0">Instructor-led customer sevice classes in Los Angeles</h4>
                </div>

                <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>
            </div>
        </div>
    </div>


    <div class="section section-intro-2" style="background-image:url('dist/images/bg-hero-5.jpg')">
        <div class="container">
            <div class="section-heading" data-aos="fade-up">
                <h2>​Exceptional Customer Service</h2>
            </div>

            <div data-aos="fade-up">
                <p>Delivering outstanding customer service is vital to any organization's long-term survival. Our 1-day customer service workshop will provide participants with the tools and skills to provide excellent customer service to your customers and thereby creating a loyal customer base.</p>
                <p>This class is suitable for any person who regularly deals with customers, be it face-to-face, via email or over the phone. Customers can also be defined as internal customers.</p>
            </div>
        </div>
    </div>

    <div id="section-book-course" class="section section-book-class">
        <div class="container">

            <div class="section-heading" data-aos="fade-up">
                <h2>Book a Customer Service Class</h2>
            </div>

            <div class="book-class" data-aos="fade-up">
                <div class="copy">
                    <h3 class="title-l2" data-aos="fade-up" data-aos-delay="250">What's Included</h3>
                    <ul data-aos="fade-up" data-aos-delay="250">
                        <li>Class certificate</li>
                        <li>Training materials</li>
                        <li>FREE repeat valid for 6 months</li>
                    </ul>
                </div>
                <div class="price-box" data-aos="fade-up" data-aos-delay="300">
                    <h3>$350</h3>
                    <h4>1 Day</h4>
                    <p>9:00am - 4:30pm</p>
                </div>
            </div>

            <div class="course-actions" data-aos="fade-up" data-aos-delay="350">
                <a href="#" class="btn btn-secondary btn-lg js-modal-data" data-target="#book-class-level" data-toggle="modal" data-classes="Customer Service Class - 1 DAY 9:00am 4:30pm" data-price="$350">
                    <span class="iconify" data-icon="simple-line-icons:layers" data-inline="false"></span>
                    Book Course
                </a>

                <a href="#" class="btn btn-primary btn-lg js-modal-data" data-target=".modal-timeable-la" data-toggle="modal" data-timetable="Los Angeles" data-classes="Customer Service" >
                    <span class="iconify" data-icon="simple-line-icons:calendar" data-inline="false"></span>
                    TimeTable
                </a>

                <a href="/downloads/business-skills/Exceptional%20Customer%20Service.pdf" class="btn btn-dark btn-lg" target="_blank">
                    <span class="iconify" data-icon="simple-line-icons:cloud-download" data-inline="false"></span>
                    Download PDF
                </a>

                <a href="#section-course-services" class="btn btn-blue btn-lg js-anchor-scroll">
                    <span class="iconify" data-icon="simple-line-icons:people" data-inline="false"></span>
                    Group Training
                </a>
            </div>
        </div>
    </div>

    <div class="section section-course-detail">
        <div class="container">
            <div class="section-heading aos-init w-auto text-center aos-animate" data-aos="fade-up">
                <h2>Detailed Course Outline</h2>
            </div>
            <h3 class="title-l2">Learning Objectives</h3>

            <div class="row">
                <div class="col-lg-6">
                    <div class="copy copy-ul-sm">

                        <ul class="">
                            <li>Defining what Great Customer Service looks like</li>
                            <li>Customer Generational differences</li>
                            <li>Developing the right Attitude and Skills</li>
                            <li>Developing key Communication Skills</li>
                            <li>Making a great First Impression</li>
                            <li>Reading Body Language</li>
                            <li>Asking Questions and Listening Skills</li>
                            <li>Establishing Rapport and Building Relationships</li>
                            <li>Effective Email Communication</li>
                            <li>Knowing the Importance of a Complaint</li>
                            <li>Recovering a Difficult Customer</li>
                            <li>10 Key Customer Service techniques</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="/dist/images/courses/customer-service/customer-service.jpg" alt="Customer Service classes in LA">
                </div>
            </div>
        </div>
    </div>

    <div class="section section-outline-bs">
        <div class="container">
            <h3 class="title-l2">Lesson Summaries</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="accordion" id="courseDetails" data-aos="fade-up">
                        <div class="card">
                            <div class="card-header" id="heading0">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse1">
                                    The Impact Customer Service Training <i></i>
                                </a>
                            </div>

                            <div id="collapse0" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Savvy businesses know that staff who deliver great customer service, resulting in greater sales and customer loyalty, are indispensable. The best customer service reps are highly valued and are often promoted to more senior roles.</p>
                                    <p>The attitude and communication skills participants learn on this class and when interacting with customers and colleagues, will quickly enhance their reputations ​and boost their careers.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading1">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    Customer Service Today <i></i>
                                </a>
                            </div>

                            <div id="collapse1" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>In this lesson we start off by defining customers (both internal and external). We discuss what customers want and then define what great customer service looks like in today’s world.</p>
                                    <p>Great customer service may look different for a millennial than it does for a baby boomer, so we look at the generational differences of the 4 major generations of today, namely the baby boomers, generation x, the millenials, and generation z.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading2">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    This Thing Called “Loyalty” <i></i>
                                </a>
                            </div>

                            <div id="collapse2" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Repeat business or customer loyalty is the lifeblood of any organization. In this lesson we define a lifetime customer, and what drives a customer to remain loyal. We also consider the cost of losing a customer.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading3">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Having the Right Skills and Attitude <i></i>
                                </a>
                            </div>

                            <div id="collapse3" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Establishing the right attitude is key to delivering great customer service. In this unit we will explore all the characteristics of a high performing customer service representative. The attitude, skills and the characteristics of high performing service providers.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="heading5">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                    Communication Skills <i></i>
                                </a>
                            </div>

                            <div id="collapse5" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails">
                                <div class="card-body copy">
                                    <p>Learning to communicate well is key to delivering outstanding customer service.  In this lesson participants will learn to make a great first and lasting impression, how to build rapport with customers, how to engage in active listening to better understand the customer’s needs, how to read body language to help in reading a situation, the power of asking questions, using language that engages and influences, and finish off with learning 5 key communication principles.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="accordion" id="courseDetails2" data-aos="fade-up">
                        <div class="card">
                            <div class="card-header" id="heading6">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                    Email and Customer Service <i></i>
                                </a>
                            </div>

                            <div id="collapse6" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>More and more customer engagement today takes place via email. This has tremendous advantages and disadvantages. We learn what customers expect using this medium, how to write effective emails and good email etiquette.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading7">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse1">
                                    Value of a Complaint <i></i>
                                </a>
                            </div>

                            <div id="collapse7" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>A complaint is often a gift for a business. In this lesson we review the ground rules for receiving a complaint, hot buttons and forbidden language.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading8">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse1">
                                    Recovering Difficult Customers <i></i>
                                </a>
                            </div>

                            <div id="collapse8" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>Dealing with difficult or angry customers can be a challenge. In this lesson you will learn seven diffusing techniques and how to best manage your own emotions. You will learn to disengage which is a useful technique when dealing with an aggressive customer and ways for your customer to save face and avoid an embarrassing situation.</p>
                                    <p>Finding ways to say “yes” and offering your customer choices is a great way to empower the customer. And follow-up procedure will ensure the customer is fully recovered.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading9">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse1">
                                    Measuring Customer Satisfaction <i></i>
                                </a>
                            </div>

                            <div id="collapse9" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>Here we look at the importance of customer feedback and different methods for collecting feedback. Net Promoter Score (NPS) is a great method to measure customer satisfaction.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading10">
                                <a class="collapsed" href="#" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse1">
                                    10 Key Customer Service techniques <i></i>
                                </a>
                            </div>

                            <div id="collapse10" class="collapse" aria-labelledby="headingOne" data-parent="#courseDetails2">
                                <div class="card-body copy">
                                    <p>The class finishes off with practical tips on what should be done  in 10 unique situations such as when a customer asks for a discount, when a customer threatens to defect to the competition, or when a customer expresses displeasure.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-copy" data-aos="fade-up">
                <p>Our outlines are a guide to the content covered in a typical class. We may change or alter the course topics to meet the objectives of a particular class.</p>
            </div>
        </div>
    </div>

<div class="section section-trainers">
    <div class="container">
        <div class="section-heading" data-aos="fade-up">
            <h2>Meet the trainers</h2>
            <h3>We have a team of experienced customer service trainers</h3>
        </div>
        <div class="owl-carousel owl-instructors owl-theme instructors-list-block">
            <div class="list-block">
                <a href=".modal-instrutor-profile-carol" data-toggle="modal">
                    <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-carol.jpg')"></i>
                    <span>Carol</span>
                </a>
            </div>
            <div class="list-block">
                <a href=".modal-instrutor-profile-secret" data-toggle="modal">
                    <i class="list-block-img" style="background-image:url('/dist/images/instructors/profile-secret.jpg')"></i>
                    <span>Secret</span>
                </a>
            </div>
        </div>

        <div class="course-rating" data-aos="fade-up" data-aos-delay="150">
            <div class="star-rate star-rate-4_8">
                <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Customer Service classes - 5 rating"></span>
            </div>

            <h3>Google 4.8 star <span>By 478 trainees</span></h3>
        </div>

        <div data-aos="fade-up" data-aos-delay="100">
            <div class="owl-carousel owl-theme owl-reviews">
                <div class="item" >
                    <div class="card-quote light">
                        <i class="icon-quote"></i>
                        <p>&quot;I learned a great deal from my training today, both for my professional and personal development. Lena was excellent! She used a number of different approaches to help me understand and listened to my thoughts and views. I would encourage others to attend this workshop. &quot;</p>
                        <span><strong>Meloney Zeeb | Envoy Health</strong></span>
                    </div>
                </div>
                <div class="item" >
                    <div class="card-quote light">
                        <i class="icon-quote"></i>
                        <p>&quot;WOW, Carol is absolutely amazing! She is grounded, created a very safe space, authentic, powerful, on-point, passionate, masterful, insightful, present and beautiful! This has been one of the best courses I have taken in my professional career. Thank you Carol!&quot;</p>
                        <span><strong>Michael Rahman | REME</strong></span>
                    </div>
                </div>
                <div class="item">
                    <div class="card-quote light">
                        <i class="icon-quote"></i>
                        <p>&quot;I haven't taken classes in a very long time. I am very happy with this class. I have learned a lot. Many things I didn't know nor realized I needed. I definitely would love to take another class with instructor Carol. I think time management class would be lovely with her.&quot;</p>
                        <span><strong>Susie Moore</strong></span>
                    </div>
                </div>
                <div class="item">
                    <div class="card-quote light">
                        <i class="icon-quote"></i>
                        <p>&quot;I really enjoyed this class and take with me a lot of valuable strategies that I will use in my current position and moving forward. The instructor had great hands on activities and made it a fun experience; as well as, an informative class.&quot;</p>
                        <span><strong>Vanessa Soto | CaesarStone</strong></span>
                    </div>
                </div>
                <div class="item">
                    <div class="card-quote light">
                        <i class="icon-quote"></i>
                        <p>&quot;I was so pleasantly surprised by how much was covered in three days. There were just two of us in the class, but even if it were 10 of us, I think I'd have the same experience. Eva was so much fun, shared a lot of great information and knowledge and I really appreciated her real-life experiences with us. I can't wait to take Customer Service Advanced.&quot;</p>
                        <span><strong>Traci Toutant | Magnetar Capital</strong></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-footer" data-aos="fade-up" data-aos-delay="200">
            <a href="/testimonials.php/?course_id=25" class="btn btn-primary btn-lg">View more</a>
        </div>
    </div>
</div>

<div id="section-course-services" class="section section-course-services">
    <div class="container">
        <div class="section-heading" data-aos="fade-up">
            <h2>Group Customer Service Training</h2>
            <p>We have trained thousands of employees from all different industries.</p>

            <h3>Developing a culture of Customer Service excellence has tremendous benefits for any organization.</h3>
        </div>

        <div class="service-list row justify-content-center" data-aos="fade-up">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-happy.svg" alt="Happier workforce" width="72" height="72">
                    </div>
                    <p>Happier and less stressed workforce</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-turnover.svg" alt="Lower employee turnover" width="91" height="72">
                    </div>
                    <p>Lower staff turnover</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-communication.svg" alt="Improved internal communication" width="88" height="72">
                    </div>
                    <p>Better internal communication</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-cooperation.svg" alt="Better co-operation" width="130" height="46">
                    </div>
                    <p>Improved internal co-operation</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-like.svg" alt="Satisfied customers" width="88" height="72">
                    </div>
                    <p>Satisfied customers</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-repeat.svg" alt="More Repeat business" width="72" height="72">
                    </div>
                    <p>Repeat business</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="service-item">
                    <div class="service-icon">
                        <img src="/dist/images/icons/icon-profit.svg" alt="Bigger profits" width="102" height="72">
                    </div>
                    <p>Greater profits</p>
                </div>
            </div>

        </div>



        <div class="text-center">
            <p class="title-l4 mt-3 mb-4">Each class is fully customized to your needs, and how your organization engages with its customers.</p>


            <a href="#section-course-form" class="btn btn-secondary btn-lg js-anchor-scroll">Obtain Pricing</a>
        </div>
    </div>
</div>

<div class="section section-hero-copy copy section-td-arrow" data-aos="fade-up">
    <div class="container">
        <h3 class="title-l2">Full service offering</h3>

        <p>Whether you’re a small team of 10 or a large organization of 10,000 employees, whether you work in commerce or public service, we can assist you defining and develop training program that includes:</p>
        <ul>
            <li>Conducting a pre-training needs assessment (via conference call or questionnaires)</li>
            <li>Developing a blue print for service excellence</li>
            <li>Customizing a curriculum that empowers employees and addresses any existing challenges</li>
            <li>Providing effective trainers to teach the skills</li>
            <li>Develop a custom tvraining manuals and/or easy reference job aids offering post class support</li>
        </ul>
    </div>
</div>

<div id="section-course-form" class="section section-group-training">
    <div class="container">
        <div class="section-heading mb-0" data-aos="fade-up">
            <h2 data-aos="fade-up">Group Training Quotation</h2>
        </div>

        <div class="section-body" >
            <form action="" class="form-group-training">

                <h4 class="text-center" data-aos="fade-up" data-aos-delay="100">Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                        <h3><input type="radio" name="training">
                            <i></i> Online Webinar</h3>

                    </label>
                </div>

                <hr class="mt-4 mb-4" data-aos="fade-up" data-aos-delay="150">

                <ul class="list-radio-checkbox row-3 mb-4" data-aos="fade-up" data-aos-delay="200">
                      <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Business Communication</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Business Etiquette</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Business Leadership</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Business Writing</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Conflict Resolution</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills" checked><i></i> Customer Service</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Grammar</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Presentations</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Project Management</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills"><i></i> Time Management</label></li>
                </ul>



                <div class="" data-aos="fade-up" data-aos-delay="200">
                    <div class="row row-sm">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-actions"  data-aos="fade-up" data-aos-delay="130">
                    <button class="btn btn-secondary btn-lg"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-la.php'; ?>

<div class="section section-faq">
    <div class="container">

        <div class="section-heading mb-4">
            <h2 class="section-title">FAQ</h2>
        </div>

        <div class="card-deck card-faq-list">
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    What is the difference between Photoshop and Illustrator?
                </h4>

                <div class="card-body">
                    <p>Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt proident. </p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    What is the best way to learn Adobe Photoshop?
                </h4>
                <div class="card-body">
                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. </p>
                </div>
            </div>
            <div class="card card-faq">
                <h4 class="card-title">
                    <span class="iconify" data-icon="simple-line-icons:question" data-inline="false"></span>
                    Where is best place to take a photoshop class?
                </h4>
                <div class="card-body">
                    <p>Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="section section-resouces-hero pb-9">
    <div class="container">
        <div class="section-body">
            <h2>Resources</h2>
            <p>For more resources and articles on <br>exceptional customer service</p>

            <a href="/resources/customer-service.php" class="btn btn-primary btn-lg"><i class="fas fa-angle-double-right mr-2"></i> Read more</a>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-book.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-carol.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/modals/instructor-secret.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>