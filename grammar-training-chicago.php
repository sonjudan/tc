<?php
$meta_title = "Grammar Training Classes in Chicago | Training Connection";
$meta_description = "Private Grammar classes offered in Chicago. Don't settle for an average class! For great deals call 888.815.0604.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<nav class="breadcrumb-holder " aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb inverted" >
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="/final-cut-pro-training.php" itemprop="url">
                    <span itemprop="title">Grammar Training</span>
                </a>
            </li>
        </ol>
    </div>
</nav>

<div class="masterhead masterhead-page" style="background-image: url('/dist/images/banner-business-writing.jpg');">
    <div class="container">
        <div class="masterhead-copy">
            <h1 data-aos="fade-up" >Grammar Training Classes</h1>

            <div data-aos="fade-up" data-aos-delay="100">
                <p>Private group Grammar training in Chicago</p>

                <a href="#section-course-form" class="btn btn-secondary btn-lg js-anchor-scroll" >
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024" class="iconify" data-icon="simple-line-icons:layers" data-inline="false"><path d="M22 302l475 259q7 3 15 3q4 0 8-1t7-2l481-259q17-9 16-29q0-9-4.5-16.5T1007 245L532 4q-15-7-29 0L23 245q-8 4-13 11.5T5 273q0 20 17 29zM517 68l406 207l-411 221l-406-221zm484 412l-95-48l-68 37l80 41l-411 221l-406-221l85-43l-68-37l-101 50Q0 489-.5 508.5T16 537l475 259q8 4 16 4q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5zm0 224l-90-44l-68 37l75 37l-411 221l-406-221l79-36l-68-37l-95 43Q0 713-.5 732.5T16 761l475 259q3 1 5.5 2t5 1.5t5.5.5q4 0 7.5-1t7.5-3l480-259q17-9 17-28.5t-18-28.5z" fill="currentColor"></path></svg>
                    Obtain a quotation for group training
                </a>
            </div>
        </div>
    </div>
</div>


<div class="section section-instructors">
    <div class="container">
        <div class="section-heading mb-0">
            <h2 class="mb-4" data-aos="fade-up">Grammar Essentials Training</h2>

            <p data-aos="fade-up">Poor grammar is unprofessional and often associated with ignorance,  laziness or haste. Error-ridden messages reflect poorly on yourself and  your organization. </p>

            <p data-aos="fade-up" data-aos-delay="100">Carefully worded business communication is both purposeful and easily  understood. The recipient of such communication should be able to  understand the message without guessing at the writer's intent,  transposing misplaced words or deciphering poorly constructed sentences.</p>
        </div>

    </div>
</div>

<div id="section-book-course" class="section section-training-options pb-0">
    <div class="container">
        <div class="section-heading align-center mb-0" data-aos="fade-up">
            <h2>Detailed Course Outline</h2>
        </div>

        <div class="section-body" data-aos="fade-up" data-aos-delay="150">

            <div class="accordion-classes g-text-Grammar" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default pb-md-5">
                        <div class="post-img"><img src="/dist/images/icons/icon-grammar.png" alt="Grammar 2019"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Grammar Essentials
                                </a>
                            </h3>
                            <p>In this course, you will review the rules of grammar, identify common grammar errors, improve your use of punctuation, learn how to construct sentences, improve word choice, and refine your writing style. This will help you clearly articulate your ideas to others, streamline the directions and instructions that you deliver, and create impressive presentations and reports.</p>

                            <div class="group-action-inline">

                                <a href="#section-course-form" class="btn btn-md btn-secondary js-anchor-scroll" target="_blank">
                                    <i class="fas fa-users mr-2" aria-hidden="true"></i>
                                    Obtain a quotation for group training
                                </a>
                                <a href="/downloads/grammar/Grammar%20Essentials.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <em></em>
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse" aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identify Nouns, Pronouns and Verbs</h5>
                                        <ul>
                                            <li>Common and proper nouns</li>
                                            <li> Compound nouns</li>
                                            <li> Uses of nouns</li>
                                            <li> Types of pronouns</li>
                                            <li> Frequently misused pronouns</li>
                                            <li> Pronoun agreement</li>
                                            <li> Its vs. It's</li>
                                            <li> Transitive and intransitive verbs</li>
                                            <li> Verb tenses</li>
                                            <li> Active and passive voice</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identifying Adjectives and Adverbs</h5>
                                        <ul>
                                            <li>Descriptive Adjectives</li>
                                            <li> Limiting adjectives</li>
                                            <li> Adverbs modifying verbs</li>
                                            <li> Adverbs modifying sentences</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identifying Prepositions, Conjunctions, and Interjections</h5>
                                        <ul>
                                            <li>Types of prepositions</li>
                                            <li> Prepositional phrases</li>
                                            <li> Compound prepositions</li>
                                            <li> Coordinating conjunctions</li>
                                            <li> Subordinating conjunctions</li>
                                            <li> Interjections with formal writing</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Avoiding Hypercorrections</h5>
                                        <ul>
                                            <li>Hypercorrections</li>
                                            <li> Achieve simplicity</li>
                                            <li> Efficiency of expression</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identifying Rules</h5>
                                        <ul>
                                            <li>Singular and plural rules</li>
                                            <li> Subject and verb agreement</li>
                                            <li> Parallel sentence structure</li>
                                            <li> Dangling modifiers</li>
                                            <li> Misplaced modifiers</li>
                                            <li> Double negatives</li>
                                            <li> "To" and "Too"</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identifying Correct Punctuation</h5>
                                        <ul>
                                            <li>Parentheses in sentences</li>
                                            <li> Parentheses vs. brackets</li>
                                            <li> Commas</li>
                                            <li> Semicolons</li>
                                            <li> Quotation marks</li>
                                            <li> Numbers and symbols</li>
                                            <li> Correct capitalization</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Identifying Sentence Fragments, Run-ons, and Comma Splices</h5>
                                        <ul>
                                            <li>Problematic vs. acceptable fragments</li>
                                            <li> Effective solutions</li>
                                            <li> Run-ons</li>
                                            <li> Comma Splices</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Improving Word Choices</h5>
                                        <ul>
                                            <li>Misused nouns</li>
                                            <li> Misused business words</li>
                                            <li> Misused verbs</li>
                                            <li> Misused adjectives</li>
                                            <li> Misused Adverbs</li>
                                            <li> Synonyms and Antonyms</li>
                                            <li> Homonyms</li>
                                            <li> Troublesome Homonyms</li>
                                            <li> More troublesome Homonyms</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Building Effective Sentences</h5>
                                        <ul>
                                            <li>Articulating goals</li>
                                            <li> Developing positive presentations</li>
                                            <li> Measuring outcomes</li>
                                            <li> Considering the audience</li>
                                            <li> Formal and informal contexts</li>
                                            <li> Email communication</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Editing Effectively</h5>
                                        <ul>
                                            <li>Common spelling errors</li>
                                            <li> Special cases</li>
                                            <li> Errors of "-able" and "-ible"</li>
                                            <li> "i" before "e"</li>
                                            <li> Double consonants</li>
                                            <li> Foreign words and phrases</li>
                                            <li> Proofreading techniques</li>
                                            <li> Edit techniques</li>
                                            <li> Edit for brevity</li>
                                            <li> Edit for clarity</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<div id="section-course-form" class="section section-group-training">
    <div class="container">
        <div class="section-heading mb-0">
            <h2 data-aos="fade-up">Group Training Quotation</h2>
        </div>

        <div class="section-body">
            <form action="" class="form-group-training">

                <h4 class="text-center" data-aos="fade-up" data-aos-delay="100">Choose your Training Format</h4>
                <div data-aos="fade-up" data-aos-delay="150" class="option-group-training">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                        <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                        <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>
                    </label>
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>
                        <h3><input type="radio" name="training">
                            <i></i> Online Webinar</h3>
                    </label>
                </div>

                <hr class="mt-4 mb-4" data-aos="fade-up" data-aos-delay="150">

                <ul class="list-radio-checkbox row-3 mb-4" data-aos="fade-up" data-aos-delay="200">
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Communication</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Etiquette</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Leadership</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Business Writing</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Conflict Resolution</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Customer Service</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]" checked><i></i> Grammar</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Presentations</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Project Management</label></li>
                    <li><label class="custom-checkbox-2"><input type="checkbox" name="business_skills[]"><i></i> Time Management</label></li>
                </ul>

                <div class="" data-aos="fade-up" data-aos-delay="200">
                    <div class="row row-sm">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-actions"  data-aos="fade-up" data-aos-delay="130">
                    <button class="btn btn-secondary btn-lg"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>
