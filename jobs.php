<?php
$meta_title = "Jobs | Training Connection";
$meta_description = "We are always looking for quality trainers";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

<main class="page-single-content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Jobs</li>
            </ol>
        </nav>

        <div class="page-intro mb-3">
            <div class=" intro-copy">
                <h1 data-aos="fade-up" >Jobs at Training connection</h1>
                <h4 data-aos="fade-up" data-aos-delay="100">We are looking for contractors who can teach the following:</h4>

            </div>
        </div>
    </div>


    <div class="section-content-l2">
        <div class="container">
            <div class="card-default">
                <h4>Chicago</h4>
                <ul class="list-ablock">
                    <li><span>Soft Skills </span></li>
                </ul>
            </div>
            <div class="card-default">
                <h4>Los Angeles</h4>
                <ul class="list-ablock">
                    <li><span>Soft Skills</span></li>
                    <li><span>Cinema 4D</span></li>
                    <li><span>Davinci Resolve</span></li>
                    <li><span>AutoDesk Maya</span></li>
                </ul>
            </div>

            <div class="mt-5">
                <h2 >Apply to become a trainer</h2>
                <h4>at Training Connection</h4>


                <div class="row row-sm mt-4">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Website (if applicable)">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Linkedin Profile (if applicable)">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Online calendar (if applicable)">
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 1">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address 2">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="City*">
                        </div>

                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="State*">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Zip*">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" style="font-size: 15px;margin-top: 14px;">Are you willing to travel interstate for training?</label>
                            <div class="form-control d-flex">
                                <label class="custom-radio mr-3">
                                    <input type="radio" name="travel" checked><i></i> Yes
                                </label>
                                <label class="custom-radio">
                                    <input type="radio" name="travel"><i></i> No
                                </label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <textarea class="form-control" rows="5" placeholder="Brief description of your training experience*"></textarea>
                </div>


                <div class="form-group form-group-options">
                    <label for="">Select which classes you consider yourself an expert in:</label>




                    <h5>Adobe</h5>
                    <ul class="list-radio-checkbox row-3">
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> After Effects Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> After Effects Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> After Effects Bootcamp</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Captivate Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Captivate Advanced</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Dreamweaver Fundamentals</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Illustrator Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Illustrator Quickstart</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Illustrator Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Illustrator Bootcamp</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> InDesign Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> InDesign Quickstart</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> InDesign Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> InDesign Bootcamp</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Photoshop Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Photoshop Quickstart</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Photoshop Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Photoshop Bootcamp</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Premiere Pro Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Premiere Pro Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Premiere Pro Bootcamp</label></li>

                        <li><label class="custom-checkbox"><input type="checkbox" name="courseAdobe"><i></i> Acrobat DC Fundamentals</label></li>
                    </ul>

                    <h5>Apple</h5>
                    <ul class="list-radio-checkbox row-3">
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseApple"><i></i> Final Cut Pro</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseApple"><i></i> Keynote</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseApple"><i></i> Logic Pro</label></li>
                    </ul>

                    <h5>Business and Softskills</h5>
                    <ul class="list-radio-checkbox row-3">
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Effective Business Communication</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Effective Etiquette for Today's Workforce</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Business Writing with Positive Impact</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Exceptional Customer Service</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Grammar Essentials</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Effective Presentations</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Project Management</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseBusiness"><i></i> Effective Time Manangement Techniques</label></li>
                    </ul>

                    <h5>Microsoft Office</h5>
                    <ul class="list-radio-checkbox row-3">
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS"><i></i> Access Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS"><i></i> Access Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Excel Level 1 - Introduction</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Excel Level 2 - Intermediate</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Excel Level 3 - Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Excel Level 4 - Macros & VBA</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> OneNote Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Outlook Level 1 - Introduction</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Outlook Level 2 - Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> PowerPoint Level 1 - Introduction</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> PowerPoint Level 2 - Advanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Microsoft Project Level 1</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Microsoft Project Level 2</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Microsoft Visio Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Word Level 1 - Introduction</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseMS" ><i></i> Word Level 2 - Advanced</label></li>
                    </ul>

                    <h5>Web Development</h5>
                    <ul class="list-radio-checkbox row-3">
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> HTML5 and CSS Adnvanced</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> HTML5 and CSS Fundamentals</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> Mobile & Responsive Web Design</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> Javascript and jquery</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> PHP and MySQL Web Development</label></li>
                        <li><label class="custom-checkbox"><input type="checkbox" name="courseWebDev"><i></i> VA</label></li>
                    </ul>
                </div>

                <button class="btn btn-secondary btn-lg">Submit Your Application</button>


            </div>
        </div>
    </div>


</main>



<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>