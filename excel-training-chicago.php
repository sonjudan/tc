<?php
$meta_title = "Excel Training Classes | Chicago | Training Connection";
$meta_description = "Need to learn Microsoft Excel? Our Chicago instructor-led classes are still the best way to learn. Call 312.698.4475 and don't settle for an average class!";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <nav class="breadcrumb-holder " aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb inverted" >
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="/" itemprop="url">
                        <span itemprop="title">Home</span>
                    </a>
                </li>
                <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a href="" itemprop="url">
                        <span itemprop="title">Microsoft Excel Training</span>
                    </a>
                </li>
            </ol>
        </div>
    </nav>

    <div class="masterhead masterhead-page masterhead-ms" style="background-image: url('/dist/images/banner-excel.jpg');">
        <div class="container">
            <div class="book-training-holder">

                <div class="masterhead-copy">
                    <h1 data-aos="fade-up" >Excel Training<br>
                      Chicago
                    </h1>

                    <div data-aos="fade-up">
                        <h4>Excel 2013, 2016, 2019 &amp; 365</h4>
                        <p>Looking for a hands-on Microsoft Excel class in Chicago?</p>

                        <p>Our Excel classes are taught by instructors present in the classroom. They will teach you step-by-step how to master Microsoft Excel, with demonstrations, followed by hands-on exercises and  one-on-one help if needed. <br>
                        </p>
                    </div>
                </div>

                <div class="book-training-artwork"  data-aos="fade-up">
                    <img src="/dist/images/courses/ms-office/book-excel-2019.png" alt="MS Excel 2019 box shot">
                    <img src="../dist/images/ribbon-satisfaction.png" alt="Satisfaction Guaranteed" class="ribbon-img">
                </div>
            </div>

        </div>
    </div>


    <div class="section section-training-intro">
        <div class="container">
            <div class="copy intro-copy" data-aos="fade-up" class="aos-init aos-animate">

                <h3>What's Included</h3>
                <ul>
                    <li>a printed Excel training manual</li>
                    <li>certificate of course completion</li>
                    <li>free refresher class valid for  6 months</li>
                </ul>
                <p>
                    <strong>Book an Excel training course today. All classes guaranteed to run! </strong><br>
                    <a href="/onsite-training.php">Onsite Excel training available countrywide.</a></br>
                  <br>
                    <a href="/onsite-training.php"></a>
                </p>

            </div>

            <div class="course-intro-row">
                <div class="course-actions aos-init" data-aos="fade-up" data-aos-delay="50">
                    <a href="#section-book-course" class="btn btn-secondary btn-lg js-anchor-scroll">
                        <i class="fas fa-cart-plus mr-2"></i>
                        Book Course
                    </a>

                    <a href=".section-package-deals" class="btn btn-excel btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-box mr-2"></i>
                        Package deals
                    </a>

                    <a href=".section-course-form" class="btn btn-blue btn-lg js-anchor-scroll" target="_blank">
                        <i class="fas fa-users mr-2"></i>
                        Group Training
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div id="section-book-course" class="section section-accordion-classes pb-0"  data-aos="fade-up">
        <div class="container">
            <div class="section-heading mb-0" data-aos="fade-up">
                <h2>Microsoft Excel Course Outlines</h2>
            </div>

            <div class="accordion-classes g-text-excel" id="accordionClasses">
                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="MS Excel Level 1 - Introduction"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Excel Level 1 - Introduction
                                </a>
                            </h3>
                            <p>Suitable for beginners this class will teach you how to create, modify, format and print worksheets. You will learn basic formulas, functions, data manipulation, charting and more. </p>

                            <div class="group-action-inline">
                               <a href="#" class="btn btn-md btn-secondary" data-target="#book-class-level-1" data-toggle="modal">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/excel/Excel-Level-1.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <a href="#" class="btn btn-md  btn-primary" data-target=".modal-timeable-excel-chicago-1" data-toggle="modal" >
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>

                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-1" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-1" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-1"  data-toggle="collapse" data-target="#collapse-outline-1" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Excel Essentials</h5>
                                        <ul>
                                            <li>Starting Excel</li>
                                            <li>The Excel Interface</li>
                                            <li>Opening a Workbook</li>
                                            <li>Navigating around a Workbook</li>
                                            <li>Finding Data</li>
                                            <li>Entering Data</li>
                                            <li>Saving and Closing a Workbook</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Creating Worksheets</h5>
                                        <ul>
                                            <li>Types of Data</li>
                                            <li>Entering Text</li>
                                            <li>Adjusting Column Width</li>
                                            <li>Entering Numbers</li>
                                            <li>Types of Formulas</li>
                                            <li>Entering Formulas</li>
                                            <li>About Functions</li>
                                            <li>Inserting Functions</li>
                                            <li>Copying and Moving Data</li>
                                            <li>Copying Formulas</li>
                                            <li>Absolute and Relative References</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Formatting</h5>
                                        <ul>
                                            <li>Text Formatting</li>
                                            <li>Number Formatting</li>
                                            <li>Alignment</li>
                                            <li>Text Wrapping</li>
                                            <li>Merging Cell Data</li>
                                            <li>Borders and Highlighting</li>
                                            <li>Styles and Themes</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Manipulating Data</h5>
                                        <ul>
                                            <li>Data Entry Shortcuts</li>
                                            <li>Fill and Auto Fill</li>
                                            <li>Replacing Data</li>
                                            <li>Paste Options</li>
                                            <li>Inserting and Deleting Rows and Columns</li>
                                            <li>Hiding Rows and Columns</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Charts</h5>
                                        <ul>
                                            <li>About Charts</li>
                                            <li>Creating Charts</li>
                                            <li>Chart Types and Elements</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Output</h5>
                                        <ul>
                                            <li>Managing Worksheet Windows</li>
                                            <li>Freezing Panes</li>
                                            <li>Printing Worksheets</li>
                                            <li>Print Setup Options</li>
                                            <li>Print Areas and Titles</li>
                                            <li>Headers and Footers</li>
                                            <li>Sharing Workbooks</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Settings and Templates</h5>
                                        <ul>
                                            <li>Workbooks Options and Properties</li>
                                            <li>Working with Templates</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="MS Excel Level 2 - Intermediate"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    Excel Level 2 - Intermediate
                                </a>
                            </h3>
                            <p>On this course you will learn to manipulate data using sorting and filtering, conditional formating, PivotTables and advanced charting. You will share data across multiple workbooks, add graphics and learn to collaborate with others. Trainees should have completed our Introduction class or have the equivalent experience prior to attending this class.</p>

                            <div class="group-action-inline">
                               <a href="#" class="btn btn-md btn-secondary" data-target="#book-class-level-2" data-toggle="modal">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/excel/Excel-Level-2.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <div class="btn-group">
                                    <a href="#" class="btn btn-md  btn-primary" data-target=".modal-timeable-excel-chicago-2" data-toggle="modal" >
                                        <i class="far fa-calendar-alt mr-2"></i>
                                        TimeTable
                                    </a>
                                </div>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-2" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-2">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-2" class="accordion-collapse collapse " aria-labelledby="heading-2" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-2"  data-toggle="collapse" data-target="#collapse-outline-2" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Managing Workbooks</h5>
                                        <ul>
                                            <li>Inserting and Deleting Worksheets</li>
                                            <li>Rearranging Worksheets</li>
                                            <li>Adding Hyperlinks</li>
                                            <li>Linking to other Workbooks</li>
                                            <li>Customizing Excel</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Named Ranges</h5>
                                        <ul>
                                            <li>Using Names in Formulas</li>
                                            <li>Other ways to use Named Ranges</li>
                                            <li>Naming Shortcuts</li>
                                            <li>Managing Named Ranges</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Filtering and Sorting Data Tables</h5>
                                        <ul>
                                            <li>Sorting Data</li>
                                            <li>Custom Sorts</li>
                                            <li>Filtering Tables</li>
                                            <li>Using Auto-Filter</li>
                                            <li>Removing Duplicate Rows</li>
                                            <li>Working with Structured References</li>
                                            <li>Data Validation</li>
                                            <li>Transposing Rows and Columns</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Summarizing Data</h5>
                                        <ul>
                                            <li>Consolidating Data</li>
                                            <li>Using Sub-totals</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">PivotTables</h5>
                                        <ul>
                                            <li>About PivotTables</li>
                                            <li>Creating PivotTables</li>
                                            <li>Formatting PivotTables</li>
                                            <li>Manipulating PivotTables</li>
                                            <li>Viewing Subsets of PivotTable Data</li>
                                            <li>Adding Slicers</li>
                                            <li>Creating PivotCharts</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Presentation Features</h5>
                                        <ul>
                                            <li>Conditional Formatting</li>
                                            <li>Graphical Conditional Formatting</li>
                                            <li>Custom Formats</li>
                                            <li>Adding Graphics</li>
                                            <li>Adding Smart Art</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Charts</h5>
                                        <ul>
                                            <li>Special Chart Types</li>
                                            <li>Inserting Trendlines</li>
                                            <li>Combination Charts</li>
                                            <li>Chart Templates</li>
                                            <li>Inserting Sparklines</li>
                                            <li>Quick Analysis Options</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Collaboration</h5>
                                        <ul>
                                            <li>Controlling Access with Permissions</li>
                                            <li>Protecting and Securing Workbooks</li>
                                            <li>Shared Workbooks</li>
                                            <li>Adding Comments</li>
                                            <li>Tracking Changes</li>
                                            <li>Merging Shared Workbooks</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>


                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="MS Excel Level 3 - Advanced"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-3"  data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                    Excel Level 3 - Advanced
                                </a>
                            </h3>
                            <p>On this class you will learn advanced functions including LOOKUPS, Date  and Time Functions, Text Functions, Statistical Functions, and Financial  Functions. You will learn how to import and export data using the Power  Pivot Data model and analyze data using What-If Analysis, Scenarios  and Goal Seeker. Finally you will learn to automate tasks using Macros  and capture user input using Forms.</p>

                            <div class="group-action-inline">
                               <a href="#" class="btn btn-md btn-secondary" data-target="#book-class-level-3" data-toggle="modal">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/excel/Excel-Level-3.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>


                                <a href="#" class="btn btn-md  btn-primary" data-target=".modal-timeable-excel-chicago-3" data-toggle="modal" >
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                    <i class="fas fa-list-ul mr-2"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-3" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-3">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>350
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-3" class="accordion-collapse collapse " aria-labelledby="heading-3" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-3"  data-toggle="collapse" data-target="#collapse-outline-3" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Logical and Lookup Functions</h5>
                                        <ul>
                                            <li>The IF Function</li>
                                            <li>Conditional Calculations</li>
                                            <li>Using SUMIF</li>
                                            <li>VLOOKUPS and HLOOKUPS</li>
                                            <li>Range LOOKUPS</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Advanced Formulas</h5>
                                        <ul>
                                            <li>Auditing and Error Trapping</li>
                                            <li>Formula Options</li>
                                            <li>Array Functions</li>
                                            <li>Using TRANSPOSE</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Special Functions</h5>
                                        <ul>
                                            <li>Date and Time Functions
                                                <ul>
                                                    <li> TODAY</li>
                                                    <li> NOW</li>
                                                    <li> DATE</li>
                                                </ul>
                                            </li>
                                            <li>Date Calculations</li>
                                            <li>Text Functions
                                                <ul>
                                                    <li>Using CONCATENATE and TRIM</li>
                                                    <li>Using UPPER, LOWER and PROPER</li>
                                                    <li>Extracting Text</li>
                                                </ul>
                                            </li>
                                            <li>Statistical Functions
                                                <ul>
                                                    <li>Using MIN and MAX</li>
                                                    <li>Using COUNT, COUNTA and COUNTBLANK</li>
                                                </ul>
                                            </li>
                                            <li>Financial Functions
                                                <ul>
                                                    <li>Using FV</li>
                                                    <li>Using PMT</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Importing and Exporting</h5>
                                        <ul>
                                            <li>The Power Pivot Data Model</li>
                                            <li>Importing Data</li>
                                            <li>Using Power Pivot</li>
                                            <li>Exporting Data</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Data Analysis</h5>
                                        <ul>
                                            <li>What-if Analysis</li>
                                            <li>Scenarios</li>
                                            <li>Using Goal Seek</li>
                                            <li>The Analysis Toolpak</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Macros and Forms</h5>
                                        <ul>
                                            <li>Recording Macros</li>
                                            <li>Running Macros</li>
                                            <li>Running Macros when Workbook is Open or Closed</li>
                                            <li>Using Forms to Record Input</li>
                                        </ul>
                                    </div>
                                </div>


                            </div>


                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="accordion-item">
                    <article class="post-default">
                        <div class="post-img"><img src="/dist/images/icons/icon-office-excel.png" alt="MS Excel Level 4 - Macros & VBA"></div>
                        <div class="post-body">
                            <h3 class="post-title">
                                <a href="#collapse-outline-4"  data-toggle="collapse" data-target="#collapse-outline-4" aria-expanded="true" aria-controls="collapse-4">
                                    Excel Level 4 - Macros & VBA
                                </a>
                            </h3>
                            <p>This course will help advanced Excel users  develop the skills required to write and debug powerful VBA code to automate Excel  operations such as inserting and formatting text, sorting and  duplicating data, generating reports and more. You will also learn how to  create interactive forms, insert calculations and handle run-time  errors.</p>

                            <div class="group-action-inline">
                               <a href="#" class="btn btn-md btn-secondary" data-target="#book-class-level-4" data-toggle="modal">
                                    <i class="fas fa-cart-plus mr-2"></i>
                                    Book this course
                                </a>
                                <a href="/downloads/excel/Excel-Level-4.pdf" class="btn btn-md btn-dark" target="_blank">
                                    <i class="far fa-file-pdf mr-2"></i>
                                    Download PDF
                                </a>

                                <a href="#" class="btn btn-md  btn-primary" data-target=".modal-timeable-excel-chicago-4" data-toggle="modal" >
                                    <i class="far fa-calendar-alt mr-2"></i>
                                    TimeTable
                                </a>
                                <button class="btn btn-md btn-blue" type="button" data-toggle="collapse" data-target="#collapse-outline-4" aria-expanded="true" aria-controls="collapse-4">
                                    <i class="fas fa-list-ul mr-3"></i>
                                    Detailed Outline
                                </button>
                            </div>

                        </div>
                        <div class="post-side">
                            <div class="accordion-action">
                                <a href="#collapse-outline-4" class="collapsed" data-toggle="collapse" data-target="#collapse-outline-4" aria-expanded="true" aria-controls="collapse-4">
                                    <span class="c-open"><i class="fas fa-plus"></i></span>
                                    <span class="c-close"><i class="fas fa-times"></i></span>
                                </a>
                            </div>
                            <div class="price-lbl">
                                <span>1 Day</span>
                                <sup>$</sup>450
                            </div>
                        </div>
                    </article>

                    <div id="collapse-outline-4" class="accordion-collapse collapse " aria-labelledby="heading-1" data-parent="#accordionClasses">

                        <div class="post-accordion-collapse">
                            <h3>
                                <span>Detailed Course Outline</span>
                                <a href="#collapse-outline-4"  data-toggle="collapse" data-target="#collapse-outline-4" aria-expanded="true" aria-controls="collapse-1">
                                    Hide outline
                                </a>
                            </h3>

                            <div class="card-columns">

                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">VBA Concepts</h5>
                                        <ul>
                                            <li>The Object Model Hierarchy</li>
                                            <li>Understanding Object Properties, Methods and Events</li>
                                            <li>VBA Module and Procedure Types</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Accessing VBA Functionality</h5>
                                        <ul>
                                            <li>Macro Security Options</li>
                                            <li>Using the Macro Recorder</li>
                                            <li>Writing and Editing Macros</li>
                                            <li>Essential VB Editor Components</li>
                                            <li>Exploring the Object Browser</li>
                                            <li>Using VBA Comments and Formatting to Write Solid Code</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Debugging and Error Handling</h5>
                                        <ul>
                                            <li>Types of Programming Errors</li>
                                            <li>VBE Debugging Tools</li>
                                            <li>Error Handling Code</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Using Variables to Store Data</h5>
                                        <ul>
                                            <li>VBA Data Types and Naming Rules</li>
                                            <li>Declaring Variables</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">VBA Looping and Decision Structures</h5>
                                        <ul>
                                            <li>For...and Do...Loops for Repetitive Actions</li>
                                            <li>If..then Statements</li>
                                            <li>Select Case Statements</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">User-Interactive Worksheets</h5>
                                        <ul>
                                            <li>Capturing User Input</li>
                                            <li>Dialog Box Types</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card card-list">
                                    <div class="card-body copy">
                                        <h5 class="card-title">Performing Calculations with VBA</h5>
                                        <ul>
                                            <li>Using VBA’s Built-in Functions</li>
                                            <li>Writing User-Defined Functions for Custom Calculations</li>
                                            <li>Code to Automate Inserting SUM Functions</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>


                            <div class="section-note">
                                <p>"Our outlines are a guide to the content covered on a typical class. We may change or alter the course topics to meet the objectives of a particular class."</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="section section-package-deals">
        <div class="container">
            <div class="section-heading" data-aos="fade-up">
                <h2>Package deals</h2>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="150">
                <div class="card-deck card-row-sm card-deck-training">

                    <div class="card card-training-plans card-package-excel">
                        <h3 class="card-title sm"><span>Excel 2-Level Package</span></h3>
                        <div class="card-body">
                            <div class="card-text">
                                <h3 class="card-price">$600</h3>
                            </div>

                            <p>Book any two levels of Excel Levels 1, 2, or 3 and save $100!</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-secondary" data-target="#book-package-deal-1" data-toggle="modal">
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book Package
                            </a>
                        </div>
                    </div>

                    <div class="card card-training-plans card-package-excel">
                        <h3 class="card-title sm"><span>Excel Levels 3 and 4 Package</span></h3>
                        <div class="card-body">
                            <div class="card-text">
                                <h3 class="card-price">$700</h3>
                            </div>

                            <p>Book Excel Level 3 and Excel VBA and save $100!</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-secondary" data-target="#book-package-deal-2" data-toggle="modal">
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book Package
                            </a>
                        </div>
                    </div>

                    <div class="card card-training-plans card-package-excel">
                        <h3 class="card-title sm"><span>Excel 3-Level Package</span></h3>
                        <div class="card-body">
                            <div class="card-text">
                                <h3 class="card-price">$900</h3>
                            </div>

                            <p>Book Levels 1-3 of Excel training and save $150!</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-secondary" data-target="#book-package-deal-3" data-toggle="modal">
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book Package
                            </a>
                        </div>
                    </div>

                    <div class="card card-training-plans card-package-excel">
                        <h3 class="card-title sm"><span>Excel Levels 2, 3, and 4 Package</span></h3>
                        <div class="card-body">
                            <div class="card-text">
                                <h3 class="card-price">$1000</h3>
                            </div>

                            <p>Book Excel Levels 2, 3, and VBA and save $150.</p>
                        </div>
                        <div class="card-footer">
                            <a href="#" class="btn btn-secondary" data-target="#book-package-deal-4" data-toggle="modal">
                                <i class="fas fa-cart-plus mr-2"></i>
                                Book Package
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/ps/section-cta-chat.php'; ?>


    <div class="section">
        <div class="container">
            <div class="section-heading  w-auto" data-aos="fade-up">
                <h2>Excel training reviews</h2>
            </div>
            <div class="section-body"  >
                <div class="course-rating" data-aos="fade-up" data-aos-delay="100">
                    <div class="star-rate star-rate-4_8">
                        <span><img src="/dist/images/icons/icon-five-stars.svg" alt="Microsoft Excel classes - 5 rating"></span>
                    </div>

                    <h3><span>Excel classes rating:</span> 4.8 stars from 6,617 reviewers </h3>
                </div>

                <div data-aos="fade-up" data-aos-delay="150">

                    <div class="owl-carousel owl-theme owl-reviews" >
                        <div class="item" >
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Thank you. The training was Fantastic. Appreciate your patience and detail. I am truly taking tools with me that will help with my efficiency.&quot;</p>
                                <span><strong>Juan Flores | Children's Institute</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;After 3 days of Excel training my brain is full! This was valuable time spent. Rich was an excellent instuctor. &quot;</p>
                                <span><strong>Mark Buczko</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Sandy Dinkel was amazing. She took her time to explain all the concepts to us as well as show us short cuts and multiple ways to get to the same end result. I would DEFINITELY recommend training connections to everyone I know. Thanks training connections!!&quot;</p>
                                <span><strong>Jeanne Whitfield | Union Pacific Railroad</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Trainer Chris is great! He made learning this excel stuff fun and entertaining. I would highly recommend this class to others, in fact I am recommending it to other departments within my firm.&quot;</p>
                                <span><strong>David Oshiro</strong></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card-quote light">
                                <i class="icon-quote"></i>
                                <p>&quot;Our instructor, Allyncia, was extremely friendly and helpful. The classroom atmosphere was welcoming, the facilities were great, and I learned an incredible amount about Excel. I would highly recommend taking classes at Training Connection.&quot;</p>
                                <span><strong>Marie Becknell</strong></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-footer text-center mt-4" data-aos="fade-up" data-aos-delay="200">
                <a href="/testimonials.php?course_id=19" class="btn btn-primary btn-lg">View all student reviews</a>
            </div>
        </div>
    </div>

    <div class="section section-guarantee">
        <div class="container container-sm">
            <div class="section-heading">
                <h2 class="mb-3" data-aos="fade-up">Free Repeat </h2>
                <p data-aos="fade-up" data-aos-delay="150">We recognize that Microsoft Excel is a complex program, and that trainees may benefit from repeating the class. Included in your course price is a FREE Repeat valid for 6 months. Often the repeat class can be with a different trainer too. <br>
                </p>
            </div>

            <div class="section-body">
                <img data-aos="fade-up" data-aos-delay="200" src="/dist/images/ribbon-guarantee.png" alt="Money back guarantee">
                <h3 class="heading-l1" data-aos="fade-up" data-aos-delay="200"><span>Our Walk Away, No Hard Feelings</span>Totally Outrageous Guarantee</h3>
                <p data-aos="fade-up" data-aos-delay="250">Microsoft Excel is not for everyone. If you decide that after half a day  in class that the software is not for you, we will give you a complete refund. We will even let you keep the hard-copy training manual in case you want to give it another go in the future.</p>
            </div>
        </div>
    </div>


    <div id="section-course-form" class="section section-course-form">
        <div class="container">
            <div class="section-heading w-auto">
                <h2 class="mb-3" data-aos="fade-up">Group Excel Training </h2>
                <p data-aos="fade-up" data-aos-delay="100">We offer group training in Excel. This can be delivered onsite at your premises, at our training center in the LOOP or via online webinar.
                    <br>
                    <strong>Fill out the form below to receive pricing.</strong>
                </p>
            </div>

            <div class="section-body" data-aos="fade-up" data-aos-delay="200">
                <form action="" class="form-group-training">

                    <h4 class="text-center pt-3">Step 1: Choose your Training Format</h4>
                    <div data-aos="fade-up" data-aos-delay="150" class="option-group-training mt-0 pb-4">
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="34px" height="59px"><path fill-rule="evenodd"  fill="rgb(47, 53, 66)"d="M22.340,45.000 C28.583,45.996 32.691,48.614 32.691,51.775 C32.691,55.898 25.943,59.007 16.993,59.007 C8.044,59.007 1.293,55.898 1.293,51.775 C1.293,48.614 5.403,45.996 11.648,45.000 C6.147,35.512 -0.008,23.450 -0.008,16.925 C-0.008,7.597 7.618,0.006 16.993,0.006 C26.366,0.006 33.995,7.597 33.995,16.925 C33.995,23.440 27.838,35.507 22.340,45.000 ZM3.462,51.775 C3.462,54.178 9.019,56.848 16.993,56.848 C24.966,56.848 30.521,54.178 30.521,51.775 C30.521,49.983 27.045,47.754 21.184,46.992 C20.893,47.483 20.645,47.893 20.383,48.330 C20.256,48.541 20.121,48.768 20.004,48.964 C19.658,49.535 19.361,50.016 19.092,50.462 C19.031,50.557 18.963,50.670 18.904,50.760 C18.650,51.177 18.461,51.481 18.298,51.733 C18.249,51.811 18.196,51.901 18.158,51.960 C17.997,52.215 17.910,52.351 17.910,52.351 L16.990,53.788 L16.076,52.351 C16.076,52.351 15.979,52.202 15.814,51.937 C15.796,51.911 15.771,51.867 15.754,51.846 C15.588,51.579 15.373,51.231 15.092,50.776 C15.018,50.657 14.929,50.510 14.847,50.379 C14.591,49.957 14.320,49.515 13.995,48.984 C13.865,48.763 13.716,48.521 13.578,48.287 C13.323,47.859 13.085,47.471 12.803,46.992 C6.943,47.754 3.462,49.983 3.462,51.775 ZM16.993,2.168 C8.815,2.168 2.161,8.783 2.161,16.925 C2.161,23.612 9.775,37.566 14.317,45.293 C15.414,47.157 16.353,48.691 16.993,49.733 C17.637,48.691 18.577,47.157 19.673,45.293 C24.212,37.561 31.821,23.604 31.821,16.925 C31.821,8.783 25.172,2.168 16.993,2.168 ZM6.359,16.925 C6.359,11.090 11.129,6.343 16.993,6.343 C22.858,6.343 27.631,11.090 27.631,16.925 C27.631,22.765 22.858,27.517 16.993,27.517 C11.129,27.517 6.359,22.765 6.359,16.925 ZM25.458,16.925 C25.458,12.279 21.662,8.503 16.993,8.503 C12.327,8.503 8.530,12.279 8.530,16.925 C8.530,21.574 12.327,25.355 16.993,25.355 C21.662,25.355 25.458,21.574 25.458,16.925 Z"/></svg>
                            <h3><input type="radio" name="training" checked><i></i> Onsite Training</h3>
                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="46px" height="57px"><path fill-rule="evenodd"  fill="rgb(23, 23, 23)"d="M45.930,57.000 L0.070,57.000 L0.025,55.841 C0.008,55.387 -0.000,55.034 -0.000,54.698 C-0.000,40.373 10.318,28.719 23.000,28.719 C35.682,28.719 46.000,40.373 46.000,54.698 C46.000,55.034 45.992,55.387 45.975,55.841 L45.930,57.000 ZM2.398,54.588 L43.602,54.588 C43.550,41.644 34.328,31.131 23.000,31.131 C11.672,31.131 2.450,41.644 2.398,54.588 ZM23.000,24.773 C16.208,24.773 10.682,19.217 10.682,12.387 C10.682,5.557 16.208,-0.000 23.000,-0.000 C29.792,-0.000 35.318,5.557 35.318,12.387 C35.318,19.217 29.792,24.773 23.000,24.773 ZM23.000,2.412 C17.530,2.412 13.081,6.886 13.081,12.387 C13.081,17.887 17.530,22.362 23.000,22.362 C28.470,22.362 32.919,17.887 32.919,12.387 C32.919,6.886 28.470,2.412 23.000,2.412 Z"/></svg>
                            <h3><input type="radio" name="training"><i></i> Private Training<span>at Training Connection</span></h3>

                        </label>
                        <label>
                            <svg xmlns="http://www.w3.org/2000/svg"xmlns:xlink="http://www.w3.org/1999/xlink"width="75px" height="63px"><path fill-rule="evenodd"  fill="rgb(54, 58, 67)"d="M68.634,54.590 L49.784,54.590 L49.784,60.342 L57.052,60.342 C57.787,60.342 58.383,60.937 58.383,61.671 C58.383,62.405 57.787,63.000 57.052,63.000 L49.784,63.000 L25.216,63.000 L17.948,63.000 C17.213,63.000 16.617,62.405 16.617,61.671 C16.617,60.937 17.213,60.342 17.948,60.342 L25.216,60.342 L25.216,54.590 L6.366,54.590 C2.855,54.590 -0.000,51.738 -0.000,48.232 L-0.000,6.358 C-0.000,2.852 2.855,-0.000 6.366,-0.000 L68.634,-0.000 C72.144,-0.000 75.000,2.852 75.000,6.358 L75.000,48.232 C75.000,51.738 72.144,54.590 68.634,54.590 ZM47.123,60.342 L47.123,54.616 L27.877,54.616 L27.877,60.342 L47.123,60.342 ZM72.339,6.358 C72.339,4.318 70.677,2.658 68.634,2.658 L6.366,2.658 C4.323,2.658 2.661,4.318 2.661,6.358 L2.661,45.005 L72.339,45.005 L72.339,6.358 ZM72.339,47.663 L2.661,47.663 L2.661,48.232 C2.661,50.272 4.323,51.932 6.366,51.932 L68.634,51.932 C70.677,51.932 72.339,50.272 72.339,48.232 L72.339,47.663 ZM37.500,23.139 C45.307,23.139 51.658,30.230 51.658,38.945 C51.658,39.177 51.651,39.407 51.642,39.637 L51.593,40.915 L23.407,40.915 L23.358,39.637 C23.349,39.407 23.342,39.177 23.342,38.945 C23.342,30.230 29.694,23.139 37.500,23.139 ZM48.980,38.256 C48.666,31.325 43.637,25.797 37.500,25.797 C31.363,25.797 26.334,31.325 26.019,38.256 L48.980,38.256 ZM37.500,22.082 C33.159,22.082 29.628,18.555 29.628,14.219 C29.628,9.883 33.159,6.356 37.500,6.356 C41.841,6.356 45.372,9.883 45.372,14.219 C45.372,18.555 41.841,22.082 37.500,22.082 ZM37.500,9.014 C34.627,9.014 32.289,11.349 32.289,14.219 C32.289,17.089 34.627,19.423 37.500,19.423 C40.373,19.423 42.711,17.089 42.711,14.219 C42.711,11.349 40.373,9.014 37.500,9.014 Z"/></svg>

                            <h3><input type="radio" name="training">
                                <i></i> Online Webinar</h3>

                        </label>
                    </div>



                    <div class="row">
                        <div class="col-md-6 pr-1 mb-3">
                            <h4>Step 2: Choose your Course/s</h4>

                            <div class="option-group-training inline-block">
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" checked><i></i> <h3>Excel Level 1 - Introduction</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" ><i></i> <h3>Excel Level 2 - Intermediate</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" ><i></i> <h3>Excel Level 3 - Advanced</h3>
                                </label>
                                <label class="custom-checkbox">
                                    <input type="checkbox" name="course" ><i></i> <h3>Excel Level 4 - Macros & VBA</h3>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Step 3: Enter Details</h4>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="No. of Trainees*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone no*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 1*">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address 2">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="City*">
                            </div>

                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="State*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Zip*">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control" placeholder="Add comment"></textarea>
                            </div>
                            <div class="form-actions">
                                <button class="btn btn-secondary"><span class="iconify" data-icon="simple-line-icons:pencil" data-inline="false"></span> Request Pricing </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations-chicago.php'; ?>

    <div class="section section-faq g-text-excel">
        <div class="container">
            <div class="section-heading align-left"  data-aos="fade-up">
                <h2>Excel Training FAQ</h2>
                <p>Training Connection is dedicated to providing the best Excel classes  in Chicago. Please find answers to some of our most frequently asked Excel training related questions below, or contact us should you require any further assistance.</p>
            </div>

            <ul class="list-faq" data-aos="fade-up">
                <li>
                    <h5>Is this face-to-face training or a webinar?</h5>
                    <p>Our Excel classes are taught by live trainers present in the classroom. This is not a webinar. All our trainers are Excel experts who are  passionate about teaching this  software program.</p>
                </li>
                <li>
                    <h5>What are the class times?</h5>
                    <p>Our MS Excel classes start at 9.00am and finish at approximately 4.30pm. Lunch hour typically starts at 12.15pm.</p>
                </li>
                <li>
                    <h5>Do I need to bring my own computer?</h5>
                    <p>No, we provide the computers at our training center in the Chicago Loop.</p>
                </li>
                <li>
                    <h5>Is this training suitable for Mac users?</h5>
                    <p>Yes, there are barely any differences between Excel 2016 and Excel 2019 on both platforms. We have a lot of Mac users attend our classes. You can even bring your Mac laptop to use in class.</p>
                </li>
                <li>
                    <h5>Which Excel training course should I attend?</h5>
                    <p>This depends on your current level of experience. If you’re learning from scratch  then our beginners class is the perfect introduction to the core concepts. Alternatively, you may consider either our intermediate or advanced classes if you have prior experience with the software. We have created an <a href="excel-class-guide.php">Excel Class Guide</a> to help you choose the right Excel class.</p>
                </li>
                <li>
                    <h5>Can you deliver Excel training onsite at our location?</h5>
                    <p>Yes, we service the greater Chicago metro including Arlington Heights, Aurora, Bollingbrook, Chicago Loop, Deerfield, Des Plaines, Evanston, Highland Park, Joliet, Naperville, Schaumburg, Skokie, South Chicago and surrounding areas.</p>
                    <p>Our trainers can also travel anywhere in the country to deliver Excel training. <a href="#section-course-form" class="js-anchor-scroll">Obtain a quotation</a> for onsite Excel training.</p>
                </li>
            </ul>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/resources-widget-excel.php'; ?>


<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-excel-book-chicago.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/sections/modals/modal-timestable-excel.php'; ?>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>