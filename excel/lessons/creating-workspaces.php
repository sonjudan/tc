<?php
$meta_title = "Creating workspaces in Excel | Training Connection";
$meta_description = "Learn to create and open a Workspace in Microsoft Excel. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Workspaces</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Creating Excel Workspaces">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Creating Excel Workspaces</h1>
                        <h5>The following topics are covered in our <a href="/excel-training.php">Excel Intermediate training class</a>.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Creating and Opening a Workspace</h4>
                <p>You use a workspace when you need to save a configuration of open workbooks on your system. In other words, let's say you've opened two or three workbooks, have arranged them satisfactorily, and then discover that you're out of time. Instead of repeating all the arranging the next time you start Excel, you simply save the arrangement as a workspace.</p>
                <p>The workspace file you create provides instructions to Excel about arranging and opening workbooks; however, it does not contain the workbooks themselves. When you open the workspace file, the actual workbooks appear. Because the workspace file contains pointers to the workbook locations, it’s important not to move the workbooks that are part of the workspace into new folders, unless you move them from within the workspace itself (i.e., by using the Save As command).</p>

                <p>To register for an Excel class in Chicago or Los Angeles call 888.815.0604.</p>

                <h4>Method</h4>
                <h6>To create a workspace:</h6>
                <ol>
                    <li>Arrange all open workbooks as desired.</li>
                    <li>From the View tab, choose Save Workspace.</li>
                    <li>In the Save Workspace dialog box, from the Save in drop-down list, select a folder in which to store the workspace.</li>
                    <li>In the File name drop-down combo box, type a name for the workspace.</li>
                    <li>Choose Save.</li>
                </ol>
                <h6>To open a workspace:</h6>
                <ol>
                    <li>On the File Button, click the Open button.</li>
                    <li>In the Open dialog box, select the file name of the workspace.</li>
                    <li>Choose Open.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will create and open a workspace.</h6>
                <ol>
                    <li>Make sure the Latham College and Advertising workbooks are both open and tiled side by side.</li>
                    <li>From the View tab, in the Window group, choose Save Workspace. [The Save Workspace dialog box appears].</li>
                    <li>In the Save in drop-down list box, make sure the Data folder is selected.</li>
                    <li>In the File name drop-down combo box, type Latham.</li>
                    <li>Choose Save. [The Latham workspace is created. A message box appears asking you if you want to save the Latham College workbook].</li>
                    <li>Choose OK.</li>
                    <li>Select the Advertising workbook window.</li>
                    <li>Maximize and then close the Advertising workbook window. [The Latham College workbook is active].</li>
                    <li>Close the Latham College workbook. [There are no open workbooks].</li>
                    <li>On the File Button menu, click the Open button. [The Open dialog box appears].</li>
                    <li>In the file list box, select Latham.</li>
                    <li>Choose Open. [The workspace opens, with the Latham College and Advertising workbooks tiled side by side].</li>
                </ol>

                <h4>Managing Multiple Worksheets and Workbooks</h4>
                <p>As you develop your workbooks, you may find that one worksheet isn't sufficient for the complexities of your data. In fact, it will probably prove necessary at times to move and copy entire worksheets, and then pasting them into multiple workbooks. Similarly, moving and copying data between worksheets and workbooks is equally valuable in creating and maintaining your workbooks. <a href="/excel-training-chicago.php">MS Excel classes offered in Chicago</a>.</p>

                <h4>Moving and Copying Worksheets</h4>
                <p>Sometimes, you'll want to change the order of the worksheets in a workbook. For instance, assume you created a sequence of worksheets named 1993, 1992, 2000, 2001, and 1999. At the time, it might have been convenient, but now it doesn’t make much sense. Luckily, Excel provides tools to make reorganizing the workbook easy. You can reorder worksheets by moving them. Making copies of worksheets is also useful for a number of reasons. For example, you might want to experiment with a table without affecting the original data. Using a duplicate worksheet makes this easy and safe. At another time, you might need to use the copy for creating multiple worksheets of similar structure. One way to move or copy worksheets is by dragging. When you drag the sheet tab, the mouse pointer with a small sheet of paper and a small, black triangle marks the location of the dragged worksheet. As an alternate method, you can use the Move or Copy dialog box, shown in Figure 1-6.</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel2-figure1-6.png" alt="Figure 1-6: The Move or Copy Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-6: The Move or Copy Dialog Box</p>

                <h4>Method</h4>
                <p>To move a worksheet:</p>
                <h6>Mouse method</h6>
                <ol>
                    <li>Drag the sheet tab of the worksheet to its new location, releasing the mouse button when the black triangle is in the desired location.</li>
                </ol>

                <h6>Shortcut menu method</h6>
                <ol>
                    <li>Right-click the tab of the worksheet you want to move.</li>
                    <li>From the sheet tab shortcut menu, choose Move or Copy.</li>
                    <li>In the Move or Copy dialog box, from the To book drop-down list, select the desired workbook.</li>
                    <li>In the Before sheet list box, select the worksheet before which the moved sheet will appear.</li>
                    <li>Choose OK.</li>
                </ol>

                <p>To copy a worksheet:</p>

                <h6>Mouse method</h6>
                <ol>
                    <li>Press and hold Ctrl.</li>
                    <li>Drag the sheet tab of the worksheet to its new location, releasing the mouse button when the black triangle is in the desired location.</li>
                    <li>Release Ctrl.</li>
                </ol>

                <h6>Shortcut menu method</h6>
                <ol>
                    <li>Right-click the tab of the worksheet you want to copy.</li>
                    <li>From the sheet tab shortcut menu, choose Move or Copy.</li>
                    <li>In the Move or Copy dialog box, from the To book drop-down list, select the desired workbook.</li>
                    <li>In the Before sheet list box, select the worksheet before which the copied sheet will appear.</li>
                    <li>Select the Create a copy check box.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will move and copy worksheets.</h6>
                <ol>
                    <li>Make sure the Latham workspace is open.</li>
                    <li>In the Advertising workbook window, select the Computers worksheet.</li>
                    <li>Drag the Computers sheet tab to the Latham College workbook window, releasing the mouse button when the small, black triangle is located after the Total Enrollment sheet tab. [As soon as you click the mouse, the mouse pointer changes to a mouse pointer with a small sheet of paper, and the small, black triangle appears. The Computers worksheet is moved from the Advertising workbook to the Latham College workbook].</li>
                    <li>Maximize the Advertising workbook window.</li>
                    <li>Right-click the Mass Mailing 2001 sheet tab. [The sheet tab shortcut menu appears].</li>
                    <li>Choose Move or Copy. [The Move or Copy dialog box appears].</li>
                    <li>From the To book drop-down list, select Latham College. [The Before sheet list box displays the worksheets in the Latham College workbook].</li>
                    <li>In the Before sheet list box, select Computers.</li>
                    <li>Choose OK. [The worksheet is moved. The Latham College workbook fills the screen].</li>
                    <li>Click the Restore button in the Latham College workbook. [The workbooks are tiled, and the Mass Mailing 2001 worksheet is visible in the Latham College workbook window].</li>
                    <li>In the Latham College workbook window, right-click the Mass Mailing 2001 sheet tab. [The sheet tab shortcut menu appears].</li>
                    <li>Choose Move or Copy. [The Move or Copy dialog box appears].</li>
                    <li>From the To book drop-down list, select Advertising. [The Before sheet list box displays the worksheets in the Advertising workbook].</li>
                    <li>In the Before sheet list box, select Mass Mailing 2002.</li>
                    <li>Select the Create a copy check box.</li>
                    <li>Choose OK. [A copy of the Mass Mailing 2001 worksheet appears in the Advertising workbook window].</li>
                    <li>In the Latham College workbook window, select the Average Costs worksheet.</li>
                    <li>Press and hold Ctrl.</li>
                    <li>Drag the Average Costs worksheet to the Advertising workbook window releasing the mouse button when the small, black triangle is located after the Summary worksheet. [As soon as you click the mouse, the mouse pointer changes to a mouse pointer with a sheet of paper and a plus sign, and the small, black triangle appears. The copy of the Average Costs worksheet appears in the Advertising workbook window].</li>
                    <li>Release Ctrl.</li>
                </ol>

                <h4>Selecting Multiple Worksheets</h4>
                <p>So far, you've selected only one worksheet at a time. This forces you to perform tasks such as inserting and deleting worksheets over and over. It's often faster to insert or delete several worksheets at one time. The same applies to moving and copying worksheets. That's why knowing how to select multiple worksheets is so important.</p>


                <h4>Method</h4>
                <h6>To select multiple worksheets:</h6>
                <ol>
                    <li>Select the first worksheet.</li>
                    <li>Press and hold Ctrl.</li>
                    <li>Select additional worksheets as desired.</li>
                </ol>

                <h6>To select all worksheets:</h6>
                <ol>
                    Right-click any sheet tab in the workbook.
                    From the sheet tab shortcut menu, choose Select All Sheets.
                </ol>

                <h6>To deselect all worksheets:</h6>
                <ol>
                    Right-click any sheet tab in the workbook.
                    From the sheet tab shortcut menu, choose Ungroup Sheets.
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will select and deselect multiple worksheets.</h6>
                <ol>
                    <li>Maximize the Latham College workbook window.</li>
                    <li>Make sure the Average Costs worksheet is selected.</li>
                    <li>Press and hold Ctrl.</li>
                    <li>Select the Print Marketing worksheet. [Two worksheets are selected].</li>
                    <li>Select the Tuition Generated worksheet. [Three worksheets are selected].</li>
                    <li>Release Ctrl.</li>
                    <li>Select the Total Enrollment worksheet, and then scroll to view the previously selected worksheets. [Only the Total Enrollment worksheet is selected].</li>
                    <li>Click the 4th tab scrolling button. [The three previously selected worksheets are no longer selected].</li>
                    <li>Right-click the Total Enrollment sheet tab. [The sheet tab shortcut menu appears].</li>
                    <li>Choose Select All Sheets. [All sheets are selected].</li>
                    <li>Right-click the Total Enrollment sheet tab. [The sheet tab shortcut menu appears].</li>
                    <li>Choose Ungroup Sheets. [Only the Total Enrollment worksheet is selected].</li>
                    <li>Save and close the Latham College workbook. [The Advertising workbook is active].</li>
                </ol>

            </div>
        </div>
    </main>

    <div class="mb-4 clearfix">&nbsp;</div>

    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/multiple-workbooks.php">Working with Multiple Workbooks</a></li>
                        <li><a href="/excel/lessons/worksheets.php">Working with Excel Worksheets</a></li>
                        <li><a href="/excel/lessons/multiple-views.php">Working with Multiple Views</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with Multiple Excel Workbooks cont. </a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas in Microsoft Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Excel Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Onsite Excel training Chicago and LA</h4>
                    <p>Do you have a group that requires Excel training? Why not give us a call today to  discuss your needs 888.815.0604. We can assess your team and provide customized Excel training to meet your exact needs. Read what our past students are saying: <a href="/testimonials.php?course_id=19">Excel training testimonials</a>. <br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>