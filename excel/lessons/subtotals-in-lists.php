<?php
$meta_title = "Using Subtotals in an Excel list | Training Connection";
$meta_description = "Learn to use Subtotals in lists in Microsoft Excel. This course is part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Subtotals in Lists</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Subtotals in an Excel List">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Using Subtotals in an Excel List</h1>
                        <h5>These  topics are covered  in Module 1 - Working with Lists, part of our <a href="/excel-training.php">Excel Advanced course</a>.</h5>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>When working with a list, you often need to know the bottom line figures or totals. A list of sales records may include the names of all salespersons, the different products they have sold, and the units of each product sold. To get a better idea of each person’s performance by product, you can use the Subtotals command to get a subtotal for each product sold by each salesperson.</p>

                <h4>Using Automatic Subtotals</h4>
                <p>Inserting automatic subtotals is a quick way to summarize data in a list. To use automatic subtotals, your data must be organized into a list and sorted by the column for which you want to display subtotals. For example, in a list of sales records, you sort by salesperson to obtain the subtotals for the number of product units sold by each salesperson. <a href="/excel-training-chicago.php">Excel classes run monthly in Chicago</a>.</p>
                <p>You determine how you want to subtotal your data using the Subtotal dialog box, shown in Figure 1-19. The options available in the Subtotal dialog box are summarized in Table 1-1.</p>

                <p>For hands-on Microsoft Excel training classes in Chicago and Los Angeles call us on 888.815.0604.</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-19.png" alt="Figure 1-19: The Subtotal Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-19: The Subtotal Dialog Box</p>


                <table class="table table-bordered table-dark table-hover table-l2 mb-1">
                    <thead>
                        <tr>
                            <td><strong>At Each Change In</strong></td>
                            <td><strong>Function</strong></td>
                        </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>At Each Change In</td>
                        <td>Identifies the field that contains the items by which you want to subtotal values in other columns.</td>
                    </tr>
                    <tr>
                        <td>Use Function</td>
                        <td>Identifies the summary function used to calculate subtotals.</td>
                    </tr>
                    <tr>
                        <td>Add Subtotal To</td>
                        <td>Identifies the fields you wish to subtotal.</td>
                    </tr>
                    <tr>
                        <td>Replace Current Subtotals</td>
                        <td>Displays new subtotal rows.</td>
                    </tr>
                    <tr>
                        <td>Page Break Between Groups</td>
                        <td>Inserts a page break between each grouping.</td>
                    </tr>
                    <tr>
                        <td>Summary Below Data</td>
                        <td>Adds subtotal rows below the groupings.</td>
                    </tr>
                    </tbody>
                </table>
                <p class="wp-caption-text">Table 1-1: Subtotal Dialog Box Options</p>

                <p>Excel calculates subtotal values by using a summary function, such as Sum or Average. The grand total values are derived from the detail data, which is the list data, and not from the subtotal rows. Grand total values are calculated by using the same summary function that you use to calculate subtotal values. Both grand total and subtotal labels are displayed in bold in the list and are automatically recalculated as you edit detail rows.</p>

                <h4>Method</h4>
                <h6>To display automatic subtotals:</h6>
                <ol>
                    <li>Sort the list by the column for which you want to display subtotals.</li>
                    <li>If necessary, select a cell in the list.</li>
                    <li>In the Outline group on the Data tab, click the Subtotal button.</li>
                    <li>In the Subtotal dialog box, from the At each change in drop‑down list, select a field.</li>
                    <li>From the Use function drop-down list, select a subtotal function.</li>
                    <li>In the Add subtotal to list box, select the desired subtotal field check box(es).</li>
                    <li>If necessary, select the Replace current subtotals and Summary below data check boxes, and deselect the Page break between groups check box.</li>
                    <li>Choose OK.</li>
                </ol>

                <h6>To remove automatic subtotals:</h6>
                <ol>
                    <li>In the Outline group on the Data tab, click the Subtotal button.</li>
                    <li>In the Subtotal dialog box, choose Remove All.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will use automatic subtotals in a list.</h6>
                <ol>
                    <li>Select the Ice Cream Sales worksheet.</li>
                    <li>Sort the list first by Salesperson and then by Product, both in ascending order.</li>
                    <li>In the Outline group on the Data tab, click the Subtotal button. [The Subtotal dialog box appears].</li>
                    <li>In the At each change in drop-down list box, make sure Salesperson is selected.</li>
                    <li>In the Use function drop-down list box, make sure Sum is selected.</li>
                    <li>In the Add Subtotal to list box, select the Units, Price/Unit, and Sales check boxes, deselecting other check boxes as necessary.</li>
                    <li>Make sure the Replace current subtotals and Summary below data check boxes are selected, and the Page break between groups check box is deselected.</li>
                    <li>Choose OK. [Automatic subtotals are inserted for each salesperson].</li>
                </ol>

                <h4>Working with Subtotal Outline Symbols</h4>
                <p>When you use automatic subtotals, various subtotal outline symbols are displayed to the left of the worksheet, as illustrated in Figure 1-20. You click the appropriate detail symbols to hide or show rows of detail data. Excel automatically outlines your list by grouping detail rows with each associated subtotal row and by grouping subtotal rows with the grand total row.</p>
                <p>You can also hide or show details for all subtotal groups at once by using the row level symbols. The level 1 symbol hides all levels of data except the grand total rows. The level 2 symbol hides all levels of data except the subtotal and grand total rows. The level 3 symbol shows all the data.</p>

                <p><img src="/dist/images/excel/excel3-figure1-20.png" alt="Figure 1-20: Subtotal Outline Symbols"></p>
                <p class="wp-caption-text">Figure 1-20: Subtotal Outline Symbols</p>

                <h4>Method</h4>
                <h6>To hide the detail rows of an automatic subtotaled list:</h6>
                <ol>
                    <li>Click the hide detail symbol beside the subtotal group.</li>
                    <li>To show the detail rows of an automatic subtotaled list:</li>
                    <li>Click the show detail symbol beside the subtotal group.</li>
                    <li>To show different levels of detail in an automatic subtotaled:</li>
                    <li>Click the appropriate level symbol.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will work with subtotal outline symbols.</h6>
                <ol>
                    <li>To the left of row 13, click the hide detail symbol for Cattapan Total. [The detail data for Cattapan is hidden. Only the subtotal row for Cattapan is visible].</li>
                    <li>To the left of row 22, click the hide detail symbol for DeMarcos Total. [The detail data for DeMarcos is hidden. Only the subtotal row for DeMarcos is visible].</li>
                    <li>Click the row level 2 symbol. [Only the subtotal rows in the list are displayed].</li>
                    <li>Click the row level 1 symbol. [Only the grand total row is displayed].</li>
                    <li>To the left of row 59, click the show detail symbol for Grand Total. [The subtotal rows reappear].</li>
                    <li>Click the row level 3 symbol. [All detail data, and the subtotal and grand total rows, are now visible].</li>
                </ol>

                <h4>Adding and Deleting Records Using the Data Form</h4>
                <p>If the existing subtotals do not show enough detail, you can insert a subtotal within a subtotal group, called a nested subtotal. For example, if your list shows the total number of products sold for each salesperson, you can add further detail to the list by adding subtotals for each individual product.</p>


                <h4>Method</h4>
                <h6>To nest subtotals:</h6>
                <ol>
                    <li>In the Outline group on the Data tab, click the Subtotal button.</li>
                    <li>In the Subtotal dialog box, from the At each change in drop-down list, select the field you want to use for the nested subtotal grouping.</li>
                    <li>From the Use function drop-down list and in the Add subtotal to list box, select any new options for the new subtotal.</li>
                    <li>Deselect the Replace current subtotals check box.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will nest subtotals.</h6>
                <ol>
                    <li>In the Outline group on the Data tab, click the Subtotal button. [The Subtotal dialog box appears].</li>
                    <li>From the At each change in drop-down list, select Product.</li>
                    <li>Deselect the Replace current subtotals check box. [The Summary below data option is dimmed].</li>
                    <li>Choose OK. [The Subtotal dialog box closes, and the new nested subtotals appear. A new row level symbol, 4, appears].</li>
                    <li>Click the row level 3 symbol. [The detail data is hidden].</li>
                    <li>Open the Subtotal dialog box.</li>
                    <li>Choose Remove All, and then save and close the workbook. [All subtotals and outline symbols are removed].</li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/creating-lists.php">Creating a List</a></li>
                        <li><a href="/excel/lessons/maintaining-lists.php">Maintaining a List</a></li>
                        <li><a href="/excel/lessons/editing-records.php">Editing Records Using the Data Form</a></li>
                        <li><a href="/excel/lessons/filtering-lists.php">Filtering a List</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Easy Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Easy Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel training testimonials</h4>
                    <p>We train thousands of students to use Excel every year. To read a sample of our reviews please click the following link <a href="/testimonials.php?course_id=19">Excel testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>