<?php
$meta_title = "Hiding and Unhiding Columns and Rows in Excel | Training Connection";
$meta_description = "This tutorial will teach you how protect your data by hiding rows and columns in Microsoft Excel.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Hide and Unhiding </li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Hiding and Unhiding Rows and Columns in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Hiding and Unhiding Rows and Columns in Excel</h1>
                        <p>When you develop workbooks for others to use, it may be wise to restrict the access they have to certain cells, worksheets, or even the entire workbook. Some cells may contain formulas that you do not want changed, and certain workbooks may be confidential. You can hide columns and rows within a worksheet, as well as hide worksheets within a workbook. You can also <a href="/excel/lessons/protect.php">password protect a workbook</a> to restrict access to it.</p>
                        <p>For more details on our <a href="/excel-training.php">hands-on Excel training</a> in Chicago and Los Angeles call us on 888.815.0604. All classes are taught by a live face-to-face trainer.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>You might want to display or print a worksheet in such a way that some of the data is hidden, but not deleted from the workbook. You can hide rows or columns to conceal confidential or confusing data. The path the to the Hide/Unhide rows and columns is via the Cells group on the Home tab, using the Format button.</p>

                <p><img src="/dist/images/excel/hide-and-unhide.png" alt="hide-and-unhide"></p>

                <p><a href="/excel-training-chicago.php">Chicago training classes in Excel</a>.</p>
                <h4>Method to hide or unhide columns and rows</h4>
                <h6>To hide a row:</h6>
                <ol>
                    <li>Select the row (or a cell in the row) you want to hide.</li>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>In the Format submenu, click Hide & Unhide.</li>
                    <li>Choose Hide Rows.</li>
                </ol>

                <h6>To hide a column:</h6>
                <ol>
                    <li>Select the column (or a cell in the column) you want to hide.</li>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>In the Format submenu, click Hide & Unhide.</li>
                    <li>Choose Hide Columns.</li>
                </ol>

                <h6>To unhide a row:</h6>
                <ol>
                    <li>Select the rows (or cells in the rows) above and below the hidden row.</li>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>In the Format submenu, click Hide & Unhide.</li>
                    <li>Choose Unhide Rows.</li>
                </ol>

                <h6>To unhide a column:</h6>
                <ol>
                    <li>Select the columns (or cells in the columns) to the left and right of the hidden column.</li>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>In the Format submenu, click Hide & Unhide.</li>
                    <li>Choose Unhide Columns.</li>
                </ol>

                <p>Also see <a href="/excel/lessons/data-validation.php">using Data validation in Excel</a>.</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of expert trainers we deliver onsite Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for an onsite Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>