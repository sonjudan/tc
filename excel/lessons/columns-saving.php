<?php
$meta_title = "Adjusting Columns widths and saving workbooks in Excel| Training Connection";
$meta_description = "Learn how adjust columns widths and save your files in Microsoft Excel This course is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Column Widths and Saving Files</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Essentials - Column Widths and Saving">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Excel Essentials - Column Widths and Saving</h1>
                        <h5>These  topics are covered  in Excel Essentials which forms part of our  <a href="/excel-training.php">Excel Level 1 Introduction class</a>.</h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Quickly Adjusting a Column Width</h4>
                <p>On occasion, you will discover that the text or values you have entered into a cell are not completely visible. This will occur when the number of characters entered exceeds the width of the column and when data appears in the cell to its right, as shown in column B of Figure 1-8.</p>

                <p>In the example, the entries in cells A1 and A2 appear just fine, because there are no entries to the right in cells B1 and B2. If the text or value appears cut off or overflows into a totally different column which will have its own data, you can quickly adjust the column width so you can view the entire cell entry.</p>

                <p><img src="/dist/images/excel/excel-figure1-12.png" alt=""></p>



                <h4>Method</h4>
                <h6>To quickly adjust a column width:</h6>

                <ol>
                    <li>Double-click the right border of the column heading.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will adjust a column width.</h6>

                <ol>
                    <li>Double-click the right border of the column heading for column A. [The column adjusts to accommodate the long text].</li>
                </ol>

                <h4>Finishing a Workbook</h4>
                <p>At the end of the day, or when you have completed your work on a particular workbook, you need to ensure that your work is safely stored. You can save your workbook in a number of ways, even as an Internet-compatible HTML document. In addition, you may want to make a printout of your worksheet to view the latest additions on paper or to share your worksheet with others. <a href="/excel-training-los-angeles.php">Instructor-led Excel classes in Los Angeles</a>.</p>

                <h4>Saving a New Workbook</h4>
                <p>When you save a workbook file, you tell Excel to accept every change you've made since the last time the workbook was saved. It's a good idea to save frequently, especially when you enter a lot of data or make major changes. That way, if there's a power outage or surge, you will lose only a few minutes of work.</p>

                <p>If you invoke the Save command in an unnamed workbook, Excel prompts you to name the workbook before it will save it to disk. From then on, Excel saves your workbook under that name unless you specify otherwise.</p>

                <p><img src="/dist/images/excel/excel-figure1-13.png" alt=""></p>

                <h4>Method</h4>
                <h6>To save a new workbook:</h6>

                <ol>
                    <li>Click the FILE button, choose Save or</li>
                    <li>Click the Save button. On the Quick Access Toolbar (looks like a floppy disk).</li>
                    <li>In the Save As dialog box, in the Save in drop-down list box, select the desired drive.</li>
                    <li>In the list of folders, double-click the desired folder.</li>
                    <li>In the File name drop-down list box, type the document name.</li>
                    <li>Choose Save.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will save your new workbook.</h6>

                <ol>
                    <li>Click the FILE button and choose Save. [Save As appears].</li>
                    <li>Under Save choose computer (Figure 1.14). [The available directories on your computer appear].</li>
                    <li>In the list of folders, double-click the Data folder. Should the correct directory not appear click the Browse button to locate it. [The contents of the Data folder appear].</li>
                    <li>In the File name drop-down list box, type Brian's Orchard 2009 Report.</li>
                    <li>Choose Save. [The worksheet is saved as Brian's Orchard 2009 Report and the new name appears in the title bar].</li>
                </ol>

                <p><img src="/dist/images/excel/excel-figure1-14.png" alt=""></p>

                <h4>Saving a Named Workbook</h4>
                <p>Once you have named your workbook, you can save subsequent changes by simply using the Save command. Excel automatically saves it under its current file name. To give you more flexibility, you can also save a named workbook under a different name using the Save As command. One benefit of doing this is that it gives you two copies of the same workbook; you can modify the copy all you want without affecting the "original," allowing you greater freedom for experimentation with formatting and other features.</p>


                <h4>Method</h4>
                <h6>To save a named workbook with its current name:</h6>
                <ol>
                    <li>Click the FILE button and choose Save or</li>
                    <li>On the Quick Access Toolbar, click the Save button.</li>
                </ol>

                <h6>To save an already named workbook with a different name:</h6>
                <ol>
                    <li>Click the FILE button and choose Save As.</li>
                    <li>In the Save in drop-down list box, select the drive on which to save the workbook.</li>
                    <li>In the list of folders, double-click the folder in which to save the workbook.</li>
                    <li>In the File name drop-down list box, type the new name.</li>
                    <li>Choose Save.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you’ll make changes to your workbook and save the file under another name.</h6>
                <ol>
                    <li>In cell D1, enter Draft Only.</li>
                    <li>Under Save choose computer (Figure 1.14). [The available directories on your computer appear].</li>
                    <li>In the list of folders, double-click the Data folder. Should the correct directory not appear click the Browse button to locate it. [The contents of the Data folder appear].</li>
                    <li>In the File name drop-down list box, type Brian's Orchard Draft.</li>
                    <li>Choose Save. [The file is saved and the dialog box closes. The title bar displays the new file name].</li>
                </ol>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Microsoft Excel</a><br></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Excel Workbooks</a><br></li>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Interface</a><br></li>
                        <li><a href="/excel/lessons/excel-selections.php">Selections in Excel</a><br></li>
                        <li><a href="/excel/lessons/columns-saving.php">Saving Files</a><br></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Closing Excel </a><br></li>
                        <li><a href="/excel/lessons/building-formulas.php">Using Formulas in Excel</a><br></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a><br></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel training courses</h4>
                    <p>Do you know that most intructor-led Excel classes are actually webinars? At Training Connection we believe that learning from a face-to-face instructor is still the best way to learn Microsoft Excel. Please read our <a href="/testimonials.php?course_id=19">past student reviews</a> we offer.<br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a><br></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a><br></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a><br></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>