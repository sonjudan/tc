<?php
$meta_title = "Understanding Excel Workbooks and Worksheets | Training Connection";
$meta_description = "Learn how to start Microsoft Excel and what Worksheets and Workbooks are. This course is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Excel Interface</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Essentials - Starting Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Excel Essentials - Starting Excel</h1>
                        <h5>These  topics are covered  in Module 1 - Excel Essentials, part of our Introductory Excel Level 1 class.</h5>
                    </div>

                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Starting Excel</h4>

                <p>To work with Excel effectively, you need to know several basic skills and concepts; how to start Excel from the Windows desktop, how to make the most of the Excel interface, how worksheets are used, and how to select cells and ranges.</p>
                <p>Please call us on <a href="tel:8888150604">888.815.0604</a> to <a href="/excel-training.php">register for an Excel class</a> in Chicago or Los Angeles.</p>

                <p>Once you are in Windows, starting Excel is a matter of a few clicks.</p>

                <h4>Method</h4>

                <h6>To load Excel:</h6>

                <ol>
                    <li>On the taskbar, click the Start button.</li>
                    <li>From the Start Menu, point to Programs.</li>
                    <li>From the Programs menu, choose Microsoft Excel.</li>
                </ol>

                <h4>Starting Excel from Windows 8 Only.</h4>
                <p>In Windows 8 you have no Start Menu, therefore you are unable to start Excel using the above method. The Start Menu is replaced but a Start Screen that contains a number of "Tiles".</p>

                <p><img src="/dist/images/excel/excel-starting.jpg" alt=""></p>

                <h4>Method</h4>

                <h6>To load Excel:</h6>

                <ol>
                    <li>On the taskbar, click the Start button.</li>
                    <li>From the Start Menu, point to Programs.</li>
                    <li>From the Programs menu, choose Microsoft Excel.</li>
                </ol>

                <h4>What is an Excel Worksheet?</h4>

                <p>A worksheet is the electronic equivalent of a paper ledger. It is a powerful platform used to enter, analyze, calculate, and manipulate data. A worksheet can be used for basic calculations such as addition and subtraction, as well as more complicated applications like statistics, audits, or mortgage tables. Moreover, worksheets allow you to quickly format your data into effective business reports.</p>

                <p>For more support, or for <a href="/excel-training-los-angeles.php">professional Excel Training Courses</a> in LA and Chicago please contact us.</p>

                <p>A worksheet is a grid of 1, 048, 576 rows and 16, 384 columns. The rows are labeled with numbers (1, 2, 3, ...) and the columns are labeled with letters (A, B, C, ..., AA, AB, AC,...), as illustrated in Figure1-1. The intersection of a row and column is a cell, the basic unit for storing data. The highlighted column and row headings indicate the active cell. You'll find the active cell's cell reference in the Name box. The cell reference consists of the highlighted column letter and row number, in this case D9. A range is a rectangular group of cells; the range reference is derived from the cell references of top left cell and the bottom right cell separated by a colon.</p>

                <p>The illustration below is how the section of the worksheet would be seen in Microsoft Excel version 2013.</p>

                <p><img src="/dist/images/excel/excel-figure1-1.jpg" alt=""></p>
                <p><img src="/dist/images/excel/excel-figure1-2.png" alt=""></p>

                <p>Above - the section of worksheet as actually seen in Microsoft Excel version 2013. Note that the selected areas are shaded a soft gray. This is so that the user can clearly see the contents of selected cells and ranges and even formatting. See example below for an enhanced view of selected formatted cells.</p>

                <p><img src="/dist/images/excel/excel-figure1-3.png" alt=""></p>

                <h4>What is an Excel Workbook?</h4>

                <p>A workbook is a collection of worksheets stored in the same file. These worksheets may contain different types of information, but they are usually related in some fashion. For example, each worksheet of a sales workbook may contain sales data for a specific division.</p>

                <p>Workbooks can contain an unlimited number of worksheets, depending on worksheet size and the amount of memory your computer has. Besides worksheets, a workbook can include chart sheets, Visual Basic modules, dialog box sheets, macro modules, and scenario report sheets.</p>

                <h4>Why learn Microsoft Excel?</h4>

                <p>Microsoft Excel is one of the most commonly used software programs in the World. Most office administration, clerical, analytical, accounting and finance jobs require a good understanding of Excel.</p>

                <p>Our classes are taught by live face-to-face trainers which means there is an actual trainer in the room. Each student will work on their own workstation and the learning experience will be totally hands-on.</p>

                <p>Included with all our workshops are training materials, certificate of course completion and a free repeat - if you decide you a need a refresher after a couple of months.</p>

                <p><a href="/testimonials.php?course_id=19">Read our  Excel class testimonials.</a></p>

                <p>There are 4 different levels of  Excel training. For more information about what is covered in each level please click on the links below.</p>
                <p>We hope to see you on an Excel class soon!</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Interface</a></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Excel Workbooks</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Excel Data Entry</a></li>
                        <li><a href="/excel/lessons/excel-selections.php">Selections in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Saving files in Excel </a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting Excel</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Copying Excel Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Work with Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="#">Excel Level 1 - Introduction</a></li>
                        <li><a href="#">Excel Level 2 - Intermediate</a></li>
                        <li><a href="#">Excel Level 3 - Advanced</a></li>
                        <li><a href="#">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>

                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>