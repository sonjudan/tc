<?php
$meta_title = "Recording Macros in Excel | Training Connection";
$meta_description = "This tutorial will teach you how to record a Macro in Microsoft Excel. This is covered as part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Macros</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Recording Macros in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Recording Macros in Microsoft Excel</h1>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>If you perform certain opertations over and over in Excel, you can record a macro which stores all the steps, and when you run the macro it will automatically perform each step automatically save you a lot of manual repetitive work.</p>
                <p>If you need to add more steps to a macro, or change existing steps, you can edit a macro. You edit macros in the Visual Basic Editor, using the Visual Basic programming language. If you no longer need a macro, you can delete it. In addition, you can create macros that use relative references, similar to creating formulas that use relative references.</p>
                <p>For more details of Excel Macro training in Chicago and Los Angeles call us on 888.815.0604. Our classes are hands-on instructor-led. <a href="/excel-training.php">Learn Macros from a live certified trainer</a>.</p>

                <h4>Recording a Macro</h4>
                <p>The easiest way to create a macro is to record it. In the Record Macro dialog box, shown in Figure 2-1, you can give your macro a name and a description. Although Excel provides default names and descriptions for macros, entering your own names and descriptions lets you identify and keep track of your macros, especially if you have a large number of macros. Once you have finished recording, you stop the recording by clicking the Stop Recording button in the Code group on the Developer tab, shown in Figure 2-2.</p>

                <div class="row">
                    <div class="col-lg-6">
                        <p><img class="aligncenter" src="/dist/images/excel/macros-record-1.png" alt="Figure 2-1: The Record Macro Dialog Box"></p>
                        <p class="wp-caption-text">Figure 2-1: The Record Macro Dialog Box</p>
                    </div>
                    <div class="col-lg-6">
                        <p><img class="aligncenter" src="/dist/images/excel/macros-record-2.png" alt="Figure 2-1: The Stop Recording button - Illuminated"></p>
                        <p class="wp-caption-text">Figure 2-1: The Stop Recording button - Illuminated</p>
                    </div>
                </div>

                <p>NOTE: The Ribbon is a component of the Microsoft Office Fluent user interface. But the Developer tab may not be in view on a standard Excel 2010 installation. Also, Macro recording and other macro options may have been disabled by your system administrator, or not enabled in the first place. <a href="/excel-training-chicago.php">Excel VBA training in Chicago</a>.</p>

                <h6>If the Developer tab is not available, do the following to display it:</h6>
                <ol>
                    <li>Click the Microsoft File Button and then click Excel Options.</li>
                    <li>In the Popular category, under Top options for working with Excel, select the Show Developer tab in the Ribbon check box, and then click OK.</li>
                </ol>

                <p><img class="aligncenter"src="/dist/images/excel/macros-record-3.png" alt="Figure 2-3: Setting the developer Tab on"></p>
                <p class="wp-caption-text">Figure 2-3: Setting the developer Tab on</p>

                <p><img class="aligncenter"src="/dist/images/excel/macros-record-4.png" alt="Figure 2-4: Developer Tab"></p>
                <p class="wp-caption-text">Figure 2-4: Developer Tab</p>

                <h6>To set the security level temporarily to enable all macros, do the following:</h6>
                <ol>
                    <li>On the Developer tab, in the Code group, click Macro Security.</li>
                    <li>Under Macro Settings, click Enable all macros (not recommended, potentially dangerous code can run), and then click OK.</li>
                </ol>
                <p>NOTE: To help prevent potentially dangerous code from running, we recommend that you return to any one of the settings that disable all macros after you finish working with macros.</p>
                <p>When you record a macro, Excel saves all your keystrokes and menu choices in the exact order in which you performed them, with the exception of certain mouse movements. For example, if you access the Format Cells dialog box by clicking the Format button in the Cells group (Home tab) and selecting Format Cells in the drop-down list, then make certain desired formatting selections, Excel only records the selections, not the path you took to the dialog box.</p>

                <h4>Steps to Record a Macro</h4>
                <h6>To record a macro:</h6>
                <ol>
                    <li>In the Code group on the Developer tab, click the Record Macro button.</li>
                    <li>In the Record Macro dialog box, in the Macro name text box, enter a name.</li>
                    <li>If desired, in the Description text box, enter a description.</li>
                    <li>Choose OK.</li>
                    <li>Choose commands or enter data you want to store in the macro.</li>
                    <li>Click the Stop Recording button in the Code group to stop the recording.</li>
                </ol>

                <p>Next - <a href="/excel/lessons/running-macros.php">see how to run a Macro</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>