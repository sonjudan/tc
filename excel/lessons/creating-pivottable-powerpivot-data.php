<?php
$meta_title = "Creating a PivotTable with PowerPivot Data in Microsoft Excel | Training Connection";
$meta_description = "Creating a PivotTable with PowerPivot Data in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating a PivotTable with PowerPivot Data</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Creating a PivotTable with PowerPivot Data">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Creating a PivotTable with PowerPivot Data</h1>
                        <h5>In this article, we will delve a little deeper into how to create a PivotTable with PowerPivot Data in Microsoft Excel.</h5>
                        <p>For instructor delivered <a href="/excel-training.php">MS Excel training wprkshops in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>If you see a security warning, click Enable Content. Ensure that the PowerPivot tab is displayed on the ribbon. If you do not see this tab, please complete the steps in the “Enabling PowerPivot” topic first.</p>
                <p>Open the PowerPivot window by clicking PowerPivot → Manage:</p>
                <p><img src="/dist/images/excel/open-powerpivot.jpg" alt="Open PowerPivot Window"></p>
                <p>After a relationship has been created between PowerPivot data, you can create a PivotTable. With the PowerPivot window open, click Home → PivotTable (not the drop-down arrow):</p>
                <p><img src="/dist/images/excel/open-pivottable.jpg" alt="Select PivotTable"></p>
                <p><a href="/excel-training-los-angeles.php">Los Angeles classes in Microosft Excel</a>.</p>
                <p>This action will open the Insert Pivot dialog. For this example, ensure that the New Worksheet radio button is selected and click OK:</p>
                <p><img src="/dist/images/excel/open-new-workbook.jpg" alt="New Worksheet Opened"></p>
                <p>The new PivotTable will now be created in Excel. Inside the PivotTable Fields pane, you will see two sets of fields: Sales and Sheet1. Click on each item to expand them:</p>
                <p><img src="/dist/images/excel/pivottable-in-excel.jpg" alt="PivotTable Field Pane"></p>
                <p>With both sets of fields now expanded, you can see all of the fields that are available. To add a field, simply click its checkbox. For this example, add Salesperson, Expense Account Balance, and Miles Traveled from the Sales field set. From the Sheet1 field set, add Sales and Profit. Now, you can see data from two different sources together in the same PivotTable:</p>
                <p><img src="/dist/images/excel/add-field.jpg" alt="Add Field to Data"></p>
                <p>You can work with this PivotTable as you would any other. If you need to access the data in this PivotTable, open the PowerPivot window by clicking PowerPivot → Manage:</p>
                <p><img src="/dist/images/excel/access-data-pivottable.jpg" alt="Access pivottable in worksheet"></p>
                <p>If the data in the external source changes, you can refresh the data in the PowerPivot window by clicking Home → Refresh:</p>
                <p><img src="/dist/images/excel/external-source-changes.jpg" alt="Refresh Powerpivot"></p>
                <p>Then, you would need to refresh the PivotTable in Excel by clicking PivotTable Tools – Analyze → Refresh.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our 1-day Excel workshop. We run classes for every skill level each month. Check out our student testimonials  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>