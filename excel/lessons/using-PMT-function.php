<?php
$meta_title = "Using the PMT Function in Microsoft Excel | Training Connection";
$meta_description = "Using the PMT Function in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using PMT Function</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using the PMT Function">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Using the PMT Function</h1>
                        <h5>In this article, we will take a look into how to use the PMT Function in Microsoft Excel.</h5>
                        <p>For <a href="/excel-training.php">instructor-led Microsoft Excel training in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>The PMT (payment) function is a financial function that is used to calculate loan payments based upon a constant interest rate. For example, if you take a loan of $10,000 at 6% annual interest over 4 years, you can use the PMT function to calculate what the monthly payment on the loan will be.</p>

                <p>To use this function, first select the cell in which you want the results to be displayed. For this example, click cell D2. Next, click Formulas → Financial → PMT:</p>

                <p><img src="/dist/images/excel/open-PMT-function.jpg" alt="Using PMT Function"></p>

                <p>This action will open the Function Arguments dialog for the PMT function:</p>

                <p><img src="/dist/images/excel/PMT-function-argument.jpg" alt="Open Function Argument PMT Dialog"></p>

                <p>The top portion of the dialog contains a number of arguments for this function. Arguments that appear bold are required in order for the function to operate. Arguments not in bold are optional:</p>

                <p><img src="/dist/images/excel/PMT-argument-requirement.jpg" alt="Required Data for PMT Argument"></p>

                <p>The Rate argument is the interest rate that will be used to calculate the payment. You have the option of inserting this number manually or selecting a cell in the current workbook that contains it.</p>

                <p><img src="/dist/images/excel/rate-argument.jpg" alt="FV Rate Argument"></p>

                <p>You need to account for the fact that the values in column A are annual interest rates, so you need to divide this data by 12. In the Rate argument field, type /12 after A2:</p>

                <p>To start, click inside the Rate field. Next, with the Function Arguments dialog still open, click cell A2:</p>

                <p><img src="/dist/images/excel/inside-rate-field.jpg" alt="Click With Rate Field"></p>

                <p>Now, type the divide operator (/) and then click cell B2:</p>

                <p><img src="/dist/images/excel/divide-operator.jpg" alt="Divide Operator Displayed"></p>

                <p>A2/B2 will now be displayed inside the Rate field. (This is used to calculate interest on a monthly basis.) The results of the information entered into this field will also be displayed:</p>

                <p><img src="/dist/images/excel/rate-field.jpg" alt="Result Displayed In Rate Field"></p>

                <p>The Nper argument is the number of payment periods required for the loan. For this example, the worksheet already displays payment periods by the number of months in column B, so type B2 into this field:</p>

                <p><img src="/dist/images/excel/PMT-Nper-arugment.jpg" alt="PMT Nper Argument Displayed"></p>

                <p>The Pv argument is the present value of the loan; in this case, the face value of the amount borrowed. Since loan amounts are listed in column C, type C2 into this field:</p>

                <p><img src="/dist/images/excel/Pv-argument.jpg" alt="Pv Argument as Loan Value"></p>

                <p>The remaining arguments listed in this dialog are optional.</p>

                <ul>
                    <li>The Fv argument is used to specify an amount that is left outstanding after the loan payments are made for all payment periods. If you leave this option out, it will default to 0, meaning that the loan will be paid in full at the end of the payments.</li>
                    <li>The Type argument will specify if the payment is made at the beginning or end of the payment period. If you enter an argument of 0, payments will be due at the end of the payment period. With an argument of 1, payments will be due at the beginning of the period. If you leave this argument out, it will default to 0.</li>
                </ul>
                <p><a href="/excel-training-chicago.php">Onsite Excel training in Chicago</a>.</p>

                <p>For this example, you can leave both of the optional arguments blank. The Function Arguments dialog will now look like the example below. As you can see, the formula result for this function is -860.66:</p>

                <p><img src="/dist/images/excel/optional-arguments.jpg" alt="Optional Arguments Remaining"></p>


                <p>Click OK:</p>


                <p><img src="/dist/images/excel/click-ok.jpg" alt="Select OK"></p>

                <p>The results of the function will now be inserted into the cell you had previously selected. In this case, you can see that a $10,000 loan over the period of 12 months at an interest rate of 6.00% will require a monthly payment of $860.66:</p>

                <p><img src="/dist/images/excel/result-inserted.jpg" alt="Result Now Shown"></p>

                <p>Using AutoFill, apply this function to cells D3-D5:</p>

                <p><img src="/dist/images/excel/autofill-to-apply-results.jpg" alt="Autofill Reamining Cells"></p>

                <p>Keep in mind that when entering functions, you do not need to use the Function Arguments dialog. You can type this function directly into the formula bar using “PMT” as a prefix:</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our Excel training workshop. We run classes for all skill levels, every month. Check out our extensive student testimonials  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>