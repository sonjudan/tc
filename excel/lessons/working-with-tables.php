<?php
$meta_title = "Working with Tables in Microsoft Excel | Training Connection";
$meta_description = "Learn how to work with tables in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working with Tables</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Working With Tables in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Working With Tables in Excel</h1>
                        <p>For classroom based, instructor-led <a href="/excel-training.php">MS Excel training and courses </a>in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h6>In this article, we will learn about:</h6>
                <ul>
                    <li>What tables are</li>
                    <li>Create tables</li>
                    <li>Resize tables</li>
                </ul>

                <h4>What is a Table?</h4>
                <p>To facilitate data analysis and management, you have the ability to turn any range of cells into a table. You can have multiple tables per worksheet, and tables can be as large or small as the amount of data you want to work with.</p>

                <p>Tables are made from adjacent columns of data, with a unique label or heading for each column. Each row in the table should have entries organized according to the column headings. This design is well suited for data organized in long, adjacent, list-like columns. In the example shown below, you can see a basic table:</p>

                <p><img src="/dist/images/excel/basic-table.jpg" alt="table displayed"></p>
                <p>By using the table features, you can manage data in table rows and columns independently from the rest of the data on the worksheet. For example, clicking on the column headers allows you to filter the data, as well as sort it using a variety of criteria. <a href="/excel-training-los-angeles.php">Public Los Angeles Excel classes</a>.</p>

                <h4>Creating Tables</h4>
                <p>To create a table using existing data, first select the entire range of data that you are going to be working with. For this example select cells A3:G33:</p>

                <p><img src="/dist/images/excel/existing-data-table.jpg" alt="select entire range of data"></p>
                <p>Next, click Insert → Table:</p>
                <p><img src="/dist/images/excel/insert-table.jpg" alt="click insert table"></p>

                <p>The Create Table dialog will be displayed. On this dialog you will see a text box that shows the cell ranges that you had previously selected, as well as a checkbox that indicates if your data includes headers. If you were creating a table from scratch, you would leave the header option deselected. Leave these settings unchanged and click OK:</p>
                <p><img src="/dist/images/excel/create-table-dialog.jpg" alt="create table displayed"></p>

                <p>Deselect the previously selected cell range and you will see that this cell range has been transformed into a table:</p>
                <p><img src="/dist/images/excel/deselect-cells.jpg" alt="select cell range"></p>

                <p>To create a table from scratch you would follow the same steps. Highlight the range of cells that you want to be included in the table, click Insert → Tables, and click OK on the Create Table dialog.</p>

                <h4>Resizing the Table</h4>
                <p>After you have inserted a table into your workbook you can still adjust its dimensions and cell range when needed.</p>

                <p>In the provided sample workbook, you will see that a row and column were not included in the existing table. To change this, first move your cursor to the bottom right-hand corner of the table where a small resize handle is present. When your cursor is in position it will change to a double-headed diagonal arrow:</p>
                <p><img src="/dist/images/excel/row-column-display.jpg" alt="resize area"></p>

                <p>Click and drag this handle in a downward direction until you see the green outline for the table include the missing row (33):</p>
                <p><img src="/dist/images/excel/click-drag-handle.jpg" alt="move handle downwards"></p>

                <p>Release your cursor and the row, including its data, will be included in the table:</p>
                <p><img src="/dist/images/excel/data-included.jpg" alt="data included in table"></p>

                <p>Repeat this process, but drag the resize handle towards the right to include the G column:</p>
                <p><img src="/dist/images/excel/resize-data-handle.jpg" alt="resize handle"></p>

                <p>Now column G and its existing data will be included in the table:</p>

                <p><img src="/dist/images/excel/existing-data-displayed.jpg" alt="data displayed in table"></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Microsoft Excel Training Reviews</h4>
                <p>Join our Microsoft Excel training workshop in Chicago and Los Angeles. Check out what our students have to say on the <a href="/testimonials.php?course_id=19">Microsoft Excel Reviews</a> page.<br>
                </p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>