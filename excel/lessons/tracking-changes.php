<?php
$meta_title = "Tracking Changes in Microsoft Excel | Training Connection";
$meta_description = "Learn how to track changes in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tracking Changes</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking changes in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Tracking changes in Excel</h1>
                        <p>For <a href="/excel-training.php">Microsoft Excel classes based in Chicago and Los Angeles </a>call us on 888.815.0604.</p>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h6>In this article, we will learn how to:</h6>
                <ol>
                    <li>Track changes</li>
                    <li>Review changes</li>
                </ol>

                <h4>Tracking Changes</h4>
                <p>When tracking is enabled, Excel will keep track of the changes that you or others who have access to your workbook have made. This includes data insertions, deletions, and more. While this feature is available in a few Microsoft Office products, Microsoft Excel is somewhat unique in that change tracking is available only in shared workbooks. While typically a shared workbook would be stored in a location where others who need to can access it, you can also track changes using a copy you only have access to.</p>

                <p>To enable tracking on a document, click Review → Track Changes → Highlight Changes:</p>

                <p><img src="/dist/images/excel/enable-tracking.jpg" alt="allow tracking"></p>
                <p>The Highlight Changes dialog will be shown. In order for you to be able to track your changes, you need to share your workbook. Click to select the “Track changes while editing…” checkbox:</p>

                <p><img src="/dist/images/excel/highlight-changes-dialog-shown.jpg" alt="share workbook to track changes"></p>
                <p>The remaining controls on this dialog will become enabled; using them you can choose exactly how changes will be tracked and highlighted. For example, you can choose to only track changes made by specific users, or even changes that have been made to a specific cell range. Leave the default settings unchanged and click OK:</p>

                <p><img src="/dist/images/excel/remaining-controls-dialog.jpg" alt="changes tracked"></p>
                <p>As this action is also sharing your workbook, you will be asked to resave it. Click OK:</p>

                <p><img src="/dist/images/excel/resave-workbook.jpg" alt="resave to share"></p>
                <p>Your workbook is now shared and tracking your changes. <a href="/excel-training-chicago.php">Need Excel training in Chicago</a>?</p>

                <p>Delete the contents of cell A1. This action will delete the title text that was inside of this cell. With the cell now blank, you will see that its upper left-hand corner is now shaded black:</p>

                <p><img src="/dist/images/excel/delete-cell-content.jpg" alt="delete title in cell"></p>
                <p>This shading indicates that this cell has been changed since tracking changes has been enabled. Move your mouse pointer over this corner and a ScreenTip will display what user made changes to this cell, when, and what they did:</p>


                <p>Move your cursor off of this corner and the ScreenTip will be hidden.</p>

                <h4>Reviewing Changes</h4>
                <p>After changes have been tracked on your workbook, you can then review these changes and accept or reject them as needed. To do this, first click Review → Track Changes → Accept/Reject Changes:</p>

                <p><img src="/dist/images/excel/accept-reject-changes.jpg" alt="review changes to accept or reject"></p>
                <p>The Select Changes to Accept or Reject dialog will now be shown. This dialog includes several controls that mirror the options found in the Highlight Changes dialog. Ensure that “Not yet reviewed” is selected from the When drop-down menu and then click OK:</p>

                <p><img src="/dist/images/excel/accept-reject-confirmation.jpg" alt="confirm changes"></p>
                <p>The Accept or Reject Changes dialog will now be shown. On this dialog will be displayed the details of the first change that was found on your worksheet. In this case you can see that the first change found was that A1 was changed to blank. This probably was not a good idea, so click Reject:</p>

                <p><img src="/dist/images/excel/accept-reject-dialog-shown.jpg" alt="click reject"></p>
                <p>Upon clicking Reject, the aforementioned change will be reversed and the next consecutive change found in your current worksheet will be displayed. In this case you want to accept this change so click Accept:</p>

                <p><img src="/dist/images/excel/change-reversed.jpg" alt="change agreements"></p>
                <p>The change that was made to the aforementioned cell will be applied to your worksheet. As you have reached the end of the changes found, the Accept or Reject Changes dialog will have closed automatically.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget ">
                <h4 class="widget-title">MS Excel Testimonials</h4>
                <p>Each year Training Connection help thousands of students gain a better understanding of Microsoft Excel. Find out what our students have to say on the <a href="/testimonials.php?course_id=19">Microsoft Excel Student Testimonials</a> page.</p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>