<?php
$meta_title = "Working with Slicers in Microsoft Excel | Training Connection";
$meta_description = "Learn how to work with Slicers in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Working with Slicers</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Working With Slicers in Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Working With Slicers in Excel</h1>
                        <p>For dedicated, instructor driven <a href="/excel-training.php">Microsoft Excel courses in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h6>In this article, we will learn how to:</h6>
                <ol>
                    <li>Insert and use a slicer</li>
                    <li>Rename a slicer</li>
                    <li>Change slicer settings</li>
                </ol>


                <p>Inserting and Using a Slicer</p>
                <p>Slicers allow you to quickly filter any data that is displayed within a PivotTable. To insert a slicer, first click anywhere inside the PivotTable that you are working with. Next, click PivotTable Tools – Analyze → Insert Slicer:</p>
                <p><img src="/dist/images/excel/insert-slicer.jpg" alt="insert slicer function"></p>

                <p>The Insert Slicers dialog will now be open. You may use the controls in this dialog to choose what criteria you would like to sort the data by. For this example ensure that a checkmark appears beside Package. Click OK:</p>
                <p><img src="/dist/images/excel/insert-slicer-dialog.jpg" alt="slicer dialog displayed"></p>

                <p>Now the slicer pop-up will be displayed:</p>
                <p><img src="/dist/images/excel/slicer-pop-up.jpg" alt="slicer pop up displayed"></p>

                <p>Move this slicer so that is doesn’t obstruct the PivotTable by clicking and dragging it by its header to a new and unobtrusive location:</p>
                <p><img src="/dist/images/excel/move-slicer.jpg" alt="slicer in unobtrusive location"></p>

                <p>Next, you need to use the slicer to sort the data in the PivotTable by criteria from the selected category. For this example, click LUX:</p>
                <p><img src="/dist/images/excel/slicer-sort-data.jpg" alt="pivot table sort"></p>

                <p>Now only vehicles that were sold with the LUX package will be accounted for in the PivotTable:</p>
                <p><img src="/dist/images/excel/pivot-table-lux.jpg" alt="pivot table lux displayed"></p>

                <h4>Renaming the Slicer</h4>

                <p>By default, any slicer that you insert into your worksheet will be named after the field that you chose for it to work with. To change this name, first click on the slicer to select it:</p>
                <p><img src="/dist/images/excel/change-slicer-name.jpg" alt="click slicer to amend name"></p>
                <p><a href="/excel-training-los-angeles.php">Private Excel group training</a>.</p>

                <p>By selecting it, you will make the Slicer Tools – Options tab available on the ribbon. Click to open this tab and you will see that the Slicer group contains a text box:</p>
                <p><img src="/dist/images/excel/slicer-tool-option.jpg" alt="slicer options"></p>

                <p>Replace the text in this text box with the new name you want for the slicer. For this example, type “Module 2:”</p>
                <p><img src="/dist/images/excel/replace-text-1.jpg" alt="rename text"></p>

                <p>The new name will immediately be applied to the currently selected slicer:</p>
                <p><img src="/dist/images/excel/new-name-applied.jpg" alt="new name displayed"></p>

                <h4>Changing Slicer Settings</h4>

                <p>To access a variety of different settings you can use to customize your slicer, right-click on the slicer and then click Slicer Settings:</p>
                <p><img src="/dist/images/excel/access-different-settings.jpg" alt="access settings"></p>

                <p>The Slicer Settings dialog will now be shown:</p>
                <p><img src="/dist/images/excel/access-slicer-settings.jpg" alt="slicer settings shown"></p>

                <p>Using the controls in this dialog you can rename the slicer, change its caption, change sortation options, as well as choose how to handle items with no data. For this example, hide the header for this slicer by deselecting the “Display header” check box:</p>
                <p><img src="/dist/images/excel/controls-in-dialog.jpg" alt="sortable options"></p>

                <p>Click OK to apply your changes:</p>
                <p><img src="/dist/images/excel/apply-settings.jpg" alt="apply changes"></p>

                <p>The header for the selected slicer will now be hidden:</p>

                <p><img src="/dist/images/excel/header-hidden.jpg" alt="header change"></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Microsoft Excel available in Chicago and LA</h4>
                <p>Join our Microsoft Excel training workshop. We run MS classes for all skill levels. Check out our extensive <a href="/testimonials.php?course_id=19">Microsoft Excel student testimonies</a> page.</p>
            </div>

        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>