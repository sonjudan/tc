<?php
$meta_title = "Array Formulas in Excel | Training Connection";
$meta_description = "In this tutorial you will learn how to use Array Formulas in Microsoft Excel. Training Connection offers Excel Training Classes in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Array Formulas</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Array Formulas in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Array Formulas in Microsoft Excel</h1>
                        <p>An array can be defined as any grouping of two or more adjacent cells. These cells form a square or rectangle. When a selection is defined as an array, operations called array formulas can be performed on every cell in the selection rather than on just a single cell. This is a powerful technique that can be used to enhance Excel formulas and functions.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">



                <h4>Using Array Formulas</h4>
                <h6>Consider the sample worksheet below:</h6>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-1.png" alt="array-formula-1"></p>

                <p>If you wanted to calculate the total value of everything in the inventory, you could calculate the value of each quantity and then add all the values together; however, a simpler solution to this problem is to use an array formula.</p>
                <p>Click to select cell C8 as this is where the formula will be placed:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-2.png" alt="array-formula-2"></p>

                <p>This example requires that you multiply one column of values by the other and add all of these results together. With this in mind, type “=SUM(B2:B6*C2:C6)” into the formula bar:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-3.png" alt="array-formula-3"></p>
                <p>While each array is treated as a single argument within the formula, the formula in its current form will result in a #VALUE error. This is because Excel does not recognize the arrays as arguments of the SUM formula. <a href="/excel-training-chicago.php">MS Excel classes in Chicago</a>.</p>
                <p>Correct this by clicking inside the formula bar and pressing Ctrl + Shift + Enter. This action will add curly braces ({}) to both ends of the formula:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-4.png" alt="array-formula-4"></p>

                <p>These curly braces denote that this formula is an array formula and Excel will treat it as such. You can now see the value of all the stock as determined by the array formula that you just configured:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-5.png" alt="array-formula-5"></p>

                <p>Now, suppose that you need to calculate the individual final unit price for each product so that it includes a 5% sales tax. Select cells D2 - D6:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-6.png" alt="array-formula-6"></p>

                <p>In the formula bar, type “=C2:C6*1.05” and then press Ctrl + Shift + Enter:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/array-formula-7.png" alt="array-formula-7"></p>
                <p>As you can see, each value in the first array (C2:C6) has been calculated according to the formula and the result is entered into the corresponding matching place in the second array (D2:D6).</p>

                <p>Keep in mind that if you want to edit an array formula, you can change the operators or values in the same manner as any other formula, but you still must press Ctrl + Shift + Enter when you are done to reapply the array formula status.</p>

                <p>Save the changes that you have made to the current workbook and then close Microsoft Excel 2013.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Excel throughout the country. View our <a href="/testimonials.php?course_id=19">Excel  class testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite Excel training</a>.</p>
                </div>
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>