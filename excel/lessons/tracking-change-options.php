<?php
$meta_title = "Tracking Change Options in Microsoft Excel | Training Connection";
$meta_description = "Learn how to track change options in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tracking Change Options</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Tracking change options in Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Tracking change options in Excel</h1>
                        <h5>In this article we will look at the different options for Tracking Changes and how to stop tracking changes.</h5>
                        <p>For classes dedicated to, <a href="/excel-training.php">Microsoft Excel training </a> available in both Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Setting Options for Tracking Changes</h4>

                <p>As tracking changes has been integrated with the sharing capabilities of Excel 2013, you can find its customization options by first clicking Review → Share Workbook:</p>
                <p><img src="/dist/images/excel/tracking-changes.jpg" alt="integrated tracking changes"></p>

                <p>This action will open the Share Workbook dialog. Click to open the Advanced tab:</p>
                <p><img src="/dist/images/excel/share-workbook-dialog.jpg" alt="click advanced tab"></p>

                <p>The controls for the Advanced tab will now be displayed:</p>
                <p><img src="/dist/images/excel/advanced-tab-displayed.jpg" alt="advanced tab now shown"></p>

                <p>At the top of this dialog, you can use the controls provided to choose how long to keep the change history for this workbook. By default this is set to 30 days, but you can choose as many or few as you need; you can even choose not to keep change history at all:</p>
                <p><img src="/dist/images/excel/change-workbook-history.jpg" alt="keep change history"></p>

                <p>The “Update changes” section allows you to choose when your changes are updated. Typically this is done whenever the workbook is saved, but you can also have this done automatically every set number of minutes:</p>
                <p><img src="/dist/images/excel/update-changes.jpg" alt="choose when changes are updated"></p>

                <p>The next section allows you to choose what happens when conflicting changes between users occurs. By default Excel will ask you which changes should win, but you can also have it so that the most recent saved changes will win. <a href="/excel-training-los-angeles.php">Small hands-on Excel 2019 classes</a>.</p>
                <p><img src="/dist/images/excel/conflicting-changes.jpg" alt="manage conflicting changes"></p>

                <p>Typically, the default settings are fine for most circumstances, so click Cancel to return to the primary Excel 2013 window. Close Microsoft Excel 2013 without saving any changes that you may have made to the current worksheet.</p>

                <h4>Stopping Tracking Changes</h4>

                <p>To stop tracking changes that are made to your workbook, you need to stop sharing it. Click Review → Track Changes → Highlight Changes:</p>
                <p><img src="/dist/images/excel/stop-tracking-changes.jpg" alt="highlight changes"></p>

                <p>The Highlight Changes dialog will now be displayed. Click the “Track changes while editing” check box to deselect it:</p>
                <p><img src="/dist/images/excel/highlight-changes-1.jpg" alt="highlight changes dialog"></p>

                <p>With this option now deselected, click OK to apply the new changes:</p>
                <p><img src="/dist/images/excel/option-now-deselected.jpg" alt="click ok to apply"></p>
                <p>As stopping track changes will also remove this workbook from shared use, a warning dialog will be displayed. To continue, click Yes:<h3>Image here</h3></p>

                <p><img src="/dist/images/excel/stopping-track-changes-confirmation.jpg" alt="confirm track changes"></p>

                <p>The current workbook will no longer track changes, nor will it be available for shared use.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">Microsoft Excel Student Testimonials</h4>
                <p>Find out what our students have to say about our MS Excel training courses and classes right here, on the <a href="/testimonials.php?course_id=19">MS Excel Testimonials</a> page.</p>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>