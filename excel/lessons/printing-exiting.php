<?php
$meta_title = "Printing Worksheets and Exiting Excel | Training Connection";
$meta_description = "Learn how to do a basic print and then exit Microsoft Excel This module forms part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Printing and Exiting</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Essentials - Printing and Exiting">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Excel Essentials - Printing and Exiting</h1>
                        <h5>Learning to Print Excel worksheets and exiting from Excel is covered in our <a href="/excel-training.php">beginner  Excel spreadsheeting course.</a></h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Printing a Worksheet</h4>
                <p>You may want to print your worksheet when you’ve finished working with it. This will give you a hard copy of your data to look over and to share with others. Best of all, you can get a quick printout of the active worksheet very easily.</p>

                <p class="alignright"><img src="/dist/images/excel/excel-figure1-15.png" alt=""></p>

                <h4>Method</h4>
                <h6>To print a worksheet:</h6>
                <ol>
                    <li>Click the FILE button and choose the Print button. <a href="/excel-training-los-angeles.php">LA-based Excel and VBA classes</a>.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will print a worksheet.</h6>
                <ol>
                    <li>Click the FILE button and choose the Print button. [The PRINT dialog box appears].</li>
                    <li>Select the printing options you require. [Click the PRINT button. The worksheet is printed.]</li>
                </ol>



                <h4>Closing a Workbook and Exiting from Excel</h4>
                <p>Closing a workbook removes it from your screen but leaves you in Excel so that you can keep working. Before you close a workbook, you should make sure that you’ve saved your work. If you’ve forgotten, however, Excel reminds you that you need to save the document and gives you a chance to do so. You can exit from Excel after you have saved and closed any open workbooks.</p>

                <h4>Method</h4>
                <h6>To close a workbook:</h6>
                <ol>
                    <li>Click the workbook Window Close button. <br>or</li>
                    <li>Click the FILE button and choose Close.</li>
                    <li>Save the file if prompted.</li>
                </ol>

                <h6>To exit from Excel:</h6>
                <ol>
                    <li>Click the application Close button. <br>or</li>
                    <li>Click the FILE button and click the Exit Excel button (bottom right).</li>
                    <li>Save the file if prompted.</li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will close the Orchard Report Draft workbook and exit Excel.</p>
                <ol>
                    <li>Click the workbook Window Close button. [Save As appears].</li>
                    <li>If a message appears asking you if you would like to save the changes to the workbook file, choose Yes. [The workbook closes].</li>
                    <li>Click the application Close button. [Excel closes].</li>
                </ol>

                <h4>Assignment</h4>
                <ol>
                    <li>Load Excel.</li>
                    <li>Select cell C3 with the keyboard.</li>
                    <li>In cell C3, enter Cathy’s Computers.</li>
                    <li>In cell C4, enter Sales Report.</li>
                    <li>Using the keyboard, select the range B6:E6.</li>
                    <li>Using Figure 1-11 as a guide, enter the years in the range B6:E6.</li>
                    <li>Select the entire worksheet.</li>
                    <li>Use the mouse to select the range A7:A10.</li>
                    <li>Using Figure 1-11 as a guide, enter the product names into the range A7:A10.</li>
                    <li>Enter zeros in the range B7 through B10.</li>
                    <li>Using a range, enter the data for 2007, 2008, 2009 and 2013.</li>
                    <li>Adjust the width of column A so you can read the text clearly.</li>
                    <li>Select the nonadjacent cell ranges B6:E6 and A7:A10.</li>
                    <li>Save the worksheet in the Data folder on the C: drive as Cathy’s Computers.</li>
                    <li>Save cells A1:E10 in the Data folder as an HTML document (with no title or header) with the name Cathy.htm.</li>
                    <li>In cell A11, enter Yearly Total.</li>
                    <li>Use Save As to save the workbook again. Use the file name Cathy’s Computers Draft.</li>
                    <li>Print the worksheet.</li>
                    <li>Close the workbook, and then exit Excel.</li>
                </ol>

                <p><img src="/dist/images/excel/excel-figure1-16.png" alt=""></p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Excel</a><br></li>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Interface</a><br></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Microsoft Excel</a><br></li>
                        <li><a href="/excel/lessons/excel-selections.php">Excel Selections</a><br></li>
                        <li><a href="/excel/lessons/columns-saving.php">Excel Column Widths</a><br></li>
                        <li><a href="/excel/lessons/building-formulas.php">Using Formulas in Excel</a><br></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Copying and Editing Formulas</a><br></li>
                        <li><a href="/excel/lessons/basic-functions.php">SUM and Average Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">The best way to learn Microsoft Excel?</h4>
                    <p>We hope you found this online tutorial useful?</p>
                    <p><p>
                    <p>The best and  most effective way to learn Excel is however to  join a face-to-face instructor-led class. We run <a href="/excel-training.php">Excel classes in Chicago and Los Angeles</a> throughout the year or we can send an instructor to deliver onsite Excel training at your offices. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a><br></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a><br></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a><br></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>