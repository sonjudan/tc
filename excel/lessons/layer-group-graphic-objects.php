<?php
$meta_title = "Layer and Group Graphic Objects in Microsoft Excel | Training Connection";
$meta_description = "Layer and group graphic objects in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Layer and Group Graphic Objects</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Layer and Group Graphic Objects">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Layer and Group Graphic Objects</h1>
                        <h5>In this article, will delve a little deeper into how to layer and group graphic objects together in Excel.</h5>
                        <p>For instructor-led <a class="excel" href="/excel-training.php">Microsoft Excel training in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Once you have added graphical objects to a worksheet, it is important to know how to organize their positioning in relation to one another. Using layers, you can choose which object overlaps another, while grouping allows you to group multiple graphical object together so that you can adjust their properties all at the same time. Over the course of this topic, you will learn all about layering and grouping graphical objects in Microsoft Excel 2016. <a href="/excel-training-chicago.php">Chicago Excel 365 Training</a>.</p>

                <h4>Topic Objectives</h4>

                <h6>In this topic, you will learn:</h6>
                <ol>
                    <li>About layering objects</li>
                    <li>About grouping objects</li>
                    <li>About positioning objects</li>
                </ol>

                <h4>Layering Objects</h4>
                <p>When a graphical object is added to a worksheet, it is added to its own layer so that it can overlap any existing objects. You can manipulate a layer and how it interacts with others by moving it forward or backward in the stack of layers – just like moving the top-most card in a deck below the next and vice-versa. To do this, first click to select the object that you would like to work with and then click Picture Tools – Format (or Drawing Tools – Format) → Bring Forward or Bring Backward:</p>

                <p><img src="/dist/images/excel/picture-tool-format.jpg" alt=""></p>

                <p>If you would instead like to move a selected graphical object to the bottom or top of the stack of layers, the Bring Forward → Send to Front, or Send Backward → Send to Back commands can be used:</p>

                <p><img src="/dist/images/excel/move-selected-graphical-object.jpg" alt=""></p>

                <p>Alternatively, you can view and interact with the various graphical objects that exist on layers within the worksheet by opening the Selection task pane. To do this, click Picture Tools (or Drawing Tools) – Format → Selection Pane:</p>

                <p><img src="/dist/images/excel/open-selection-task-panel.jpg" alt=""></p>

                <p>This task pane will list all of the graphical objects that exist on the current worksheet and how they are currently arranged:</p>

                <p><img src="/dist/images/excel/graphical-object-list.jpg" alt=""></p>

                <p>If you would like to change how these objects are arranged, click and drag them around in this list to shift their location:</p>

                <p><img src="/dist/images/excel/change-object-arrangement.jpg" alt=""></p>


                <h4>Grouping Objects</h4>
                <p>If you would like to work with multiple objects as a group, such as moving them all at the same time, you can group them together. To do this, select each graphical object that you would like to group (hold the Ctrl key while clicking on each object). Next, click Picture Tools – Format (or Drawing Tools – Format) → Group → Group:</p>

                <p><img src="/dist/images/excel/group-graphical-images.jpg" alt=""></p>

                <p>Once two or more graphical objects are grouped together, any changes made to the group will be applied to all of its members – including size, position, and more:</p>

                <p><img src="/dist/images/excel/applied-group-changes.jpg" alt=""></p>

                <p>To ungroup any objects that have been grouped together, first click to select the group in question and then click Picture Tools – Format (or Drawing Tools – Format) → Group → Ungroup:</p>

                <p><img src="/dist/images/excel/ungrouping-objects.jpg" alt=""></p>

                <p>After ungrouping a group, you can quickly reform it by selecting one of the objects that a member of the previous group and clicking Picture Tools – Format (or Drawing Tools – Format) → Group → Regroup.</p>

                <h4>Positioning Objects</h4>
                <p>Objects can be positioned in relation to the grid of the worksheet, as well as in relation to other shapes appear on it. You can enable either of these options by clicking Picture Tools – Format → Align → Snap to Grid (or Snap to Shape) while the object that you would like to work with is selected:</p>

                <p><img src="/dist/images/excel/change-object-positioning.jpg" alt=""></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Instructor driven Microsoft Excel classes in Chicago and LA</h4>
                    <p>Looking to join a 1-day Excel workshop? We run beginner, intermediate and advanced classes every month. Check our past students  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.<br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>