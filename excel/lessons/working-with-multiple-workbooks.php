<?php
$meta_title = "Working with multiple Workbooks in Excel Part 2 | Training Connection";
$meta_description = "Learn how to manage data across multiple Excel Workbooks. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Multiple Workbooks cont.</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Working with Multiple Workbooks cont.">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Working with Multiple Workbooks cont.</h1>

                        <h5>
                            The following topics are covered  in Module 1 in our Excel Level 2 Intermediate class.
                        </h5>
                        <p>For Working with Multiple workbooks part A <a href="/excel/lessons/multiple-workbooks.php">click here</a>.</p>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Navigating in Multiple Workbooks</h4>
                <p>Sometimes you need to transfer data from one workbook to another, perhaps as a way to consolidate information about one client or product. In order to do this efficiently, you must open and view multiple workbooks.</p>
                <p>For more om Microsoft Excel classes in Chicago and Los Angeles <a href="/excel-training.php">click here</a>.</p>

                <h4>Opening Multiple Workbooks</h4>
                <p>You may open as many workbooks as you want, but having too many workbooks open at the same time can slow down your system or cause your computer to lock-up. Therefore, you should only open the workbooks you need and close those with which you have finished working.</p>

                <h4>Method</h4>
                <h6>To open multiple workbooks:</h6>
                <ol>
                    <li>Click on the File Button on your top left hand corner.</li>
                    <li>In the Open dialog box, select the workbook you want to open.</li>
                    <li>Choose Open.</li>
                    <li>Repeat steps 1 through 3 until all needed workbooks are open.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will open multiple workbooks.</h6>
                <ol>
                    <li>On the File Button. [The Open dialog box appears].</li>
                    <li>In the Look in drop-down list box, make sure the Data folder appears.</li>
                    <li>In the file list box, select the Advertising workbook.</li>
                    <li>Choose Open. [The Advertising workbook opens].</li>
                </ol>


                <h4>Selecting and Viewing Multiple Workbooks</h4>
                <p>When you open a workbook, it appears on the screen in its default size, usually maximized. In most cases, this will cover any other open workbooks on the system. This doesn't mean the other workbooks aren't there. You just can’t see them. You can easily change your view of one workbook to another. You can also view more than one workbook on the screen, using the Arrange command on the Window group.</p>


                <h4>Method</h4>
                <h6>To select multiple workbooks:</h6>
                <ol>
                    <li>From the Window group, select the file name of the workbook you want to view.</li>
                </ol>

                <h6>To view multiple workbooks:</h6>
                <ol>
                    <li>From the Window group, choose Arrange.</li>
                    <li>In the Arrange Window dialog box, in the Arrange area, select the desired option.</li>
                    <li>If necessary, deselect the Windows of active workbook check box.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will select and view multiple workbooks.</h6>
                <ol>
                    <li>From the Window group, choose Latham College from the Switch Windows option. [The Latham College workbook is now active].</li>
                    <li>From the Window group, choose Arrange. [The Arrange Windows dialog box appears].</li>
                    <li>In the Arrange area, make sure the Tiled option button is selected.</li>
                    <li>Choose OK. [The two workbook windows are tiled side by side].</li>
                </ol>

                <h4>Creating and Opening a Workspace</h4>
                <p>You use a workspace when you need to save a configuration of open workbooks on your system. In other words, let's say you've opened two or three workbooks, have arranged them satisfactorily, and then discover that you’re out of time. Instead of repeating all the arranging the next time you start Excel, you simply save the arrangement as a workspace. The workspace file you create provides instructions to Excel about arranging and opening workbooks; however, it does not contain the workbooks themselves. When you open the workspace file, the actual workbooks appear. Because the workspace file contains pointers to the workbook locations, it's important not to move the workbooks that are part of the workspace into new folders, unless you move them from within the workspace itself (i.e., by using the Save As command).</p>


                <h4>Method</h4>
                <h6>To create a workspace:</h6>
                <ol>
                    <li>Arrange all open workbooks as desired.</li>
                    <li>From the View tab, choose Save Workspace.</li>
                    <li>In the Save Workspace dialog box, from the Save in drop-down list, select a folder in which to store the workspace.</li>
                    <li>In the File name drop-down combo box, type a name for the workspace.</li>
                    <li>Choose Save.</li>
                </ol>

                <h6>To open a workspace:</h6>
                <ol>
                    <li>On the File Button, click the Open button.</li>
                    <li>In the Open dialog box, select the file name of the workspace.</li>
                    <li>Choose Open.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will select and view multiple workbooks.</h6>
                <ol>
                    <li>Make sure the Latham College and Advertising workbooks are both open and tiled side by side.</li>
                    <li>From the View tab, in the Window group, choose Save Workspace. [The Save Workspace dialog box appears].</li>
                    <li>In the Save in drop-down list box, make sure the Data folder is selected.</li>
                    <li>In the File name drop-down combo box, type Latham.</li>
                    <li>Choose Save. [The Latham workspace is created. A message box appears asking you if you want to save the Latham College workbook].</li>
                    <li>Choose OK.</li>
                    <li>Select the Advertising workbook window.</li>
                    <li>Maximize and then close the Advertising workbook window. [The Latham College workbook is active].</li>
                    <li>Close the Latham College workbook. [There are no open workbooks].</li>
                    <li>On the File Button menu, click the Open button. [The Open dialog box appears].</li>
                    <li>In the file list box, select Latham.</li>
                    <li>Choose Open. [The workspace opens, with the Latham College and Advertising workbooks tiled side by side].</li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/multiple-workbooks.php">Working with Multiple Workbooks</a></li>
                        <li><a href="/excel/lessons/worksheets.php">Working with Excel Worksheets</a></li>
                        <li><a href="/excel/lessons/multiple-views.php">Working with Multiple Views</a></li>
                        <li><a href="/excel/lessons/creating-workspaces.php">Creating an Excel Workspace</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">How to build Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing  Formulas in MS Excel</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel Intermediate classes Chicago and LA</h4>
                    <p>Looking for a face-to-face instructor-led Excel class? You have come to the right place! We have thousands of satisfied students each year. See a sample of <a href="/testimonials.php?course_id=19">recent class reviews</a>. There are 4 different levels of  Excel training. For more information about what is covered in each level please click on the links below.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>