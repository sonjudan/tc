<?php
$meta_title = "Editing Records using the Data Form in Excel | Training Connection";
$meta_description = "Learn how to edit Excel records using Data form. This course is part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editing Records using Data Form</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Editing Excel Records Using the Data Form">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Editing Excel Records Using the Data Form</h1>
                        <h5>
                            These  topics are covered  in Module 1 - Working with Lists, part of our <a href="/excel-training.php">Advanced  Excel training course</a>.
                        </h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>You can edit any data that appears in an edit box in a data form. If record data appears with no edit box, then you cannot edit this data in the data form because the field contains a formula.</p>

                <p>If you edit a record and then decide to cancel your edit, you can choose Restore. You must choose Restore before choosing New or scrolling to another record; otherwise, the Restore button will be dimmed and you will not be able to cancel your edit.</p>

                <p>For instructor-led Microsoft Excel classes in Chicago and LA call us on 888.815.0604.</p>

                <h4>Method</h4>
                <h6>To edit a record using the data form:</h6>
                <ol>
                    <li>In the data form, move to the record you want to edit.</li>
                    <li>Click the desired edit box.</li>
                    <li>Make the desired changes.</li>
                    <li>If desired, move to the next edit box in which you want to edit the data.</li>
                    <li>Press Enter or scroll to another record to accept the edit and keep the data form open. or</li>
                    <li>Choose Close.</li>
                </ol>

                <h6>To restore an edited record using the data form:</h6>
                <ol>
                    <li>Choose Restore before pressing Enter, choosing New, scrolling to another record, or closing the data form.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will enter column labels in a list.</h6>
                <ol>
                    <li>Open the Car Comparison workbook, and, if necessary, select the Rental worksheet.</li>
                    <li>Open the data form for the list.</li>
                    <li>Scroll until the record with ID NO 33 is displayed. [The record for the Ford Festiva is displayed].</li>
                    <li>Click the IN edit box.</li>
                    <li>Change n to y. [The Restore command is active].</li>
                    <li>Move the data form so the record for the Ford Fiesta in the worksheet is visible . [The n in the IN column is still displayed in the worksheet].</li>
                    <li>Click the bottom scroll arrow repeatedly until the record with ID NO 3 is displayed. [The n changes to y in the worksheet in the In field for the Fort Fiesta as soon as you click the scroll arrow. The record for the Ford Tempo is displayed in the data form. The Restore button is dimmed and no longer active].</li>
                    <li>Move the data form so the record for the Ford Tempo (ID NO 3) in the worksheet is visible.</li>
                    <li>Click the DOORS edit box.</li>
                    <li>Change 4 to 2. [The 4 is still displayed in the worksheet, and the Restore button is active].</li>
                    <li>Choose Restore. [Excel changes the 2 back to a 4 in the Doors field for the Ford Tempo. The Restore button is dimmed and no longer active].</li>
                    <li>Close the data form.</li>
                </ol>

                <h4>Adding and Deleting Fields in a List</h4>
                <p>In addition to editing records, Excel lets you alter the list structure by adding and deleting fields. You add fields to a list by adding columns. If you add a new column, Excel automatically includes the new column within the list. You delete a field by deleting the appropriate column from the worksheet.</p>




                <h4>Method</h4>
                <h6>To add a field to a list:</h6>
                <ol>
                    <li>Select the column that will be to the right of the new column.</li>
                    <li>In the Cells group on the Home tab, click the Insert button.</li>
                    <li>In the appropriate cell, enter the column label.or</li>
                    <li>If the new column will be the last column in the list, in the appropriate cell, enter the column label.</li>
                </ol>

                <h6>To delete a field from a list:</h6>
                <ol>
                    <li>Select the column containing the field that you want to delete.</li>
                    <li>In the Cells group on the Home tab, click the Delete button</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will add a field to a list, and then you will delete the added field.</h6>
                <ol>
                    <li>In cell I1, enter MILES.</li>
                    <li>Open the data form for the list. [The new field name and edit box appear in the data form].</li>
                    <li>Close the data form.</li>
                    <li>Select column I.</li>
                    <li>In the Cells group on the Home tab, click the Delete button. [The MILES column is deleted].</li>
                    <li>Select column G.</li>
                    <li>In the Cells group on the Home tab, click the Insert button. [A new column is inserted to the left of the RATE column].</li>
                    <li>In cell G1, enter MILES.</li>
                    <li>Open the data form for the list. [The new field name and edit box appear in the data form].</li>
                    <li>Close the data form.</li>
                </ol>

                <h4>Finding Records in a List</h4>
                <p>A computerized list gives you the ability to quickly and easily search for information meeting any criteria that you specify. The Criteria button of the data form allows you to search for a particular record based on the information you enter in the edit boxes. You can search using a single criterion or multiple criteria. You can also search for exact matches, or, by using operators such as > or <, you can search for data in a specific range. <a href="/excel-training-los-angeles.php">LA Excel training course</a>,</p>
                <p>If no records meet all search criteria, the current record will be displayed. Because searches are not case-sensitive, you can type the criteria in uppercase or lowercase letters without affecting the results of your search.</p>

                <h4>Method</h4>
                <h6>To find records in a list:</h6>
                <ol>
                    <li>Open the data form.</li>
                    <li>Choose Criteria.</li>
                    <li>Select the field on which you want to base your search.</li>
                    <li>Type the desired operator (=, >, <, >=, <=).</li>
                    <li>Type the desired value.</li>
                    <li>Repeat steps 3 to 5 for each criterion.</li>
                    <li>Choose Find Next to find the next record in the list that meets the search criteria. or</li>
                    <li>Choose Find Prev to find the previous record in the list that meets the search criteria.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will find records using a single criterion, and then you will find records using multiple criteria.</h6>
                <ol>
                    <li>Select the Cars worksheet.</li>
                    <li>Open the data form for the list.</li>
                    <li>Choose Criteria. [Criteria appears in the upper right corner of the data form. All edit boxes are empty, the New button is dimmed, and the Clear button has replaced the Delete button].</li>
                    <li>Click the MAKE edit box.</li>
                    <li>Type Ford.</li>
                    <li>Choose Find Prev. [A beep indicates that there are no previous records meeting the criterion. The first record of the list is displayed].</li>
                    <li>Choose Find Next. [The first record meeting the criterion is displayed].</li>
                    <li>Continue to choose Find Next until you hear a beep indicating that you have viewed all the records meeting the criterion.</li>
                    <li>Choose Criteria. [Ford appears in the MAKE edit box].</li>
                    <li>Choose Clear. [The previous search criterion is cleared from the data form.he previous search criterion is cleared from the data form].</li>
                    <li>Click the MAKE edit box.</li>
                    <li>Type Chevy.</li>
                    <li>Click the DOORS edit box.</li>
                    <li>Type 2.</li>
                    <li>Choose Find Prev. [The first record meeting both criteria is displayed].</li>
                    <li>Continue to choose Find Prev until you hear a beep indicating that you have viewed all the previous records meeting the criteria.</li>
                    <li>Choose Criteria. [Chevy appears in the MAKE edit box, and 2 appears in the DOORS edit box].</li>
                    <li>Choose Clear. [The previous search criteria are cleared from the data form].</li>
                    <li>Click the RATE edit box.</li>
                    <li>Type >29.95.</li>
                    <li>Click the MAKE edit box.</li>
                    <li>Type Dodge.</li>
                    <li>Choose Find Prev. [A beep sounds immediately, indicating that no previous records meet these criteria].</li>
                    <li>Choose Find Next until you hear a beep indicating that you have viewed all records meeting the criteria.</li>
                    <li>Close the data form.</li>
                </ol>

                <h4>Sorting a List</h4>
                <p>To rearrange the records in your list in a specific order, you can sort the list based on the fields identified by the column labels. To sort a list, you can use a single criterion, such as the last names of the individuals or the state in which the individuals live. In a very large list, you can use multiple criteria, such as a last name and a first name.</p>
                <p>To sort a list, you first select a cell in the list, just as you would before accessing the data form. You then specify the fields to sort by in the Sort dialog box, shown in Figure 1-11.</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-11.png" alt="Figure 1-11: The Short Dialog Box with one Sort level created"></p>
                <p class="wp-caption-text">Figure 1-11: The Short Dialog Box with one Sort level created</p>

                <p>When you sort data, Excel rearranges rows, columns, or individual cells using the column sort order that you specify, either ascending order (A-Z or 1-9) or descending order (Z-A or 9-1). If you do not specify a specific order, Excel sorts the rows in ascending order.</p>

                <p>Microsoft Excel uses the following guidelines when sorting lists:</p>
                <ol>
                    <li>Rows with blank cells are placed at the bottom of the sorted list.</li>
                    <li>Hidden rows are not moved.</li>
                    <li>Values are sorted before text and before numbers formatted as text.</li>
                    <li>Numbers formatted as text are sorted before text alone.</li>
                    <li>The sort options are saved from the last sort done until the column labels or the sort is changed in the list.</li>
                </ol>

                <p>You can add Sort ‘levels’. This constitutes ‘Then by’ sorts. Meaning multiple sort criteria, processed in the order of the sort instruction at the top of the list, descending downwards.</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-12.png" alt="Figure 1-12: The Short Dialog Box with one Sort level created ("Sort by" plus one "Then by")"></p>
                <p class="wp-caption-text">Figure 1-12: The Short Dialog Box with one Sort level created ("Sort by" plus one "Then by")</p>

                <h4>Method</h4>
                <h6>To sort a list:</h6>
                <ol>
                    <li>Select a cell in the list you want to sort.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Sort button.</li>
                    <li>In the Sort dialog box, from the Sort by drop-down list, select the first column by which you want to sort your list.</li>
                    <li>Select an option in the Order section of the Sort by fields (options).</li>
                    <li>If desired, click the Add level button, creating the first of a ‘Then by’ list of sort instructions. Select the second list column by which you want to sort your list.</li>
                    <li>Select the Ascending or Descending ORDER options.</li>
                    <li>If desired, add another level and in the second Then by drop-down list, select the third column by which you want to sort your list.</li>
                    <li>Select the required Ascending or Descending options.</li>
                    <li>Make sure the My Data has Headers check box is checked.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will sort a list.</h6>
                <ol>
                    <li>Select a cell in the Cars worksheet list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Sort button. [The Sort dialog box appears, and the list is selected].</li>
                    <li>From the Sort by drop-down list, select ID NO.</li>
                    <li>Make sure the Sort is on Values and the Order is Smallest to Largest.</li>
                    <li>Make sure not to add any Sort levels.</li>
                    <li>Make sure the My Data has Headers check box is checked, and then choose OK. [The list is sorted by ID number].</li>
                    <li>In the Sort and Filter group on the Data tab, click the Sort button. [The Sort dialog box appears, and the list is selected]</li>
                    <li>From the Sort by drop-down list, select MAKE.</li>
                    <li>Make sure the Sort is on Values and the Order is A-Z.</li>
                    <li>Add a level and in the Then by line, select MODEL as the active column.</li>
                    <li>Make sure the Sort is on Values and the Order is A-Z.</li>
                    <li>Make sure the My Data has Headers check box is checked, and then choose OK. [The list is sorted first by MAKE and then by MODEL].</li>
                    <li>Save the workbook.</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <a href="/excel/lessons/creating-lists.php">Creating a List</a>
                        <a href="/excel/lessons/maintaining-lists.php"><br>Maintaining a List</a>
                        <a href="/excel/lessons/filtering-lists.php">Filtering a List</a>
                        <a href="/excel/lessons/subtotals-in-lists.php">Using Subtotals in a List</a>
                        <a href="/excel/lessons/building-formulas.php">Building Formulas in Microsoft Excel</a>
                        <a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a>
                        <a href="/excel/lessons/basic-functions.php">Using Basic Functions in Excel</a>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel student reviews</h4>
                    <p>Every year we train thousands of satisfied students. Read a sample of testimonials at our <a href="/testimonials.php?course_id=19">Excel training testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>