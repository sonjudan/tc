<?php
$meta_title = "Data Validation in Microsoft Excel | Training Connection";
$meta_description = "aThis tutorial will teach you how to use Data Validation to protect your data in Microsoft Excel.aa";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data Validation</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Data Validation in Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Using Data Validation in Excel</h1>
                        <p>For more details on our <a href="/excel-training.php">face-to-face Excel training</a> in Chicago and Los Angeles call us on 888.815.0604. All classes are hands-on and come with a free repeat.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>To protect against incorrect data entry, you can use data validation to restrict the type of data that may be entered into a cell (see Figure 1 below). You can specify a list of the valid entries or limit the number of characters in an entry. To further assist in accurate data entry, you can have a data input message appear that informs you of the type of data to be entered in a particular cell (see Figure 2 below). You can also have an invalid data error message appear when incorrect data is entered (see Figure 3 below).</p>


                <p><img class="aligncenter"src="/dist/images/excel/data-validation.png" alt="Figure 1: The Data Validation Dialog Box"></p>
                <p class="wp-caption-text">Figure 1: The Data Validation Dialog Box</p>

                <p><img class="aligncenter"src="/dist/images/excel/input-message.png" alt="Figure 2: A user-generated data input message (Data validation help)"></p>
                <p class="wp-caption-text"><a href="/excel-training-chicago.php">Get information on Excel training</a>,</p>
                <p class="wp-caption-text">Figure 2: A user-generated data input message (Data validation help)</p>

                <p><img class="aligncenter"src="/dist/images/excel/data-entry-error.png" alt="Figure 3: Data entry error message box (user-generated data error message)"></p>
                <p class="wp-caption-text">Figure 3: Data entry error message box (user-generated data error message)</p>


                <h4>Method to set data validation</h4>
                <h6>To apply data entry restrictions:</h6>
                <ol>
                    <li>Select the desired cells.</li>
                    <li>On the Data tab, in the Data Tools group, click Data Validation.</li>
                    <li>In the Data Validation dialog box, select the Settings tab.</li>
                    <li>In the Validation criteria area, in the Allow drop-down list box, select the desired data type.</li>
                    <li>In the drop-down list box(es) that appear, make the necessary selections.</li>
                    <li>Choose OK.</li>
                </ol>

                <h6>To create a data input message:</h6>
                <ol>
                    <li>Select the cells to which you have applied data validation.</li>
                    <li>On the Data tab, in the Data Tools group, click Data Validation.</li>
                    <li>In the Data Validation dialog box, select the Input Message tab.</li>
                    <li>On the Input Message page, make sure the Show input message when cell is selected check box is selected.</li>
                    <li>In the Title text box, type a title.</li>
                    <li>In the Input message text box, type an input message.</li>
                </ol>

                <h6>To create an invalid data error message:</h6>
                <ol>
                    <li>Select the cells to which you have applied data validation.</li>
                    <li>On the Data tab, in the Data Tools group, click Data Validation.</li>
                    <li>In the Data Validation dialog box, select the Error Alert tab.</li>
                    <li>On the Error Alert page, make sure the Show error alert after invalid data is entered check box is selected.</li>
                    <li>From the Style drop-down list, select the desired style.</li>
                    <li>In the Title text box, type a title.</li>
                    <li>In the Error message text box, type an error message.</li>
                </ol>

                <p>Also see <a href="/excel/lessons/protect.php">Protecting your worksheet</a>.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of skilled trainers we deliver onsite Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training class testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite Excel training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>