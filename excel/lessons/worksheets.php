<?php
$meta_title = "Inserting, renaming and deleting Excel Worksheets | Training Connection";
$meta_description = "Learn to insert, rename and delete worksheets in Microsoft Excel. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Worksheets</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Inserting, renaming, and deleting Excel worksheets">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Inserting, renaming, and deleting Excel worksheets</h1>
                        <h5>
                            These  topics are covered in Module 1 in our 1-day Excel Intermediate course.
                        </h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Inserting New Worksheets</h4>
                <p>Inserting a new worksheet into a workbook is easy. Excel inserts a new worksheet before the currently selected worksheet.</p>

                <p>For <a href="/excel-training.php">Microsoft Excel training</a> in Chicago and Los Angeles give us a call us on 888.815.0604.</p>

                <h4>Method</h4>
                <h6>To insert a new worksheet:</h6>
                <ol>
                    <li>From the Insert menu, choose Worksheet. or</li>
                    <li>Click the Insert new worksheet button next to the worksheet tabs as show in Figure 1.1</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will insert two new worksheets into a workbook</h6>
                <ol>
                    <li>Select the Tuition Generated worksheet from the Latham College workbook.</li>
                    <li>Click on the Home tab, in the Cells group, click Insert, and then click Insert Sheet. [A new worksheet, Sheet1, is inserted in front of the Tuition Generated worksheet].</li>
                    <li>Repeat steps 1 and 2 to insert another worksheet in front of the Tuition Generated worksheet. [A second new worksheet, Sheet2, is inserted].</li>
                </ol>

                <h4>Renaming Worksheets</h4>
                <p>Once you have created a new worksheet, you can rename it. You can create worksheet names up to 31 characters long, including spaces. The worksheet name cannot be enclosed in square brackets and cannot include the following characters:  colon (:), slash (/), backslash (\), question mark (?), and asterisk (*). <a href="/excel-training-chicago.php">Public Excel training in Chicago.</a></p>

                <h4>Method</h4>
                <h6>To rename a worksheet:</h6>
                <ol>
                    <li>Double-click the worksheet tab.</li>
                    <li>Type the new worksheet name.</li>
                    <li>Press Enter</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will rename worksheets.</h6>
                <ol>
                    <li>Double-click the Sheet2 sheet tab. [The Sheet2 sheet tab is selected and the worksheet name is highlighted].</li>
                    <li>Type Notes.</li>
                    <li>Press Enter. [The worksheet is renamed Notes].</li>
                    <li>Double-click the Sheet1 sheet tab. [The Sheet1 sheet tab is selected and the worksheet name is highlighted..</li>
                    <li>Type Temporary.</li>
                    <li>Press Enter. [The worksheet is renamed Temporary].</li>
                </ol>


                <h4>Deleting Worksheets</h4>
                <p>You can delete a worksheet simply by choosing Delete Sheet from the Cells group on the Home tab. Be very careful, however. Once you have deleted a worksheet from a workbook, you cannot undelete it.</p>

                <h4>Method</h4>
                <h6>To delete a worksheet:</h6>
                <ol>
                    <li>Select the worksheet to be deleted.</li>
                    <li>On the Home tab, in the Cells group, click the arrow next to Delete, and then click Delete Sheet.</li>
                </ol>
                <p>Note: You can also right-click the sheet tab to view a shortcut menu, from which you can choose Delete.</p>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will delete one of the worksheets in the workbook.</h6>
                <ol>
                    <li>Double-click the worksheet tab.</li>
                    <li>From the Cells group, choose Delete. [The pop up menu shows tab options].</li>
                    <li>Choose Delete Sheet. [The Temporary worksheet is deleted and the Notes worksheet is selected].</li>
                </ol>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/multiple-workbooks.php">Working with multiple Excel workbooks</a></li>
                        <li><a href="/excel/lessons/multiple-views.php">Working with multiple views in Excel</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with multiple workbooks cont.</a></li>
                        <li><a href="/excel/lessons/creating-workspaces.php">MS Excel Workspaces</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Excel Formulas</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Copying Excel Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Creating  Functions in Excel</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">More about our Excel Intermediate class</h4>
                    <p>Other topics covered on this workshop include advanced functions and formulas, charting, advanced formatting and more. <a href="/testimonials.php?course_id=19">Read our testimonials</a>.</p>
                    <p>There are 4 different levels of  Excel training. For more information about what is covered in each level please click on the links below.</p>
                    <p>We hope to see you on an Excel class soon!</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>