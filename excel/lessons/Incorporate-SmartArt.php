<?php
$meta_title = "Incorporate SmartArt in Microsoft Excel | Training Connection";
$meta_description = "Incorporate SmartArt in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Incorporate SmartArt</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Incorporate SmartArt">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Incorporate SmartArt</h1>
                        <h5>In this article, will delve a little deeper into how to incorporate SmartArt in Microsoft Excel.</h5>
                        <p>For instructor-led <a href="/excel-training.php">Microsoft Excel workshop in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>SmartArt combines text-based information with graphics to create a more appearance-driven look. Using Excel’s tools, you will be able to create great SmartArt that can be used for a variety of different purposes. Over the course of this topic, you will learn how to insert SmartArt into your workbooks, as well as customize it after it has been inserted.</p>


                <h4>Topic Objectives</h4>

                <h6>In this topic, you will learn:</h6>
                <ul>
                    <li>About SmartArt graphics</li>
                    <li>About the Choose a SmartArt Graphic dialog box</li>
                    <li>About the Text pane</li>
                </ul>

                <h4>About SmartArt</h4>

                <p>SmartArt graphics are used to visually represent text-based content. A great example of this would be a flowchart or hierarchy diagram; while you can describe these things, it is much more intuitive to the reader to see a graphic that describes and separates each step. Just like other graphical objects, SmartArt graphics are individual objects on a worksheet that can be moved and modified as group when needed. Some of these changes are made using the Sizing handles (1), while the Text Pane (3) allows you to quickly add text. This pane is toggled by clicking the arrow button (2) that appears on the left side of a SmartArt object:</p>

                <p><img src="/dist/images/excel/flowchart-diagram.jpg" alt="flowchart diagram demonstration"></p>

                <h4>The Choose a SmartArt Graphic Dialog Box</h4>
                <p>To insert SmartArt into a worksheet, click Insert → SmartArt:</p>

                <p><img src="/dist/images/excel/smartart-graphic-dialog-box.jpg" alt="inserting smartart into worksheet"></p>

                <p>This action will display the Choose a SmartArt Graphic dialog box:</p>

                <p><img src="/dist/images/excel/display-smartart-dialog-box.jpg" alt="smartart dialog box displayed"></p>

                <p>The Choose a SmartArt Graphic dialog box is divided into several categories of graphics, each of which is displayed in a panel on the left. Here is an overview of each of these categories.</p>
                <ul>
                    <li>The List category is used to create bulleted lists with some visual flair</li>
                    <li>The Process category is used to illustrate information in sequential order, such as a series of steps to complete a task</li>
                    <li>The Cycle category is intended to illustrate continuous processes</li>
                    <li>The Hierarchy category will display the steps in a process or the people in an organizational chart</li>
                    <li>The Relationship category is used to show how elements can connect to each other</li>
                    <li>The Matrix category is used to show how elements in a system relate to it.</li>
                    <li>The Pyramid category is used to create diagrams that display how elements of varying importance, size, or power relate proportionally to each other</li>
                    <li>Finally, the Picture category is used to create diagrams that display content using a combination of text and graphics</li>
                </ul>

                <p>By clicking on these categories, you will change what type of SmartArt layouts are displayed in the middle pane. Clicking the thumbnail for a SmartArt graphic will display a preview for that graphic as well as a brief description for it. With the thumbnail still selected, click OK to insert this graphic. <a href="/excel-training-los-angeles.php">More about Excel training classes</a> in Downtown Los Angeles.</p>

                <h4>About the Text Pane</h4>

                <p>When you first insert SmartArt into a worksheet, the Text pane will be displayed beside it:</p>

                <p>(It can also be displayed or hidden by clicking SmartArt Tools – Design → Text Pane.)</p>
                <p>Using this pane you are able to enter text into the SmartArt graphic. While you are still able to enter text directly into the graphic by clicking on the placeholder text and typing over it, the Text pane offers broader control over it.</p>
                <p>For example, if you are working with bulleted lists, the Text pane will give you a clear indication where you are inserting text in relation to the other bullet points. Additionally, because each bullet point represents a graphic, adding new graphics is usually as simple as adding another bullet point inside the Text pane. (However, note that not all SmartArt graphics work this way. Depending upon exactly what SmartArt graphic that you are working with, adding new bullet points may just add a bullet point to the text within the shape.)</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our 1-day Excel workshop. We run beginner, intermediate and advanced classes every month. Check our past students  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>