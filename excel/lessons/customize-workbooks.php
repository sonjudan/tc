<?php
$meta_title = "Customize Workbooks in Microsoft Excel | Training Connection";
$meta_description = "Customize Workbooks in Microsoft Excel. This course is part of our Microsoft Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Customize Workbooks</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Customize Workbooks">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Customize Workbooks</h1>
                        <h5>In this article, will delve a little deeper into how to customize workbooks in Microsoft Excel.</h5>
                        <p>For <a href="/excel-training.php">class based Microsoft Excel training</a> in Chicago and Los Angeles call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Your workbooks can be customized in a number of different ways. Over the course of this topic, we will focus on customization through the addition of comments, hyperlinks, watermarks, and background pictures.</p>

                <h4>Topic Objectives</h4>

                <h6>In this topic, you will learn:</h6>
                <ol>
                    <li>About comments</li>
                    <li>About hyperlinks</li>
                    <li>About watermarks</li>
                </ol>

                <h4>Comments</h4>

                <p>Notes can be added to any worksheet by adding comments. Typically they are used to add additional context to items within a worksheet to help guide other users, but they can be used to add notes about any topic.</p>
                <p>To add a comment, first select the cell that you would like the comment to be added to. Next, click Review → New Comment:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/add-new-comment.jpg" alt=""></p>

                <p>This action will create a new comment and a red comment indicator will appear in the upper right-hand corner of the currently selected cell. The comment will be labeled with the current user’s name, but this can be changed by replacing that text as needed. To add text to the comment, type inside the provided text area:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/add-text-to-comment-box.jpg" alt=""></p>
                
                <p>Once you are done, click anywhere outside of the comment text area to hide it; however, the red comment indicator will remain visible:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/red-comment-indicator.jpg" alt=""></p>

                <p>To view the comment again, click to select the cell in question or simply hover your cursor over it. To delete a comment, click to select the cell in which the comment has been placed and click Review → Delete.</p>

                <h4>Hyperlinks</h4>

                <p>Hyperlinks are a mainstay in the computing world. They enable you to navigate around your computer, browse the Internet, and jump to different cells within the same workbook. Excel lets you use this handy feature for breaking up long workbooks, pointing people to a web page, providing contact information, and much more. <a href="/excel-training-los-angeles.php">MS Office and Excel training in LA</a>.</p>

                <p>To create a hyperlink, first click to select the cell where you want the hyperlink to be created. Next, click Insert → Hyperlink:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/create-hyperlink.jpg" alt=""></p>
                
                <p>This action will display the Insert Hyperlink dialog box.</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/insert-hyperlink-dialog-box.jpg" alt=""></p>
                <p>The left-hand side of the dialog allows you to choose the type of link (1) that you want to create. By default, the “Existing File or Web Page” option will be selected. (It will likely be the type of link you use most often.)</p>

                <p>With this option, you will see the settings shown above. At the top of the dialog, you can set the text to display (2). This is the text that will turn blue and will contain the actual link. (By default, any text in the cell that you selected will appear in this field, but you can modify it if you wish.)</p>

                <p>Below this field, you can choose the file (3) or website (4) that you want to link to. You can also set up a ScreenTip (5) for the link.</p>

                <p>When you are ready, click OK to save your changes, or click Cancel to discard them. Note that the OK button will not be active until both the “Text to display” and Address fields are filled in.</p>

                <p>Once the hyperlink has been inserted, you will see that it appear blue and underlined. Clicking on the link will open it, but clicking and holding on the link will select the cell that contains it:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/hyperlink-displayed-in-blue.jpg" alt=""></p>
                
                <p>To remove a link, right-click the hyperlink in question and then click Remove Hyperlink in the context menu.</p>

                <h4>Watermarks</h4>

                <p>While Excel 2016 does not include a direct way to create watermarks, you can add them using WordArt. To do this, first insert WordArt by clicking Insert → WordArt → [WordArt Style]:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/type-watermark-text.jpg" alt=""></p>

                <p>Next, type the text that you would like to make up the watermark and rotate it if needed:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/format-shape-pane.jpg" alt=""></p>

                <p>The watermark will now be ready and you can reposition it as needed:</p>

                <p><img src="https://www.trainingconnection.com/images/Lessons/Excel/reposition-watermark.jpg" alt=""></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Instructor driven Excel training available in Chicago and LA</h4>
                    <p>Join our 1-day Excel workshop. We run beginner, intermediate and advanced classes every month. Check our past students  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>