<?php
$meta_title = "Create and Use Templates In Microsoft Excel | Training Connection";
$meta_description = "Learn how to Create and Use Templates In Microsoft Excel. Training Connection offers Excel Training Courses in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create and Use Templates In Microsoft Excel</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Create and Use Templates In Microsoft Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Create and Use Templates In Microsoft Excel</h1>

                        <p>Templates are an excellent resource that you can use to create workbooks much more quickly than having to create one from scratch. Over the course of this topic, you will learn more about templates and how to create them yourself.</p>
                        <p>For more on <a href="/excel-training.php">Instructor-led Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">



                <h4>Topic Objectives</h4>
                <h6>In this topic, you will learn:</h6>
                <ol>
                    <li>About templates</li>
                    <li>About template types</li>
                    <li>How to create a template</li>
                    <li>Modifying a template</li>
                </ol>

                <h4>Templates</h4>
                <p>A template is a workbook that contains preformatted styles, graphics, objects, and/or text. Its purpose is to provide a method of laying out content to save you time and help you keep your work consistent. Templates can be used with styles and themes to provide even more customization options. <a href="/excel-training-los-angeles.php">MS Excel classes</a> in Los Angeles.</p>
                <p>Microsoft Excel templates are saved as .xltx files. This way, they can be re-used without overwriting the template contents. You can also save templates with macros using the .xltm extension.</p>

                <h4>Template Types</h4>
                <p>Microsoft Excel 2016 includes many different predefined templates that you can choose from that cover a wide variety of different needs. If you can’t initially find the exact template for your purposes, you can probably find it using the search function.</p>

                <p><img src="/dist/images/excel/excel-template-types.jpg" alt="excel template types image"></p>

                <p>Custom templates are those that are created by yourself, your organization, or someone that you are working with. You can see these templates listed under the Personal heading of the new workbook window:</p>

                <p><img src="/dist/images/excel/custom-excel-templates.jpg" alt="custom excel templates screenshot"></p>

                <h4>Creating a template</h4>

                <p>If you want to create your own template, it’s easy! First, create the workbook that you would like to use as a template in the future:</p>

                <p><img src="/dist/images/excel/creating-own-template.jpg" alt="creating your own template"></p>

                <p>Once relevant information has been placed in the template, save it by clicking File → Save As → Browse:</p>

                <p><img src="/dist/images/excel/saving-your-template.jpg" alt="saving your template" width="350"></p>

                <p>The Save As dialog will open. First, click the “Save as type” drop-down menu and choose Excel Template:</p>

                <p><img src="/dist/images/excel/saving-template-2.jpg" alt="saving your template step 2"></p>

                <h4>Modifying a Template</h4>

                <p>To modify a template, click File → Open → Browse:</p>

                <p><img src="/dist/images/excel/modifying-a-template.jpg" alt="modifying your template"></p>

                <p>In the Open dialog box, navigate to the location of the template file, select it, and click Open:</p>

                <p><img src="/dist/images/excel/template-file-selection.jpg" alt="select your save file"></p>

                <p>(If you cannot see your template file, ensure that the filter above the Open button is set to All Excel Files.)</p>

                <p>If you are using File Explorer to open a template for modification, double-clicking a template will open a new Excel workbook based on the template and not the actual template itself. In order to open the template for modification, right-click the template file and click Open:</p>

                <p><img src="/dist/images/excel/file-explorer.jpg"></p>

                <p>Once a template file is open for modification, Excel’s title bar will display the template name (not Book1, Book2, etc.):</p>

                <p><img src="/dist/images/excel/opening-modification-file.jpg" alt="opening a file for modification"></p>

                <p>Saving the file in the default location will ensure that it is displayed in the Personal category of the New category in Backstage view:</p>

                <p><img src="/dist/images/excel/enter-template-name.jpg" alt="entering a template name"></p>



            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Excel throughout the country. View our <a href="/testimonials.php?course_id=19">Excel  testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>