<?php
$meta_title = "Digital Signature in Microsoft Excel | Training Connection";
$meta_description = "Adding a Digital Signature in Excel. This and other topics are covered on our instructor-led Excel Training classes, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Digital Signatures</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Adding a Digital Signature in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Adding a Digital Signature in Excel</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h6>To add a digital signature to a workbook, use the following procedure.</h6>
                <ol>
                    <li>Select the File tab from the Ribbon to open the Backstage View.</li>
                    <li>Select Protect Workbook.</li>
                    <li>Select Add a Digital Signature.</li>
                    <li>Excel may display an informational message. Select OK.</li>
                    <li>In the Sign dialog box, select the Commitment Type from the drop down list.</li>
                    <li>Add Digital Signature Dialogue Box. <a href="/excel-training-chicago.php">Chicago Excel classes</a>.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/digital-signature.jpg" alt="digital-signature"></p>

                <ol start="6">
                    <li>Enter a Purpose for signing the document.</li>
                    <li>If you would like to include additional information about the signer, select Details.</li>
                    <li>In the Additional Signing Information dialog box, enter the signature information and select OK.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/additional-signing-information.jpg" alt="additional-signing-information"></p>

                <ol start="9">
                    <li>Your Signature Certificate should appear in the Signing as area. If not, select Change and choose a new one from the Windows Security dialog box.</li>
                    <li>Select Sign.</li>
                    <li>The Signature Confirmation dialog box displays. Select OK.</li>
                </ol>

                <p>For <a href="/excel-training.php">MS Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.  </p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <a href="/excel/lessons/permissions.php">Permissions and Protecting worksheets</a>
                        <a href="/excel/lessons/sharing-workbooks.php">Sharing workbooks in Excel</a>
                        <a href="/excel/lessons/basic-functions.php">Using Basic Functions</a>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">student Excel reviews</a> or <a href="/onsite-training.php">obtain a quote</a> for a customized Excel class.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>