<?php
$meta_title = "Running Macros in Excel | Training Connection";
$meta_description = "This tutorial will teach you how to run a Macro in Microsoft Excel. This is covered as part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Running Macros</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Running a Macro in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Running a Macro in Microsoft Excel</h1>
                        <p>Once you have recorded a macro, you can play back the keystrokes and command selections anywhere in the workbook. Many of the macros you record will run so quickly that they seem to work almost instantaneously. You select the macro you want to run in the Macro dialog box, shown in Figure 2-5. If you entered a description when you created a macro, the description appears at the bottom of the dialog box when you select the macro.</p>
                        <p> For more details of <a href="/excel/vba.php"> Excel Macro and VBA training classes</a> in Chicago and Los Angeles call us on 888.815.0604. Our classes are hands-on and taught by live certified trainers.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p><img class="aligncenter" src="/dist/images/excel/macro-confi.png" alt="Figure 2-5: The Macro Dialog Box"></p>
                <p class="wp-caption-text">Figure 2-5: The Macro Dialog Box</p>

                <h4>Steps to Run a Macro</h4>
                <h6>To run a macro:</h6>
                <ol>
                    <li>In the Code group on the Developer tab, click the Macros button.</li>
                    <li>In the Macro dialog box, in the Macro Name list box, select the macro you want to run.</li>
                    <li>Choose Run.</li>
                </ol>

                <h4>Recording a Macro in the Personal Macro Workbook</h4>
                <p>By default, when you record a macro, the macro is stored in the current workbook. You can only use that macro when the workbook it is stored in is open. If you want a particular macro to be available whenever you open Excel, you can record the macro in the Personal Macro Workbook. The Personal Macro Workbook opens when you start Excel and remains open unless you manually close it.
                    <a href="/excel-training-los-angeles.php">Onsite Excel training in Los Angeles</a>.<br>
                    <a href="/excel/lessons/macros.php">Also see Recording a Macro</a>
                </p>

                <h4>To record a macro in the Personal Macro Workbook:</h4>
                <ol>
                    <li>In the Code group on the Developer tab, click the Record Macro button.</li>
                    <li>The macro dialog box opens</li>
                    <li>In the Record Macro dialog box, enter a name and description for the macro.</li>
                    <li>From the Store macro in drop-down list, select Personal Macro Workbook.</li>
                    <li>Choose OK.</li>
                    <li>Choose commands or enter data you want to store in the macro.</li>
                    <li>Click the Stop Recording button in the Code group to stop the recording.</li>
                </ol>
                <p>Also see <a href="/excel/lessons/edit-macros.php">How to Edit a Macro</a>.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a onsite Excel training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>