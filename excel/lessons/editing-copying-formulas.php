<?php
$meta_title = "Editing and Copying Formulas in Excel | Training Connection";
$meta_description = "This tutorial will teach you how to edit and copy formualas in Excel. This is covered as part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editing and Copying Formulas</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Editing and Copying Formulas in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Editing and Copying Formulas in Microsoft Excel</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Editing a Formula</h4>
                <p>To edit a formula, use the following procedure. The following example uses an incorrect cell reference in a formula.</p>
                <ol>
                    <li>Select the cell with the formula you want to correct to make it active.</li>
                    <li>Select the Formula Bar. Excel highlights the cell references in the current formula.</li>
                </ol>

                <p>For more on <a href="/excel-training.php">instructor-led Microsoft Excel  classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p><img class="" src="/dist/images/excel/excel-4.png" alt=""></p>
                <p>Highlight the operator or cell references and either type over with the correct reference or operator, or select the correct cell to replace a cell reference.</p>

                <p><img class="" src="/dist/images/excel/excel-5.png" alt=""></p>

                <p>Press ENTER to complete the formula. Excel calculates the formula and moves to the next row. <a href="/excel-training-chicago.php">Click here</a> for more info on Excel classes offered in Chicago.</p>


                <h4>Copying a Formula</h4>
                <p>To copy and paste a formula, use the following procedure.</p>
                <ol>
                    <li>Select the cell with the formula you want to copy. You can also click on the cell and use the keyboard shortcut: CTRL + C.</li>
                    <li class="clearfix">
                        <p>Select Copy from the Home tab on the Ribbon.</p>
                        <div class="clearfix"></div>
                        <img class="alignleft mr-5 mb-5" src="/dist/images/excel/excel-6.png" alt="">
                        <p>Excel highlights the cell whose contents you are copying. This will remain highlighted until you finish pasting, in case you want to paste the cell contents more than once.</p>
                    </li>
                    <li>
                        <img class="alignright ml-5" src="/dist/images/excel/excel-7.png" alt="">
                        <p>Select the cell where you want to copy the formula. Excel displays a number of paste options. To paste a formula, select Paste or Paste formula. Note that as you hover your mouse over the paste options, the rest of the context menu is dimmed. You can also select the cell and use the keyboard shortcut: CTRL + V.</p>

                    </li>
                    <li>Press ENTER to complete the formula. Excel calculates the formula and moves to the next row.</li>
                </ol>


                <h4>Copying a formula with an Absolute Reference</h4>
                <p>To copy a formula with an absolute reference, use the following procedure.</p>
                <ol>
                    <li>Create a new column labeled Taxes.</li>
                    <li>Select the Taxes column for the first product (cell E4).</li>
                    <li>Enter the = sign to begin the formula.</li>
                    <li>Select cell B16 to use it as the first value in the formula. Excel enters the reference as part of the formula. Use the Formula Bar to enter dollar signs before the column and the row (i.e., $B$16).</li>
                    <li>Enter * and the relative reference in the Total Value column.</li>
                </ol>

                <p><img class="" src="/dist/images/excel/excel-8.png" alt=""></p>

                <ol start="6">
                    <li>Press ENTER to complete the formula. Excel moves to the next row and performs the calculations in the formula.</li>
                </ol>
            </div>
        </div>

    </main>


    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/excel-interface.php">Excel Interface</a></li>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Excel</a></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Workbooks</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Excel</a></li>
                        <li><a href="/excel/lessons/excel-selections.php">Making Selections in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Adjusting Column Widths </a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting MS Excel </a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas </a></li>
                        <li><a href="/excel/lessons/basic-functions.php"> Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="#">Excel Level 1 - Introduction</a></li>
                        <li><a href="#">Excel Level 2 - Intermediate</a></li>
                        <li><a href="#">Excel Level 3 - Advanced</a></li>
                        <li><a href="#">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>