<?php
$meta_title = "Excel Multiple Cell References | Training Connection";
$meta_description = "The tutorial covers multiple cell references and 3D references in Microsoft Excel. Training Connection offers Excel Training Courses in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Multiple Cell References</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Multiple Cell References in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Multiple Cell References in Microsoft Excel</h1>
                        <p>Excel is capable of completing complex calculations relatively quickly. Most of the time, your calculations in Excel will involve using multiple pieces of data for each calculation. In order to do this, you will need to be able to reference multiple cells at the same time.</p>

                        <p>For more on <a href="/excel-training.php">MS Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Using Multiple Cell References</h4>
                <h6>Consider the sample worksheet below:</h6>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-1.png" alt="multi-cell-1"></p>
                <p>In order to calculate profit per product, total sales figures need to be calculated and then the cost of the items needs to be subtracted from the sales. To calculate the sales amount, select cell D2 and type =B2*C2 in the formula bar:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-2.png" alt="multi-cell-2"></p>
                <p>Press Enter. Next, select cell D3 and click inside the formula bar. Type an equals sign (=) to begin the formula and then click cell B3:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-3.png" alt="multi-cell-3"></p>
                <p>With B3 now inserted into the formula, you now need to add a mathematical operator. Type * for multiplication and then click C3 to add that cell to the formula:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-4.png" alt="multi-cell-4"></p>
                <p>Press Enter to complete the formula</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-5.png" alt="multi-cell-5"></p>
                <p>Now that the sales values have been calculated for these two products, you need to calculate the profit. Profit is calculated by subtracting the expenses from the sales. Keeping the order of operations in mind, the formula will be: Sales - (Units Sold * Cost Per Item + Overhead).</p>
                <p><a href="/excel-training-los-angeles.php">Beginner to Advanced Excel training</a>.</p>
                <p>Click inside cell G2 and then type the following formula: =D2-(B2*E2+F2). Note that each cell will be color coded as it is entered into the formula:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-6.png" alt="multi-cell-6"></p>
                <p>Press Enter. You will now see a calculation of the profit for the first product:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-7.png" alt="multi-cell-7"></p>
                <p>Save the changes that you have made to the current workbook and close Microsoft Excel 2013. <a href="/excel-training-los-angeles.php">Excel classes offered on 2013, 2016, 2019 and 365</a>.</p>
                <h4>Using 3D References</h4>
                <p>A reference that refers to the same cell or range of cells on multiple sheets is called a 3D reference. They are useful when referencing several worksheets that follow the same pattern. For example, 3D references could become handy when you consolidate different sales results from different departments, for example.</p>
                <p>In the sample workbook, the data you need for a particular formula is spread out over two worksheets. With Sheet1 open, click cell C2 and then click inside the formula bar. Type =SUM(Sheet2!B2*Sheet1!B2):</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-8.png" alt="multi-cell-8"></p>
                <p>As you can see, “Sheet2!B2” references data in cell B2 on Sheet2 in the current workbook, while Sheet1!B2 references data in cell B2 on Sheet1.</p>
                <p>Press Enter to apply the new formula. You will see the results displayed:</p>

                <p><img class="d-inline-block" src="/dist/images/excel/multi-cell-9.png" alt="multi-cell-9"></p>
                <p>Save the changes that you have made to the current workbook and close Microsoft Excel 2013.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Excel throughout the country. View our <a href="/testimonials.php?course_id=19">Excel  student testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>