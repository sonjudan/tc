<?php
$meta_title = "Working with multiple Workbooks in Excel | Training Connection";
$meta_description = "Learn to manage data across multiple Microsoft Excel Workbooks. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data across Workbooks</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Sharing Data across Worksheets and Workbooks">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Sharing Data across Worksheets and Workbooks</h1>
                        <h5>
                            The following topics are covered in our <a href="/excel-training.php">Intermediate Excel training class</a>.
                        </h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Moving and Copying Data between Worksheets</h4>
                <p>You can move and copy data between worksheets just as you do within a single worksheet. Instead of specifying the paste destination in the source worksheet, you specify the paste location in a second worksheet, called the destination worksheet, which can be in the same or another workbook. When you move or copy cells or ranges that contain formulas and functions, you must take into account whether or not they employ relative or absolute references.</p>
                <p>For Instructor-led Excel training in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Method</h4>
                <p>To move data between worksheets:</p>
                <h6>Ribbon method</h6>
                <ol>
                    <li>In the source worksheet, select the data you want to move.</li>
                    <li>On the Home tab, click the Cut button in the Clipboard group.</li>
                    <li>In the destination worksheet, select the desired location for the data.</li>
                    <li>On the Home tab, click the Paste button OR</li>
                    <li>Press Enter</li>
                </ol>

                <h6>Mouse method</h6>
                <ol>
                    <li>Tile the source and destination worksheet windows.</li>
                    <li>In the source worksheet, select the data you want to move.</li>
                    <li>Drag the data to the desired location in the destination worksheet.</li>
                    <li>To copy data between worksheets:</li>
                </ol>

                <h6>Ribbon method</h6>
                <ol>
                    <li>In the source worksheet, select the data you want to copy.</li>
                    <li>On the Home tab, click the Copy button.</li>
                    <li>In the destination worksheet, select the desired location for the data.</li>
                    <li>On the Home tab, click the Paste button OR</li>
                    <li>Press Enter</li>
                </ol>

                <h6>Mouse method</h6>
                <ol>
                    <li>Tile the source and destination worksheet windows.</li>
                    <li>In the source worksheet, select the data you want to copy.</li>
                    <li>Press and hold Ctrl.</li>
                    <li>Drag the data to the desired location in the destination worksheet.</li>
                    <li>Release Ctrl</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will move and copy data between worksheets.</h6>
                <ol>
                    <li>In the Advertising Workbook choose the Mass Mailing 2000 worksheet, select cells B5:B9</li>
                    <li>On the Home tab, click the Copy button. [A moving border appears around the selected range].</li>
                    <li>Select the SUMMARY worksheet, and then select cell B5.</li>
                    <li>Press Enter. [The range is pasted into the SUMMARY worksheet].</li>
                    <li>From the Window group located in the View tab, choose New Window. [A copy of the Advertising workbook window appears].</li>
                    <li>Tile the SUMMARY and Mass Mailing 2001 worksheet windows.</li>
                    <li>In the Mass Mailing 2001 worksheet window, select the range B5:B9.</li>
                    <li>Drag the selected source data to the SUMMARY worksheet window, to the range C5:C9. [As you drag, the right worksheet window becomes active and an outline appears the size of the selected data. Then, the data is moved to the new location].</li>
                    <li>Press and hold Ctrl.</li>
                    <li>In the SUMMARY worksheet window, drag the selected data back to the Mass Mailing 2001 worksheet window, to the range B5:B9. [A plus sign is added to the mouse pointer when positioned over the border of the selected range. As you drag, the right worksheet window becomes active and an outline the size of the selected data appears].</li>
                    <li>Release Ctrl. [The 2001 data appears in both worksheet windows].</li>
                    <li>In the left worksheet window, select the Mass Mailing 2002 worksheet, and then select cells B5:B9.</li>
                    <li>On the Home tab (clipboard group), click the Cut button. [A moving border appears around the selected range].</li>
                    <li>In the right worksheet window, in the SUMMARY worksheet, select cell D5.</li>
                    <li>On the Home tab (clipboard group), click the Paste button. [The data is moved to the new location and is too long for the cell width].</li>
                    <li>Click the Undo button. [The data is returned to the source worksheet. The selected range has a moving border around it].</li>
                    <li>Press Esc. [The moving border disappears].</li>
                    <li>Close the left worksheet window.</li>
                    <li>Maximize the remaining worksheet window.</li>
                </ol>

                <h4>Linking Data Between Worksheets</h4>
                <p>When you link data between worksheets, it becomes dynamic. In other words, if you change the data in the source worksheet, you automatically change the linked data in the destination worksheet. You can use linked data to create dynamic formulas and to keep your worksheets in agreement. <a href="/excel-training-chicago.php">Small Excel clases in Chicago</a>.</p>

                <h4>Linking Data with the Paste Special Command</h4>
                <p>One way to link data is to paste a copy of it in the destination worksheet using the Paste Special dialog box, shown in Figure 1-7. In this way, you ensure that both the source data and the copy are updated.</p>
                <p>Because linking involves multiple worksheets, Excel uses 3-D references. A 3-D reference consists of the name of the source worksheet in single quotes, followed by an exclamation mark and the cell reference, range reference, or range name. For example, ‘Mass Mailing 2002’!B5 refers to cell B5 in the Mass Mailing 2002 worksheet. You will see a 3-D reference in the Formula bar when you select a cell containing linked data.</p>

                <p><img src="/dist/images/excel/excel2-figure1-7.png" alt="Figure 1-7: The Pate Special Options from Paste buttons and Paste Speical Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-7: The Pate Special Options from Paste buttons and Paste Speical Dialog Box</p>

                <h4>Method</h4>
                <h6>To link data between worksheets:</h6>
                <ol>
                    <li>In the source worksheet, select the data to be linked.</li>
                    <li>On the Home tab (clipboard group), click the Copy button.</li>
                    <li>In the destination worksheet, select the location for the linked data.</li>
                    <li>On the Home tab (clipboard group), choose Paste > Paste Special.</li>
                    <li>In the Paste Special dialog box, choose Paste Link.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will link data between worksheets.</h6>
                <ol>
                    <li>In the Mass Mailing 2002 worksheet, select cells B5:B9.</li>
                    <li>On the Home tab (clipboard group), click the Copy button. [A moving border appears around the selected range].</li>
                    <li>In the SUMMARY worksheet, select cell D5.</li>
                    <li>From the Clipboard group on the Home tab, click the drop down arrow, choose Paste Special. [The Paste Special dialog box appears].</li>
                    <li>Choose Paste Link. [The dialog box closes, the data is pasted, and the formula bar shows a 3-D reference].</li>
                    <li>If necessary, widen the column to view all data.</li>
                    <li>Select the Mass Mailing 2002 worksheet. [The moving border is still around the selected range].</li>
                    <li>Press Esc. [The moving border disappears].</li>
                    <li>Edit the contents of cell B5 to read 1,186,521.</li>
                    <li>Check the SUMMARY worksheet to see any changes. [The data in cell D5 is updated].</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/multiple-workbooks.php">Working with Multiple Workbooks</a></li>
                        <li><a href="/excel/lessons/worksheets.php">Working with Excel Worksheets</a></li>
                        <li><a href="/excel/lessons/multiple-views.php">Working with Multiple Views</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with Multiple Excel Workbooks cont. </a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Working with Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Formulas - Editing and Copying</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic MS Functions</a></li>

                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel training Course in Chicago and Los Angeles</h4>
                    <p>We offer hands-on Excel training classes. There is no better way to learn than from a live face-to-face trainer. We have thousands of happy students - click the following link to read a <a href="/testimonials.php?course_id=19">sample of Excel training testimonials</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>