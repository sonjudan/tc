<?php
$meta_title = "Building Formulas in Microsoft Excel | Training Connection";
$meta_description = "Learn to build basic formulas in Excel. This is covered as part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Column Widths and Saving Files</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Building Formulas in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Building Formulas in Microsoft Excel</h1>

                        <p>The backbone of Excel is its ability to perform calculations. There are two ways to set up calculations in Excel: using formulas or using functions. Formulas are mathematical expressions that you build yourself. You need to follow proper math principles in order to obtain the expected answer. Building the formula is simply a matter of combining the proper cell addresses with the correct operators in the right order. This module will explore how to build, edit, and copy formulas. This module will also explain the difference between relative and absolute references. Finally, this module will explain how to use the Status Bar to perform simple calculations. We will explore functions in the next module.</p>
                        <p><a href="/excel-training.php">For instructor-led MS Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>The Math Basics of Excel</h4>
                <p>Review the different types of operators.</p>

                <h6>The Arithmetic operators are:</h6>

                <p>
                    +  Plus Sign - Adds values <br>
                    -  Minus Sign - Subtracts values <br>
                    *  Asterisk - Multiplies values <br>
                    /  Forward slash - Divides values <br>
                    %  Percent sign - Finds the percentage of a value <br>
                    ^  Caret - Exponentiation - Finds the exponential value
                </p>

                <h6>The Comparison operators are:</h6>
                <p>
                    =  Equals sign - Equates values <br>
                    >  Greater than sign - Indicates that one value is greater than the other <br>
                    <  Less than sign - Indicates that one value is less than the other <br>
                    >= Greater than or equal to - Indicates that one value is greater than or equal to the other <br>
                    <= Less than or equal to - Indicates that one value is less than or equal to the other <br>
                    <>  Not Equal - Indicates that values are not equal
                </p>

                <p>Text concatenation allows you to combine text from different cells into a single piece of text. The operator is the & sign.</p>
                <h6>The reference operators combine a range of cells to use together in an operation. The reference operators are:</h6>

                <p>
                    : Colon - A Range operator that produces a reference to all of the cells between the references on either side of the colon <br>
                    , Comma - A Union operator that combines multiple range references <br>
                    <space> - An intersection operator that returns a reference to the cells common to the ranges in the formula
                </p>

                <h4>Building a Formula</h4>
                <p>To enter a formula to calculate the Total Value in the sample worksheet, use the following procedure. <a href="/excel-training-chicago.php">Chicago-based Excel training</a>.</p>

                <img class="alignleft mr-5" src="/dist/images/excel/excel-1.png" alt="">

                <ol class="ml-5">
                    <li>Select the Total Value column for the first product (cell D4).</li>
                    <li>Enter the = sign to begin the formula.</li>
                    <li>Select cell B4 to use it as the first value in the formula. Excel enters the reference as part of the formula.</li>
                </ol>

               <div class="clearfix"></div>

                <img class="alignleft mr-5" src="/dist/images/excel/excel-2.png" alt="">

                <ol start="4">
                    <li>Enter the * sign.</li>
                    <li>Click on cell C4 to use it as the second value in the formula. Excel enters the references as part of the formula.</li>
                    <li>Press ENTER to complete the formula. Excel moves to the next row and performs the calculations in the formula.</li>
                </ol>

                <div class="clearfix"></div>

                <p>The following illustration shows the answer to the calculation in the cell, and since the cell is active, you can see the formula in the Formula bar.</p>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/excel-interface.php">Excel Interface</a></li>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Excel</a></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Workbooks</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Excel</a></li>
                        <li><a href="/excel/lessons/excel-selections.php">Making Selections in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Adjusting Column Widths </a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting MS Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas </a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>