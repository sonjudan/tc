<?php
$meta_title = "Worksheet Protection in Microsoft Excel | Training Connection";
$meta_description = "This tutorial will teach you how to protect your worksheets in Microsoft Excel.";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Protect your Worksheet</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Worksheet Protection in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Using Worksheet Protection in Excel</h1>
                        <p>By default, all cells in a worksheet are designated as <em>locked</em>. You cannot prohibit changes to locked cells unless you protect the worksheet, after which none of the locked cells can be modified. If you want to be able to modify specific cells in a protected worksheet, you must <em>unlock</em> them before protecting the worksheet. When you modify any unlocked cells, the results in any protected cells that contain formulas dependent upon unlocked cells will also be modified.</p>
                        <p>For more details on our <a href="/excel-training.php">Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604. All classes are hands-on and instructor-led.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>When you protect a worksheet, you can also add password protection. To make modifications to any protected cells in a password protected worksheet, a user must first enter the password. Passwords in Excel are case‑sensitive, depending on exact capitalization. When you use a password, be sure to use one that you will remember. Once you password protect a worksheet, the only way to unprotect it is by re‑entering that password.</p>
                <p>Also see <a href="/excel/lessons/rows-columns.php">how to protect data using Hide/Unhide columns and rows</a>.</p>

                <h4>Method to protect your worksheets</h4>

                <h6>To unlock cells before protecting a worksheet:</h6>
                <ol>
                    <li>Select the cells you want to unlock.</li>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>In the Format submenu, deselect the Lock Cell button (see Figure below).</li>
                </ol>
                <p>For more details on LA Excel classes <a href="/excel-training-los-angeles.php">click here</a>.</p>

                <h6>To protect a worksheet:</h6>
                <ol>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>From the Format submenu, choose Protect Sheet.</li>
                    <li>If desired, in the Protect Sheet dialog box, in the Password text box, enter a password.</li>
                    <li>Choose OK.</li>
                    <li>In the Confirm Password dialog box, in the Re-enter password to proceed text box, reenter the password.</li>
                    <li>Choose OK.</li>
                </ol>

                <p><img src="/dist/images/excel/protection.png" alt="protection"></p>

                <h6>To unprotect a worksheet:</h6>
                <ol>
                    <li>On the Home tab, in the Cells group, click Format.</li>
                    <li>From the Format submenu, choose Unprotect Sheet.</li>
                    <li>If necessary, in the Unprotect Sheet dialog box, in the Password text box, enter the password.</li>
                    <li>Choose OK.</li>
                </ol>

                <p>Also see <a href="/excel/lessons/data-validation.php">how to use Data Validation in Excel</a>.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of skilled trainers we deliver onsite Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel student testimonials</a> or <a href="/onsite-training.php">obtain a quote for onsite training on Microsoft Excel</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>