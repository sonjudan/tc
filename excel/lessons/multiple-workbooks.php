<?php
$meta_title = "Working with multiple Excel Workbooks and Worksheets Part 1 | Training Connection";
$meta_description = "Learn to work with multiple Microsoft Excel Worksheets and Workbooks. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Multiple Workbooks</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Multiple Excel Worksheets and Workbooks">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Using Multiple Excel Worksheets and Workbooks</h1>
                        <h5>These  topics are covered  in Module 1 in our Excel Level 2 Intermediate course.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Using Multiple Worksheets</h4>
                <p>Analyzing and consolidating large amounts of data is one of Excel’s strongest features. By combining several related worksheets into a single workbook, you can restructure your data and organize it more efficiently. By default, a new workbook contains three worksheets; however, a workbook can contain as many as 255 worksheets or as few as one worksheet.</p>
                <p>For hands-on <a href="/excel-training.php">classroom based Excel training</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Scrolling Through Sheet Tabs</h4>
                <p>When you only have a few worksheets, it’s easy to move from worksheet to worksheet by clicking the sheet tabs. However, as you add more worksheets, you may discover that the sheet tabs for all of the worksheets are not always visible. To make them visible, you use the tab scrolling buttons, shown in Figure 1-1. The tab scrolling buttons only bring sheet tabs into view. If you want to use a worksheet, you still need to select it. <a href="/excel-training-los-angeles.php">Excel VBA and Macros training in Los Angeles</a>.</p>

                <p><img src="/dist/images/excel/excel2-figure1-1.png" alt="Figure 1-1: The Tab Scrolling Buttons"></p>
                <p class="wp-caption-text">Figure 1-1: The Tab Scrolling Buttons</p>

                <table class="table table-bordered table-dark table-hover table-l2">
                    <thead>
                        <tr>
                            <td style="width:260px"><strong>Tab Scrolling Buttons</strong></td>
                            <td><strong>Function</strong></td>
                        </tr>
                    </thead>

                    <tr>
                        <td><img src="/dist/images/excel/scroll-button-1.png" alt="Shows first/last tabs"></td>
                        <td>Brings the first/last sheet tab into view</td>
                    </tr>
                    <tr>
                        <td><img src="/dist/images/excel/scroll-button-2.png" alt="Brings the tab to the left into view"></td>
                        <td>Brings the next sheet tab to the left into view</td>
                    </tr>
                    <tr>
                        <td><img src="/dist/images/excel/scroll-button-3.png" alt="Brings the tab to the right into view"></td>
                        <td>Brings the next sheet tab to the right into view</td>
                    </tr>
                    <tr>
                        <td><img src="/dist/images/excel/scroll-button-4.png" alt="Inserts a new worksheet tab"></td>
                        <td>Insert new worksheet</td>
                    </tr>
                    </tbody>
                </table>

                <h4>Method</h4>
                <h6>To scroll through sheet tabs:</h6>

                <ol>
                    <li>Click the desired tab scrolling button.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will view multiple sheet tabs.</h6>

                <ol>
                    <li>Open the Latham College workbook. [The Enrollment by Seminar worksheet is active].</li>
                    <li>Click the button  if this button is to the left of the tabs the first sheet is displayed if the button is to the right the last sheet is displayed. [The first/last sheet tab in the workbook comes into view].</li>
                    <li>Click the Print Marketing sheet tab. [The Print Marketing worksheet is active].</li>
                    <li>Click the Left scrolling button. [The first sheet tab in the workbook comes into view].</li>
                    <li>Click the Right scrolling button. [The next sheet tab on the right side comes into view].</li>
                    <li>Click the Enrollment by Seminar sheet tab. [The Enrollment by Seminar worksheet is active].</li>
                </ol>

                <h4>Coloring Sheet Tabs</h4>
                <p>You will probably find that one of the easiest ways to keep track of your different sheets is to color the tabs for quick reference (as shown below in Figure 1-2).</p>

                <p><img src="/dist/images/excel/excel2-figure1-2.png" alt="Figure 1-2: Workbook with colored tabs"></p>
                <p class="wp-caption-text">Figure 1-2: Workbook with colored tabs</p>


                <h4>Method</h4>
                <h6>To scroll through sheet tabs:</h6>
                <ol>
                    <li>Right-click the tab you want to color code.</li>
                    <li>Select Tab Color.</li>
                    <li>Select a color and click OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will view multiple sheet tabs.</h6>
                <ol>
                    <li>Open the Fashion Passion workbook. [The Catalogue Sales worksheet is active].</li>
                    <li>Right-click the fifth tab (overdue accounts). [The pop up menu shows tab options].</li>
                    <li>Choose Tab Color...[The color palette opens].</li>
                    <li>Click the Red color box. [The color is highlighted in the palette].</li>
                    <li>Choose OK. The dialog box closes.</li>
                    <li>Click a tab other than the overdue accounts tab. [The overdue accounts tab is red].</li>
                    <li>Color the other tabs in your choice of colors.</li>
                    <li>Color the other tabs in your choice of colors.</li>
                </ol>

                <p>For more on working with Multiple workbooks please click here.</p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/worksheets.php">Working with Excel Worksheets</a></li>
                        <li><a href="/excel/lessons/multiple-views.php">Working with Multiple Views</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with Multiple Workbooks</a></li>
                        <li><a href="/excel/lessons/creating-workspaces.php">Setting up an Excel Workspace</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">More about our Excel Intermediate training</h4>
                    <p>This is a one day hands-on workshop taught by a certified Microsoft Office trainer. At Training Connection we believe that face to face instructor-led training is still the most effective way to learn. Plus all students are entitled to a free repeat should they need a refresher class.  </p>
                    <p>We have trained tens of thousands of students, click here to review some <a href="/testimonials.php?course_id=19">Excel training testimonials</a>.</p>
                    <p>There are 4 different levels of  Excel training. For more information about what is covered in each level please click on the links below.</p>
                    <p>We hope to see you on an Excel class soon!</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>