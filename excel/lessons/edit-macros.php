<?php
$meta_title = "Editing and Deleting Macros in Excel | Training Connection";
$meta_description = "This tutorial will teach you how to first edit and then delete a  Macro in Microsoft Excel. This is covered as part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Editing Macros</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Editing and Deleting Macros in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Editing and Deleting Macros in Microsoft Excel</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Editing a Macro</h4>
                <p>If you need to make simple changes to a macro, such as inserting text or deleting a command, such as a specific format applied to a cell, you can edit the macro. You edit a macro in the Visual Basic Editor, shown in Figure 2-6. The elements of the Visual Basic Editor are described in the table below.</p>
                <p>The Project Explorer, Properties window, and Code window all appear when you open the Visual Basic Editor. Since you won’t need the Properties window while performing simple editing, you can close the Properties window, and then expand the Project Explorer to view more of its window.</p>
                <p>Each open workbook in Excel has a project associated with it in the Project Explorer. Navigating the Project Explorer is similar to navigating Windows Explorer, in that they both have hierarchical structures. The code for a macro is stored in a module, which is simply a holding place for the code, just as a worksheet is a holding place for data in cells. Double-clicking a module in Project Explorer displays the module’s code in the Code window. Editing Visual Basic code is similar to editing text in a word processing program.</p>

                <p>For more details of <a href="/excel/vba.php"> Macro and VBA Excel training</a> in Chicago and Los Angeles call us on 888.815.0604. Our classes are hands-on and instructor-led. <a href="/excel-training.php">Beginner, Intermediate and Advanced Excel classes</a> are also available.</p>


                <p><img src="/dist/images/excel/visual-basic-editor.png" alt="Figure 2-6: The Visual Basic Editor"></p>
                <p class="wp-caption-text">Figure 2-6: The Visual Basic Editor</p>

                <table class="table table-bordered table-dark table-hover table-l2">
                    <thead>
                        <tr>
                            <td><strong>Element</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Project Explorer</td>
                        <td>Contains projects that store the Visual Basic code for each open workbook. Each project can contain folders for objects (such as the worksheets in the workbook), forms, references, and modules. You select a module to view its code, copy modules to other open workbooks, and delete modules.</td>
                    </tr>
                    <tr>
                        <td>Code window</td>
                        <td>Displays the Visual Basic code for the selected module in a project.</td>
                    </tr>
                    <tr>
                        <td>Properties window</td>
                        <td>Displays specific characteristics of an object, such as the name of the object, or the standard width of the columns.</td>
                    </tr>
                    <tr>
                        <td>Standard (macro) toolbar</td>
                        <td>Displays the basic tools needed to use the Visual Basic Editor.</td>
                    </tr>
                    <tr>
                        <td>Object box</td>
                        <td>Contains a drop-down list from which to select the desired object whose code you want to view in the Code window. If General appears in the Object box, all the code for the macros associated with the selected module appears in the Code window.</td>
                    </tr>
                    <tr>
                        <td>Procedure box</td>
                        <td>Contains a drop-down list from which to select a macro to display the macro’s code in the Code window.</td>
                    </tr>
                    </tbody>
                </table>

                <h4>Steps to Edit a Macro</h4>
                <h6>To display the Visual Basic Editor:</h6>
                <ol>
                    <li>In the Code group on the Developer tab, click the Visual Basic button.</li>
                </ol>

                <h6>To edit a macro:</h6>
                <ol>
                    <li>Display the Visual Basic Editor.</li>
                    <li>From the Tools menu, choose Macros.</li>
                    <li>In the Macros dialog box, from the Macros In drop-down list, select the project containing the macro you want to edit.</li>
                    <li>In the Macro Name list box, select the desired macro.</li>
                    <li>Choose Edit.</li>
                    <li>In the Code window, make the desired edits.</li>
                    <li>Close the Macros dialog box.</li>
                </ol>

                <h6>To close the Visual Basic Editor:</h6>
                <ol>
                    <li>From the File menu, choose Close and Return to Microsoft Excel.</li>
                </ol>

                <p>Also  <a href="/excel/lessons/macros.php">see how to record a Macro</a>.</p>

                <h4>Deleting a Macro</h4>
                <p>If you no longer need a macro, you can delete it. Deleting unwanted macros makes it easier to view macros in the Code window in the Visual Basic Editor as well as view macros in the Macro dialog box. <a href="/excel-training-los-angeles.php">Excel training offered in Los Angeles</a>.</p>

                <p>You can delete a macro in an open workbook using the Macro dialog box or the Visual Basic Editor. If you want to delete a macro in the Personal Macro Workbook using the Macro dialog box, you must first unhide the Personal Macro Workbook. A benefit of using the Visual Basic Editor is that you can delete any macro in any open workbook or the Personal Macro Workbook, without unhiding it.</p>


                <h4>Steps to Delete a Macro</h4>
                <h6>Macro dialog box method:</h6>
                <ol>
                    <li>Locate the Code group in the Developer tab on the Ribbon</li>
                    <li>In the Code group on the Developer tab, click the Macros button.</li>
                    <li>In the Macro dialog box, in the Macro Name list box, select the macro you want to delete.</li>
                    <li>Choose Delete.</li>
                    <li>In the message box that appears, choose Yes.</li>
                </ol>

                <h6>Visual Basic Editor method:</h6>
                <ol>
                    <li>Locate the Code group in the Developer tab on the Ribbon.</li>
                    <li>In the Code group on the Developer tab, click the Visual Basic button.</li>
                    <li>From the Tools menu, choose Macros.</li>
                    <li>In the Macros dialog box, from the Macros In drop-down list, select the project containing the macro you want to delete.</li>
                    <li>In the Macro Name list box, select the desired macro.</li>
                    <li>Choose Delete.</li>
                    <li>From the File menu, choose Close and Return to Microsoft Excel.</li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>