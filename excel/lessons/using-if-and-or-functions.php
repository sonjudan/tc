<?php
$meta_title = "Using IF, AND, OR Functions in Microsoft Excel | Training Connection";
$meta_description = "Using IF, AND, OR Functions in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using IF, AND, OR Functions</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using IF, AND, OR Functions">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Using IF, AND, OR Functions</h1>
                        <p>In this article, we will delve a little deeper into how to Use IF, AND, OR Functions in Microsoft Excel.</p>
                        <p>For instructor-led <a href="/excel-training.php">Microsoft Excel training in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Logic operations play a big part in Excel’s functionality, especially the IF function. You can use this function to calculate different values depending on the evaluation of a condition. The structure of an IF function is as follows:</p>

                <p><img src="/dist/images/excel/IF-function.jpg" alt="Using IF Function"></p>

                <p>IF functions are called conditional functions because the return value will depend on whether or not a specific condition was satisfied. Consider the following function: IF(A1=10,5,1). If the value in A1 equals 10, then 5 is returned. Otherwise, 1 is returned.</p>
                <p>For example, in the sample worksheet that you currently have open, fill in cell B1 with this function: IF(A1=10,5,1).</p>

                <p><img src="/dist/images/excel/conditional-fucntion.jpg" alt="Conditional IF Function"></p>

                <p><a href="/excel-training-los-angeles.php">Excel formula training in Los Angeles</a>.</p>
                <p>With the formula now complete, press Enter to complete the calculation. The value returned will be 5 because the value in A1 is equal to 10:</p>

                <p><img src="/dist/images/excel/complete-calculation.jpg" alt="Formula Complete"></p>

                <p>Change the value in cell A1 to 9.9. You will see that the IF function will return a value of 1 rather than 5:</p>

                <p><img src="/dist/images/excel/change-value.jpg" alt="Results of Changed Value"></p>

                <p>Excel’s AND function can work with the IF function to find two (or more) conditions that have to be met, rather than just one. The AND function is structured like this:</p>

                <p><img src="/dist/images/excel/AND-function.jpg" alt="Using AND Function"></p>

                <p>Back to the IF statement that you just created, let’s say that you need to display “5” in cell B1 when the value in A1 is greater than or equal to 5, but less than or equal to 9. This means that the AND statement would be nested inside the IF statement, and would look like this: =IF(AND(A1>=5,A1<=9),5,1). Type this new formula into B1:</p>

                <p><img src="/dist/images/excel/IF-formula.jpg" alt="IF AND function combined"></p>

                <p>Press Enter to apply the new function. The result will immediately be calculated. In this example, because the number in A1 is greater than 9, 1 will be displayed in B1:</p>

                <p><img src="/dist/images/excel/apply-new-function.jpg" alt="New Function Applied"></p>

                <p>Change the number in A1 to 6. The number 5 will be displayed in B1 because it is greater than or equal to 5 AND less than or equal to 9:</p>

                <p><img src="/dist/images/excel/change-numbers.jpg" alt="Changes to Formula"></p>

                <p>Another function you can use with the IF function is OR. This function works similar to the AND statement, but it will be triggered when it finds one condition OR the other, rather than both. The OR function is structured like this:</p>

                <p><img src="/dist/images/excel/OR-function.jpg" alt="OR function displayed"></p>

                <p>Returning to the IF statement that you’ve been working on, let’s say that you need to display 5 in cell B1 when the value in A1 is equal to 1 OR 3. This means that the OR statement would be nested inside the IF statement, and would look like this: =IF(OR(A1=1,A1=3),5,1).</p>

                <p>Replace the formula in B1 with this formula:</p>

                <p><img src="/dist/images/excel/OR-IF-Function.jpg" alt="IF OR function combined"></p>
                <p>Press Enter to apply the new function. The result will be immediately calculated. In this example, because the number in A1 is neither equal to 1 or 3, the number 1 will be displayed in B1:</p>

                <p><img src="/dist/images/excel/enter-new-function.jpg" alt="New Function Displayed"></p>
                <p>Change the value in cell A1 to 3. You will see that the value in B1 will change to 5 because one of the two OR conditions you set in the formula have been met:</p>

                <p><img src="/dist/images/excel/change-cell-value.jpg" alt="Value Changed"></p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our 1-day Excel workshop. We run beginner, intermediate and advanced classes every month. Check our past students  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.<br>
                    </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>