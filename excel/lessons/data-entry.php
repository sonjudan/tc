<?php
$meta_title = "Excel Data Entry Techniques & Exercises | Training Connection";
$meta_description = "Learn how to enter data into Microsoft Excel Worksheets. This module is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Data Entry</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Data Entry">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Excel Data Entry</h1>
                        <h5 class="mb-1">
                            These  topics are covered  in Module 1 - Excel Essentials, which forms part of our Excel Introductory course.
                        </h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Entering Data</h4>

                <p>The first step in creating a useful worksheet is entering data. By entering data, you are inputting the information that you want Excel to display, calculate, and store. Data can be entered into a cell or a range of cells. You can even set up a sequence of data and let Excel fill in the remainder of the sequence based on your first few entries.</p>
                <p>For instructor-led Excel training class in Chicago and Los Angeles call us on (888) 815-0604.</p>

                <p><a href="/excel-training.php"> Looking for Excel training in Chicago and Los Angeles?</a></p>

                <h4>Identifying Types of Data</h4>

                <p>Excel worksheets contain four types of data: text, values, dates, and formulas. Examples of each are found in Table 1-2.</p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-1">

                    <thead>
                        <tr>
                            <td class="h6">Text</td>
                            <td class="h6">Value</td>
                            <td class="h6">Date</td>
                            <td class="h6">Formula</td>
                        </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Supplies</td>
                        <td>852.34</td>
                        <td>12/3/02</td>
                        <td>=C3+D3+E3</td>
                    </tr>
                    <tr>
                        <td>12 Dozen</td>
                        <td>42980.00254</td>
                        <td>Jan 3, 2001</td>
                        <td>=245*C3</td>
                    </tr>
                    </tbody>
                </table>
                <p class="text-center"><small>Table 1-2 : Examples of Data Types</small></p>

                <p>Text data is alphanumeric and cannot be used in most formulas. Values are numerals only. Although a date may seem to be text, as soon as you enter what Excel recognizes as a date, it is formatted and stored using a decimal date format. As a result, dates can be used in complex functions.</p>
                <p>Formulas are made up of values and <em>operators</em>. Because formulas contain references to worksheet cells and ranges, they depend on other elements of the worksheet. For instance, if a formula includes a reference to cell C3 and you change the value that is located in C3, the result of the formula will change accordingly. </p>

                <h4>Entering Text Data into a Cell</h4>

                <p>A single cell can hold up to 32,000 alphanumeric characters. If the cell is not wide enough and if the cell to the right contains data, some characters may not be visible. Excel hasn’t lost this data; it just doesn’t show it.</p>
                <p>On occasion, you may need to enter a number as text. For example, you may want to exclude a number from a summed column. If you type an apostrophe (’) before the number, for example ‘2013, Excel accepts it as text and aligns it on the left, as illustrated in Figure 1-9. All other numbers are right-aligned by default. The apostrophe does not show up in the worksheet cell, but you can see it in the Formula bar. You don’t need to type the apostrophe when a phrase begins with a number as long as it includes text characters, for example, 1st Quarter Summary.</p>

                <p>Note Excel 2013 will produce an error at this point de-noted by a green arrow in the top left corner of the cell (This error indicates inconsistent data i.e. number formatted as text). A yellow diamond with an ! in the middle will also appear with options to correct this error.</p>

                <p><img src="/dist/images/excel/excel-figure3-1.png" alt=""></p>

                <p>When you enter functional data into your worksheet (that is a calculation or part thereof; e.g. the equals sign), three buttons appear in the Formula bar to the right of the Name box, shown in Figure 1‑6. Use the Cancel button if you decide not to continue to enter the data into the cell and the Enter button to accept the entry. The Edit Formula button is only operable when the cell contains a formula. <a href="/excel-training-los-angeles.php">LA-based Excel classes</a>.</p>

                <h4>Method</h4>
                <h6>To enter text data into a cell:</h6>

                <ol>
                    <li>Select the cell.</li>
                    <li>Type the information in the cell.</li>
                    <li>Click the Enter button. <br>or</li>
                    <li>Press Enter to enter the data and move down one cell. <br>or</li>
                    <li>Select another cell.</li>
                </ol>
                <h6>To cancel data before it is entered:</h6>

                <ol>
                    <li>Click the Cancel button. <br>or</li>
                    <li>Press Esc</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will enter text data into cells.</h6>

                <ol>
                    <li>Select cell A1.</li>
                    <li>Type Brian’s Orchard.</li>
                    <li>Click the Enter button. [Brian’s Orchard appears in A1, and appears to flow into the next column. A1 is still the active cell].</li>
                    <li>Select cell A2.</li>
                    <li>Type 1st Quarter Summary.</li>
                    <li>Click cell A3. [1st Quarter Summary appears in cell A2 and cell A3 is the active cell].</li>
                    <li>Type 2013.</li>
                    <li>Click the Cancel button. [The entry is canceled].</li>
                    <li>Type '2013.</li>
                    <li>Press Enter. [2013 appears in cell A3 as text and A4 is the active cell].</li>
                    <li>Using the worksheet below, enter the headings for cells A5, B5, C5, D5, and E5.</li>
                </ol>
                <p><img src="/dist/images/excel/excel-figure3-2.png" alt=""></p>

                <h4>Entering Values</h4>
                <p>You enter values into the worksheet the same way you enter text. However, it’s important to note that values can contain only the following characters:</p>

                <p align="text-center"><pre>1 2 3 4 5 6 7 8 9 0 + - ( ) , / $ % . E e</pre></p>


                <h4>Method</h4>
                <h6>To enter values in a cell:</h6>
                <ol>
                    <li>Select the cell.</li>
                    <li>Type the value in the cell.</li>
                    <li>Click the Enter button. <br>or</li>
                    <li>Press Enter to enter the value and move down one cell. <br>or</li>
                    <li>Select another cell.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will enter values into cells.</h6>
                <ol>
                    <li>Select cell C6.</li>
                    <li>Type 65. [The range A1 to D5 is selected].</li>
                    <li>Click the Enter button. [65 appears in cell C6].</li>
                    <li>Select cell C7.</li>
                    <li>Type 35. [35 appears in cell C7].</li>
                    <li>Select cell C8.</li>
                    <li>Type 20.</li>
                    <li>Press Enter. [20 appears in cell C8].</li>
                    <li>In cell D6, enter 0.59.</li>
                    <li>In cell D7, enter 1.12.</li>
                    <li>In cell D8, enter -0.25.</li>
                </ol>

                <h4>Entering Data into a Range</h4>
                <p>Selecting an entire worksheet is useful when you want to make changes on a global scale. For instance, you might want to change the size of the font in every cell in the worksheet. Once you select the entire worksheet using the Select All button, illustrated in Figure 1-4, you can do this in a single step.</p>



                <h4>Method</h4>
                <h6>In the following exercise, you will enter data into a range.</h6>
                <ol>
                    <li>Select the desired range.</li>
                    <li>Type the information into the first cell.</li>
                    <li>Press Enter to move to the next cell.</li>
                    <li>Type the appropriate information.</li>
                    <li>Repeat steps 3 and 4 until all information is entered.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will enter data into a range.</h6>
                <ol>
                    <li>Select the range B6:B10. [The range is selected, and B6 is the active cell.</li>
                    <li>Type Apple Butter.</li>
                    <li>Press Enter. [B7 is the active cell].</li>
                    <li>Type Peach Jam.</li>
                    <li>Press Enter. [B8 is the active cell].</li>
                    <li>Type Free Sample.</li>
                    <li>Press Enter. [B9 is the active cell].</li>
                    <li>Type Pear Butter.</li>
                    <li>Press Enter. [B10 is the active cell].</li>
                    <li>Type Roger’s Peanuts.</li>
                    <li>Press Enter. [B6 is the active cell].</li>
                    <li>Select the range C9:D10. [The range C9:D10 is selected. The active cell is C9].</li>
                    <li>In cell C9, type 44, and then press Enter. [Cell C10 becomes active].</li>
                    <li>In cell C10, enter 114, and then press Enter. [Cell D9 becomes active].</li>
                    <li>In cell D9, enter 0.69, and then press Enter. [Cell D10 becomes active].</li>
                    <li>In cell D10, enter 8.99, and then press Enter. [Cell C9 becomes active again].</li>
                </ol>


            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Microsoft Excel</a><br></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Excel Workbooks</a><br></li>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Interface</a><br></li>
                        <li><a href="/excel/lessons/excel-selections.php">Selections in Excel</a><br></li>
                        <li><a href="/excel/lessons/columns-saving.php">Saving Files</a><br></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Closing Excel </a><br></li>
                        <li><a href="/excel/lessons/building-formulas.php">Using Formulas in Excel</a><br></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a><br></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel training courses</h4>
                    <p>Do you know that most intructor-led Excel classes are actually webinars? At Training Connection we believe that learning from a face-to-face instructor is still the best way to learn Microsoft Excel. Please read our <a href="/testimonials.php?course_id=19">past student reviews</a> we offer.<br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a><br></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a><br></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a><br></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>