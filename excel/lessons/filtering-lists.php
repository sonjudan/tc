<?php
$meta_title = "Filtering lists in Microsoft Excel | Training Connection";
$meta_description = "Learn to filter lists in Microsoft Excel. This course is part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Filtering Lists</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Filtering a list in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Filtering a list in Excel</h1>
                        <h5>These topics are covered in Module 1 - Working with Lists, part of our <a href="/excel-training.php">Advanced Excel course</a>.</h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Filtering can give you more control over your list, particularly if your list contains a large number of records. For example, suppose you operate a small grocery store and have a master inventory of all the items in the store. Your list would include everything from dairy products to fresh vegetables to cookies. What if you suddenly needed to know how many types of cheese were on the shelf? You could scroll through the entire list, counting the cheeses as you go, but it would make more sense to filter the list so that it displays only dairy products, or better yet, only cheeses.</p>
                <p>Filtering a list lets you find and work with a subset of the data in your list by displaying only the records that contain a certain value or meet specific criteria. The remaining records are hidden from view until you instruct Excel to display them again. You can then copy this filtered list to another location without disturbing the primary source list. <a href="/excel-training-chicago.php">Chicago MS Office classes</a>.</p>

                <h4>Using AutoFilter</h4>
                <p>Choosing AutoFilter from the Sort and Filter group on the Home tab applies drop-down arrows directly to column labels in your list, as illustrated in Figure 1-13. You then select a specific value from one of the drop-down lists to display all records in your list containing that value. For example, you can select Ford from the MAKE column drop-down list to display only those records for Ford cars.</p>

                <p>For instructor-led Microsoft Excel classes in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p><img src="/dist/images/excel/excel3-figure1-13.png" alt="Figure 1-13: Auto Filter Dropdown Arrows"></p>
                <p class="wp-caption-text">Figure 1-13: Auto Filter Dropdown Arrows</p>

                <h4>Method</h4>
                <h6>To filter a list using AutoFilter:</h6>
                <ol>
                    <li>Select a cell in the list you want to filter.</li>
                    <li>In the Sort and Filter group on the Home tab, click the Filter button.</li>
                    <li>In the desired column heading cell, from the AutoFilter drop-down list, select the desired items by selecting/deselecting.</li>
                    <li>Repeat step 3 to filter the list by other columns.</li>
                </ol>

                <h6>To deactivate AutoFilter:</h6>
                <ol>
                    <li>In the Sort and Filter group on the Home tab, click the lit-up Filter button.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will filter a list.</h6>
                <ol>
                    <li>Make sure a cell in the Cars worksheet list is selected.</li>
                    <li>In the Sort and Filter group on the Home tab, click the Filter button. [The Filter is switched on and the Filter button is illuminated].</li>
                    <li>Look at the titles of the list of data. [The AutoFilter drop-down arrows appear in the headings of the list].</li>
                    <li>From the MAKE AutoFilter drop-down list, deselect Select All and click to select Ford. [Only records that contain Ford in the Make column are visible. All other records are hidden. The MAKE AutoFilter drop-down arrow and the row headings of the visible records are blue].</li>
                    <li>From the DOORS AutoFilter drop-down list, deselect Select All and click to select 2. [Only Ford cars with 2 doors are visible. The DOORS AutoFilter drop-down arrow is also blue].</li>
                    <li>From the MAKE AutoFilter drop-down list, select only Chevy. [Only Chevy cars with 2 doors are visible].</li>
                    <li>In the Sort and Filter group on the Home tab, click the Filter button. [The Filter dropdown arrows disappear].</li>
                    <li>Save and close the workbook.</li>
                </ol>


                <h4>Custom AutoFilter Criteria</h4>
                <p>The Custom AutoFilter command lets you customize the criteria, by which you filter your list, using one or two comparison criteria. This is similar to searching for specific records using the data form; however, all your results are displayed in the worksheet, as opposed to scrolling through the records. Another advantage of using a custom AutoFilter is that you can display records that meet one criterion or the other.</p>
                <p>For example, you can create a list of all employees with salaries greater than $40,000 and less than $55,000 or a list of all employees with salaries greater than $60,000 or less than $20,000. You specify custom criteria using comparison operators in text form, such as equals, in the Custom AutoFilter dialog box, shown in Figure 1-14. Note that in Figure 1-15 the Custom AutoFilter is using text operators as criteria as opposed to numeric ones. The Custom AutoFilter options are context sensitive and the criteria options will reflect the data in the list column being filtered.</p>


                <div class="row">
                    <div class="col-lg-6">
                        <p><img src="/dist/images/excel/excel3-figure1-14.png" alt="Figure 1-14: The Custom AutoFilter Dialog Box (numeric criteria)"></p>
                        <p class="wp-caption-text">Figure 1-14: The Custom AutoFilter Dialog Box (numeric criteria)</p>
                    </div>
                    <div class="col-lg-6">
                        <p><img src="/dist/images/excel/excel3-figure1-15.png" alt="Figure 1-15: The Custom AutoFilter Dialog Box (Text criteria)"></p>
                        <p class="wp-caption-text">Figure 1-15: The Custom AutoFilter Dialog Box (Text criteria)</p>
                    </div>
                </div>


                <h4>Method</h4>
                <h6>To customize AutoFilter criteria:</h6>
                <ol>
                    <li>Select a cell in the list you want to filter.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Filter button.</li>
                    <li>In the column with which you want to filter the data, from the AutoFilter drop-down list, select (Number Filters...).</li>
                    <li>In the next drop-down menu, select Custom Filter.</li>
                    <li>In the Custom AutoFilter dialog box, in the Show rows where: area, from the first comparison operator drop-down list, select an operator.</li>
                    <li>In the value drop-down combo box, type or select a value.</li>
                    <li>To display records that meet two criteria, select the And or Or option button, and then repeat steps 5 and 6 for the second comparison operator drop-down list box and value drop-down combo box.</li>
                    <li>Choose OK.</li>
                </ol>

                <p><img src="/dist/images/excel/excel3-figure1-15A.png" alt="Figure 1-15A: Naviating to the Custom Auto Filter Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-15A: Naviating to the Custom Auto Filter Dialog Box</p>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will customize AutoFilter criteria.</h6>
                <ol>
                    <li>Open the Fairfield Group workbook, and, if necessary, select the Canadian Operations worksheet.</li>
                    <li>Select a cell in the list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Filter button. [The AutoFilter drop-down arrows appear].</li>
                    <li>From the Salary AutoFilter drop-down list, select (Number Filters...). [Another drop-down menu appears with number Filters options].</li>
                    <li>In the next drop-down menu, select Custom Filter. [The Custom AutoFilter dialog box appears].</li>
                    <li>In the Show rows where area, from the first comparison operator drop-down list, select is greater than or equal to.</li>
                    <li>In the adjacent value drop-down combo box, type 40000.</li>
                    <li>Choose OK. [Only the records in which the salary Is greater than or equal to 40,000 are displayed. The Salary AutoFilter drop-down arrow becomes a filter symbol].</li>
                    <li>From the Salary AutoFilter drop-down list, click to select (Select All) and click OK. [All records are visible, and the Salary AutoFilter black drop-down arrow is back (filter icon removed)].</li>
                    <li>From the Dept AutoFilter drop-down list, navigate to Custom Filter (note - text filter). [The Custom AutoFilter dialog box appears].</li>
                    <li>In the first comparison operator drop-down list box, make sure equals is selected.</li>
                    <li>From the adjacent value drop-down list, select Investigation.</li>
                    <li>Select the Or option button.</li>
                    <li>From the second comparison operator drop-down list, select equals.</li>
                    <li>From the adjacent value drop-down list, select Marketing, and then choose OK. [Only the records for employees in the Investigation or Marketing department are displayed].</li>
                    <li>From the Salary AutoFilter drop-down list, navigate to Custom Filter. [The Custom AutoFilter dialog box appears].</li>
                    <li>From the first comparison operator drop-down list, select is greater than or equal to.</li>
                    <li>In the adjacent value drop-down combo box, type 40000.</li>
                    <li>Choose OK. [Only the records for employees in the Investigation or Marketing department with salaries greater than or equal to $40,000 are displayed, as shown in Figure 1-15B].</li>
                </ol>

                <p><img src="/dist/images/excel/excel3-figure1-15B.png" alt="Figure 1-15B: The Filtered List (note filter icons next to the column labels on which the data is filtered)"></p>
                <p class="wp-caption-text">Figure 1-15B: The Filtered List (note filter icons next to the column labels on which the data is filtered)</p>

                <h4>Using the Top 10 Feature</h4>
                <p>Another option available from each AutoFilter drop-down list is the Top 10 feature. When you filter a list using the Top 10 feature, only the top number or the top percent of records remain. You can also filter to display the bottom number or the bottom percent of records. For example, if you want to list the top wage earners in the company, you can filter the Salary column to display only those records with the top ten salaries. If you filter for the top ten percent of wage earners, however, your list would include only those personnel whose salaries together equaled ten percent of the total.</p>

                <p>Although called Top 10, you can filter for any number or percentage of items you desire. You make your selections for this feature using the Top Ten AutoFilter dialog box, shown in Figure 1-16.</p>

                <p><img src="/dist/images/excel/excel3-figure1-16.png" alt="Figure 1-16: The Top 10 AutoFilter Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-16: The Top 10 AutoFilter Dialog Box</p>

                <h4>Method</h4>
                <h6>To use the Top 10 feature:</h6>
                <ol>
                    <li>If necessary, activate AutoFilter for the desired list.</li>
                    <li>In the column with which you want to filter the data, from the AutoFilter drop-down list, select (Number Filters...). In the next drop-down menu, select (Top 10...).</li>
                    <li>In the Top 10 AutoFilter dialog box, from the first drop-down list, select Top or Bottom.</li>
                    <li>In the spin box, enter or select the desired number.</li>
                    <li>From the second drop-down list, select Items or Percent.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will use the Top 10 feature.</h6>
                <ol>
                    <li>Click a cell in the list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Clear button. [The full list of data records is displayed].</li>
                    <li>From the Salary AutoFilter select (Number Filters...), then in the next drop-down list, select (Top 10...). [The Top 10 AutoFilter dialog box appears].</li>
                    <li>Make sure the following appear in the first drop-down list box, the spin box, and the second drop-down list box: Top 10 Items</li>
                    <li>Choose OK. [Only the records of employees with the top ten salaries are visible].</li>
                    <li>Display the full list.</li>
                    <li>From the Salary AutoFilter drop-down list, select (Top 10...). [The Top 10 AutoFilter dialog box appears].</li>
                    <li>Select the following in order in the first drop‑down list box, the spin box, and the second drop-down list box: Bottom 15 Percent</li>
                    <li>Choose OK. [Only the records of employees whose salaries make up the bottom 15 percent are visible].</li>
                    <li>Display the full list.</li>
                    <li>Using the Top 10 AutoFilter feature, filter the list for the top 15 salaries. [Only the records of employees with the top fifteen salaries are visible].</li>
                </ol>

                <h4>Performing an Advanced Filter</h4>
                <p>You use the Advanced Filter command when you need to set more than two criteria or when you need to use a formula for computed criteria. You must create a criteria range before you can use the Advanced Filter command.</p>
                <p>A criteria range is a range of cells that Excel uses to filter a list. The criteria range can be located anywhere on the worksheet outside of the list, and must contain some or all of the list’s field names and the desired criteria in the rows below them. The criteria range may span several rows, depending on compound criteria requirements. For example, to find records that meet criteria A and criteria B, enter the criteria in one row. To find records that meet criteria A or criteria B, enter the criteria in separate rows.</p>
                <p>You enter the list range and the criteria range in the Advanced Filter dialog box, shown in Figure 1-17. A sample criteria range is shown in Figure 1-18.</p>

                <p><img src="/dist/images/excel/excel3-figure1-17.png" alt="Figure 1-17: The Advanced Filter Dialog Box"></p>
                <p class="wp-caption-text">Figure 1-17: The Advanced Filter Dialog Box</p>


                <p><img src="/dist/images/excel/excel3-figure1-18.png" alt="Figure 1-18: A Sample Criteria Range"></p>
                <p class="wp-caption-text">Figure 1-18: A Sample Criteria Range</p>

                <h4>Method</h4>
                <h6>To perform an advanced filter:</h6>
                <ol>
                    <li>Copy the column headings you want to use for the criteria to an empty range on the worksheet.</li>
                    <li>Below the appropriate copied column headings, enter the criteria.</li>
                    <li>Select a cell in the list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Filter button.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Advanced Filter button.</li>
                    <li>In the Advanced Filter dialog box, in the Action area, select the Filter the list, in-place option button.</li>
                    <li>In the List range text box, enter the desired list range.</li>
                    <li>In the Criteria range text box, enter the range that includes the copied column headings and the criteria.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will perform an advanced filter.</h6>
                <ol>
                    <li>Select the American Operations worksheet.</li>
                    <li>Select the range A8:J8.</li>
                    <li>Copy the range to A3:J3.</li>
                    <li>In cell E4, enter Corporate. [The first criterion is set].</li>
                    <li>In cell E5, enter Investigations. [The second criterion is set].</li>
                    <li>In cell F4, enter Sales. [The third criterion is set].</li>
                    <li>Select any cell in the list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Filter button. [The Filter submenu arrows appear in headings].</li>
                    <li>In the Sort and Filter group on the Data tab, click the Advanced Filter button. [The list is selected, and the Advanced Filter dialog box appears].</li>
                    <li>In the Action area, make sure the Filter the list, in-place option button is selected.</li>
                    <li>In the List range text box, make sure $A$8:$J$48 is displayed.</li>
                    <li>In the Criteria range text box, type A3:J5.</li>
                    <li>Choose OK. [Only the records of employees in the Sales department and in the Corporate or Investigations division are visible].</li>
                    <li>Display the full list.</li>
                </ol>

                <h4>Copying Filtered Data to Another Location</h4>
                <p>When you filter data, Excel displays only those records that meet the filter criteria, and hides the records that do not meet the filter criteria. When performing an advanced filter, you can also copy your filtered data to another location on your worksheet, to let you compare your filtered list to your complete list.</p>
                <p>To copy all the filtered data to another location, you click the Copy to text box, and then enter the upper left cell of the range to which you want to copy the filtered list. If the location to which you choose to copy the data contains any data, you will get an error message.</p>

                <h4>Method</h4>
                <h6>To copy filtered data to another location:</h6>
                <ol>
                    <li>Create the advanced filter criteria range.</li>
                    <li>Select a cell in the list.</li>
                    <li>From the Data menu, choose Filter.</li>
                    <li>From the Filter submenu, choose Advanced Filter.</li>
                    <li>In the Advanced Filter dialog box, in the Action area, select the Copy to another location option button.</li>
                    <li>In the List range text box, enter the desired list range.</li>
                    <li>In the Criteria range text box, enter the range that includes the copied column headings and the criteria.</li>
                    <li>In the Copy to text box, enter the upper left cell of the range to which to copy the filtered list.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will copy filtered data to another location.</h6>
                <ol>
                    <li>Make sure the American Operation worksheet is active.</li>
                    <li>In cell E4, enter Government. [The first criterion is set].</li>
                    <li>In cell H4, enter >50000. [The second criterion is set].</li>
                    <li>Delete the data in cells E5 and F4.</li>
                    <li>Select a cell in the list.</li>
                    <li>In the Sort and Filter group on the Data tab, click the Filter button. [The Filter submenu appears].</li>
                    <li>In the Sort and Filter group on the Data tab, click the Advanced Filter button. [The Advanced Filter dialog box appears].</li>
                    <li>In the Action area, select the Copy to another location option button.</li>
                    <li>In the List range text box, make sure the range $A$8:$J$48 is displayed.</li>
                    <li>In the Criteria range text box, change the existing range to $A$3:$J$4.</li>
                    <li>In the Copy to text box, enter A50.</li>
                    <li>Choose OK. [A copy of the filtered data appears below the list].</li>
                    <li>Scroll to the bottom of the list to view the data. [Only one employee is listed].</li>
                    <li>Save the workbook.</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/creating-lists.php">Creating a List</a></li>
                        <li><a href="/excel/lessons/maintaining-lists.php">Maintaining a List</a></li>
                        <li><a href="/excel/lessons/editing-records.php">Editing Records Using the Data Form</a></li>
                        <li><a href="/excel/lessons/subtotals-in-lists.php">Using Subtotals in a List</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel Training Reviews</h4>
                    <p> We train thousands of students each year how to effectively use Excel. View a sample of our <a href="/testimonials.php?course_id=19">Excel training reviews</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>