<?php
$meta_title = "Using Basic Functions in Excel | Training Connection";
$meta_description = "This tutorial will teach you use basic functions such as SUM and AVERAGE in Microsoft Excel. This is covered as part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Basic Functions</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Basic Functions in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Using Basic Functions in Microsoft Excel</h1>

                        <p>This tutorial introduces Excel functions, which are a little like templates for common formulas. There are many different types of functions. First, we will look at the SUM function. You will learn about using AutoComplete for entering formulas. We'll look at other basic common functions such as AVERAGE. We'll take a look at the Formulas tab introduced in the Ribbon.</p>

                        <p>For more on <a href="/excel-training.php">MS Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Formulas vs. Functions</h4>

                <p>To open the Insert Function dialog box, use the following procedure.</p>
                <ol>
                    <li>Select the Insert Function tool right next to the Formula Bar.</li>
                </ol>

                <p><img class="" src="/dist/images/excel/excel-9.png" alt=""></p>

                <p>Investigate the different categories and functions in the Insert Function dialog box. Point out the bottom part of the screen where the syntax and description of the function appear.</p>

                <p><img class="" src="/dist/images/excel/excel-10.png" alt=""></p>

                <h4>Using the SUM Function</h4>
                <p>Review how to use a SUM function to add the total for each category in the sample file, use the following procedure.</p>

                <ol>
                    <li>Select the Total –First Six Months column for the first category (cell H5).</li>
                    <li>Select the AutoSum tool in the Editing Group on the Home tab of the Ribbon.</li>
                </ol>
                <p><img class="" src="/dist/images/excel/excel-11.png" alt=""></p>
                <ol star="3">
                    <li>Excel enters the function with a default selection of the cell references you want to use in the function highlighted.</li>
                </ol>
                <p><img class="" src="/dist/images/excel/excel-12.png" alt=""></p>
                <ol star="4">
                    <li>If the cell references are not accurate, you can drag the highlighted area to include additional cells or remove cells you do not want used in the function.</li>
                    <li>Press ENTER to complete the function.</li>
                </ol>
                <p>Excel performs the calculation and moves to the next row. In the following illustration, the cell with the function is active, so that you can see the function syntax in the Formula Bar and the result in the cell. <a href="/excel-training-los-angeles.php">Excel training offered in Los Angeles</a>.</p>
                <p><img class="" src="/dist/images/excel/excel-13.png" alt=""></p>


                <h4>Using AutoComplete</h4>
                <p>To use the AutoComplete feature, use the following procedure.</p>
                <ol>
                    <li>Begin typing the SUM function. As soon as you type the Equals sign and the letter S, Excel displays a possible list of matching functions.</li>
                </ol>
                <p><img class="d-inline-block" src="/dist/images/excel/excel-14.png" alt=""></p>
                <ol start="2">
                    <li>To select the SUM Function from the list, double-click on the SUM function.</li>
                    <li>Excel enters the function, but you must still enter the arguments. You can simply click on multiple cells, or click and drag to select a cell range. You can also type in the cell references.</li>
                </ol>
                <p><img class="d-inline-block" src="/dist/images/excel/excel-15.png" alt=""></p>

                <ol start="2">
                    <li>Enter the final parenthesis mark to end the function.</li>
                    <li>Press ENTER to enter the function in the cell.</li>
                </ol>

                <h4>Using Other Basic Excel Functions</h4>
                <p>Review how to use the AVERAGE function as an example of another function, use the following procedure.</p>


                <ol>
                    <li>Add a new label in column I: Average.</li>
                    <li>Select the cell in the Average column for the first category.</li>
                    <li>Select the arrow next to the SUM function on the Home tab of the Ribbon to see the list of other common functions.</li>
                </ol>
                <p><img class="d-inline-block" src="/dist/images/excel/excel-16.png" alt=""></p>

                <div class="clearfix"></div>

                <ol start="4">
                    <li>Select Average.
                        Excel enters the function with the most likely cell references.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/excel-17.png" alt=""></p>

                <ol start="5">
                    <li>Replace the cell references so that cell H5 is not included in the average.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/excel-18.png" alt=""></p>

                <ol start="6">
                    <li>Press ENTER to complete the function.</li>
                </ol>




            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/excel-interface.php">Excel Interface</a></li>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Excel</a></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Workbooks</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Excel</a></li>
                        <li><a href="/excel/lessons/excel-selections.php">Making Selections in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Adjusting Column Widths </a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting MS Excel </a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Formulas</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas </a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>