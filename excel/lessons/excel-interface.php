<?php
$meta_title = "The Excel Interface - Excel Training | Training Connection";
$meta_description = "Learn the Excel Interface and all its components. This course is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Excel Interface</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Understanding the Excel Interface">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Understanding the Excel Interface</h1>
                        <h5>These topics are covered in Module 1 - Excel Essentials, part of our <a href="/excel-training.php">Beginner Excel course</a>.</h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Identifying Parts of the Screen</h4>

                <p>The Excel window is illustrated in Figure 1-4. The bulk of the screen is occupied by the worksheet window. This grid provides a convenient workspace where you can enter and manage your data. Surrounding the worksheet window are several command interfaces, each of which allows you to receive information about, or apply functions to, the data on the worksheet.</p>
                <p>For instructor-led Excel training class in Chicago and Los Angeles call us on (888) 815-0604.</p>

                <p><img src="/dist/images/excel/screen-excel-tooltip.png" alt=""></p>

                <h4>The Parts of the Excel Window</h4>

                <table class="table table-bordered table-dark table-hover table-l2">
                    <tbody>
                        <tr class="tr">
                            <th scope="row" class="w-td-md">Worksheet control buttons</th>
                            <td>The worksheet control buttons allow you to minimize, maximize/restore, or close the worksheet.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Formula bar</th>
                            <td>The formula bar allows you to enter and edit data in the worksheet.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Worksheet window</th>
                            <td>The worksheet window contains the open, active worksheet. Worksheet windows can be moved and sized. You can have more than one worksheet window open at the same time.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Scroll bars</th>
                            <td>The vertical and horizontal scroll bars enable you to move quickly through the worksheet, vertically and horizontally.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Ribbon</th>
                            <td>The Ribbon is designed to help you quickly find the commands that you need to complete a task. Commands are organized in logical groups, which are collected together under tabs. Each tab relates to a type of activity, such as writing or laying out a page. To reduce clutter, some tabs are shown only when needed.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Title bar <span class="text-light">(extreme top right of the Excel window)</span></th>
                            <td>The title bar contains the name of the application and the active file.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">File Button</th>
                            <td>
                                Clicking on the File Button displays a drop down menu containing a number of options, such as open, save, and print.
                                <br>
                                The options in the File Button menu are very similar to options found under the Office Button and File menu in previous versions of Excel.
                            </td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Sheet tabs <span class="text-light">(Worksheet tabs)</span></th>
                            <td>The sheet tabs let you switch between worksheets in a workbook by clicking the appropriate tab.</td>
                        </tr>
                        <tr class="tr">
                            <th scope="row">Quick Access Toolbar</th>
                            <td>The Quick Access Toolbar (see Figure 1-5) is a customizable toolbar that contains a set of commands that are independent of the tab that is currently displayed. You can move the Quick Access Toolbar from one of the two possible locations, and you can add buttons that represent commands to the Quick Access Toolbar.</td>
                        </tr>
                    </tbody>
                </table>


                <h5>The Ribbon</h5>

                <p>The Ribbon is the strip of buttons and icons, organised into TABS, located above the work area in Excel 2013. The Ribbon (with its various tabs) replaces the menus and toolbars found in earlier versions of Excel. At the top of the actual Ribbon are a number of tabs, such as Home, Insert, and Page Layout. Clicking on a tab displays the options located in this section of the ribbon. <a href="/excel-training-chicago.php">Excel training in the Loop</a>.</p>

                <p>For example, when Excel 2013 opens, the options under the Home tab are displayed (see image to the right). These options are grouped according to their function - such as Clipboard (includes cut, copy, and paste options), and Font (includes current font, font size, bold, italic, and underline options).</p>

                <p>Clicking o n an option on the ribbon may lead to further options contained in a Contextual Menu that relate specifically to the option chosen.</p>

                <p><img src="/dist/images/excel/screen-excel-tools.png" alt=""></p>

                <p>Although, as written in the label for Figure 1-4, tab sections can be referred to as [tab name] ribbon, like Home Ribbon, Insert Ribbon, etc, tab sections can also be referred to as only their tab names. Eg. the Home tab, the Insert tab, the Page Layout tab, etc.</p>

            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


<div class="section-widget g-text-excel" data-aos="fade-up" >
    <div class="container">

        <div class="widget-row">
            <div class="widget">
                <h4 class="widget-title">Related Excel Lessons</h4>
                <ul>
                    <li><a href="#">Starting Excel</a></li>
                    <li><a href="#">Opening Workbooks</a></li>
                    <li><a href="#">Data Entry in Excel</a></li>
                    <li><a href="#">Making Selections in Excel</a></li>
                    <li><a href="#">Adjusting Column Widths</a></li>
                    <li><a href="#">Printing and Exiting MS Excel</a></li>
                    <li><a href="#">Cheat sheets</a></li>
                </ul>
            </div>
            <div class="widget">
                <h4 class="widget-title">Group Excel training</h4>
                <p>Through our network of local trainers we deliver onsite group Excel training right across the country. View our
                    <a href="#">testimonials</a> or <a href="#">obtain a quote for a customized Excel class</a>.</p>
            </div>
            <div class="widget">
                <h4 class="widget-title">Excel courses</h4>
                <ul>
                    <li><a href="#">Excel Level 1 - Introduction</a></li>
                    <li><a href="#">Excel Level 2 - Intermediate</a></li>
                    <li><a href="#">Excel Level 3 - Advanced</a></li>
                    <li><a href="#">Excel Level 4 - Macros and VBA</a></li>
                    <li><a href="#">Get Certified in Excel</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>