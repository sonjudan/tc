<?php
$meta_title = "Creating Lists in Microsoft Excel | Training Connection";
$meta_description = "Learn to create lists in Microsoft Excel. This course is part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Creating Lists</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Creating a list in Excel">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Creating a list in Excel</h1>
                        <h5>These  topics are covered  in Module 1 - Working with Lists, part of our <a href="/excel/advanced.php">Advanced  Excel course.</a></h5>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <p>A list is a sequence of rows of related data. You can use lists whenever you need to organize large amounts of similar data, such as a database of names and addresses. You create a list in much the same way you create a worksheet. You enter information into a list by entering data into cells. Although you can change list elements after you have created a list, it is best to spend time planning your list before you begin entering data.</p>
                <p>You plan your list by determining the column labels you want to include, the type of desired output, and how you might want to report or sort information in your database. For example, if you need to sort the list by last name, be sure to include a separate field for last names.</p>

                <h4>Identifying the Parts of a List</h4>
                <p>A list consists of records and fields. A record is a set of related data that corresponds to a row in the list. It contains alphanumeric data that, for example, might refer to the name, address, zip code, and telephone number of one person. Records are organized into fields according to the columns in your list and named according to the column labels. For example, you might name a field Last Name, Address 2, or Phone Number. The parts of a list are shown in Figure 1-1.</p>

                <p>Each column of information in a list must have a column label as Excel uses these column labels as field names. You must start your first row of data directly below the column labels. If you don't, Excel won't recognize the column labels as belonging to the list. If you need to differentiate the column labels from the data, underline the labels or use a border along the bottom of the row of column labels.</p>

                <p>For instructor-led MS Excel classes in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p><img src="/dist/images/excel/excel3-figure1-1.png" alt="Figure 1-1: Parts of a List"></p>
                <p class="wp-caption-text">Figure 1-1: Parts of a List</p>

                <h4>Entering Column Labels in a List</h4>
                <p>Once you have decided which fields you need and how you would like them displayed in your list, you can enter the column labels. You enter column labels into the worksheet the same way you would enter text into a worksheet cell. <a href="/excel-training-chicago.php">Chicago Excel 2019 training</a>.</p>

                <h6>Remember the following rules when creating column labels:</h6>
                <ol>
                    <li>Use only alphanumeric characters.</li>
                    <li>Column labels must be in the row directly above the data.</li>
                    <li>Column labels can be up to 32,767 characters long, but shorter names are easier to use.</li>
                    <li>Column labels must be unique.</li>
                    <li>Don’t put blank rows or dashed lines under the column labels.</li>
                    <li>Don’t use labels that look like range names, formulas, or cell addresses.</li>
                </ol>

                <h4>Method</h4>
                <h6>To enter column labels in a list:</h6>
                <ol>
                    <li>In the first cell of the first row of the list, enter a column label.</li>
                    <li>Move one cell to the right.</li>
                    <li>Enter the second column label.</li>
                    <li>Repeat steps 2 and 3 until finished.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will enter column labels in a list.</h6>

                <ol>
                    <li>Start Excel. [Excel loads, and Book 1 opens. Sheet 1 is the active sheet].</li>
                    <li>
                    In Sheet 1, in cell A2:H2, type the column labels listed below, in order, formatting the cells in Arial, 12 points, bold
                    <br>
                    ID NO
                    <br>
                    MAKE
                    <br>
                    MODEL
                    <br>
                    DOORS
                    <br>
                    AUTO
                    <br>
                    CONVRT
                    <br>
                    IN
                    <br>
                    RATE
                    </li>
                    <li>If necessary, adjust the column widths of the worksheet so that you can clearly see all the field names.</li>
                    <li>Save the workbook as Auto Research.</li>
                </ol>

                <p><img src="/dist/images/excel/excel3-figure1-2.png" alt="Figure 1-2: List Data"></p>
                <p class="wp-caption-text">Figure 1-2: List Data</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/maintaining-lists.php">Maintaining a List</a></li>
                        <li><a href="/excel/lessons/editing-records.php">Editing Records Using the Data Form</a></li>
                        <li><a href="/excel/lessons/filtering-lists.php">Filtering a List</a></li>
                        <li><a href="/excel/lessons/subtotals-in-lists.php">Using Subtotals in a List</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Using Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">How to Edit and Copy Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel reviews</h4>
                    <p>Live face-to-face training is the most effective way to learn Microsoft Excel. We have trained thousands of students how to effectively use Excel. Click on the following link to view a sample of our <a href="/testimonials.php?course_id=19">past student testimonials</a>. </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>