<?php
$meta_title = "Sharing Workbooks in Microsoft Excel | Training Connection";
$meta_description = "Learn to share workbooks, track changes and merge workbooks. This and other topics are covered on our instructor-led Excel Training classes, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Sharing Workbooks</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Sharing a book">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Sharing a Workbook</h1>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h6>To share a workbook, use the following procedure.</h6>
                <ol>
                    <li>Select the Review tab from the Ribbon.</li>
                    <li>Select Share Workbook.</li>
                </ol>

                <p><img src="/dist/images/excel/share-workbook-ribbon.png" alt="share-workbook-ribbon"></p>


                <p class="alignright"><img src="/dist/images/excel/share-workbook-dialogue-box.png" alt="share-workbook-dialogue-box"></p>

                <ol start="3">
                    <li>In the Share Workbook dialog box, check the Allow changes by more than one user at the same time box.</li>
                    <li>Select OK.</li>
                    <li>In the Save As dialog box, enter a new File Name, if desired. Navigate to the location where you want to save the workbook and select Save.</li>
                </ol>

                <p>Note that if there are links in the workbook, you may need to verify them. Now you can email the people who will share the workbook a link to the file or indicate the location of the file.</p>
                <p>
                For <a href="/excel-training.php">Microsoft Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Editing a Shared Workbook</h4>
                <h6>To edit a shared workbook, use the following procedure.</h6>
                <ol>
                    <li>Open the shared workbook.</li>
                    <li>Make any necessary changes.</li>
                    <li>Save the workbook.</li>
                </ol>

                <h6>You can see who else has the workbook open on the Editing tab of the Share Workbook dialog box.</h6>
                <ol>
                    <li>Select the Review tab from the Ribbon.</li>
                    <li>Select Share Workbook.</li>
                </ol>

                <p>Select the Advanced tab of the Share Workbook dialog box to choose to get automatic updates of the other users' changes periodically, with or without saving. <a href="/excel-training-los-angeles.php">Hands-on Excel classes in Downtowm Los Angeles</a>.</p>

                <p><img class="d-inline-block" src="/dist/images/excel/share-workbook-dialogue-box-advanced-tab.png" alt="share-workbook-dialogue-box-advanced-tab"></p>

                <h4>Tracking Changes</h4>
                <h6>To turn on change tracking for a workbook, use the following procedure.</h6>
                <ol>
                    <li>Select the Review tab from the Ribbon.</li>
                    <li>Select Share Workbook.</li>
                    <li>Select the Advanced tab of the Share Workbook dialog box.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/share-workbook-save.png" alt="share-workbook-save"></p>

                <ol start="4">
                    <li>Excel keeps the change history for a default of 30 days. You can change the number of days.</li>
                    <li>Select OK.</li>
                </ol>

                <h6>To review the Highlight Changes dialog box, use the following procedure.</h6>
                <ol>
                    <li>Select the Review tab from the Ribbon.</li>
                    <li>Select Track Changes. Select Highlight Changes.</li>
                </ol>

                <p><img src="/dist/images/excel/menu-highlight-changes.png" alt="menu-highlight-changes"></p>

                <ol start="3">
                    <li>In the Highlight Changes dialog box, you can select when to highlight changes, whose changes to highlight and where to highlight the changes. Select OK when you have finished making your selections.</li>
                </ol>

                <p class=""><img class="d-inline-block" src="/dist/images/excel/highlight-changes.png" alt="highlight-changes"></p>

                <div class="clearfix"></div>

                <h4>Merging Copies of a Shared Workbook</h4>
                <p>To add the Compare and Merge Workbooks command to the Quick Access toolbar, use the following procedure.</p>

                <ol>
                    <li>Select the arrow next to the Quick Access Toolbar.</li>
                    <li>Select More Commands from the menu.</li>
                </ol>

                <p class=""><img class="d-inline-block" src="/dist/images/excel/More-commands.png" alt="More-commands"></p>

                <ol start="3">
                    <li>In the Excel Options dialog box, select All Commands from the Choose Commands From drop down list.</li>
                    <li>Highlight Compare and Merge Workbooks in the left list.</li>
                    <li>Select Add.</li>
                </ol>

                <p class=""><img class="d-inline-block" src="/dist/images/excel/compare-merge.png" alt="compare-merge"></p>

                <ol start="6">
                    <li>Select OK.</li>
                </ol>

                <h6>To compare and merge workbooks, use the following procedure.</h6>
                <ol>
                    <li>Open the copy of the shared workbook where you want to merge the changes.</li>
                    <li>On the Quick Access Toolbar, click Compare and Merge Workbooks.</li>
                </ol>

                <p class=""><img class="d-inline-block" src="/dist/images/excel/menu.png" alt="menu"></p>

                <ol start="3">
                    <li>Save the workbook if prompted.</li>
                    <li>In the Select Files to Merge into Current Workbook dialog box, select a copy of the workbook that contains the changes that you want to merge. Hold down CTRL or SHIFT to select multiple copies. Select OK.</li>
                </ol>

                <p class=""><img class="d-inline-block" src="/dist/images/excel/merge-workbooks.png" alt="merge-workbooks"></p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/permissions.php">Permissions and Protecting Workbooks</a></li>
                        <li><a href="/excel/lessons/signatures.php">Adding a Digital Signature</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Working with Mutiple Workbooks </a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training testimonials</a> or <a href="/onsite-training.php">obtain a quote for an onsite Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>