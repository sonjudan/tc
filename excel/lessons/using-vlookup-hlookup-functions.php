<?php
$meta_title = "Using VLOOKUP and HLOOKUP Functions in Microsoft Excel | Training Connection";
$meta_description = "Using VLOOKUP and HLOOKUP Functions in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using VLOOKUP and HLOOKUP Functions</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using VLOOKUP and HLOOKUP Functions">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Using VLOOKUP and HLOOKUP Functions</h1>
                        <h5>In this article, we will demonstrate how to use the VLOOKUP and HLOOKUP Functions in Microsoft Excel.</h5>
                        <p>For dedicated, instructor driven <a href="/excel-training.php">Excel training in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Both the VLOOKUP and HLOOKUP functions are used to find specific item(s) in a table of data. The VLOOKUP function contains a number of arguments which are written out like this:</p>
                <p>VLOOKUP (value that is being looked for, lookup table name or range, number of the column in the table containing the relevant data, true or false)</p>
                <p>To use the VLOOKUP function correctly, you need to have your spreadsheet data laid out properly in table form with at least two columns. The first column in the table will contain the keys (identifiers that the VLOOKUP function will examine for a match). In the sample workbook, the keys are the names of the cities. This first column can be referred to as the lookup column:</p>

                <p><img src="/dist/images/excel/vlookup-function.jpg" alt="Using VLOOKUP Function"></p>
                <p>The other columns in the table contain data that corresponds to the column of keys. Your table can be several columns wide, and you can specify which column VLOOKUP will retrieve data from by putting a number corresponding to the given column in the function.</p>
                <p>You can see that this table contains data about the airfare costs to a number of different cities. To help simplify things, all of the data inside the table has been given the range name “prices:” <a href="/excel-training-los-angeles.php">Excel public and private classes in Los Angeles</a>.</p>

                <p><img src="/dist/images/excel/data-range.jpg" alt="Data Range Displayed"></p>
                <p>Click inside D2 and type =VLOOKUP(“New York”,prices,2) into the formula bar. Press Enter on your keyboard to apply the new formula:</p>

                <p><img src="/dist/images/excel/vlookup-formula-bar.jpg" alt="VLOOKUP on Formula Bar"></p>
                <p>D2 will now show the value 700 (the value of a ticket to New York). This lookup function looked vertically down the leftmost column of the lookup table (prices) until it found a match for the text string “New York.” The function then returned the value that is in the second (2) column of the table, in the row where the match was found.</p><p>HLOOKUP operates the same as VLOOKUP, except that it looks across rows (horizontal) for a match rather than down (vertical) columns. If you were to use HLOOKUP, you will need to have a table arranged in a horizontal manner, like so:</p>

                <p><img src="/dist/images/excel/hlookup-function.jpg" alt="HLOOKUP Displayed"></p>
                <p>If you prefer, you can use the Function Arguments dialog to create VLOOKUP or HLOOKUP functions. Click Formulas → Lookup & Reference → VLOOKUP (or HLOOKUP):</p>

                <p><img src="/dist/images/excel/use-function-argument-for-lookup.jpg" alt="Function Argument for Lookups"></p>
                <p>Then, you can use the Function Arguments dialog to construct your function.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our Microsoft Excel training workshop. We run MS classes for all skill levels, every month. Check out our extensive student testimonials  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.<br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>