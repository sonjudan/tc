<?php
$meta_title = "Managing Themes In Microsoft Excel | Training Connection";
$meta_description = "Learn how to Manage Themes In Microsoft Excel. Training Connection offers Excel Training Courses in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Managing Themes In Microsoft Excel</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Managing Themes In Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Managing Themes In Excel</h1>
                        <p>Themes control most of the visual aspects of any workbook and choosing the right theme is important to how your data is displayed. Over the course of this topic you will learn about themes in Excel 2016 and how they can be both changed and customized.</p>

                        <p>For more on <a href="/excel-training.php">Instructor-led Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Topic Objectives</h4>
                <h6>In this topic, you will learn:</h6>
                <ol>
                    <li>About themes and how to change them</li>
                    <li>How to customize themes</li>
                </ol>

                <h4>About Themes</h4>
                <p>Themes are a combination of preset colors, fonts, and effects. Each theme includes 12 colors, font selections for header and body text, as well as colors and effects for shapes and SmartArt. <a href="/excel-training-chicago.php">Small hands-on Excel classes</a> in Chicago.</p>
                <p>To change a workbook’s theme, click Page Layout → Themes:</p>

                <img src="/dist/images/excel/change-workbook-theme.jpg" alt="changing a workbook theme screenshot">

                <p>This will show a gallery of themes. As you mouse over each thumbnail in this gallery, you will see a preview applied to your workbook (if it has theme elements such as SmartArt, headers, or body text). Click the new theme to apply it:</p>

                <img src="/dist/images/excel/theme-elements.jpg" alt="excel theme elements screenshot">

                <h4>Customizing Themes</h4>

                <p>The default theme for new workbooks in Microsoft Excel 2016 is Office. You can customize the styles offered by this theme or any other theme that is currently applied by choosing a new style options from the Colors, Fonts, and Effects drop-down commands that are found inside the Themes group of the Page Layout tab.</p>

                <img src="/dist/images/excel/custom-themes-tab.jpg" alt="custom themes tab screenshot">


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Excel throughout the country. View our <a href="/testimonials.php?course_id=19">Excel  testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>