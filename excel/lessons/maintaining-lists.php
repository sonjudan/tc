<?php
$meta_title = "Maintaining lists in Microsoft Excel | Training Connection";
$meta_description = "Learn to maintain lists in Microsoft Excel. This course is part of our Level 3 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Maintaining Lists</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Maintaining a list in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Maintaining a list in Excel</h1>
                        <h5>These  topics are covered  in Module 1 - Working with Lists, part of our <a href="/excel-training.php">Excel Advanced class</a>.</h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <p>Excel provides an easy-to-use tool called the data form to maintain lists. Hardened spreadsheet users generally prefer to edit data in lists in the worksheet cells. In other words, in-cell or in-line editing. But many Excel users, particularly those fairly new to Excel, like the Data Forms or simply ‘Form’ feature in Excel.</p>

                <p>The data form is a friendly, easier-on-the-eye input and data records analysis tool. It displays the data from your list one record at a time, letting you add, delete, or modify records in a list. It’s true that it is indeed easier on the eye to look at each record in its own form rather than as a line of cell data entries.</p>

                <h4>Using the Data Form</h4>
                <p>The data form, shown in Figure 1-3, displays each field in the selected list. Each column label, or field name, is located on the left side of the form, and the field information is listed in edit boxes. If a column in the list contains a formula, the corresponding values will appear in the data form without edit boxes. You use the scroll bar to move between records. On the right side of the window are list management buttons that let you add new records, delete records, or modify records in your list. Each data form can display as many as 32 fields. If the list contains more than 32 fields, only the first 32 fields will appear in the data form. <a href="/excel-training-los-angeles.php">Advanced MS Excel training in Los Angeles</a>.</p>

                <p>For hands-on Microsoft Excel training classes in Chicago and Los Angeles call us on 888.815.0604.</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-3.png" alt="Figure 1-3: The Data Form"></p>
                <p class="wp-caption-text">Figure 1-3: The Data Form</p>

                <h4>Accessing the Form feature in Excel 2013</h4>
                <p>Excel's built-in data entry form is not present in the Excel 2013 UI. To use the feature, you must customize your Quick Access Toolbar and add the Form command from the Commands Not in the Ribbon group. See below as an exercise in Quick Access toolbar customizing.</p>

                <p>Note that at the top of the Excel 2013 screen (in a defaults Excel setup), the Quick Access Toolbar will look similar to this:</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-4.png" alt="Figure 1-4: The Standard Excel Quick Access Toolbar"></p>
                <p class="wp-caption-text">Figure 1-4: The Standard Excel Quick Access Toolbar</p>
                <p>Of course, you can add your own items to the Quick Access Toolbar. As mentioned above, Data Forms are not on the Excel Ribbon, but are tucked away in the exhaustive list of 2013 additional features and options. So we'll add the Data Forms icon to the Quick Access Toolbar.</p>
                <p>To find Data Forms, click on the File Button, then click on Excel options at the bottom:</p>
                <p>When you click the Excel Options button, you'll see this dialogue box pop up:</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-5.png" alt="Figure 1-5: Excel Options (Customized Quick Access Toolbar Tab)"></p>
                <p class="wp-caption-text">Figure 1-5: Excel Options (Customized Quick Access Toolbar Tab)</p>

                <p>Click the Customize button on the left. The idea is that you can place any items you like on the Quick Access toolbar at the top of Excel 2013. You pick one from the list, and then click the Add button in the middle.</p>
                <p>To add the [Data] Form option to the Quick Access Toolbar, click the drop down list where it says Choose Commands From. You should see this image excerpt of the drop-down list.</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-6.png" alt="Figure 1-6: Selecting Commands Not in the Ribbon"></p>
                <p class="wp-caption-text">Figure 1-6: Selecting Commands Not in the Ribbon</p>

                <p>Click on Commands Not in the Ribbon. The list box will change:</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-7.png" alt="Figure 1-7: The List of Commands not in the Ribbon"></p>
                <p class="wp-caption-text">Figure 1-7: The List of Commands not in the Ribbon</p>

                <p>From the Commands Not in the Ribbon list, select Form. Now click the Add button in the Middle. The list box on the right will then look something like this one:</p>


                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-8.png" alt="Figure 1-8: The Quick Access Toolbar is about to change"></p>
                <p class="wp-caption-text">Figure 1-8: The Quick Access Toolbar is about to change</p>

                <p>When you click OK on the Excel Options dialog box, you'll be returned to Excel 2013. Look at the Quick Access toolbar, and you should see your new item:</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-9.png" alt="Figure 1-9: The Updated Quick Access Toolbar"></p>
                <p class="wp-caption-text">Figure 1-9: The Updated Quick Access Toolbar</p>

                <h4>Method</h4>
                <h6>To open the data form:</h6>
                <ol>
                    <li>Select a cell in the list.</li>
                    <li>On the Quick Access Toolbar, click the Form button.</li>
                </ol>

                <h6>To move between records using the data form:</h6>
                <ol>
                    <li>Choose Find Next or click the lower scroll arrow to move to the next record in the list. or</li>
                    <li>Choose Find Prev or click the upper scroll arrow to move to the previous record in the list. or</li>
                    <li>Drag the scroll box until the desired record number appears in the upper right corner of the data form.</li>
                </ol>

                <h6>To close the data form:</h6>
                <ol>
                    <li>Choose Close. or</li>
                    <li>Click the Close button.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will open the data form, scroll through records in the data form, and then close the data form.</h6>
                <ol>
                    <li>Select a cell in the list.</li>
                    <li>On the Quick Launch Toolbar, click the Form button. [The data form appears, displaying 1 of 4 in the upper right corner. The data for record 1 appears in the edit boxes, and the data in the ID NO edit box is highlighted].</li>
                    <li>Click the bottom scroll arrow until the data for the Chevy Astrovan appears. [4 of 4 appears in the upper right corner of the data form].</li>
                    <li>Drag the scroll box to the top of the scroll bar. [The record number changes as you scroll, displaying 1 of 1 when finished].</li>
                    <li>Choose Close. [The data form closes].</li>
                </ol>

                <h4>Adding and Deleting Records Using the Data Form</h4>
                <p>You can easily add a new record to a list using the data form. When you choose New, Excel blanks out all the edit boxes in the form and the words “New Record” appear in the upper right corner of the data form. Deleting records using the data form is a permanent operation that cannot be undone.</p>

                <h4>Method</h4>
                <h6>To add records using the data form:</h6>
                <ol>
                    <li>Open the data form. [The data for record 1 appears in the edit boxes].</li>
                    <li>Choose New. [All edit boxes are blank, and New Record appears in the upper right of the data form].</li>
                    <li>In the ID NO edit box, type 18. [The Restore button is active].</li>
                    <li>Repeat steps 2 and 3 to add additional records.</li>
                    <li>Choose Close to add the last record and close the data form. or</li>
                    <li>Choose New or scroll to a record to add the last record and keep the data form open.</li>
                </ol>

                <h6>To delete records using the data form:</h6>
                <ol>
                    <li>Open the data form.</li>
                    <li>Move to the record you want to delete.</li>
                    <li>Choose Delete.</li>
                    <li>In the confirmation box that appears, choose OK.</li>
                    <li>Choose Close when finished.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will add and delete records using the data form.</h6>
                <ol>
                    <li>Open the data form. [The data for record 1 appears in the edit boxes].</li>
                    <li>Choose New. [All edit boxes are blank, and New Record appears in the upper right of the data form].</li>
                    <li>In the ID NO edit box, type 18. [The Restore button is active].</li>
                    <li>Press Tab. [The insertion point moves to the next edit box].</li>
                    <li>In the appropriate edit boxes, enter the remaining data for the Pontiac Sunbird as shown in Figure 1-10.</li>
                    <li>Choose New. [The new record is added to the list and a new blank record appears].</li>
                    <li>Add the record for the Ford Festiva as shown Figure 1-10, and then choose New.</li>
                    <li>Use the scroll arrows to select the record for the Ford Taurus.</li>
                    <li>Choose Delete. [A confirmation box appears, stating, “Displayed record will be permanently deleted”].</li>
                    <li>Choose OK. [The record is deleted].</li>
                    <li>Close the data form, and then save and close the workbook.</li>
                </ol>

                <p><img class="aligncenter" src="/dist/images/excel/excel3-figure1-10.png" alt="Figure 1-10: New List Data"></p>
                <p class="wp-caption-text">Figure 1-10: New List Data</p>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/creating-lists.php">Creating a List</a></li>
                        <li><a href="/excel/lessons/editing-records.php">Editing Records Using the Data Form</a></li>
                        <li><a href="/excel/lessons/filtering-lists.php">Filtering a List</a></li>
                        <li><a href="/excel/lessons/subtotals-in-lists.php">Using Subtotals in a List</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Easy Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing  Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Creating Basic Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel training testimonials</h4>
                    <p>We train thousands of students to use Excel every year. See what our past students are saying on our <a href="/testimonials.php?course_id=19">Excel testimonial page</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>