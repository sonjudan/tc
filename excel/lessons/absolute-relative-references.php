<?php
$meta_title = "Relative and Absolute Cell References | Training Connection";
$meta_description = "Learn the differences between Relative and Absolute cell references in Microsoft Excel. Training Connection offers Excel Training Courses in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cell Reference</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Relative and Absolute Cell References in Microsoft Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Relative and Absolute Cell References in Microsoft Excel</h1>
                    </div>
                    <div data-aos="fade-up" >
                        <p>A <strong>cell reference </strong>refers to a single <strong>cell </strong>or range of <strong>cells </strong>on a Excel worksheet. These cells can be referred to by Excel Formulas when calcuations are made. In this tutorial you will learn the differences between relative and absolute cell references. Each behaves differently when copied and filled to other cells. Relative references <strong>change</strong> when a formula is copied to another cell whereas Absolute references,  remain <strong>constant</strong>, no matter where they are copied.</p>
                        <p>For more on <a href="/excel-training.php">Instructor-led Microsoft Excel training classes</a> in Chicago and Los Angeles call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">


                <h4>Understanding Relative and Absolute Cell References</h4>
                <p>Excel worksheets are composed of rows (horizontal, referenced by numbers) and columns (vertical, referenced by letters). When these two elements intersect, they create a cell, and each cell is given a name. In the example below, you can see that the cell that is selected is named “C2.” This is because it lies at the intersection of column C and row 2:</p>

                <p><img src="/dist/images/excel/cell-ref-1.png" alt="cell-ref-1"></p>
                <p>This particular cell contains a formula that adds the values found in cells A2 and B2 together, which you can see in the formula bar when cell C2 is selected. Select cell C2. Click and drag the small square in the bottom right-hand corner of this cell down one cell to C3:</p>

                <p><img src="/dist/images/excel/cell-ref-2.png" alt="cell-ref-2"></p>
                <p>Release your mouse button. You will see that cell C3 now contains 0:</p>

                <p><img src="/dist/images/excel/cell-ref-3.png" alt="cell-ref-3"></p>

                <p>This has happened because the cell references, which are visible in the formula bar when C3 is selected, have been changed. When the AutoFill operation was performed, the cell references were modified relative to the location of the formula. Since A3 and B3 contain no values, the result is 0. <a href="/excel-training-chicago.php">Hands-on Chicago Excel classes</a>.</p>
                <p>The same thing will happen if you copy and paste the contents of cell C2 somewhere else. Try pasting cell C2 to F4</p>

                <p><img src="/dist/images/excel/cell-ref-4.png" alt="cell-ref-4"></p>

                <p>F4 will now display a value of 0 because the values relative to the formula location (D4 and E4) are empty. This happens because the original formula uses relative cell references. Most of the time, these relative references aren’t an issue. However, if data is to be moved around or copied using AutoFill, relative cell references can create incorrect and confusing results.</p>

                <p>To avoid this, you can use absolute cell references. These references use dollar signs ($) to make sure a formula always references the same location, no matter where it is moved. You can assign absolute cell references in three ways:</p>
                <ul>
                    <li>$Column$Row: Both the row and column designation won’t change ($A$1).</li>
                    <li>$ColumnRow: The column designation won’t change, but the row can ($A1).</li>
                    <li>Column$Row: The row designation won’t change, but the column can (A$1).</li>
                </ul>

                <p>Select cell C2. Replace the formula with the following formula that uses absolute cell references: =$A$2+$B$2:</p>
                <p><img src="/dist/images/excel/cell-ref-5.png" alt="cell-ref-5"></p>

                <p>Press Enter to apply the new changes. Copy and paste this cell to E3. You will see that the formula and result will stay the same:</p>
                <p><img src="/dist/images/excel/cell-ref-6.png" alt="cell-ref-6"></p>

                <p>Save the changes that you have made to the current workbook and then close Microsoft Excel.</p>

                <p>Next: <a href="/excel/lessons/multiple-cell-references.php">Multiple and 3D Cell References</a></p>




            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite Microsoft Excel throughout the country. View our <a href="/testimonials.php?course_id=19">Excel  testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>