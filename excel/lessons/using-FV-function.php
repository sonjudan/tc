<?php
$meta_title = "Using the FV Function in Microsoft Excel | Training Connection";
$meta_description = "Using the FV Function in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using FV Function</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using the FV Function">
                    </div>
                    <div data-aos="fade-up" >
                        <h1>Using the FV Function</h1>
                        <h5>In this article, we will delve a little deeper into how to use the FV Function in Microsoft Excel.</h5>

                        <p>For instructor delivered <a href="/excel-training.php">MS Excel training in Chicago and Los Angeles</a> call us on 888.815.0604..</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <p>Another commonly used financial function is the FV (future value) function. This function returns the future value of a series of periodic payments to an investment at a fixed interest rate. For example, if you put $5,000 a year into an investment that yields 3.5% annual interest, the FV function can tell you how much your investment will be worth after a given period.</p>

                <p>To use this function, first select the cell in which you would like the results to be displayed. For this example, click cell D2. Next, click Formulas → Financial → FV:</p>

                <p><img src="/dist/images/excel/FV-function.jpg" alt="Using FV Function"></p>

                <p>This action will open the Function Arguments dialog for the FV function:</p>

                <p><img src="/dist/images/excel/function-argument.jpg" alt="Open Function Argument Dialog"></p>

                <p>The top portion of this dialog contains a number of arguments that you can use to tailor this function. The arguments in bold are required in order for this function to operate. Arguments not in bold are optional:</p>

                <p><img src="/dist/images/excel/function-argument-requirement.jpg" alt="Required Data for Argument"></p>

                <p>The Rate argument is the interest rate at which you expect to earn on the investment value. In the current workbook, interest rates are listed in column A, so with the Rate field selected, click cell A2:</p>

                <p><img src="/dist/images/excel/rate-argument.jpg" alt="FV Rate Argument"></p>
                <p><a href="/excel-training-chicago.php">Excel formula classes in Chicago</a>.</p>

                <p>You need to account for the fact that the values in column A are annual interest rates, so you need to divide this data by 12. In the Rate argument field, type /12 after A2:</p>

                <p><img src="/dist/images/excel/annual-interest-rate.jpg" alt="Divide Rate"></p>

                <p>Next, the Nper argument is the number of payment periods over which contributions will be made to the investment. In the sample workbook, all payment periods are stored in column B, so with the Nper field selected, click B2:</p>

                <p><img src="/dist/images/excel/Nper-argument.jpg" alt="Nper Argument Displayed"></p>
                <p>The Pmt argument is used to define the payment that you will make in each period. In this workbook, payment amounts are stored in column C. With the Pmt field selected, type -C2:</p>

                <p><img src="/dist/images/excel/pmt-argument.jpg" alt="Define Payment Period"></p>

                <p>The remaining arguments left in this function are optional.</p>

                <p>The Pv argument is use to set a lump sum that you can begin with. (By default, this argument is 0.)</p>
                <p>The Type argument will specify if the payment is made at the beginning or end of the payment period: enter 1 for payments at the beginning of the payment period and 0 for payments at the end of the payment period. (By default, this argument is also 0.)</p>

                <p>The Function Arguments dialog will now look like this:</p>

                <p><img src="/dist/images/excel/function-arguments-dialog-displayed.jpg" alt="FV Argument Displayed"></p>

                <p>The result of the function indicates the end value of this investment will be $3,034.61. Click OK to insert this function:</p>

                <p><img src="/dist/images/excel/insert-function.jpg" alt="Function Results"></p>

                <p>The result of the function will now be displayed in cell D2 of the current worksheet:</p>

                <p><img src="/dist/images/excel/results-displayed.jpg" alt="result of function"></p>

                <p>Using AutoFill, apply this function to cells D3-D5:</p>

                <p><img src="/dist/images/excel/use-autofill.jpg" alt="Use Autofill to Apply Function"></p>

                <p>When using the FV function, you do not need to use the Function Arguments dialog. You can type the function directly into the formula bar using “FV” as a prefix:</p>

                <p><img src="/dist/images/excel/function-in-formula-bar.jpg" alt="Type Function in Formula Bar"></p>
            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget widget-col-2">
                    <h4 class="widget-title">Class based Microsoft Excel available in Chicago and LA</h4>
                    <p>Join our 1-day Excel workshop. We run classes for every skill level each month. Check out real student testimonials  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>