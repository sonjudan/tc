<?php
$meta_title = "Using Comments in MS Excel in Microsoft Excel | Training Connection";
$meta_description = "Learn how to use comments in Microsoft Excel. This course is part of our Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Using Comments</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Using Comments in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Using Comments in Excel</h1>
                        <p>For classroom based, <a href="/excel-training.php">instructor delivered Microsoft Excel training courses </a>in Chicago and LA call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h6>In this article, we will learn how to:</h6>

                <ul>
                    <li>Insert comments</li>
                    <li>Edit comments</li>
                    <li>Navigate through comments</li>
                    <li>Delete comments</li>
                </ul>


                <h4>Inserting Comments</h4>

                <p>To insert a comment into your worksheet, first click to select the cell that you would like to attach the comment to. For this example, click to select G4:</p>
                <p><img src="/dist/images/excel/insert-comment.jpg" alt="comment in worksheet"></p>

                <p>Next, click Review → New Comment:</p>
                <p><img src="/dist/images/excel/review-comment.jpg" alt="click review"></p>

                <p>The comment will then be added to the selected cell. Cells that have a comment added to them will be indicated with a red upper right-hand corner. The comment text box will be displayed. For this example, type “Please review.” into this text box:</p>
                <p><img src="/dist/images/excel/comment-added.jpg" alt="comment added to cell"></p>

                <p>Click elsewhere on your worksheet and the comment text box will be hidden:</p>
                <p><img src="/dist/images/excel/comment-box-hidden.jpg" alt="box no longer shows"></p>

                <h4>Editing Comments</h4>

                <p>To display any comments that appear on your worksheet, click on the cell that the comment has been attached to. For example, click cell G4:</p>
                <p><img src="/dist/images/excel/edit-comment.jpg" alt="display any comment"></p>

                <p>While this will display the comment, you wont be able to edit it. To do this keep the cell selected, and then click Review → Edit Comment:</p>
                <p><img src="/dist/images/excel/review-edit-comment.jpg" alt="click review to edit comment"></p>

                <p>The comment that is attached to the current cell will now be displayed and you will be able to edit it. For this example, replace the text in this comment with “Please review for errors:”</p>
                <p><img src="/dist/images/excel/replace-text.jpg" alt="edit text"></p>

                <p>After you have edited your comment, click elsewhere on your worksheet to apply the changes. LA <a href="/excel-training-los-angeles.php">Excel courses</a> - beginner to advanced.</p>

                <h4>Navigating Through Comments</h4>

                <p>While comments that exist in your worksheet are indicated by a red upper right-hand corner, a quicker way to navigate through your comments is to use the commands on the Review tab. Click Review → Next:</p>
                <p><img src="/dist/images/excel/command-review-tab.jpg" alt="use command tab to navigate"></p>

                <p>The first comment will now be displayed:</p>
                <p><img src="/dist/images/excel/comment-displayed.jpg" alt="comment now shown"></p>

                <p>Again, click Review → Next:</p>
                <p><img src="/dist/images/excel/click-review-again.jpg" alt="click review next"></p>

                <p>The next consecutive comment will be displayed and the previous comment will be hidden:</p>
                <p><img src="/dist/images/excel/next-consecutive-comment.jpg" alt="previous comment hidden"></p>

                <p>Keep following this system to continue through the comments that exist within your worksheet. If you need to go backwards, click Review → Previous.</p>

                <h4>Deleting Comments</h4>

                <p>To delete a comment, first click to select the cell that the comment that you want to delete is attached to. For this example, click to select cell G4:</p>
                <p><img src="/dist/images/excel/delete-comment.jpg" alt="select comment to delete"></p>

                <p>Next, click Review → Delete:</p>
                <p><img src="/dist/images/excel/review-delete.jpg" alt="click review delete"></p>

                <p>The comment will now have been deleted from the selected cell. If necessary, this action can be undone using the Undo command.</p>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget">
                <h4 class="widget-title">MS Excel Course Reviews</h4>
                <p>Each year we train thousands of students to use Microsoft Excel. Find out what they have to say about our courses on the <a href="/testimonials.php?course_id=19">MS Excel Student Reviews</a> page.</p>
            </div>

        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>