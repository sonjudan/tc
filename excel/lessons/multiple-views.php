<?php
$meta_title = "Creating multiple views and Freezing frames in Excel Worksheets | Training Connection";
$meta_description = "Learn to add horizontal and vertical split bars and freeze frames in Microsoft Excel. This course is part of our Level 2 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Multiple Views</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Creating multiple views in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Creating multiple views in Excel</h1>
                        <h5>The following topics are covered in Module 1 in our Excel Intermediate class.</h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Creating Multiple Views</h4>
                <p>In many cases, you might find it helpful to work with different sections of your worksheet at the same time. You might, for example, want to keep the labels in row 4 visible while you scroll down to look at information located in row 35. You do this by applying either split bars or freezing panes.</p>
                <p>For live <a href="/excel-training.php">face-to-face Excel training</a> in Chicago and Los Angeles call us on 888.815.0604.</p>

                <h4>Applying Split Bars</h4>
                <p>When you apply split bars to a worksheet, Excel creates identical copies of the worksheet side by side. Split bars are illustrated in Figure 1-3. If you apply either a horizontal or vertical split bar, you can scroll within one pane while the other pane remains stationary. If you apply both horizontal and vertical split bars, in which four panes are created, only two panes remain stationary when you scroll within one pane. For example, if you horizontally scroll in the upper right pane, you simultaneously scroll through the lower right pane while the two left panes remain stationary. <a href="/excel-training-chicago.php">Certified MS Excel training</a> in Chicago.</p>
                <p>Although the Split command can be accessed from the Windows group on the View tab, you can also manipulate split bars with the mouse using the split boxes shown in Figure 1-4.</p>
                <p>You can move between the different panes by simply clicking the pane in which you want to work. Because each pane is a view of the same worksheet, a change in one pane means a change to the worksheet.</p>
                
                <p><img src="/dist/images/excel/excel2-figure1-3.png" alt="Figure 1-3: Worsheet Split Bars"></p>
                <p class="wp-caption-text">Figure 1-3: Worsheet Split Bars</p>

                <h4>Method</h4>
                <h6>To apply horizontal and vertical split bars:</h6>
                <ol>
                    <li>Click the cell to the right of the location for a vertical split and below the location for a horizontal split.</li>
                    <li>From the Windows group on the View tab, choose Split.</li>
                </ol>

                <h6>To apply a horizontal or vertical split bar:</h6>
                <ol>
                    <li>Position the mouse pointer over the horizontal or vertical split box.</li>
                    <li>When the mouse pointer changes to a split pointer, drag the split box down or left to the desired location.</li>
                </ol>

                <h6>To remove a split bar:</h6>
                <ol>
                    <li>Double-click any part of the split bar that divides the panes.</li>
                </ol>

                <p><img src="/dist/images/excel/excel2-figure1-4.png" alt="Figure 1-4: The Split Boxes"></p>
                <p class="wp-caption-text">Figure 1-4: The Split Boxes</p>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will apply split bars.</h6>
                <ol>
                    <li>Select the Enrollment by Seminar worksheet.</li>
                    <li>Select cell B5.</li>
                    <li>From the Windows group on the View tab, choose Split. [The worksheet is split into four panes].</li>
                    <li>Double-click any part of the split bar that divides the panes. [The split bars are removed].</li>
                    <li>Position the mouse pointer over the vertical split box. [The mouse pointer changes to a split pointer].</li>
                    <li>Drag the vertical adjustment to the right of column B. [As you drag, an outline of the split bar appears. Then the window is split into two panes. Because cell B5 was selected, the left pane is now selected].</li>
                    <li>Use the Name box to select cell V7. [Cell V7 is visible in the left pane. The right pane does not change].</li>
                    <li>Drag the vertical split bar to the left edge of the worksheet window. [The split bar is removed].</li>
                </ol>

                <h4>Freezing Panes</h4>
                <p>Another way to divide your worksheet into panes is by freezing sections of the worksheet. Freezing panes is useful when you are working with large tables because you can hold horizontal and vertical labels stationary while you move through the data.</p>

                <h4>Method</h4>
                <h6>To freeze panes of data:</h6>
                <ol>
                    <li>Select the cell below and to the right of the location for the frozen panes.</li>
                    <li>From the Window menu, choose Freeze Panes.</li>
                    <li>Note: If the cell you select is in column A or in row 1, choosing Freeze Panes will result in two panes instead of four.</li>
                </ol>

                <h6>To unfreeze panes of data:</h6>
                <ol>
                    <li>From the Window menu, choose Unfreeze Panes.</li>
                    <li>Note: When you freeze panes, the Freeze Panes option changes to Unfreeze Panes so that you can unlock frozen rows or columns.</li>
                </ol>


                <h4>Exercise</h4>
                <h6>In the following exercise, you will freeze and unfreeze worksheet panes.</h6>
                <ol>
                    <li>Select cell B13.</li>
                    <li>From the Window menu, choose Freeze Panes. [The worksheet is divided into four panes. The panes are separated by thin, dark lines].</li>
                    <li>Press Right Arrow until you can see the Dec-95 column. [The labels in column A remain stationary as you scroll to the right].</li>
                    <li>Press Page Down twice. [Rows 1 through 12 remain stationary as you scroll].</li>
                    <li>From the Window menu, choose Unfreeze Panes. [The worksheet is restored to a single pane, and the thin, dark lines are removed].</li>
                </ol>

                <h4>Viewing and Arranging Multiple Worksheet Windows</h4>
                <p>Often, it is useful to view more than one worksheet at a time. You can arrange worksheets on your screen so that you can view them simultaneously. Once you have more than one copy of your worksheet window open, you can select different worksheets to view from each worksheet window, and then arrange the windows to best suit your needs. For example, Figure 1-5 displays four worksheets from the same workbook at once. Once you have arranged the worksheet windows, you can move between different windows simply by clicking the window in which you want to work. It takes one click to activate a window and another click to select anything in that window.</p>

                <p><img class="aligncenter" src="/dist/images/excel/excel2-figure1-5.png" alt="Figure 1-5: Tiled Worksheets"></p>
                <p class="wp-caption-text">Figure 1-5: Tiled Worksheets</p>


                <p>The Arrange Windows dialog box provides options to let you set up your data on the screen. These options are summarized in Table 1-2. </p>

                <table class="table table-bordered table-dark table-hover table-l2 mb-1">
                    <thead>
                        <tr>
                            <td><strong>Option</strong></td>
                            <td><strong>Function</strong></td>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>Tiled</td>
                            <td>Arranges windows so that each fills an equal portion of the work area.</td>
                        </tr>
                        <tr>
                            <td>Horizontal</td>
                            <td>Arranges windows one below the other.</td>
                        </tr>
                        <tr>
                            <td>Vertical</td>
                            <td>Arranges windows next to each other.</td>
                        </tr>
                        <tr>
                            <td>Cascade</td>
                            <td>Stacks the windows, offset vertically with the title bar of each window showing.</td>
                        </tr>
                        <tr>
                            <td>Windows of active workbook</td>
                            <td>Provides views of only the currently active workbook when other workbooks are open. </td>
                    </tr>
                    </tbody>
                </table>
                <p class="wp-caption-text">Table 1-2 : The Arrange Windows Dialog Box Options</p>


                <h4>Method</h4>
                <h6>To view and arrange multiple worksheet windows:</h6>
                <ol>
                    <li>Select the Book/sheet you’d like to view full-size. From the Window group on the View tab, choose New Window.</li>
                    <li>This opens a copy in Read Only mode.</li>
                    <li>Repeat steps 1 and 2 for each worksheet you want to view. Closing copies and returning as required.</li>
                    <li>In the Window group, choose Arrange All.</li>
                    <li>In the Arrange Windows dialog box, in the Arrange area, select the desired option.</li>
                    <li>If necessary, select the Windows of active workbook check box.</li>
                    <li>Choose OK.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will view and arrange multiple worksheet windows.</h6>
                <ol>
                    <li>From the Window group on the View tab, choose New Window. [A copy of the worksheet window appears on top of the original, and the window number is referenced in the title bar].</li>
                    <li>In the new window, select the Total Enrollment worksheet.</li>
                    <li>From the Window group, choose New Window. [Another copy of the worksheet window appears and the window number is referenced in the title bar].</li>
                    <li>In the new window, select the Tuition Generated worksheet.</li>
                    <li>From the Window group, choose New Window. [Another copy of the worksheet window appears and the window number is referenced in the title bar].</li>
                    <li>In the new window, select the Average Costs worksheet.</li>
                    <li>From the Window group, choose Arrange. [The Arrange Windows dialog box appears].</li>
                    <li>In the Arrange area, make sure the Tiled option button is selected.</li>
                    <li>Choose OK. [The worksheet windows are tiled].</li>
                    <li>Click anywhere in the top right worksheet window. [The second worksheet window becomes active. Scroll bars appear].</li>
                    <li>Close copies 2,3, and 4 of the workbook by clicking their Close buttons.</li>
                    <li>Maximize the remaining worksheet window. [The Enrollment by Seminar worksheet is active and fills the window].</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/multiple-workbooks.php">Multiple Workbooks</a></li>
                        <li><a href="/excel/lessons/worksheets.php">Excel Worksheets</a></li>
                        <li><a href="/excel/lessons/working-with-multiple-workbooks.php">Multiple Workbooks cont. </a></li>
                        <li><a href="/excel/lessons/creating-workspaces.php">Excel Workspaces</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Beginner Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Copying Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Need in house Excel training?</h4>
                    <p>Through our network of local trainers we can provide Excel training at your offices right across the country.</p>
                    <p>We have trained tens of thousands of students, <a href="/testimonials.php?course_id=19">click here</a> to review some testimonials or <a href="/onsite-training.php">obtain a quotation for group Excel training</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>