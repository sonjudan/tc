<?php
$meta_title = "Working with Permissions in Microsoft Excel | Training Connection";
$meta_description = "Marking a Worksheet final, restricting access using permissions and adding a password. This and other topics are covered on our instructor-led Excel Training classes, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Permissions</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Working with Permissions in Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Working with Permissions in Excel</h1>
                        <p>The following tutorial introduces you to the Information tab on the Backstage View. You’ll learn about marking a workbook as final, which makes the workbook read-only. You’ll also learn about permissions - both encrypting the workbook with a password and restricting permissions. This tutorial explains how to protect both the current sheet and an entire workbook’s structure.</p>
                        <p>For <a href="/excel-training.php">MS Excel training classes in Chicago and Los Angeles</a> call us on 888.815.0604.</p>
                    </div>
                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Marking Worksheet as Final</h4>
                <h6>To mark a workbook as final, use the following procedure.</h6>
                <ol>
                    <li>Select the File tab from the Ribbon to open the Backstage View.</li>
                    <li>Select Protect Workbook.</li>
                    <li>Select Mark as Final.</li>
                </ol>

                <p><img src="/dist/images/excel/marking-as-final.png" alt="Makring as Final"></p>

                <ol start="4">
                    <li>Excel displays a warning message. Select OK to continue.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/warning-message.png" alt="Warning Message"></p>

                <ol start="5">
                    <li>Excel displays a warning message. Select OK to continue.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/information-message.png" alt="Information Message"></p>

                <p>Notice the yellow bar at the top of the workbook to indicate that the workbook has been marked as final. <a href="/excel-training-los-angeles.php">Excel 2019 classes in Los Angeles</a>.</p>

                <p><img src="/dist/images/excel/yellow-bar.png" alt="Yellow Bar"></p>

                <p>Notice that on the Info tab on the Backstage View, the Permissions area has changed.</p>

                <p><img src="/dist/images/excel/protect-workbook.png" alt="Protect Workbook"></p>

                <h4>Encrypting with a Password</h4>
                <h6>To encrypt a workbook with a password, use the following procedure.</h6>
                <ol>
                    <li>Select the File tab from the Ribbon to open the Backstage View.</li>
                    <li>Select Protect Workbook</li>
                    <li>Select Encrypt with Password.</li>
                </ol>

                <p><img src="/dist/images/excel/encrypt-with-password.png" alt="Encrypt with password"></p>

                <ol start="4">
                    <li>Excel displays the Protect Sheet dialog box. </li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/protect-sheet.png" alt="Protect sheet"></p>
                <ol start="5">
                    <li>You can enter a password if desired to unprotect the sheet.</li>
                    <li>Check the boxes for the actions that you want to allow other users to perform on the sheet.</li>
                    <li>Select OK.</li>
                </ol>


                <h6>To protect a workbook structure, use the following procedure.</h6>
                <ol>
                    <li>Select the File tab from the Ribbon to open the Backstage View.</li>
                    <li>Select Protect Workbook.</li>
                    <li>Select Protect Workbook Structure.</li>
                    <li>Excel displays the Protect Structure and Windows dialog box.</li>
                </ol>

                <p><img class="d-inline-block" src="/dist/images/excel/protect-structure.png" alt="Protect structure"></p>

                <ol start="5">
                    <li>Check the boxes for the options you want to protect.</li>
                    <li>You can enter a password if desired to unprotect the workbook.</li>
                    <li>Select OK.</li>
                </ol>


            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/sharing-workbooks.php">Sharing Workbooks in Excel</a></li>
                        <li><a href="/excel/lessons/signatures.php">Digital Signatures</a></li>
                        <li><a href="multiple-views.php">Working with multiple Views</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Group Excel training</h4>
                    <p>Through our network of local trainers we deliver onsite group Microsoft Excel training right across the country. View our <a href="/testimonials.php?course_id=19">Excel training student testimonials</a> or <a href="/onsite-training.php">obtain a quote for a customized Excel class</a>.</p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>