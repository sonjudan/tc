<?php
$meta_title = "Excel Selection Techniques & Exercises | Training Connection";
$meta_description = "Learn how to make Selections in Microsoft Excel. This course is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Selections</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Selection Techniques">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Excel Selection Techniques</h1>
                        <h5>These  topics are covered  in Module 1 - Excel Essentials, which forms part of our Beginner  Excel training course.</h5>
                    </div>

                </div>
            </div>


            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">

                <h4>Using Selection Techniques</h4>
                <p>Before you can enter data, you have to select a cell. Before you can change the data, you have to select it. In this section, you'll learn effective, easy techniques that enable you to select cells, ranges, and nonadjacent cells. With one click, you can even select your entire worksheet, which is definitely a timesaver when it comes to making global changes.</p>
                <p><a href="/excel-training.php">Training Connection offer 4 levels of Excel training in Chicago and Los Angeles.</a></p>

                <h4>Selecting a Cell</h4>
                <p>Selecting a cell is the first step in entering data or executing most commands in a worksheet. The single cell that receives the data or formula you enter is the active cell. When a cell is selected, its border becomes bold and the column and row indicators are highlighted. <a href="/excel-training-los-angeles.php">Small instructor-led Excel classes</a> in Los Angeles.</p>

                <h4>Method</h4>
                <h6>To select a cell:</h6>
                <ol>
                    <li>Click the desired cell or</li>
                    <li>Use the arrow keys to move the desired cell</li>
                </ol>
                <h4>Exercise</h4>
                <p>In the following exercise, you will select cells using the keyboard and the mouse.</p>
                <ol>
                    <li>If cell A1 is not selected, click it using the mouse. [Cell A1 is selected and is the active cell.]</li>
                    <li>Press Down Arrow 4 times. [A5 becomes the active cell].</li>
                    <li>Press Right Arrow 2 times. [C5 becomes the active cell].</li>
                    <li>Click cell E5. [E5 becomes the active cell].</li>
                </ol>

                <h4>Selecting a Range of Cells</h4>
                <p>A rectangular selection of multiple cells is referred to as a range. You may wish to select a range when entering a group of data or when you wish to perform the same action on several cells. The range appears as a shaded block of cells, and the active cell in a range is defined by a bold border and white background. When a range is selected, its column and row headings are highlighted. In Figure 1-7, the selected range is referred to as B6:C9 ("B6 to C9").</p>

                <p><img class="" src="/dist/images/excel/excel-figure1-7.png" alt=""></p>
                <h4>Method</h4>
                <p>To select a range of cells:</p>
                <h6>Mouse method</h6>
                <ol>
                    <li>Select the first cell of the desired range.</li>
                    <li>Drag the mouse pointer through the range of cells you wish to include. or</li>
                    <li>Press and hold Shift, and then click the final cell of the range.</li>
                    <li>Release Shift</li>
                </ol>
                <h6>Keyboard method</h6>
                <ol>
                    <li>Use the arrow keys to move the cell pointer to the first cell of the desired range.</li>
                    <li>Press and hold Shift, and then use the arrow keys to highlight the desired range.</li>
                    <li>Release Shift.</li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will select ranges of cells using the mouse and the keyboard.</p>
                <ol>
                    <li>Open a Blank workbook select cell A1.</li>
                    <li>Drag the mouse pointer to cell D5. [The range A1 to D5 is selected].</li>
                    <li>Using the arrow keys, select cell B2. [Cell B2 is selected. The first range, A5 to D5, is deselected].</li>
                    <li>Press and hold Shift, and then use the arrow keys to select cell E7. [The range B2:E7 is selected].</li>
                    <li>Release Shift.</li>
                    <li>Select cell A1. [The range B2:E7 is deselected].</li>
                    <li>Press and hold Shift, and then click cell D1.</li>
                    <li>Release Shift. [The range A1:D1 is selected].</li>
                    <li>Click cell A1. [The range A1:D1 is deselected].</li>
                </ol>

                <h4>Selecting Nonadjacent Cells and Ranges</h4>
                <p>As a rule, to select multiple, nonadjacent objects in Windows, you employ the Ctrl key. For example, you may use Ctrl and the mouse to select multiple files or folders in Explorer or My Computer. This method also works in Excel to select nonadjacent cells and ranges.</p>

                <h4>Method</h4>
                <h6>To select nonadjacent cells or ranges:</h6>
                <ol>
                    <li>Select the first cell or range.</li>
                    <li>Press and hold Ctrl</li>
                    <li>Select the next cell or range.</li>
                    <li>Release Ctrl.</li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will select nonadjacent cells and ranges.</p>
                <ol>
                    <li>Make sure cell A1 is selected.</li>
                    <li>Press and hold Ctrl. [The range A1 to D5 is selected].</li>
                    <li>Select cell C4. [Cells A1 and C4 are selected].</li>
                    <li>Using the mouse, select the range B6:E10. [Cells A1 and C4, and the range B6:E10 are selected].</li>
                    <li>Release Ctrl.</li>
                    <li>Select cell A1.</li>
                </ol>

                <h4>Selecting an Entire Worksheet</h4>
                <p>Selecting an entire worksheet is useful when you want to make changes on a global scale. For instance, you might want to change the size of the font in every cell in the worksheet. Once you select the entire worksheet using the Select All button, illustrated in Figure 1-4, you can do this in a single step.</p>

                <p><img class="" src="/dist/images/excel/excel-figure1-8.png" alt=""></p>

                <h4>Method</h4>
                <p>To select an entire worksheet:</p>
                <ol>
                    <li>Click the Select All button.</li>
                </ol>

                <h4>Exercise</h4>
                <p>In the following exercise, you will select the entire worksheet.</p>
                <ol>
                    <li>Click the Select All button. [The entire worksheet is highlighted].</li>
                    <li>Click any cell. [Only the clicked cell is selected.].</li>
                </ol>

            </div>
        </div>

    </main>

    <div class="mb-4 clearfix">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Microsoft Excel</a></li>
                        <li><a href="/excel/lessons/opening-excel-files.php">Opening Excel Files</a></li>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Screen Interface</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Adjusting Columns</a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting </a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Building Basic Formulas in Excel</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php">Using Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Onsite Excel training</h4>
                    <p>Do you have a group that needs Excel training. Why not have one of our certified trainers deliver a class at your offices? <a href="/onsite-training.php">Click here</a> for an onsite excel training quotation or view our <a href="/testimonials.php?course_id=19">student testimonials</a>.<br></p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="/excel/introduction.php">Excel Level 1 - Introduction</a></li>
                        <li><a href="/excel/intermediate.php">Excel Level 2 - Intermediate</a></li>
                        <li><a href="/excel/advanced.php">Excel Level 3 - Advanced</a></li>
                        <li><a href="/excel/vba.php">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>