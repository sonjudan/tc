<?php
$meta_title = "Opening Files in Microsoft Excel | Training Connection";
$meta_description = "Opening new, existing and recently opened Workbook files in Microsoft Excel. This course is part of our Level 1 Excel Training Course, available in Chicago and Los Angeles";

include_once $_SERVER["DOCUMENT_ROOT"]. '/header.php';
?>

    <main class="page-single-content g-text-excel">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/resources.php">Resources</a></li>
                    <li class="breadcrumb-item"><a href="#">Microsoft Office</a></li>
                    <li class="breadcrumb-item"><a href="/resources/excel.php">Exel</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Opening Workbooks</li>
                </ol>
            </nav>

            <div class="page-intro mb-3">
                <div class=" intro-copy">
                    <div class="intro-icon-r" data-aos="fade-up">
                        <img src="/dist/images/icons/icon-office-excel.png" alt="Excel Essentials - Starting Excel">
                    </div>
                    <div data-aos="fade-up">
                        <h1>Opening Excel Workbooks</h1>
                        <h5>These  topics are covered  in Module 1 - Excel Level 1 Introduction course.</h5>
                    </div>

                </div>
            </div>

            <div class="page-copy copy mt-5" data-aos="fade-up" data-aos-delay="150">
                <h4>Opening a Workbook</h4>

                <p>When you first start Excel, a blank default workbook is loaded. You can use this blank workbook to start a new worksheet. But what if you have already used (saved and closed) the default workbook? At this point you have a couple of options. One would be to open a workbook stored on disk; another would be to create another new, blank workbook.</p>
                <p>Please call us on <a href="tel:8888150604">888.815.0604</a> to <a href="/excel-training.php">register for an Excel class</a> in Chicago or Los Angeles.</p>

                <h4>Opening an Existing Workbook</h4>

                <p>You open an existing workbook using the Open screen, illustrated in Figure&nbsp;2-1. By clicking the browse button shown in Figure 2-1 to make the Open Dialogue box appears Figure 2-2. Besides specifying a file to open, you can also tell Excel to search for a particular file based on a set of criteria, such as a word or property it contains. The Open dialog box also contains several tools that allow you to change the view in the Open dialog box. Most of these tools, similar to those used in <em>My Computer</em> navigation, will be familiar to you already.</p>

                <p><img class="aligncenter mt-5" src="/dist/images/excel/excel-figure2-1.png" alt=""></p>
                <p><img class="aligncenter" src="/dist/images/excel/excel-figure2-2.png" alt=""></p>

                <h4>Method</h4>
                <h6>To open an existing workbook:</h6>
                <ol>
                    <li>Click the FILE button and choose Open.</li>
                    <li>In the Open screen, if the file you wish to open is not listed, click “Browse” to select the desired drive or directory.</li>
                    <li>In the file list box, select the desired file.</li>
                    <li>Choose Open.</li>
                </ol>
                <p>Note: You can also double-click the file to select and open it. <a href="/excel-training-chicago.php">Excel classes in the Chicago Loop</a>.</p>

                <h4>Exercise</h4>
                <p>In the following exercise, you will open an existing workbook and then close it.</p>

                <ol>
                    <li>Load Excel. [Excel loads, and Book1 appears].</li>
                    <li>Click the FILE button and choose OPEN. [The Open Screen appears].</li>
                    <li>Click Browse and in drop-down list box, select the required drive or directory.</li>
                    <li>In the file list box, under NAME, double-click the Data folder.</li>
                    <li>In the file list box, select Budget May.</li>
                    <li>Choose Open. [Budget May opens].</li>
                    <li>Close the open Budget May workbook. Do not save any changes. [The worksheet window is blank].</li>
                </ol>

                <h4>Creating a New Workbook</h4>
                <p>When you create a new workbook, you have the option of choosing one of Excel’s built-in workbook templates. Depending on how Excel was installed, these could include templates for purchase orders, expense reports, invoices, and more. The template you’ll use the most, however, will probably be the blank workbook. Although you can access the blank workbook template via the NEW command from the FILE BUTTON menu, there is an easier way.</p>
                <p><img src="/dist/images/excel/excel-figure2-3.png" alt=""></p>


                <h4>Method</h4>
                <h6>To open a recently used workbook:</h6>
                <ol>
                    <li>From the File menu, choose the file you want to open.</li>
                </ol>

                <h4>Exercise</h4>
                <h6>In the following exercise, you will open and close a recently used workbook.</h6>
                <ol>
                    <li>From the File menu, select Budget May. [The selected file opens].</li>
                    <li>Close the open workbook.</li>
                </ol>

                <p>Note: The more recently opened files will push out the ones less recently opened and these will disappear off the bottom of the list. You can override this by clicking the push-pin symbol next to a file you wish to keep in the list. In Figure 2-5 the file Better Coffee Sales has been permanently saved in the list. Click the symbol again to release the file from permanent residence on the list.</p>

                <p><img src="/dist/images/excel/excel-figure2-4.png" alt=""></p>





            </div>
        </div>

    </main>


    <div class="mb-5">&nbsp;</div>


    <div class="section-widget g-text-excel" data-aos="fade-up" >
        <div class="container">

            <div class="widget-row">
                <div class="widget ">
                    <h4 class="widget-title">Related Excel Lessons</h4>
                    <ul>
                        <li><a href="/excel/lessons/starting-excel.php">Starting Microsoft Excel</a></li>
                        <li><a href="/excel/lessons/excel-interface.php">The Excel Screen Interface</a></li>
                        <li><a href="/excel/lessons/excel-selections.php">Excel Selections</a></li>
                        <li><a href="/excel/lessons/data-entry.php">Data Entry in Excel</a></li>
                        <li><a href="/excel/lessons/columns-saving.php">Column Widths and Saving Workbooks </a></li>
                        <li><a href="/excel/lessons/printing-exiting.php">Printing and Exiting</a></li>
                        <li><a href="/excel/lessons/building-formulas.php">Basic Excel Formulas</a></li>
                        <li><a href="/excel/lessons/editing-copying-formulas.php">Editing and Copying Formulas</a></li>
                        <li><a href="/excel/lessons/basic-functions.php"> Basic Excel Functions</a></li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Public Excel classes in Chicago and LA</h4>
                    <p>Do you need to join a 1-day Excel workshop? We run beginner, intermediate and advanced classes every month. Check our past students  <a href="/testimonials.php?course_id=19">Excel class reviews</a>.<br>
                    </p>
                </div>
                <div class="widget">
                    <h4 class="widget-title">Excel courses</h4>
                    <ul>
                        <li><a href="#">Excel Level 1 - Introduction</a></li>
                        <li><a href="#">Excel Level 2 - Intermediate</a></li>
                        <li><a href="#">Excel Level 3 - Advanced</a></li>
                        <li><a href="#">Excel Level 4 - Macros and VBA</a></li>
                        <li><a href="#">Get Certified in Excel</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/sections/locations.php'; ?>
<?php include_once $_SERVER["DOCUMENT_ROOT"]. '/footer.php'; ?>